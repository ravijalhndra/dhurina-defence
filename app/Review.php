<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    
     protected $table='review';

    protected $fillable=['user_id', 'mentor_id', 'title', 'review', 'rating'];

    
}
