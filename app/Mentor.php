<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mentor extends Model
{
    //
    
     protected $table='mentors';

    protected $fillable=['timestamp', 'field_type', 'groupid', 'likes', 'comment', 'view', 'posttype', 'uid', 'posturl', 'post_description', 'jsondata'];

    
}
