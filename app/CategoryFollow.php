<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryFollow extends Model
{
    //
    
     protected $table='category_follow_rec';

    protected $fillable=['profile_uid','category_id'];

    
}
