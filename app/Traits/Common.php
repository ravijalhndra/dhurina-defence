<?php 
namespace App\Traits;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Institute;
use App\SentNotification;
use Hash;
use Mail;
use Auth;
use View;
use DB;
use App\Mail\AddEmail;
use Zipper;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

trait Common
{
    public function filter_test($new_record)
    {
          $field_type=2;
          foreach ($new_record as $key => $new_rec) {
          $notification=DB::table('sent_notifications')->where('postid',$new_rec->id)->first();
                if($notification)
                {
                    $new_rec->notification="sent";
                }
                else
                {
                    $new_rec->notification="";
                }
            $applicant=DB::table('result_record')->where('testid',$new_rec->id)->count();
            $new_rec->applicant=$applicant;
            $added_questions=DB::table('test_question')->where('testid',$new_rec->id)->count();
            $new_rec->added_questions=$added_questions;
            // dd($new_rec);

            $comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$new_rec->id)->get();
            foreach ($comments as $comment) {
                $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
                foreach ($reply as $key => $rep) {
                  $comment->reply[]=$rep;
                }
              $new_rec->comments[]=$comment;
            }
          }
        return $new_record;
    }

    public function add_test_common($request,$premium)
    {
        $field_type=2;
        $post_type=0;
        if($request->add_comment)
        {
            $add_comment=$request->add_comment;
        }
        else
        {
            $add_comment="on";
        }

        
        $ans_status="yes";
        

       if($request->ans_status_new)
        {
            $ans_status_new=$request->ans_status_new;
        }
        else
        {
            $ans_status_new="no";
        }

         if($request->offline_status)
        {
            $offline_status=$request->offline_status;
        }
        else
        {
            $offline_status="no";
        }

        if($request->add_like)
        {
            $add_like=$request->add_like;
        }
        else
        {
            $add_like="on";
        }

        if($request->negative_marks)
        {
            $negative_marks=$request->negative_marks;
        }
        else
        {
            $negative_marks="no";
        }


        $total_questions=0;
        for ($i=0; $i <count($request->cat) ; $i++) {
            if($request->cat[$i]!="")
            {

                $sub_test_data[]=array('test'=>$request->cat[$i],'question_no'=>$request->questions[$i]);
                $total_questions=$total_questions+$request->questions[$i];
            } 
        }
        
        if($request->type_institute == 'group')
        {
            if(!isset($request->groupid))
            {
                $groupid="all";
            }
            else
            {
            $groupid=implode(",", $request->groupid);
            }
        }
        else if($request->type_institute == 'college')
        {
            $groupid=0;
            $post_type = 1;
            $college_id = $request->college;
            $stream = $request->stream;
            $courseid = $request->course;
            $year = $request->year;
        }
        else{
            $groupid=implode(",", $request->groupid);
            $post_type = 1;
            $college_id = 0;
            $stream = 0;
            $courseid = 0;
            $year = 0;

        }
        
        // $sub_test=explode(',',$request->sub_tests);
        
        // for ($i=0; $i <count($sub_test) ; $i++)
        // { 
        //     $sub_tests=explode('=',$sub_test[$i]);
        //     $sub_test_data[]=array('test'=>$sub_tests[0],'question_no'=>$sub_tests[1]);
        //     $total_questions=$total_questions+$sub_tests[1];
        // }
        $post_description=json_encode($sub_test_data);
        if ($request->payment=="paid") 
        {
            if ($request->amount=="")
            {
                $amount='10';
            }
            else
            {
                $amount=$request->amount;
            }
        }
        else
        {
            $amount="free";
        }
       
        if($request->type_institute=='group')
        {
            if($request->posttype=='create_topic')
            {
                $posttype=$request->new_topic;
            }
            else
            {
                $posttype=$request->posttype;
            }
       }

       elseif ($request->type_institute=='college')
       {
          
            if($request->posttype_college=='create_topic')
            {
                $posttype=$request->new_topic_college;
            }
            else
            {
                $posttype=$request->posttype_college;
            }
        }
       
        if($request->secret_key!="")
        {
            $secret_key=$request->secret_key;
        }
        else
        {
            $secret_key="0";
        }

        if($request->image!="")
        {
            $file=$request->image;
            $rand=rand(1,1000000);
            $destinationPath = public_path()."/mechanicalinsider/admin/tests_images/images/";
            $filename = $file->getClientOriginalName();
            $filename=$rand.$filename;
            $upload=$file->move($destinationPath, $filename);
            $image="http://161.202.16.83/mechanicalinsider/admin/tests_images/images/".$filename;
                    
        }
        elseif($request->link!="")
        {
            $image=$request->link;
        }
        else
        {
          $image="http://161.202.16.83/mechanicalinsider/admin/tests_images/images/default_test_image.jpg";
        }


        $data=[
        'writer'=>$request->postby,
        'testname'=>$request->test_name,
        'description'=>$request->description,
        'time'=>$request->time,
        'secret_key'=>$secret_key,
        'question'=>$total_questions,
        'payment'=>$amount,
        'image'=>$image,
        'add_comment'=>$add_comment,
        'add_like'=>$add_like,
        'ans_status'=>$ans_status,
        'ans_status_new'=>$ans_status_new,
        'offline_status'=>$offline_status,
        'negative_marks'=>$negative_marks,
        'start_date'=>$request->start_date." ".$request->start_time,
        'end_date'=>$request->end_date." ".$request->end_time,
        'first_prize'=>$request->first_prize,
        'second_prize'=>$request->second_prize,
        'third_prize'=>$request->third_prize,
        'result_declare'=>"no"
        ];

        $jsondata=json_encode($data);
       
        $data=DB::table('common_table')->orderBy('id','desc')->first();
        if($data)
        {
            $l_id=$data->id;
            $last_id=$data->id+1;
        }
        else
        {
            $l_id=1;
            $last_id=1;
        }
       
        
       
        if($request->type_institute=="both"){ 
            $posttype=$request->posttype_all;
            $id=DB::table('common_table')->insertGetId(
                ['id'=>$last_id,'view'=>'0','school'=>json_encode($request->sub),'field_type'=>2,'post_type'=>2,'posttype'=>$posttype,'groupid'=>'all','post_description'=>$post_description,'jsondata'=>$jsondata,'status'=>'published','premium'=>$premium,'uid'=>Auth::user()->email]
            );
            $collegedata = DB::table('college_data')->insertGetId(
                ['postid'=>$id,'courseid'=>0,'college_id' =>0,'stream'=>0,'year'=>0]);           
          
        }
        elseif($request->type_institute=="college"){
           
            $id=DB::table('common_table')->insertGetId(
                ['id'=>$last_id,'view'=>'0','field_type'=>$field_type,'school'=>json_encode($request->sub),'post_type'=>$post_type,'posttype'=>$posttype,'groupid' => $groupid,'post_description'=>$post_description,'jsondata'=>$jsondata,'status'=>'published','premium'=>$premium,'uid'=>Auth::user()->email]
           );
        }
    
        elseif($request->type_institute=="group"){
       
        $id=DB::table('common_table')->insertGetId(
            ['id'=>$last_id,'view'=>'0','field_type'=>$field_type,'school'=>json_encode($request->sub),'post_type'=>$post_type,'posttype'=>$posttype,'groupid' => $groupid,'post_description'=>$post_description,'jsondata'=>$jsondata,'status'=>'published','premium'=>$premium,'uid'=>Auth::user()->email]
       );
    }
        if($id)
        {
            
            $dir_name=$id;
            mkdir(public_path()."/mechanicalinsider/admin/tests_images/".$dir_name, 0777, true);
            
            if($request->type_institute=="college")
            {
                $collegedataid = DB::table('college_data')->insertGetId(
                    ['postid'=>$id,'field_type'=>$field_type,'courseid'=>$courseid,'college_id' => $college_id,'stream'=>$stream,'year'=>$year]);
                }
                
            if($request->posttype=='create_topic')
                {
                    if($request->type_institute=="group")
                    {
                    $find=DB::table('topic_search')->where('topic_name',$request->new_topic)->first();
                    
                    if($find!='null' && !empty($find))
                    {
                        $new_testcount=$find->testcount + 1;
                        DB::table('topic_search')->where('topic_name',$request->new_topic)->update(['testcount'=>$new_testcount]);
                    }
                    else
                    {
                       DB::table('topic_search')->insert(['topic_name'=>$request->new_topic,'postcount'=>'0','testcount'=>'1']); 
                    }
                   }
                }
                  else
                  {
                      
                    if($request->type_institute=="college")
                    {
                    $find=DB::table('topic_search')->where('topic_name',$request->new_topic_college)->first();
                    if($find!='null' && !empty($find))
                    {
                    $new_testcount=$find->testcount + 1;
                    DB::table('topic_search')->where('topic_name',$request->new_topic_college)->update(['testcount'=>$new_testcount]);
                    }
                    else
                    {
                      $topic_id=DB::table('topic_search')->insertGetId(['topic_name'=>$request->new_topic_college,'postcount'=>'0','testcount'=>'1','topic_type'=>'1']);
                      $topic_college=DB::table('topic_college')->insert(['topicid'=>$topic_id,'collegeid'=>$college_id,'stream'=>$stream,'year'=>$year]); 
                    }
                  } 
                    
                  }
                }
                else
                {
                    $find=DB::table('topic_search')->where('topic_name',$request->posttype)->first();
                    if($find)
                    {
                        $new_testcount=$find->testcount + 1;
                        DB::table('topic_search')->where('topic_name',$request->posttype)->update(['testcount'=>$new_testcount]);
                    }
                }
            
            return $last_id;
    }

    public function update_test_common($request)
    {
      $field_type=2;
        if($request->add_comment)
        {
            $add_comment=$request->add_comment;
        }
        else
        {
            $add_comment="on";
        }
        if($request->add_like)
        {
            $add_like=$request->add_like;
        }
        else
        {
            $add_like="on";
        }

        if($request->ans_status_new)
        {
            $ans_status_new=$request->ans_status_new;
        }
        else
        {
            $ans_status_new="no";
        }

        $ans_status="yes";

        if($request->offline_status)
        {
            $offline_status=$request->offline_status;
        }
        else
        {
            $offline_status="no";
        }

        if($request->negative_marks)
        {
            $negative_marks=$request->negative_marks;
        }
        else
        {
            $negative_marks="no";
        }

        $total_questions=0;
        for ($i=0; $i <count($request->cat) ; $i++) { 
           if($request->cat[$i]!="")
            {
                
                $sub_test_data[]=array('test'=>$request->cat[$i],'question_no'=>$request->questions[$i]);
                $total_questions=$total_questions+$request->questions[$i];
            }
        }
       
      
        if($request->type_institute=="group")
        {   
           
            if(!isset($request->groupid))
            {
                $groupid="";
            }
            else
            {
            $groupid=implode(",", $request->groupid);
            }
            $post_type = 0;
        }
       
        elseif($request->type_institute=="college")
        {
            $groupid=0;
            $post_type =1;
            $college_id =$request->college;
           
            $stream =$request->stream;
           
            $courseid =$request->course;
         
            $year =$request->year;
        }
     
        elseif($request->type_institute=="both")
        {
            $groupid=implode(",", $request->groupid);
            $posttype=$request->posttype_all;
            $post_type = 2;
            $college_id = 0;
            $stream = 0;
            $courseid = 0;
            $year = 0;
        }
       
        // if(!isset($request->groupid))
        // {
        //     $groupid="";
        // }
        // else
        // {
        //   $groupid=implode(",", $request->groupid);
        // }
        
        $post_description=json_encode($sub_test_data);
        if ($request->payment=="paid") 
        {
            if ($request->payment1=="")
            {
                $amount='10';
            }
            else
            {
                $amount=$request->payment1;
            }
        }
        else
        {
            $amount="free";
        }
        // if($request->posttype=='create_topic')
        // {
        //     $posttype=$request->new_topic;
        // }
        // else
        // {
        //     $posttype=$request->posttype;
        // }

        if($request->type_institute=='group')
        {
            if($request->posttype=='create_topic')
            {
                $posttype=$request->new_topic;
            }
            else
            {
                $posttype=$request->posttype;
            }
       }

       elseif ($request->type_institute=='college')
       {
          
            if($request->posttype_college=='create_topic')
            {
                $posttype=$request->new_topic_college;
            }
            else
            {
                $posttype=$request->posttype_college;
            }
        }

        if($request->image!="")
        {
            $file=$request->image;
            $rand=rand(1,1000000);
            $destinationPath = public_path()."/mechanicalinsider/admin/tests_images/images/";
            $filename = $file->getClientOriginalName();
            $filename=$rand.$filename;
            $upload=$file->move($destinationPath, $filename);
            $image="http://hellotopper.in/mechanicalinsider/admin/tests_images/images/".$filename;
                    
        }
        elseif($request->link!="")
        {
            $image=$request->link;
        }
        else
        {
          $image="http://hellotopper.in/mechanicalinsider/admin/tests_images/images/default_test_image.jpg";
        }

        if($request->secret_key!="")
        {
            $secret_key=$request->secret_key;
        }
        else
        {
            $secret_key="0";
        }

        if( $request->groupid != '')
        {
            $groupid=implode(",", $request->groupid);
        }
        else 
        {
            $groupid=0;
        }
         

        $data=[
        'writer'=>$request->postby,
        'testname'=>$request->test_name,
        'description'=>$request->description,
        'time'=>$request->time,
        'question'=>$total_questions,
        'secret_key'=>$secret_key,
        'payment'=>$amount,
        'image'=>$image,
        'add_comment'=>$add_comment,
        'add_like'=>$add_like,
        'ans_status'=>$ans_status,
        'ans_status_new'=>$ans_status_new,
        'offline_status'=>$offline_status,
        'negative_marks'=>$negative_marks,
        'start_date'=>$request->start_date." ".$request->start_time,
        'end_date'=>$request->end_date." ".$request->end_time,
        'first_prize'=>$request->first_prize,
        'second_prize'=>$request->second_prize,
        'third_prize'=>$request->third_prize,
        'result_declare'=>"no"
        ];

        $postdata=DB::table('common_table')->where('id',$request->id)->first();
        if($postdata)
        {
            $old_json_data=json_decode($postdata->jsondata,true);
            if(isset($old_json_data['job_id']))
            {
                $data['job_id']=$old_json_data['job_id'];
            }
        }

        $jsondata=json_encode($data);
           
            if($request->type_institute=="group")
            {
                $find=DB::table('topic_search')->where('topic_name',$request->new_topic)->first();
                
                if($find!='null' && !empty($find))
                {
                    $new_testcount=$find->testcount + 1;
                    DB::table('topic_search')->where('topic_name',$request->new_topic)->update(['testcount'=>$new_testcount]);
                }
                else
                {
                DB::table('topic_search')->insert(['topic_name'=>$request->new_topic,'postcount'=>'0','testcount'=>'1']); 
                }
            }
           
           
            $college_id=DB::table('college_data')->where('postid',$request->id)->first();
            if($college_id!='null' && !empty($college_id))
            {
            $delete_id= DB::table('college_data')->where('postid',$request->id)->delete();  
            } 

            if($request->sub !=''){
                $subject=json_encode($request->sub);
            }
            else{
                $subject=json_encode([]);
            }

            $update=DB::table('common_table')->where('id',$request->id)->update(
            ['groupid' => $groupid,'post_description'=>$post_description,'jsondata'=>$jsondata,'school'=>$subject]

            
            // ['groupid' => $groupid,'post_description'=>$post_description,'posttype'=>$posttype,'jsondata'=>$jsondata,'status'=>'notpublished']
        );



         //--------->Update in case of college---------->
           if($request->type_institute=="college")
                {
                    $post_type=1;

                    $update_clg=DB::table('common_table')->where('id',$request->id)->update(
                    ['posttype'=>$posttype,'post_type'=>$post_type,'groupid' => $groupid,'post_description'=>$post_description,'jsondata'=>$jsondata]);
                    
                    $status=DB::table('common_table')->select('status')->where('id',$request->id)->first();
                    $update_id=DB::table('college_data')->where('postid',$request->id)->first();
                   
                    if($status->status=='notpublished')
                    {
                        if($update_id!='null' && !empty($update_id))
                        {
                        
                        $update_college = DB::table('college_data')->where('postid',$request->id)->update(
                        ['courseid'=>$request->course,'college_id' =>$request->college,'stream'=>$request->stream,'year'=>$request->year,'field_type'=>2,'status'=>'notpublished']);
                        }
                        else
                        {
                            $update_college = DB::table('college_data')->insert(
                                ['postid'=>$request->id,'courseid'=>$request->course,'college_id' => $request->college,'stream'=>$request->stream,'year'=>$request->year,'field_type'=>2,'status'=>'notpublished']);
                        }
                    }
                    else{

                        if($update_id!='null' && !empty($update_id))
                        {
                        
                        $update_college = DB::table('college_data')->where('postid',$request->id)->update(
                        ['courseid'=>$request->course,'college_id' =>$request->college,'stream'=>$request->stream,'year'=>$request->year,'field_type'=>2,'status'=>'published']);
                        }
                        else
                        {
                            $update_college = DB::table('college_data')->insert(
                                ['postid'=>$request->id,'courseid'=>$request->course,'college_id' => $request->college,'stream'=>$request->stream,'year'=>$request->year,'field_type'=>2,'status'=>'published']);
                        }

                    }
          
                    $find=DB::table('topic_search')->where('topic_name',$request->new_topic_college)->first();
                    if($find!='null' && !empty($find))
                    {
                    $new_testcount=$find->testcount + 1;
                    DB::table('topic_search')->where('topic_name',$request->new_topic_college)->update(['testcount'=>$new_testcount]);
                    }
                    else
                    {
                      $topic_id=DB::table('topic_search')->insertGetId(['topic_name'=>$request->new_topic_college,'postcount'=>'0','testcount'=>'1','topic_type'=>'1']);
                      $topic_college=DB::table('topic_college')->insert(['topicid'=>$topic_id,'collegeid'=>$college_id,'stream'=>$stream,'year'=>$year]); 
                    }
                    
            }



            //update in condition of both//
            if($request->type_institute=="both")
            {
                $status=DB::table('common_table')->select('status')->where('id',$request->id)->first();
               
                $update_common=DB::table('common_table')->where('id',$request->id)->update(
                    ['school'=>json_encode($request->sub) ,'groupid' => $groupid,'post_description'=>$post_description,'jsondata'=>$jsondata]);
                
                    $update_id=DB::table('college_data')->where('postid',$request->id)->first();
                   
                    if($update_id!='null' && !empty($update_id))
                    {
                        $update_college= DB::table('college_data')->where('postid',$request->id)->update(
                            ['courseid'=>0,'college_id' => 0,'stream'=>0,'year'=>0,'field_type'=>2]);
                    }
                    else
                    {
                        if($status->status=='published')
                        {
                        $insert_college= DB::table('college_data')->insert(
                            ['postid'=>$request->id,'courseid'=>0,'college_id'=>0,'stream'=>0,'year'=>0,'field_type'=>2,'status'=>'published']);
                        }
                        else
                        {
                            $insert_college= DB::table('college_data')->insert(
                                ['postid'=>$request->id,'courseid'=>0,'college_id'=>0,'stream'=>0,'year'=>0,'field_type'=>2,'status'=>'unpublished']);
                         
                        }
                        }
                
           }


      

        return $update;
    }

    public function test_search_common()
    {
      
    }

    public function send_otp_sms()
    {
        return "hello world";
        $sms=config('services.sms_api_key');
        $api_key=$sms['api_key'];
        return file_get_contents("https://2factor.in/API/V1/$api_key/SMS/+91$mobile/$otp/OTP");
    }

    public function create_video($title)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.jwplayer.com/v2/sites/n7aTnI7S/media/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{"upload":{"method":"multipart"},"metadata":{"title":"'.$title.'"}}',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: hhqUTTa88ULpd6R8MkZbg2InVEcxeWF6WTBVWEZUVGpoa2MwUTVjVmhhTVdKeFFYVm4n'
        ),
        ));

        $response = curl_exec($curl);
        $err1 = curl_error($curl);
        curl_close($curl);
        return ['response'=>json_decode($response),'err'=>json_decode($err1)];
    }
    public function get_thumbnails($media_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://cdn.jwplayer.com/v2/media/'.$media_id.'/poster.jpg',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $res = curl_exec($curl);

        curl_close($curl);
        return $res;
    }
    public function get_video($media_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.jwplayer.com/v2/sites/n7aTnI7S/media/'.$media_id.'/',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: hhqUTTa88ULpd6R8MkZbg2InVEcxeWF6WTBVWEZUVGpoa2MwUTVjVmhhTVdKeFFYVm4n',
            'Accept: application/json'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
       
        return $response;
      
    }
}