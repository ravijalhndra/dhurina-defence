<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    protected $fillable=['name','email','phone','address'];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
