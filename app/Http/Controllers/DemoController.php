<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Institute;
use App\SentNotification;
use Hash;
use Mail;
use Auth;
use View;
use DB;
use App\Mail\AddEmail;
use Zipper;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use Response;

class DemoController extends Controller
{
    
    public function export_csv(Request $request)
    {
        $data=DB::table('test_question')->where('testid',210)->get();
        foreach($data as $d)
        {
            $data1=json_decode($d->jsondata,true);
            // dd($data1)
            $d->question=$data1['data']['question']['value'];
            $d->question_image=$data1['data']['question']['image'] != "NULL"? url('/mechanicalinsider/admin/'.$data1['data']['question']['image']):"";

            $d->option_a=$data1['data']['option a']['value'];
            $d->option_a_image=$data1['data']['option a']['image'] != "NULL"? url('/mechanicalinsider/admin/'.$data1['data']['option a']['image']):"";

            $d->option_b=$data1['data']['option b']['value'];
            $d->option_b_image=$data1['data']['option b']['image'] != "NULL"? url('/mechanicalinsider/admin/'.$data1['data']['option b']['image']):"";

            $d->option_c=$data1['data']['option c']['value'];
            $d->option_c_image=$data1['data']['option c']['image'] != "NULL"? url('/mechanicalinsider/admin/'.$data1['data']['option c']['image']):"";

            $d->option_d=$data1['data']['option d']['value'];
            $d->option_d_image=$data1['data']['option d']['image'] != "NULL"? url('/mechanicalinsider/admin/'.$data1['data']['option d']['image']):"";

            $d->option_e=$data1['data']['option e']['value'];
            $d->option_e_image=$data1['data']['option e']['image'] != "NULL"? url('/mechanicalinsider/admin/'.$data1['data']['option e']['image']):"";

            $d->answer=$data1['data']['answer']['value'];
            $d->answer_image=$data1['data']['answer']['image'] != "NULL"? url('/mechanicalinsider/admin/'.$data1['data']['answer']['image']):"";

            $d->hint=$data1['data']['hint']['value'];
            $d->hint_image=$data1['data']['hint']['image'] != "NULL"? url('/mechanicalinsider/admin/'.$data1['data']['hint']['image']):"";

            $d->solution=$data1['data']['solution']['value'];
            $d->solution_image=$data1['data']['solution']['image'] != "NULL"? url('/mechanicalinsider/admin/'.$data1['data']['solution']['image']):"";
  
        }

        $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=file.csv",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
    );

    $columns = array('Id', 'Sub Test Name', 'Question', 'Question Image', 'Option A', 'Option A Image', 'Option B', 'Option B Image', 'Option C', 'Option C Image', 'Option D', 'Option D Image', 'Option E', 'Option E Image', 'Answer', 'Answer Image', 'Hint', 'Hint Image', 'Solution', 'Solution Image',);

    $callback = function() use ($data, $columns)
    {
        $file = fopen('php://output', 'w');
        fputcsv($file, $columns);

        foreach($data as $d) {
            fputcsv($file, array($d->id, $d->sub_test_name, $d->question, $d->question_image, $d->option_a, $d->option_a_image, $d->option_b, $d->option_b_image, $d->option_c, $d->option_c_image, $d->option_d, $d->option_d_image, $d->option_e, $d->option_e_image, $d->answer, $d->answer_image, $d->hint, $d->hint_image, $d->solution, $d->solution_image,));
        }
        fclose($file);
    };
    return Response::stream($callback, 200, $headers);


        // dd($data);
    }

    public function publish_test($id)
    {
          $last_id=DB::table('common_table')->orderBy('id','desc')->first();
          $last_id=$last_id->id +1;
          DB::table('common_table')->where('id',$id)->update(['id'=>$last_id,'status'=>'published']);
          DB::table('result_record')->where('testid',$id)->update(['testid'=>$last_id]);
          rename(public_path()."/mechanicalinsider/admin/tests_images/".$id,public_path()."/mechanicalinsider/admin/tests_images/".$last_id);
          DB::table('test_question')->where('testid',$id)->update(['testid'=>$last_id]);

          $zip=$this->make_test_zip($last_id);
          return redirect('test')->with('success','Test published Successfully.'); 
    }

    public function unpublish_test($id)
    {
        
        DB::table('common_table')->where('id',$id)->update(['status'=>'notpublished']);
            return redirect('test')->with('success','Test Unpublished Successfully.');
        
    }

    public function make_test_zip($id)
    {
        
        $dir_name=$id;
         $dir=public_path()."/mechanicalinsider/admin/tests_images/".$dir_name."/";
         // $path=public_path()."/mechanicalinsider/admin/tests_images/$dir_name.zip";
         $des=public_path()."/mechanicalinsider/admin/tests_images/$dir_name.zip";

        if(file_exists(public_path()."/mechanicalinsider/admin/tests_images/$dir_name".'.zip'))
        {
           unlink(public_path()."/mechanicalinsider/admin/tests_images/$dir_name".'.zip');
        }
        
         // $zip= $this->Zip( $dir,$path);

        // $dir="./TestingTest";

$files = array(); 

   $cdir = scandir($dir); 
   foreach ($cdir as $key => $value) 
   { 
      if (!in_array($value,array(".",".."))) 
      { 
      
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
         { 
            $files[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
         } 
         else 
         { 
            $files[] = $dir.$value; 
         } 
      } 
   } 

   print_r($files);

$result = $this->create_zip($files,$des,false,$dir_name);




        return redirect('test');

    }

    

 public function create_zip($files,$destination,$overwrite,$dir_name) {

//if the zip file already exists and overwrite is false, return false
    if(file_exists($destination) && !$overwrite) { return false; }
    //vars
    $valid_files = array();
    //if files were passed in...
    if(is_array($files)) {
        //cycle through each file
        foreach($files as $file) {
            //make sure the file exists
            if(file_exists($file)) {
                $valid_files[] = $file;
            }
        }
    }
    //if we have good files...
    if(count($valid_files)) {
        //create the archive
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }
        //add the files
        foreach($valid_files as $file) {
            $new_filename = $dir_name."/".substr($file,strrpos($file,'/') + 1);
            $zip->addFile($file,$new_filename);

        }
        //debug
        //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
        
        //close the zip -- done!
        $zip->close();
        
        //check to make sure the file exists
        return file_exists($destination);
    }
    else
    {
        return false;
    } 
}



    



}