<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Redirect;
use Hash;
use Mail;
use Auth;
use View;
use DB;

class NotifyController extends Controller
{
    public function notification(Request $r)
    {
		$title='Notification';  
		$new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();      
        return view::make('notification', compact('title','new_Courses')); 
    }

    public function send_notification(Request $r)
    {
		$statestags = array();

		if($r->type == 'school')
		{
			$mode='tags';
			$statestags[]=array("key"=>'ref_code',"relation"=>'!=',"value"=>'');			
		}
		elseif($r->type == 'edudream')
		{
			$mode='tags';
			$statestags[]=array("key"=>'ref_code',"relation"=>'=',"value"=>'');
		}

		elseif($r->type == 'class')
		{
			$mode='player_id';
			$course_access=DB::table('course_access');
			if( $r->status == 'wherenotin' )
			{
				$course_access=$course_access->whereNotIn('course_id',$r->course_id);
			}
			else 
			{
				$course_access=$course_access->whereIn('course_id',$r->course_id);
			}

			$course_access=$course_access->distinct('user_id')->pluck('user_id')->toArray();
			if($this->check_count($course_access) > 0)
			{
				$user_player=DB::table('users')->wherein('id',$course_access)->whereNotNull('gcmid')->distinct('gcmid')->pluck('gcmid')->toArray();
				$statestags=$user_player;
			}
		}
		else 
		{
			$mode='all';
		}

		
		//image
		$image_url='';
			$files = $r->file('image');
			if($files!='')
			{
				$imageName = date("YmdHis").'.'.$r->image->getClientOriginalExtension();
				$success = $r->image->move(public_path('img/push'), $imageName);
				$image_url=$this->path().'/img/push/'.$imageName;				
			}

		$content=$r->msg;
		$data=array("type"=>5,"name"=>'admin_message',"image_url"=>$image_url);

		//Send notification
			$push= $this->push_notification($content,$data,'player_id',$statestags);
		//Add in DB	
			$db= $this->add_notify($r,$image_url,'admin');

		return back()->with('success','Message has been sent.');

	}
	



}
