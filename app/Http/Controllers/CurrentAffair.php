<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workshop;
use App\User;
use App\Role;
use App\Institute;
use App\SentNotification;
use App\InstituteSubadmin;
use Hash;
use DB;
use Carbon;
use View;
use Redirect;
use Mail;
use App\Mail\AddEmail;
use Auth;
class CurrentAffair extends Controller
{
    //
   
   public function add()
    {
        if(Auth::user()->groups=="all")
        {
          $groups1=DB::table('discussions')->orderBy('id','desc')->get();
          foreach ($groups1 as $group){
            $groups[]=$group->id;
          }
        }
        else
        {
          $user_groups=Auth::user()->groups;
          if(strrchr($user_groups,","))
          {
            $groups=explode(",",$user_groups);
          }
          else
          {
            $groups[]=$user_groups;
          }
        }
        foreach($groups as $group)
        {
        $data[]=DB::table('discussions')->where('id',$group)->first();
        }

        return view::make('currentAffairStore',compact('data'));
    }



     public function view()
	{
		$field_type='6';
		$record1=Workshop::where('field_type',$field_type)->orderBy('id', 'desc')->get()->toArray(); // get record from database as  using Workshop Model
    $new_record=[];
		foreach ($record1 as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec['groupid'];
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
      $new_record=$record1;
    }
    if(!empty($new_record))
    {
      foreach ($new_record as $key => $new_rec) {
        $notification=DB::table('sent_notifications')->where('postid',$new_rec['id'])->first();
            if($notification)
            {
                $new_record[$key]['notification']="sent";
            }
            else
            {
                $new_record[$key]['notification']="";
            }

        $comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$new_rec['id'])->get();
        foreach ($comments as $comment) {
          $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
            foreach ($reply as $rep) {
              $comment->reply[]=$rep;
            }
          $new_record[$key]['comments'][]=$comment;
        }
      }
    } 
    $record=$new_record;
		
		return View::make('currentAffairView',compact('record'));
		
	}
	public function store(Request $request){
		
		// echo '<pre>';
		// print_r($request->all());
		// echo '</pre>';
		try{
				 $uid=Auth::user()->email;
				$file = $request->file('logo');
				if($file)
				{
					
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/currentaffair";
                    // dd($destinationPath);
                    // Get the orginal filname or create the filename of your choice
                    $filename = $file->getClientOriginalName();
                    $filename=$rand.$filename;
                    $upload=$file->move($destinationPath, $filename);
                }
                else
                {
                    $filename="";
                		
			
			  	}
          if($request->url=="")
                {
                  $description=$request->description;
                  $url=NULL;
                }
                else
                {
                  $description=NULL;
                  $url=$request->url;
                }
				
				$groupid=implode(",", $request->groupid);
        $result['title']=$request['title'];
				// $result['url']=$url;
				$result['image']=$filename;
				$result_json=json_encode($result);
				$tags_explode=explode(",", $request['tags']);
				$work=new Workshop;
				$work->field_type='6';
				$work->post_description=$description;
				$work->posturl=$url;
        $work->uid=$uid;  
				$work->groupid=$groupid;	
				$work->jsondata=$result_json;
				$work->save();
			
			return redirect('currentAffair')->with('success','Current Affair Added Successfully.');		
		    
	}
	
	catch(Exception $e)
    	{
    		return redirect('currentAffair_store')->with('error','Something Went wrong.Please Try again.');
    	}
		
		
		
	}
	public function update_currentAffair($id,Request $request){
		
		try{
			$record=Workshop::where('id',$id)->first();
      if(Auth::user()->groups=="all")
        {
          $groups1=DB::table('discussions')->orderBy('id','desc')->get();
          foreach ($groups1 as $group){
            $groups[]=$group->id;
          }
        }
        else
        {
          $user_groups=Auth::user()->groups;
          if(strrchr($user_groups,","))
          {
            $groups=explode(",",$user_groups);
          }
          else
          {
            $groups[]=$user_groups;
          }
        }
        foreach($groups as $group)
        {
        $data[]=DB::table('discussions')->where('id',$group)->first();
        }
		//$tags=DB::table('workshop_registerfield')->where('postid',$id)->first();
		// dd($tags);
		return View::make('currentAffair_edit',compact('record','data'));
			
			
		}
		catch(Exception $e)
    	{
    		return redirect('currentAffair_store')->with('error','Something Went wrong.Please Try again.');
    	}
		
	}
	public function update_currentAffair_Action(Request $request){
		try{
				$id=$request['id'];
				 $uid=Auth::user()->email;
				$file = $request->file('logo');
				if($file)
				{
					
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/currentaffair";
                    // dd($destinationPath);
                    // Get the orginal filname or create the filename of your choice
                    $filename = $file->getClientOriginalName();
                    $filename=$rand.$filename;
                    $upload=$file->move($destinationPath, $filename);
                }
                else
                {
                    $filename=$request->old_image;
                		
			
			  	}
          if($request->url=="")
                {
                  $description=$request->description;
                  $url=NULL;
                }
                else
                {
                  $description=NULL;
                  $url=$request->url;
                }
				
          $result['title']=$request['title'];
					// $result['url']=$url;
				$result['image']=$filename;
				$result_json=json_encode($result);
				$tags_explode=explode(",", $request['tags']);
        $groupid=implode(",", $request->groupid);
				$work= Workshop::find($id);
				$work->field_type='6';
        $work->uid=$uid;  
				$work->groupid=$groupid;	
				$work->post_description=$description;
				$work->posturl=$url;
				$work->jsondata=$result_json;
				$work->save();
			return redirect('currentAffair')->with('success','Current Affair updated Successfully.');		
			
		}
		catch(Exception $e)
    	{
    		return redirect('currentAffair_store')->with('error','Something Went wrong.Please Try again.');
    	}
		
	}

	public function delete_currentAffair($id)
	{
		
		$user = DB::delete("DELETE FROM `common_table` WHERE `id`='$id'");
		 if($user>0)
		 {
		 	 return redirect::back()->with('success','Data Successfully Deleted');
		 }
		 else 
		 {
			 return redirect::back()->with('error','Some Error occured try Again later.');
		 }
		
	}

    public function currentAffairDetail($id)
    {
    	$field_type=6;
    	$query=DB::table('common_table')->where('id',$id)->first();
			$comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$query->id)->get();
			foreach ($comments as $comment) {
				$query->comments[]=$comment;
			}
		
    	return view::make('currentAffairDetail',compact('query'));
    }


    public function send_notification($id)
    {
        $post=DB::table('common_table')->where('id',$id)->first();
        $field_type=$post->field_type;
        $json=json_decode($post->jsondata,true);
        $image=str_replace(" ","%20",$json['image']);
        $thumbnail="";
        // $thumbnail=str_replace(" ","%20",$json['thumbnail']);
        $base_url=url('/mechanicalinsider/currentaffair/');
        $groupid=explode(",",$post->groupid);
  //old code
        // $uid=[];
        // foreach ($groupid as $gid) {
        //     $data=DB::table('group_follow_rec')->where('group_name',$gid)->get();
        //    foreach ($data as $d) {
        //     if(!in_array($d->profile_uid, $uid))
        //         {
        //             $uid[]=$d->profile_uid;
        //         }
        //    }
        // }
        // $player_id=[];
        // foreach ($uid as $u) {
        //     $players=DB::table('users')->where('email',$u)->get();
        //     foreach ($players as $player) {
        //             $json1=json_decode($player->notification,true);
        //             if($json1['institute']==true)
        //             {
        //                 $player_list[]=$player;
        //             }

        //         }
            
        // }
        // foreach ($player_list as $player) {
                
        //             $player_id[]=$player->gcmid;
                
        //     }
        // $content = array(
        //    "en" => $json['title']
           
        //    );
          
        //   $fields = array(
        //    'app_id' => "306439b4-709b-468e-b687-d0f9e6917268",
        //    // 'included_segments' => $pp,
        //    'include_player_ids' => $player_id,
           
        //       'data' => array("key" => "value"),
        //    'contents' => $content,
        //    "large_icon" => "http://tyroad.co.uk/hellotopper_admin/public/img/hellotopper.png",
        //    "big_picture" => "$base_url$image"
          
        //   );
        //   // print_r($fields);
           
        //   $fields = json_encode($fields);
           
           
          
        //   $ch = curl_init();
          
            
        //   curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        //   curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MDQyY2E5MTEtMzdhZS00N2EwLWI4N2UtZTM3NGFlMjcxYWUw',
        //                                              'Content-Type: application/json; charset=utf-8'));
        //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //   curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //   curl_setopt($ch, CURLOPT_POST, TRUE);
        //   curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //   $response = curl_exec($ch);
        //   curl_close($ch);
          // $result=json_decode($response,true);
//old code closes

//new code

        $content = array(
           "en" => $json['title']
           
           );
          
          $fields = array(
           'app_id' => "306439b4-709b-468e-b687-d0f9e6917268",
           'included_segments' => $groupid,
           
              'data' => array("id" => $id,"field_type"=>$field_type),
           'contents' => $content,
            "large_icon" => "",
           "big_picture" => $base_url.'/'.$image
          
          );

          $fields = json_encode($fields);
          // print_r($fields);
           
           
          
          $ch = curl_init();
          
            
          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MDQyY2E5MTEtMzdhZS00N2EwLWI4N2UtZTM3NGFlMjcxYWUw',
                                                     'Content-Type: application/json; charset=utf-8'));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_HEADER, FALSE);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

          $response = curl_exec($ch);
          curl_close($ch);
          $result=json_decode($response,true);
          // dd($result);

 //new code close


          if(array_key_exists('id', $result))
          {
            $notification=SentNotification::where('postid',$id)->first();
            if(isset($notification->postid))
            {
                SentNotification::where('postid',$id)->update(['postid'=>$id]);
            }
            else
            {
                
                $notification = new SentNotification();

                $notification->postid = $id;

                $notification->save();
            }
            return redirect()->back()->with('success','Notification Sent Successfully');
          }
          else
          {
            return redirect()->back()->with('error','Something went wrong.Please try again.');
          }
    
    }

  public function search(Request $request)
  {
    $field_type='6';
    $prefix=$request->val;
    $record=DB::select("select * from common_table where field_type='6' AND  (post_description LIKE '%$prefix%' OR posttype LIKE '%$prefix%' OR jsondata LIKE '%$prefix%') ORDER BY `id` DESC");
    $record=json_decode(json_encode($record),true);
    // dd($record);

    $new_record=[];
    foreach ($record as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec['groupid'];
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
      $new_record=$record;
    }
    if(!empty($new_record))
    {
      foreach ($new_record as $key => $new_rec) {
        $notification=DB::table('sent_notifications')->where('postid',$new_rec['id'])->first();
            if($notification)
            {
                $new_record[$key]['notification']="sent";
            }
            else
            {
                $new_record[$key]['notification']="";
            }

        $comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$new_rec['id'])->get();
        foreach ($comments as $comment) {
          $new_record[$key]['comments'][]=$comment;
          
        }
      }
    } 
    $record=$new_record;
    return View::make('current_affair_search',compact('record'));
  }
  



}
