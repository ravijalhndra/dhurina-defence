<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Workshop;
use App\User;
use App\Role;
use App\Institute;
use App\SentNotification;
use App\InstituteSubadmin;
use Hash;
use DB;
use Carbon;
use View;
use Redirect;
use Mail;
use App\Mail\AddEmail;
use Auth;

class JobsController extends Controller
{
   public function jobs()
   {
   	$field_type='3';
		 $record=Workshop::where('field_type',$field_type)->orderBy('id', 'desc')->get()->toArray(); // get record from database as  using Workshop Model
     $new_record=[];
    foreach ($record as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec['groupid'];
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
      $new_record=$record;
    }
    if(!empty($new_record))
    {
      foreach ($new_record as $key => $new_rec) {
        $notification=DB::table('sent_notifications')->where('postid',$new_rec['id'])->first();
            if($notification)
            {
                $new_record[$key]['notification']="sent";
            }
            else
            {
                $new_record[$key]['notification']="";
            }

        $comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$new_rec['id'])->get();
        foreach ($comments as $comment) {
          $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
            foreach ($reply as $rep) {
              $comment->reply[]=$rep;
            }
          $new_record[$key]['comments'][]=$comment;
        }
      }
    } 
    $record=$new_record;
		return View::make('Jobs',compact('record'));
   }

   public function add_job()
   {
    if(Auth::user()->groups=="all")
    {
      $groups1=DB::table('discussions')->orderBy('id','desc')->get();
      foreach ($groups1 as $group){
        $groups[]=$group->id;
      }
    }
    else
    {
      $user_groups=Auth::user()->groups;
      if(strrchr($user_groups,","))
      {
        $groups=explode(",",$user_groups);
      }
      else
      {
        $groups[]=$user_groups;
      }
    }
        foreach($groups as $group)
        {
        $data[]=DB::table('discussions')->where('id',$group)->first();
        }

        $course = DB::table('course')->get();
     return view::make('jobAdd',compact('data','course'));
   }

   public function job_store(Request $request)
   {
     if($request->type_institute=="group")
     {
      $groupid=implode(",", $request->groupid);
     }
     else
     {
       $groupid = 0;
       $course = $request->course;
       $college = $request->college;
       $stream = $request->stream;
       $year = $request->year;
     }
      
        $file = $request->file('logo');
                if($file) 
                {
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/jobsimage";
                    // dd($destinationPath);
                    // Get the orginal filname or create the filename of your choice
                    $filename = $file->getClientOriginalName();
                    $filename=$rand.$filename;
                    $upload=$file->move($destinationPath, $filename);
                }
                else
                {
                    $filename="download.png";
                }

                if($request->icon!="")
                {
                    $file=$request->icon;
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/jobsimage/icons/";
                    $filename = $file->getClientOriginalName();
                    $filename=$rand.$filename;
                    $upload=$file->move($destinationPath, $filename);
                    $icon="http://hellotopper.in/mechanicalinsider/jobsimage/icons/".$filename;
                            
                }
                else
                {
                    $icon=$request->link;
                }
        
                // $payment=$request->payment;
                // if($payment=='paid')
                // {
                //     $payment=$request->payment1;
                // }
                if($request->website=="")
                {
                  $description=$request->description;
                  $url=NULL;
                }
                else
                {
                  $description=NULL;
                  $url=$request->website;
                }
        $jsondata=[];
        $jsondata['website']=$url;
        $jsondata['postname']=$request->postname;
        $jsondata['lastdate']=$request->lastdate_submit;
        $jsondata['title']=$request->title;
        $jsondata['image']=$filename;
        $jsondata['icon']=$icon;
        $jsondata['post']=$request->posts;
        // $jsondata['fees']=$payment;
        $jsondata=json_encode($jsondata);
        $uid=Auth::user()->email;
       

        $insert=DB::table('common_table')->insertGetId(
            ['field_type' => 3, 'groupid' => $groupid,'uid'=>$uid,'post_description'=>$description,'jsondata'=>$jsondata]
        );

        if($insert)
        {
          if($request->type_institute=="college")
          {
            $collegedataid = DB::table('college_data')->insertGetId(
              ['postid'=>$insert,'field_type'=>3,'courseid'=>$course,'college_id' => $college,'stream'=>$stream,'year'=>$year]);
          }
            return redirect('jobs')->with('success','Job Added Successfully.');
        }
        else
        {
            return redirect('jobs')->with('error','Something went wrong. Please try again.');

        }
   }

    public function delete_job(Request $request)
    {
        $id=$request->del_id;
        $delete=DB::table('common_table')->where('id',$id)->delete();
        if($delete)
        {
            return redirect('jobs')->with('success','Post Deleted Successfully');
        }
    }

  public function edit_job($id)
    {
        if(Auth::user()->groups=="all")
        {
            $groups1=DB::table('discussions')->orderBy('id','desc')->get();
            foreach ($groups1 as $group){
              $groups[]=$group->id;
            }
        }
        else
        {
            $user_groups=Auth::user()->groups;
            if(strrchr($user_groups,","))
            {
              $groups=explode(",",$user_groups);
            }
            else
            {
              $groups[]=$user_groups;
            }
        }
        foreach($groups as $group)
        {
        $data[]=DB::table('discussions')->where('id',$group)->first();
        }        
        $all=DB::table('common_table')->where('id',$id)->first();

        $course = DB::table('course')->get();

        $collegedata = DB::table('college_data')->where('postid',$id)->first();
        $college_slected = DB::table('college')->where('course_id',$collegedata->courseid)->get();
        $stream_selected = DB::table('college')->where('id',$collegedata->college_id)->first();
        $stream = json_decode($stream_selected->stream);
        $year = $stream_selected->year;

        return view::make('edit_job',compact('data','all','course','college_slected','stream','year','collegedata'));
    }

    public function update_job(Request $request)
    {
      if($request->type_institute=="group")
      {
      $groupid=implode(",", $request->groupid);
      }
      else
      {
        $groupid = 0;
       $course = $request->course;
       $college = $request->college;
       $stream = $request->stream;
       $year = $request->year;
      }
        $file = $request->file('logo');
                if($file) 
                {
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/jobsimage";
                    // dd($destinationPath);
                    // Get the orginal filname or create the filename of your choice
                    $filename = $file->getClientOriginalName();
                    $filename=$rand.$filename;
                    $upload=$file->move($destinationPath, $filename);
                }
                else
                {
                    $filename=$request->old_image;
                }

                if($request->icon!="")
                {
                    $file=$request->icon;
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/jobsimage/icons/";
                    $filename = $file->getClientOriginalName();
                    $filename=$rand.$filename;
                    $upload=$file->move($destinationPath, $filename);
                    $icon="http://hellotopper.in/mechanicalinsider/jobsimage/icons/".$filename;
                            
                }
                else
                {
                    $icon=$request->link;
                }
        
                // $payment=$request->payment;
                // if($payment=='paid')
                // {
                //     $payment=$request->payment1;
                // }
                if($request->description!="")
                {
                  $description=$request->description;
                  $url=NULL;
                }
                else
                {
                  $description=NULL;
                  $url=$request->website;
                }
        $jsondata=[];
        $jsondata['website']=$url;
        $jsondata['postname']=$request->postname;
        $jsondata['lastdate']=$request->lastdate_submit;
        $jsondata['title']=$request->title;
        $jsondata['image']=$filename;
        $jsondata['icon']=$icon;
        $jsondata['post']=$request->post;
        // $jsondata['fees']=$payment;
        $jsondata=json_encode($jsondata);
        $uid=Auth::user()->email;
       

        $update=DB::table('common_table')->where('id',$request->id)->update(
            [ 'groupid' => $groupid,'uid'=>$uid,'post_description'=>$description,'jsondata'=>$jsondata]
        );

        if($request->type_institute=="college")
          {
            $college=DB::table('college_data')->where('postid',$request->id)->update(
              ['courseid'=>$course,'college_id' => $college,'stream'=>$stream,'year'=>$year]);
          }

        if($update)
        {
            return redirect('jobs')->with('success','Job Updated Successfully.');
        }
        else
        {
            return redirect('jobs')->with('error','Something went wrong. Please try again.');

        }  
    }
   

   public function job_detail($id)
   {
   	$field_type=3;
    	$query=DB::table('common_table')->where('id',$id)->first();
			$comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$query->id)->get();
			foreach ($comments as $comment) 
			{
				$query->comments[]=$comment;
			}
		return View::make('job_detail',compact('query'));
   }
   
    public function delete_job_comment(Request $request)
    {
        $id=$request->comment_id;
        $delete=DB::table('comment_on_post')->where('id',$id)->delete();
        if($delete)
        {
            return redirect()->back()->with('success','comment deleted Successfully');
        }
    }


    public function send_notification($id)
    {
        $post=DB::table('common_table')->where('id',$id)->first();
        $field_type=$post->field_type;

        $json=json_decode($post->jsondata,true);
        $image=str_replace(" ","%20",$json['image']);
        $thumbnail="";
        // $thumbnail=str_replace(" ","%20",$json['thumbnail']);
        $base_url=url('/mechanicalinsider/jobsimage/');
        $groupid=explode(",",$post->groupid);
//old code
        // $uid=[];
        // foreach ($groupid as $gid) {
        //     $data=DB::table('group_follow_rec')->where('group_name',$gid)->get();
        //    foreach ($data as $d) {
        //     if(!in_array($d->profile_uid, $uid))
        //         {
        //             $uid[]=$d->profile_uid;
        //         }
        //    }
        // }
        // $player_id=[];
        // foreach ($uid as $u) {
        //     $players=DB::table('users')->where('email',$u)->get();
        //     foreach ($players as $player) {
        //             $json1=json_decode($player->notification,true);
        //             if($json1['institute']==true)
        //             {
        //                 $player_list[]=$player;
        //             }

        //         }
            
        // }
        // foreach ($player_list as $player) {
                
        //             $player_id[]=$player->gcmid;
                
        //     }
        // $content = array(
        //    "en" => $json['title']
           
        //    );
          
        //   $fields = array(
        //    'app_id' => "306439b4-709b-468e-b687-d0f9e6917268",
        //    // 'included_segments' => $pp,
        //    'include_player_ids' => $player_id,
           
        //       'data' => array("key" => "value"),
        //    'contents' => $content,
        //    "large_icon" => "http://tyroad.co.uk/hellotopper_admin/public/img/hellotopper.png",
        //    "big_picture" => "$base_url$image"
          
        //   );
        //   // print_r($fields);
           
        //   $fields = json_encode($fields);
           
           
          
        //   $ch = curl_init();
          
            
        //   curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        //   curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MDQyY2E5MTEtMzdhZS00N2EwLWI4N2UtZTM3NGFlMjcxYWUw',
        //                                              'Content-Type: application/json; charset=utf-8'));
        //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //   curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //   curl_setopt($ch, CURLOPT_POST, TRUE);
        //   curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //   $response = curl_exec($ch);
        //   curl_close($ch);
        //   $result=json_decode($response,true);
//old code closes

        //new code

        $content = array(
           "en" => $json['title']
           
           );
          
          $fields = array(
           'app_id' => "306439b4-709b-468e-b687-d0f9e6917268",
           'included_segments' => $groupid,
           
              'data' => array("id" => $id,"field_type"=>$field_type),
           'contents' => $content,
            "large_icon" => "",
           "big_picture" => $base_url.'/'.$image
          
          );

          $fields = json_encode($fields);
          // print_r($fields);
           
           
          
          $ch = curl_init();
          
            
          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MDQyY2E5MTEtMzdhZS00N2EwLWI4N2UtZTM3NGFlMjcxYWUw',
                                                     'Content-Type: application/json; charset=utf-8'));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_HEADER, FALSE);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

          $response = curl_exec($ch);
          curl_close($ch);
          $result=json_decode($response,true);
          // dd($result);

 //new code close

          if(array_key_exists('id', $result))
          {
            $notification=SentNotification::where('postid',$id)->first();
            if(isset($notification->postid))
            {
                SentNotification::where('postid',$id)->update(['postid'=>$id]);
            }
            else
            {
                
                $notification = new SentNotification();

                $notification->postid = $id;

                $notification->save();
            }
            return redirect()->back()->with('success','Notification Sent Successfully');
          }
          else
          {
            return redirect()->back()->with('error','Something went wrong.Please try again.');
          }
    
    }

     public function search(Request $request)
   {
    $field_type='3';
    $prefix=$request->val;
     $record=DB::select("select * from common_table where field_type='3' AND  (post_description LIKE '%$prefix%' OR jsondata LIKE '%$prefix%') ORDER BY `id` DESC");
    $record=json_decode(json_encode($record),true); // get record from database as  using Workshop Model
     $new_record=[];
    foreach ($record as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec['groupid'];
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
      $new_record=$record;
    }
    if(!empty($new_record))
    {
      foreach ($new_record as $key => $new_rec) {
        $notification=DB::table('sent_notifications')->where('postid',$new_rec['id'])->first();
            if($notification)
            {
                $new_record[$key]['notification']="sent";
            }
            else
            {
                $new_record[$key]['notification']="";
            }

        $comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$new_rec['id'])->get();
        foreach ($comments as $comment) {
          $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
            foreach ($reply as $rep) {
              $comment->reply[]=$rep;
            }
          $new_record[$key]['comments'][]=$comment;
        }
      }
    } 
    $record=$new_record;
    return View::make('jobs_search',compact('record'));
   }
	
	
   
}
