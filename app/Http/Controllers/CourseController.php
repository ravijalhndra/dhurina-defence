<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Hash;
use Redirect;
use Auth;
use View;
use App\Mail\SchoolLogin;
use App\Sector;
use Mail;
use Storage;
use Config;
use VideoThumbnail;
use File;
use App\Mail\ForgotPassword;
use App\topic;

class CourseController extends Controller
{

    public function index()
    {  
        return view::make('index');
    }

    public function dashboard()
    {  
        return view::make('dashboard');
    }

    public function privacy_policy()
    {  
        return view::make('privacy_policy');
    }

    public function push()
    {   
        $content='Live Event-Dummy live video';
        $data=array("type"=>5,"name"=>'live_event',"id"=>8,"link"=>'https://livestream.vpray.live/live/smil:class1.smil/playlist.m3u8');
        $push= $this->push_notification($content,$data,'player_id',null);
        return $push;
    }

    public function add_course(Request $r)
    {
        if(Auth::user()->role == 'school')
        {
          return back()->with('error','Sorry,You do not access to access this page.');
        }

        $title='Add Courses';
        if(Auth::user()->role == 'school' )
        {
            $school=DB::table('admins')->where('role','school')->where('id',Auth::user()->id)->get();
        }
        else
        {
            $school=DB::table('admins')->where('role','school')->get();
        }

        $subject=DB::table('subject')->where('school_id',Auth::user()->id)->get();
        $sector=Sector::OrderBy('name','ASC')->get();       
        return view::make('add_course', compact('title','school','subject','sector'));
    }

    public function insert_course(Request $r)
    {

        $file = $r->file('banner');
        if ($file!='' && $file!='null') {
            $rand = rand(1, 1000000);
            $destinationPath = public_path().'/img';
            $filename = $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $filename = $rand.$filename;
            $upload = $file->move($destinationPath, $filename);
        } else {
            $filename = '';
        }

        if($r->school_id != '')
        {
            $school_id=$r->school_id;
        }
        else
        {
            $school_id=Auth::user()->id;
        }

        $insert = DB::table('new_courses')
        ->insert(['school_id'=>$school_id,'name' => $r->name,'description' =>$r->description,'price' =>$r->price,'discount'=>$r->discount,'sector'=>$r->sector,'image'=>$filename,'subject'=>json_encode($r->sub)]);

        if($insert)
        {
            return redirect()->route('view_course')->with('success','Course has been added successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }

    public function view_course(Request $r)
    {
        $title='View Course';
        if(Auth::user()->role == 'admin')
        {
            // $data=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',Auth::user()->id)->orderBy('position','ASC')->get();
            
            $data = DB::table('new_courses')
            ->join('sectors', 'new_courses.sector', '=', 'sectors.id')
            ->select('new_courses.*', 'sectors.name as sector')
            ->whereNull('new_courses.school_id')
            ->orWhere('new_courses.school_id',Auth::user()->id)
            ->orderBy('new_courses.position','ASC')
            ->get();
        
        }
        else
        {
            $course_id=json_decode(Auth::user()->course_id);

            $data = DB::table('new_courses')
            ->join('sectors', 'new_courses.sector', '=', 'sectors.id')
            ->select('new_courses.*', 'sectors.name as sector')
            ->whereIn('new_courses.id',$course_id)            
            ->orderBy('new_courses.position','ASC')
            ->get();

            // $data=DB::table('new_courses')->whereIn('id',$course_id)->orderBy('position','ASC')->get();
        }
        return view::make('view_course', compact('title','data'));       
    }

    public function edit_course(Request $r)
    {
        if(Auth::user()->role == 'school')
        {
          return back()->with('error','Sorry,You do not access to access this page.');
        }

        $id=decrypt($r->id);
        $data= DB::table('new_courses')->where('id',$id)->first();
        $school=DB::table('admins')->where('role','school')->get();
        $subject=DB::table('subject')->where('school_id',Auth::user()->id)->get();
        $sector=Sector::OrderBy('name','ASC')->get();  
        return view::make('edit_course', compact('data','school','subject','sector'));
    }

    public function update_course(Request $r)
    {

        if($r->school_id != '')
        {
            $school_id=$r->school_id;
        }
        else
        {
            $school_id=Auth::user()->id;
        }

        $file = $r->file('banner');
        if ($file!='' && $file!='null') {
            $rand = rand(1, 1000000);
            $destinationPath = public_path().'/img';
            $filename = $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $filename = $rand.$filename;
            $upload = $file->move($destinationPath, $filename);

            $update = DB::table('new_courses')->where('id',$r->id)
            ->update(['school_id'=>$school_id,'name' => $r->name,'description' =>$r->description,'price' =>$r->price,'discount'=>$r->discount,'sector'=>$r->sector,'image'=>$filename,'position'=>$r->position,'subject'=>json_encode($r->sub) ]);
        } 
        else 
        {
            $update = DB::table('new_courses')->where('id',$r->id)
            ->update(['school_id'=>$school_id,'name' => $r->name,'description' =>$r->description,'price' =>$r->price,'discount'=>$r->discount,'sector'=>$r->sector,'position'=>$r->position,'subject'=>json_encode($r->sub)]);
        }
    
        if($update || $update == 0)
        {
            return redirect()->route('view_course')->with('success','Course has been Updated successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }

    }

    public function delete_course(Request $r)
    {
        $check=DB::table('new_courses')->where('id',$r->del_id)->delete();
        if($check)
        {
            return redirect()->route('view_course')->with('success','Course has been deleted successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }

    }

    public function publish(Request $r)
    {
        $update = DB::table('new_courses')->where('id',$r->del)->update(['publish' => $r->type]);
        if($update)
        {
            return redirect()->route('view_course')->with('success','Course has been Updated successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }



    // _______________________________live Event_______________

    public function add_event(Request $r)
    {
        
        $title='Add Event';
        if(Auth::user()->role == 'admin')
        {
          $course=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
        }
        else
        {
          $course=DB::table('new_courses')->where('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
        }

        return view::make('add_event', compact('title','course'));
    }

    public function insert_event(Request $r)
    {
        $file = $r->file('banner');
        if ($file!='' && $file!='null') {
            $rand = rand(1, 1000000);
            $destinationPath = public_path().'/img';
            $filename = $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $filename = $rand.$filename;
            $upload = $file->move($destinationPath, $filename);
        } else {
            $filename = '';
        }

        $time=strtotime($r->start);    
        $uid=Auth::User()->email;
        $insert = DB::table('live_event')
        ->insertGetId(['course_id' => $r->course_id,'name' =>$r->name,'description' =>$r->description,'start_time'=>$time,
        'access'=>$r->access,'link'=>$r->link,'image'=>$filename,'uid'=>$uid]);

        if($insert)
        {
            //Send push notification
            $content='Live Event-'.$r->name;
            $data=array("type"=>5,"name"=>'live_event',"id"=>$insert,"link"=>$r->link);
            $push= $this->push_notification($content,$data,'all',null);

            return redirect()->route('view_event')->with('success','Event has been added successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
        
    }


    public function view_event(Request $r)
    {
        $title='View Event';
        $uid=Auth::User()->email;
        $data= DB::table('live_event')
            ->join('new_courses', 'live_event.course_id', '=', 'new_courses.id')
            ->select('live_event.*', 'new_courses.name as course_name')
            ->where('live_event.uid',$uid)
            ->get();
        return view::make('view_event', compact('title','data'));   
    }

    public function edit_event(Request $r)
    {
       
        $id=decrypt($r->id);
        if(Auth::user()->role == 'admin')
        {
          $course=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
        }
        else
        {
          $course=DB::table('new_courses')->where('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
        }
                $data= DB::table('live_event')->where('id',$id)->first();
        return view::make('edit_event', compact('data','course'));
    }

    public function update_event(Request $r)
    {
        $file = $r->file('banner');
        if ($file!='' && $file!='null') {
            $rand = rand(1, 1000000);
            $destinationPath = public_path().'/img';
            $filename = $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $filename = $rand.$filename;
            $upload = $file->move($destinationPath, $filename);


            $time=strtotime($r->start); 
            $uid=Auth::User()->email;
            $update = DB::table('live_event')->where('id',$r->id)
            ->update(['course_id' => $r->course_id,'name' =>$r->name,'description' =>$r->description,'start_time'=>$time,
            'access'=>$r->access,'link'=>$r->link,'image'=>$filename,'uid'=>$uid]);

        } 
        else 
        {
            $time=strtotime($r->start); 
            $uid=Auth::User()->email;
            $update = DB::table('live_event')->where('id',$r->id)
            ->update(['course_id' => $r->course_id,'name' =>$r->name,'description' =>$r->description,'start_time'=>$time,
            'access'=>$r->access,'link'=>$r->link,'uid'=>$uid]);
        }

        if($update)
            {
                return redirect()->route('view_event')->with('success','Event has been Updated successfully.');
            }
            else
            {
                return back()->with('error','Something went wrong, please try again!');
            }


    }

    public function delete_event(Request $r)
    {
        $check=DB::table('live_event')->where('id',$r->del_id)->delete();
        if($check)
        {
            return redirect()->route('view_event')->with('success','Event has been deleted successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }

    public function publish_event(Request $r)
    {
        $update = DB::table('live_event')->where('id',$r->del)->update(['publish' => $r->type]);
        if($update)
        {
            return redirect()->route('view_event')->with('success','Event has been Updated successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        } 
    }


    //_______________________SCHOOL MODULE_________________


    public function add_school(Request $r)
    {

        if(Auth::user()->role == 'school')
        {
          return back()->with('error','Sorry,You do not access to access this page.');
        }

        $title='Add School';
        $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',Auth::user()->id)->orderBy('position','ASC')->get();
        return view::make('add_school',compact('title','new_Courses'));
    }

    public function insert_school(Request $r)
    {
        
        $check=DB::table('admins')->where('email',$r->email)->first();
        if($check)
        {
            return back()->with('error','Oops! Email address already exist,Please use unique email address.');
        }
        else
        {
            //generated random password
                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
                $password = substr(str_shuffle($chars), 0, 8);

            //Ref Code
                // $ref_name=substr($r->name,0,3);    
                // $ref_number=substr($r->mobile,0,3);
                // $ref_code=$ref_name.$ref_number;

            $insert = DB::table('admins')
            ->insert(['email' => $r->email,'password' =>bcrypt($password),'mobile' =>$r->mobile,'city'=>$r->city,
            'role'=>'school','name'=>$r->name,'ref_code'=>$r->ref_code,'groups'=>'all','course_id'=>json_encode($r->course_id)]);
    
            if($insert)
            {
                //Send mail for login detail
                Mail::to($r->email)->send(new SchoolLogin($r->name,$password,$r->email));
                
                return redirect()->route('view_school')->with('success','School has been added successfully.');
            }
            else
            {
                return back()->with('error','Something went wrong, please try again!');
            }
        }

    }
    public function view_school(Request $r)
    {
        $title='View School';
        $data=DB::table('admins')->where('role','school')->OrderBy('id','ASC')->get();
        if($this->check_count($data) > 0)
        {
            foreach($data as $d)
            {
                //NO. of student
                    $d->student_count=0;
                    $user_count=DB::table('users')->where('referralcode',$d->ref_code)->count();
                    if($user_count > 0){
                        $d->student_count=$user_count;
                    }

                // no of buy course students
                    $d->buyCourse=0;
                    $buy=DB::table('course_access')->where('ref_code',$d->ref_code)->count();
                    if($buy > 0)
                    {
                        $d->buyCourse=$buy; 
                    }

            }
        }

        return view::make('view_school', compact('title','data'));   
    }



    public function edit_school(Request $r)
    {
        if(Auth::user()->role == 'school')
        {
          return back()->with('error','Sorry,You do not access to access this page.');
        }

        $id=decrypt($r->id);
        $data= DB::table('admins')->where('id',$id)->first();
        $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',Auth::user()->id)->orderBy('position','ASC')->get();
        return view::make('edit_school', compact('data','new_Courses'));
    }
    public function update_school(Request $r)
    {
        $insert = DB::table('admins')->where('id',$r->id)
        ->update(['mobile' =>$r->mobile,'city'=>$r->city,'name'=>$r->name,'ref_code'=>$r->ref_code,'course_id'=>json_encode($r->course_id)]);

        if($insert)
        {                
            return redirect()->route('view_school')->with('success','School has been updated successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }        

    }

    public function delete_school(Request $r)
    {
        $check=DB::table('admins')->where('id',$r->del_id)->delete();
        if($check)
        {
            return redirect()->route('view_school')->with('success','School has been deleted successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }
    
    //__________________________________ SUBJECT __________________

    public function add_subject(Request $r)
    {
        if(Auth::user()->role == 'school')
        {
          return back()->with('error','Sorry,You do not access to access this page.');
        }

        $title='Add Subject';
        return view::make('add_subject',compact('title'));
    }

    public function insert_subject(Request $r)
    {
        if(count($r->sub) > 0)
        {
            $lenght= count($r->sub);
            $school_id=Auth::User()->id;
            for ($i=0; $i < $lenght; $i++) 
            { 
                $check=DB::table('subject')->where('school_id',$school_id)->where('name',$r->sub[$i])->first();
                if(!$check)
                {

                    $file = $r->icon[$i];
                    if ($file!='' && $file!='null') 
                    {
                        $milliseconds = date("YmdHis").rand(10,100);
                        $imageName =$milliseconds .'.'.$r->icon[$i]->getClientOriginalExtension();
                        $success = $r->icon[$i]->move(public_path('img'), $imageName);
                    } else 
                    {
                        $imageName = '';
                    }

                    $insert = DB::table('subject')->insert(['name' => $r->sub[$i],'image'=>$imageName,'school_id' =>$school_id]);                
                }               
            }

            return redirect()->route('view_subject')->with('success','Subject has been added successfully.');
        } 
        else
        {
            return back()->with('error','Sorry,Subject could not be empty.');
        }

    }

    public function view_subject(Request $r)
    {
        $title='View Subject';

        if(Auth::user()->role == 'admin' )
        {
            $school_id=Auth::User()->id;
            $data=DB::table('subject')->where('school_id',$school_id)->OrderBy('id','DESC')->get();
        }
        else 
        {
            $course_id=json_decode(Auth::user()->course_id,true);
            $new_Courses=DB::table('new_courses')->whereIn('id',$course_id)->orderBy('name','ASC')->get();

            $sub=array(); 
            foreach ($new_Courses as $values) 
            {
                $sub=array_unique(array_merge(json_decode($values->subject),$sub));
            }

            $data=DB::table('subject')->whereIn('id',$sub)->get();            
        }
       
        return view::make('view_subject', compact('title','data'));   
    }

    public function edit_subject(Request $r)
    {
        if(Auth::user()->role == 'school')
        {
          return back()->with('error','Sorry,You do not access to access this page.');
        }

        $id=decrypt($r->id);
        $data= DB::table('subject')->where('id',$id)->first();
        return view::make('edit_subject', compact('data'));
    }

    public function update_subject(Request $r)
    {

        $file = $r->icon;
        if ($file!='' && $file!='null') 
        {
            $milliseconds = date("YmdHis").rand(10,100);
            $imageName =$milliseconds .'.'.$r->icon->getClientOriginalExtension();
            $success = $r->icon->move(public_path('img'), $imageName);
            
            $insert = DB::table('subject')->where('id',$r->id)->update(['name' =>$r->sub,'image'=>$imageName ]);
            
        } else 
        {
            $insert = DB::table('subject')->where('id',$r->id)->update(['name' =>$r->sub]);
        }

       
        if($insert)
        {                
            return redirect()->route('view_subject')->with('success','Subject has been updated successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        } 
    }

    public function delete_subject(Request $r)
    {
        $check=DB::table('subject')->where('id',$r->del_id)->delete();
        if($check)
        {
            return redirect()->route('view_subject')->with('success','Subject has been deleted successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }

    //--------------------ADD Video -------------------

    public function add_video(Request $r)
    {

        if(Auth::user()->role == 'school')
        {
          return back()->with('error','Sorry,You do not access to access this page.');
        }


        $title='Add Video';

        if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin')
        {
          $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();
        }
        else
        {
          $new_Courses=DB::table('new_courses')->where('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
        }

        $subject=DB::table('subject')->where('school_id',Auth::user()->id)->get();
        return view::make('add_video', compact('title','new_Courses','subject'));
    }

    public function insert_video(Request $r)
    {

        if($r->image != '')
        {   
                //S3 uploading code 
                $imageName = time().'.'.$r->image->getClientOriginalExtension();
                $image = $r->file('image');
                $t = Storage::disk('s3')->put($imageName, file_get_contents($image), 'public');
                $imageName = Storage::disk('s3')->url($imageName);

            // $imageName = date("YmdHis").'.'.$r->image->getClientOriginalExtension();
            // $success = $r->image->move(public_path('Video'), $imageName);
            // $imageName=$this->path().'/Video/'.$imageName;

            if($imageName != '')
            {
                $thumbnail_name=date("YmdHis").".jpg";
                $thumbnail=VideoThumbnail::createThumbnail($imageName, public_path('img/thumbs'),  $thumbnail_name, 1, 640, 360);               
                    // $thumb = $this->path().'/Video/thumbnail/'.$thumbnail_name;


                if($thumbnail)
                {
                    $localFile = public_path('img/thumbs').'/'.$thumbnail_name;                      
                    $th = Storage::disk('s3_thumbnail')->put($thumbnail_name,fopen($localFile, 'r+'), 'public');
                    $thumb = Storage::disk('s3_thumbnail')->url($thumbnail_name);
                    File::delete(public_path('img/thumbs/'.$thumbnail_name));
                }

                $insert = DB::table('video')->insert(['school_id' => 1,'course_id'=>json_encode($r->course_id),
                'subject_id'=>json_encode($r->subject_id),'name'=>$r->name,'video'=> $imageName,'thumbnail'=>$thumb,'timestamp'=>date("YmdHis")]);
                if($insert)
                {
                    return redirect()->route('view_video')->with('success','Video has been uploaded successfully.');
                }
                else
                {
                    return back()->with('error','Something went wrong, please try again!');
                }
            }
            else
            {
                return back()->with('error','Something went wrong, please try again!'); 
            }
        }
        else
        {
            return back()->with('error','Oops, Video could not be empty.');
        }
    }

    public function view_video(Request $request)
    {      
        
                   
       
        $title='View Video';
        $filter = $request->filter;
        $course_ids = $request->course;
        $subject_id = $request->subject;

        if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin')
        {            
            $data=DB::table('video')->where('school_id',1)->whereNotNull('thumbnail');

            //filter
            if($filter != ''){                
                $data= $data->where('name','like','%'.$filter.'%')
                ->orWhere('publish','like','%'.$filter.'%')->orderBy('id','desc');
            }
            else 
            {
                $data= $data->orderBy('id','desc');
            }

            //Course
            if($course_ids != ''){                
                $data= $data->whereRaw('json_contains(course_id, \'["'.$course_ids.'"]\')');
            }

            //subject
            if($subject_id != ''){                
                $data= $data->whereRaw('json_contains(subject_id, \'["'.$subject_id.'"]\')');
            }

            $data= $data->paginate(50);
        }
        else 
        {

            $course_id=json_decode(Auth::user()->course_id,true); 

            $data=DB::table('video')->whereNotNull('thumbnail')->where('publish','true');

            //Course
            if($course_ids != ''){                
                $data= $data->whereRaw('json_contains(course_id, \'["'.$course_ids.'"]\')')->orderBy('position','desc');
            }
            else 
            {
                $data= $data->where(function ($q) use($course_id) {
                    foreach($course_id as $key => $ord)
                    {                        
                        if($key == 0)
                        {
                            $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                        }
                        else 
                        {
                            $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                        }
                    }
                })->orderBy('position','desc');
            }            
            
            //filter
            if($filter != ''){
                $data= $data->where('name','like','%'.$filter.'%')
                ->orWhere('publish','like','%'.$filter.'%');
            }

            //subject
            if($subject_id != ''){                
                $data= $data->whereRaw('json_contains(subject_id, \'["'.$subject_id.'"]\')');
            }

            $data=$data->paginate(50);
                     
        }
        
        if($this->check_count($data) > 0)
        {
            foreach($data as $d)
            {
                $d->course_name='';                
                $course=DB::table('new_courses')->select('name')->whereIn('id',json_decode($d->course_id) )->get();
                if($this->check_count($course) > 0)
                {
                    $cou_name=[];
                    foreach($course as $cou)
                    {
                        $cou_name[]=$cou->name;
                    }
                     
                    $d->course_name=implode(",",$cou_name); 
                }

                $d->subject_name='';
                $subject=DB::table('subject')->select('name')->whereIn('id',json_decode($d->subject_id) )->get();
                if($this->check_count($subject) > 0)
                {
                    $sub_name=[];
                    foreach($subject as $s)
                    {
                        $sub_name[]=$s->name;
                    }

                    $d->subject_name=implode(",",$sub_name); 
                } 
                
                if(Auth::user()->role == 'school' )
                {
                    $video_access=DB::table('video_school_access')->where('school_id',Auth::user()->id )->where('video_id',$d->id)->first();
                    if($video_access)
                    {
                        $d->publish='true';                        
                    }
                    else {
                        $d->publish='false';
                    }
                }
                
            }
        }

       

        if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin' )
        {
            $new_Courses=DB::table('new_courses')->where('publish','true')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();          
        }
        else
        {
            $course_id=json_decode(Auth::user()->course_id,true);
            $new_Courses=DB::table('new_courses')->whereIn('id',$course_id)->orderBy('name','ASC')->get();           
        }

        $subject='';
        if($course_ids != '')
        {
            $course=DB::table('new_courses')->where('id',$course_ids)->first();
            $subject=DB::table('subject')->whereIn('id',json_decode($course->subject))->get();
        }

        return view::make('view_video', compact('title','data','new_Courses','subject','filter','course_ids','subject_id')); 
    }

    public function edit_video(Request $r)
    {

        if(Auth::user()->role == 'school')
        {
          return back()->with('error','Sorry,You do not access to access this page.');
        }

        
        $id=decrypt($r->id);
        $title='Edit Video';
        $data=DB::table('video')->where('id',$id)->first();
        if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin')
        {
          $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();
          $subject=DB::table('subject')->where('school_id',1)->whereIn('id', json_decode($data->subject_id))->get(); 
        }
        else
        {
          $new_Courses=DB::table('new_courses')->where('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
          $subject=DB::table('subject')->where('school_id',Auth::user()->id)->whereIn('id', json_decode($data->subject_id))->get(); 
        }

        $course_video=json_decode($data->course_id) ;
        $subject_video=json_decode($data->subject_id);


        $topic=topic::where(function ($q) use($course_video) {
            foreach($course_video as $key => $ord)
            {
                if($key == 0)
                {
                    $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
                else 
                {
                    $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
            }
        })
        ->where(function ($w) use($subject_video) {
            foreach($subject_video as $keys => $ords)
            {
                if($keys == 0)
                {
                    $w->whereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                }
                else 
                {
                    $w->orWhereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                }
            }
        })->get();

        return view::make('edit_video', compact('title','new_Courses','data','subject','topic'));
    }

    public function update_video(Request $r)
    {
        // $thumbnail="https://img.youtube.com/vi/".$r->video."/0.jpg";

        $insert = DB::table('video')->where('id',$r->id)->update(['course_id'=>json_encode($r->course_id),'subject_id'=>json_encode($r->subject_id),
        'topic'=>json_encode($r->topic_id),'name'=>$r->name,'position'=>(int)$r->position,'access'=>$r->access,'timestamp'=>date("YmdHis")]);
        if($insert)
        {
            return redirect()->route('view_video')->with('success','Video has been updated successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }

    }

    public function delete_video(Request $r)
    {
        $ids=explode(",",$r->del_id);       
        $check=DB::table('video')->whereIn('id',$ids)->first();
        if($check)
        {
            $delete=DB::table('video')->whereIn('id',$ids)->delete();
            return redirect()->route('view_video')->with('success','Video has been deleted successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }

    public function publish_video(Request $r)
    {

        $ids=explode(",",$r->del);

        $update = DB::table('video')->whereIn('id',$ids)->update(['publish' => $r->type]);
        if($update)
        {
            return back()->with('success','Video has been Updated successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }

    public function school_publish_video(Request $r)
    {
        $ids=explode(",",$r->del);
        $insert='';
        $check=DB::table('video_school_access')->where('school_id',$r->school_id)->whereIn('video_id',$ids)->get();
        if($this->check_count($check) > 0)
        {                        
            if($r->type == 'false')
            {
                foreach ($check as  $c) 
                {
                    $insert =DB::table('video_school_access')->where('school_id',$r->school_id)->where('id',$c->id)->delete();
                    $publish= 'Unpublish'; 
                }
                
            }   
                       
        }
        else 
        {  
            $created_at=date('y-m-d h:i:s');
            foreach ($ids as  $value) 
            {
                if($r->type == 'true')
                {
                    $insert = DB::table('video_school_access')->insert(['school_id' =>$r->school_id,'ref_code'=>$r->ref_code,
                    'video_id'=>$value,'created_at'=>$created_at,'updated_at'=>$created_at]);  
                }
            }             

            $publish= 'Publish';
        }
        if($insert)
        {
            return back()->with('success','Video has been '.$publish.'.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }

    }

    //Sort video
    public function sort_video(Request $r)
    {
        $title='Video-sorting';
        $course_ids = $r->course;
        $subject_id = $r->subject;

        $data=DB::table('video')->whereNotNull('thumbnail')
                ->whereRaw('json_contains(course_id, \'["'.$course_ids.'"]\')');
                if($subject_id !='')
                {
                    $data=$data->whereRaw('json_contains(subject_id, \'["'.$subject_id.'"]\')');
                }
                

                $data=$data->paginate(50);       
        
        
        if($this->check_count($data) > 0)
        {
            foreach($data as $d)
            {
                $d->course_name='';                
                $course=DB::table('new_courses')->select('name')->whereIn('id',json_decode($d->course_id) )->get();
                if($this->check_count($course) > 0)
                {
                    $cou_name=[];
                    foreach($course as $cou)
                    {
                        $cou_name[]=$cou->name;
                    }
                     
                    $d->course_name=implode(",",$cou_name); 
                }

                $d->subject_name='';
                $subject=DB::table('subject')->select('name')->whereIn('id',json_decode($d->subject_id) )->get();
                if($this->check_count($subject) > 0)
                {
                    $sub_name=[];
                    foreach($subject as $s)
                    {
                        $sub_name[]=$s->name;
                    }

                    $d->subject_name=implode(",",$sub_name); 
                } 
                
                if(Auth::user()->role == 'school' )
                {
                    $video_access=DB::table('video_school_access')->where('school_id',Auth::user()->id )->where('video_id',$d->id)->first();
                    if($video_access)
                    {
                        $d->publish='true';                        
                    }
                    else {
                        $d->publish='false';
                    }
                }
                
            }
        }


        if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin' )
        {
            $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();
            $subject=DB::table('subject')->where('school_id',1)->get();
        }
        else
        {
            $course_id=json_decode(Auth::user()->course_id,true);
            $new_Courses=DB::table('new_courses')->whereIn('id',$course_id)->orderBy('name','ASC')->get();

            $sub=array(); 
            foreach ($new_Courses as $values) 
            {
                $sub=array_unique(array_merge(json_decode($values->subject),$sub));
            }

            $subject=DB::table('subject')->whereIn('id',$sub)->get();
        } 


        return view::make('sort_video', compact('title','data','course_ids','subject_id','new_Courses','subject'));
    }
    public function sort_videos(Request $r)
    {        
        if($this->check_count($r->position) > 0)
        {
            foreach($r->position as $key=>$value)
            {
                if($value != '')
                {
                    $video_id=$r->ids[$key];
                    $poisition=(int)$value;

                    $update = DB::table('video')->where('id',$video_id)->update(['position'=>$poisition]);

                }

            }
            return back()->with('success','Video position has been updated successfully.');
        }else {
            return back()->with('error','Please enter atleast one position.');
        }
     
    }
    


    //____________________ABOUT__________________

    public function about(Request $r)
    {
        $title='About';
        $data=DB::table('about')->where('school_id',Auth::user()->id)->first();
        return view::make('about', compact('title','data'));
    }

    public function insert_about(Request $r)
    {
        if($r->id != '')
        {
            $update = DB::table('about')->where('id',$r->id)->update(['school_id'=>Auth::user()->id,'about' => $r->about
            ,'contact'=>$r->contact,'timestamp'=>date("YmdHis")]);
        }
        else
        {
            $update = DB::table('about')->insert(['school_id' => Auth::user()->id,'about'=>$r->about,
            'contact'=>$r->contact,'timestamp'=>date("YmdHis")]);
        }

        if($update)
        {
            return redirect()->route('about')->with('success','About us has been addedd successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }

    //____________________________ ASSIGN COURSE______________________

    public function assign_course(Request $r)
    {
        $title='Assign Course';
        return view::make('assign_course', compact('title'));
    }

    public function insert_assign(Request $r)
    {
        try{
        $fileD = fopen($r->file('file'),"r");
        $column=fgetcsv($fileD);
        while(!feof($fileD))
        {
            $rowData[]=fgetcsv($fileD);
        }

        foreach ($rowData as $key => $value) 
        {
          if(isset($value[0]))
          {
              
                $check=DB::table('users')->where('mobile',$value[0])->first();
                if($check)
                {                
                    $course_check=DB::table('new_courses')->where('id',$value[1])->first();
                    if($course_check)
                    {
                        $assign=DB::table('course_access')->where('user_id',$check->id)->where('course_id',$value[1])->first();
                        if(!$assign)
                        {
                            $insert = DB::table('course_access')->insert(['mobile'=> $check->mobile,'school_id'=>Auth::user()->id,'user_id' => $check->id,'course_id'=>$value[1],'course_name'=>$course_check->name,'created_at'=>date("YmdHis"),'updated_at'=>date("YmdHis"),'ref_code'=>Auth::user()->ref_code]);                       
                        }
                    }
                }
            }
        }

        return redirect()->route('assign_course_view')->with('success','Assign Course has been added successfully.');
    }
    catch (Throwable $e) {
        report($e);
 
        return false;
    }

    }


    public function assign_ebook_view(Request $r)
    {       
        $title='View Video';
        $filter=$r->filter;
        $type=$r->type;

        if($r->type!=''){
            $data=DB::table('course_access')
            ->join('notes', 'notes.id', '=', 'course_access.course_id')
            ->join('users', 'users.id', '=', 'course_access.user_id')
            // ->where('course_access.type',$r->type)
            ->select('users.*','notes.name as course_name','course_access.id as assess_id','course_access.ref_code');
        }
        else{
            $data=DB::table('course_access')
            ->join('notes', 'notes.id', '=', 'course_access.course_id')
            ->join('users', 'users.id', '=', 'course_access.user_id')
            ->whereIn('course_access.type',['ebook','notes'])
            ->select('users.*','notes.name as course_name','course_access.id as assess_id','course_access.ref_code');
        }
        
        if(Auth::user()->role == 'school')
        {
            $data=$data->where('course_access.ref_code',Auth::user()->ref_code);
        }

        $data=$data->OrderBy('course_access.id','DESC');

        if($filter != '')
        {
            $data=$data->orwhere(function ($query) use ($filter) {
                $query->orWhere('course_access.mobile','like','%'.$filter)
                ->orWhere('users.name','like','%'.$filter)
                ->orWhere('course_access.course_name','like','%'.$filter)
                ->orWhere('course_access.ref_code','like','%'.$filter);
            });
        }


        $data=$data->paginate(100);

        return view::make('assign_ebook_view', compact('title','data','filter','type')); 
    }

    public function assign_course_view(Request $r)
    {
        
        $title='View Video';
        $filter=$r->filter;
        $type=$r->type;

        // if($type=='course'){
            $data=DB::table('course_access')
            
            ->join('users', 'users.id', '=', 'course_access.user_id')
            ->select('users.*','course_access.course_id as course_id','course_access.id as assess_id','course_access.ref_code','course_access.type');
            
        // }
        // else{
        //     $data=DB::table('course_access')
        //     ->join('notes', 'notes.id', '=', 'course_access.course_id')
        //     ->join('users', 'users.id', '=', 'course_access.user_id')
        //     ->select('users.*','notes.name as course_name','course_access.id as assess_id','course_access.ref_code');
        // }

       
        if(Auth::user()->role == 'school')
        {
            $data=$data->where('course_access.ref_code',Auth::user()->ref_code);
        }

        $data=$data->whereIn('course_access.type',['course','test_series'])->OrderBy('course_access.id','DESC');


        if($filter != '')
        {
            $data=$data->where(function ($query) use ($filter) {
                $query->orWhere('course_access.mobile','like','%'.$filter)
                ->orWhere('users.name','like','%'.$filter)
                ->orWhere('course_access.id','like','%'.$filter)
                ->orWhere('course_access.ref_code','like','%'.$filter);
            });
        }


        $data=$data->paginate(100);
              
        foreach($data as $key=>$value){

            if($value->type == 'course'){
                $courseName=DB::table('new_courses')->where('id',$value->course_id)->first();
                if($this->check_count($courseName)>0){
                    $value->course_name=$courseName->name;
                }else{
                    $value->course_name='';
                }
            }else{
                $seriesName=DB::table('test_series')->where('id',$value->course_id)->first();
                if($this->check_count($seriesName)>0){
                    $value->course_name=$seriesName->name;
                }else{
                    $value->course_name='';
                }
            }
        }

        return view::make('assign_course_view', compact('title','data','filter','type')); 
    }

    public function delete_assign(Request $r)
    {
        
        $check=DB::table('course_access')->where('id','=',$r->del_id)->delete();
      
        
        if($check)
        {
            return redirect()->route('assign_ebook_view')->with('success','Assign Course has been deleted successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }



    //Send email
    public function send_email(Request $r)
    {
        $check=DB::table('admins')->where('email',$r->email)->first();
        if($check)
        {
            //Mail service.
            Mail::to($r->email)->send(new ForgotPassword($check->name,$check->id));
            return redirect()->route('login')->with('success','Verification link has been sent to your email account.');
        }
        else 
        {
            return back()->with('error','We can not find a user with that e-mail address.');
        }
       
    }
    
    public function change_password(Request $r)
    {
        $id=decrypt($r->id);
        $check=DB::table('admins')->where('id',$id)->first();
        if($check)
        {
            return view::make('auth.passwords.reset', compact('check'));  
        }
        else 
        {
            echo "We can not find a user with that e-mail address.";
        }
    }

    public function reset_password(Request $r)
    {
        $check=DB::table('admins')->where('id',$r->id)->first();
        if($check)
        {
            if($r->password == $r->password_confirmation)
            {
                $update = DB::table('admins')->where('id',$r->id)->update(['password' =>bcrypt($r->password)]);
                return redirect()->route('login')->with('success','Your password has been reset successfully.');
            }
            else 
            {
                return back()->with('error','The password confirmation does not match.');            
            }
        }
        else 
        {
            return back()->with('error','We can not find a user with that e-mail address.');            
        }
    }


    // VIDEO COmment    
    public function get_comment(Request $r)
    {
        $title='Video Comment';
        $id=decrypt($r->id);         
        
        $data= DB::table('video_comment')
            ->join('video', 'video_comment.video_id', '=', 'video.id')
            ->join('users', 'video_comment.user_id', '=', 'users.id')
            ->select('video_comment.*', 'video.name as video_name', 'users.name as username','users.mobile as usermobile')
            ->where('video_comment.video_id',$id)
            ->get();
        return view::make('video_comment', compact('title','data')); 
    }

    public function delete_Comment(Request $r)
    {
        $check=DB::table('video_comment')->where('id',$r->del_id)->delete();
        if($check)
        {
            return back()->with('success','Video Comment has been deleted successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }


    // Topic
        public function add_topic(Request $r)
        {
            $title='Topic';

            if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin')
            {
                $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();
            }
            else
            {
                $new_Courses=DB::table('new_courses')->where('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
            }

            return view::make('topic', compact('title','new_Courses'));
        }

    // Insert topic
        public function insert_topic(Request $r)
        {            
            if(count($r->sub) > 0)
            {
                $lenght= count($r->sub);
                $school_id=Auth::User()->id;
                for ($i=0; $i < $lenght; $i++) 
                {                     
                    $check=topic::where('school_id',$school_id)->where('name',$r->sub[$i])->first();
                    if(!$check)
                    {
                        $insert=new topic;
                        $insert->school_id=$school_id;
                        $insert->name=$r->sub[$i];
                        $insert->course_id=json_encode($r->course_id);
                        $insert->subject_id=json_encode($r->subject_id);
                        $insert->position=$r->position[$i];
                        $insert->save();                
                    }               
                }

                return redirect()->route('view_topic')->with('success','Subject has been added successfully.');
            } 
            else
            {
                return back()->with('error','Sorry,Subject could not be empty.');
            }
        }    

    // get topic
        public function view_topic(Request $r)
        {
            $title='View Topic';
            $data=topic::orderby('position','ASC')->paginate(50);
            if($this->check_count($data) > 0)
            {
                foreach ($data as $key => $d) 
                {
                    $d->course_name=''; 
                    $course=DB::table('new_courses')->select('name')->whereIn('id',json_decode($d->course_id) )->get();
                    if($this->check_count($course) > 0)
                    {
                        $cou_name=[];
                        foreach($course as $cou)
                        {
                            $cou_name[]=$cou->name;
                        }
                         
                        $d->course_name=implode(",",$cou_name); 
                    }

                    $d->subject_name='';
                    $subject=DB::table('subject')->select('name')->whereIn('id',json_decode($d->subject_id) )->get();
                    if($this->check_count($subject) > 0)
                    {
                        $sub_name=[];
                        foreach($subject as $s)
                        {
                            $sub_name[]=$s->name;
                        }
    
                        $d->subject_name=implode(",",$sub_name); 
                    } 
                }
            }

            return view::make('view_topic', compact('title','data'));  
        }

    //Edit topic
        public function edit_topic(Request $r)
        {
            $title='Edit Topic';
            $id=decrypt($r->id);
            $data=topic::find($id);

            if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin')
            {
              $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();
              $subject=DB::table('subject')->where('school_id',1)->whereIn('id', json_decode($data->subject_id))->get(); 
            }
            else
            {
              $new_Courses=DB::table('new_courses')->where('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
              $subject=DB::table('subject')->where('school_id',Auth::user()->id)->whereIn('id', json_decode($data->subject_id))->get(); 
            }

            return view::make('edit_topic', compact('title','data','new_Courses','subject'));  

        }    

    // Update topic
        public function update_topic(Request $r)
        {
            $update=topic::find($r->id);
            if($update)
            {
                $update->course_id=json_encode($r->course_id);
                $update->subject_id=json_encode($r->subject_id);
                $update->name=$r->sub;
                $update->position=$r->position;
                $save=$update->save();
                if($save)
                {
                    return redirect()->route('view_topic')->with('success','Topic has been updated successfully.');
                }
                else 
                {
                    return back()->with('error','Something went wrong, please try again!');
                }
            }
            else 
            {
                return back()->with('error','Sorry,Record not found.');
            }
        }


    // Delete topic
        public function delete_topic(Request $r)
        {
            $delete=topic::where('id',$r->del_id)->delete();
            if($delete)
            {
                return back()->with('success','Topic has been deleted successfully.');
            }
            else 
            {
                return back()->with('error','Sorry,Topic could not be empty.');
            }
        }


    ////Assign course/note/ebook manual
    
        public function assign_manully_course(Request $r)
        {
            $title="Assign Manully Course";
            $course_id=json_decode(Auth::user()->course_id);
            //Course
            $couse = DB::table('new_courses');
                if(Auth::user()->role == 'admin'){
                    $couse =$couse->whereNull('new_courses.school_id')
                    ->orWhere('new_courses.school_id',Auth::user()->id);
                }else{
                    $couse =$couse->whereIn('new_courses.id',$course_id);
                }  

            $couse =$couse->orderBy('new_courses.name','ASC')->get();

            //Notes
                $notes=DB::table('notes')->where('type','notes')->OrderBy('name','ASC')->get();
                
            //Ebook 
                $ebook=DB::table('notes')->where('type','ebook')->OrderBy('name','ASC')->get();

            //Test-series
                $test=DB::table('test_series')->where('delete_status',1)->where('status','published')->get(); 

            return view::make('assign_manully_course', compact('title','couse','notes','ebook','test'));             

        }

        public function assign_manully(Request $r)
        {
           $course_id='';
            $table='';
           if($r->type == 'course'){
            $course_id=$r->course_id;
            $table='new_courses';
           }else if($r->type == 'notes'){
            $course_id=$r->note_id;
            $table='notes';
           }else if($r->type == 'test_series'){
            $course_id=$r->test_id;
            $table='test_series';
           }
           else{
            $course_id=$r->ebook_id;
            $table='notes';
           }

           $check=DB::table('users')->where('mobile',$r->mobile)->first();
           if($this->check_count($check) > 0){

                $course_check=DB::table($table)->where('id',$course_id)->first();  
                if($this->check_count($course_check) > 0){
                    $check_assign=DB::table('course_access')->where('mobile', $r->mobile)->where('user_id',$check->id)->where('course_id',$course_id)->where('type',$r->type)->first();
                    if($check_assign)
                    {
                        return back()->with('error','Course already assign this user');  
                    }
                    $insert = DB::table('course_access')->insert(['mobile'=> $r->mobile,'school_id'=>Auth::user()->id,'user_id' => $check->id,'course_id'=>$course_id,'course_name'=>$course_check->name,'created_at'=>date("YmdHis"),'updated_at'=>date("YmdHis"),'ref_code'=>Auth::user()->ref_code,'type'=>$r->type]);     
                    if($insert) {
                        return back()->with('success','Course has been assign successfully.');

                    }else {
                        return back()->with('error','Something went wrong,Please try again later.');  
                    }

                }else{
                    return back()->with('error','Sorry,Course invalid.');     
                }
           }
           else {
             return back()->with('error','Sorry,Mobile number invalid.');            
           }


        }


        


}
