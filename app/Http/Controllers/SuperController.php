<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Role;
use App\Permission;
use App\Institute;
use App\InstituteSubadmin;
use Hash;
use DB;
use View;
use Mail;
use Auth;
use App\Mail\AddEmail;
use App\video_duration;

class SuperController extends Controller
{

    public function get_city(Request $request)
    {
        $state = $request->state;
        $city = DB::table('cities')->where('city_state',$state)->get();
        // print_r($city);
        return response($city);
    }

    public function user_search(Request $request)
    {
        $cityselected = DB::table('cities')->select('city_name')->where('city_id',$request->city)->get();
        $users=DB::table('users')->where('city',$cityselected)->orderBy('id','desc')->paginate('100');
        return view('ajax.search_users',compact('users','state')); 
    }
    
    public function view_users(Request $request)
    {
        $role='admin';
        $ref='';
        $admin=DB::table('admins')->where('id',Auth::User()->id)->first();
        if($admin->role == 'school')
        {
            $role='school';
            $ref=$admin->ref_code;
        }

        $city = Input::get('city');
        $filter = Input::get('filter');
        if($city!='')
        {          
            $users=DB::table('users')->where('city',$city)->orderBy('id','desc')->paginate('100');
        }
        else if($filter!='')
        {
            if($role == 'admin')
            {
                $users=DB::table('users')->where('verified','yes')->where('check',0)
                ->where(function($query) use ($filter) {
                    $query->where('name', 'like','%'.$filter.'%')
                          ->orWhere('id',$filter)
                          ->orWhere('email',$filter)
                          ->orWhere('mobile',$filter);
                })
                ->orderBy('id','desc')->paginate('100');
                
            }
            else
            {
                $users=DB::table('users') ->where('referralcode',$ref)->where('verified','yes')->where('check',0)
                ->where(function($query) use ($filter) {
                    $query->where('name', 'like','%'.$filter.'%')
                          ->orWhere('email',$filter)
                          ->orWhere('mobile',$filter);
                })
                ->orderBy('id','desc')->paginate('100');
            }
                
        }
        else
        {
            if($role == 'admin')
            {
                $users=DB::table('users')->where('verified','yes')->orderBy('id','desc')->paginate('100');
            }
            else
            {
                $users=DB::table('users')->where('verified','yes')->where('referralcode',$ref)->orderBy('id','desc')->paginate('100');
            }            
        }

        foreach ($users as $key => $value) 
        {
            $value->duration=0;
            $check=video_duration::where('user_id',$value->id)->sum('duration');
            if($check > 0)
            {
                $value->duration=$check; 
            }
        }
     
        $state = DB::table('cities')->select('city_state')->distinct()->get();
        $users->appends(request()->input())->links();
        return view('view_users',compact('users','state'));
    }


    //Edit User
    public function edit_user(Request $r)
    {
        $id=decrypt($r->id);
        $data= DB::table('users')->where('id',$id)->first();
        return view::make('edit_user', compact('data'));
    }

    //update user
    public function update_user(Request $r)
    {
        if($r->id != '')
        {
            if($r->ref_code != '')
            {
                $check=DB::table('admins')->where('role','school')->where('ref_code',$r->ref_code)->first();
                if($check)
                {
                    $update = DB::table('users')->where('id',$r->id)->update(['referralcode'=>$r->ref_code]);
                    if($update || $update == 0)
                        {
                            return redirect()->route('view_users')->with('success','User ref code has been Updated successfully.');
                        }
                        else
                        {
                            return back()->with('error','Something went wrong, please try again!');
                        }
                }
                else
                {
                    return back()->with('error','Opps! Invalid  Referral code.');
                }

            }
            else 
            {
                $update = DB::table('users')->where('id',$r->id)->update(['referralcode'=>'']);
                if($update || $update == 0)
                    {
                        return redirect()->route('view_users')->with('success','User ref code has been Updated successfully.');
                    }
                    else
                    {
                        return back()->with('error','Something went wrong, please try again!');
                    }
            }            
        }
        else 
        {
            return back()->with('error','Sorry,UserID and Ref code can not be empty');
        }
    }

    public function add_admin(Request $request)
    {
       
    	$all=$request->all();
    	$characters = '0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
						
            $charactersLength = strlen($characters);
            $length='8';
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
    		}
    	try{
            $check=User::where('email',$request->email)->count();
            if($check>0)
            {
                return redirect()->back()->with('error','Email already registered.');
            }
			
			if($request->role == 'admin')
			{
    		$groups=implode(',',$request->group);
            $admin=new User($all);
            $admin->groups=$groups;
    		$admin->password=Hash::make($randomString);
			
    		$admin->save();
			
			}
			
			else
			{
			
            $admin=new User($all);

            if($request->group!='')
            {
                $admin->groups=implode(',',$request->group);
            }
            else {
                $admin->groups='';
            }
            
			$admin->role=$request->role;
			$admin->dob=$request->dob;
			$admin->qualification=$request->qualification;
			$admin->city=$request->city;
			$admin->experience=$request->experience;
			$admin->password=Hash::make($randomString);
			
    		$admin->save();	
			}
    		if($admin)
    		{
    			$user=User::find($admin->id);
				$owner = new Role();
				$owner->name= $request['email'];
				$owner->display_name = 'Admin'; // optional
				$owner->description  = 'User is the owner of a given project'; // optional
				$owner->save();
				$value=Role::where('name',$request['email'])->get()->toArray();
				$owner_id=$value[0]['id'];
				
				$user->attachRole($owner_id);
			
				foreach($request->link as $key=>$val){
					$owner->attachPermission($val);
					
					
				}
			
				// parameter can be an Role object, array, or id
					$add='admin';
                    $password=$randomString;
                    $email=$request->email;

                    Mail::to($email)->send(new AddEmail($add,$password,$email));

                   return redirect('/view_admin')->with('success','Admin Added Successfully!');
    		}
    	}	
    	catch(Exception $e)
    	{
    		//return redirect('add_admin')->with('error','Something went Wrong Please try again!');;
    	}
    	


    }
	public function addAdmin()
    {
        $query=DB::table('discussions')->get();
		$permissions=DB::table('permissions')->get();
        return view::make('add_admin',compact('query','permissions'));
    }
    public function edit_admin($id)
    {
        $query=DB::table('discussions')->get();
        $permissions=DB::table('permissions')->get();
        $admins = DB::table('admins')->where('id',$id)->first();
        $role=DB::table('role_user')->where('user_id',$admins->id)->first();
        $role_id=$role->role_id;
        $user_permissions=DB::table('permission_role')->where('role_id',$role->role_id)->get();
        foreach ($user_permissions as $permission) {
            $selected_permissions[]=$permission->permission_id;
        }
        // dd($permissions);
        return view::make('edit_admin',compact('query','permissions','selected_permissions','admins','role_id'));
    }

    public function update_admin(Request $request)
    {
        $all=$request->all();
       
        try{
			if($request->role == 'admin')
			{
			
            $groups=implode(',',$request->group);
            $admin=User::find($request->id);
            $admin->groups=$groups;
            $admin->name=$request->name;
			$admin->role=$request->role;
			$admin->dob='';
			$admin->qualification='';
			$admin->city='';
			$admin->experience='';
            $admin->save();
			}
			
			else
			{
			
            $admin=User::find($request->id);
            $admin->groups=implode(',',$request->group);
			$admin->role=$request->role;
			$admin->dob=$request->dob;
			$admin->qualification=$request->qualification;
			$admin->city=$request->city;
			$admin->experience=$request->experience;
			
    		$admin->save();	
			}
			
			
            if($admin)
            {
                $user=User::find($request->id);
                $owner =Role::find($request->role_id);
                $delete=DB::table('permission_role')->where('role_id',$request->role_id)->delete();
                           
                foreach($request->link as $key=>$val){
                    $owner->attachPermission($val);
                    
                    
                }
            
                // parameter can be an Role object, array, or id

                   return redirect('/view_admin')->with('success','Admin Updated Successfully!');
            }
        }   
        catch(Exception $e)
        {
            //return redirect('add_admin')->with('error','Something went Wrong Please try again!');;
        }
        


    }



    public function view_admin()
    {
        //$admins=Role::where('name', 'admin')->first()->users()->get();
		$admins = DB::table('admins')->orderBy('id','desc')->get();
        return view('view_admin',compact('admins'));
    }

    public function delete_admin(Request $request)
    {
       try{
            $data=User::find($request->del_id);
            $email=$data->email;
            DB::table('roles')->where('name',$email)->delete();
           User::destroy($request->del_id);
           return redirect('view_admin')->with('success','Admin Deleted Successfully.'); 
        }
        catch(Exception $e)
        {
            return redirect('view_admin')->with('error','Something went worng. Please try again.');
        }
    }


    public function add_institutes(Request $request)
    {
    	$all=$request->all();

    	$characters = '0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
						
            $charactersLength = strlen($characters);
            $length='8';
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
    		}
    		session(['password' => $randomString]);
    	try{

    		$admin=new User();
    		$admin->name=$request->name;
    		$admin->email=$request->email;
    		$admin->password=Hash::make($randomString);
    		$admin->save();

    		if($admin)
    		{
    			$user=User::find($admin->id);
    			$user->attachRole('3'); // parameter can be an Role object, array, or id

    			$institute=new Institute($all);
    			$institute->admin_id=$admin->id;
    		$institute->save();

	    		if($institute)
	    		{
	    			$add='institute';
	    			$password=$randomString;
                    $email=$request->email;

	    			Mail::to($email)->send(new AddEmail($add,$password,$email));

	            	return redirect('/view_institute')->with('success','Institute Added Successfully!');
	    		}
    		}


    		
    	}	
    	catch(Exception $e)
    	{
    		return redirect('add_institutes')->with('error','Something went wrong.Please Try again!');
    	}
    	

    }


    public function view_institute()
    {
        $institutes=Role::where('name', 'institute_admin')->first()->users()->join('institutes', 'admins.id', '=', 'institutes.admin_id')->select('admins.*', 'institutes.phone', 'institutes.address','institutes.id as ins_id')->get();
        return view('view_institute',compact('institutes'));
    }

    public function view_employees(Request $request)
    {
        $subadmins=InstituteSubadmin::where('institute_id',$request->id)->orderBy('id','desc')->get();
        return view('view_institute_subadmin',compact('subadmins'));
    }


    //block user
    public function block_user(Request $r)
    {
        $update = DB::table('users')->where('id',$r->id)->update(['block_status'=>$r->type ]);
        if($update)
        {
            //Send push notification
            $content='Dear User you account has been '.$r->type.' by admin.' ;
            $data=array("type"=>9,"name"=>'logout',"id"=>$r->id);
            $push= $this->push_notification($content,$data,'player_id',[$r->player_id]);
            return back()->with('success','User has been '.$r->type);
        }
        else 
        {
            return back()->with('error','Something went wrong, please try again!');
        }
        
    }

    //Block Comment
    public function block_comment(Request $r)
    {
        $update = DB::table('users')->where('id',$r->u_id)->update(['block_comment'=>$r->u_type ]);
        if($update)
        {
            //Send push notification
            $content='sorry your comment section has been temporarily '.$r->u_type.' you can not comment now.Please contact with administrator' ;
            $data=array("type"=>7,"name"=>'common',"id"=>$r->u_id);
            $push= $this->push_notification($content,$data,'player_id',[$r->u_player_id]);
            return back()->with('success','User Comment has been '.$r->u_type);
        }
        else 
        {
            return back()->with('error','Something went wrong, please try again!');
        }
        
    }
   

}
