<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use DB;
use Carbon;
use View;
use URL;
use Redirect;
use App\SentNotification;
use App\topic;

use Auth;

class pdfController extends Controller
{
	
	public function add()
    {

      if(Auth::user()->role == 'school')
      {
        return back()->with('error','Sorry,You do not access to access this page.');
      }
      $groups=[];
        if(Auth::user()->groups=="all")
        {
            $groups1=DB::table('discussions')->orderBy('id','desc')->get();
            foreach ($groups1 as $group){
              $groups[]=$group->id;
            }
        }
        else
        {
            $user_groups=Auth::user()->groups;
            if(strrchr($user_groups,","))
            {
              $groups=explode(",",$user_groups);
            }
            else
            {
              $groups[]=$user_groups;
            }
        }
        $data=[];
        if(count($groups)>0)
        {
          foreach($groups as $group)
          {
          $data[]=DB::table('discussions')->where('id',$group)->first();
          }
        }
        $course = DB::table('course')->get();
        if(Auth::user()->role == 'admin')
        {
          $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
        }
        else
        {
          $new_Courses=DB::table('new_courses')->where('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
        }

        $subject=DB::table('subject')->where('school_id',Auth::user()->id)->get();
        return view::make('pdf',compact('data','course','new_Courses','subject'));
    }

	public function store_data(Request $r)
	{
      $groupid=implode(",",$r->groupid);
      if($r->add_comment)
      {
          $add_comment=$r->add_comment;
      }
      else
      {
          $add_comment="on";
      }
      
      if($r->add_like)
      {
          $add_like=$r->add_like;
      }
      else
      {
          $add_like="on";
      }

        $pdfUrl='';
        $result['add_comment']=$add_comment;
        $result['add_like']=$add_like;
				$result['title']=$r['title'];
				$result['writer']=$r['writer'];
				$result['type']=$r['type'];
				 $file = $r->file('pdf');
				if($r['type']=="link")
				{
					$result['link']=$r['link'];
				}
				else 
				{					
					if($file)
					{					
            $rand=rand(1,1000000);
            $destinationPath = public_path()."/mechanicalinsider/pdf";
            $filename = $file->getClientOriginalName();
            $filename=$rand.$filename;
            $upload=$file->move($destinationPath, $filename);
            $url=URL::to('/');
            $result['link']=$url."/mechanicalinsider/pdf/".$filename;
            $pdfUrl=$url."/mechanicalinsider/pdf/".$filename;
	        }
          else
          {
	          $filename=$r->old_image;	                	
						$result['link']="";
				  }					
				}
				
      $result_json=json_encode($result,JSON_UNESCAPED_UNICODE);

      $uid=Auth::User()->email;
      
      $user=DB::table('common_table')->insertGetId(
      ['field_type'=>'4','groupid' => $groupid,'jsondata'=>$result_json,'uid'=>$uid,'school'=>json_encode($r->sub),'access'=>$r->access,'topic'=>json_encode($r->topic_id)]);

		 if($user)
		 {
        $p_url=$url."/mechanicalinsider/pdf/".$filename;
        $content='PDF-'.$r['title'];
        $data=array("type"=>1,"name"=>'pdf',"id"=>$user,'p_url'=>$p_url);
        $push= $this->push_notification($content,$data,'all',null);
		 	  return redirect('view_pdf2')->with('success','Data Added Successfully.');
		 }
		 else 
		 {
			 return redirect('pdf')->with('error','Some Error occured try Again later.');
		 }
		
	}
	
	public function edit_data(Request $r)
	{
		 		if($r->add_comment)
        {
            $add_comment=$r->add_comment;
        }
        else
        {
            $add_comment="off";
        }
        if($r->add_like)
        {
            $add_like=$r->add_like;
        }
        else
        {
            $add_like="off";
        }

      if($r->type_institute=="group")
      {
        $groupid=implode(",",$r->groupid);
      }
      else
      {
        $groupid=implode(",",$r->groupid);
        $courseid = 0;
        $collegeid = 0;
        $stream = 0;
        $year =0;
      }

        $result['add_comment']=$add_comment;
        $result['add_like']=$add_like;
				$result['title']=$r['title'];
				$result['writer']=$r['writer'];
				$id=$r['id'];
				$link_existing=$r['link_existing'];
				$result['type']=$r['type'];
         $file = $r->file('pdf');
         
				if($r['type']=="link")
				{
					
					$result['link']=$r['link'];
				}
				else 
				{
					
					if($file)
					{
						
	                    $rand=rand(1,1000000);
	                    $destinationPath = public_path()."/mechanicalinsider/pdf";
	                    $filename = $file->getClientOriginalName();
	                    $filename=$rand.$filename;
	                    $upload=$file->move($destinationPath, $filename);
	                    $url=URL::to('/');
						$result['link']=$url."/mechanicalinsider/pdf/".$filename;
	                }
	                else
	                {
	                
	                	
						$result['link']=$link_existing;
				  	}
					
					
				}
				
      $result_json=json_encode($result);
      $school_id=json_encode($r->sub);
				
      $uid=Auth::User()->email;
     $user = DB::update("UPDATE `common_table` SET `jsondata`='$result_json',`groupid`='$groupid',`uid`='$uid' WHERE id='$id'");
     $college = DB::update("UPDATE `college_data` SET `courseid`='$courseid',`college_id`='$collegeid',`stream`='$stream',`year`='$year'  WHERE postid='$id'");
		 if($user>0)
		 {
		 	 return redirect('view_pdf')->with('success','Data Successfully Updated');
		 }
		 else 
		 {
			 return redirect('view_pdf')->with('error','Some Error occured try Again later.');
		 }
		
	}


   public function view_pdf()
   {

      if(Auth::user()->role == 'admin')
      {
        $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
      }
      else
      {
        $new_Courses=DB::table('new_courses')->where('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
      }

    	return view::make('view_pdf',compact('new_Courses'));
   }
   
   public function delete_pdf(Request $r)
   {
   		$id=$r['del_id'];
        $delete=DB::table('common_table')->where('id',$id)->delete();
        if($delete)
        {
            return redirect()->back()->with('success','comment deleted Successfully');
        }
		else 
		{
			return redirect()->back()->with('error','comment deleted Successfully');
		}
   }
    

    public function send_notification($id)
    {
        $post=DB::table('common_table')->where('id',$id)->first();
        $field_type=$post->field_type;
        $json=json_decode($post->jsondata,true);
        if(isset($json['icon']))
        {
        $icon=$json['icon'];
        }
        else
        {
          $icon="";
        }
        // $image=str_replace(" ","%20",$json['image']);
        $thumbnail="";
        // $thumbnail=str_replace(" ","%20",$json['thumbnail']);
        // $base_url="http://mechanicallive.com/mechanicalinsider/currentaffair/";
        $groupid=explode(",",$post->groupid);
        // $uid=[];
        // foreach ($groupid as $gid) {
        //     $data=DB::table('group_follow_rec')->where('group_name',$gid)->get();
        //    foreach ($data as $d) {
        //     if(!in_array($d->profile_uid, $uid))
        //         {
        //             $uid[]=$d->profile_uid;
        //         }
        //    }
        // }
        // $player_id=[];
        // foreach ($uid as $u) {
        //     $players=DB::table('users')->where('email',$u)->get();
        //     foreach ($players as $player) {
        //             $json1=json_decode($player->notification,true);
        //             if($json1['institute']==true)
        //             {
        //                 $player_list[]=$player;
        //             }

        //         }
            
        // }
        // foreach ($player_list as $player) {
                
        //             $player_id[]=$player->gcmid;
                
        //     }
        // $content = array(
        //    "en" => $json['title']
           
        //    );
          
        //   $fields = array(
        //    'app_id' => "306439b4-709b-468e-b687-d0f9e6917268",
        //    // 'included_segments' => $pp,
        //    'include_player_ids' => $player_id,
           
        //       'data' => array("key" => "value"),
        //    'contents' => $content,
        //    "large_icon" => "http://tyroad.co.uk/hellotopper_admin/public/img/hellotopper.png",
        //    "big_picture" => ""
          
        //   );
        //   // print_r($fields);
           
        //   $fields = json_encode($fields);
           
           
          
        //   $ch = curl_init();
          
            
        //   curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        //   curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MDQyY2E5MTEtMzdhZS00N2EwLWI4N2UtZTM3NGFlMjcxYWUw',
        //                                              'Content-Type: application/json; charset=utf-8'));
        //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //   curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //   curl_setopt($ch, CURLOPT_POST, TRUE);
        //   curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //   $response = curl_exec($ch);
        //   curl_close($ch);
        //   $result=json_decode($response,true);
//old code closes
        //new code

        $content = array(
           "en" => $json['title']
           
           );
          
           $fields = array(
           'app_id' => "306439b4-709b-468e-b687-d0f9e6917268",
           'included_segments' => $groupid,
           
              'data' => array("id" => $id,"field_type"=>$field_type),
           'contents' => $content,
            "large_icon" => "",
           "big_picture" => $icon
          
          );
          $fields = json_encode($fields);
          // print_r($fields);
           
           
          
          $ch = curl_init();
          
            
          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MDQyY2E5MTEtMzdhZS00N2EwLWI4N2UtZTM3NGFlMjcxYWUw',
                                                     'Content-Type: application/json; charset=utf-8'));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_HEADER, FALSE);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

          $response = curl_exec($ch);
          curl_close($ch);
          $result=json_decode($response,true);
          // dd($result);

 //new code close


          if(array_key_exists('id', $result))
          {
            $notification=SentNotification::where('postid',$id)->first();
            if(isset($notification->postid))
            {
                SentNotification::where('postid',$id)->update(['postid'=>$id]);
            }
            else
            {
                
                $notification = new SentNotification();

                $notification->postid = $id;

                $notification->save();
            }
            return redirect()->back()->with('success','Notification Sent Successfully');
          }
          else
          {
            return redirect()->back()->with('error','Something went wrong.Please try again.');
          }
    
    }

    public function search(Request $request)
   {
   	$field_type=4;
    	$query1=DB::table('common_table')->where('field_type',$field_type)->where('jsondata','like','%'.$request->val.'%')->get();
   //  	$data=DB::table('discussions')->get();
			// foreach ($query as $rec) {
			// $notification=DB::table('sent_notifications')->where('postid',$rec->id)->first();
   //          if($notification)
   //          {
   //              $rec->notification="sent";
   //          }
   //          else
   //          {
   //              $rec->notification="";
   //          }
   //       }

      if(Auth::user()->groups=="all")
        {
            $groups1=DB::table('discussions')->orderBy('id','desc')->get();
            foreach ($groups1 as $group){
              $groups[]=$group->id;
            }
        }
        else
        {
            $user_groups=Auth::user()->groups;
            if(strrchr($user_groups,","))
            {
              $groups=explode(",",$user_groups);
            }
            else
            {
              $groups[]=$user_groups;
            }
        }
        foreach($groups as $group)
        {
        $data[]=DB::table('discussions')->where('id',$group)->first();
        }
      $new_record=[];
    foreach ($query1 as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec->groupid;
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
      $new_record=$query1;
    }
    if(!empty($new_record))
    {
      foreach ($new_record as $key => $new_rec) {
      $notification=DB::table('sent_notifications')->where('postid',$new_rec->id)->first();
            if($notification)
            {
                $new_rec->notification="sent";
            }
            else
            {
                $new_rec->notification="";
            }

        $comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$new_rec->id)->get();
        foreach ($comments as $comment) {
            $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
            foreach ($reply as $key => $rep) {
              $comment->reply[]=$rep;
            }
          $new_rec->comments[]=$comment;
        }
      }
    } 
    $query=$new_record;
		
    	return view::make('pdf_search',compact('query','data'));
   }



  //view pdf 2
  public function view_pdf2(Request $r)
  {
    $title='View PDF';
    $filter = $r->filter;
    $course_ids = $r->course;
    $subject_id = $r->subject;


    $data=DB::table('common_table')->where('field_type',4)->orderBy('id','desc');
    
    if($r->course != ''){
      $data=$data->where('groupid','like','%'.$r->course.'%');
    }
    
    
    if($r->subject != ''){
        $data=$data->whereRaw('json_contains(school, \'["'.$r->subject.'"]\')');
    }

    if($filter != ''){
      $data=$data->whereJsonContains('jsondata->title',$filter);
    }
    
    $data=$data->paginate(50);

    if($this->check_count($data) > 0)
    {
      foreach ($data as $key => $value) 
      {
        //Course Name
        if($value->groupid != 'all'){
          $course_id=explode(",",$value->groupid);          
          $value->course_name=''; 
          $course=DB::table('new_courses')->select('name')->whereIn('id',$course_id )->get();
          if($this->check_count($course) > 0)
          {
              $cou_name=[];
              foreach($course as $cou)
              {
                  $cou_name[]=$cou->name;
              }
              
              $value->course_name=implode(",",$cou_name); 
          }
        }
        else{
          $value->course_name=$value->groupid; 
        }
         
        // End Course name
        
        //Subject name
          $value->subject_name='';
          if($value->school != ''){

            $subject=DB::table('subject')->select('name')->whereIn('id',json_decode($value->school) )->get();
            if($this->check_count($subject) > 0)
            {
                $sub_name=[];
                foreach($subject as $s)
                {
                    $sub_name[]=$s->name;
                }

                $value->subject_name=implode(",",$sub_name); 
            }
          }
            
        //End Subject name
        
        //Info
          $json=json_decode($value->jsondata);
          
          $value->pdf_name=$json->title;
          $value->pdf_link=$json->link;
        //end info

      }
    }


    if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin' )
      {
        $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();
        $subject=DB::table('subject')->where('school_id',1)->get();
      }
    else{
        $course_id=json_decode(Auth::user()->course_id,true);
        $new_Courses=DB::table('new_courses')->whereIn('id',$course_id)->orderBy('name','ASC')->get();

        $sub=array(); 
        foreach ($new_Courses as $values) 
        {
            $sub=array_unique(array_merge(json_decode($values->subject),$sub));
        }

        $subject=DB::table('subject')->whereIn('id',$sub)->get();
    }  
  
    return view::make('view_pdf2',compact('data','new_Courses','subject','filter','course_ids','subject_id'));

  }

  public function edit_pdf(request $r)
  {
    $id=decrypt($r->id);
    $title='Edit PDF';
    $data=DB::table('common_table')->where('id',$id)->first();
    
    if($data->groupid != 'all'){
      $data->course_id=explode(",",$data->groupid); 
    }else {
      $data->course_id=[];
    }

    $course_video=$data->course_id;
    $subject_video=json_decode($data->school);

    $json=json_decode($data->jsondata);

    $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();
    $subject=DB::table('subject')->where('school_id',Auth::user()->id)->get();

    $topic=topic::where(function ($q) use($course_video) {
        foreach($course_video as $key => $ord)
        {
            if($key == 0)
            {
                $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
            }
            else 
            {
                $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
            }
        }
      })
      ->where(function ($w) use($subject_video) {
          foreach($subject_video as $keys => $ords)
          {
              if($keys == 0)
              {
                  $w->whereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
              }
              else 
              {
                  $w->orWhereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
              }
          }
      })->get();

      // dd($topic);
    return view::make('edit_pdf', compact('title','new_Courses','data','subject','json','topic'));

  }

  public function update_pdf(Request $r )
  {
      $check=DB::table('common_table')->where('id',$r->id)->first();
      if($check){

        $groupid=implode(",",$r->groupid);

        $result['add_comment']='on';
        $result['add_like']='on';
        $result['title']=$r->title;
        $result['writer']=$r->writer;
        $result['type']=$r->type;

        if($r->type=="link")
				{					
					$result['link']=$r->link;
        }
        else 
        {
          $file = $r->file('pdf');
          if($file)
					{
            $rand=rand(1,1000000);
            $destinationPath = public_path()."/mechanicalinsider/pdf";
            $filename = $file->getClientOriginalName();
            $filename=$rand.$filename;
            $upload=$file->move($destinationPath, $filename);
            $url=URL::to('/');
            $result['link']=$url."/mechanicalinsider/pdf/".$filename;
 
          }
          else 
          {
            $result['link']=$r->link_existing;
          }

        }
       

        $result_json=json_encode($result,JSON_UNESCAPED_UNICODE);
       
        $school_id=json_encode($r->sub);
        $topic=json_encode($r->topic_id);
        $uid=Auth::User()->email;
        $access=$r->access;

        $user = DB::update("UPDATE `common_table` SET `jsondata`='$result_json',`groupid`='$groupid',`uid`='$uid',`school`='$school_id',`access`='$access' ,`topic`='$topic'  WHERE id='$r->id'");

        if($user)
        {
          return redirect('view_pdf2')->with('success','Data Successfully Updated');
        }
        else 
        {
          return redirect('view_pdf2')->with('error','Something went wrong,Please try again later.');
        }
      }
      else {
        return redirect('view_pdf2')->with('error','Sorry,Record not found.');
      }

  }
    
   
}
