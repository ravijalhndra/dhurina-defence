<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workshop;
use App\User;
use App\Role;
use App\Institute;
use App\SentNotification;
use App\InstituteSubadmin;
use Hash;
use DB;
use Carbon;
use View;
use Redirect;
use Mail;
use App\Mail\AddEmail;
use Auth;
use Excel;
use PDF;
class WorkshopController extends Controller
{
    
    public function add()
    {
        if(Auth::user()->groups=="all")
        {
            $groups1=DB::table('discussions')->orderBy('id','desc')->get();
            foreach ($groups1 as $group){
              $groups[]=$group->id;
            }
        }
        else
        {
            $user_groups=Auth::user()->groups;
            if(strrchr($user_groups,","))
            {
              $groups=explode(",",$user_groups);
            }
            else
            {
              $groups[]=$user_groups;
            }
        }
        foreach($groups as $group)
        {
        $data[]=DB::table('discussions')->where('id',$group)->first();
        }
        $course = DB::table('course')->get();
        return view::make('workshopAdd',compact('data','course'));
    }




    public function ViewDetail()
	{
		$field_type='5';
		 // $record=Workshop::where('field_type',$field_type)->orderBy('id', 'desc')->get();
    $record=DB::table('common_table')->where('field_type',$field_type)->orderBy('id','desc')->get();
		 
		  // get record from database as  using Workshop Model

    $new_record=[];
    foreach ($record as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec->groupid;
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
      $new_record=$record;
    }
    if(!empty($new_record))
    {
      foreach ($new_record as $key => $new_rec) {
      $notification=DB::table('sent_notifications')->where('postid',$new_rec->id)->first();
            if($notification)
            {
                $new_rec->notification="sent";
            }
            else
            {
                $new_rec->notification="";
            }

        $comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$new_rec->id)->get();
        foreach ($comments as $comment) {
            $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
            foreach ($reply as $key => $rep) {
              $comment->reply[]=$rep;
            }
          $new_rec->comments[]=$comment;
        }
      }
    } 
    $record=$new_record;


		
		

		return View::make('workshopView',compact('record'));
		
	}

	public function ajax(Request $request)
    {
    	$postid=$request->postid;
    	$field_type=$request->field_type;
    	$new_limit=$request->limit;
    	
    	$com=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$postid)->get();
	      return view::make('ajax',compact('com','new_limit','postid','field_type'));
    }

	public function edit_workshop($id)
	{
		
		$record=Workshop::where('id',$id)->first();
    if(Auth::user()->groups=="all")
        {
            $groups1=DB::table('discussions')->orderBy('id','desc')->get();
            foreach ($groups1 as $group){
              $groups[]=$group->id;
            }
        }
        else
        {
            $user_groups=Auth::user()->groups;
            if(strrchr($user_groups,","))
            {
              $groups=explode(",",$user_groups);
            }
            else
            {
              $groups[]=$user_groups;
            }
        }
        foreach($groups as $group)
        {
        $data[]=DB::table('discussions')->where('id',$group)->first();
        }
       $tags=DB::table('workshop_registerfield')->where('postid',$id)->first();
       $course = DB::table('course')->get();
       $college=DB::table('college')->get();
       $collegedata = DB::table('college_data')->where('postid',$id)->first();
       
       if($collegedata!='null' && !empty($collegedata))
       {
        $college_slected = DB::table('college')->where('course_id',$collegedata->courseid)->get();
        $stream_selected = DB::table('college')->where('id',$collegedata->college_id)->first();
        if($stream_selected!='null' && $stream_selected!='')
        {
        $stream = json_decode($stream_selected->stream);
        $year = $stream_selected->year;
        }
       }
    // dd($tags);
    
		return View::make('workshop_edit',compact('record','tags','college','data','course','collegedata','college_slected','stream','year'));
	}
	
	public function delete_workshop($id)
	{
    $data = DB::table('college_data')->where('postid', $id)->first();	
   
    if($data!='null' && $data!='')
    {
    $user1 = DB::delete("DELETE FROM `common_table` WHERE `id`='$id'");
    $user = DB::delete("DELETE FROM `college_data` WHERE `postid`='$id'");
    if($user>0)
		 {
     return redirect::back()->with('success','Data Successfully Deleted');
     }
     else 
		 {
			 return redirect::back()->with('error','Some Error occured try Again later.');
		 }
    }
    else
    {
      $user1 = DB::delete("DELETE FROM `common_table` WHERE `id`='$id'");
     
      if($user1 > 0)
		 {
       return redirect::back()->with('success','Data Successfully Deleted');
     }
     else 
		 {
			 return redirect::back()->with('error','Some Error occured try Again later.');
		 }
    }
	}

    public function store(Request $request){
    
				
		try{
				 $uid=Auth::user()->email;
				$file = $request->file('upload');
				if($file)
				{
					
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/workshopimage";
                    // dd($destinationPath);
                    // Get the orginal filname or create the filename of your choice
                    $filename = $file->getClientOriginalName();
                    $filename=$rand.$filename;
                    $upload=$file->move($destinationPath, $filename);
                }
                else
                {
                    $filename="";
                		
			
			  	}
				if($request['payment']=='paid')
				{
					$amount=$request['amount'];
					
				}
				else{
					$amount="";
        }
        if($request->type_institute=="group")
        {
          $groupid=implode(",", $request->groupid);
        }
        else
        {
          $groupid = 0;
          $courseid = $request->course;
          $college_id = $request->college;
          $stream = $request->stream;
          $year = $request->year;
        }
				$result['title']=$request['title'];
				// $result['description']=$request['description'];
				$result['image']=$filename;
				$result['date']=$request['date'];
				$result['fees']=$amount;
				$result['location']=$request['location'];
				$result['link']=$request['url'];
				$result['phone']=$request['phone'];
			
				$result_json=json_encode($result);
				$tags_explode=explode(",", $request['tags']);
				foreach ($tags_explode as $key => $value) {
					$arr[]['hint']=$value;
				}
				$array['field']=$arr;
        $arr_json=json_encode($array);
       
        if($request->type_institute=="group")
        {
				$work=new Workshop;
				$work->field_type='5';
        $work->uid=$uid;  
        $work->post_description=$request['description'];

        $work->groupid=$groupid;
				$work->jsondata=$result_json;
        $save = $work->save();
        $LastInsertId = $work->id;
        }
        
        if($request->type_institute=="college")
        {
          $work=new Workshop;
          $work->field_type='5';
          $work->uid=$uid;  
          $work->post_description=$request['description'];
          $work->post_type=1;
         
          $work->groupid=$groupid;
          $work->jsondata=$result_json;
          $save = $work->save();
          $LastInsertId = $work->id;
          $insert=DB::table('college_data')->insert(
            ['postid' => $LastInsertId,'field_type'=>5,'courseid'=>$courseid,'college_id' => $college_id,'stream'=>$stream,'year'=>$year,'status'=>'published']
            
        );
      }

        if($request->type_institute=="both")
        {
          $work=new Workshop;
          $work->field_type='5';
          $work->uid=$uid;  
          $work->post_description=$request['description'];
          $work->post_type=2;
          $work->groupid='all';
          $work->jsondata=$result_json;
          $work->post_type=2;
          $save = $work->save();
          $LastInsertId = $work->id;
          
          $insert=DB::table('college_data')->insert(
            ['postid' => $LastInsertId,'field_type'=>5,'courseid'=>0,'college_id' =>0,'stream'=>0,'year'=>0,'status'=>'published']
            
        );
       
      }
        
        
				
				 $insert=DB::table('workshop_registerfield')->insert(
            ['postid' => $LastInsertId,'jsondata'=>$arr_json]
            
        );
		// $field_type='5';
		// $record=Workshop::where('field_type',$field_type)->orderBy('id', 'desc')->get()->toArray(); // get record from database as  using Workshop Model
		// return View::make('workshopView',compact('record'));		
		    	return redirect('workshop')->with('success','Workshop updated Successfully.');	
	}
	
	catch(Exception $e)
    	{
    		return redirect('add_wrokshop')->with('error','Something Went wrong.Please Try again.');
    	}		
    }


    public function update_workshop($id,Request $request)
    {

		try{
				 $uid=Auth::user()->email;
				$file = $request->file('upload');
				if($file)
				{
					
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/workshopimage";
                    // dd($destinationPath);
                    // Get the orginal filname or create the filename of your choice
                    $filename = $file->getClientOriginalName();
                    $filename=$rand.$filename;
                    $upload=$file->move($destinationPath, $filename);
                }
                else
                {
                    $filename=$request->old_image;
                		
			
			  	}
				if($request['payment']=='paid')
				{
					$amount=$request['amount'];
					
				}
				else{
					$amount="";
        }
        if($request->type_institute=="group")
        {
          $groupid=implode(",", $request->groupid);
        }
        else
        {
          $groupid = 0;
          $courseid = $request->course;
          $college_id = $request->college;
          $stream = $request->stream;
          $year = $request->year;
        }
				$result['title']=$request['title'];
				// $result['description']=$request['description'];
				$result['image']=$filename;
				$result['date']=$request['date'];
				$result['fees']=$amount;
				$result['location']=$request['location'];
				$result['link']=$request['url'];
				$result['phone']=$request['phone'];
			
				$result_json=json_encode($result);
				$tags_explode=explode(",", $request['tags']);
				foreach ($tags_explode as $key => $value) {
					$arr[]['hint']=$value;
				}
				$array['field']=$arr;
        $arr_json=json_encode($array);
      
        if($request->type_institute=="group")
        {
        
        $work= Workshop::find($id);
        $work->groupid=$groupid;  
        $work->post_description=$request['description'];  
				$work->uid=$uid;	
        $work->jsondata=$result_json;
        $work->post_type=0;
        $work->save();
        $data=DB::table('college_data')->where('postid',$id)->first();
        if($data!='null' && $data!='')
        {
        $delete=DB::table('college_data')->where('postid',$id)->delete();
        }  
        }

        if($request->type_institute == "college")
        {
          
          $work= Workshop::find($id);
          $work->groupid=$groupid;  
          $work->post_description=$request['description'];  
          $work->uid=$uid;	
          $work->post_type=1;
          $work->jsondata=$result_json;
          $work->save();
         $data = DB::table('college_data')->where('postid', $id)->first();
         
         if($data!='null' && $data!='')
         {
         $update_college=DB::table('college_data')->where('postid',$id)->update(
            ['courseid'=>$courseid,'college_id' => $college_id,'stream'=>$stream,'year'=>$year]);
         }   
         else
         {
          $insert_college=DB::table('college_data')->insert(
            ['postid'=>$id,'courseid'=>$courseid,'college_id' => $college_id,'stream'=>$stream,'year'=>$year,'status'=>'published']); 
         }
        }

        if($request->type_institute=="both")
        {
         
      	$work= Workshop::find($id);
        $work->groupid='all';  
        $work->post_description=$request['description'];  
        $work->uid=$uid;	
        $work->post_type=2;
				$work->jsondata=$result_json;
        $work->save();

        $data=DB::table('college_data')->where('postid',$id)->first();
        if($data!='null' && !empty($data))
        {
        $update_college=DB::table('college_data')->where('postid',$id)->update(
            ['courseid'=>0,'college_id' => 0,'stream'=>0,'year'=>0]);
        }
        else
        {
          $insert_college=DB::table('college_data')->insert(
            ['postid'=>$id,'courseid'=>0,'college_id' => 0,'stream'=>0,'year'=>0,'status'=>'published']);  
        }
        }

         $update=DB::table('workshop_registerfield')->where('postid',$id)->update(
            ['jsondata'=>$arr_json]
            
        );

        // $update_college=DB::table('college_data')->where('postid',$id)->update(
        //   ['courseid'=>$courseid,'college_id' => $college_id,'stream'=>$stream,'year'=>$year]);
          
		
		return redirect('workshop')->with('success','Workshop updated Successfully.');		
		    
	}
	
	catch(Exception $e)
    	{
    		return redirect('edit_workshop/'.$id.'')->with('error','Something Went wrong.Please Try again.');
    	}		
    }

    public function workshop_detail($id)
    {
    	$field_type=5;
    	$query=DB::table('common_table')->where('id',$id)->first();
			$comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$query->id)->get();
			foreach ($comments as $comment) {
				$query->comments[]=$comment;
			}
		
    	return view::make('workshop_details',compact('query'));
    }

    public function delete_workshop_comment(Request $request)
    {
        $id=$request->comment_id;
        $delete=DB::table('comment_on_post')->where('id',$id)->delete();
        if($delete)
        {
            return redirect()->back()->with('success','comment deleted Successfully');
        }
    }


    public function send_notification($id)
    {

        $post=DB::table('common_table')->where('id',$id)->first();
       
        $field_type=$post->field_type;

        $json=json_decode($post->jsondata,true);
        $image=str_replace(" ","%20",$json['image']);
        $thumbnail="";
        // $thumbnail=str_replace(" ","%20",$json['thumbnail']);
        $base_url=url('/mechanicalinsider/workshopimage/');
        $groupid=explode(",",$post->groupid);

        // $uid=[];
        // foreach ($groupid as $gid) {
        //     $data=DB::table('group_follow_rec')->where('group_name',$gid)->get();
        //    foreach ($data as $d) {
        //     if(!in_array($d->profile_uid, $uid))
        //         {
        //             $uid[]=$d->profile_uid;
        //         }
        //    }
        // }
        // $player_id=[];
        // foreach ($uid as $u) {
        //     $players=DB::table('users')->where('email',$u)->get();
        //     foreach ($players as $player) {
        //             $json1=json_decode($player->notification,true);
        //             if($json1['institute']==true)
        //             {
        //                 $player_list[]=$player;
        //             }

        //         }
            
        // }
        // foreach ($player_list as $player) {
                
        //             $player_id[]=$player->gcmid;
                
        //     }
        // $content = array(
        //    "en" => $json['title']
           
        //    );
          
        //   $fields = array(
        //    'app_id' => "306439b4-709b-468e-b687-d0f9e6917268",
        //    // 'included_segments' => $pp,
        //    'include_player_ids' => $player_id,
           
        //       'data' => array("key" => "value"),
        //    'contents' => $content,
        //    "large_icon" => "http://tyroad.co.uk/hellotopper_admin/public/img/hellotopper.png",
        //    "big_picture" => "$base_url$image"
          
        //   );
        //   // dd($fields);
        //   // print_r($fields);
           
        //   $fields = json_encode($fields);
           
           
          
        //   $ch = curl_init();
          
            
        //   curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        //   curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MDQyY2E5MTEtMzdhZS00N2EwLWI4N2UtZTM3NGFlMjcxYWUw',
        //                                              'Content-Type: application/json; charset=utf-8'));
        //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //   curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //   curl_setopt($ch, CURLOPT_POST, TRUE);
        //   curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //   $response = curl_exec($ch);
        //   curl_close($ch);
        //   $result=json_decode($response,true);

//old code closes

        //new code

        $content = array(
           "en" => $json['title']
           
           );
          
           $fields = array(
           'app_id' => "306439b4-709b-468e-b687-d0f9e6917268",
           'included_segments' =>  array('All'),
           
              'data' => array("id" => $id,"field_type"=>$field_type),
           'contents' => $content,
            "large_icon" => "",
           "big_picture" => $base_url.'/'.$image
          
          );

          $fields = json_encode($fields);
          // print_r($fields);
           
           
          
          $ch = curl_init();
          
            
          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MDQyY2E5MTEtMzdhZS00N2EwLWI4N2UtZTM3NGFlMjcxYWUw',
                                                     'Content-Type: application/json; charset=utf-8'));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_HEADER, FALSE);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

          $response = curl_exec($ch);
         

          curl_close($ch);
          $result=json_decode($response,true);
         

 //new code close
          if(array_key_exists('id', $result))
          {
            $notification=SentNotification::where('postid',$id)->first();
            if(isset($notification->postid))
            {
                SentNotification::where('postid',$id)->update(['postid'=>$id]);
            }
            else
            {
                
                $notification = new SentNotification();

                $notification->postid = $id;

                $notification->save();
            }
            return redirect()->back()->with('success','Notification Sent Successfully');
          }
          else
          {
            return redirect()->back()->with('error','Something went wrong.Please try again.');
          }
    
    }

    public function search(Request $request)
    {
      $field_type='5';
     // $record=Workshop::where('field_type',$field_type)->orderBy('id', 'desc')->get();
     
      // get record from database as  using Workshop Model


    $record=DB::table('common_table')->where('field_type',$field_type)->where('jsondata','like','%'.$request->val.'%')->orderBy('id','desc')->get();

    $new_record=[];
    foreach ($record as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec->groupid;
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
      $new_record=$record;
    }
    if(!empty($new_record))
    {
      foreach ($new_record as $key => $new_rec) {
      $notification=DB::table('sent_notifications')->where('postid',$new_rec->id)->first();
            if($notification)
            {
                $new_rec->notification="sent";
            }
            else
            {
                $new_rec->notification="";
            }

        $comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$new_rec->id)->get();
        foreach ($comments as $comment) {
          $new_rec->comments[]=$comment;
        }
      }
    } 
    $record=$new_record;
    return View::make('workshop_search',compact('record'));
    }

    public function show_registered_users($id)
    {
      $data=DB::table('workshop_registerfield')->where('postid',$id)->first();
      $users=DB::table('workshop_registeration_detail')->where('postid',$id)->get();
      return view::make('workshop_registered_users',compact('data','users','id'));
    }


    public function create_pdf($id)
    {
        $data=DB::table('workshop_registerfield')->where('postid',$id)->first();
      $users=DB::table('workshop_registeration_detail')->where('postid',$id)->get();
        view()->share('data',$data);
        view()->share('users',$users);
            $pdf = PDF::loadView('pdf_view');
            return $pdf->download('pdf_view');
        
        return view('pdf_view');
    }


    public function export_csv($id)
    {
       $data=DB::table('workshop_registerfield')->where('postid',$id)->first();
      $data=json_decode($data->jsondata,true);
      foreach($data as $dd){
      foreach($dd as $d){
        $head[]=$d['hint'];
      }
    }
      // dd($head);


      $filename = "learner.csv";
        $fp = fopen('php://output', 'w');
         header('HTTP/1.1 200 OK');
        header('Cache-Control: no-cache, must-revalidate');
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=".$filename);
        // readfile('download/' . $filename);

        fputcsv($fp,$head);

        $users=DB::table('workshop_registeration_detail')->where('postid',$id)->get();
      foreach ($users as $user){
              $res=json_decode($user->jsondata,true);
              foreach($res as $ress){
              $value[]=$ress['value'];
              }
              fputcsv($fp, $value);
              $value="";
            }
            fclose($fp);   
    }


    //     public function export_csv($id)  
// {
    
//       $data=DB::table('workshop_registerfield')->where('postid',$id)->first();
//       $data=json_decode($data->jsondata,true);
//       foreach($data as $dd){
//       foreach($dd as $d){
//         $head[]=$d['hint'];
//       }
//   }
//   $head_array=implode(",",$head);
//   // $table_array[]=$head_array;
//       $users=DB::table('workshop_registeration_detail')->where('postid',$id)->get();
//       foreach ($users as $user){
//               $res=json_decode($user->jsondata,true);
//               // dd($res);
//               foreach($res as $ress){
//                 $r_val=$ress['value'];
//                 // $r_key=$ress['hint'];
//                 $value[]=$r_val;
//                 // $value=implode(",",$value);
//               }

//               $table_array[]=$value;
//               $value="";
//             }
//             // dd($r_val);

//   dd($table_array);
//     // $result = DB::table('order')->where('id',$id)->first();  // the data you want to download as csv
//             // dd($result);

//     Excel::create('RegisteredUsers', function($excel) use ($table_array) {

//         // Set the spreadsheet title, creator, and description
//         $excel->setTitle('Registered Users');
//         $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
//         $excel->setDescription('workshop registered users');

//         // Build the spreadsheet, passing in the payments array
//         $excel->sheet('sheet1', function($sheet) use ($table_array) {
//             $sheet->fromArray($table_array, null, 'A1', false, false);
//         });

//     })->download('csv');

//     //         return Excel::create('csvfile', function ($excel) use ($table_array) {
//     //     $excel->sheet('mySheet', function ($sheet) use ($table_array) {
//     //         $sheet->fromArray($table_array);
//     //     });
//     // })->download('csv');
// }


    
}