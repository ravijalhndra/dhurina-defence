<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Mail\SendOtp;
use App\Sector;
use App\Jobs\Notification;
use App\Order;
use Mail;
use DB;
use Hash;


class CoreController extends Controller
{
    
    public function profile_update(Request $r)
    {
        try {
            $check=DB::table('users')->where('id',$r->user_id)->where('verified','yes')->first();
            if($check)
            {                   
                if($r->image != '')
                {
                    $imageName = date("YmdHis").'.'.$r->image->getClientOriginalExtension();
                    $success = $r->image->move(public_path('mechanicalinsider/userimage'), $imageName);
                    $imageName=$this->path().'/mechanicalinsider/userimage/'.$imageName;                
                }
                else{
                    $imageName=$check->image;
                }

                $insert=DB::table('users')->where('id',$r->user_id)->update(['name'=>$r->name,'email'=>$r->email,'state'=>$r->state,'college'=>$r->college,'image'=>$imageName]);

                if($insert == 0 || $insert == 1){
                    $data=DB::table('users')->where('id',$r->user_id)->first();
                    return response()->json(['data'=>$data,'message'=>'Profile has been updated successfully.','status'=>'success']);
                }
                else {
                    return response()->json(['message'=>'Something went wrong,Please try again later.','status'=>'fail']);           
                }


            }else 
            {
            return response()->json(['message'=>'Sorry,user record not found.','status'=>'fail']);

            }

        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        }
           

    }


    //getusercomment
    public function getusercomment(Request $r)
    {
        try {

            $check=DB::table('users')->where('id',$r->user_id)->where('verified','yes')->first();
            if($check)
            { 
                $limit=10;
                $skip=0;
                if($r->index > 0){
                    $skip=$r->index * $limit;
                }

                $comment=DB::table('common_table')->whereIn('field_type',[0,7,8])->where('user_id',$r->user_id)
                ->OrderBy('id','DESC')->limit($limit)->skip($skip)->get();

                if($this->check_count($comment) > 0){

                    foreach ($comment as $key => $value) {    
                        
                        // Like Status

                            $value->likestatus="dislike";

                            $like_status=DB::table('like_on_comment')->select('status')->where('user_id',$value->user_id)->where('commentid',$value->id)->first();
                            if($like_status){
                                $value->likestatus=$like_status->status;
                            }

                        // End Like Status        

                    }

                    return response()->json(['data'=>$comment,'message'=>'Successfully Record fetching.','status'=>'success']);
                }
                else {
                    return response()->json(['message'=>'Sorry, Record not found.','status'=>'fail']);
                }

            }
            else 
            {
                return response()->json(['message'=>'Sorry,user record not found.','status'=>'fail']);
            }

        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        } 

    }

    //get_common_comment

    public function get_common_comment(Request $r)
    {
        try {

            $check=DB::table('users')->where('id',$r->user_id)->where('verified','yes')->first();
            if($check)
            {
                $limit=10;
                $skip=0;
                if($r->index > 0){
                    $skip=$r->index * $limit;
                }
                $postcomment=array();

                $comment=DB::table('comment_on_post')->where('postid',$r->pid)
                ->OrderBy('id','DESC')->limit($limit)->skip($skip)->get();

                $post=DB::table('common_table')->where('id',$r->pid)->limit(1)->first();
                if($post){

                    $post->likestatus='dislike';
                    $like_status=DB::table('like_on_comment')->select('status')->where('user_id',$r->user_id)->where('commentid',$r->pid)->first();
                    if($like_status){
                        $post->likestatus=$like_status->status;
                    }

                    $postcomment=$post;
                }

                if($this->check_count($comment) > 0  || $this->check_count($postcomment) > 0 )
                {
                    foreach($comment as $key=>$value){

                        $check=DB::table('comment_reply_post')->where('postid',$value->postid)->where('comment_id',$value->id)->where('type','post')->get();
                        $value->reply=$check;
                    }

                    
                    return response()->json(['data'=>$comment,'postcomment'=>$postcomment,'message'=>'Successfully Record fetching.','status'=>'success']);    
                }
                else 
                {
                    return response()->json(['message'=>'Sorry, Record not found.','status'=>'fail']);
                }

            }
            else 
            {
                return response()->json(['message'=>'Sorry,user record not found.','status'=>'fail']);
            }   

        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        } 
    }


    //appversion

    public function appversion(Request $r)
    {
        $version=DB::table('appversion')->first();
        if($version){
            return response()->json(['data'=>$version,'message'=>'Successfully Record fetching.','status'=>'success']);  
        }else {
            return response()->json(['message'=>'Sorry, Record not found.','status'=>'fail']);

        }
    }

    //like post
    public function like_post(Request $r)
    {
        try{

            $check=DB::table('users')->where('id',$r->user_id)->where('verified','yes')->first();
            if($check)
            {
                $status='like';

                $like_check=DB::table('like_on_comment')->where('user_id',$r->user_id)->where('commentid',$r->cid)->first();
                if($like_check){
                    
                    if($like_check->status == 'like'){
                        $status='dislike';
                    }

                    $insert=DB::table('like_on_comment')->where('id',$like_check->id)->update(
                        ['email' =>$r->email, 'user_id' =>$r->user_id,'commentid'=>$r->cid,'status'=>$status]);

                }
                else {

                    $insert=DB::table('like_on_comment')->insert(
                        ['email' =>$r->email, 'user_id' =>$r->user_id,'commentid'=>$r->cid,'status'=>$status]);
                }


                if($insert){

                    if($status == 'like'){
                        //Common table comment count increament
                        $common_increment=DB::table('common_table')->where('id',$r->cid)->increment('likes',1);
                    }
                    else {                        
                        //Common table comment count increament
                        $common_increment=DB::table('common_table')->where('id',$r->cid)->decrement('likes',1);
                    }
                    

                    $data=DB::table('like_on_comment')->where('user_id',$r->user_id)->where('commentid',$r->cid)->first();
                    return response()->json(['data'=>$data,'message'=>'Successfully Record fetching.','status'=>'success']);  
                }
                else {
                    return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
                }


            }
            else 
            {
                return response()->json(['message'=>'Sorry,user record not found.','status'=>'fail']);
            } 

        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        } 
    }

    //insert_common_comment

    public function insert_common_comment(Request $r)
    {   
        try{

            $check=DB::table('users')->select('id','name','college','image','verified')->where('id',$r->user_id)->where('verified','yes')->first();
            if($check)
            {

                $common_table=DB::table('common_table')->select('field_type')->where('id',$r->postid)->first();
                if($common_table)
                {
                    $post_data = array(
                        'comments' => $r->comment,
                        'name' => $check->name,
                        'collage' => $check->college,
                        'userimage' => $check->image,
                        'com_image' => "null"
                    );

                    $json=addslashes(json_encode($post_data,JSON_HEX_QUOT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));


                    $insert=DB::table('comment_on_post')->insert(
                        ['posttype' =>$common_table->field_type, 'postid' =>$r->postid,'uid'=>$r->email,'user_id'=>$r->user_id,
                        'jsondata'=>json_encode($post_data)]);


                    if( $insert){

                        //Common table comment count increament
                        $common_increment=DB::table('common_table')->where('id',$r->postid)->increment('comment',1);

                        //User table comment count increament
                        $user_increment=DB::table('users')->where('id',$r->user_id)->increment('comment',1);

                        return response()->json(['message'=>'Thank you! Your comment has been successfully submitted.','status'=>'success']);

                    }
                    else 
                    {
                            return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
                    }

                }
                else 
                {
                    return response()->json(['message'=>'Sorry,Post record not found.','status'=>'fail']);
                }
            }
            else 
            {
                return response()->json(['message'=>'Sorry,user record not found.','status'=>'fail']);
            }


        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        } 

    }

    //update_players
    public function update_players(Request $r )
    {
        try{

            $check=DB::table('users')->where('id',$r->user_id)->where('verified','yes')->first();
            if($check)
            {
                $insert=DB::table('users')->where('id',$r->user_id)->update(['gcmid' =>$r->gcmid, 'registration_id' =>$r->registration_id]); 
                 
                if( $insert){
                    return response()->json(['message'=>'Your profile has beed updated.','status'=>'success']);
                }
                else 
                {
                    return response()->json(['message'=>'Device and Player ID already up-to-date','status'=>'fail']);
                }
            }
            else 
            {
                return response()->json(['message'=>'Sorry,user record not found.','status'=>'fail']);
            }

        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        } 
    }

    public function homedata(Request $r)
    {
        try{

            $check=DB::table('users')->where('id',$r->user_id)->where('verified','yes')->first();
            if($check)
            {
                $limit=10;
                $skip=0;
                if($r->index > 0){
                    $skip=$r->index * $limit;
                }

                $comment=DB::table('common_table')->where('field_type',0);

                if($r->ref_code !=''){
                    $comment=$comment->where('ref_code',$r->ref_code);
                }else {
                    $comment=$comment->where(function($query) {
                        $query->where('ref_code', null)
                              ->orWhere('ref_code', '');
                    });                    
                }

                $comment=$comment->where('post_type','!=',1)->where('status','published')->where('premium',0)
                ->OrderBy('id','DESC')->limit($limit)->skip($skip)->get();

                if($this->check_count($comment) > 0){

                    foreach ($comment as $key => $value) {    
                        
                        // Like Status

                            $value->likestatus="dislike";

                            $like_status=DB::table('like_on_comment')->select('status')->where('user_id',$r->user_id)->where('commentid',$value->id)->first();
                            if($like_status){
                                $value->likestatus=$like_status->status;
                            }

                        // End Like Status        

                    }

                    return response()->json(['data'=>$comment,'message'=>'Successfully Record fetching.','status'=>'success']);
                }
                else {
                    return response()->json(['message'=>'Sorry, Record not found.','status'=>'fail']);
                }

            }
            else 
            {
                return response()->json(['message'=>'Sorry,user record not found.','status'=>'fail']);
            }

        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        } 
    }


    public function login2(Request $r)
    {

        $check=DB::table('users')->where('mobile',$r->mobile)->where('check',0)->first();
        if($check)
        {

            if($check->block_status == 'block')
            {
                return response()->json(['message'=>'Sorry your account is temporarily locked .Please contact with administrator.','status'=>'fail']);
            }

            if($check->verified == 'yes')
            {
                $pass=$check->password;
                $password=$r->password;

                if (Hash::check($password,$pass))
                {                    
                    //Check device and PLayer ID
                    if($r->player_id !=  $check->gcmid && $r->device_id != $check->registration_id ){
                        $msg='Sorry,Someone access your login detail';
                        if($check->gcmid != ''){
                            Notification::dispatch($msg,$check->gcmid);
                        }
                       
                    }

                    //update PLayerID
                    $playerinsert=DB::table('users')->where('id',$check->id)->update(['gcmid' =>$r->player_id, 'registration_id' =>$r->device_id]); 

                    $data=DB::table('users')->where('id',$check->id)->first(); 

                    if($data->referralcode != '')
                    {
                        $school=DB::table('admins')->where('ref_code',$data->referralcode)->first();
                        if($school)
                        {
                            $data->school=$school->name;
                        }
                    }

                    return response()->json(['data'=>$data,'message'=>'Successfully loggin','status'=>'success']);
                }
                else
                {
                    return response()->json(['message'=>'Invalid password','status'=>'fail']);
                }
            }
            else
            {
                return response()->json(['message'=>'Sorry,account is not verified','status'=>'fail']);
            }
        }
        else
        { 
           return response()->json(['message'=>'Invalid mobile number,Please enter valid mobile number.','status'=>'fail']);
        }

    }

    public function getuserinfo(Request $r )
    {
        try{

            $check=DB::table('users')->where('id',$r->user_id)->where('verified','yes')->first();
            if($check)
            {
                return response()->json(['data'=>$check,'message'=>'Successfully fetcging user detail.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Sorry,user record not found.','status'=>'fail']);
            }
        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        }

    }

    //get_Premiumtest
    public function get_Premiumtest(Request $r )
    {
        try{

            $check=DB::table('users')->where('id',$r->user_id)->where('verified','yes')->first();
            if($check)
            {
                $limit=10;
                $skip=0;
                if($r->index > 0){
                    $skip=$r->index * $limit;
                }

                $admin=DB::table('admins')->where('role','admin')->first();
                $admin_id= $admin->id;

                $school_id='';
                $ref_code='';                
                if($r->ref_code != '')
                {
                    $check=DB::table('admins')->where('ref_code',$r->ref_code)->first();
                    if($check)
                    {
                        $school_id= $check->id;
                        $ref_code=$check->email;
                    }
                }
                else 
                {
                    $ref_code=$admin->email;
                }
                
                //Get CourseID
                $course=DB::table('new_courses')->where('name',$r->course_id)
                ->where('school_id',$school_id)
                ->orWhere(function($query) use ($r,$admin_id) {
                    $query->where('school_id', $admin_id)
                        ->where('name', $r->course_id);
                })->get()->pluck('id');
                
             
                $comment=DB::table('common_table')->where('status','published')->where('post_type','!=',0)
                        ->where('uid',$ref_code);
                    
                    if($r->course_id != ''){
                        $comment=$comment->where(function ($q) use($course) {            
                            foreach($course as $keys1 => $ords1)
                            {
                                if($keys1 == 0)
                                {
                                    $q->whereRaw("find_in_set('".$ords1."',groupid)");
                                }
                                else {
                                    $q->whereRaw("find_in_set('".$ords1."',groupid)");
                                }
                            }
                        });
                    }    

                $comment=$comment->OrderBy('id','DESC')->limit($limit)->skip($skip)->get();

                if($this->check_count($comment) > 0){

                    foreach ($comment as $key => $value) {    
                        
                        // Like Status
                            $value->likestatus="dislike";
                            $like_status=DB::table('like_on_comment')->select('status')->where('user_id',$r->user_id)->where('commentid',$value->id)->first();
                            if($like_status){
                                $value->likestatus=$like_status->status;
                            }
                        // End Like Status        

                    }

                    return response()->json(['data'=>$comment,'message'=>'Successfully Record fetching.','status'=>'success']);
                }
                else {
                    return response()->json(['message'=>'Sorry, Record not found.','status'=>'fail']);
                }
            }
            else 
            {
                return response()->json(['message'=>'Sorry,user record not found.','status'=>'fail']);
            }


        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        }
    }



    //postcomment_ingroup
    public function postcomment_ingroup(Request $r)
    {
        try{
            
            $check=DB::table('users')->where('id',$r->user_id)->where('verified','yes')->first();
            if($check)
            {
                $date=date("y/m/d");
                $time=date('H:i:s');
                $dt="20".$date;

                $post_type=0;
                if($check->type == 2){
                    $post_type=1;
                }
                  
                if($r->image != '')
                {
                    $imageName = date("YmdHis").'.'.$r->image->getClientOriginalExtension();
                    $success = $r->image->move(public_path('mechanicalinsider/commentimage'), $imageName);

                    $imageName='commentimage/'.$imageName;
                    
                    $post_data = array(
                        'comments' => $r->comment,
                        'name' => $check->name,
                        'collage' => '',
                        'userimage' => $check->image,
                        'com_image' => $imageName
                    );

                }
                else 
                {
                    $post_data = array(
                        'comments' => $r->comment,
                        'name' => $check->name,
                        'collage' => '',
                        'userimage' => $check->image,
                        'com_image' => 'null'
                    );
                }

                $json=json_encode($post_data);
                    
                $insert=DB::table('common_table')->insert(
                    ['field_type' =>'0','post_type' =>$post_type,'groupid'=>$r->pid,'likes'=>'0',
                    'comment'=>'0','view'=>'0','uid'=>$check->email,'user_id'=>$r->user_id,'status'=>'notpublished','jsondata'=>$json ,'ref_code'=>$r->ref_code]);


                if($insert){

                    $t=$dt." ".$time;

                    //discussion Increment
                    $discussions_increment=DB::table('discussions')->where('id',$r->pid)->increment('comments',1);

                    //User Increment
                    $user_increment=DB::table('users')->where('id',$r->user_id)->increment('comment',1);

                    return response()->json(['message'=>'Comment Inserted Successfully.','status'=>'success']);
                }    
                else 
                {
                    return response()->json(['message'=>'Something went wrong, Please try again later.','status'=>'fail']);
                }

            }
            else 
            {
                return response()->json(['message'=>'Sorry,user record not found.','status'=>'fail']);
            }
        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        }

    }


    //get_group
    public function gtq(Request $r)
    {
        $test=DB::table('common_table')->where('id',$r->testid)->first();
        if($test)
        {
            $arr=array();
            $arr['test_record']=$test;

            $question=DB::table('test_question')->where('testid',$r->testid)->OrderBy('id','ASC')->get();
            if($this->check_count($question) > 0){
                $arr['data']=$question;
                $arr['response']='success';
            }
            else {
                $arr['response']='no question';
            }

            return response()->json(['data'=>$arr,'status'=>'success']);
        }
        else 
        {
            return response()->json(['message'=>'Sorry,no question found.','status'=>'fail']);
        }
    }

    //insert_result
    public function insert_result(Request $r)
    {
        $arr=array();
        $dt=date('Y-m-d');
        $result=DB::table('result_record')->where('testid',$r->testid)->where('user_id',$r->user_id)->first();
        if($result){

            $insert=DB::table('result_record')->where('id',$result->id)->update(
                ['testid' =>$r->testid,'uid' =>$r->uid,'user_id'=>$r->user_id,'mark'=>$r->marks,'timetaken'=>$r->time,'date'=>$dt]
            );

            $common=DB::table('common_table')->where('id',$r->testid)->first();
            if($common)
            {
                $arr['test']=$common;
                $arr['status']= 'success';
				$arr['response']= 'Successfully';
            }
            else 
            {
                $arr['status']= 'error';
				$arr['response']= 'Cannot Update Record';	
            }

        }
        else
        {
            $insert=DB::table('result_record')->insert(
            ['testid' =>$r->testid,'uid' =>$r->uid,'user_id'=>$r->user_id,'mark'=>$r->marks,'timetaken'=>$r->time,'date'=>$dt]);
            
            if($insert){

                //view
                $view=DB::table('common_table')->where('id',$r->testid)->increment('view',1);

                $common=DB::table('common_table')->where('id',$r->testid)->first();
                if($common)
                {
                    $arr['test']=$common;
                    $arr['status']= 'success';
				    $arr['response']= 'Successfully';
                }
                else 
                {
                    $arr['status']= 'error';
                    $arr['response']= 'Cannot Update Record';	
                }
            }
        }
        return response()->json(['data'=>$arr,'status'=>'success']);        
    }

    //find rank
    public function test_rank(Request $r){

        $time=date('Y-m-d H:i:s');

        $check=DB::table('result_record')->where('testid',$r->testid)->where('user_id',$r->user_id)->first();
        if($this->check_count($check)>0){

            $test_time=DB::table('common_table')->where('id',$r->testid)->first();
            if($this->check_count($test_time) > 0){
                
                $limit=10;
                $skip=0;
                if($r->index > 0){
                    $skip=$r->index * $limit;
                }

                if($test_time->rank_time <= $time ){
                    $resultRank=DB::table('result_record')->join('users','result_record.user_id','=','users.id')
                    ->select('result_record.*','users.name as user_name','users.mobile as user_mobile','users.image as user_image')  
                    ->where('result_record.testid',$r->testid)->orderBy('result_record.mark','DESC')->orderBy('result_record.timetaken','ASC')
                    // ->skip($skip)->limit($limit)
                    ->get();

                    if($this->check_count($resultRank) > 0){
                        $userObject=[];
                        $dataObject=[];

                        foreach ($resultRank as $key => $value) {

                            if($r->user_id == $value->user_id){
                                $value->rank=$key + 1;
                                $userObject=$value; 

                                if($key  > 10){
                                    break;
                                }
                                
                                // unset($resultRank[$key] );
                            }
                            
                            if($key <10){

                                $value->rank=$key + 1; 
                                $dataObject[]=$value;
                            }

                        }


                        return response()->json(['userRank'=> $userObject,'data'=>$dataObject,'test_detail'=>$test_time,'response'=>'Successfully.','status'=>'success']); 
                    }else{
                        return response()->json(['response'=>'Something went wrong,Please try again later.','status'=>'error']); 
                    }

                }else {
                    
                    $get_time=date('d M H:i a',strtotime($test_time->rank_time));
                    return response()->json(['response'=>'Sorry,Rank will be disclosed on '.$get_time.'.','status'=>'error']); 
                }

            }else {
                return response()->json(['response'=>'Sorry,Test not found.','status'=>'error']); 
            }

        }else {
            return response()->json(['response'=>'Sorry,You did not submit test.','status'=>'error']); 
        }
    }


    //Check user submit test or not
    public function check_test_submission(Request $r){

        $time=date('Y-m-d H:i:s');

        $check=DB::table('result_record')->where('testid',$r->testid)->where('user_id',$r->user_id)->first();
        $testCheck=DB::table('common_table')->where('id',$r->testid)->first();
        if($this->check_count($testCheck)>0){
        
            if($testCheck->rank_time <= $time ){
                return response()->json(['response'=>'You can submit test now.','status'=>'success']);
            }else{
                if($this->check_count($check)>0){
                    return response()->json(['response'=>'Sorry,You already submit test.','status'=>'error']);
                }else{
                    return response()->json(['response'=>'No test submittion found.','status'=>'success']); 
                }  
            }

        }else{            
            return response()->json(['response'=>'Sorry,Test not found submit test.','status'=>'error']); 
        }

    }


    //get_post
    public function get_post(Request $r)
    {
        $common=DB::table('common_table')->where('id',$r->postid)->first();
        if($common){

            //view
            $view=DB::table('common_table')->where('id',$r->postid)->increment('view',1);

            // Like Status
                $common->likestatus="dislike";

                $like_status=DB::table('like_on_comment')->select('status')->where('user_id',$common->user_id)->where('commentid',$r->postid)->first();
                if($like_status){
                    $common->likestatus=$like_status->status;
                }
            // End Like Status 

            return response()->json(['data'=>$common,'message'=>'Record found Successfully.','status'=>'success']);

        }
        else 
        {
            return response()->json(['message'=>'Sorry,Post not found.','status'=>'fail']);
        }

    }

    // Delete_groupcomment
    public function delete_groupcomment(Request $r)
    {
        if($r->user_id && $r->id && $r->email){

            $check=DB::table('common_table')->where('id',$r->id)->first();
            if($check){

                $delete=DB::table('common_table')->where('id',$r->id)->delete();
                if($delete){

                    //Common table comment count increament
                    $decrement=DB::table('users')->where('id',$r->user_id)->decrement('comment',1);

                    return response()->json(['message'=>'Record has been deleted Successfully.','status'=>'success']);
                }
                else{
                    return response()->json(['message'=>'Something went wrong,Please try again later.','status'=>'fail']);
                }

            }
            else {
                return response()->json(['message'=>'Sorry,Post not found.','status'=>'fail']);
            }

        }
        else {
            return response()->json(['message'=>'Sorry,UserID,ID and Email can not be empty.','status'=>'fail']);
        }        
    }

    // likecomment
    public function likecomment(Request $r)
    {
        if($r->id && $r->email && $r->user_id && $r->email1 && $r->user_id1){

            $check=DB::table('users')->where('id',$r->user_id)->first();
            if($check){

                $username=$check->name;
                $image=$check->image;

                $status_check='like';

                $check_like=DB::table('like_on_comment')->where('user_id',$r->user_id)->where('commentid',$r->id)->first();
                if($check_like){

                    $status='like';
                    if($check_like->status == 'like')
                    {
                        $status='dislike';
                        $status_check='dislike';
                    }

                    $insert=DB::table('like_on_comment')->where('commentid',$r->id)->update(['status'=>$status]);
                }
                else 
                {
                    $status_check='dislike';
                    $insert=DB::table('like_on_comment')->insert(
                        ['email' =>$r->email, 'user_id' =>$r->user_id,'commentid'=>$r->id,'status'=>'dislike']); 
                }

                if($insert)
                {
                    $common_increment=DB::table('common_table')->where('id',$r->id);
                    if($status_check == 'dislike')
                    {
                        $common_increment=$common_increment->increment('likes',1);
                    }
                    else {
                        $common_increment=$common_increment->decrement('likes',-1);
                    }

                    return response()->json(['data'=>$status_check,'message'=>'Successfully.','status'=>'success']);

                }else {
                    return response()->json(['message'=>'Something went wrong,Please try again later.','status'=>'fail']);
                }
            }
            else {
                return response()->json(['message'=>'Sorry,User not found.','status'=>'fail']);
            }
        }
        else {
            return response()->json(['message'=>'Sorry,Param missing.','status'=>'fail']);

        }

    }


    //replies_delete
    public function replies_delete(Request $r)
    {
        if($r->id && $r->email && $r->user_id && $r->psid)
        {
            $check=DB::table('comment_on_post')->where('id',$r->id)->first();
            if($check)
            {
                $delete=DB::table('comment_on_post')->where('id',$r->id)->delete();
                if($delete)
                {
                    $decrement=DB::table('common_table')->where('id',$r->psid)->decrement('comment',-1);
                    $user_decrement=DB::table('users')->where('id',$r->user_id)->decrement('comment',-0);

                    return response()->json(['message'=>'Record deleted successfully.','status'=>'success']);

                }
                else 
                {
                    return response()->json(['message'=>'Something went wrong,Please try again later.','status'=>'fail']);
                }
            }
            else 
            {
                return response()->json(['message'=>'Sorry,Record not found.','status'=>'fail']);
            }        
        }
        else 
        {
            return response()->json(['message'=>'Sorry,Param missing.','status'=>'fail']);
        }

    }


    public function get_question(){
        echo 'Hi';
    

    }
    public function my_orders(Request $r)
    {
       
        // $data=DB::table('order')->where('user_id',$r->user_id)->where('status','SUCCESS')->get();
        $data=DB::table('course_access')->where('user_id',$r->user_id)->where('type','course')->join('new_courses','course_access.course_id','=','new_courses.id')
        ->get();
        
        foreach ($data as $value) {
            $value->school_detail=DB::table('subject')->whereIn('id',json_decode($value->subject))->select('id','name','image')->get();
        }
        if(count($data)>0)
        {
            return response()->json(['data'=>$data,'message'=>'Record found','status'=>'success']);
        }
        else {
            return response()->json(['message'=>'Record not found','status'=>'fail']);

        }
    }

}
