<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\video_duration;

class VideoController extends Controller
{

    //Add video duration according to date

    public function duration(Request $r)
    {
        try 
        {
            
            $check=video_duration::where('user_id',$r->user_id)->where('date',$r->date)->first();
            if($check)
            {
                $insert=video_duration::find($check->id);
                $insert->duration=$check->duration + $r->duration;
            }
            else 
            {
                $insert=new video_duration;
                $insert->duration=$r->duration;
            }

            $insert->user_id=$r->user_id;
            $insert->mobile=$r->mobile;
            $insert->date=$r->date;           
            $insert->ref_code=$r->ref_code;
            $save=$insert->save();
            if($save)
            {
                return response()->json(['data'=>$insert,'message'=>'Video duration has been saved.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Something went wrong,Please try again later.','status'=>'fail']);
            }
        } 
        catch (\Exception $e) 
        {
            return response()->json(['status'=>'fail','message'=>$e->getMessage()]);
        }


    }


}
