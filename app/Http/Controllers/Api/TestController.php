<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Vimeo
use Vimeo\Laravel\Facades\Vimeo;
use DB;
use Hash;
use Storage;
use Config;
use File;

class TestController extends Controller
{
    public function vimeo_video(Request $r)
    {
        if(!$r->video || $r->video == '') 
        {
            return response()->json(['status'=>'fail']);
        }

        try
        {
            $file = $r->file('video');
            $realpath = $file->getRealPath();

            $lib=$this->get_vimeo();
            $lib->setCURLOptions([CURLOPT_TIMEOUT => 300]);
            // Upload the file and include the video title and description.
            $uri = $lib->upload($realpath , array(
                'name' => $r->title,
                'description' => $r->description
            ));


            return response()->json(["URI"=>$uri,'status'=>'success']);

            // Get the metadata response from the upload and log out the Vimeo.com url
            $video_data = $lib->request($uri . '?fields=link');
            // echo '"' . $file_full_path . ' has been uploaded to ' . $video_data['body']['link'] . "\n";

            

            // Make an API call to see if the video is finished transcoding.
            $video_data = $lib->request($uri . '?fields=transcode.status');
            // echo 'The transcode status for ' . $uri . ' is: ' . $video_data['body']['transcode']['status'] . "\n";

            
            
        } 
        catch (\Exception $e) 
        {
            $message=$e->getMessage();
            return response()->json(['status'=>$message]);
        }
    }

    public function get_vimeo_video(Request $r)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.vimeo.com/me/videos?direction=desc&per_page=5&sort=date&fields=uri,name,description,pictures,stats,files");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer dd7315483bec73a74c06718022ec036e',
            'Content-Type: json',
            'Accept: application/vnd.vimeo.*+json;version=3.4'
        ));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close ($ch);

        $response = json_decode($server_output, true);
        $data=$response['data'];

        if (sizeof($data)>0) 
        {
            print_r($data);
        }
        
    }

    //course_move
    public function course_move()
    {
        $get_school=DB::table('admins')->where('id',221)->get();
        if($this->check_count($get_school) > 0)
        {
            foreach ($get_school as  $value) 
            {
                $school_id=$value->id;
                $course_id=array();

                //get course
                $get_course=DB::table('new_courses')->where('school_id',$school_id)->get();
                if($this->check_count($get_course) > 0)
                {
                    foreach ($get_course as  $val)
                    {
                       $course_name=$val->name;

                       $admin_course=DB::table('new_courses')->where('school_id',1)->where('name',$course_name)->first();
                       if($admin_course)
                       {
                        $course_id[]=strval($admin_course->id); 
                       }
                         
                    }
                }
                // print_r($course_id);
                //$update_course=$insert=DB::table('admins')->where('id',$school_id)->update(['course_id'=>json_encode($course_id)]);                
            }
        }
    }

    public function access_course_move()
    {
        $get_course=DB::table('course_access')->get();
        if($this->check_count($get_course) > 0)
        {
            foreach ($get_course as  $value) 
            {
                $course_name=DB::table('new_courses')->select('name','id')->where('id',$value->course_id)->first();
                if($course_name)
                {
                    $insert=DB::table('course_access')->where('id',$value->id)->update(['course_name'=>$course_name->name]);
                }

                // $course_name=DB::table('new_courses')->select('name','id')->where('id',$value->course_id)->first();
                // if($course_name)
                // {
                //     $admin_course=DB::table('new_courses')->where('school_id',1)->where('name',$course_name->name)->first();
                //     if($admin_course)
                //     {
                //        $insert=DB::table('course_access')->where('id',$value->id)->update(['course_id'=>$admin_course->id]); 
                //     }
               
                // }               
            }
        }

    }
    

    public function get_thumnail()
    {
    //    $get_video=DB::table('video')->where('type','aws')->get();
        $get_video=DB::table('video')->OrderBy('id','DESC')->limit(20)->get();
        print_r($get_video); die;
       if($get_video) 
       {
            foreach ($get_video as $video) 
            {
                $id=$video->id;
                $video_id=$video->vimeo;

                //run Cron for vimeo rest api
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://api.vimeo.com/me/videos/".$video_id."?fields=status,pictures,stats,files");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: bearer 162eb652085f7272fe8311336216656a',
                    'Content-Type: json',
                    'Accept: application/vnd.vimeo.*+json;version=3.4'
                ));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $server_output = curl_exec($ch);
                curl_close ($ch);
                $response = json_decode($server_output, true);

                if (array_key_exists('status',$response)) 
                {
                    $thumbnail='';
                    if (array_key_exists('3',$response['pictures']['sizes'])) 
                    {
                        $thumbnail=$response['pictures']['sizes'][3]['link'];
                    }
                    else {
                        $thumbnail=$response['pictures']['sizes'][2]['link'];
                    }

                    $quality=json_encode($response['files']);

                    $update = DB::table('video')->where('id',$id)->update(['thumbnail'=>$thumbnail,'quality'=>$quality,'type'=>'vimeo']);

                }

            }
       }
      
    }



    //AWS to Vimeo

    public function aws_to_vimeo(Request $r)
    {
        $video=DB::table('video')->whereNull('vimeo')->Orwhere('vimeo','')->get();
        
        if($this->check_count($video) > 0)
        {
            foreach($video as $v)
            {
                $url=explode('/',$v->video); 
                $video_key=end($url); 
                $exists = Storage::disk('s3')->has($video_key);
                if ($exists) 
                {   
                    try
                    {  
                        $localFile = public_path('Video/').$video_key;  
                        copy($v->video, $localFile);

                        $lib=$this->get_vimeo();
                        $lib->setCURLOptions([CURLOPT_TIMEOUT => 300]);

                        // Upload the file and include the video title and description.
                        $uri = $lib->upload($localFile, array(
                            'name' => $v->name,
                            'description' => date("Y-m-d H:i:s")
                        ));

                        $videoID=explode('/',$uri); 
                        $video_hack=end($videoID);                       

                        //update
                        $update=DB::table('video')->where('id',$v->id)->update(['vimeo' =>$video_hack]);

                        File::delete(public_path('Video/'.$video_key));
                    } 
                    catch (\Throwable $th) 
                    {
                        echo $th;
                    }
                }
            }
        }
    }


    public function move_video_access(Request $r)
    {

        $allSchool=DB::table('admins')->where('role','school')->get();
        if($this->check_count($allSchool) > 0)
        {
            foreach($allSchool as $school_id)
            {
                $course_id=json_decode($school_id->course_id,true);

                if(count($course_id) > 0)
                {                    
                    $video_not=DB::table('videoschool')->where('school_id',$school_id->id)->get()->pluck('video_id'); 


                    $data=DB::table('video')->where('publish','true')
                    ->where(function ($q) use($course_id) {
                        foreach($course_id as $key => $ord)
                        {
                            if($key == 0)
                            {
                                $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                            }
                            else 
                            {
                                $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                            }
                        }
                    })->get();

                    print_r( $this->check_count($data) );
                    
                    break;
                }

                
            }

        }

        
    }


    public function duplicate_user()
    {
        $list=array();
        $UDID=[2418,3997,4312,1721,1722,2492,3211,2861,2862,2439,2440,985,993,1003,3486,3487,707,790,1671,1085,492,520,582,588,589,3253,3452,3325,3388,3416,1772,1773,3403,3407,3418,1935,2047,2049,506,534,541,294,309,1209,1391,1096,538,539,3723,3724,1242,1307,1050,1162,3726,3849,2134,2472,3235,3242,3244,3331,3332,2360,2368,2376,2414,2432,2684,2685,2819,837,2119,2665,2666,471,1692,1742,988,989,391,392,1780,2084,2671,2674,2864,1952,2641,2648,1691,1791,1819,1647,1740,1230,1231,2476,2173,2675,2824,2905,3052,3054,3068,3070,3072,3074,3938,3989,2407,2849,180,417,4426,4569,1995,3329,2273,962,1051,1604,1562,1787,229,507,508,557,436,438,3836,3136,2699,2708,2709,2712,2714,1594,1679,2926,2985,4039,4052,3283,3286,1482,1644,1653,1701,3510,3522,1070,1377,1746,1724,1725,1726,3387,1846,1849,3981,1129,2140,2237,244,366,435,434,213,228,580,2556,3061,2516,1840,170,171,174,379,1246,1249,1251,1252,1481,1684,1689,1932,1934,1956,2012,2772,2778,3478,3480,4080,1860,1861,1537,1543,1975,1983,1066,1067,960,966,3500,3619,3621,1022,1026,2485,2486,2533,175,234,396,3798,4013,251,252,143,341,2208,2250,2936,3199,295,297,3563,3853,3856,2498,2858,1982,2549,2593,2370,2342,1903,2017,2018,2058,1333,1334,2387,2390,1128,1199,1278,1818,1820,3180,3341,3343,3637,3641,3645,2295,2397,3549,3560,3592,3599,3603,3611,3624,3647,3650,3672,3697,3706,3712,3758,3789,3805,3854,3859,3881,3884,3895,3928,3965,3969,4089,4267,4283,4286,4357,4397,4418,4447,2914,2915,2979,3140,3141,3149,3174,3151,3379,2833,2843,2999,2379,2384,414,415,254,583,584,3590,144,1472,1473,1259,1049];
        
        $users=DB::table('users')->select('mobile','id','referralcode','verified')->whereIn('id',$UDID)
        ->where('check',0)->orderBy('mobile','ASC')->get();
        foreach ($users as $key => $value)
        {
            if($value->verified == 'no' ){
                DB::table('users')->where('id',$value->id)->update(['check' =>1]);
                break;
            }

            
        }

        $list=array_unique($list);

        echo "<pre>"; print_r(json_encode($list));  echo "</pre>";

        // echo "<pre>"; print_r($not);  echo "</pre>";
        // dd(json_encode($list));
        // $res=array();
        // $res['mobile']=$value->mobile;
        // $res['count']=$count;
        // $res['id']=$value->id;
        // $list[]=$res;
        
    }

    public function course_mobile(Request $r)
    {
        // $mobile=DB::table('users')->select('mobile')->where('id',$r->user_id)->first();
        // echo $mobile->mobile;


        // $add_mobile=DB::table('users')->where('check',1)->update(['check' =>0]);

        //CommonTable move UserID
        // $course=DB::table('course_access')->get();
        // foreach ($course as $key => $value) 
        // {
        //     $user=DB::table('new_courses')->where('id',$value->course_id)->first();
        //     if($user){
        //         $add_mobile=DB::table('course_access')->where('id',$value->id)->update(['course_name' =>$user->name]);
        //     }
        // }

        //Like on comment Add UserID
        // $likes=DB::table('like_on_comment')->get();
        // foreach ($likes as $key => $value) 
        // {
        //     $user=DB::table('users')->where('email',$value->email)->first();
        //     if($user){

        //         $add_mobile=DB::table('like_on_comment')->where('id',$value->id)->update(['user_id' =>$user->id]);

        //     }
        // }

        //comment on post add UserID
        // $comment=DB::table('comment_on_post')->get();
        // foreach ($comment as $key => $value) 
        // {
        //     $user=DB::table('users')->where('email',$value->uid)->first();
        //     if($user){

        //         $add_mobile=DB::table('comment_on_post')->where('id',$value->id)->update(['user_id' =>$user->id]);

        //     }
        // }


        //result record
        // $result=DB::table('result_record')->get();
        // foreach ($result as $key => $value) 
        // {
        //     $user=DB::table('users')->where('email',$value->uid)->first();
        //     if($user){
        //         $result_update=DB::table('result_record')->where('id',$value->id)->update(['user_id' =>$user->id]);
        //     }
        // }

        //Course access
        // $check=DB::table('users')->where('mobile','9462149566')->first();
        // if($check)
        // {                
        //     $course_check=DB::table('new_courses')->where('id',223)->first();
        //     if($course_check)
        //     {
        //         $assign=DB::table('course_access')->where('user_id',$check->id)->where('course_id',223)->first();
        //         print_r($assign); die;

        //         if(!$assign)
        //         {
        //             $insert = DB::table('course_access')->insert(['mobile'=> $check->mobile,'school_id'=>Auth::user()->id,'user_id' => $check->id,'course_id'=>$value[1],'course_name'=>$course_check->name,'created_at'=>date("YmdHis"),'updated_at'=>date("YmdHis")]);                       
        //         }
        //     }
        // }



        //Add ref Code
        $check=DB::table('course_access')->where('ref_code','admin')->get();        
        foreach ($check as $key => $value) 
        {
            //Online Case
            $online_check=DB::table('order')->where('mobile',$value->mobile)->where('course_id',$value->course_id)->where('status','SUCCESS')->first();
            if($online_check)
            {
                $update_ref=DB::table('course_access')->where('id',$value->id)->update(['ref_code' =>'online']);  
            }
            else   // School ref code
            {
                if($value->school_id != '1')
                {
                    $admin_check=DB::table('admins')->where('id',$value->school_id)->first();
                    if($admin_check)
                    {
                        $update_ref=DB::table('course_access')->where('id',$value->id)->update(['ref_code' =>$admin_check->ref_code]); 
                    }
                }
            }
        }

        echo count($check);

    }


    public function check_pdf (Request $r)
    {
        $school_id='';
        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $school_id= $check->id;
            }
        }

        $admin=DB::table('admins')->where('role','admin')->first();
        $admin_id= $admin->id;

        $course=DB::table('new_courses')->where('name',$r->course_id)
        ->where('school_id',$school_id)
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->course_id);
        })->get()->pluck('id');

        $subject=DB::table('subject')->where('name',$r->subject_id)
        ->where('school_id',$school_id)        
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->subject_id);
        })->get()->pluck('id');

       
        //pagination
        $skip=0;
        if($r->index != '')
        {
            $skip=$r->index * 10;
        }

        $data=DB::table('common_table')->where('field_type',4)
        ->where(function ($q) use($course) {
            
            foreach($course as $keys1 => $ords1)
            {
                if($keys1 == 0)
                {
                    $q->whereRaw("find_in_set('".$ords1."',groupid)");
                }
                else {
                    $q->whereRaw("find_in_set('".$ords1."',groupid)");
                }
            }
        })
        ->where(function ($w) use($subject) {
            foreach($subject as $keys => $ords)
            {
                if($keys == 0)
                {
                    $w->whereRaw('json_contains(school, \'["'.$ords.'"]\')');    
                }
                else 
                {
                    $w->orWhereRaw('json_contains(school, \'["'.$ords.'"]\')');    
                }
            }
        })->skip($skip)->take(10)->get();

        if($this->check_count($data) > 0)
        {
            $mobile=DB::table('users')->select('mobile')->where('id',$r->user_id)->first();
            foreach($data as $key => $d)
            {
                $course_asses=DB::table('course_access')->where('mobile',$mobile->mobile)->where('course_name',$r->course_id )->first();
                if($course_asses)
                {
                    $d->access='true';
                }
                else
                {
                    $d->access='false';                                      
                }

                //like-dislike status
                $d->likestatus='dislike';
                $status=DB::table('like_on_comment')->select('status')->where('email',$r->email)->where('commentid',$d->id)->first();
                if($status)
                {
                    $d->likestatus=$status->status;
                }
            }

            return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']); 
            
        }   
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }

    }



    public function dummy(Request $r)
    {
       
    }
}
