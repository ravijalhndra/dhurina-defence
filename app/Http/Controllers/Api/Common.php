<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class Common extends Controller
{
	//Check sum
	public function get_checksum(Request $r)
	{
		if($r->type == 'create')
		{
		   $checksum= sha1('createname='.$r->name.'&meetingID='.$r->meetingID.'zp28SYNqcyfacl5ZSja6nPUZlFJKpLK01vX22mujE');
		}else
		{
			$checksum= sha1('joinfullName='.$r->name.'&meetingID='.$r->meetingID.'&password='.$r->password.'&userID='.$r->UserID.'zp28SYNqcyfacl5ZSja6nPUZlFJKpLK01vX22mujE');
			// $checksum= sha1('joinfullName=Pawan&meetingID=Rohit_123&password=QR6ijX3m&userID=5290zp28SYNqcyfacl5ZSja6nPUZlFJKpLK01vX22mujE');
		}

		if($checksum)
		{
			return response()->json(['data'=>$checksum,'message'=>'Successfully Checksum generated','status'=>'success']);
		}
		else
		{
			return response()->json(['message'=>'Sorry,Detail not found','status'=>'fail']);
		}
        
	}


    //
    public function getgroupdetails(Request $r){
		
		// $sql="select * from discussions WHERE id='$gid'";

    	 $group_det=DB::table('discussions')->where('id',$r->groupid)->first();

    	 if(sizeof($group_det)>0)
    	 {	
    	 	
	    	 // $sql1="select * from group_follow_rec WHERE `profile_uid`='$em' AND `group_name`= '$gid'";

	    	 $follow_rec=DB::table('group_follow_rec')->where('profile_uid',$r->email)->where('group_name',$r->groupid)->first();


				$res['status']='true';
				$res['message']='success';
			if(sizeof($follow_rec)> 0)
			{	
				$group_det->follow_status='follow';
				$group_det->notification=$follow_rec->notification;
			}
			else
			{
				$group_det->follow_status='unfollow';
				$group_det->notification='false';
			}

			$follow_rec=DB::table('common_table')->whereRaw("find_in_set($r->groupid,groupid)")->count();

			$group_det->postcount=$follow_rec;
		 }
		 else{
		 	
				$res['status']='false';
				$res['message']='group not exist';
		 }

		$res['group']=$group_det;
		
		echo json_encode($res);

	}


 // banner views insert
    public function banner_views(Request $r){
		
		// $sql="select * from discussions WHERE id='$gid'";

    	if(!empty($r->pid)&&!empty($r->userid))
    	{
	    	 
	    	$ins= DB::table('banner_views')->insert(
			    ['uid' => $r->userid, 'banner_id' => $r->pid]
			);

	    	if($ins){
	    			$res['status']=true;
					$res['message']='success';	
	    	}

			 else{
			 	
					$res['status']=false;
					$res['message']='something went wronge';
			 }
		}
		else{
			 	
					$res['status']=false;
					$res['message']='param missing';
			 }
		
		echo json_encode($res);

	}

public function leaderboarddemo(Request $r){

		if($r->time=='Today')
		{		
			$data = DB::table("points_earns")
			    ->select(DB::raw("SUM(points) as point,student_id"))
			    ->whereDay('created_at',date('d'))
			    ->groupBy("student_id")
			    ->get();
		}
		else if($r->time=='This Month')
		{		
			$data = DB::table("points_earns")
			    ->select(DB::raw("SUM(points) as point,student_id"))
			    ->whereMonth('created_at',date('m'))
			    ->groupBy("student_id")
			    ->orderBy("point","DESC")
			    ->get();
		}
		else if($r->time=='This Year'){

			$data = DB::table("points_earns")
			    ->select(DB::raw("SUM(points) as point,student_id"))
			    ->groupBy("student_id")
			    ->whereYear('created_at',date('y'))
			    ->orderBy("point","DESC")
			    ->get();
		}
		else{

			$data = DB::table("points_earns")
			    ->select(DB::raw("SUM(points) as point,student_id"))
			    ->groupBy("student_id")
			    ->orderBy("point","DESC")
			    ->get();
		}

		$arr=array();

        foreach ($data as $value) {

            $info=DB::table("users")->where('email',$value->student_id)->first();

            $value->name=$info->name;
            $value->image=$info->image;
             $value->level=$info->level;
            $value->college=$info->college;
            $value->city=$info->city;

            $arr['users'][]=$value;
        }

        echo json_encode($arr);


    }

}
