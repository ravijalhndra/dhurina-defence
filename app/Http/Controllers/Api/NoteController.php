<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\note;
use DB;

class NoteController extends Controller
{
    
    public function get_notes(Request $r)
    {
        $skip=0;
        if($r->index != '')
        {
            $skip=$r->index * 20;
        }

        $data=note::where('publish','true')->OrderBy('position','ASC')->skip($skip)->take(10);
        if($r->sector!=''){
            $data=$data->where('sector_id',$r->sector);
        }

        $data=$data->get();  
             
        if($this->check_count($data) > 0){
            return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']);
        }else{
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }

    }

    public function get_notes1(Request $r)
    {
        if(empty($r->user_id)){
            return response()->json(['message'=>'Param Missing','status'=>'fail']);
        }
        
        $skip=0;
        if($r->index != '')
        {
            $skip=$r->index * 20;
        }

        $data=note::where('publish','true')->OrderBy('position','ASC')->skip($skip)->take(20);

        
        if($r->sector!=''){
            $data=$data->where('sector_id',$r->sector);
        }

        if($r->type !=''){
            $data=$data->where('type',$r->type);
        }

        $data=$data->get();      
        
        foreach($data as $d){
            $check=DB::table('course_access')->where('course_id',$d->id)->where('user_id',$r->user_id)->where('type',$r->type)->count();
            if($check > 0){
                $d->access='true';
            }
            else{
                $d->access='false';
            }
        }
        
        if($this->check_count($data) > 0){
            return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']);
        }else{
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }

    }
}
