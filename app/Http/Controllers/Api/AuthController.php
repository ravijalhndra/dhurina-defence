<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Mail\SendOtp;
use App\Sector;
use App\notification;
use Mail;
use DB;
use Hash;
use Storage;
use Config;
use App\Academy_Info;
use App\topic;


class AuthController extends Controller
{
    //signup
    public function signup(Request $r)
    {
        try {

            $check=DB::table('users')->where('mobile',$r->mobile)->first();
            $status='';

            if($this->check_count($check) > 0)      
            {
                if($check->verified == 'no')
                {
                    $status='update';
                }
                else {
                    return response()->json(['message'=>'Oops! Mobile Number or Email already exists.','status'=>'fail']);
                }            
            }
            else {
                $status='insert';
            }


            if($status != '')
            {
                $otp=rand(1111,9999);
                $name=$r->name;
                $type='verify';

                if($status== 'update')
                {
                    $insert=DB::table('users')->where('id',$check->id)->update(
                        ['name' =>$r->name, 'email' =>$r->email,'password'=>bcrypt($r->password),'state'=>$r->state,'sms_otp'=>$otp,
                        'referralcode'=>$r->ref_code]
                    );
                }
                else if($status == 'insert')
                {
                    $insert=DB::table('users')->insert(
                        ['name' =>$r->name, 'email' =>$r->email,'password'=>bcrypt($r->password),'mobile'=>$r->mobile,'state'=>$r->state,'sms_otp'=>$otp
                        ,'referralcode'=>$r->ref_code]
                    );
                }	
                else {
                    return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
                }

                if($insert)
                {
                    //sms service.
                    $this->sms($r->mobile,$otp);

                    //update player_id
                    if($r->player_id != '' || $r->device_id != '')
                    {
                        $playerinsert=DB::table('users')->where('mobile',$r->mobile)->update(['gcmid' =>$r->player_id, 'registration_id' =>$r->device_id]);
                    }

                    $data=DB::table('users')->where('mobile',$r->mobile)->first();
                    if($data->referralcode != '')
                    {
                        $school=DB::table('admins')->where('ref_code',$data->referralcode)->first();
                        if($school)
                        {
                            $data->school=$school->name;
                        }
                    }
                    
                    return response()->json(['data'=>$data,'message'=>'OTP has been sent to your mobile number '.$r->mobile,'status'=>'success']);
                }
                else {
                    return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
                }	
            }
            else
            {
                return response()->json(['message'=>'Something went wrong,Please try again later.','status'=>'fail']);
            }
        } catch (Exception $e) {

            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        }
    }

    //verify
    public function verify(Request $r)
    {
        $check=DB::table('users')->where('id',$r->id)->first();
        if($check)
        {
            if($check->verified == 'yes')
            {
                return response()->json(['message'=>'Sorry,account is already verified','status'=>'fail']);
            }
            else
            {
                //OTP match
                if($check->sms_otp != $r->otp)
                {
                    return response()->json(['message'=>'Incorrect OTP,Please enter correct otp.','status'=>'fail']);
                }

                $insert=DB::table('users')->where('id',$check->id)->update( ['verified' =>'yes'] );
                if($insert)
                {
                    $data=DB::table('users')->where('id',$r->id)->first();
                    if($data->referralcode != '')
                    {
                        $school=DB::table('admins')->where('ref_code',$data->referralcode)->first();
                        if($school)
                        {
                            $data->school=$school->name;
                        }
                    }
                    
                    return response()->json(['data'=>$data,'message'=>'Your account has been verified successfully','status'=>'success']);
                }
                else
                {
                    return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
                }

            }
        }
        else
        { 
           return response()->json(['message'=>'Invalid mobile number,Please enter valid mobile number.','status'=>'fail']);
        }
    }


    //login
    public function login(Request $r)
    {
        $check=DB::table('users')->where('mobile',$r->mobile)->where('check',0)->first();
        if($check)
        {
            if($check->block_status == 'block')
            {
                return response()->json(['message'=>'Sorry your account is temporarily locked .Please contact with administrator.','status'=>'fail']);
 
            }

            if($check->verified == 'yes')
            {
                $pass=$check->password;
                $password=$r->password;

                if (Hash::check($password,$pass))
                {
                    if($check->referralcode != '')
                    {
                        $school=DB::table('admins')->where('ref_code',$check->referralcode)->first();
                        if($school)
                        {
                            $check->school=$school->name;
                        }
                    }

                    return response()->json(['data'=>$check,'message'=>'Successfully loggin','status'=>'success']);
                }
                else
                {
                    return response()->json(['message'=>'Invalid password','status'=>'fail']);
                }
            }
            else
            {
                return response()->json(['message'=>'Sorry,account is not verified','status'=>'fail']);
            }
        }
        else
        { 
           return response()->json(['message'=>'Invalid mobile number,Please enter valid mobile number.','status'=>'fail']);
        }
    }

    public function get_course(Request $r)
    {
        $banner_id='';
        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('role','school')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                // $banner_id=$check->id;
                // $data= DB::table('new_courses')->where('publish','true')->where('school_id',$check->id) ->OrderBy('name','ASC')->get();

                $banner_id=$check->id ;
                $data = DB::table('new_courses')
                ->join('sectors', 'new_courses.sector', '=', 'sectors.id')
                ->select('new_courses.*', 'sectors.name as sector','sectors.id as sector_id')
                ->where('new_courses.publish','true')   
                ->whereIn('new_courses.id',json_decode($check->course_id))       
                ->orderBy('new_courses.name','ASC')
                ->get();

                // $data= DB::table('new_courses')->where('publish','true')->whereIn('id',json_decode($check->course_id)) ->OrderBy('name','ASC')->get();
            }
            else
            {
                $check=DB::table('admins')->where('role','admin')->first();
                $banner_id=$check->id;

                $data = DB::table('new_courses')
                ->join('sectors', 'new_courses.sector', '=', 'sectors.id')
                ->select('new_courses.*', 'sectors.name as sector','sectors.id as sector_id')
                ->where('new_courses.publish','true')   
                ->where('new_courses.school_id',$banner_id)      
                ->orderBy('new_courses.name','ASC')
                ->get();
                // $data= DB::table('new_courses')->where('publish','true')->where('school_id',$banner_id) ->OrderBy('name','ASC')->get(); 
            }
        }
        else
        {
            $check=DB::table('admins')->where('role','admin')->first();
            $banner_id=$check->id;

            $data = DB::table('new_courses')
            ->join('sectors', 'new_courses.sector', '=', 'sectors.id')
            ->select('new_courses.*', 'sectors.name as sector','sectors.id as sector_id')
            ->where('new_courses.publish','true')   
            ->where('new_courses.school_id',$banner_id)      
            ->orderBy('new_courses.name','ASC')
            ->get();

            // $data= DB::table('new_courses')->where('publish','true')->where('school_id',$banner_id) ->OrderBy('name','ASC')->get();
        }

        $banner=DB::table('banner')->where('group_id',$banner_id)->OrderBy('id','DESC')->get();

        if($this->check_count($data) > 0)
        {
            foreach($data as $d)
            {
                $subject=json_decode($d->subject);
                $sub=DB::table('subject')->select('id','name','image') ->whereIn('id',$subject)->get();
                if($this->check_count($data) > 0)
                {
                    $d->school_detail=$sub;
                }
                else
                {
                    $d->school_detail=[];
                }

                //access
                $mobile=DB::table('users')->select('mobile')->where('id',$r->user_id)->first();
                $course_asses=DB::table('course_access')->where('mobile',$mobile->mobile)->where('course_id',$d->id )->first();
                if($course_asses)
                {
                    $d->access='true';
                }
                else
                {
                    $d->access='false';                                      
                }                              

            }

            return response()->json(['data'=>$data,'banner'=>$banner,'message'=>'Record fetching','status'=>'success']); 
        }   
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }

    }


    public function get_sector(Request $r)
    {
        $banner_id='';
        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('role','school')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $banner_id=$check->id ;               
            }
            else
            {
                $check=DB::table('admins')->where('role','admin')->first();
                $banner_id=$check->id;
            }
        }
        else
        {
            $check=DB::table('admins')->where('role','admin')->first();
            $banner_id=$check->id;
        }

        // $banner=DB::table('banner')->where('group_id',$banner_id)->OrderBy('id','DESC')->get();
        $ref_code=explode(" ",$r->ref_code);

        $banner=DB::table('banner')->where(function ($q) use($ref_code) {
            foreach($ref_code as $key => $ord)
            {
                if($key == 0)
                {
                    $q->whereRaw('json_contains(refcode, \'["'.$ord.'"]\')');    
                }
                else 
                {
                    $q->orWhereRaw('json_contains(refcode, \'["'.$ord.'"]\')');    
                }
            }
        })->orwhere('group_id',$banner_id)->OrderBy('id','DESC')->get();

        $data=Sector::OrderBy('id','ASC')->get();
        if($this->check_count($data) > 0)
        {
            return response()->json(['data'=>$data,'banner'=>$banner,'message'=>'Record fetching','status'=>'success']); 
        }
        else 
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }

    }

    public function get_sector2(Request $r)
    {  
        

        $banner_id='';
        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('role','school')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $banner_id=$check->id ;               
            }
            else
            {
                $check=DB::table('admins')->where('role','admin')->first();
                $banner_id=$check->id;
            }
        }
        else
        {
            $check=DB::table('admins')->where('role','admin')->first();
            $banner_id=$check->id;
        }

        $ref_code=explode(" ",$r->ref_code);

        $banner=DB::table('banner')->where(function ($q) use($ref_code) {
            foreach($ref_code as $key => $ord)
            {
                if($key == 0)
                {
                    $q->whereRaw('json_contains(refcode, \'["'.$ord.'"]\')');    
                }
                else 
                {
                    $q->orWhereRaw('json_contains(refcode, \'["'.$ord.'"]\')');    
                }
            }
        })->orwhere('group_id',$banner_id)->OrderBy('id','DESC')->get();

        $data=Sector::OrderBy('name','ASC')->get();
        if($this->check_count($data) > 0)
        {
            return response()->json(['data'=>$data,'banner'=>$banner,'message'=>'Record fetching','status'=>'success']); 
        }
        else 
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }
        

        die;
        $content=$r->content;
        $data=array("type"=>5,"name"=>'live_event',"id"=>1,"link"=>'');
        $type=$r->type;
        $player_id=["f4c3fee1-6a16-4b12-9169-1d8d3c40e842"];

        $message = array("en" =>$content);
		$fields = array( 
					'app_id' => '43729d91-558f-4c77-b1b7-c355d663afdd', 
					'priority'=>'high',
					'contents' => $message,
					'data' => $data,
				); 
		
		if($type == 'tags')
		{
			$fields['tags'] = $player_id;
		}
		
		elseif ($type == 'player_id') 
		{
			$fields['include_player_ids'] = $player_id;
		}	
		else 
		{
			$fields['included_segments'] = array('Active Users');
		}
		if (array_key_exists("image_url",$data))
		{			
			$fields['big_picture'] = $data['image_url'];				
		}
		
		$fields = json_encode($fields);
        // print("\nJSON sent:\n");
			// print($fields);	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
					'Authorization: Basic NmEwYjkxNjYtNGJlYy00MGQ5LTg2NGItYTAwOTNiNThjMTZm'));
	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	
		$response = curl_exec($ch);
		curl_close($ch);

        return response()->json(['message'=>'Record not found.','status'=>$fields]);

    }


    public function get_course2(Request $r)
    {        
        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('role','school')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $data = DB::table('new_courses')
                ->join('sectors', 'new_courses.sector', '=', 'sectors.id')
                ->select('new_courses.*', 'sectors.name as sector','sectors.id as sector_id')
                ->where('new_courses.sector',$r->sector_id)
                ->where('new_courses.publish','true')   
                ->whereIn('new_courses.id',json_decode($check->course_id))       
                ->orderBy('new_courses.position','ASC')
                ->get();
            }
            else
            {
                $check=DB::table('admins')->where('role','admin')->first();
                $banner_id=$check->id;

                $data = DB::table('new_courses')
                ->join('sectors', 'new_courses.sector', '=', 'sectors.id')
                ->select('new_courses.*', 'sectors.name as sector','sectors.id as sector_id')
                ->where('new_courses.sector',$r->sector_id)
                ->where('new_courses.publish','true')   
                ->where('new_courses.school_id',$banner_id)      
                ->orderBy('new_courses.position','ASC')
                ->get();
            }
        }
        else
        {
            $check=DB::table('admins')->where('role','admin')->first();
            $banner_id=$check->id;

            $data = DB::table('new_courses')
            ->join('sectors', 'new_courses.sector', '=', 'sectors.id')
            ->select('new_courses.*', 'sectors.name as sector','sectors.id as sector_id')
            ->where('new_courses.sector',$r->sector_id)
            ->where('new_courses.publish','true')   
            ->where('new_courses.school_id',$banner_id)      
            ->orderBy('new_courses.position','ASC')
            ->get();
        }

        if($this->check_count($data) > 0)
        {
            foreach($data as $d)
            {
                $subject=json_decode($d->subject);
                $sub=DB::table('subject')->select('id','name','image') ->whereIn('id',$subject)->get();
                if($this->check_count($data) > 0)
                {
                    $d->school_detail=$sub;
                }
                else
                {
                    $d->school_detail=[];
                }

                //access
                $mobile=DB::table('users')->select('mobile')->where('id',$r->user_id)->first();
                $course_asses=DB::table('course_access')->where('mobile',$mobile->mobile)->where('course_id',$d->id )->where('type','course')->first();
                if($course_asses)
                {
                    $d->access='true';
                }
                else
                {
                    $course_ids=[$d->id];
                    $d->access='false'; 

                    $get_testSeries=DB::table('test_series')->where(function ($q) use($course_ids) {
                        foreach($course_ids as $key => $ord)
                        {
                            if($key == 0)
                            {
                                $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                            }
                            else 
                            {
                                $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                            }
                        }
                    })->get();
                    if($this->check_count($get_testSeries) > 0 ){

                        foreach($get_testSeries as $key=>$value){

                            $testCheck=DB::table('course_access')->where('mobile',$mobile->mobile)->where('course_id',$value->id )->where('type','test_series')->first();
                            if($this->check_count($testCheck) > 0 ){
                                $d->access='true'; 
                                break;
                            }
                        }                            
                    }
                                                       
                }  
            }

            return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']); 
        }   
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }

    }

    public function get_event(Request $r)
    {
        if($r->user_id != '')
        {
            if($r->ref_code != '')
            {
                $check=DB::table('admins')->where('ref_code',$r->ref_code)->first();
                if($check)
                {
                    $data = DB::table('live_event')
                        ->join('new_courses', 'live_event.course_id', '=', 'new_courses.id')
                        ->select('live_event.*', 'new_courses.name as course name')
                        ->where('live_event.uid',$check->email)->where('live_event.publish','true')->where('live_event.access','public')
                        ->OrderBy('live_event.id','DESC')
                        ->get();
                }
                else
                {
                    $admin=DB::table('admins')->where('role','admin')->first();
                    $data = DB::table('live_event')
                        ->join('new_courses', 'live_event.course_id', '=', 'new_courses.id')
                        ->select('live_event.*', 'new_courses.name as course name')
                        ->where('live_event.uid',$admin->email)->where('live_event.publish','true')->where('live_event.access','public')
                        ->OrderBy('live_event.id','DESC')
                        ->get();
                }
            }
            else
            {
                $admin=DB::table('admins')->where('role','admin')->first();
                $data = DB::table('live_event')
                    ->join('new_courses', 'live_event.course_id', '=', 'new_courses.id')
                    ->select('live_event.*', 'new_courses.name as course name')
                    ->where('live_event.uid',$admin->email)->where('live_event.publish','true')->where('live_event.access','public')
                    ->OrderBy('live_event.id','DESC')
                    ->get();
            }            
            
            if($this->check_count($data) > 0)
            {
                return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']); 
            }   
            else
            {
                return response()->json(['message'=>'Record not found.','status'=>'fail']);
            }
        }
        else
        {
            return response()->json(['message'=>'Sorry,UserID can not be empty.','status'=>'fail']);
        }
            
    }

    public function forgot_password(Request $r)
    {
        $check=DB::table('users')->where('mobile',$r->mobile)->first();
        if($check)
        {
            if($check->verified == 'yes')
            {
                $otp=rand(1111,9999);

                //sms service.
                $this->sms($r->mobile,$otp);

                $insert=DB::table('users')->where('id',$check->id)->update(['sms_otp'=>$otp]);
                return response()->json(['otp'=>$otp,'message'=>'OTP has been sent successfully,please verify OTP.','status'=>'success']);
            }
            else
            {
                return response()->json(['message'=>'Sorry,account is not verified','status'=>'fail']);
            }
        } 
        else
        {
            return response()->json(['message'=>'Sorry, Invalid mobile number, Please enter a valid mobile number.','status'=>'fail']);
        }
    }

    public function reset(Request $r)
    {
        
        $check=DB::table('users')->where('mobile',$r->mobile)->first();        
        if($check)
        {
            $insert=DB::table('users')->where('id',$check->id)->update(['password'=>bcrypt($r->password)]);

            if($insert)
            {
                $data=DB::table('users')->where('id',$check->id)->first();
                return response()->json(['data'=>$data,'message'=>'Thank-you your passoword has been reset successfully.','status'=>'success']);
            }
            else
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
        }
        else
        {
            return response()->json(['message'=>'Invalid email address,Please enter valid email address.','status'=>'fail']);
        }

    }

    public function verify_otp(Request $r)
    {
        $check=DB::table('users')->where('mobile',$r->mobile)->first();
        if($check)
        {
            
            //OTP match
            if($check->sms_otp != $r->otp)
            {
                return response()->json(['message'=>'Incorrect OTP,Please enter correct otp.','status'=>'fail']);
            }

            return response()->json(['message'=>'Otp has been verified please reset your password','status'=>'success']);
            
        }
        else
        { 
           return response()->json(['message'=>'Invalid mobile number,Please enter valid mobile number.','status'=>'fail']);
        }
    }

    public function check_refcode(Request $r)
    {
        if($r->ref_code)
        {
            $check=DB::table('admins')->where('role','school')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                return response()->json(['message'=>'Referral code verified successfully.','status'=>'success']);
            }
            else
            {
                return response()->json(['message'=>'Opps! Invalid  Referral code.','status'=>'fail']);
            }
        }
        else
        {
            return response()->json(['message'=>'Referral Code could not be empty.','status'=>'fail']);
        }
        
    }

    public function get_video(Request $r)
    {
        $school_id='';
        $video_not=array();
        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $school_id= $check->id;
                $video_not=DB::table('videoschool')->where('school_id',$school_id)->get()->pluck('video_id');
            }
        }
        
        $admin=DB::table('admins')->where('role','admin')->first();
        $admin_id= $admin->id;
        

        $course=DB::table('new_courses')->where('name',$r->course_id)
        ->where('school_id',$school_id)
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->course_id);
        })->get()->pluck('id');

       
        $subject=DB::table('subject')->where('name',$r->subject_id)
        ->where('school_id',$school_id)        
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->subject_id);
        })->get()->pluck('id');

        // $course=DB::table('new_courses')->where('name',$r->course_id)
        // ->where('school_id',$school_id)->get()->pluck('id');

        // $subject=DB::table('subject')->where('name',$r->subject_id)
        // ->where('school_id',$school_id)->get()->pluck('id');

        $data=DB::table('video')->where('publish','true')->where('video','!=','')->whereNotIn('id',$video_not)
        ->where(function ($q) use($course) {
            foreach($course as $key => $ord)
            {
                if($key == 0)
                {
                    $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
                else 
                {
                    $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
            }
        })
        ->where(function ($w) use($subject) {
            foreach($subject as $keys => $ords)
            {
                if($keys == 0)
                {
                    $w->whereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                }
                else 
                {
                    $w->orWhereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                }
            }
        })->get();

        if($this->check_count($data) > 0)
        {
            $mobile=DB::table('users')->select('mobile')->where('id',$r->user_id)->first();
            foreach($data as $d)
            {
                $course_asses=DB::table('course_access')->where('mobile',$mobile->mobile)->whereIn('course_id',json_decode($d->course_id) )->first();
                if($course_asses)
                {
                    $d->access='true';
                }
                else
                {
                    $d->access='false';                                      
                }
            }

            return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']); 
        }   
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }
    }

    


    public function vimeo_video_get(Request $r)
    {
        $school_id='';
        $videoIn=array();

        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $school_id= $check->id;
                // $video_not=DB::table('videoschool')->where('school_id',$school_id)->get()->pluck('video_id');
                $videoIn=DB::table('video_school_access')->where('school_id',$school_id)->get()->pluck('video_id');            
            }
        }

        $admin=DB::table('admins')->where('role','admin')->first();
        $admin_id= $admin->id;        

        $course=DB::table('new_courses')->where('name',$r->course_id)
        ->where('school_id',$school_id)
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->course_id);
        })->get()->pluck('id');

       
        $subject=DB::table('subject')->where('name',$r->subject_id)
        ->where('school_id',$school_id)        
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->subject_id);
        })->get()->pluck('id');

        $data=DB::table('video')->where('publish','true')->select('id','course_id','subject_id','name','video','vimeo','thumbnail','type','position','access')
        ->whereNotNull('thumbnail');
        
        if($r->ref_code != '')
        {
            $data=$data->whereIn('id',$videoIn);
        }
           

        $data=$data->where(function ($q) use($course) {
            foreach($course as $key => $ord)
            {
                if($key == 0)
                {
                    $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
                else 
                {
                    $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
            }
        })->where(function ($w) use($subject) {
            foreach($subject as $keys => $ords)
            {
                if($keys == 0)
                {
                    $w->whereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                }
                else 
                {
                    $w->orWhereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                }
            }
        })->OrderBy('position','ASC')->get();

        if($this->check_count($data) > 0)
        {
            $mobile=DB::table('users')->select('mobile')->where('id',$r->user_id)->first();
            foreach($data as $d)
            {
                $course_ids=json_decode($d->course_id);
                $d->access_mode=$d->access; 

                if($d->access == 'private'){
                    $course_asses=DB::table('course_access')->where('mobile',$mobile->mobile)
                    // ->whereIn('course_id', $course_ids)
                    ->where('course_name',$r->course_id)
                    ->get();
                    
                    if($this->check_count($course_asses) > 0 )
                    {
                        $d->access='true';
                    }
                    else
                    {
                        $d->access='false';                                      
                    }
                }
                else {
                    $d->access='true';
                }
                
            }

            return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']); 
        }   
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }
    }


    //GEt Viemo video with pagination
    public function vimeo_video_get_2(Request $r)
    {
        $school_id='';
        $videoIn=array();

        $skip=0;
        if($r->index != '')
        {
            $skip=$r->index * 20;
        }

        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $school_id= $check->id;
                // $video_not=DB::table('videoschool')->where('school_id',$school_id)->get()->pluck('video_id');
                $videoIn=DB::table('video_school_access')->where('school_id',$school_id)->get()->pluck('video_id');            
            }
        }

        $admin=DB::table('admins')->where('role','admin')->first();
        $admin_id= $admin->id;        

        $course=DB::table('new_courses')->where('name',$r->course_id)
        ->where('school_id',$school_id)
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->course_id);
        })->get()->pluck('id');

       
        $subject=DB::table('subject')->where('name',$r->subject_id)
        ->where('school_id',$school_id)        
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->subject_id);
        })->get()->pluck('id');

        // $data=DB::table('video')->where('publish','true')->select('id','course_id','subject_id','name','video','vimeo','thumbnail','type','position','access','duration')
        // ->whereNotNull('thumbnail');
        $data=DB::table('video')->select('id','course_id','subject_id','name','video','vimeo','thumbnail','type','position','access','duration')
        ->whereNotNull('thumbnail');
        
        if($r->ref_code != '')
        {
            $data=$data->whereIn('id',$videoIn);
        }
           

        $data=$data->where(function ($q) use($course) {
            foreach($course as $key => $ord)
            {
                if($key == 0)
                {
                    $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
                else 
                {
                    $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
            }
        })->where(function ($w) use($subject) {
            foreach($subject as $keys => $ords)
            {
                if($keys == 0)
                {
                    $w->whereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                }
                else 
                {
                    $w->orWhereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                }
            }
        });
        
        if( $r->topic_id !='' )
        {            
            $data=$data->whereRaw('json_contains(topic, \'["'.$r->topic_id.'"]\')');
        }

        $data=$data->OrderBy('position','ASC')->skip($skip)->take(20)->get();

        if($this->check_count($data) > 0)
        {
            $mobile=DB::table('users')->select('mobile')->where('id',$r->user_id)->first();
            foreach($data as $d)
            {
                $course_ids=json_decode($d->course_id);
               
                $d->access_mode=$d->access; 

                //Check Access
                if($d->access == 'private'){
                    $course_asses=DB::table('course_access')->where('mobile',$mobile->mobile)                   
                    ->where('course_name',$r->course_id)
                    ->where('type','course')
                    ->get();
                    
                    if($this->check_count($course_asses) > 0 )
                    {
                        $d->access='true';
                    }
                    else
                    {                        
                        $d->access='false'; 
                        $get_testSeries=DB::table('test_series')->where(function ($q) use($course_ids) {
                            foreach($course_ids as $key => $ord)
                            {
                                if($key == 0)
                                {
                                    $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                                }
                                else 
                                {
                                    $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                                }
                            }
                        })->get();
                        if($this->check_count($get_testSeries) > 0 ){

                            foreach($get_testSeries as $key=>$value){

                                $testCheck=DB::table('course_access')->where('mobile',$mobile->mobile)->where('course_id',$value->id )->where('type','test_series')->first();
                                if($this->check_count($testCheck) > 0 ){
                                    $d->access='true'; 
                                    break;
                                }
                            }                            
                        }                                                            
                    }
                }
                else {
                    $d->access='true';
                }
                
            }

            return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']); 
        }   
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }
    }
    

    //GEt Viemo video with pagination
    public function demovimeo_video_get_2(Request $r)
    {
        $school_id='';
        $videoIn=array();

        $skip=0;
        if($r->index != '')
        {
            $skip=$r->index * 20;
        }

        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $school_id= $check->id;
                // $video_not=DB::table('videoschool')->where('school_id',$school_id)->get()->pluck('video_id');
                $videoIn=DB::table('video_school_access')->where('school_id',$school_id)->get()->pluck('video_id');            
            }
        }

        $admin=DB::table('admins')->where('role','admin')->first();
        $admin_id= $admin->id;        

        $course=DB::table('new_courses')->where('name',$r->course_id)
        ->where('school_id',$school_id)
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->course_id);
        })->get()->pluck('id');

      

        $subject=DB::table('subject')->where('name',$r->subject_id)
        ->where('school_id',$school_id)        
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->subject_id);
        })->get()->pluck('id');

        
        $data=DB::table('video')->where('publish','true')->select('id','course_id','subject_id','name','video','vimeo','thumbnail','type','position','access')
        ->whereNotNull('thumbnail');
        
        if($r->ref_code != '')
        {
            $data=$data->whereIn('id',$videoIn);
        }
           

        $data=$data->where(function ($q) use($course) {
            foreach($course as $key => $ord)
            {
                if($key == 0)
                {
                    $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
                else 
                {
                    $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
            }
        })->where(function ($w) use($subject) {
            foreach($subject as $keys => $ords)
            {
                if($keys == 0)
                {
                    $w->whereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                }
                else 
                {
                    $w->orWhereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                }
            }
        });
        
        if( $r->topic_id !='' )
        {            
            $data=$data->whereRaw('json_contains(topic, \'["'.$r->topic_id.'"]\')');
        }

        $data=$data->OrderBy('position','ASC')->skip($skip)->take(20)->get();

        if($this->check_count($data) > 0)
        {
            $mobile=DB::table('users')->select('mobile')->where('id',$r->user_id)->first();
            foreach($data as $d)
            {
                $course_ids=json_decode($d->course_id);
               
                $d->access_mode=$d->access; 

                //Check Access
                if($d->access == 'private'){
                    $course_asses=DB::table('course_access')->where('mobile',$mobile->mobile)                   
                    ->where('course_name',$r->course_id)
                    ->where('type','course')
                    ->get();
                    
                    if($this->check_count($course_asses) > 0 )
                    {
                        $d->access='true';
                    }
                    else
                    {                        
                        $d->access='false'; 
                        $get_testSeries=DB::table('test_series')->where(function ($q) use($course_ids) {
                            foreach($course_ids as $key => $ord)
                            {
                                if($key == 0)
                                {
                                    $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                                }
                                else 
                                {
                                    $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                                }
                            }
                        })->get();
                        if($this->check_count($get_testSeries) > 0 ){

                            foreach($get_testSeries as $key=>$value){

                                $testCheck=DB::table('course_access')->where('mobile',$mobile->mobile)->where('course_id',$value->id )->where('type','test_series')->first();
                                if($this->check_count($testCheck) > 0 ){
                                    $d->access='true'; 
                                    break;
                                }
                            }                            
                        }                                                            
                    }
                }
                else {
                    $d->access='true';
                }
                
            }

            return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']); 
        }   
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }
    }

    //vimeo_detail
    public function vimeo_detail(Request $r)
    {   
        if(!empty($r->video_id))
        {
            $types=["backend","vimeo"];
            $rand=rand(0,1); 

            if($types[$rand]=="vimeo")
            {
                $url="https://api.vimeo.com/me/videos/".$r->video_id."?fields=files";
                $check=$this->vimeo_curl($url);

                if($check)
                {
                    $response=$check['files'];
                }
                else 
                {
                    $data=DB::table('video')->where('vimeo',$r->video_id)->select('quality')->first();
                    $response = json_decode($data->quality, true);
                }
            }
            else 
            {
               try 
                {
                    $data=DB::table('video')->where('vimeo',$r->video_id)->select('quality')->first();
                    $response = json_decode($data->quality, true); 
                }
                catch (\Exception $e) 
                {
                    $url="https://api.vimeo.com/me/videos/".$r->video_id."?fields=files";
                    $check=$this->vimeo_curl($url);
                    $response=$check['files'];
                }
            }

            if($this->check_count($response) > 0)
            {
                return response()->json(['data'=>$response,'type'=>$types[$rand],'message'=>'successfully fetching quality','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Sorry,VideoID Quality not found','status'=>'fail']);
            }

        }
        else 
        {
            return response()->json(['message'=>'Sorry,VideoID can not be empty','status'=>'fail']);
        }

    }


    


    public function get_all_video(Request $r)
    {
        $school_id='';
        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $school_id= $check->id;
            }
            else
            {
                $admin=DB::table('admins')->where('role','admin')->first(); 
                $school_id= $admin->id;
            }
        }
        else
        {
            $admin=DB::table('admins')->where('role','admin')->first();
            $school_id= $admin->id;
        }
        
        $data=DB::table('video')
        ->join('new_courses', 'new_courses.id', '=', 'video.course_id')
        ->join('subject', 'subject.id', '=', 'video.subject_id')
        ->select('video.*','new_courses.name as course_name','subject.name as subject_name')
        ->where('video.school_id', $school_id)
        ->where('video.publish','true')
        ->where('video.type','aws')
        ->get();

        if($this->check_count($data) > 0)
        {
            return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']); 
        }   
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }
    }


    public function get_pdf(Request $r)
    {
        $school_id='';
        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $school_id= $check->id;
            }
        }

        $admin=DB::table('admins')->where('role','admin')->first();
        $admin_id= $admin->id;

        $course=DB::table('new_courses')->where('name',$r->course_id)
        ->where('school_id',$school_id)
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->course_id);
        })->get()->pluck('id')->toArray();

        $subject=DB::table('subject')->where('name',$r->subject_id)
        ->where('school_id',$school_id)        
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->subject_id);
        })->get()->pluck('id');

        //pagination
        $skip=0;
        if($r->index != '')
        {
            $skip=$r->index * 10;
        }

        $data=DB::table('common_table')->where('field_type',4)->where('access','public')
        ->where(function ($w) use($subject) {
            foreach($subject as $keys => $ords)
            {
                if($keys == 0)
                {
                    $w->whereRaw('json_contains(school, \'["'.$ords.'"]\')');    
                }
                else 
                {
                    $w->orWhereRaw('json_contains(school, \'["'.$ords.'"]\')');    
                }
            }
        })->skip($skip)->take(10)->get();

        if($this->check_count($data) > 0)
        {
            $mobile=DB::table('users')->select('mobile')->where('id',$r->user_id)->first();
            foreach($data as $key => $d)
            {
                $course_asses=DB::table('course_access')->where('mobile',$mobile->mobile)->where('course_name',$r->course_id )->first();
                if($course_asses)
                {
                    $d->access='true';
                }
                else
                {
                    $d->access='false';                                      
                }

                //like-dislike status
                $d->likestatus='dislike';
                $status=DB::table('like_on_comment')->select('status')->where('email',$r->email)->where('commentid',$d->id)->first();
                if($status)
                {
                    $d->likestatus=$status->status;
                }

                if($course != '')
                {
                    $group_array=explode(",",$d->groupid);
                    $results=array_intersect($course,$group_array);   
                   
                    if (!$results && !in_array('all', $group_array))
                    {
                        unset($data[$key]);
                    }
                }

            }            

            if($this->check_count($data) > 0){
                return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']); 
            }
            else {
                return response()->json(['message'=>'Record not found.','status'=>'fail']);
            }
            
        }   
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }

    }

    public function get_pdf2 (Request $r)
    {
        $school_id='';
        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $school_id= $check->id;
            }
        }

        $admin=DB::table('admins')->where('role','admin')->first();
        $admin_id= $admin->id;

        $course=DB::table('new_courses')->where('name',$r->course_id)
        ->where('school_id',$school_id)
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->course_id);
        })->get()->pluck('id');

        $subject=DB::table('subject')->where('name',$r->subject_id)
        ->where('school_id',$school_id)        
        ->orWhere(function($query) use ($r,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $r->subject_id);
        })->get()->pluck('id');

        //pagination
        $skip=0;
        if($r->index != '')
        {
            $skip=$r->index * 20;
        }

        // $data=DB::table('common_table')->where('field_type',4)->where('access','public')
        $data=DB::table('common_table')->where('field_type',4)
        ->where(function ($q) use($course) {
            
            foreach($course as $keys1 => $ords1)
            {
                if($keys1 == 0)
                {
                    $q->whereRaw("find_in_set('".$ords1."',groupid)");
                }
                else {
                    $q->whereRaw("find_in_set('".$ords1."',groupid)");
                }
            }
        })
        ->where(function ($w) use($subject) {
            foreach($subject as $keys => $ords)
            {
                if($keys == 0)
                {
                    $w->whereRaw('json_contains(school, \'["'.$ords.'"]\')');    
                }
                else 
                {
                    $w->orWhereRaw('json_contains(school, \'["'.$ords.'"]\')');    
                }
            }
        });
        
        if( $r->topic_id !='' )
        {            
            $data=$data->whereRaw('json_contains(topic, \'["'.$r->topic_id.'"]\')');
        }

        $data=$data->orderBy('id','DESC')->skip($skip)->take(20)->get();

        if($this->check_count($data) > 0)
        {
            $mobile=DB::table('users')->select('mobile')->where('id',$r->user_id)->first();
            foreach($data as $key => $d)
            {
                if($d->access=='public')
                {
                    $d->access='true';
                }
                else
                {
                    $course_asses=DB::table('course_access')->where('mobile',$mobile->mobile)->where('course_name',$r->course_id)->first();
                    if($course_asses)
                    {
                        $d->access='true';
                    }
                    else
                    {
                        $d->access='false';                                      
                    }
                }
                //like-dislike status
                $d->likestatus='dislike';
                $status=DB::table('like_on_comment')->select('status')->where('email',$r->email)->where('commentid',$d->id)->first();
                if($status)
                {
                    $d->likestatus=$status->status;
                }
            }

            return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']);
            
        }   
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }

    }


    public function about(Request $r)
    {  
        $school_id='';
        if($r->ref_code != '')
        {
            $check=DB::table('admins')->where('ref_code',$r->ref_code)->first();
            if($check)
            {
                $school_id= $check->id;
            }
            else
            {
                $admin=DB::table('admins')->where('role','admin')->first(); 
                $school_id= $admin->id;
            }
        }
        else
        {
            $admin=DB::table('admins')->where('role','admin')->first();
            $school_id= $admin->id;
        }

        $data=DB::table('about')->where('school_id', $school_id)->first();

        if($this->check_count($data) > 0)
        {
            return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']); 
        }   
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']);
        }

    }

    //___________________________________VIDEO COMMENT_________________________

    public function insert_comment(Request $r)
    {
        if($r->user_id != '' && $r->video_id !='' && $r->comment !='')
        {
            $check=DB::table('video')->where('id',$r->video_id)->first();
            if($check)
            {
                $milliseconds = round(microtime(true) * 1000);
                $insert=DB::table('video_comment')->insert(['user_id' =>$r->user_id, 'video_id' =>$r->video_id,'comment'=>$r->comment,'timestamp'=>$milliseconds]);
                   
                if($insert)
                {
                    return response()->json(['message'=>'Comment has been sent successfully.','status'=>'success']);
                }
                else
                {
                    return response()->json(['message'=>'Something went wrong,Please try again later.','status'=>'fail']);
                }

            }
            else
            {
                return response()->json(['message'=>'Sorry,Video Record not found.','status'=>'fail']);
            }
        }   
        else
        {
            return response()->json(['message'=>'Sorry UserID,VideoID and comment can not be empty.','status'=>'fail']);
        }
    }

    public function get_comment(Request $r)
    {        
        if($r->video_id !='')
        {
            //pagination
            $skip=0;
            if($r->index != '')
            {
                $skip=$r->index * 10;
            }

            $data=DB::table('video_comment')
            ->join('users', 'users.id', '=', 'video_comment.user_id')
            ->select('video_comment.*','users.name as user_name','users.image as user_image')
            ->where('video_comment.video_id', $r->video_id)
            ->skip($skip)
            ->take(10)
            ->get();
    
            if($this->check_count($data) > 0)
            {
                foreach($data as $key=>$value){

                    $check=DB::table('comment_reply_post')->where('postid',$r->video_id)->where('comment_id',$value->id)->where('type','video')->get();
                    $value->reply=$check;
                }

                return response()->json(['data'=>$data,'message'=>'Record fetching','status'=>'success']); 
            }   
            else
            {
                return response()->json(['message'=>'Record not found.','status'=>'fail']);
            }
        }
        else
        {
            return response()->json(['message'=>'Sorry UserID,VideoID can not be empty.','status'=>'fail']);
        }
    }

    public function delete_comment(Request $r)
    {
        if($r->id !='')
        {
            $delete=DB::table('video_comment')->where('id',$r->id)->delete();
    
            if($delete)
            {
                return response()->json(['message'=>'Comment has been deleted successfully.','status'=>'success']); 
            }   
            else
            {
                return response()->json(['message'=>'Comment ID Record not found.','status'=>'fail']);
            }
        }
        else
        {
            return response()->json(['message'=>'Sorry Comment ID can not be empty.','status'=>'fail']);
        }
    }

    
    public function get_subject(Request $r)
    {   
        $check=DB::table('new_courses')->whereIn('id',$r->course)->get();
        if($this->check_count($check) > 0)
        {     
            $sub=array();      
            foreach($check as $c)
            {
                $sub=array_unique(array_merge(json_decode($c->subject),$sub));
            }

            $subject=DB::table('subject')->whereIn('id',$sub)->get();
            if($this->check_count($subject) > 0)
            {
                return response()->json(['data'=>$subject,'message'=>'Record fetching','status'=>'success']); 
            }   
            else
            {
                return response()->json(['message'=>'Sorry,Subject not found.','status'=>'fail']);
            }

        }   
        else
        {
            return response()->json(['message'=>'Sorry,Subject not found.','status'=>'fail']); 
        }
    }

    public function get_topics(Request $r)
    {
        try 
        {
            $course=$r->course_ids;
            $subject=$r->subject_ids;

            $check=topic::where(function ($q) use($course) {
                foreach($course as $key => $ord)
                {
                    if($key == 0)
                    {
                        $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                    }
                    else 
                    {
                        $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                    }
                }
            })
            ->where(function ($w) use($subject) {
                foreach($subject as $keys => $ords)
                {
                    if($keys == 0)
                    {
                        $w->whereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                    }
                    else 
                    {
                        $w->orWhereRaw('json_contains(subject_id, \'["'.$ords.'"]\')');    
                    }
                }
            })->orderBy('position','ASC')->get();

            if($this->check_count($check) > 0)
            {
                return response()->json(['data'=>$check,'message'=>'Successfully get topic record.','status'=>'success']); 
            }
            else 
            {
                return response()->json(['message'=>'Sorry,Topic Record not found.','status'=>'fail']); 
            }

        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        }
    }


    public function get_quality_video(Request $r)
    {
        if($r->id != '')
        {
            $check=DB::table('video')->where('id',$r->id)->first();
            if($check)
            {
                $str = explode('/', $check->video);
                $filename=end($str); 

                $f_name=explode('.', $filename);

                //Video quality
                $quality[]= $f_name[0].'_240.mp4';
                $quality[]= $f_name[0].'_360.mp4';
                $quality[]= $f_name[0].'_480.mp4';
                $quality[]= $f_name[0].'_720.mp4';

                $quality_urls=array();    
                
                $checked=Storage::disk('s3_transcoding')->has($f_name[0].'_240.mp4');
                if($checked)
                {
                    foreach($quality as $q)
                    {
                        $quality_url['quality']=$q;

                        $client = Storage::disk('s3_transcoding')->getDriver()->getAdapter()->getClient();
                        $bucket = Config::get('filesystems.disks.s3_transcoding.bucket');
                        $command = $client->getCommand('GetObject', [
                            'Bucket' => $bucket,
                            'Key' => $q
                        ]);
                        $request = $client->createPresignedRequest($command, '+20 minutes');
                        $presignedUrl = (string)$request->getUri();

                        $quality_url['url']=$presignedUrl;
                        $quality_urls[]=$quality_url;
                    }

                    $check->url=json_encode($quality_urls);                    
                    return response()->json(['data'=>$check,'message'=>'Record fetching','status'=>'success']); 
                }
                else
                {
                    return response()->json(['message'=>'Sorry,Video not found.','status'=>'fail']); 

                }                    
            }
            else
            {
                return response()->json(['message'=>'Sorry,Video not found.','status'=>'fail']); 
            }

        }
        else
        {
            return response()->json(['message'=>'Sorry,Video ID can not be empty.','status'=>'fail']); 
        }

    }
 

    function version_update(Request $r)
    {
        $check=DB::table('appversion')->first();
        if($check)
        {
            $insert=DB::table('appversion')->where('id',$check->id)->update(['version' =>$r->version, 'version_code' =>$r->version_code]);
            if($insert)
                {
                    $data=DB::table('appversion')->first();
                    return response()->json(['data'=>$data,'message'=>'updated.','status'=>'success']);
                }
                else
                {
                    return response()->json(['message'=>'Something went wrong,Please try again later.','status'=>'fail']);
                }
        }
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']); 
        }
    }

    public function transcoder(Request $r)
    {
        // $command="full.js 240,20200608110230,mp4,EduDream ";
        // $output = exec("cd /var/www/html/ffpmeg/ && node ".$command);
        // echo $output;

        $video=DB::table('video')->whereNull('quality')->get();
        if($this->check_count($video) > 0)
        {
            foreach($video as $v)
            {   
                $url=explode('/',$v->video); 
                $value=end($url);                 
                
                //node full.js 360,20200608110230,mp4,EduDream         
                print_r($value);
                break;
            }            
        }       
    }

    public function video_transfer(Request $r)
    {
        $video=DB::table('video')->whereNull('local_path')->get();
        if($this->check_count($video) > 0)
        {
            foreach($video as $v)
            {
                $url=explode('/',$v->video); 
                $video_key=end($url); 
                $exists = Storage::disk('s3')->has($video_key);
                if ($exists) 
                {   
                    try
                    {
                        $localFile = public_path('Video/').$video_key;  
                        copy($v->video, $localFile);

                        //update
                        $update=DB::table('video')->where('id',$v->id)->update(['local_path' =>$video_key]);
                    } 
                    catch (\Throwable $th) 
                    {
                        echo $th;
                    }
                }
            }
        }
    }

    public function video_transfer_quality(Request $r)
    {
        $video=DB::table('video')->whereNull('quality')->get();
        if($this->check_count($video) > 0)
        {
            foreach($video as $v)
            {
                $url=explode('/',$v->video); 
                $video_key=end($url); 
                $f_name=explode('.', $video_key);

                //Video quality
                $quality['240']= $f_name[0].'_240.mp4';
                $quality['360']= $f_name[0].'_360.mp4';
                $quality['480']= $f_name[0].'_480.mp4';
                $quality['720']= $f_name[0].'_720.mp4';
                $quality_urls=array();  
                
                foreach($quality as $key=>$q)
                {
                    $exists = Storage::disk('s3_transcoding')->has($q);
                    if ($exists) 
                    {   
                        $quality_url['quality']=$key;
                        $quality_url['url']=$q;

                        $client = Storage::disk('s3_transcoding')->getDriver()->getAdapter()->getClient();
                        $bucket = Config::get('filesystems.disks.s3_transcoding.bucket');
                        $command = $client->getCommand('GetObject', [
                            'Bucket' => $bucket,
                            'Key' => $q
                        ]);

                        $request = $client->createPresignedRequest($command, '+1 hour');
                        $presignedUrl = (string)$request->getUri();

                        try
                        {  
                            $localFile = public_path('Video/transcoder/').$q;  
                            copy($presignedUrl, $localFile);    
                            $quality_urls[]=$quality_url;                            
                        } 
                        catch (\Throwable $th) 
                        {
                            echo $th;
                        }
                    }
                } 

                //update
                $update=DB::table('video')->where('id',$v->id)->update(['quality' =>json_encode($quality_urls)]);
            }
        }
    }

    
    public function update_player(Request $r)
    {
        $check=DB::table('users')->where('id',$r->user_id)->first();
        
        if($this->check_count($check) > 0)
        {  
            $insert=DB::table('users')->where('id',$r->user_id)->update(['gcmid' =>$r->gcmid, 'registration_id' =>$r->registration_id]);

            try {

                return response()->json(['message'=>'Successfully updation.','status'=>'success']);               
                               
            } catch (Exception $e) {
                return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
            }
                
        }
        else
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']); 
        }
    }

    public function get_notify(Request $r)
    {
        if($r->ref_code != '')
        {
            $mode='school';
        }
        else 
        {
            $mode='edudream';
        }

        $notify=notification::where('mode',$mode)->Orwhere('mode','both')->get();
        if( $this->check_count($notify) > 0)
        {
            return response()->json(['data'=>$notify,'message'=>'Successfully get notification.','status'=>'success']); 
        }   
        else 
        {
            return response()->json(['message'=>'Record not found.','status'=>'fail']); 
        } 

    }

    public function get_topic(Request $r)
    {
        try 
        {
            $check=topic::whereRaw('json_contains(course_id, \'["'.$r->course_id.'"]\')')
            ->whereRaw('json_contains(subject_id, \'["'.$r->subject_id.'"]\')')->OrderBy('position','ASC')->get();
            
            if($r->ref_code != '')
            {
                $checked=DB::table('admins')->where('ref_code',$r->ref_code)->first();
                if($checked)
                {
                    $school_id= $checked->id;
                    $videoIn=DB::table('video_school_access')->where('school_id',$school_id)->get()->pluck('video_id');            
                }
            }

            if($this->check_count($check) > 0)
            {
                foreach ($check as $key => $value) 
                {                    
                   
                    $check_video=DB::table('video')->where('publish','true')->whereRaw('json_contains(course_id, \'["'.$r->course_id.'"]\')')
                    ->whereRaw('json_contains(subject_id, \'["'.$r->subject_id.'"]\')')->whereRaw('json_contains(topic, \'["'.$value->id.'"]\')');
                    
                    if($r->ref_code != ''){
                        $check_video=$check_video->whereIn('id',$videoIn);
                    }

                    $check_video=$check_video->count();

                    $value->pdf_count=0;
                    $value->video_count=$check_video;

                    $value->quiz_count=0;
                }

               

                return response()->json(['data'=>$check,'message'=>'Successfully get topic record.','status'=>'success']); 
            }
            else 
            {
                return response()->json(['message'=>'Sorry,Topic Record not found.','status'=>'fail']); 
            }

        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        }
    }



    public function notify(Request $r)
    {        
        try {

            $player[]=$r->player_id;
            $content=$r->message;

            $message = array("en" =>$content);
            $fields = array( 
                            'app_id' => '43729d91-558f-4c77-b1b7-c355d663afdd', 
                            'priority'=>'high',
                            'include_player_ids' =>$player,
                            'contents' => $message,
                        ); 

            $fields = json_encode($fields);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                        'Authorization: Basic NmEwYjkxNjYtNGJlYy00MGQ5LTg2NGItYTAwOTNiNThjMTZm'));
        
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
            $response = curl_exec($ch);
            curl_close($ch);
          
            return response()->json(['message'=>'Sent.','status'=>'success']); 
        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        }
    }
    

    //Add academy info
    public function add_info(Request $r){
        if (!empty($r->name) && !empty($r->mobile)) {
            $check=Academy_Info::where('mobile_no',$r->mobile)->count();
            if($check < 1){
                $insert = new Academy_Info();
                $insert->name=$r->name;
                $insert->mobile_no=$r->mobile;
                $insert->city=$r->city;
                $insert->address=$r->address;
                $insert->state=$r->state;
                $save=$insert->save();
                return response()->json(['message'=>'Academy info added successfully','status'=>'success','data'=>$insert]); 
            }
            else{
                return response()->json(['message'=>'Oops! Mobile Number already exist.','status'=>'fail']);
            }
        } 
        return response()->json(['message'=>'Param Missing','status'=>'fail']);
        
    }

    public function get_new_topic(Request $r)
    {
        try 
        {
            $check=topic::whereRaw('json_contains(course_id, \'["'.$r->course_id.'"]\')')
            ->whereRaw('json_contains(subject_id, \'["'.$r->subject_id.'"]\')')->OrderBy('position','ASC')->get();
            
            if($this->check_count($check) > 0)
            {
                foreach ($check as $key => $value) 
                {                    
                   
                    $check_video=DB::table('video')->where('publish','true')->whereRaw('json_contains(course_id, \'["'.$r->course_id.'"]\')')
                    ->whereRaw('json_contains(subject_id, \'["'.$r->subject_id.'"]\')')->whereRaw('json_contains(topic, \'["'.$value->id.'"]\')')->count();
                    
                    $value->pdf_count=0;
                    $value->video_count=$check_video;                    
                    $value->quiz_count=0;
                }

                return response()->json(['data'=>$check,'message'=>'Successfully get topic record.','status'=>'success']); 
            }
            else 
            {
                return response()->json(['message'=>'Sorry,Topic Record not found.','status'=>'fail']); 
            }

        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        }
    }

    public function get_new_topic2(Request $r)
    {
        try 
        {
            $check=topic::whereRaw('json_contains(course_id, \'["'.$r->course_id.'"]\')')
            ->whereRaw('json_contains(subject_id, \'["'.$r->subject_id.'"]\')')->OrderBy('position','ASC')->get();
           
            if($r->ref_code != '')
            {
                $checked=DB::table('admins')->where('ref_code',$r->ref_code)->first();
                if($checked)
                {
                    $school_id= $checked->id;
                    $videoIn=DB::table('video_school_access')->where('school_id',$school_id)->get()->pluck('video_id');            
                }
            }

            if($this->check_count($check) > 0)
            {
                foreach ($check as $key => $value) 
                {                    
                   
                    $check_video=DB::table('video')->where('publish','true')->whereRaw('json_contains(course_id, \'["'.$r->course_id.'"]\')')
                    ->whereRaw('json_contains(subject_id, \'["'.$r->subject_id.'"]\')')->whereRaw('json_contains(topic, \'["'.$value->id.'"]\')');
                   
                    if($r->ref_code != ''){
                        $check_video=$check_video->whereIn('id',$videoIn);
                    }

                    $check_video=$check_video->count();

                    $pdf=$this->get_count($r->course_name,$r->subject_name,$r->ref_code,$value->id);

                    $value->pdf_count=$pdf;
                    $value->video_count=$check_video;                    
                    $value->quiz_count=0;
                }

                return response()->json(['data'=>$check,'message'=>'Successfully get topic record.','status'=>'success']); 
            }
            else 
            {
                return response()->json(['message'=>'Sorry,Topic Record not found.','status'=>'fail']); 
            }

        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);            
        }
    }

    function get_count($course_name,$subject_name,$ref_code,$topic_id)
    {
        $school_id='';
        if($ref_code != '')
        {
            $check=DB::table('admins')->where('ref_code',$ref_code)->first();
            if($check)
            {
                $school_id= $check->id;
            }
        }

        $admin=DB::table('admins')->where('role','admin')->first();
        $admin_id= $admin->id;

        $course=DB::table('new_courses')->where('name',$course_name)
        ->where('school_id',$school_id)
        ->orWhere(function($query) use ($course_name,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $course_name);
        })->get()->pluck('id');

        

        $subject=DB::table('subject')->where('name',$subject_name)
        ->where('school_id',$school_id)        
        ->orWhere(function($query) use ($subject_name,$admin_id) {
            $query->where('school_id', $admin_id)
                  ->where('name', $subject_name);
        })->get()->pluck('id');

       

        
        $data=DB::table('common_table')->where('field_type',4)->where('access','public')
        ->where(function ($q) use($course) {
            
            foreach($course as $keys1 => $ords1)
            {
                if($keys1 == 0)
                {
                    $q->whereRaw("find_in_set('".$ords1."',groupid)");
                }
                else {
                    $q->whereRaw("find_in_set('".$ords1."',groupid)");
                }
            }
        })
        ->where(function ($w) use($subject) {
            foreach($subject as $keys => $ords)
            {
                if($keys == 0)
                {
                    $w->whereRaw('json_contains(school, \'["'.$ords.'"]\')');    
                }
                else 
                {
                    $w->orWhereRaw('json_contains(school, \'["'.$ords.'"]\')');    
                }
            }
        });

        if( $topic_id !='' )
        {            
            $data=$data->whereRaw('json_contains(topic, \'["'.$topic_id.'"]\')');
        }
        $data=$data->count();



        // $pdf=DB::table('common_table')->where('field_type',4)->where('access','public')
        // ->whereRaw("find_in_set('".$course_id."',groupid)")
        // ->whereRaw('json_contains(school, \'["'.$subject_id.'"]\')')
        // ->count(); 

        return $data;
    }

    public function get_test_series(Request $r){
        
        if(!empty($r->sector_id) && !empty($r->mobile)){

            $skip=0;
            if($r->index != '')
            {
                $skip=$r->index * 10;
            }

            $data=DB::table('test_series');
            if($r->sector_id != 'all'){
                $data=$data->where('sector_id',$r->sector_id);
            }            
            $data=$data->where('delete_status',1);
            
            //EDU member can see unpublished content too!
            $member=["7568956667","9664145105","6377572467","8950208444","9001210008"];

            if (!in_array($r->mobile, $member)){
                $data=$data->where('status','published');
            }

            if($r->ref_code != ''){
                $data=$data->where('ref_code',$r->ref_code);
            }else{
                $data=$data->where('ref_code','superadmin');
            }
            
            $data=$data->skip($skip)->take(10)->get();
           
            if($this->check_count($data) > 0)
            {
                foreach($data as $d){
                    $course_asses=DB::table('course_access')->where('mobile',$r->mobile)->where('course_id',$d->id )->first();
                    $d->test_count= DB::table('test_series')->where('sector_id',$d->sector_id)->count();
                    if($course_asses)
                    {
                        $d->access='true';
                    }
                    else
                    {
                        $d->access='false';                                      
                    }  
                }

                return response()->json(['data'=>$data,'message'=>'Successfully get test series.','status'=>'success']); 
            }
            else 
            {
                return response()->json(['message'=>'Sorry,Test Series  Record not found.','status'=>'fail']); 
            }

        }
        else{
            return response()->json(['message'=>'Param Missing','status'=>'fail']);
        }

    }

    //get test series test

    public function series_test_list(Request $r){

        if (!empty($r->series_id)) {

            $data=DB::table('test_series')->where('id',$r->series_id)->select('test_id')->first();

            $test_id=json_decode($data->test_id,true);
            
            $comment=DB::table('common_table')->where('status','published')->where('post_type','!=',0)
            ->whereIn('id',$test_id)->orderBy('id','desc')->get();
           
            if($this->check_count($comment) > 0){

                foreach ($comment as $key => $value) {    
                    
                    // Like Status
                        $value->likestatus="dislike";
                        $like_status=DB::table('like_on_comment')->select('status')->where('user_id',$r->user_id)->where('commentid',$value->id)->first();
                        if($like_status){
                            $value->likestatus=$like_status->status;
                        }
                    // End Like Status        

                }

                return response()->json(['data'=>$comment,'message'=>'Successfully Record fetching.','status'=>'success']);
            }
            else {
                return response()->json(['message'=>'Sorry, Record not found.','status'=>'fail']);
            }

        }
        else {
            return response()->json(['message'=>'Sorry, Record not found.','status'=>'fail']);
        }
    }

    public function top_courses(Request $r)
	{
        $check=DB::table('admins')->where('role','admin')->first();
        $banner_id=$check->id;

      
		$data=DB::table('order')->select(DB::raw('COUNT(id) as cnt', 'course_id'),'course_id')->groupBy('course_id')->orderBy('cnt', 'DESC')->get()->take(10);
        foreach ($data as $value) {
            
            $course = DB::table('new_courses')->where('id',$value->course_id)->first();           
            
            if($course)
            {
                $course->sector = DB::table('sectors')->where('id',$course->sector)->first();
                $value->course=$course;
                $subject=json_decode($course->subject);
                $value->course->sub=DB::table('subject')->select('id','name','image') ->whereIn('id',$subject)->get();
               
            }
            
            $course_asses=DB::table('course_access')->where('course_id',$value->course_id)->where('user_id',$r->user_id)->first();
            
            if($course_asses)
                    {
                        $value->school_name=DB::table('admins')->select('name')->where('id',$course_asses->school_id)->first();

                        $value->access='true';
                    }
                    else
                    {
                     $value->school_name=null;
                        $value->access='false';                                      
                    }  
        }
        if(count($data)>0)
        {
            return response()->json(['data'=>$data,'message'=>'Successfully Record fetching.','status'=>'success']);
        }
        else {
            return response()->json(['message'=>'Sorry, Record not found.','status'=>'fail']);
        }
	}

    // public function get_test(Request $r)
    // {   
       
    //     $course=$r->course;
        
        
    //     $check=DB::table('common_table')
    //     ->where(function($query) use ($course){
    //         foreach($course as $key=>$c) {
    //             if($key==0)
    //             $query->whereRaw("find_in_set('".$c."',groupid)");
    //             else
    //             $query->orwhereRaw("find_in_set('".$c."',groupid)");
    //         }
    //     })->where('field_type',2)->where('post_type',2)->select('jsondata','id','groupid')->get();
         
        
    //     $size =sizeof($check);

    //     if($size > 0){

    //         foreach($check as $d){
    //         $test=json_decode($d->jsondata,true);
    //         $d->name=$test['testname'];
    //         }    
            
    //         return response()->json(['data'=>$check,'message'=>'Record fetching','status'=>'success']); 
    //     }
    //     else{
    //         return response()->json(['message'=>'Sorry,Test not found.','status'=>'fail']); 
    //     }
            
        
    // }

}
