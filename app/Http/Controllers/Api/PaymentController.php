<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use DB;

class PaymentController extends Controller
{
    public function generate_token(Request $req)
    {
        //Insertion in DB
        $this->orderDate($req);

        $fields = array( 
            'orderId' => $req->order_id, 
            'orderAmount' =>$req->amount,
            "orderCurrency"=>"INR"
            ); 

        $fields = json_encode($fields);

        $app_Id=Config::get('services.pay.app_id');
        $scretkey=Config::get('services.pay.Secret_key');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.cashfree.com/api/v2/cftoken/order");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','x-client-id: '.$app_Id.' ','x-client-secret: '.$scretkey.' '));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
        $response = curl_exec($ch);
        curl_close($ch);
        
        
        return $response;
    }

    public function order_response(Request $r)
    {
        $check=DB::table('order')->where('order_id',$r->order_id)->first();
        if($check)
        {
            try {
                $insert=DB::table('order')->where('id',$check->id)->update(
                    ['transaction_id' =>$r->transaction_id, 'referral_ id' =>$r->referral_id,'status'=>$r->status]);

                if($insert){

                    if($r->status == 'SUCCESS'){
                        $course = DB::table('course_access')->insert(['school_id'=>'1','user_id' => $check->user_id,'mobile'=>$check->mobile,'course_id'=>$check->course_id,'course_name'=>$check->course_name,'type'=>$check->type,'ref_code'=>'online','created_at'=>date("YmdHis"),'updated_at'=>date("YmdHis")]);                       
                    }

                    return response()->json(['message'=>'Your transaction has been successfully completed.','status'=>'success']);         
                }
                else {
                    return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
                }
            } catch (Exception $e) {
                return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);
            }
        }
        else {
            return response()->json(['message'=>'Sorry,OrderId not found','status'=>'fail']);
        }
       
    }

    public function webhook(Request $r)
    {
        $check=DB::table('order')->where('order_id',$r->order_id)->first();
        if($check){

            if($check->status != $r->status)
            {
                $insert=DB::table('order')->where('id',$check->id)->update(['status'=>$r->status]);

                if($insert){

                    if($r->status == 'SUCCESS'){
                        $course = DB::table('course_access')->insert(['school_id'=>'1','user_id' => $check->user_id,'mobile'=>$check->mobile,'course_id'=>$check->course_id,'course_name'=>$check->course_name,'type'=>$check->type,'ref_code'=>'online','created_at'=>date("YmdHis"),'updated_at'=>date("YmdHis")]);                       
                    }

                    return response()->json(['message'=>'Your transaction has been successfully completed.','status'=>'success']);         
                }
                else {
                    return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
                }
            }
        }
        else {
             return response()->json(['message'=>'Sorry,OrderId not found','status'=>'fail']);
        }
    }

    public function cancel_payment(Request $r)
    {
        $check=DB::table('order')->where('order_id',$r->order_id)->first();
        if($check)
        {
            $delete=DB::table('order')->where('id',$check->id)->update(['status'=>'cancel']);
    
            if($delete)
            {
                return response()->json(['message'=>'Your transaction has been declined.','status'=>'success']); 
            }   
            else
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
        }
        else {
            return response()->json(['message'=>'Sorry,OrderId not found','status'=>'fail']);
        }
    }

    function orderDate($req)
    {
        try {
            
            $insert=DB::table('order')->insert(
                ['order_id'=>$req->order_id,'amount'=>$req->amount,'user_id'=>$req->user_id,'name' =>$req->name,'email' =>$req->email,
                'mobile'=>$req->mobile,'course_id'=>$req->course_id,'course_name'=>$req->course_name,'type'=>$req->type,
                'created_at'=>date("YmdHis"),'updated_at'=>date("YmdHis")]
            );
    
            if($insert)
            {
                return true;           
            }
            else {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
        } catch (Exception $e) {
            return response()->json(['message'=>$e->getMessage(),'status'=>'fail']);
        }
    }

    public function webhook_curl(Request $r)
    {

       //All pending Payments
       $payment=DB::table('order')->where('mobile','9413011835')->OrderBy('id','DESC')->get();
        
       foreach ($payment as $key => $value) {
           
           $orderId=$value->order_id;

           $curl = curl_init();
           curl_setopt_array($curl, array(
           CURLOPT_URL => "https://api.cashfree.com/api/v1/order/info/status",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS => "appId=6290878313f4f66475b40c98980926&secretKey=5fde76516ab3e55c5d0a4fc42c75e5c3b5fed4c8&orderId=".$orderId,
           CURLOPT_HTTPHEADER => array(
                   "cache-control: no-cache",
                   "content-type: application/x-www-form-urlencoded"
               ),
           ));

           $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

           if ($err) {}

           else 
           {
               $transaction=json_decode($response);

            
               if (array_key_exists('orderStatus',$transaction) && $transaction->status=='OK')
               {                
                   if (array_key_exists('txStatus',$transaction))
                   {
                   
                       if ($transaction->txStatus=='SUCCESS') 
                       {
                           date_default_timezone_set('Asia/Kolkata');
                           $date = date('YmdHis');

                           $status=$transaction->txStatus;

                           if($value->status != $status)
                           {
                               //update in DB Order table
                                $insert=DB::table('order')->where('id',$value->id)->update(['status'=>$status]);

                                //Add in course access
                                $assign=DB::table('course_access')->where('user_id',$value->user_id)->where('course_id',$value->course_id)->first();
                                if(!$assign)
                                {
                                    $course = DB::table('course_access')->insert(['school_id'=>'1','user_id' => $value->user_id,'mobile'=>$check->mobile,'course_id'=>$value->course_id,'course_name'=>$value->course_name,'ref_code'=>'online','created_at'=>$date,'updated_at'=>$date]);                       
                                } 
                           }

                       }
                   }
               }
           }

       }


    }


}
