<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TestQuestion;
use App\TopicSearch;

use App\block_list;
use App\time_record;
use App\comp_result;
use App\challege;
use App\User;
use App\points_earn;
use App\rank_level;

use DateTime;
use DateTimeZone;
use DB;

class ChallengeController extends Controller
{
    //------------------------- view challenge_result--------------------------------------//
    public function challenge_result(Request $r)
    {
        if(!empty($r->user_id) && !empty($r->challenge_id))
        {
            $get_chall=challege::where('id',$r->challenge_id)->first();
            if (sizeof($get_chall)>0) 
            {
                $get_user=DB::table('users')->where('email',$get_chall->userid)->first();
                $get_opponent=DB::table('users')->where('email',$get_chall->opponentid)->first();

                // Get both users name and image
                $get_chall['user_name']=$get_user->name;
                $get_chall['opponent_name']=$get_opponent->name;

                $get_chall['user_points']= $get_user->xp;
                $get_chall['opponent_points']= $get_opponent->xp;

                $get_chall['user_image']=$get_user->image;
                $get_chall['opponent_image']=$get_opponent->image;

                // Get correct answer match
                $user_correct=comp_result::where('student_id',$get_chall->userid)
                                            ->where('challenge_id',$r->challenge_id)
                                            ->where('ques_status','R')
                                            ->count();

                $opponent_correct=comp_result::where('student_id',$get_chall->opponentid)
                                            ->where('challenge_id',$r->challenge_id)
                                            ->where('ques_status','R')
                                            ->count();

                $get_chall['user_correct']=$user_correct;
                $get_chall['opponent_correct']=$opponent_correct;

                // Points of questions answered
                $bonus_ans=points_earn::where('challenge_id',$r->challenge_id)
                                        ->where('question_type','play')
                                        ->where('student_id',$r->user_id)
                                        ->whereNotNull('question_id')
                                        ->get();
                $bonus_points=0;
                if(sizeof($bonus_ans)>0)
                {
                    foreach ($bonus_ans as $bonus) 
                    {
                        $bonus_points=$bonus_points+$bonus->points;
                    }
                }

                $get_chall['bonus_points']=$bonus_points;

                // Points of challenge complete
                $match_points=0;
                $match_result=points_earn::where('challenge_id',$r->challenge_id)
                                        ->where('question_type','play')
                                        ->where('student_id',$r->user_id)
                                        ->whereNull('question_id')
                                        ->first();
                if (sizeof($match_result)>0) 
                {
                    $get_chall['match_result']=$match_result->points;
                    $match_points=$match_points+$match_result->points;
                }
                else
                {
                    $get_chall['match_result']=0;
                }

                    $get_chall['total_points']=$bonus_points+$match_points;
                

                //  Get question W/R history
                $questions=json_decode($get_chall->questions);
                $user_answers=array();
                $opponent_answers=array();
                foreach ($questions as $question) 
                {
                    $user_status=comp_result::where('student_id',$get_chall->userid)
                                            ->where('challenge_id',$r->challenge_id)
                                            ->where('ques_id',$question)
                                            ->first();
                    if(sizeof($user_status)>0)
                    {
                        $user_answers[]=$user_status->ques_status;
                    }
                    else
                    {
                        $user_answers[]='U';
                    }
                    
                    $opponent_status=comp_result::where('student_id',$get_chall->opponentid)
                                            ->where('challenge_id',$r->challenge_id)
                                            ->where('ques_id',$question)
                                            ->first();
                    if(sizeof($opponent_status)>0)
                    {
                        $opponent_answers[]=$opponent_status->ques_status;
                    }
                    else
                    {
                        $opponent_answers[]='U';
                    }
                }

                $answer_details['user']=$user_answers;
                $answer_details['opponent']=$opponent_answers;
                $get_chall['answers']=json_encode($answer_details);


                // Get ranks of both user/opponent
                $user_data=DB::table('users')->select('level')->where('email',$get_chall->userid)->first();
                $opponent_data=DB::table('users')->select('level')->where('email',$get_chall->opponentid)->first();

                $user_rank='';
                $opponent_rank='';

                // $rank_data=rank_level::get();
                // foreach ($rank_data as $ranks) 
                // {
                //     if($ranks->point>=$user_data->xp)
                //     {
                //         $user_rank=$ranks->name;
                //         break;
                //     }
                // }
                // foreach ($rank_data as $ranks) 
                // {
                //     if($ranks->point>=$opponent_data->xp)
                //     {
                //         $opponent_rank=$ranks->name;
                //         break;
                //     }
                // }

                $get_chall['user_rank']= $user_data->level;
                $get_chall['opponent_rank']= $opponent_data->level;
                
                // Get win status
                if($get_chall->winner_id=='DRAW')
                {
                    $get_chall['result_status']= 'DRAW';
                }
                elseif($r->user_id==$get_chall->winner_id)
                {
                    $get_chall['result_status']= 'You Won'; 
                }
                else
                {
                    $get_chall['result_status']= 'You Lost';
                }

                // Get totoal time user\opponent completed the challenge
                $user_time=0;
                $opponent_time=0;

                // Get user time
                $get_time1=comp_result::where('student_id',$get_chall->userid)
                                        ->where('challenge_id',$r->challenge_id)
                                        ->get();
                if(sizeof($get_time1)>0)
                {
                    foreach($get_time1 as $time1)
                    {
                        $user_time=$user_time+(int)$time1->time;
                    }
                }

                // Get opponent time
                $get_time2=comp_result::where('student_id',$get_chall->opponentid)
                                        ->where('challenge_id',$r->challenge_id)
                                        ->get();
                if(sizeof($get_time2)>0)
                {
                    foreach($get_time2 as $time2)
                    {
                        $opponent_time=$opponent_time+(int)$time2->time;
                    }
                }

                $get_chall['user_time']=$user_time;
                $get_chall['opponent_time']=$opponent_time;
                
                $msg['message']=$get_chall;
                $msg['status']='success';
                echo json_encode($msg);
            } 
            else 
            {
                $msg['message']='Challenge Not found!!';
                $msg['status']='fail';
                echo json_encode($msg);
            }
        }
        else
        {
            $msg['message']='Param Missing!!';
            $msg['status']='fail';
            echo json_encode($msg);
        }  
    }
    
    // //------------------------- challenge timeout--------------------------------------//
    // public function challenge_timeout(Request $r)
    // {
    //     if (!empty($r->challenge_id) && !empty($r->user_id) && !empty($r->time)) 
    //     {
    //         // get challenge details
    //         $get_chall=challege::where('id',$r->challenge_id)->first();

    //         // getting last question user answered
    //         $get_lastq=comp_result::where('challenge_id',$r->challenge_id)
    //                                 ->where('student_id',$r->user_id)
    //                                 ->orderBy('id','DESC')
    //                                 ->first();
    //         $questions=json_decode($get_chall->questions);

    //         $length=count($questions);

    //         // getting index of last question in question set
    //         if (sizeof($get_lastq)>0) 
    //         {
    //                 $index = array_search($get_lastq->ques_id, $questions);
    //                 $index=$index+1;
    //         } 
    //         else 
    //         {
    //             $index=0;
    //         }

    //         for ($i=$index; $i<$length; $i++) 
    //         {
    //             // Inserting questions as wrong answered
    //             $insert=new comp_result;
    //             $insert->student_id=$r->user_id;
    //             $insert->challenge_id=$r->challenge_id;
    //             $insert->ques_id=$questions[$i];
    //             $insert->ques_status='W';
    //             $insert->save();
    //         }

    //         // Checking if answering person is opponent or not  
    //         if ($r->user_id==$get_chall->opponentid) 
    //         {
    //             // check who is winner
    //             $user_correct_count=comp_result::where('student_id',$get_chall->userid)
    //                                                 ->where('challenge_id',$r->challenge_id)
    //                                                 ->where('ques_status','R')
    //                                                 ->count();
    //             $opponent_correct_count=comp_result::where('student_id',$get_chall->opponentid)
    //                                                 ->where('challenge_id',$r->challenge_id)
    //                                                 ->where('ques_status','R')
    //                                                 ->count();
    //             // give points to winner
    //             if($user_correct_count!=$opponent_correct_count)
    //             {
    //                 $insert_win_point=new points_earn;
    //                 $insert_win_point->challenge_id=$r->challenge_id;
    //                 $insert_win_point->question_type='play';

    //                 if ($user_correct_count>$opponent_correct_count) 
    //                 {
    //                     $insert_win_point->student_id=$get_chall->userid;

    //                     // Get user points and rank before updating for update level purpose
    //                     $get_xp=DB::table('users')->where('email',$get_chall->userid)->first();
    //                     $old_xp=$get_xp->xp;
    //                     $target_rank=0;
    //                     $level_id=null;
    //                     $level_name=null;

    //                     $rank_level=rank_level::get();
    //                     foreach($rank_level as $rank)
    //                     {
    //                         if($rank->point>$old_xp)
    //                         {
    //                             $target_rank=$rank->point;
    //                             $level_id=$rank->id;
    //                             $level_name=$rank->name;
    //                             break;
    //                         }
    //                     }

    //                     // Update User xp
    //                     $t_ap=$get_xp->xp+$get_chall->win_points;
    //                     $update=DB::table('users')->where('email',$get_chall->userid)->update(['xp'=>$t_ap]);


    //                     // Get user points and rank after updating for update level purpose
    //                     $user_point_after=DB::table('users')->where('email',$get_chall->userid)->first();
    //                     $new_xp=$user_point_after->xp;

    //                     if($new_xp>=$target_rank && $level_id!='')
    //                     {
    //                         // Store user's notify
    //                         $this->profile_update($get_chall->userid,$level_id,$level_name);
    //                     }

    //                     //update winner status in challenge
    //                     $update_winner=challege::find($r->challenge_id);
    //                     $update_winner->winner_id=$get_chall->userid;
    //                     $update_winner->save();
    //                 } 
    //                 else 
    //                 {
    //                     $insert_win_point->student_id=$get_chall->opponentid; 

    //                     // Get user points and rank before updating for update level purpose
    //                     $get_xp=DB::table('users')->where('email',$get_chall->opponentid)->first();
    //                     $old_xp=$get_xp->xp;
    //                     $target_rank=0;
    //                     $level_id=null;
    //                     $level_name=null;

    //                     $rank_level=rank_level::get();
    //                     foreach($rank_level as $rank)
    //                     {
    //                         if($rank->point>$old_xp)
    //                         {
    //                             $target_rank=$rank->point;
    //                             $level_id=$rank->id;
    //                             $level_name=$rank->name;
    //                             break;
    //                         }
    //                     }

    //                     // Update User xp
    //                     $t_ap=$get_xp->xp+$get_chall->win_points;
    //                     $update=DB::table('users')->where('email',$get_chall->opponentid)->update(['xp'=>$t_ap]);


    //                     // Get user points and rank after updating for update level purpose
    //                     $user_point_after=DB::table('users')->where('email',$get_chall->opponentid)->first();
    //                     $new_xp=$user_point_after->xp;

    //                     if($new_xp>=$target_rank && $level_id!='')
    //                     {
    //                         // Store user's notify
    //                         $this->profile_update($get_chall->opponentid,$level_id,$level_name);
    //                     }

    //                     //update winner status in challenge
    //                     $update_winner=challege::find($r->challenge_id);
    //                     $update_winner->winner_id=$get_chall->opponentid;
    //                     $update_winner->save();
    //                 }
    //                 $insert_win_point->points=$get_chall->win_points;
    //                 $insert_win_point->save();
    //             }
    //             else
    //             {
    //                 //update winner status in challenge
    //                 $update_winner=challege::find($r->challenge_id);
    //                 $update_winner->winner_id='DRAW';
    //                 $update_winner->save();
    //             }
    //         } 

    //         // update status that challenge completed
    //         $update_timerecord=new time_record;
    //         $update_timerecord->student_id=$r->user_id;
    //         $update_timerecord->challenge_id=$r->challenge_id;

    //         $update_timerecord->time=$r->time;
    //         $update_timerecord->save();

    //         if ($update_timerecord) 
    //         {
    //             $msg['status']='success';
    //             echo json_encode($msg);
    //         } 
    //         else 
    //         {
    //             $msg['data']='Fail to update user!!';
    //             $msg['status']='fail';
    //             echo json_encode($msg);
    //         }
    //     } 
    //     else 
    //     {
    //         $msg['message']='Param Missing!!';
    //         $msg['status']='fail';
    //         echo json_encode($msg);
    //     }
    // }

 //------------------------- Challage list with their status--------------------------------------//
    
    public function getChallenge(Request $r) {


    	
        if (!empty($r->user_id)) 
        {
            // Get blocked users
            $block_arr=array();
            $get_blocked=block_list::where('user_id',$r->user_id)->get();

            if(sizeof($get_blocked)>0)
            {
                foreach($get_blocked as $blocked)
                {
                    $block_arr[]=$blocked->opponent_id;
                }
            }

            // print_r($block_arr);

            // Get user created challenges
            $from_user_id=challege::select("id")->whereNotIn('userid',$block_arr)->whereNotIn('opponentid',$block_arr)->where('play_status','>',0)->where('opponentid',$r->user_id)->orWhere('userid',$r->user_id)->orderBy('id','DESC')->paginate(10);

            // print_r($from_user_id);

            $id_arr=array();
            foreach ($from_user_id as $data1) 
            {
                $id_arr[]=$data1->id;
            }

            // Get opponent created challenges and they completed from their end
            // $from_opp_id=challege::select("id")->whereNotIn('userid',$block_arr)->Where('opponentid',$r->user_id)->get();


     // print_r(json_decode($from_opp_id));

            // print_r($from_opp_id);

            foreach ($from_user_id as $data2) 
            {
                $get_time=time_record::where('challenge_id',$data2->id)->where('student_id',$data2->userid)->count();
                if($get_time>0)
                {
                    $id_arr[]=$data2->id;
                }
            }


            // Merge both 
            $count_chall=sizeof($id_arr);
            $get_chall=challege::whereIn('id',$id_arr)->orderBy('id','DESC')->limit(10)->get();       

            if (sizeof($get_chall)>0) 
            {
                $data1=array();
                $data2=array();
                $comp_id=array();

                foreach ($get_chall as $challenge) 
                {
                    // calculate time difference
                    $dateTime = new DateTime('now', new DateTimeZone('Asia/Kolkata')); 
                    $cdate=$dateTime->format("Y-m-d h:i:s");
                    //$cdate=date('Y-m-d h:i:s');
                    $mdate=$challenge->created_at;
        
                    $curdate=strtotime($cdate);
                    $mydate=strtotime($mdate);
        
                    $expdate = date('Y-m-d H:i:s', strtotime('+2 day', $mydate));
                    $expdate=strtotime($expdate);
        
                    //status for who created challenge
                    $user_complete=time_record::where('student_id',$challenge->userid)
                                                ->where('challenge_id',$challenge->id)->first();
                    if($r->user_id==$challenge->userid)
                    {
                        // check if win or not
                        if ($challenge->winner_id!='' && $challenge->winner_id!='DRAW') 
                        {
                            if ($challenge->winner_id==$r->user_id) 
                            {
                                $challenge['win_status']='You Won';
                            } 
                            else 
                            {
                                $challenge['win_status']='You Lost';
                            } 
                        } //check draw status
                        elseif ($challenge->winner_id!='' && $challenge->winner_id=='DRAW') 
                        {
                            $challenge['win_status']='Draw';
                        } 
                        else 
                        {
                            if ($challenge->status==2) 
                            {
                                $challenge['win_status']='Rejected';
                            } 
                            elseif($challenge->status==3) 
                            {
                                $challenge['win_status']='Ignored';
                            }
                            elseif ($curdate > $expdate) 
                            {
                                $challenge['win_status']='Expired';
                            } 
                            elseif(sizeof($user_complete)>0) 
                            {
                                $challenge['win_status']='Waiting'; 
                            }
                            else 
                            {
                                $challenge['win_status']='Your Turn';
                            }
                        }
                    }//status for opponent 
                    else 
                    {
                        if ($challenge->winner_id!='' && $challenge->winner_id!='DRAW') 
                        {
                            if ($challenge->winner_id==$r->user_id) 
                            {
                                $challenge['win_status']='You Won';
                            } 
                            else 
                            {
                                $challenge['win_status']='You Lost';
                            } 
                        }
                        elseif ($challenge->winner_id!='' && $challenge->winner_id=='DRAW') 
                        {
                            $challenge['win_status']='Draw';
                        } 
                        else 
                        {
                            if ($challenge->status==2) 
                            {
                                $challenge['win_status']='Rejected';
                            } 
                            elseif($challenge->status==3) 
                            {
                                $challenge['win_status']='Ignored';
                            }
                            elseif ($curdate > $expdate) 
                            {
                                $challenge['win_status']='Expired';
                            } 
                            elseif(sizeof($user_complete)>0) 
                            {
                                $challenge['win_status']='Waiting'; 
                            }
                            else 
                            {
                                $challenge['win_status']='Your Turn';
                            }
                        }
                    }
        
                    if ($challenge->userid!=$r->user_id) 
                    {
                        $get_students=DB::table('users')->where('email',$challenge->userid)->first();
                        $challenge['opponent_name']=$get_students->name;
                        $challenge['opponent_image']=$get_students->image;
                    } 
                    else 
                    {
                        $get_students=DB::table('users')->where('email',$challenge->opponentid)->first();
                        $challenge['opponent_name']=$get_students->name;
                        $challenge['opponent_image']=$get_students->image;
                    }  
        
                    // Get correct answer match
                    $user_correct=comp_result::where('student_id',$challenge->userid)
                                            ->where('challenge_id',$challenge->id)
                                            ->where('ques_status','R')
                                            ->count();
        
                    $opponent_correct=comp_result::where('student_id',$challenge->opponentid)
                                    ->where('challenge_id',$challenge->id)
                                    ->where('ques_status','R')
                                    ->count();
        
                    $challenge['user_correct']=$user_correct;
                    $challenge['opponent_correct']=$opponent_correct;
                    $data2[]=$challenge;
                }         
                
                // if ($count_chall>4) 
                // {
                //     $msg['pagination_chall']='true';
                // } 
                // else 
                // {
                //     $msg['pagination_chall']='false';
                // }
                
                $msg['challenge']=$data2;
                $msg['status']='success';
                echo json_encode($msg);
            } 
            else 
            {
                $msg['challenge']=array();
                $msg['message']='Nothing found!!';
                $msg['status']='fail';
                echo json_encode($msg);
            }  
        }
        else 
        {
            $msg['message']='Param Missing!!';
            $msg['status']='fail';
            echo json_encode($msg);
        }
    }
    

    //------------------------- Challage list with their status--------------------------------------//
    public function competition_list(Request $r)
    {
        if (!empty($r->user_id)) 
        {
            // Get blocked users
            $block_arr=array();
            $get_blocked=block_list::where('user_id',$r->user_id)->get();

            if(sizeof($get_blocked)>0)
            {
                foreach($get_blocked as $blocked)
                {
                    $block_arr[]=$blocked->opponent_id;
                }
            }

            // print_r($block_arr);

            // Get user created challenges
            $from_user_id=challege::whereNotIn('opponentid',$block_arr)->where('userid',$r->user_id)->get();
            $id_arr=array();
            foreach ($from_user_id as $data1) 
            {
                $id_arr[]=$data1->id;
            }

            // Get opponent created challenges and they completed from their end
            $from_opp_id=challege::whereNotIn('userid',$block_arr)->Where('opponentid',$r->user_id)->get();

            // print_r($from_opp_id);

            foreach ($from_opp_id as $data2) 
            {
                $get_time=time_record::where('challenge_id',$data2->id)->where('student_id',$data2->userid)->count();
                if($get_time>0)
                {
                    $id_arr[]=$data2->id;
                }
            }


            // Merge both 
            $count_chall=sizeof($id_arr);
            $get_chall=challege::whereIn('id',$id_arr)->orderBy('id','DESC')->limit(20)->get();       

            if (sizeof($get_chall)>0) 
            {
                $data1=array();
                $data2=array();
                $comp_id=array();

                foreach ($get_chall as $challenge) 
                {
                    // calculate time difference
                    $dateTime = new DateTime('now', new DateTimeZone('Asia/Kolkata')); 
                    $cdate=$dateTime->format("Y-m-d h:i:s");
                    //$cdate=date('Y-m-d h:i:s');
                    $mdate=$challenge->created_at;
        
                    $curdate=strtotime($cdate);
                    $mydate=strtotime($mdate);
        
                    $expdate = date('Y-m-d H:i:s', strtotime('+2 day', $mydate));
                    $expdate=strtotime($expdate);
        
                    //status for who created challenge
                    $user_complete=time_record::where('student_id',$challenge->userid)
                                                ->where('challenge_id',$challenge->id)->first();
                    if($r->user_id==$challenge->userid)
                    {
                        // check if win or not
                        if ($challenge->winner_id!='' && $challenge->winner_id!='DRAW') 
                        {
                            if ($challenge->winner_id==$r->user_id) 
                            {
                                $challenge['win_status']='You Won';
                            } 
                            else 
                            {
                                $challenge['win_status']='You Lost';
                            } 
                        } //check draw status
                        elseif ($challenge->winner_id!='' && $challenge->winner_id=='DRAW') 
                        {
                            $challenge['win_status']='Draw';
                        } 
                        else 
                        {
                            if ($challenge->status==2) 
                            {
                                $challenge['win_status']='Rejected';
                            } 
                            elseif($challenge->status==3) 
                            {
                                $challenge['win_status']='Ignored';
                            }
                            elseif ($curdate > $expdate) 
                            {
                                $challenge['win_status']='Expired';
                            } 
                            elseif(sizeof($user_complete)>0) 
                            {
                                $challenge['win_status']='Waiting'; 
                            }
                            else 
                            {
                                $challenge['win_status']='Your Turn';
                            }
                        }
                    }//status for opponent 
                    else 
                    {
                        if ($challenge->winner_id!='' && $challenge->winner_id!='DRAW') 
                        {
                            if ($challenge->winner_id==$r->user_id) 
                            {
                                $challenge['win_status']='You Won';
                            } 
                            else 
                            {
                                $challenge['win_status']='You Lost';
                            } 
                        }
                        elseif ($challenge->winner_id!='' && $challenge->winner_id=='DRAW') 
                        {
                            $challenge['win_status']='Draw';
                        } 
                        else 
                        {
                            if ($challenge->status==2) 
                            {
                                $challenge['win_status']='Rejected';
                            } 
                            elseif($challenge->status==3) 
                            {
                                $challenge['win_status']='Ignored';
                            }
                            elseif ($curdate > $expdate) 
                            {
                                $challenge['win_status']='Expired';
                            } 
                            elseif(sizeof($user_complete)>0) 
                            {
                                $challenge['win_status']='Waiting'; 
                            }
                            else 
                            {
                                $challenge['win_status']='Your Turn';
                            }
                        }
                    }
        
                    if ($challenge->userid!=$r->user_id) 
                    {
                        $get_students=DB::table('users')->where('email',$challenge->userid)->first();
                        $challenge['opponent_name']=$get_students->name;
                        $challenge['opponent_image']=$get_students->image;
                    } 
                    else 
                    {
                        $get_students=DB::table('users')->where('email',$challenge->opponentid)->first();
                        $challenge['opponent_name']=$get_students->name;
                        $challenge['opponent_image']=$get_students->image;



                    }  
        
                    // Get correct answer match
                    $user_correct=comp_result::where('student_id',$challenge->userid)
                                            ->where('challenge_id',$challenge->id)
                                            ->where('ques_status','R')
                                            ->count();
        
                    $opponent_correct=comp_result::where('student_id',$challenge->opponentid)
                                    ->where('challenge_id',$challenge->id)
                                    ->where('ques_status','R')
                                    ->count();
        
                    $challenge['user_correct']=$user_correct;
                    $challenge['opponent_correct']=$opponent_correct;
                    $data2[]=$challenge;
                }         
                
                if ($count_chall>4) 
                {
                    $msg['pagination_chall']='true';
                } 
                else 
                {
                    $msg['pagination_chall']='false';
                }
                
                $msg['challenge']=$data2;
                $msg['status']='success';
                echo json_encode($msg);
            } 
            else 
            {
                $msg['challenge']=array();
                $msg['message']='Nothing found!!';
                $msg['status']='fail';
                echo json_encode($msg);
            }  
        }
        else 
        {
            $msg['message']='Param Missing!!';
            $msg['status']='fail';
            echo json_encode($msg);
        }
    }
    
    //------------------- Action on challange-------------//
    public function accept_challenge(Request $r)
    {
        if (!empty($r->user_id) && !empty($r->type) && !empty($r->challenge_id)) 
        {   
            $get_challenge=challege::where('id',$r->challenge_id)->first();
            if (sizeof($get_challenge)>0) 
            {
                $update=challege::find($r->challenge_id);
                if ($r->type=='accept') 
                {
                    $update->status=1;
                } 
                elseif($r->type=='reject')
                {
                    $update->status=2;
                }
                else
                {
                    $block_status=block_list::where('user_id',$r->user_id)->where('opponent_id',$get_challenge->userid)->count();
                    
                    if ($block_status<1) 
                    {
                        $insert=new block_list;
                        $insert->user_id=$get_challenge->opponentid;
                        $insert->opponent_id=$get_challenge->userid;
                        $insert->save();
                    }
                    
                    $update->status=3;
                }
                
                $update->save();
                if ($update) 
                {
                    $msg['data']=$update;
                    $msg['status']='success';
                    echo json_encode($msg);
                }
                else 
                {
                    $msg['message']='Fail to update!!';
                    $msg['status']='fail';
                    echo json_encode($msg);
                }
            }
            else 
            {
                $msg['message']='Challenge not found!!';
                $msg['status']='fail';
                echo json_encode($msg);
            } 
        } 
        else 
        {
            $msg['message']='Param Missing!!';
            $msg['status']='fail';
            echo json_encode($msg);
        }
    }

    // get blocked users

    public function getblockeduser(Request $r)
    {

        $block_status=DB::table('block_lists')->where('user_id',$r->user_id)->paginate(20);

        $arr=array();
        
            if(sizeof($block_status)>0)
            {
                foreach($block_status as $blocked)
                {   
                    $userdet=DB::table('users')->where('email',$blocked->opponent_id)->first();

                    $blocked->name=$userdet->name;
                    $blocked->image=$userdet->image;
                    $blocked->level=$userdet->level;
                    $blocked->college=$userdet->college;
                    $blocked->city=$userdet->city;

                    // $block_arr[]=$blocked->opponent_id;

                    $arr['user'][]=$blocked;
                }

                $arr['status']="true";
                $arr['message']="success";

            }else
            {

                $arr['status']="false";
                $arr['message']="You have no blocked users";
            }

        echo json_encode($arr);
    }


// unblock users

    public function unblockuser(Request $r)
    {

        $block_status=block_list::where('user_id',$r->user_id)->where('opponent_id',$r->opponent_id)->first();
        
        $arr=array();

            if(sizeof($block_status)>0)
            {
               block_list::where('user_id',$r->user_id)->where('opponent_id',$r->opponent_id)->delete();

                $arr['status']="true";
                $arr['message']="success";

            }else
            {
                $arr['status']="false";
                $arr['message']="no users found";
            }

        echo json_encode($arr);
    }


// get college, stream and year wise topic
    public function getTopics(Request $r){

        $type1=DB::table('users')->select('email','type','version')->where('email',$r->email)->first();
        
        if($type1)
        {   


        	$type=$type1->type;


            if($type == 2)
            {   

                $data_user=DB::table('student_college')->where('student_college.uid',$r->email)
                ->first();

                $data=DB::table('topic_college')
                 ->join('topic_search','topic_search.id','=','topic_college.topicid')
                ->select('topic_college.topicid as tc_topic_id','topic_college.collegeid as tc_college_id','topic_college.stream as tc_stream','topic_college.year as tc_year','topic_search.*')
                ->having('tc_year',$data_user->year)
                ->having('tc_stream',$data_user->stream)
                ->having('tc_college_id',$data_user->collegeid)
                ->get();
     
                $res['status']='true'; 
                $res['message']='success';
                $res['data']=$data;
                $res['type']=$type;
            }
            else
            {
                $query= DB::table('topic_search')->Where('topic_type','0')->Where('id','>',$r->index)->limit(20)->get();                 
                $res['status']='true';
                $res['message']='success';  
                $res['data']=$query;
                $res['type']=$type;
            }


            if($type1->version<50){
            	 $update=DB::table('users')->where('email',$r->email)->update(['version'=>'53']);
            }	


        }
        else
        {
            $res['status']='false';
            $res['message']='user not exist';  
                
        }    

        echo json_encode($res);

    }

// get subject for individual users
    public function getChallengeSubject(Request $r)

    {

        $data=DB::table('users')->Where('email',$r->user_id)->first();

            if($data)
            {
                $type=$data->type;

                if($type<2)   // for individual users
                {    

        	        $query= DB::table('test_question')
        				->select('sub_test_name',DB::raw('count(id) as counting'))
        				->groupBy('sub_test_name')
        				->having('counting','>=',150)
        				->get();
        		
        			if($query){	
        				$res['status']="true";
        				$res['message']="success";	
            		  	$res['data']=$query;
            		}
            		else{
            			$res['status']="false";
            			$res['message']="something went wrong please try after some time";
            		}
                }
                else
                {   

                        $info=DB::table('student_college')->Where('uid',$r->user_id)->first();

                        $test=DB::table('college_data')->select('postid')
                                ->where([
                                    ['college_id', '=', $info->collegeid],
                                    ['stream', '=', $info->stream],
                                    ['field_type','=','2']])
                                ->orWhere([['college_id', '=', '0'],
                                          ['field_type','=','2']])->get();

                        $tid=array();

                        foreach ($test as $r) {
                            $tid[]=$r->postid;
                        }

                        // print_r($tid);

                        $query= DB::table('test_question')
                                    ->select('sub_test_name',DB::raw('count(id) as counting'))
                                    ->whereIn('testid',$tid)
                                    ->groupBy('sub_test_name')
                                    ->having('counting','>=',50)
                                    ->get();
                            
                            if($query){ 
                                $res['status']="true";
                                $res['message']="success";  
                                $res['data']=$query;
                            }
                            else{
                                $res['status']="false";
                                $res['message']="something went wrong please try after some time";
                            }
                       
                }
            }        
            else
            {
                $res['status']="false";
                $res['message']="user not exist";
            }
            
        echo json_encode($res);

    }

// get subject for college users
    public function getChallengeSubjectcollege(Request $r)
    {

        $data=DB::table('users')->Where('email',$r->user_id)->first();

            if($data)
            {
                $type=$data->type;

                if($type>1)
                {    
                    $info=DB::table('student_college')->Where('uid',$r->user_id)->first();

                    $test=DB::table('college_data')->select('postid')
                            ->where([
                                ['college_id', '=', $info->collegeid],
                                ['stream', '=', $info->stream],
                                ['field_type','=','2']])
                            ->orWhere([['college_id', '=', '0'],
                                      ['field_type','=','2']])->get();

                    $tid=array();

                    foreach ($test as $r) {
                        $tid[]=$r->postid;
                    }

                    // print_r($tid);

                    $query= DB::table('test_question')
                                ->select('sub_test_name',DB::raw('count(id) as counting'))
                                ->whereIn('testid',$tid)
                                ->groupBy('sub_test_name')
                                ->having('counting','>=',40)
                                ->get();
                        
                        if($query){ 
                            $res['status']="true";
                            $res['message']="success";  
                            $res['data']=$query;
                        }
                        else{
                            $res['status']="false";
                            $res['message']="something went wrong please try after some time";
                        }
                }

            }

        echo json_encode($res);       

    }



    // without college,stream and year condition for college student get all topic of all college    
    
    public function getTopic(Request $r){

		$data=DB::table('users')->select('type')->Where('email',$r->email)->first();
		if($data){
			$type=$data->type;
			if($type<2)
				$type=0;
			else
				$type=1;

		// $type=0;
			
	    	$query= DB::table('topic_search')->Where('topic_type',$type)->Where('id','>',$r->index)->limit(20)->get();    				
	    	
	    	$res['status']='true';
	    	$res['message']='success';	
	    	$res['data']=$query;		
	    				
    	}      
    	else{
    		$res['status']='false';	
    		$res['message']='user not exist';
	    	$res['data']=$query;
    	}      
            echo json_encode($res);
    }       




    // get opponent    
    public function getOpponents(Request $r){


        $data=DB::table('users')->select('type')->where('email',$r->email)->first();

        $block_user=DB::table('block_lists')->select('opponent_id','user_id')->where('user_id',$r->email)->orWhere('opponent_id',$r->email)->get();
                $blocked_user_id=array();
                foreach($block_user as $bu)
                {

                    if($bu->opponent_id==$r->email)
                        $blocked_user_id[]=$bu->user_id;
                    else
                    $blocked_user_id[]=$bu->opponent_id;

                }


		$email = $r->email;
	
        $date = date("Y-m-d");

        $today_user = DB::select("select `opponentid`, count(id) as counting from `challeges` WHERE `userid` = '$email' AND DATE(`created_at`) = '$date' group by `opponentid` having `counting` >= 5");
        											

				foreach($today_user as $tu)
                {
                    $blocked_user_id[]=$tu->opponentid;
                }


       
        if($data){
            $type=$data->type;
            
            if($type<2)
            {    

                $query= DB::table('users')->select('users.id as user_id','name','image','email','college')->Where('type','<','2')->Where('email','!=',$r->email)->where('name', 'LIKE', "%$r->name%")->where('version','>','48')->whereNotIn('email',$blocked_user_id)->orderBy('id', 'desc')->paginate(20); 

                 $res['status']='true';
                $res['message']='success';  
                $res['users']=$query ;

            }
            else
            {

                $dt_stream=DB::table('student_college')->select('stream')->where('uid',$r->email)->first();

                //  $data=DB::table('users')
                // ->join('student_college','student_college.uid','=','users.email')
                // ->select('type')->Where('email',$r->email)
                // ->select('users.type','student_college.stream')
                // ->first();

                // $query= DB::table('users')->Where('type','2')->Where('id','<',$r->index)->orderBy('id', 'desc')->limit(20)->get();  
                
                $datatable=DB::table('student_college')
                ->join('users','users.email','=','student_college.uid')
                ->where('student_college.stream',$dt_stream->stream)
                ->where('student_college.status','true')
                ->where('version','>','48')
                ->where('name','LIKE', "%$r->name%")
                ->whereNotIn('student_college.uid',$blocked_user_id)
                ->whereNotIn('student_college.uid',[$r->email])
                ->select('student_college.*','users.name','users.image','users.id as user_id','users.college','users.email')
                ->orderBy('student_college.id', 'desc')
                ->paginate(20);

                $res['status']='true';
                $res['message']='success';  
                // $res['subject']=$dt_stream->stream;
                $res['users']=$datatable; 
            }
            
            // $res['status']='true';
            // $res['message']='success';  
            // $res['data']=$query;        
                        
        }   
        else{
            $res['status']='false'; 
            $res['message']='user not exist';
            $res['data']=$query;
        }
            echo json_encode($res);
    }


     // get opponent new 88   
    public function getOpponents_new(Request $r){

        $data=DB::table('users')->select('type')->where('email',$r->email)->first();

        $block_user=DB::table('block_lists')->select('opponent_id','user_id')->where('user_id',$r->email)->orWhere('opponent_id',$r->email)->get();

                $blocked_user_id=array();
                foreach($block_user as $bu)
                {

                    if($bu->opponent_id==$r->email)
                        $blocked_user_id[]=$bu->user_id;
                    else
                    $blocked_user_id[]=$bu->opponent_id;

                }

        $email = $r->email;
	
        $date = date("Y-m-d");

        $today_user = DB::select("select `opponentid`, count(id) as counting from `challeges` WHERE `userid` = '$email' AND DATE(`created_at`) = '$date' group by `opponentid` having `counting` >= 5");
        											

				foreach($today_user as $tu)
                {
                    $blocked_user_id[]=$tu->opponentid;
                }


        // echo json_encode($blocked_user_id);			

       
        if($data){
            $type=$data->type;
            
            if($type<2)
            {    

                $query= DB::table('users')->select('users.id as user_id','name','image','email','college')->Where('type','<','2')->Where('email','!=',$r->email)->where('name', 'LIKE', "%$r->name%")->where('version','>','48')->whereNotIn('email',$blocked_user_id)->orderBy('id', 'desc')->paginate(20); 

                 $res['status']='true';
                $res['message']='success';  
                $res['users']=$query ;

            }
            else
            {

                $dt_stream=DB::table('student_college')->select('stream')->where('uid',$r->email)->first();

                
                $datatable=DB::table('student_college')
                ->join('users','users.email','=','student_college.uid')
                ->where('student_college.stream',$dt_stream->stream)
                ->where('student_college.status','true')
                ->where('version','>','48')
                ->where('name','LIKE', "%$r->name%")
                ->whereNotIn('student_college.uid',$blocked_user_id)
                ->whereNotIn('student_college.uid',[$r->email])
                ->select('student_college.*','users.name','users.image','users.id as user_id','users.college','users.email')
                ->orderBy('student_college.id', 'desc')
                ->paginate(20);

                $res['status']='true';
                $res['message']='success';  
                // $res['subject']=$dt_stream->stream;
                $res['users']=$datatable; 
            }
             
        }   
        else{
            $res['status']='false'; 
            $res['message']='user not exist';
            $res['data']=$query;
        }
            echo json_encode($res);
    }

    public function getopponent_details(Request $r){

        $data=DB::table('users')->Where('email',$r->email)->orWhere('email',$r->opponent_id)->get();

        if(count($data)>1)
        { 

           if($r->status=='true')
           { 

            // $user_type=$data[0]->type;

            // if($user_type < 2)
            // {
               
               $que= DB::table('test_question')->select('id')->where('sub_test_name', $r->subject)->inRandomOrder()->limit(10)->get();

               $arr= array();

               foreach ($que as $value) {
                   $arr[]=$value->id;
               }

               $pt=DB::table('challenge_point')->where('id',1)->first();

               $pnt=mt_rand($pt->min,$pt->max);

                $ins= DB::table('challeges')->insertGetId(['userid' => $r->email, 'opponentid' =>$r->opponent_id,'win_points'=> $pnt,'questions'=>json_encode($arr),'subject'=>$r->subject]);    
            // }
            // else{
               
            //     $post_ids=DB::table('college_data')->select('postid')->where('stream',$r->subject)->get();
            //     $post_id=array();
            //     foreach($post_ids as $pid)
            //     {
            //         $post_id[]=$pid->postid;
            //     }

            //     $que= DB::table('test_question')->select('id')->whereIn('testid',$post_id)->inRandomOrder()->limit(10)->get();

            //    $arr= array();

            //    foreach ($que as $value) {
            //        $arr[]=$value->id;
            //    }

            

            // $ins= DB::table('challeges')->insertGetId(['userid' => $r->email, 'opponentid' =>$r->opponent_id,'win_points'=> '5','questions'=>json_encode($arr),'subject'=>$r->subject]);    

            // }

            $res['status']="true";
            $res['message']="success";
            $res['data']=$data;
            $res['challenge_id']=$ins;
            $res['win_points']=$pnt;
            $res['subject']=$r->subject;
           
           }
           else{

            $p=DB::table('challeges')->select('win_points')->Where('id',$r->challenge_id)->first();

            $res['status']="true";
            $res['message']="success";
            $res['data']=$data;
            $res['challenge_id']=$r->challenge_id;
            $res['win_points']=$p->win_points;
            $res['subject']=$r->subject;
           } 
            // DB::getPdo()->lastInsertId();
        }
        else
        {
            $res['status']="false";
            $res['message']="no user found";
        }

        echo json_encode($res);
    }

    //-------------------------challange questions--------------------------------------//
    public function challange_questions(Request $r)
    {
        if (!empty($r->user_id) && !empty($r->challenge_id)) 
        {
            $challenge_id=$r->challenge_id;
            $get_challenge=challege::where('id',$challenge_id)->first();

            // get already answered questions
            $pre_arr=array();
            $pre_questions=comp_result::where('student_id',$r->user_id)->where('challenge_id',$challenge_id)->get();
            $pre_count=sizeof($pre_questions);
            if($pre_count>0)
            {
                foreach($pre_questions as $pre_ques)
                {
                    $pre_arr[]=$pre_ques->ques_id;
                }

                $all_ques=json_decode($get_challenge->questions);
                $questions = array_diff($all_ques, $pre_arr);
            }
            else
            {
                $questions=json_decode($get_challenge->questions);
            }

            if (sizeof($questions)>0) 
            {
                $mydata=array();
                foreach ($questions as $question) 
                {
                    $get_questions=DB::table('test_question')->where('id',$question)->first();

                    if ($get_challenge->opponentid==$r->user_id) 
                    {
                        $get_status=comp_result::where('student_id',$get_challenge->userid)
                                                ->where('challenge_id',$r->challenge_id)
                                                ->where('ques_id',$question)
                                                ->first();
    
                        if (sizeof($get_status)>0) 
                        {
                            $get_questions->user_ans_status=$get_status->ques_status;
                        } 
                        else 
                        {
                            $get_questions->user_ans_status='U';
                        }
                    }
                    $mydata[]=$get_questions;
                }

                $user_count=comp_result::where('student_id',$get_challenge->userid)->where('challenge_id',$challenge_id)->where('ques_status','R')->count();
                $opponent_count=comp_result::where('student_id',$get_challenge->opponentid)->where('challenge_id',$challenge_id)->where('ques_status','R')->count();

                $msg['user_count']=$user_count;
                $msg['opponent_count']=$opponent_count;
                $msg['owner_id']=$get_challenge->userid;
                 $msg['per_que_time']=40;
                $msg['data']=$mydata;
                $msg['status']='true';
                echo json_encode($msg);
            }
            else 
            {
                $msg['message']='Nothing Found!!';
                $msg['status']='false';
                echo json_encode($msg);
            }

        } 
        else 
        {
            $msg['message']='Param Missing!!';
            $msg['status']='false';
            echo json_encode($msg);
        }
    }

   //-------------------------------------------- get single question---------------------------//

      public function getsingle_question(Request $r)
    {

        if (!empty($r->user_id) && !empty($r->challenge_id)&& !empty($r->question_id)) 
        {
                       
            $pre_questions=comp_result::where('student_id',$r->user_id)->where('challenge_id',$r->challenge_id)->where('ques_id',$r->question_id)->first();

            // print_r($pre_questions);
           
            $pre_count=sizeof($pre_questions);
            if($pre_count>0)
            {   
                $get_questions=DB::table('test_question')->where('id',$r->question_id)->first();
                
                $pre_questions->question=$get_questions;

                $msg['message']='success';
                $msg['status']='true';
                $msg['data']=$pre_questions;
            }
            else
            {
                $msg['message']='question not found';
                $msg['status']='false';
                $msg['data']="0";
            }    

            echo json_encode($msg);
            

        } 
        else 
        {
            $msg['message']='Param Missing!!';
            $msg['status']='false';
            echo json_encode($msg);
        }
    }




    public function getchallenge_question(Request $r)
    {
        $data=DB::table('challeges')->Where('id',$r->challenge_id)->first();
        if(count($data)>0)
        { 
           $que= DB::table('test_question')->whereIn('id', json_decode($data->questions,true))->get();

            $res['status']="true";
            $res['message']="success";
            $res['per_que_time']=40;  // in second
            $res['data']=$que;
        }
        else
        {
            $res['status']="false";
            $res['message']="challenge not found";
        }
        echo json_encode($res);
    }

    //-------------------------Answer challenge questions--------------------------------------//
    public function answer_chall_question(Request $r)
    {
        if (!empty($r->user_id) && !empty($r->challenge_id) && !empty($r->ques_id) && !empty($r->time) && !empty($r->ques_status) && !empty($r->ques_answer) && !empty($r->last_ques)) 
        {        
            $get_chall_type=challege::where('id',$r->challenge_id)->first();

            $get_pre_ques=comp_result::where('student_id',$r->user_id)
                                    ->where('challenge_id',$r->challenge_id)
                                    ->where('ques_id',$r->ques_id)
                                    ->count();
        
            if($get_pre_ques<1)
            {
                // give points on rihgt answer
                if ($r->ques_status=='R') 
                {
                    $insert1=new points_earn;
                    $insert1->student_id=$r->user_id;
                    $insert1->challenge_id=$r->challenge_id;
                    $insert1->question_id=$r->ques_id;
                    $insert1->question_type='play';

                    // Get user points and rank before updating for update level purpose
                    $get_xp=DB::table('users')->where('email',$r->user_id)->first();
                    $old_xp=$get_xp->xp;
                    $target_rank=0;
                    $level_id=null;
                    $level_name=null;

                    $rank_level=rank_level::get();
                    foreach($rank_level as $rank)
                    {
                        if($rank->point>$old_xp)
                        {
                            $target_rank=$rank->point;
                            $level_id=$rank->id;
                            $level_name=$rank->name;
                            break;
                        }
                    }

                    // Update User xp
                    $t_ap=$get_xp->xp+1;
                    $update=DB::table('users')->where('email',$r->user_id)->update(['xp'=>$t_ap]);

                    $insert1->points=1;
                    $insert1->save();
                }

                //update status that user/opponent started playing
                $update_chal_status=challege::find($r->challenge_id);
                if($get_chall_type->userid==$r->user_id)
                {
                    $update_chal_status->play_status=1;
                }
                else
                {
                    $update_chal_status->play_status=2;
                }

                // store question's result status
                $insert=new comp_result;
                $insert->student_id=$r->user_id;
                $insert->challenge_id=$r->challenge_id;
                $insert->ques_id=$r->ques_id;
                $insert->ques_status=$r->ques_status;
                $insert->ques_answer=$r->ques_answer;
                $insert->time=$r->time;
                $insert->save();

                // if it is last question
                if ($r->last_ques=='yes') 
                {
                    $insert_time=new time_record;
                    $insert_time->student_id=$r->user_id;
                    $insert_time->challenge_id=$r->challenge_id;
                    $insert_time->time=$r->time;
                    $insert_time->save(); 

                    // find winner and give points
                    if($get_chall_type->opponentid==$r->user_id)
                    {
                        $user_total=points_earn::where('student_id',$get_chall_type->userid)
                                                ->where('challenge_id',$r->challenge_id)
                                                ->where('question_type','play')
                                                ->whereNotNull('question_id')
                                                ->count();
                        $opponent_total=points_earn::where('student_id',$get_chall_type->opponentid)
                                                ->where('challenge_id',$r->challenge_id)
                                                ->where('question_type','play')
                                                ->whereNotNull('question_id')
                                                ->count();

                        if($user_total!=$opponent_total)
                        {
                            $win_id=null;
                            $insert_winner=new points_earn;
                            $insert_winner->challenge_id=$r->challenge_id;
                            $insert_winner->question_type='play';
                            
                            if ($user_total>$opponent_total) 
                            {
                                $insert_winner->student_id=$get_chall_type->userid;

                                // Get user points and rank before updating for update level purpose
                                $xp_data = $this->current_xp($get_chall_type->userid);
                                $old_xp=$xp_data['old_xp'];
                                $target_rank=$xp_data['target_rank'];
                                $level_id=$xp_data['level_id'];
                                $level_name=$xp_data['level_name'];
        
                                // Update user xp
                                $t_ap=$old_xp+$get_chall_type->win_points;
                                $update=DB::table('users')->where('email',$get_chall_type->userid)->update(['xp'=>$t_ap]);
        
                                // Get user points and rank after updating for update level purpose
                                $user_point_after=DB::table('users')->where('email',$get_chall_type->userid)->first();
                                $new_xp=$user_point_after->xp;
        
                                if($new_xp>=$target_rank && $level_id!='')
                                {
                                    // Store user's notify
                                    $this->profile_update($get_chall_type->userid,$level_id,$level_name);
                                }

                                //update winner status in challenge
                                $update_chal_status->winner_id=$get_chall_type->userid;

                                // Get win ID
                                $win_id=$get_chall_type->userid;
                            } 
                            else 
                            {
                                $insert_winner->student_id=$get_chall_type->opponentid;

                                // Get user points and rank before updating for update level purpose
                                $xp_data = $this->current_xp($get_chall_type->opponentid);
                                $old_xp=$xp_data['old_xp'];
                                $target_rank=$xp_data['target_rank'];
                                $level_id=$xp_data['level_id'];
                                $level_name=$xp_data['level_name'];

                                // Update User xp
                                $t_ap=$old_xp+$get_chall_type->win_points;
                                $update=DB::table('users')->where('email',$get_chall_type->opponentid)->update(['xp'=>$t_ap]);

                                // Get user points and rank after updating for update level purpose
                                $user_point_after=DB::table('users')->where('email',$get_chall_type->opponentid)->first();
                                $new_xp=$user_point_after->xp;

                                if($new_xp>=$target_rank && $level_id!='')
                                {
                                    // Store user's notify
                                    $this->profile_update($get_chall_type->opponentid,$level_id,$level_name);
                                }

                                //update winner status in challenge
                                $update_chal_status->winner_id=$get_chall_type->opponentid;

                                // Get win ID
                                $win_id=$get_chall_type->opponentid;
                                
                            }
                            $insert_winner->points=$get_chall_type->win_points;
                            $insert_winner->save();
                            
                            // Notiify both users for challenge winning

                            $this->win_alert($get_chall_type->userid,$get_chall_type->id,$win_id,$get_chall_type->subject);
                            $this->win_alert($get_chall_type->opponentid,$get_chall_type->id,$win_id,$get_chall_type->subject);
                        }
                        else
                        {
                            //update DRAW status in challenge
                            $update_chal_status->winner_id='DRAW';
                            $update_chal_status->save();

                            $half=round(($get_chall_type->win_points)/2);
                            //=========================user 1 =============================//
                            $insert_winner=new points_earn;
                            $insert_winner->challenge_id=$r->challenge_id;
                            $insert_winner->question_type='play';
                            $insert_winner->student_id=$get_chall_type->userid;
                            $insert_winner->points=$half;
                            $insert_winner->save();

                            // Get user points and rank before updating for update level purpose
                            $xp_data = $this->current_xp($get_chall_type->userid);
                            $old_xp=$xp_data['old_xp'];
                            $target_rank=$xp_data['target_rank'];
                            $level_id=$xp_data['level_id'];
                            $level_name=$xp_data['level_name'];

                            // Update User xp
                            $t_ap=$old_xp+$half;
                            $update=DB::table('users')->where('email',$get_chall_type->userid)->update(['xp'=>$t_ap]);


                            // Get user points and rank after updating for update level purpose
                            $user_point_after=DB::table('users')->where('email',$get_chall_type->userid)->first();
                            $new_xp=$user_point_after->xp;

                            if($new_xp>=$target_rank && $level_id!='')
                            {
                                // Store user's notify
                                $this->profile_update($get_chall_type->userid,$level_id,$level_name);
                            }

                            //=========================user 2 =============================//
                            $insert_winner1=new points_earn;
                            $insert_winner1->challenge_id=$r->challenge_id;
                            $insert_winner1->question_type='play';
                            $insert_winner1->student_id=$get_chall_type->opponentid;
                            $insert_winner1->points=$half;
                            $insert_winner1->save();

                            // Get user points and rank before updating for update level purpose
                            $xp_data1 = $this->current_xp($get_chall_type->opponentid);
                            $old_xp1=$xp_data1['old_xp'];
                            $target_rank1=$xp_data1['target_rank'];
                            $level_id1=$xp_data1['level_id'];
                            $level_name1=$xp_data1['level_name'];

                            // Update User xp
                            $t_ap1=$old_xp1+$half;
                            $update1=DB::table('users')->where('email',$get_chall_type->opponentid)->update(['xp'=>$t_ap1]);


                            // Get user points and rank after updating for update level purpose
                            $user_point_after1=DB::table('users')->where('email',$get_chall_type->opponentid)->first();
                            $new_xp1=$user_point_after1->xp;

                            if($new_xp1>=$target_rank1 && $level_id1!='')
                            {
                                // Store user's notify
                                $this->profile_update($get_chall_type->opponentid,$level_id1,$level_name1);
                            }

                            // Notiify both users for challenge DRAW
                            // $this->draw_alert($get_chall_type->userid,$get_chall_type->id,$get_chall_type->subject,$half);
                            // $this->draw_alert($get_chall_type->opponentid,$get_chall_type->id,$get_chall_type->subject,$half);
                        }   
                    }
                    else 
                    {    
                        // Send alert notification
                        $opp_data=DB::table('users')->where('email',$get_chall_type->userid)->first();
                        $user=DB::table('users')->where('email',$get_chall_type->opponentid)->first();
                        $message=$opp_data->name." challenged you in ".$get_chall_type->subject;
                        $player_id[]= $user->gcmid;
                        $content = array(
                        "en" => $message
                        );

                        $data = array(
                            "type" => 'chall_invite',
                            "chall_id"=>$get_chall_type->id
                            );
                        $this->alert_Message($player_id,$content,$data);
                    }  
                }
                $update_chal_status->save();

                if (sizeof($insert)>0) 
                {
                    $msg['data']=$insert;
                    $msg['status']='success';
                    echo json_encode($msg);
                }
                else 
                {
                    $msg['status']='fail';
                    echo json_encode($msg);
                }
            }
            else
            {
                $msg['message']='answer already given';
                $msg['status']='fail';
                echo json_encode($msg);
            }
        } 
        else 
        {
            $msg['message']='Param Mising!!';
            $msg['status']='fail';
            echo json_encode($msg);
        }
    }

    //-------------------------Answer challenge questions dummy--------------------------------------//
    
    public function answer_chall(Request $r)
    {
        if (!empty($r->user_id) && !empty($r->challenge_id) && !empty($r->ques_id) && !empty($r->time) && !empty($r->ques_status) && !empty($r->ques_answer) && !empty($r->last_ques)) 
        {        
            $get_chall_type=challege::where('id',$r->challenge_id)->first();

            $get_pre_ques=comp_result::where('student_id',$r->user_id)
                                    ->where('challenge_id',$r->challenge_id)
                                    ->where('ques_id',$r->ques_id)
                                    ->count();
        
            if($get_pre_ques<1)
            {
                // give points on rihgt answer
                if ($r->ques_status=='R') 
                {
                    $insert1=new points_earn;
                    $insert1->student_id=$r->user_id;
                    $insert1->challenge_id=$r->challenge_id;
                    $insert1->question_id=$r->ques_id;
                    $insert1->question_type='play';

                    // Get user points and rank before updating for update level purpose
                    $get_xp=DB::table('users')->where('email',$r->user_id)->first();
                    $old_xp=$get_xp->xp;
                    $target_rank=0;
                    $level_id=null;
                    $level_name=null;

                    // $rank_level=rank_level::get();
                    // foreach($rank_level as $rank)
                    // {
                    //     if($rank->point>$old_xp)
                    //     {
                    //         $target_rank=$rank->point;
                    //         $level_id=$rank->id;
                    //         $level_name=$rank->name;
                    //         break;
                    //     }
                    // }

                    // Update User xp
                    $t_ap=$get_xp->xp+1;
                    $update=DB::table('users')->where('email',$r->user_id)->update(['xp'=>$t_ap]);

                    $this->check_level($r->user_id);

                    $insert1->points=1;
                    $insert1->save();
                }

                //update status that user/opponent started playing
                $update_chal_status=challege::find($r->challenge_id);
                if($get_chall_type->userid==$r->user_id)
                {
                    $update_chal_status->play_status=1;
                }
                else
                {
                    $update_chal_status->play_status=2;
                }

                // store question's result status
                $insert=new comp_result;
                $insert->student_id=$r->user_id;
                $insert->challenge_id=$r->challenge_id;
                $insert->ques_id=$r->ques_id;
                $insert->ques_status=$r->ques_status;
                $insert->ques_answer=$r->ques_answer;
                $insert->time=$r->time;
                $insert->save();

                // if it is last question
                if ($r->last_ques=='yes') 
                {
                    $insert_time=new time_record;
                    $insert_time->student_id=$r->user_id;
                    $insert_time->challenge_id=$r->challenge_id;
                    $insert_time->time=$r->time;
                    $insert_time->save(); 

                    // find winner and give points
                    if($get_chall_type->opponentid==$r->user_id)
                    {
                        $user_total=points_earn::where('student_id',$get_chall_type->userid)
                                                ->where('challenge_id',$r->challenge_id)
                                                ->where('question_type','play')
                                                ->whereNotNull('question_id')
                                                ->count();
                        $opponent_total=points_earn::where('student_id',$get_chall_type->opponentid)
                                                ->where('challenge_id',$r->challenge_id)
                                                ->where('question_type','play')
                                                ->whereNotNull('question_id')
                                                ->count();

                        if($user_total!=$opponent_total)
                        {
                            $win_id=null;
                            $insert_winner=new points_earn;
                            $insert_winner->challenge_id=$r->challenge_id;
                            $insert_winner->question_type='play';
                            
                            if ($user_total>$opponent_total) 
                            {
                                $insert_winner->student_id=$get_chall_type->userid;

                                // Get user points and rank before updating for update level purpose
                                $xp_data = $this->current_xp($get_chall_type->userid);
                                $old_xp=$xp_data['old_xp'];
                                $target_rank=$xp_data['target_rank'];
                                $level_id=$xp_data['level_id'];
                                $level_name=$xp_data['level_name'];
        
                                // Update user xp
                                $t_ap=$old_xp+$get_chall_type->win_points;
                                $update=DB::table('users')->where('email',$get_chall_type->userid)->update(['xp'=>$t_ap]);
        
                                // Get user points and rank after updating for update level purpose

                                $this->check_level($get_chall_type->userid);
/*
                                // $user_point_after=DB::table('users')->where('email',$get_chall_type->userid)->first();
                                // $new_xp=$user_point_after->xp;
        
                                // if($new_xp>=$target_rank && $level_id!='')
                                // {
                                //     // Store user's notify
                                //     $this->profile_update($get_chall_type->userid,$level_id,$level_name);
                                // }
*/
                                //update winner status in challenge
                                $update_chal_status->winner_id=$get_chall_type->userid;

                                // Get win ID
                                $win_id=$get_chall_type->userid;
                            } 
                            else 
                            {
                                $insert_winner->student_id=$get_chall_type->opponentid;

                                // Get user points and rank before updating for update level purpose
                                $xp_data = $this->current_xp($get_chall_type->opponentid);
                                $old_xp=$xp_data['old_xp'];
                                $target_rank=$xp_data['target_rank'];
                                $level_id=$xp_data['level_id'];
                                $level_name=$xp_data['level_name'];

                                // Update User xp
                                $t_ap=$old_xp+$get_chall_type->win_points;
                                $update=DB::table('users')->where('email',$get_chall_type->opponentid)->update(['xp'=>$t_ap]);

                                // Get user points and rank after updating for update level purpose

                                $this->check_level($get_chall_type->opponentid);

/*                                
                                // $user_point_after=DB::table('users')->where('email',$get_chall_type->opponentid)->first();
                                // $new_xp=$user_point_after->xp;

                                // if($new_xp>=$target_rank && $level_id!='')
                                // {
                                //     // Store user's notify
                                //     $this->profile_update($get_chall_type->opponentid,$level_id,$level_name);
                                // }
*/
                                //update winner status in challenge
                                $update_chal_status->winner_id=$get_chall_type->opponentid;

                                // Get win ID
                                $win_id=$get_chall_type->opponentid;
                                
                            }
                            $insert_winner->points=$get_chall_type->win_points;
                            $insert_winner->save();
                            
                            // Notiify both users for challenge winning

                            $this->win_alert($get_chall_type->userid,$get_chall_type->id,$win_id,$get_chall_type->subject);
                            $this->win_alert($get_chall_type->opponentid,$get_chall_type->id,$win_id,$get_chall_type->subject);
                        }
                        else
                        {
                            //update DRAW status in challenge
                            $update_chal_status->winner_id='DRAW';
                            $update_chal_status->save();

                            $half=round(($get_chall_type->win_points)/2);
                            //=========================user 1 =============================//
                            $insert_winner=new points_earn;
                            $insert_winner->challenge_id=$r->challenge_id;
                            $insert_winner->question_type='play';
                            $insert_winner->student_id=$get_chall_type->userid;
                            $insert_winner->points=$half;
                            $insert_winner->save();

                            // Get user points and rank before updating for update level purpose
                            $xp_data = $this->current_xp($get_chall_type->userid);
                            $old_xp=$xp_data['old_xp'];
                            $target_rank=$xp_data['target_rank'];
                            $level_id=$xp_data['level_id'];
                            $level_name=$xp_data['level_name'];

                            // Update User xp
                            $t_ap=$old_xp+$half;
                            $update=DB::table('users')->where('email',$get_chall_type->userid)->update(['xp'=>$t_ap]);

                            $this->check_level($get_chall_type->userid);

                            // Get user points and rank after updating for update level purpose
/*                            
                            // $user_point_after=DB::table('users')->where('email',$get_chall_type->userid)->first();
                            // $new_xp=$user_point_after->xp;

                            // if($new_xp>=$target_rank && $level_id!='')
                            // {
                            //     // Store user's notify
                            //     $this->profile_update($get_chall_type->userid,$level_id,$level_name);
                            // }
*/
                            //=========================user 2 =============================//
                            $insert_winner1=new points_earn;
                            $insert_winner1->challenge_id=$r->challenge_id;
                            $insert_winner1->question_type='play';
                            $insert_winner1->student_id=$get_chall_type->opponentid;
                            $insert_winner1->points=$half;
                            $insert_winner1->save();

                            // Get user points and rank before updating for update level purpose
                            $xp_data1 = $this->current_xp($get_chall_type->opponentid);
                            $old_xp1=$xp_data1['old_xp'];
                            $target_rank1=$xp_data1['target_rank'];
                            $level_id1=$xp_data1['level_id'];
                            $level_name1=$xp_data1['level_name'];

                            // Update User xp
                            $t_ap1=$old_xp1+$half;
                            $update1=DB::table('users')->where('email',$get_chall_type->opponentid)->update(['xp'=>$t_ap1]);

                            $this->check_level($get_chall_type->opponentid);


                            // Get user points and rank after updating for update level purpose
/*
                            // $user_point_after1=DB::table('users')->where('email',$get_chall_type->opponentid)->first();
                            // $new_xp1=$user_point_after1->xp;

                            // if($new_xp1>=$target_rank1 && $level_id1!='')
                            // {
                            //     // Store user's notify
                            //     $this->profile_update($get_chall_type->opponentid,$level_id1,$level_name1);
                            // }
*/
                            // Notiify both users for challenge DRAW
                            
                            // $this->draw_alert($get_chall_type->userid,$get_chall_type->opponentid,$get_chall_type->subject,$get_chall_type->id);

                            // $this->draw_alert();
                        }   
                    }
                    else 
                    {    
                        // Send alert notification
                        $opp_data=DB::table('users')->where('email',$get_chall_type->userid)->first();
                        $user=DB::table('users')->where('email',$get_chall_type->opponentid)->first();
                        $message=$opp_data->name." challenged you in ".$get_chall_type->subject;
                        $player_id[]= $user->gcmid;
                        $content = array(
                        "en" => $message
                        );

                        $data = array(
                            "type" => 'chall_invite',
                            "chall_id"=>$get_chall_type->id
                            );
                        $this->alert_Message($player_id,$content,$data);
                    }  
                }
                $update_chal_status->save();

                if (sizeof($insert)>0) 
                {
                    $msg['data']=$insert;
                    $msg['status']='success';
                    echo json_encode($msg);
                }
                else 
                {
                    $msg['status']='fail';
                    echo json_encode($msg);
                }
            }
            else
            {
                $msg['message']='answer already given';
                $msg['status']='fail';
                echo json_encode($msg);
            }
        } 
        else 
        {
            $msg['message']='Param Mising!!';
            $msg['status']='fail';
            echo json_encode($msg);
        }
    }


    //for version 3.87

    public function answer_chall87(Request $r)
    {
        if (!empty($r->user_id) && !empty($r->challenge_id) && !empty($r->ques_id) && !empty($r->time) && !empty($r->ques_status) && !empty($r->ques_answer) && !empty($r->last_ques)) 
        {        
            $get_chall_type=challege::where('id',$r->challenge_id)->first();

            $get_pre_ques=comp_result::where('student_id',$r->user_id)
                                    ->where('challenge_id',$r->challenge_id)
                                    ->where('ques_id',$r->ques_id)
                                    ->count();
 			       
            if($get_pre_ques<1)
            {
                // give points on rihgt answer
                if ($r->ques_status=='R') 
                {
                    $insert1=new points_earn;
                    $insert1->student_id=$r->user_id;
                    $insert1->challenge_id=$r->challenge_id;
                    $insert1->question_id=$r->ques_id;
                    $insert1->question_type='play';

                    // Get user points and rank before updating for update level purpose
                    $get_xp=DB::table('users')->where('email',$r->user_id)->first();
                    $old_xp=$get_xp->xp;
                    $target_rank=0;
                    $level_id=null;
                    $level_name=null;

            
                }

                //update status that user/opponent started playing

 				$update_chal_status=challege::find($r->challenge_id);

                // store question's result status
                $insert=new comp_result;
                $insert->student_id=$r->user_id;
                $insert->challenge_id=$r->challenge_id;
                $insert->ques_id=$r->ques_id;
                $insert->ques_status=$r->ques_status;
                $insert->ques_answer=$r->ques_answer;
                $insert->time=$r->time;
                $insert->save();

                // if it is last question
                if ($r->last_ques=='yes') 
                {


	                if($get_chall_type->userid==$r->user_id)
	                {
	                    $update_chal_status->play_status=1;
	                }
	                else
	                {
	                    $update_chal_status->play_status=2;
	                }

                    $insert_time=new time_record;
                    $insert_time->student_id=$r->user_id;
                    $insert_time->challenge_id=$r->challenge_id;
                    $insert_time->time=$r->time;
                    $insert_time->save(); 

                    // find winner and give points
                    if($get_chall_type->opponentid==$r->user_id)
                    {
                    	//user create the challenge
                        $user_total=comp_result::where('student_id',$get_chall_type->userid)
                                                ->where('challenge_id',$r->challenge_id)
                                                ->where('ques_status','R')
                                                ->whereNotNull('ques_id')
                                                ->count();
                                                
                        $opponent_total=comp_result::where('student_id',$get_chall_type->opponentid)
                                                ->where('challenge_id',$r->challenge_id)
                                                ->where('ques_status','R')
                                                ->whereNotNull('ques_id')
                                                ->count();

                        if($user_total!=$opponent_total)
                        {
                            $win_id=null;
                            $insert_winner=new points_earn;
                            $insert_winner->challenge_id=$r->challenge_id;
                            $insert_winner->question_type='play';
                            
                            if ($user_total>$opponent_total) 
                            {
                                $insert_winner->student_id=$get_chall_type->userid;

                                // Get user points and rank before updating for update level purpose
                                $xp_data = $this->current_xp($get_chall_type->userid);
                                $old_xp=$xp_data['old_xp'];
                                $target_rank=$xp_data['target_rank'];
                                $level_id=$xp_data['level_id'];
                                $level_name=$xp_data['level_name'];
        
                                // Update user xp
                                $t_ap=$old_xp+$get_chall_type->win_points;
                                $update=DB::table('users')->where('email',$get_chall_type->userid)->update(['xp'=>$t_ap]);
        
                                // Get user points and rank after updating for update level purpose

                                $this->check_level($get_chall_type->userid);

                                //update winner status in challenge
                                $update_chal_status->winner_id=$get_chall_type->userid;

                                // Get win ID
                                $win_id=$get_chall_type->userid;
                            } 
                            else 
                            {
                                $insert_winner->student_id=$get_chall_type->opponentid;

                                // Get user points and rank before updating for update level purpose
                                $xp_data = $this->current_xp($get_chall_type->opponentid);
                                $old_xp=$xp_data['old_xp'];
                                $target_rank=$xp_data['target_rank'];
                                $level_id=$xp_data['level_id'];
                                $level_name=$xp_data['level_name'];

                                // Update User xp
                                $t_ap=$old_xp+$get_chall_type->win_points;
                                $update=DB::table('users')->where('email',$get_chall_type->opponentid)->update(['xp'=>$t_ap]);

                                // Get user points and rank after updating for update level purpose

                                $this->check_level($get_chall_type->opponentid);

                              
                                //update winner status in challenge
                                $update_chal_status->winner_id=$get_chall_type->opponentid;

                                // Get win ID
                                $win_id=$get_chall_type->opponentid;
                                
                            }
                            $insert_winner->points=$get_chall_type->win_points;
                            $insert_winner->save();
                            
                            // Notiify both users for challenge winning

                            $this->win_alert($get_chall_type->userid,$get_chall_type->id,$win_id,$get_chall_type->subject);
                            $this->win_alert($get_chall_type->opponentid,$get_chall_type->id,$win_id,$get_chall_type->subject);
                        }
                        else
                        {
                            //update DRAW status in challenge
                            $update_chal_status->winner_id='DRAW';
                            $update_chal_status->save();

                            $half=round(($get_chall_type->win_points)/2);
                            //=========================user 1 =============================//
                            $insert_winner=new points_earn;
                            $insert_winner->challenge_id=$r->challenge_id;
                            $insert_winner->question_type='play';
                            $insert_winner->student_id=$get_chall_type->userid;
                            $insert_winner->points=$half;
                            $insert_winner->save();

                            // Get user points and rank before updating for update level purpose
                            $xp_data = $this->current_xp($get_chall_type->userid);
                            $old_xp=$xp_data['old_xp'];
                            $target_rank=$xp_data['target_rank'];
                            $level_id=$xp_data['level_id'];
                            $level_name=$xp_data['level_name'];

                            // Update User xp
                            $t_ap=$old_xp+$half;
                            $update=DB::table('users')->where('email',$get_chall_type->userid)->update(['xp'=>$t_ap]);

                            $this->check_level($get_chall_type->userid);

                            //=========================user 2 =============================//
                            $insert_winner1=new points_earn;
                            $insert_winner1->challenge_id=$r->challenge_id;
                            $insert_winner1->question_type='play';
                            $insert_winner1->student_id=$get_chall_type->opponentid;
                            $insert_winner1->points=$half;
                            $insert_winner1->save();

                            // Get user points and rank before updating for update level purpose
                            $xp_data1 = $this->current_xp($get_chall_type->opponentid);
                            $old_xp1=$xp_data1['old_xp'];
                            $target_rank1=$xp_data1['target_rank'];
                            $level_id1=$xp_data1['level_id'];
                            $level_name1=$xp_data1['level_name'];

                            // Update User xp
                            $t_ap1=$old_xp1+$half;
                            $update1=DB::table('users')->where('email',$get_chall_type->opponentid)->update(['xp'=>$t_ap1]);

                            $this->check_level($get_chall_type->opponentid);

                            
                            // $this->draw_alert($get_chall_type->userid,$get_chall_type->opponentid,$get_chall_type->subject,$get_chall_type->id);

                            // $this->draw_alert();
                        }   
                    }
                    else 
                        
                    {    
                        // Send alert notification
                        $opp_data=DB::table('users')->where('email',$get_chall_type->userid)->first();
                        $user=DB::table('users')->where('email',$get_chall_type->opponentid)->first();
                        $message=$opp_data->name." challenged you in ".$get_chall_type->subject;
                        $player_id[]= $user->gcmid;
                        $content = array(
                        "en" => $message
                        );

                        $data = array(
                            "type" => 'chall_invite',
                            "chall_id"=>$get_chall_type->id
                            );
                        $this->alert_Message($player_id,$content,$data);
                    }  
                }

                $update_chal_status->save();

                if (sizeof($insert)>0) 
                {
                    $msg['data']=$insert;
                    $msg['status']='success';
                    echo json_encode($msg);
                }
                else 
                {
                    $msg['status']='fail';
                    echo json_encode($msg);
                }
            }
            else
            {
                $msg['message']='answer already given';
                $msg['status']='fail';
                echo json_encode($msg);
            }
        } 
        else 
        {
            $msg['message']='Param Mising!!';
            $msg['status']='fail';
            echo json_encode($msg);
        }
    }

// leader board api for all users

	public function leaderboard(Request $r){

        if($r->time=='Today')
        {   
            // echo 'Today';   
            $data = DB::table("points_earns")
                ->select(DB::raw("SUM(points) as point,student_id"))
                ->whereDay('created_at',date('d'))
                ->groupBy("student_id")
                ->paginate(20);
        }
        else if($r->time=='This Month')
        {       

            // echo 'This Month';

            $data = DB::table("points_earns")
                ->select(DB::raw("SUM(points) as point,student_id"))
                ->whereMonth('created_at',date('m'))
                ->groupBy("student_id")
                ->orderBy("point","DESC")
                ->paginate(20);
        }
        else if($r->time=='This Year'){

            // echo date('yy');
            $data = DB::table("points_earns")
                ->select(DB::raw("SUM(points) as point,student_id"))
                ->groupBy("student_id")
                ->whereYear('created_at',date('Y'))
                ->orderBy("point","DESC")
                ->paginate(20);
        }
        else{

            // echo 'All Time';

            $data = DB::table("points_earns")
                ->select(DB::raw("SUM(points) as point,student_id"))
                ->groupBy("student_id")
                ->orderBy("point","DESC")
                ->paginate(20);
        }

        $arr=array();
        $res=array();


        if(sizeof($data)>0){

            foreach ($data as $value) {

                $info=DB::table("users")->where('email',$value->student_id)->first();

                $value->name=$info->name;
                $value->image=$info->image;
                 $value->level=$info->level;
                $value->college=$info->college;
                $value->city=$info->city;

                $arr['users'][]=$value;
            }
          $res['status']='true';
          $res['data']=$arr;
            
        }
        else{
            
          $res['status']='false';
          $res['data']=$arr;
            
        }

        echo json_encode($res);

    }

// get challenge points and rules

  public function getChallengePoints(Request $r)

     {

     	$userinfo=DB::table('users')->select("level","xp")->Where('email',$r->userid)->first();
	


		if($userinfo)
		{
				$lev_name=DB::table('challenge_level')->Where('level',$userinfo->level)->first();
				if($lev_name)
					$res['levelname']=$lev_name->name;
				else
					$res['levelname']='Newcomer';

				$res['level']=$userinfo->level;
		    	$res['xp']=$userinfo->xp;
		    	$res['status']='true';
		    	$res['message']='success';	

			$data=DB::table('challenge_point')->Where('id',1)->first();
			if($data)
			{	
		    	$res['data']=$data;		
		    				
	    	}      
	    	else{
	    		$res['status']='false';	
	    		$res['message']='Something went wrong please try after some time';
	    	}
	    }
	    else{
	    	$res['status']='false';	
	    		$res['message']='user not exist';
	    }	      
            echo json_encode($res);
    }   

// request for withdraw money

    public function withdrawmoney(Request $r){

     	$userinfo=DB::table('users')->select("level","xp")->Where('email',$r->userid)->first();
	
		if($userinfo)
		{

			// get user current level for request
			$check_lev1=DB::table('challenge_level')
			->Where('point','<=',$userinfo->xp)->orderBy('point','desc')->first();

				if($check_lev1)
				{	

					//get previous request level
					$check_req=DB::table('withdraw_request')->where('user_id',$r->userid)->orderBy('id','desc')->first();

					if($check_req)  // if user request is exist
					{ 
						if($check_lev1->level>$check_req->level_for)  // if user level is greater to previous request
						{
							$inst=DB::table('withdraw_request')->insert(
							   ['user_id' => $r->userid,'level_for' => $check_lev1->level]);

							$res['status']='true';	
						   	$res['message']='Your request has been submitted successfully our team will contact you shortly';
					   }
					   else
					   {
					   		if($check_req->status)
					   		{	
						   		$res['status']='true';	
							    $res['message']='not reach the withdraw for next level';
							}
							else
							{
								$res['status']='true';	
							    $res['message']='Your Request Already Submitted Please Wait For Reply';
							}
					   }

					}
					else   // if the  user request first time
					{
							$inst=DB::table('withdraw_request')->insert(
							   ['user_id' => $r->userid,'level_for' => $check_lev1->level]);

							$res['status']='true';	
						   	$res['message']='Your request has been submitted successfully our team will contact you shortly';
					}   		
						
				}
				else
				{
					$res['status']='true';	
				 	$res['message']='not reach the withdraw level';
				}
	    }
	    else{
	    	$res['status']='false';	
	    		$res['message']='user not exist';
	    }	      
            echo json_encode($res);
    }   


//**************************************Calling function****************************************************** *///
    // -------------notify and alert on challenge win
    public function win_alert($user_id,$chall_id,$win_id,$course_name)
    {
        // Send alert notification
        $user=DB::table('users')->where('email',$user_id)->first();
        $win=DB::table('users')->where('email',$win_id)->first();

        $user_chall=challege::where('id',$chall_id)->first();
        if ($user_id==$user_chall->winner_id) 
        {
            $message="You won a challenge in ".$course_name;
        } 
        else 
        {
            $message=$win->name." won a challenge in ".$course_name;
        }
        
        $player_id[]= $user->gcmid;
        $content = array(
        "en" => $message
        );

        $data = array(
            "type" => 'chall_result',
            "chall_id"=>$chall_id
            );
        $this->alert_Message($player_id,$content,$data);
    }

	//-------------- increases level-------------------//

 //------------------get current xp------------------//
    public function check_level($user_id)
    {
        $get_xp=DB::table('users')->where('email',$user_id)->first();

        // Get user points and rank before updating for update level purpose
        $user_xp=$get_xp->xp;

        $level=DB::table('challenge_level')->where('point','>=',$user_xp)->orderBy('point','asc')->first();

        if($level){
        	if($get_xp->level==$level->level){

        	}
        	else if($get_xp->level<$level->level){
        		$update1=DB::table('users')->where('email',$user_id)->update(['level'=>$level->level]);

		 	$message="Congratulations you have reached level ".$level->name." now.";
		        $player_id[]= $get_xp->gcmid;
		        $content = array(
		        "en" => $message
		        );

		        $data = array(
		            "type" => 'level',
		            "level_id"=>$level->level
		            );

		        $this->alert_Message($player_id,$content,$data);


        	}
        }


    }

    //------------------get current xp------------------//
    public function current_xp($user_id)
    {
        $get_xp=DB::table('users')->where('email',$user_id)->first();

        // Get user points and rank before updating for update level purpose
        $old_xp=$get_xp->xp;
        $target_rank=0;
        $level_id=null;
        $level_name='';

        $rank_level=rank_level::get();
        foreach($rank_level as $rank)
        {
            if($rank->point>$old_xp)
            {
                $target_rank=$rank->point;
                $level_id=$rank->id;
                $level_name=$rank->name;
                break;
            }
        } 

        return array('old_xp' => $old_xp, 'target_rank' => $target_rank,'level_id'=> $level_id,'level_name'=>$level_name);
    }

    // --------update user profile notification history---------------------//
    public function profile_update($user_id,$level_id,$level_name)
    {
        // Send alert notification
        $user=DB::table('users')->where('email',$user_id)->first();
        $message="You have reached level ".$level_name." now.";
        $player_id[]= $user->gcmid;
        $content = array(
        "en" => $message
        );

        $data = array(
            "type" => 'level',
            "level_id"=>$level_id
            );

        $this->alert_Message($player_id,$content,$data);
    }

    //---------------------------- alert_Message-------------------------------------//
	public function alert_Message($player_id,$content,$data)
	{
		  $fields = array( 
					  'app_id' => '306439b4-709b-468e-b687-d0f9e6917268', 
					  'include_player_ids' =>$player_id,
                      'contents' => $content,
                      'data' => $data
					  ); 

			$fields = json_encode($fields);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
						'Authorization: Basic MDQyY2E5MTEtMzdhZS00N2EwLWI4N2UtZTM3NGFlMjcxYWUw'));
		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		
			$response = curl_exec($ch);
			curl_close($ch);

            return $response;
	}


//----------------------------draw alert_Message-------------------------------------//
	public function draw_Message($userid,$opp_id,$course_name,$chall_id)

	{   $user=DB::table('users')->where('email',$userid)->first();

        $opp=DB::table('users')->where('email',$opp_id)->first();


      
            $foruser="challenge draw in ".$course_name." ".$user->name;
             $foropp="challenge draw in ".$course_name." ".$opp->name;
        
        $player_id[]= $user->gcmid;

        $content1 = array(
        "en" => $foruser
        );

        $data1 = array(
            "type" => 'draw',
            "chall_id"=>$chall_id
            );

         $content2 = array(
        "en" => $foruser
        );

        $data2 = array(
            "type" => 'chall_result',
            "chall_id"=>$chall_id
            );

        $this->alert_Message($user->gcmid,$content1,$data1);
        $this->alert_Message($opp->gcmid,$content1,$data1);
	
	}


}
