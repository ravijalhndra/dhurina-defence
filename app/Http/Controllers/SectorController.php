<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Sector;
use View;

class SectorController extends Controller
{
    public function add_sector(Request $r)
    {
        $title='Add Sector';
        return view::make('add_sector',compact('title'));
    }

    public function insert_sector(Request $r)
    {

       if($r->name){

            $check=Sector::where('name',$r->name)->first();
            if(!$check){
                $insert=new Sector;
                $insert->name=$r->name;

                $file = $r->file('banner');
                if ($file!='' && $file!='null') {
                    $rand = rand(1, 1000000);
                    $destinationPath = public_path().'/img';
                    $filename = $file->getClientOriginalName();
                    $filename = str_replace(' ', '_', $filename);
                    $filename = $rand.$filename;
                    $upload = $file->move($destinationPath, $filename);
                }
                else 
                {
                    $filename = '';
                }


                $insert->image=$filename;
                $save=$insert->Save();
                if($save){
                    return redirect()->route('view_sector')->with('success','Sector has been added successfully.');
                }else{
                    return back()->with('error','Something went wrong,Please try again later.');
                }
            }
            else{
                return back()->with('error','Sorry,Sector name already exist, Please try new one.');
            }   

       }
       else{
        return back()->with('error','Sorry,Sector could not be empty.');
       }
    }

    
    public function view_sector(Request $r)
    {
        $title='View Sector';
        $data=Sector::OrderBy('id','DESC')->get();
        return view::make('view_sector', compact('title','data'));
    }

    public function edit_sector(Request $r)
    {
        $title='Edit Sector';
        $id=decrypt($r->id);
        $data=Sector::find($id);
        return view::make('edit_sector', compact('title','data'));
    }

    public function update_sector(Request $r)
    {
        $checked=Sector::whereNotIn('id',[$r->id])->where('name',$r->name)->first();
        if(!$checked){
            $check=Sector::find($r->id);
            if($check){

                    $check->name=$r->name;

                    $file = $r->file('banner');
                    if ($file!='' && $file!='null') {
                        $rand = rand(1, 1000000);
                        $destinationPath = public_path().'/img';
                        $filename = $file->getClientOriginalName();
                        $filename = str_replace(' ', '_', $filename);
                        $filename = $rand.$filename;
                        $upload = $file->move($destinationPath, $filename);

                        $check->image=$filename;
                    }

                    $save=$check->save();
                    if($save){
                        return redirect()->route('view_sector')->with('success','Sector has been updated successfully.');
                    }else{
                        return back()->with('error','Something went wrong,Please try again later.');
                    }
            }
            else {
                return back()->with('error','Sorry,Sector could not be empty.');
            }
        }
        else{
            return back()->with('error','Sorry,Sector name already exist, Please try new one.');
        } 
    }

    public function delete_sector(Request $r)
    {       
        $check=Sector::find($r->del_id);
        if($check){
            $save=$check->delete();
            if($save){
                return redirect()->route('view_sector')->with('success','Sector has been deleted successfully.');
            }else{
                return back()->with('error','Something went wrong,Please try again later.');
            }
        }
        else {
         return back()->with('error','Sorry,Sector could not be empty.');
        }
    }

    public function sector_move(Request $r)
    {
        $all_course=DB::table('new_courses')->get();

        foreach($all_course as $d){
            $name=$d->sector;

            $check=Sector::where('name',$name)->first();
            if($check){
                $update = DB::table('new_courses')->where('sector',$name)->update(['sector'=>$check->id]);
            }
        }
    }
    
}
