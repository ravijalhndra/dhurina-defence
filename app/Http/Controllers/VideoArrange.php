<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\video_arrange;
use App\video_duration;
use App\User;
use DB;
use Hash;
use Redirect;
use Auth;
use View;

class VideoArrange extends Controller
{
    
    public function playlist_subject(Request $r)
    {
        $course_id=decrypt($r->course_id);
        $new_Courses=DB::table('new_courses')->where('id',$course_id)->orderBy('name','ASC')->first();


        $sub=array(); 
        $sub=array_unique(array_merge(json_decode($new_Courses->subject),$sub));            

        $subject=DB::table('subject')->whereIn('id',$sub)->get(); 
        
        return view::make('playlist_subject', compact('subject','new_Courses'));
    }

    public function playlist_video(Request $r)
    {
        $course_id=$r->course_id;        
       

        $data=DB::table('video')->whereNotNull('thumbnail')->where('publish','true')
                ->whereRaw('json_contains(course_id, \'["'.$r->course_id.'"]\')')
                ->where(function ($q) use($r) {
                    foreach($r->subject_id as $key => $ord)
                    {                        
                        if($key == 0)
                        {
                            $q->whereRaw('json_contains(subject_id, \'["'.$ord.'"]\')');    
                        }
                        else 
                        {
                            $q->orWhereRaw('json_contains(subject_id, \'["'.$ord.'"]\')');    
                        }
                    }
                })->orderBy('id','desc')->paginate(50);
        
        if($this->check_count($data) > 0)
        {
            foreach($data as $d)
            {
                $d->course_name='';                
                $course=DB::table('new_courses')->select('name')->whereIn('id',json_decode($d->course_id) )->get();
                if($this->check_count($course) > 0)
                {
                    $cou_name=[];
                    foreach($course as $cou)
                    {
                        $cou_name[]=$cou->name;
                    }
                     
                    $d->course_name=implode(",",$cou_name); 
                }

                $d->subject_name='';
                $subject=DB::table('subject')->select('name')->whereIn('id',json_decode($d->subject_id) )->get();
                if($this->check_count($subject) > 0)
                {
                    $sub_name=[];
                    foreach($subject as $s)
                    {
                        $sub_name[]=$s->name;
                    }

                    $d->subject_name=implode(",",$sub_name); 
                } 
                
                if(Auth::user()->role == 'school' )
                {
                    $video_access=DB::table('video_school_access')->where('school_id',Auth::user()->id )->where('video_id',$d->id)->first();
                    if($video_access)
                    {
                        $d->publish='true';                        
                    }
                    else {
                        $d->publish='false';
                    }
                }
                
            }

        }        

        $subject_id=implode(",",$r->subject_id);

        return view::make('playlist_video', compact('data','course_id','subject_id'));
    }


    public function save_playlist(Request $r)
    {
        print_r($r->arrange);
    }


    //Video Duration
    public function video_duration(Request $r)
    {
        $title='Video Duration';
        $id=decrypt($r->id);
        $user_detail=DB::table('users')->where('id',$id)->first();
        $users=video_duration::where('user_id',$id)->orderby('id','DESC')->paginate(100);

        return view::make('video_duration', compact('users','title','user_detail'));
    }


}
