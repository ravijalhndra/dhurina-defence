<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;


class CashfreeController extends Controller
{
    public function view_payment(Request $r)
    {
        $filter=$r->filter;

        $users=DB::table('order')->join('users', 'users.id', '=', 'order.user_id')->select('order.*','users.referralcode');
            if($filter != '')
            {
                $users=$users->orWhere('order.order_id','like','%'.$filter.'%')
                ->orWhere('order.email',$filter)
                ->orWhere('order.mobile',$filter)
                ->orWhere('order.name',$filter)            
                ->orWhere('order.course_name',$filter)
                ->orWhere('users.referralcode',$filter);
            }
            if($r->status != '')
            {
                $users=$users->where('order.status',$r->status); 
            }
            $users=$users->orderBy('order.id','desc')->paginate('100');

        $users->appends(request()->input())->links();
       
        return view('view_payment',compact('users'));


    }
}
