<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Youtube;
use DB;
use Hash;
use Redirect;
use Auth;
use View;

class YoutubeController extends Controller
{

    public function upload_req(Request $r )
    {
      
        return view('youtubevideo');
    }
    
    //Demmo upload 
    public function upload_youtube(Request $request)
    {
        try {
           
            $video = Youtube::upload($request->file('video')->getPathName(), [
                'title'       => $request->input('title'),
                'description' => $request->input('description')
            ]);
     
            return "Video uploaded successfully. Video ID is ". $video->getVideoId();

        } catch (\Exception $e) {
            print_r( $e->getMessage());
        }
           
    }

}
