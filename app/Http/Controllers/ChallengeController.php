<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Response;
use View;
use Auth;

class ChallengeController extends Controller
{
//<------------Challenge Level-------------->

  public function challengelevel()
  {
    $data=DB::table('challenge_level')->get();
    return view::make('view-challenge-level',compact('data'));
  }

  public function pendingrequests()
  {
    $data=DB::table('withdraw_request')->orderBy('status','asc')->orderBy('id','DESC')->paginate(10);
    return view::make('pendingrequests',compact('data'));
  }

  public function assignamount($id){
    $data=DB::table('withdraw_request')->where('id',$id)->first();
    if($data)
    {
        $level=DB::table('challenge_level')->where('level',$data->level_for)->first();
        $rand = rand($level->minimum_amount,$level->money);
    
        $update=DB::table('withdraw_request')->where('id', $id)->update(['payment' => $rand, 'status' => 1]);
    
        if($update)
        {
            return redirect()->back()->with('success',"Verify Assigned $rand Rupees."); 
        }
        else{
            return redirect()->back()->with('error','Something Went Wrong');
        }
    
      
    }
    else
    {
      return redirect()->back()->with('error','Something Went Wrong');
    }
   
  }
  public function add_challenge_level()
  {
    $data = DB::table('challenge_level')->orderBy('id', 'DESC')->first();
    return view::make('add_challenge_level',compact('data'));
  }

  public function _add_challenge_data(Request $r)
  {
    $level=$r->level;
    $point=$r->point;
    $name=$r->name;
    $data=DB::table('challenge_level')->insert(['point' => $point, 'level' => $level, 'name' => $name]);
    if($data)
    {
     return redirect()->route('challengelevel');
    }
    else{
      return redirect()->back();
    }
  }

  public function get_last_data()
  {
     $data = DB::table('challenge_level')->orderBy('id', 'DESC')->first();
     return Response::json($data);
  }

  public function edit_clevel($id)
  {
  $cid=$id;
  $user = DB::table('challenge_level')->where('id',$id)->first();
  return view::make('edit_challenge_level',compact('user'));
  }

   public function update_clevel(Request $r)
  {
    $cid=$r->id;
    $level=$r->level;
    $point=$r->point;
    $name=$r->name;
    $data=DB::table('challenge_level')->where('id', $cid)->update(['point' => $point, 'level' => $level, 'name' => $name]);

     if($data)
    {
     return redirect()->route('challengelevel');
    }
    else{
      return redirect()->back();
    }
  }
   public function _get_clevel_data(Request $r)
   {
      $cid=$r->id;
      $data=array();
      $data['current_level']=DB::table('challenge_level')->where('id', $cid)->first();
      $data['next_level']=DB::table('challenge_level')->where('id', $cid+1)->first();
      return Response::json($data);
      
   }
  


 //  <---------------------------Edit Challenge Point-------------------->  
 public function edit_challenge()
 {
   $data=DB::table('challenge_point')->where('id', 1)->first();
   return view::make('challenge_point',compact('data'));
 }

 public function _edit_challenge(Request $req)
 {
   $id=$req->id;
   $description=$req->description;
   $min=$req->min;
   $max=$req->max;
   $request_level=$req->request_level;
   $message=$req->message;
   DB::table('challenge_point') ->where('id',$id)->update(['description' =>$description,'min'=>$min,'max'=>$max,'request_level'=>$request_level,'message'=>$message]);
   $data=DB::table('challenge_point')->where('id', 1)->first();
   return view::make('challenge_point',compact('data'));
 }
 
    //       <-------------View challenged data------------------------>

    public function view_challenge()
    {
     

      $data=DB::table('challeges')->where('play_status','2')->orderBy('id','dsc')->paginate(10)->appends(request()->query());

        $next_page_url=$data->nextPageUrl();
        if($data->currentPage()>1)
        {
            $json='true';
        }
        else
        {
            $json='false';
        }
      
      
     




     
      foreach($data as $d)
      {
          $dd= DB::table('users')->select('name as user_name')->where('email',$d->userid)->first();
          $dd1= DB::table('users')->select('name as opponent_name')->where('email',$d->opponentid)->first();
          $winner_name= DB::table('users')->select('name as opponent_name')->where('email',$d->opponentid)->first();
          $d->user_name=$dd->user_name;
          $d->opponent_name =$dd1->opponent_name;
      }


      
      return view::make('view_challenge',compact('data','next_page_url'));
    
    }

    //<----------------View Pending Challenge------------------>

    public function pending_challenge()
    {
      $data=DB::table('challeges')->where('play_status','0')->orWhere('play_status','1')->orderBy('id','DESC')->paginate(20);
      
      foreach($data as $d)
      {
          $dd= DB::table('users')->select('name as user_name')->where('email',$d->userid)->first();
          $dd1= DB::table('users')->select('name as opponent_name')->where('email',$d->opponentid)->first();
          //$winner_name= DB::table('users')->select('name as opponent_name')->where('email',$d->opponentid)->first();
          $d->user_name=$dd->user_name;
          $d->opponent_name =$dd1->opponent_name;
      }
      return view::make('pending_challenge',compact('data'));
    
    }

    public function leadership()
    {
      $subject=DB::table('test_question')->distinct()->select('sub_test_name')->get();
      $dept=DB::table('department')->get();
      $data=DB::table('points_earns')->select(DB::raw('sum(points) as points'),'student_id')->orderBy('points', 'DESC')->groupBy('student_id')->get();
      foreach($data as $d)
      {
        $dd= DB::table('users')->select('name as user_name')->where('email',$d->student_id)->first();
        $d->user_name=$dd->user_name; 
      }
      return view('view_leadership', compact('subject','dept','data'));
    }

    public function filter_leadership(Request $r)
    {
     $subject=$r->subject;

     $time=$r->time;
    
     $subject=DB::table('challeges')->select('id')->where('subject',$subject)->get();
     $ids=array();
      foreach($subject as $s)
      {
        $ids[]=$s->id;

      }


      
      $ldate = date('Y-m-d');
      $week_ago=date('Y-m-d', strtotime($ldate. ' - 7 days'));
      $month=date('Y-m-d', strtotime($ldate. ' - 30 days'));
     
      $year=date('Y-m-d', strtotime($ldate. ' - 365 days'));
      if($time=='today')
      {
      $sub=DB::table('points_earns')->select(DB::raw('sum(points) as points'),'student_id')->whereIn('challenge_id',$ids)->whereDate('created_at','=',$ldate)->groupBy('student_id')->get();
      }
      else if($time == 'week')
      {
      $sub=DB::table('points_earns')->select(DB::raw('sum(points) as points'),'student_id')->whereIn('challenge_id',$ids)->whereDate('created_at','<=', $ldate)->whereDate('created_at','>=', $week_ago)->groupBy('student_id')->get();
      }
      else if($time == 'month')
      {
      $sub=DB::table('points_earns')->select(DB::raw('sum(points) as points'),'student_id')->whereIn('challenge_id',$ids)->whereDate('created_at','<=', $ldate)->whereDate('created_at','>=', $month)->groupBy('student_id')->get();
      }
      else if($time == 'year')
      {
      $sub=DB::table('points_earns')->select(DB::raw('sum(points) as points'),'student_id')->whereIn('challenge_id',$ids)->whereDate('created_at','<=', $ldate)->whereDate('created_at','>=', $year)->groupBy('student_id')->get();
      }
      foreach($sub as $s)
      {
        $dd= DB::table('users')->select('name as user_name')->where('email',$s->student_id)->first();
        $s->user_name=$dd->user_name; 
      }
     
      $dept=DB::table('department')->get();

      return response($sub);
      
    }

    public function add_sub_ques()
    {
      $topic=DB::table('subject_question')->select('sub_test_name')->get();
      return view::make('add_subject_question',compact('topic'));
    }

    public function submit_sub_question(Request $request)
    {
        $all=$request->all();
        if($request->question=="")
        {
            $question="NULL";
        }
        else
        {
            $question=$request->question;
        }
        
        if($request->option_a=="")
        {
            $option_a="NULL";
        }
        else
        {
            $option_a=$request->option_a;
        }
        
        if($request->option_b=="")
        {
            $option_b="NULL";
        }
        else
        {
            $option_b=$request->option_b;
        }
        
        if($request->option_c=="")
        {
            $option_c="NULL";
        }
        else
        {
            $option_c=$request->option_c;
        }
        
        if($request->option_d=="")
        {
            $option_d="NULL";
        }
        else
        {
            $option_d=$request->option_d;
        }
        
        if($request->option_e=="")
        {
            $option_e="NULL";
        }
        else
        {
            $option_e=$request->option_e;
        }
        
        if($request->answer=="")
        {
            $answer="NULL";
        }
        else
        {
            $answer=$request->answer;
        }
        
        if($request->hint=="")
        {
            $hint="NULL";
        }
        else
        {
            $hint=$request->hint;
        }
        
        if($request->solution=="")
        {
            $solution="NULL";
        }
        else
        {
            $solution=$request->solution;
        }

        $userId = Auth::id();
        $user_name=Auth::user()->name;
        $user_email=Auth::user()->email;
       
        // if($request->ans_status)
        // {
        //     $ans_status=$request->ans_status;
        // }
        // else
        // {
        //     $ans_status="no";
        // }

       
       

              
                    $question_image_name="NULL";
               
                    $option_a_image_name="NULL";
               
            
                    $option_b_image_name="NULL";
               
                    $option_c_image_name="NULL";
               
                    $option_d_image_name="NULL";
               
                    $option_e_image_name="NULL";
                
                    $answer_image_name="NULL";
              
               
                    $hint_image_name="NULL";
               
               
                    $solution_image_name="NULL";
               

            $question_data=[];
            $question_data['data']['question'] = array(
            'value' => $question,
            'image' => $question_image_name
            );
            $question_data['data']['option a'] = array(
            'value' => $option_a,
            'image' => $option_a_image_name,
            'orignal' => 'a'
            );
            $question_data['data']['option b'] = array(
            'value' => $option_b,
            'image' => $option_b_image_name,
            'orignal' => 'b'
            );
            $question_data['data']['option c'] = array(
            'value' => $option_c,
            'image' => $option_c_image_name,
            'orignal' => 'c'
            );
            $question_data['data']['option d'] = array(
            'value' => $option_d,
            'image' => $option_d_image_name,
            'orignal' => 'd'
            );
            $question_data['data']['option e'] = array(
            'value' => $option_e,
            'image' => $option_e_image_name,
            'orignal' => 'e'
            );
            $question_data['data']['answer'] = array(
            'value' => $answer,
            'image' => $answer_image_name
            );
            $question_data['data']['hint'] = array(
            'value' => $hint,
            'image' => $hint_image_name
            );
            $question_data['data']['solution'] = array(
            'value' => $solution,
            'image' => $solution_image_name
            );
             $question_data['data']['by'] = array(
            'uid' => $user_email,
            'name' =>  $user_name
            );
            // $question_data['data']['ans_status'] = $ans_status;

        $json=json_encode($question_data);
      

       
        if($request->new_subject!='null' && !empty($request->new_subject))
        {
          $new_subject_name=$request->new_subject;
        
        $ins=DB::table('subject_question')->insert(
            ['sub_test_name' => $new_subject_name]
        );
        $insert=DB::table('test_question')->insert(['testid'=>0,'sub_test_name'=>$new_subject_name,'jsondata'=>$json]);

       }
       else
       {
        $sub_name=$request->subject_name;
        $insert=DB::table('test_question')->insert(['testid'=>0,'sub_test_name'=>$sub_name,'jsondata'=>$json]);
       }
        if($insert)
        {
            return redirect()->route('view_sub_question')->with('success',"Question added Successfully.");
        }
        else
        {
            return redirect()->back()->with('Warning',"Something  Went Wrong");
        }
    }

    public function view_sub_question()
    {
      $query=DB::table('test_question')->where('testid',0)->orWhere('testid',1)->get();
      return view::make('view_subject_question',compact('query'));
    }

    public function verify_question($id)
    {
     $data=DB::table('test_question')->where('id',$id)->first();
     if($data->testid==0)
     {
      $testid=1;
     }
     else
     {
      $testid=0;
     }

    $update=DB::table('test_question')->where('id',$id)->update(['testid'=>$testid]); 
    if($update)
    {
    
      return redirect()->route('view_sub_question')->with('success','Verify Successfully'); 
    }
    else
    {
      return redirect()->back()->with('error','Something Went Wrong');
    }
  }

  public function delete_question(Request $r)
  {
    $id=$r->id;
    $delete=DB::table('test_question')->where('id',$id)->delete(); 
    if($delete)
    {
      $query=DB::table('test_question')->where('testid',0)->orWhere('testid',1)->get();
      return view::make('view_subject_question',compact('query'))->with('success','Verify Successfully'); 
    }
    else
    {
      return redirect()->back()->with('error','Something Went Wrong');
    }


  }

   
}
