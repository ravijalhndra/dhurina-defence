<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Academy_Info;
use App\Institute;
use App\SentNotification;
use Hash;
use Mail;
use Auth;
use View;
use DB;
use App\Mail\AddEmail;
use Zipper;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use App\Jobs\PremiumTestResult;
use Carbon\Carbon;
use Redirect;

use App\Traits\Common;

class GeneralController extends Controller
{
    use Common;


    public function chat_webview_front(Request $r)
    {
        $title='chat-webview';
        $link=$r->id;
        $url="https://vimeo.com/live-chat/".$link."/";
        return view::make('chat_webview_front',compact('title','url'));
    }
    

    public function change_password(Request $request)
    {
    	$password=Auth::user()->password;
    	$id=Auth::user()->id;
    	if(Hash::check($request->old_password,$password))
    	{
    		if($request->new_password==$request->confirm_password)
    		{
    			$new_password=Hash::make($request->new_password);
				  $change=User::find($id);
    			$change->password=$new_password;
    			$change->save();
    			return redirect('dashboard')->with('success','Password Successfully Changed.');
    		}
    		else
    		{
    			return redirect('change_password')->with('error','Confirm Password did not match.');
    		}
    		
    	}
    	else
    	{
    		return redirect('change_password')->with('error','You enterd the wrong old Password.');
    	}
    
    }

    public function view_posts()
    {
        $field_type=1;
        $record=DB::table('common_table')->where('field_type',$field_type)->orderBy('id','desc')->paginate(5);
        $next_page_url=$record->nextPageUrl();
        if($record->currentPage()>1)
        {
            $json='true';
        }
        else
        {
            $json='false';
        }

        $new_record=[];
    foreach ($record as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec->groupid;
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
    	$new_record=$record;
    }
    if(!empty($new_record))
    {
      foreach ($new_record as $key => $new_rec) {
      $notification=DB::table('sent_notifications')->where('postid',$new_rec->id)->first();
            if($notification)
            {
                $new_rec->notification="sent";
            }
            else
            {
                $new_rec->notification="";
            }

        $comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$new_rec->id)->get();
        foreach ($comments as $comment) {
            $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
            foreach ($reply as $key => $rep) {
              $comment->reply[]=$rep;
            }
          $new_rec->comments[]=$comment;
        }
      }
    } 
    $record=$new_record;
    if($json=='true')
    {
        // return response()->json(['data'=>$record,'status'=>'success']);
        return view::make('view_post_page',compact('record','next_page_url'));
    }
    
        return view::make('view_posts',compact('record','next_page_url'));
    }


    public function add_posts()
    {
        $query=DB::table('topic_search')->get();
        if(Auth::user()->groups=="all")
        {
          $groups1=DB::table('discussions')->orderBy('id','desc')->get();
          foreach ($groups1 as $group){
            $groups[]=$group->id;
          }
        }
        else
        {
          $user_groups=Auth::user()->groups;
          if(strrchr($user_groups,","))
          {
            $groups=explode(",",$user_groups);
          }
          else
          {
            $groups[]=$user_groups;
          }
        }
        foreach($groups as $group)
        {
        $data[]=DB::table('discussions')->where('id',$group)->first();
        }
        $course = DB::table('course')->get();
        return view::make('add_posts',compact('query','data','course'));
    }


    public function submit_posts(Request $request)
    {
       
        $all=$request->all();
        if($request->add_comment)
        {
            $add_comment=$request->add_comment;
        }
        else
        {
            $add_comment="on";
        }
        if($request->add_like)
        {
            $add_like=$request->add_like;
        }
        else
        {
            $add_like="on";
        }

        if($request->type_institute=="group")
        {
        $groupid=implode(",", $request->groupid);
        }
        else
        {
            $groupid = 0;
            $course = $request->course;
            $college = $request->college;
            $stream = $request->stream;
            $year = $request->year;
        }
        $file = $request->file('pic');
                if($file) 
                {
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/newsimage";
                    // dd($destinationPath);
                    // Get the orginal filname or create the filename of your choice
                    $filename = $file->getClientOriginalName();
                    $filename=$rand.$filename;
                    $upload=$file->move($destinationPath, $filename);
                }
                else
                {
                    $filename="default_pic.png";
                }
        $file1 = $request->file('thumbnail');
                if($file1) 
                {
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/newsimage";
                    // Get the orginal filname or create the filename of your choice
                    $filename1 = $file1->getClientOriginalName();
                    $filename1=$rand.$filename1;
                    $upload=$file1->move($destinationPath, $filename1);
                }
                else
                {
                    if(count($request->groupid)==1)
                    {
                        $first_group=$request->groupid[0];
                        $imagePath = public_path()."/mechanicalinsider/newsimage/".$first_group.".png";
                        if(file_exists($imagePath))
                        {
                            $filename1=$first_group.".png";
                        }
                        else
                        {
                            $group_image=DB::table('discussions')->where('id',$first_group)->select('image')->first();
                            $group_image=$group_image->image;
                            $group_image_path= url('/')."/mechanicalinsider/groupimage/".$group_image;
                            file_put_contents($imagePath, file_get_contents($group_image_path));
                            $filename1=$first_group."png";
                        }
                    }
                    else
                    {
                        $filename1="default.png";
                    }
                    
                }

                $payment=$request->payment;
                if($payment=='paid')
                {
                    $payment=$request->payment1;
                }
                if($request->posttype=='create_topic')
                {
                    $posttype=$request->new_topic;
                }
                else
                {
                    $posttype=$request->posttype;
                }
        $jsondata=[];
        $jsondata['writer']=$request->writer;
        $jsondata['title']=$request->title;
        $jsondata['pic']=$filename;
        $jsondata['thumbnail']=$filename1;
        $jsondata['payment']=$payment;
        $jsondata['add_like']=$add_like;
        $jsondata['add_comment']=$add_comment;
        $jsondata['post_type']=$request->posttype;
        $jsondata=json_encode($jsondata);
        $uid=Auth::user()->email;
        if($request->posturl=="")
        {
            $posturl="news";
        }
        else
        {
            $posturl=$request->posturl;
        }
        $data=DB::table('common_table')->orderBy('id','desc')->first();
        $l_id=$data->id;
        $last_id=$data->id+1;
        if($request->type_institute=="both")
        {
           
           
            $insert=DB::table('common_table')->insertGetId(
                ['id'=>$last_id,'field_type' => 1, 'groupid' => 'all','post_type'=>2,'posttype'=>$posttype,'uid'=>$uid,'posturl'=>$posturl,'post_description'=>$request->post_description,'jsondata'=>$jsondata]
            );

            $insert=DB::table('college_data')->insert(
                ['field_type' =>1, 'postid' => $insert,'courseid'=>0,'college_id'=>0,'stream'=>0,'year'=>0,'status'=>'published']);
            
            return redirect('view_posts')->with('success','Post Added Successfully.');
        }
         else{


        $insert=DB::table('common_table')->insertGetId(
            ['id'=>$last_id,'field_type'=>1, 'groupid' => $groupid,'posttype'=>$posttype,'uid'=>$uid,'posturl'=>$posturl,'post_description'=>$request->post_description,'jsondata'=>$jsondata]
        );
 
        if($insert)
        {
            if($request->type_institute=="college")
            {
                $insert=DB::table('college_data')->insert(
                    ['field_type'=>1, 'postid' => $insert,'courseid'=>$course,'college_id'=>$college,'stream'=>$stream,'year'=>$year,'status'=>'published']
                );
            }
            if($request->posttype=='create_topic')
                {
                    $find=DB::table('topic_search')->where('topic_name',$request->new_topic)->first();
                    if($find)
                    {
                        $new_postcount=$find->postcount + 1;
                        DB::table('topic_search')->where('topic_name',$request->new_topic)->update(['postcount'=>$new_postcount]);
                    }
                    else
                    {
                       DB::table('topic_search')->insert(['topic_name'=>$request->new_topic,'testcount'=>'0','postcount'=>'1']); 
                    }
                }
                else
                {
                    $find=DB::table('topic_search')->where('topic_name',$request->posttype)->first();
                    if($find)
                    {
                        $new_postcount=$find->postcount + 1;
                        DB::table('topic_search')->where('topic_name',$request->posttype)->update(['postcount'=>$new_postcount]);
                    }
                }
            return redirect('view_posts')->with('success','Post Added Successfully.');
        }
    
        else
        {
            return redirect('view_posts')->with('error','Something went wrong. Please try again.');

        }
    }
    }


    public function edit_posts(Request $request)
    {
        $id=$request->id;
        $query=DB::table('topic_search')->get();
        if(Auth::user()->groups=="all")
        {
          $groups1=DB::table('discussions')->orderBy('id','desc')->get();
          foreach ($groups1 as $group){
            $groups[]=$group->id;
          }
        }
        else
        {
          $user_groups=Auth::user()->groups;
          if(strrchr($user_groups,","))
          {
            $groups=explode(",",$user_groups);
          }
          else
          {
            $groups[]=$user_groups;
          }
        }
        foreach($groups as $group)
        {
        $data[]=DB::table('discussions')->where('id',$group)->first();
        }
        $all=DB::table('common_table')->where('id',$id)->first();
        
        $course = DB::table('course')->get();
        $college= DB::table('college')->get();
        $depart= DB::table('department')->get();
         
        if($all->post_type==1)
        {
            
        $collegedata = DB::table('college_data')->where('postid',$id)->first();
        if($collegedata!='null' && !empty($collgedata))
        {
        $college_slected = DB::table('college')->where('course_id',$collegedata->courseid)->first();
        $stream_selected = DB::table('college')->where('id',$collegedata->college_id)->first();
        if($stream_selected!='null' && !empty($stream_selected))
        {
        $stream = json_decode($stream_selected->stream);
        $year = $stream_selected->year;
        }    
    }
    }
        return view::make('edit_posts',compact('query','depart','college','data','all','course','college_slected','stream','year','collegedata'));
    }

    public function delete_post(Request $request)
    {
        $id=$request->del_id;
        $post=DB::table('common_table')->where('id',$id)->first();
        $posttype=$post->posttype;
        $delete=DB::table('common_table')->where('id',$id)->delete();
        $delete=DB::table('college_data')->where('postid',$id)->delete();
        if($delete)
        {
            $old=DB::table('topic_search')->where('topic_name',$posttype)->first();
            $old_postcount=$old->postcount -1;
            DB::table('topic_search')->where('topic_name',$posttype)->update(['postcount'=>$old_postcount]);
            return redirect('view_posts')->with('success','Post Deleted Successfully');
        }
    }

    public function update_post(Request $request)
    {
        
        $all=$request->all();
        if($request->add_comment)
        {
            $add_comment=$request->add_comment;
        }
        else
        {
            $add_comment="on";
        }
        if($request->add_like)
        {
            $add_like=$request->add_like;
        }
        else
        {
            $add_like="on";
        }
        if($request->type_institute=="group")
        {
            $groupid=implode(",", $request->groupid);
        }
        else
        {
            $groupid = 0;
            $course = $request->course;
            $college = $request->college;
            $stream = $request->stream;
            $year = $request->year;
        }
        
        $file = $request->file('pic');
                if($file) 
                {
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/newsimage";
                    // Get the orginal filname or create the filename of your choice
                    $filename = $file->getClientOriginalName();
                    $filename=$rand.$filename;
                    $upload=$file->move($destinationPath, $filename);
                }
                else
                {
                    $filename=$request->old_pic;
                }
        $file1 = $request->file('thumbnail');
                if($file1) 
                {
                    $rand=rand(1,1000000);
                    $destinationPath = public_path()."/mechanicalinsider/newsimage";
                    // Get the orginal filname or create the filename of your choice
                    $filename1 = $file1->getClientOriginalName();
                    $filename1=$rand.$filename1;
                    $upload=$file1->move($destinationPath, $filename1);
                }
                else
                {
                    $filename1=$request->old_thumbnail;
                }
                $payment=$request->payment;
                if($payment=='paid')
                {
                    $payment=$request->payment1;
                }
                if($request->posttype=='create_topic')
                {
                    $posttype=$request->new_topic;
                }
                else
                {
                    $posttype=$request->posttype;
                }
        $jsondata=[];
        $jsondata['writer']=$request->writer;
        $jsondata['title']=$request->title;
        $jsondata['pic']=$filename;
        $jsondata['thumbnail']=$filename1;
        $jsondata['payment']=$payment;
        $jsondata['add_like']=$add_like;
        $jsondata['add_comment']=$add_comment;
        $jsondata['post_type']=$request->post_type;
        $jsondata=json_encode($jsondata);
        $uid=Auth::user()->email;
        if($request->posturl=="")
        {
            $posturl="news";
        }
        else
        {
            $posturl=$request->posturl;
        }
      

        if($request->type_institute=="both")
        {
           $update=DB::table('common_table')->where('id',$request->id)->update(
                [ 'groupid' => 'all','post_type'=>2,'posttype'=>$posttype,'uid'=>$uid,'posturl'=>$posturl,'post_description'=>$request->post_description,'jsondata'=>$jsondata]
            );
            $data=DB::table('college_data')->where('postid',$request->id)->first();
            if($data!='null' && !empty($data))
            {
            $update_college=DB::table('college_data')->where('postid',$request->id)->update(
                ['courseid'=>0,'college_id' => 0,'stream'=>0,'year'=>0]);
            }
            else
            {
              $insert_college=DB::table('college_data')->insert(
                ['postid'=>$request->id,'courseid'=>0,'college_id' => 0,'stream'=>0,'year'=>0,'status'=>'published']);  
            }
            return redirect('view_posts')->with('success','Post Edit Successfully.');
        }

        if($request->type_institute == "college")
        {
          
            $update=DB::table('common_table')->where('id',$request->id)->update(
                [ 'groupid' => 0,'post_type'=>1,'posttype'=>$posttype,'uid'=>$uid,'posturl'=>$posturl,'post_description'=>$request->post_description,'jsondata'=>$jsondata]
            );    
         
         $data = DB::table('college_data')->where('postid',$request->id)->first();
         
         if($data!='null' && $data!='')
         {
         $update_college=DB::table('college_data')->where('postid',$request->id)->update(
            ['courseid'=>$request->course,'college_id' =>$request->college,'stream'=>$request->stream,'year'=>$request->year]);
         }   
         else
         {
          $insert_college=DB::table('college_data')->insert(
            ['postid'=>$request->id,'field_type'=>1,'courseid'=>$request->course,'college_id' =>$request->college,'stream'=>$request->stream,'year'=>$request->year,'status'=>'published']); 
         }
        }

        if($request->type_institute=="group")
        {
        
        $update=DB::table('common_table')->where('id',$request->id)->update(
                [ 'groupid' =>$groupid,'post_type'=>0,'posttype'=>$posttype,'uid'=>$uid,'posturl'=>$posturl,'post_description'=>$request->post_description,'jsondata'=>$jsondata]
            );  
        $data=DB::table('college_data')->where('postid',$request->id)->first();
        if($data!='null' && $data!='')
        {
        $delete=DB::table('college_data')->where('postid',$request->id)->delete();
        }  
        
    }
        
         
            
    //    $update=DB::table('college_data')->where('postid',$request->id)->update(
    //         [ 'courseid'=>$course,'college_id'=>$college,'stream'=>$stream,'year'=>$year]
    //     );
        
        if($update)
        {
            if($posttype!=$request->old_topic)
            {
               $find=DB::table('topic_search')->where('topic_name',$posttype)->first();
                    if($find)
                    {
                        $new_postcount=$find->postcount + 1;
                        DB::table('topic_search')->where('topic_name',$posttype)->update(['postcount'=>$new_postcount]);
                        $old=DB::table('topic_search')->where('topic_name',$request->old_topic)->first();
                        $old_postcount=$old->postcount -1;
                        DB::table('topic_search')->where('topic_name',$request->old_topic)->update(['postcount'=>$old_postcount]);
                    }
                    else
                    {
                       DB::table('topic_search')->insert(['topic_name'=>$posttype,'testcount'=>'0','postcount'=>'1']); 
                       $old=DB::table('topic_search')->where('topic_name',$request->old_topic)->first();
                        $old_postcount=$old->postcount -1;
                        DB::table('topic_search')->where('topic_name',$request->old_topic)->update(['postcount'=>$old_postcount]);
                    } 
            }
            return redirect('view_posts')->with('success','Post Updated Successfully.');
        }
    
        else
        {
            return redirect('view_posts')->with('error','Something went wrong. Please try again.');

        }
    

    }

    public function view_posts_detail($id)
    {
        $query=DB::table('common_table')->where('id',$id)->first();
        $comment=DB::table('comment_on_post')->where('postid',$id)->get();
        return view::make('view_post_details',compact('query','comment'));
    }

    // public function ajax(Request $request)
    // {
    //     $postid=$request->postid;
    //     $field_type=1;
    //     $new_limit=$request->limit;
    //     $com=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$postid)->get();
    
    //       return view::make('ajax',compact('com','new_limit','postid'));
    // }

    public function delete_post_comment(Request $request)
    {
        $id=$request->comment_id;

        $detail=DB::table('common_table')->where('id',$request->postid)->first();
        $total_comment=$detail->comment;
        $data=DB::table('comment_on_post')->where('id',$request->comment_id)->first();
        $total_reply=$data->comment;
        $total_comment=$total_comment-$total_reply;

        $comment=$total_comment-1;

        $update=DB::table('common_table')->where('id',$request->postid)->update(['comment'=>$comment]);
        $delete_reply=DB::table('comment_replies')->where('commentid',$request->comment_id)->delete();


        $delete=DB::table('comment_on_post')->where('id',$id)->delete();
        if($delete)
        {
            return redirect()->back()->with('success','comment deleted Successfully');
        }
    }

    // public function view_employees(Request $request)
    // {
    //     $institute_id=$request->id;
    //     $subadmins=InstituteSubadmin::where('institute_id',$institute_id)->get();
    //     return view('view_institute_subadmin',compact('subadmins'));
    // }

    public function get_course(Request $req)
    {
        $college = DB::table('college')->where('course_id',$req->course)->get();
      
        return response()->json(['data'=>$college,'status'=>400]);
    }

    public function get_stream(Request $req)
    {
        $stream = DB::table('college')->where('id',$req->college)->get();
        $stream_college = array();
        foreach($stream as $key)
        {
            $stream_college = json_decode($key->stream);
        }
        
        return response($stream);
    }


    public function add_test()
    { 

        if(Auth::user()->role == 'school')
        {
          return back()->with('error','Sorry,You do not access to access this page.');
        }

       
        if(Auth::user()->role == 'admin')
        {
          $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
        }
        else
        {
          $new_Courses=DB::table('new_courses')->where('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
        }
        
        return view::make('add_test',compact('new_Courses'));
    }

      public function add_demo_test()
    {
        $query=DB::table('topic_search')->where('topic_type',0) ->get();
        $all_topic=DB::table('topic_search')->get();
        $college_topic=DB::table('topic_search')->where('topic_type',1) ->get();
        if(Auth::user()->groups=="all")
        {
          $groups1=DB::table('discussions')->orderBy('id','desc')->get();
          foreach ($groups1 as $group){
            $groups[]=$group->id;
          }
        }
        else
        {
          $user_groups=Auth::user()->groups;
          if(strrchr($user_groups,","))
          {
            $groups=explode(",",$user_groups);
          }
          else
          {
            $groups[]=$user_groups;
          }
        }
        foreach($groups as $group)
        {
        $data[]=DB::table('discussions')->where('id',$group)->first();
        }
        $subject_question=DB::table('subject_question')->get();
        $course = DB::table('course')->get();
        return view::make('demo_add_test',compact('data','query','course','college_topic','all_topic','subject_question'));
    }

    public function edit_test($id)
    {
        $field_type=2;
        $record=DB::table('common_table')->where('field_type',$field_type)->where('id',$id)->first();

        if($record)
        {
            if($record->groupid != 'all'){
              $record->course_id=explode(",",$record->groupid); 
            }else {
              $record->course_id=[];
            }

           
            if(Auth::user()->role == 'admin')
            {
              $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
            }
            else
            {
              $new_Courses=DB::table('new_courses')->where('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
            }

            $subject=DB::table('subject')->where('school_id',Auth::user()->id)->get();
    
            return view::make('edit_test',compact('new_Courses','record','subject'));
        
         
        }
        else 
        {
          return redirect('view_test')->with('error','Something went wrong. Please try again.');
        } 
      
    }



    // public function view_tests()
    // {
    //     $field_type=2;
    //     $query=DB::table('common_table')->where('field_type',$field_type)->orderBy('id','desc')->get();
    //     foreach ($query as $q) {
    //         if($q->groupid=="all")
    //         {
    //             $q->groups="All";
    //         }
    //         else
    //         {
    //             $group_id=explode(",",$q->groupid);
    //             $data=DB::table('discussions')->select('topic')->whereIn('id',$group_id)->get();
    //             foreach ($data as $dd) {
    //                 $data1[]=$dd->topic;
    //             }
    //             $data2=implode(", ",$data1);
    //             $q->groups=$data2;
    //             }
    //         }
    //     return view::make('view_tests',compact('query'));
    // }

    public function view_tests()
    {
        $field_type=2;
        $uid=Auth::User()->email;
        $record=DB::table('common_table')->where('uid',$uid)->where('field_type',$field_type)->where('premium',0)->where('status','published')->orderBy('id','desc')->paginate(10);
        
        $next_page_url=$record->nextPageUrl();
        if($record->currentPage()>1)
        {
            $json='true';
        }
        else
        {
            $json='false';
        }
        $new_record=[];
    foreach ($record as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec->groupid;
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
    	$new_record=$record;
    }
    if(!empty($new_record))
    {
      $new_record=$this->filter_test($new_record);
    } 
    $record=$new_record;
    session(['record_count'=>count($record)]);

    if($json=='true')
    {
        return view::make('view_tests_new_page',compact('record','next_page_url'));
    }
    
        return view::make('view_tests_new',compact('record','next_page_url'));
    
    }
//------------------Unpublish Test--------------------//
    public function view_unpublish_test()
    {
        $field_type=2;
        $record=DB::table('common_table')->where('field_type',$field_type)->where('premium',0)->where('status','notpublished')->orderBy('id','desc')->paginate(10);
        
        $next_page_url=$record->nextPageUrl();
        if($record->currentPage()>1)
        {
            $json='true';
        }
        else
        {
            $json='false';
        }
        $new_record=[];
    foreach ($record as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec->groupid;
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
    	$new_record=$record;
    }
    if(!empty($new_record))
    {
      $new_record=$this->filter_test($new_record);
    } 
    $record=$new_record;
    session(['record_count'=>count($record)]);

    if($json=='true')
    {
        return view::make('view_tests_new_page',compact('record','next_page_url'));
    }
    
        return view::make('view_tests_new',compact('record','next_page_url'));
    
    }

   public function view_college_tests($id)
    {
        $college_id=$id;
         $record = DB::table('college_data')
            ->join('common_table',  'college_data.postid', '=', 'common_table.id')
            ->select('common_table.*')
            ->where('college_data.college_id',$college_id)
            ->where('common_table.field_type',2)
            ->where('common_table.premium',0) 
            ->where('common_table.post_type',1)
            ->paginate(10);           


        // $field_type=2;
        // $record=DB::table('common_table')->where('field_type',$field_type)->where('premium',0)->where('post_type',1)->orderBy('id','desc')->paginate(10);
        
        $next_page_url=$record->nextPageUrl();
        if($record->currentPage()>1)
        {
            $json='true';
        }
        else
        {
            $json='false';
        }
        $new_record=[];
    foreach ($record as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec->groupid;
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
        $new_record=$record;
    }
    if(!empty($new_record))
    {
      $new_record=$this->filter_test($new_record);
    } 
    $record=$new_record;
    session(['record_count'=>count($record)]);

    if($json=='true')
    {
        return view::make('view_tests_new_page',compact('record','next_page_url'));
    }
    
        return view::make('view_tests_new',compact('record','next_page_url'));
    
    }


    public function submit_test(Request $r)
    {
        $premium=0;

        if ($r->payment=="paid") 
        {
          if ($r->amount=="")
          {
              $amount='10';
          }
          else
          {
              $amount=$r->amount;
          }
        }
        else
        {
            $amount="free";
        }

        $total_questions=0;
        for ($i=0; $i <count($r->cat) ; $i++) 
        {
            if($r->cat[$i]!="")
            {
              $sub_test_data[]=array('test'=>$r->cat[$i],'question_no'=>$r->questions[$i]);
              $total_questions=$total_questions+$r->questions[$i];
            } 
        }


        //subject name in test
        $subject_detail=json_encode($r->subject);


        $negative_mark='';
        if($r->negitive_number != '')
        {
          $negative_mark=$r->negitive_number;
        }
        

        $post_description=json_encode($sub_test_data);

        $data=[
          'writer'=>$r->postby,
          'testname'=>$r->test_name,
          'description'=>$r->description,
          'time'=>$r->time,
          'question'=>$total_questions,
          'payment'=>$amount,
          'image'=>'',
          'add_comment'=>'',
          'add_like'=>'',
          'ans_status'=>'',
          'ans_status_new'=>'',
          'offline_status'=>'',
          'negative_marks'=>$r->negative,
          'negative_mark'=>$negative_mark,
          'start_date'=>'',
          'end_date'=>'',
          'first_prize'=>'',
          'second_prize'=>'',
          'third_prize'=>'',
          'result_declare'=>"no"
        ];

        $jsondata=json_encode($data);
        $course_id=implode(",",$r->groupid);

        $time=date('Y-m-d H:i:s',strtotime($r->rank_time));

        $insert=DB::table('common_table')->insertGetId(['view'=>'0','school'=>json_encode($r->subject_id),'field_type'=>2,'post_type'=>2,
        'groupid'=>$course_id,'post_description'=>$post_description,'subject_details'=>$subject_detail,'jsondata'=>$jsondata,'status'=>'published',
        'premium'=>$premium,'uid'=>Auth::user()->email,'rank_time'=>$time ]);

        if($insert)
        {
          $content='Test-'.$r->test_name;
          $data=array("type"=>2,"name"=>'test',"id"=>$insert);
          // $push= $this->push_notification($content,$data);

          return redirect('view_test')->with('success','Test Added Successfully.');
        }
        else
        {
          return redirect('view_test')->with('error','Something went wrong. Please try again.');
        }

    }

    public function update_test(Request $r)
    {
        // $update=$this->update_test_common($request);

        $premium=0;

        if ($r->payment=="paid") 
        {
          if ($r->amount=="")
          {
              $amount='10';
          }
          else
          {
              $amount=$r->amount;
          }
        }
        else
        {
            $amount="free";
        }

        $total_questions=0;
        for ($i=0; $i <count($r->cat) ; $i++) 
        {
            if($r->cat[$i]!="")
            {
              $sub_test_data[]=array('test'=>$r->cat[$i],'question_no'=>$r->questions[$i]);
              $total_questions=$total_questions+$r->questions[$i];
            } 
        }

        $post_description=json_encode($sub_test_data);

        //subject name in test
        $subject_detail=json_encode($r->subject);

        $data=[
          'writer'=>$r->postby,
          'testname'=>$r->test_name,
          'description'=>$r->description,
          'time'=>$r->time,
          'question'=>$total_questions,
          'payment'=>$amount,
          'image'=>'',
          'add_comment'=>'',
          'add_like'=>'',
          'ans_status'=>'',
          'ans_status_new'=>'',
          'offline_status'=>'',
          'negative_marks'=>$r->negative,
          'negative_mark'=>$r->negitive_number,
          'start_date'=>'',
          'end_date'=>'',
          'first_prize'=>'',
          'second_prize'=>'',
          'third_prize'=>'',
          'result_declare'=>"no"
        ];

        $jsondata=json_encode($data);
        $course_id=implode(",",$r->groupid);
        $time=date('Y-m-d H:i:s',strtotime($r->rank_time));

        $update=DB::table('common_table')->where('id',$r->id)->update(['school'=>json_encode($r->subject_id),'groupid'=>$course_id,
        'post_description'=>$post_description,'subject_details'=>$subject_detail,'jsondata'=>$jsondata,'status'=>'published',
        'premium'=>$premium,'uid'=>Auth::user()->email,'rank_time'=>$time]);

        if($update==0||$update==1)
        {
          return redirect('view_test')->with('success','Test Updated Successfully.');
        }
        else
        {
          return redirect('view_test')->with('error','Something went wrong. Please try again.');
        }
        
    }

    public function publish_test($id)
    {
          
          $data=DB::table('common_table')->orderBy('id','desc')->first();
        
          $post_type=$data->post_type;
          $l_id=$data->id;
          
          $last_id=$l_id+1;
       
        
          //check test is premium or not
          $test=DB::table('common_table')->where('id',$id)->first();
		      $date = date('Y-m-d H:i:s');
          DB::table('common_table')->where('id',$id)->update(['id'=>$last_id,'status'=>'published','timestamp'=>$date]);
          DB::table('result_record')->where('testid',$id)->update(['testid'=>$last_id]);
          
          if($post_type==1)
          {
            DB::table('college_data')->where('postid',$id)->update(['postid'=>$last_id,'status'=>'published']);
          }
          elseif($post_type==2)
          {
            DB::table('college_data')->where('postid',$id)->update(['postid'=>$last_id,'status'=>'published']);    
          }
          rename(public_path()."/mechanicalinsider/admin/tests_images/".$id,public_path()."/mechanicalinsider/admin/tests_images/".$last_id);
          DB::table('test_question')->where('testid',$id)->update(['testid'=>$last_id]);
          $zip=$this->make_test_zip($last_id);
          if($test->premium==1)
          {
                $json_data=json_decode($test->jsondata,true);
                $now=date('Y-m-d H:i');
                $mili=30*60+300*60;
                $to_time = strtotime($now)+$mili;
                $from_time = strtotime($json_data['end_date']);
                $minutes=round(abs($to_time - $from_time) / 60,2);

                $job=(new PremiumTestResult($last_id))->delay(Carbon::now()->addMinutes($minutes));
                $jobid=app(\Illuminate\Contracts\Bus\Dispatcher::class)->dispatch($job);
                // $jobid=PremiumTestResult::dispatch($test)->delay(Carbon::now()->addMinutes($minutes));
                $json_data['job_id']=$jobid;
                $jsondata=json_encode($json_data);
                $update=DB::table('common_table')->where('id',$last_id)->update(['jsondata'=>$jsondata]);
                
              return redirect('premium_test')->with('success','Test published Successfully.'); 
          }
          else
          {
              return redirect('test')->with('success','Test published Successfully.'); 
          }
    }

    public function unpublish_test($id)
    {
       
        DB::table('common_table')->where('id',$id)->update(['status'=>'notpublished']);
        DB::table('college_data')->where('postid',$id)->update(['status'=>'notpublished']);
        $test=DB::table('common_table')->where('id',$id)->first();
      
        if($test->premium==1)
          {
              $json_data=json_decode($test->jsondata,true);
              $job_id=$json_data['job_id'];
              // $delete=DB::table('jobs')->where('id',$job_id)->delete();

              return redirect('premium_test')->with('success','Test Unpublished Successfully.'); 
          }
          else
          {
               return redirect('test')->with('success','Test Unpublished Successfully.'); 
          }
           
        
    }

    public function make_test_zip($id)
    {
        
        $dir_name=$id;
         $dir=public_path()."/mechanicalinsider/admin/tests_images/".$dir_name."/";
         // $path=public_path()."/mechanicalinsider/admin/tests_images/$dir_name.zip";
         $des=public_path()."/mechanicalinsider/admin/tests_images/$dir_name.zip";

        if(file_exists(public_path()."/mechanicalinsider/admin/tests_images/$dir_name".'.zip'))
        {
           unlink(public_path()."/mechanicalinsider/admin/tests_images/$dir_name".'.zip');
        }
        
         // $zip= $this->Zip( $dir,$path);

        // $dir="./TestingTest";

$files = array(); 

   $cdir = scandir($dir); 
   foreach ($cdir as $key => $value) 
   { 
      if (!in_array($value,array(".",".."))) 
      { 
      
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
         { 
            $files[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
         } 
         else 
         { 
            $files[] = $dir.$value; 
         } 
      } 
   } 

   print_r($files);

$result = $this->create_zip($files,$des,false,$dir_name);




        return redirect('test');

    }

    

 public function create_zip($files,$destination,$overwrite,$dir_name) {

//if the zip file already exists and overwrite is false, return false
    if(file_exists($destination) && !$overwrite) { return false; }
    //vars
    $valid_files = array();
    //if files were passed in...
    if(is_array($files)) {
        //cycle through each file
        foreach($files as $file) {
            //make sure the file exists
            if(file_exists($file)) {
                $valid_files[] = $file;
            }
        }
    }
    //if we have good files...
    if(count($valid_files)) {
        //create the archive
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }
        //add the files
        foreach($valid_files as $file) {
            $new_filename = $dir_name."/".substr($file,strrpos($file,'/') + 1);
            $zip->addFile($file,$new_filename);

        }
        //debug
        //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
        
        //close the zip -- done!
        $zip->close();
        
        //check to make sure the file exists
        return file_exists($destination);
    }
    else
    {
        return false;
    } 
}

    // public function Zip($source, $destination)
    //     {
    //         if (!extension_loaded('zip') || !file_exists($source)) {
    //             return false;
    //         }

    //         $zip = new ZipArchive();
    //         if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
    //             return false;
    //         }

    //         $source = str_replace('\\', '/', realpath($source));

    //         if (is_dir($source) === true)
    //         {
    //             $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

    //             foreach ($files as $file)
    //             {
    //                 $file = str_replace('\\', '/', $file);

    //                 // Ignore "." and ".." folders
    //                 if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
    //                     continue;

    //                 $file = realpath($file);

    //                 if (is_dir($file) === true)
    //                 {
    //                     $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
    //                 }
    //                 else if (is_file($file) === true)
    //                 {
    //                     $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
    //                 }
    //             }
    //         }
    //         else if (is_file($source) === true)
    //         {
    //             $zip->addFromString(basename($source), file_get_contents($source));
    //         }

    //         return $zip->close();

    //     }


    public function delete_test(Request $request)
    {
       $id=$request->del_id;
       
       $test=DB::table('common_table')->where('id',$id)->first();
       $posttype=$test->posttype;
       $dir_name=$id;
       if(file_exists(public_path()."/mechanicalinsider/admin/tests_images/$dir_name".'.zip'))
        {
           unlink(public_path()."/mechanicalinsider/admin/tests_images/$dir_name".'.zip');
        }

        $directory=public_path()."/mechanicalinsider/admin/tests_images/$dir_name";
            foreach(glob("{$directory}/*") as $file)
            {
                if(is_dir($file)) { 
                    recursiveRemoveDirectory($file);
                } else {
                    unlink($file);
                }
            }
           if( is_dir($directory))
           {rmdir($directory);}

        // rmdir();
        $delete=DB::table('common_table')->where('id',$id)->get();
        
        $arr=json_decode($delete,true);
        $post_type=$arr[0]['post_type'];
        
        
        if($post_type==1)
        {
         
        $d=DB::table('college_data')->where('postid',$id)->delete();
        }
        $delete=DB::table('common_table')->where('id',$id)->delete();

        $del=DB::table('test_question')->where('testid',$id)->delete();
        if($del||$delete)
        {
            $old=DB::table('topic_search')->where('topic_name',$posttype)->first();  
            if($old)   
            {
              $old_testcount=$old->testcount-1;
              DB::table('topic_search')->where('topic_name',$posttype)->update(['testcount'=>$old_testcount]);
            }       
           
            
            
            if($test->premium==0)
            {
                return redirect('view_test')->with('success','Test Deleted Successfully');
            }
            else
            {
                return redirect('premium_test')->with('success','Test Deleted Successfully');
            }

        }
        else
        {
            return redirect()->back()->with('error','Something went wrong. Please try again.');
        }
    }


    public function view_questions($id)
    {
        $query=DB::table('test_question')->where('testid',$id)->orderBy('id','asc')->get();
        $common=DB::table('common_table')->where('id',$id)->first();

        $total_questions='';
        $test_name='';
        if($common)
        {
          $json=json_decode($common->jsondata,true);
          $test_name=$json['testname'];
          $total_questions=$json['question'];
        }
        

        $added_questions=DB::table('test_question')->where('testid',$id)->count();
        if($total_questions>$added_questions)
        {
            $add_question='true';
        }
        else
        {
            $add_question='false';
        }
        return view::make('view_questions',compact('query','test_name','add_question'));
        // dd($query);
    }

    public function poll_questions($id)
    {
       
        $record=DB::table('common_table')->where('id',$id)->first();
        
        $answars=DB::table('postquestion_answer')
        ->join('users', 'postquestion_answer.uid', '=', 'users.email')
        ->select('users.name','users.mobile','users.email','postquestion_answer.*')
        ->where('postquestion_answer.postid',$record->id)->get();

        $jsondata = json_decode($record->jsondata,true);
        foreach($answars as $a)
        {
            $a->answar = $jsondata[$a->answer];
            
        }
        return view::make('view_poll_questions',compact('record','answars','jsondata'));
         
    }

    public function view_full_question($question_id)
    {
        $data=DB::table('test_question')->where('id',$question_id)->first();
        // dd($data);
        return view::make('question_details',compact('data'));
    }


    public function add_questions($id)
    {
        $query=DB::table('common_table')->where('id',$id)->first();
		
        $test_id=$query->id;
        $json=json_decode($query->jsondata,true);
		    $tests_name=$json['testname'];
        $sub_test=json_decode($query->post_description,true);

        for($i=0;$i<count($sub_test);$i++){
          $test_name[] = $sub_test[$i]['test'];
        }

        foreach ($sub_test as $tests) {
            $test=$tests['test'];
            $question=$tests['question_no'];
            $tests_ques[]=array($test=>$question);
        }
        
        for($i=0;$i<count($test_name);$i++){
          $question_entered[$test_name[$i]]=DB::table('test_question')->select('sub_test_name')->where('testid',$test_id)->where('sub_test_name',trim($test_name[$i]))->count();
        }

        $subjectQuestion=[];
        if($query->subject_details != ''){
          $subjectQuestion=json_decode($query->subject_details,true);
        }

        return view::make('add_question',compact('question_entered','tests_ques','test_name','test_id','tests_name','subjectQuestion'));

    }

    public function submit_questions(Request $request)
    {
        $all=$request->all();
        if($request->question=="")
        {
            $question="NULL";
        }
        else
        {
            $question=$request->question;
        }
        
        if($request->option_a=="")
        {
            $option_a="NULL";
        }
        else
        {
            $option_a=$request->option_a;
        }
        
        if($request->option_b=="")
        {
            $option_b="NULL";
        }
        else
        {
            $option_b=$request->option_b;
        }
        
        if($request->option_c=="")
        {
            $option_c="NULL";
        }
        else
        {
            $option_c=$request->option_c;
        }
        
        if($request->option_d=="")
        {
            $option_d="NULL";
        }
        else
        {
            $option_d=$request->option_d;
        }
        
        if($request->option_e=="")
        {
            $option_e="NULL";
        }
        else
        {
            $option_e=$request->option_e;
        }
        
        if($request->answer=="")
        {
            $answer="NULL";
        }
        else
        {
            $answer=$request->answer;
        }
        
        if($request->hint=="")
        {
            $hint="NULL";
        }
        else
        {
            $hint=$request->hint;
        }
        
        if($request->solution=="")
        {
            $solution="NULL";
        }
        else
        {
            $solution=$request->solution;
        }

        $question_time=$request->question_time;

          $user_name=Auth::user()->name;
        $user_email=Auth::user()->email;

        // if($request->ans_status)
        // {
        //     $ans_status=$request->ans_status;
        // }
        // else
        // {
        //     $ans_status="no";
        // }

        $dir_name=$request->test_id;
        $path=public_path()."/mechanicalinsider/admin/tests_images/".$dir_name."/";

                if($request->file('question_image')) 
                {
                    $rand=rand(1,1000000);
                    $question_image_name = $request->file('question_image')->getClientOriginalName();
                    $question_image_name="tests_images/".$dir_name."/".$rand.$question_image_name;
                    $request->file('question_image')->move($path, $question_image_name);
                }
                else
                {
                    $question_image_name="NULL";
                }

                if($request->file('option_a_image')) 
                {
                    $rand=rand(1,1000000);
                    $option_a_image_name = $request->file('option_a_image')->getClientOriginalName();
                    $option_a_image_name="tests_images/".$dir_name."/".$rand.$option_a_image_name;
                    $request->file('option_a_image')->move($path, $option_a_image_name);
                }
                else
                {
                    $option_a_image_name="NULL";
                }

                if($request->file('option_b_image')) 
                {
                    $rand=rand(1,1000000);
                    $option_b_image_name = $request->file('option_b_image')->getClientOriginalName();
                    $option_b_image_name="tests_images/".$dir_name."/".$rand.$option_b_image_name;
                    $request->file('option_b_image')->move($path, $option_b_image_name);
                }
                else
                {
                    $option_b_image_name="NULL";
                }

                if($request->file('option_c_image')) 
                {
                    $rand=rand(1,1000000);
                    $option_c_image_name = $request->file('option_c_image')->getClientOriginalName();
                    $option_c_image_name="tests_images/".$dir_name."/".$rand.$option_c_image_name;
                    $request->file('option_c_image')->move($path, $option_c_image_name);
                }
                else
                {
                    $option_c_image_name="NULL";
                }

                if($request->file('option_d_image')) 
                {
                    $rand=rand(1,1000000);
                    $option_d_image_name = $request->file('option_d_image')->getClientOriginalName();
                    $option_d_image_name="tests_images/".$dir_name."/".$rand.$option_d_image_name;
                    $request->file('option_d_image')->move($path, $option_d_image_name);
                }
                else
                {
                    $option_d_image_name="NULL";
                }

                if($request->file('option_e_image')) 
                {
                    $rand=rand(1,1000000);
                    $option_e_image_name = $request->file('option_e_image')->getClientOriginalName();
                    $option_e_image_name="tests_images/".$dir_name."/".$rand.$option_e_image_name;
                    $request->file('option_e_image')->move($path, $option_e_image_name);
                }
                else
                {
                    $option_e_image_name="NULL";
                }

                if($request->file('answer_image')) 
                {
                    $rand=rand(1,1000000);
                    $answer_image_name = $request->file('answer_image')->getClientOriginalName();
                    $answer_image_name="tests_images/".$dir_name."/".$rand.$answer_image_name;
                    $request->file('answer_image')->move($path, $answer_image_name);
                }
                else
                {
                    $answer_image_name="NULL";
                }

                if($request->file('hint_image')) 
                {
                    $rand=rand(1,1000000);
                    $hint_image_name = $request->file('hint_image')->getClientOriginalName();
                    $hint_image_name="tests_images/".$dir_name."/".$rand.$hint_image_name;
                    $request->file('hint_image')->move($path, $hint_image_name);
                }
                else
                {
                    $hint_image_name="NULL";
                }

                if($request->file('solution_image')) 
                {
                    $rand=rand(1,1000000);
                    $solution_image_name = $request->file('solution_image')->getClientOriginalName();
                    $solution_image_name="tests_images/".$dir_name."/".$rand.$solution_image_name;
                    $request->file('solution_image')->move($path, $solution_image_name);
                }
                else
                {
                    $solution_image_name="NULL";
                }

            $question_data=[];
            $question_data['data']['question'] = array(
            'value' => $question,
            'image' => $question_image_name
            );
            $question_data['data']['option a'] = array(
            'value' => $option_a,
            'image' => $option_a_image_name,
            'orignal' => 'a'
            );
            $question_data['data']['option b'] = array(
            'value' => $option_b,
            'image' => $option_b_image_name,
            'orignal' => 'b'
            );
            $question_data['data']['option c'] = array(
            'value' => $option_c,
            'image' => $option_c_image_name,
            'orignal' => 'c'
            );
            $question_data['data']['option d'] = array(
            'value' => $option_d,
            'image' => $option_d_image_name,
            'orignal' => 'd'
            );
            $question_data['data']['option e'] = array(
            'value' => $option_e,
            'image' => $option_e_image_name,
            'orignal' => 'e'
            );
            $question_data['data']['answer'] = array(
            'value' => $answer,
            'image' => $answer_image_name
            );
            $question_data['data']['hint'] = array(
            'value' => $hint,
            'image' => $hint_image_name
            );
            $question_data['data']['solution'] = array(
            'value' => $solution,
            'image' => $solution_image_name
            );
             $question_data['data']['by'] = array(
            'uid' => $user_email,
            'name' =>  $user_name
            );
            $question_data['data']['time'] = array(
            'question_time' => $question_time,
            );
          
            // $question_data['data']['ans_status'] = $ans_status;

        $json=json_encode($question_data);

        $insert=DB::table('test_question')->insert(
            ['testid' => $request->test_id,'subject_name'=>$request->subjectQuestion, 'sub_test_name'=>$request->sub_test_name,'jsondata'=>$json]
        );
        if($insert)
        {
            return redirect('add_questions/'.$request->test_id)->with('success',"Question added Successfully.");
        }
        else
        {
            return redirect()->back()->with('success',"Question added Successfully.");
        }
    }

    public function edit_question($id)
    {
        $query=DB::table('test_question')->where('id',$id)->first();
       
        $common=DB::table('common_table')->where('id',$query->testid)->first();

        $json=json_decode($common->jsondata,true);
        $test_name=$json['testname'];
        $test_id=$common->id;

        $subjectQuestion=[];
        if($common->subject_details != ''){
          $subjectQuestion=json_decode($common->subject_details,true);
        }

        return view::make('edit_question',compact('query','test_name','test_id','subjectQuestion'));
    }

    public function update_question(Request $request)
    {
        $all=$request->all();
        // dd($all);

        if($request->question=="")
        {
            $question="NULL";
        }
        else
        {
            $question=$request->question;
        }
        
        if($request->option_a=="")
        {
            $option_a="NULL";
        }
        else
        {
            $option_a=$request->option_a;
        }
        
        if($request->option_b=="")
        {
            $option_b="NULL";
        }
        else
        {
            $option_b=$request->option_b;
        }
        
        if($request->option_c=="")
        {
            $option_c="NULL";
        }
        else
        {
            $option_c=$request->option_c;
        }
        
        if($request->option_d=="")
        {
            $option_d="NULL";
        }
        else
        {
            $option_d=$request->option_d;
        }
        
        if($request->option_e=="")
        {
            $option_e="NULL";
        }
        else
        {
            $option_e=$request->option_e;
        }
        
        if($request->answer=="")
        {
            $answer="NULL";
        }
        else
        {
            $answer=$request->answer;
        }
        
        if($request->hint=="")
        {
            $hint="NULL";
        }
        else
        {
            $hint=$request->hint;
        }
        
        if($request->solution=="")
        {
            $solution="NULL";
        }
        else
        {
            $solution=$request->solution;
        }

        

        $dir_name=$request->test_id;
        
        $path=public_path()."/mechanicalinsider/admin/tests_images/".$dir_name."/";

          if($request->file('question_image')) 
          {
              $rand=rand(1,1000000);
              $question_image_name = $request->file('question_image')->getClientOriginalName();
              $question_image_name="tests_images/".$dir_name."/".$rand.$question_image_name;
              $request->file('question_image')->move($path, $question_image_name);
          }
          else
          {
              $question_image_name=$request->old_question_image;
          }

          if($request->file('option_a_image')) 
          {
              $rand=rand(1,1000000);
              $option_a_image_name = $request->file('option_a_image')->getClientOriginalName();
              $option_a_image_name="tests_images/".$dir_name."/".$rand.$option_a_image_name;
              $request->file('option_a_image')->move($path, $option_a_image_name);
          }
          else
          {
              $option_a_image_name=$request->old_option_a_image;
          }

          if($request->file('option_b_image')) 
          {
              $rand=rand(1,1000000);
              $option_b_image_name = $request->file('option_b_image')->getClientOriginalName();
              $option_b_image_name="tests_images/".$dir_name."/".$rand.$option_b_image_name;
              $request->file('option_b_image')->move($path, $option_b_image_name);
          }
          else
          {
              $option_b_image_name=$request->old_option_b_image;
          }

          if($request->file('option_c_image')) 
          {
              $rand=rand(1,1000000);
              $option_c_image_name = $request->file('option_c_image')->getClientOriginalName();
              $option_c_image_name="tests_images/".$dir_name."/".$rand.$option_c_image_name;
              $request->file('option_c_image')->move($path, $option_c_image_name);
          }
          else
          {
              $option_c_image_name=$request->old_option_c_image;
          }

          if($request->file('option_d_image')) 
          {
              $rand=rand(1,1000000);
              $option_d_image_name = $request->file('option_d_image')->getClientOriginalName();
              $option_d_image_name="tests_images/".$dir_name."/".$rand.$option_d_image_name;
              $request->file('option_d_image')->move($path, $option_d_image_name);
          }
          else
          {
              $option_d_image_name=$request->old_option_d_image;
          }

          if($request->file('option_e_image')) 
          {
              $rand=rand(1,1000000);
              $option_e_image_name = $request->file('option_e_image')->getClientOriginalName();
              $option_e_image_name="tests_images/".$dir_name."/".$rand.$option_e_image_name;
              $request->file('option_e_image')->move($path, $option_e_image_name);
          }
          else
          {
              $option_e_image_name=$request->old_option_e_image;
          }

          if($request->file('answer_image')) 
          {
              $rand=rand(1,1000000);
              $answer_image_name = $request->file('answer_image')->getClientOriginalName();
              $answer_image_name="tests_images/".$dir_name."/".$rand.$answer_image_name;
              $request->file('answer_image')->move($path, $answer_image_name);
          }
          else
          {
              $answer_image_name=$request->old_answer_image;
          }

          if($request->file('hint_image')) 
          {
              $rand=rand(1,1000000);
              $hint_image_name = $request->file('hint_image')->getClientOriginalName();
              $hint_image_name="tests_images/".$dir_name."/".$rand.$hint_image_name;
              $request->file('hint_image')->move($path, $hint_image_name);
          }
          else
          {
              $hint_image_name=$request->old_hint_image;
          }

          if($request->file('solution_image')) 
          {
              $rand=rand(1,1000000);
              $solution_image_name = $request->file('solution_image')->getClientOriginalName();
              $solution_image_name="tests_images/".$dir_name."/".$rand.$solution_image_name;
              $request->file('solution_image')->move($path, $solution_image_name);
          }
          else
          {
              $solution_image_name=$request->old_solution_image;
          }

          
            $question_data=[];
            $question_data['data']['question'] = array(
            'value' => $question,
            'image' => $question_image_name
            );
            $question_data['data']['option a'] = array(
            'value' => $option_a,
            'image' => $option_a_image_name
            );
            $question_data['data']['option b'] = array(
            'value' => $option_b,
            'image' => $option_b_image_name
            );
            $question_data['data']['option c'] = array(
            'value' => $option_c,
            'image' => $option_c_image_name
            );
            $question_data['data']['option d'] = array(
            'value' => $option_d,
            'image' => $option_d_image_name
            );
            $question_data['data']['option e'] = array(
            'value' => $option_e,
            'image' => $option_e_image_name
            );
            $question_data['data']['answer'] = array(
            'value' => $answer,
            'image' => $answer_image_name
            );
            $question_data['data']['hint'] = array(
            'value' => $hint,
            'image' => $hint_image_name
            );
            $question_data['data']['solution'] = array(
            'value' => $solution,
            'image' => $solution_image_name
            );
            // $question_data['data']['ans_status'] = $ans_status;

        $json=json_encode($question_data);

        $update=DB::table('test_question')->where('id',$request->id)->update(
            ['jsondata'=>$json,'subject_name'=>$request->subjectQuestion]
        );
        $update1= DB::table('common_table')->where('id',$request->test_id)->update(
            ['status'=>'notpublished']
        );
            return redirect('question/'.$request->id)->with('success',"Question Updated Successfully.");
        // if($update)
        // {
        // }
        // else
        // {
        //     return redirect()->back()->with('error',"Something went wrong. Please try again.");
        // }


    }

    public function delete_question(Request $request)
    {
       $id=$request->del_id;
       $test_id=$request->test_id;
       $update=DB::table('common_table')->where('id',$request->test_id)->update(
            ['status'=>'notpublished',]
        );
        $delete=DB::table('test_question')->where('id',$id)->delete();
        if($delete)
        {
            return redirect('questions/'.$test_id.'')->with('success','Question Deleted Successfully');
        } 
    }

    public function send_test_notification($id)
    {
        

      
        $post=DB::table('common_table')->where('id',$id)->first();
        $field_type=$post->field_type;
        $json=json_decode($post->jsondata,true);
        if(isset($json['image']))
        {
            $image=$json['image'];
        }
        else
        {
            $image=url('/mechanicalinsider/admin/images/test_image.png');
        }
        if($image=="")
        {
            $image=url('/mechanicalinsider/admin/images/test_image.png');
        }
        $groupid=explode(",",$post->groupid);

        print_r($post);


$content = array(
    "en" => $json['testname']
    
    );
 if($post->post_type=='2')
 {
    $fields = array(
        'app_id' => "e73bcec1-8c19-476d-a4c6-d5bc3ab47ed8",
        'included_segments' => array('All'),
        
           'data' => array("id" => $id,"field_type"=>$field_type),
        'contents' => $content,
         "large_icon" => "",
        "big_picture" => $image
       
       );
 }
 else{
    $fields = array(
        'app_id' => "e73bcec1-8c19-476d-a4c6-d5bc3ab47ed8",
        'included_segments' => $groupid,
        
           'data' => array("id" => $id,"field_type"=>$field_type),
        'contents' => $content,
         "large_icon" => "",
        "big_picture" => $image
       
       );
 }

       
          
		
         
           
          $fields = json_encode($fields);
          // print_r($fields);
          // dd($fields);
           
           
          
          $ch = curl_init();
          
            
          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MGI4ZWFiMmYtZjhkNi00MmEzLTkyNGItMWQ1OWRmZGFmMzg4',
                                                     'Content-Type: application/json; charset=utf-8'));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_HEADER, FALSE);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

          $response = curl_exec($ch);
          curl_close($ch);
          $result=json_decode($response,true);
          // dd($result);

 //new code close



          if(array_key_exists('id', $result))
          {
            $notification=SentNotification::where('postid',$id)->first();
            if(isset($notification->postid))
            {
                SentNotification::where('postid',$id)->update(['postid'=>$id]);
            }
            else
            {
                
                $notification = new SentNotification();

                $notification->postid = $id;

                $notification->save();
            }
            return redirect()->back()->with('success','Notification Sent Successfully');
          }
          else
          {
            return redirect()->back()->with('error','Something went wrong.Please try again.');
          }
    
    }


    public function send_notification($id)
    {
        $post=DB::table('common_table')->where('id',$id)->first();
        $field_type=$post->field_type;
        $json=json_decode($post->jsondata,true);
        $image=str_replace(" ","%20",$json['pic']);
        $thumbnail=str_replace(" ","%20",$json['thumbnail']);
        $base_url=url('/mechanicalinsider/newsimage/');

        if($post->groupid=='all')
        {
        $groups1=DB::table('discussions')->orderBy('id','desc')->get();
        foreach ($groups1 as $group){
            $groups[]=$group->id;
        }    
        $group=json_encode($groups);
        $str = join(",", $groups);
        $groupid=explode(',',$str); 
        }

        else
        {
        $groupid=explode(",",$post->groupids);   
        }
        
   //old  code
        // $uid=[];
        // foreach ($groupid as $gid) {
        //     $data=DB::table('group_follow_rec')->where('group_name',$gid)->get();
        //    foreach ($data as $d) {
        //     if(!in_array($d->profile_uid, $uid))
        //         {
        //             $uid[]=$d->profile_uid;
        //         }
        //    }
        // }
        // // dd($uid);
        // $player_id=[];
        // foreach ($uid as $u) {
        //     $players=DB::table('users')->where('email',$u)->get();
        //     foreach ($players as $player) {
        //             $json1=json_decode($player->notification,true);
        //             if($json1['institute']==true)
        //             {
        //                 $player_list[]=$player;
        //             }

        //         }
            
        // }
        // foreach ($player_list as $player) {
                
        //             $player_id[]=$player->gcmid;
                
        //     }
        // $content = array(
        //    "en" => $json['title']
           
        //    );
          
        //   $fields = array(
        //    'app_id' => "e73bcec1-8c19-476d-a4c6-d5bc3ab47ed8",
        //    // 'included_segments' => $pp,
        //    'include_player_ids' => $player_id,
           
        //       'data' => array("key" => "value"),
        //    'contents' => $content,
        //    "large_icon" => "$base_url$thumbnail",
        //    "big_picture" => "$base_url$image"
          
        //   );
        //   // print_r($fields);
           
        //   $fields = json_encode($fields);
           
           
          
        //   $ch = curl_init();
          
            
        //   curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        //   curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MGI4ZWFiMmYtZjhkNi00MmEzLTkyNGItMWQ1OWRmZGFmMzg4',
        //                                              'Content-Type: application/json; charset=utf-8'));
        //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //   curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //   curl_setopt($ch, CURLOPT_POST, TRUE);
        //   curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //   $response = curl_exec($ch);
        //   curl_close($ch);
        //   $result=json_decode($response,true);
  //old code clsoe




		$content = array(
           "en" => $json['title']
           
           );

          $fields = array(
           'app_id' => "e73bcec1-8c19-476d-a4c6-d5bc3ab47ed8",
           'included_segments' => $groupid,
           
              'data' => array("id" => $id,"field_type"=>$field_type),
           'contents' => $content,
            "large_icon" => $base_url.'/'.$thumbnail,
           "big_picture" => $base_url.'/'.$image
          
          );
         
           
          $fields = json_encode($fields);



          // dd($fields);
          // print_r($fields);
           
           
          
          $ch = curl_init();
          
            
          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MGI4ZWFiMmYtZjhkNi00MmEzLTkyNGItMWQ1OWRmZGFmMzg4',
                                                     'Content-Type: application/json; charset=utf-8'));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_HEADER, FALSE);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

          $response = curl_exec($ch);
        
          curl_close($ch);
          $result=json_decode($response,true);
          
          

 //new code close



          if(array_key_exists('id', $result))
          {
            $notification=SentNotification::where('postid',$id)->first();
            if(isset($notification->postid))
            {
                SentNotification::where('postid',$id)->update(['postid'=>$id]);
            }
            else
            {
                
                $notification = new SentNotification();

                $notification->postid = $id;

                $notification->save();
            }
            return redirect()->back()->with('success','Notification Sent Successfully');
          }
          else
          {
            return redirect()->back()->with('error','Something went wrong.Please try again.');
          }
    
    }


    public function send_custom_notification(Request $request)
    {
        $id=Auth::user()->id;
        if(Auth::user()->groups=="all")
		{
			$groups=DB::table('discussions')->orderBy('id','desc')->get();
			foreach ($groups as $group){
				$groupid[]=$group->id;
			}
			$groupid=implode(",", $groupid);
			$data=(object)['groups'=>$groupid];
		}
		else
		{
		$data=DB::table('admins')->where('id',$id)->select('groups')->first();
		}

        $val= strrchr($data->groups,",");
        if($val)
        {
            $groups=explode(",",$data->groups);
        }
        else
        {
            $groups[]=$data->groups;
        }
        // $uid=[];
        // foreach ($groups as $group) {
        //     $data=DB::table('group_follow_rec')->where('group_name',$group)->get();
        //    foreach ($data as $d) {
        //     if(!in_array($d->profile_uid, $uid))
        //         {
        //             $uid[]=$d->profile_uid;
        //         }
        //    }
        // }

        // $player_id=[];
        // foreach ($uid as $u) {
        //     $players=DB::table('users')->where('email',$u)->get();
        //     if(Auth::user()->groups=="all")
        //     {
        //         foreach ($players as $player) {
        //             $json1=json_decode($player->notification,true);
        //             if($json1['content']==true)
        //             {
        //                 $player_list[]=$player;
        //             }

        //         }
                
        //     }
        //     else
        //     {
        //         foreach ($players as $player) {
        //             $json1=json_decode($player->notification,true);
        //             if($json1['institute']==true)
        //             {
        //                 $player_list[]=$player;
        //             }

        //         }
        //     }

            
        // }
        // foreach ($player_list as $player) {
                    
        //                 $player_id[]=$player->gcmid;
                    
        //         }
        // $content = array(
        //    "en" => $request->description
           
        //    );
          
        //   $fields = array(
        //    'app_id' => "e73bcec1-8c19-476d-a4c6-d5bc3ab47ed8",
        //    // 'included_segments' => $pp,
        //    'include_player_ids' => $player_id,
           
        //       'data' => array("key" => "value"),
        //    'contents' => $content,
           
          
        //   );
        //   // print_r($fields);
           
        //   $fields = json_encode($fields);
           
           
          
        //   $ch = curl_init();
          
            
        //   curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        //   curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MGI4ZWFiMmYtZjhkNi00MmEzLTkyNGItMWQ1OWRmZGFmMzg4',
        //                                              'Content-Type: application/json; charset=utf-8'));
        //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //   curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //   curl_setopt($ch, CURLOPT_POST, TRUE);
        //   curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //   $response = curl_exec($ch);
        //   curl_close($ch);
        //   $result=json_decode($response,true);
        //   // print_r($result);

 //old code closes
//new code

          $content = array(
           "en" => $request->description
           
           );
          
          $fields = array(
           'app_id' => "e73bcec1-8c19-476d-a4c6-d5bc3ab47ed8",
           'included_segments' => $groups,
           
              'data' => array("key" => "value"),
           'contents' => $content,
           
          );

          $fields = json_encode($fields);
          // print_r($fields);
           
           
          
          $ch = curl_init();
          
            
          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MGI4ZWFiMmYtZjhkNi00MmEzLTkyNGItMWQ1OWRmZGFmMzg4',
                                                     'Content-Type: application/json; charset=utf-8'));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_HEADER, FALSE);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

          $response = curl_exec($ch);
          curl_close($ch);
          $result=json_decode($response,true);
          // dd($result);

 //new code close

          if(array_key_exists('id', $result))
          {
            
            return redirect()->back()->with('success','Notification Sent Successfully');
          }
          else
          {
            return redirect()->back()->with('error','Something went wrong.Please try again.');
          }
    
    }

    public function search(Request $request)
    {
        $field_type=1;
		$prefix=$request['val'];
    
		
		$record=DB::select("select * from common_table where field_type='1' AND  (post_description LIKE '%$prefix%' OR posttype LIKE '%$prefix%' OR jsondata LIKE '%$prefix%') ORDER BY `id` DESC");
   
       $new_record=[];
    foreach ($record as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec->groupid;
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
    	$new_record=$record;
    }
    if(!empty($new_record))
    {
      foreach ($new_record as $key => $new_rec) {
      $notification=DB::table('sent_notifications')->where('postid',$new_rec->id)->first();
            if($notification)
            {
                $new_rec->notification="sent";
            }
            else
            {
                $new_rec->notification="";
            }

        $comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$new_rec->id)->get();
        foreach ($comments as $comment) {
            $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
            foreach ($reply as $key => $rep) {
              $comment->reply[]=$rep;
            }
          $new_rec->comments[]=$comment;
        }
      }
    } 
    $record=$new_record;
        return view::make('post_search',compact('record'));
    }


    public function delete_reply(Request $request)
    {
        $id=$request->id;
        $detail=DB::table('comment_on_post')->where('id',$request->commentid)->first();
        $total_comment=$detail->comment;
        $postid=$detail->postid;
        $comment=$total_comment-1;
        $update=DB::table('comment_on_post')->where('id',$request->commentid)->update(['comment'=> $comment]);
        $data1=DB::table('common_table')->where('id',$postid)->first();
        $common_comment=$data1->comment;
        $new_comment=$common_comment-1;
        $update1=DB::table('common_table')->where('id',$postid)->update(['comment'=> $new_comment]);
        $delete=DB::table('comment_replies')->where('id',$id)->delete();

        $data=DB::table('comment_replies')->where('commentid',$request->commentid)->get();

        if($delete)
        {
            return view::make('reply_comment',compact('data'));
        }
    }


    public function delete_comment(Request $request)
    {
        $detail=DB::table('common_table')->where('id',$request->postid)->first();
        $total_comment=$detail->comment;
        $data=DB::table('comment_on_post')->where('id',$request->commentid)->first();
        $total_reply=$data->comment;
        $total_comment=$total_comment-$total_reply;

        $comment=$total_comment-1;

        $update=DB::table('common_table')->where('id',$request->postid)->update(['comment'=>$comment]);
        $delete_reply=DB::table('comment_replies')->where('commentid',$request->commentid)->delete();
        $delete=DB::table('comment_on_post')->where('id',$request->commentid)->delete();
        $postid=$request->postid;
        $new_limit=$request->limit;
        
        $com2=DB::table('comment_on_post')->where('postid',$postid)->get();
        foreach ($com2 as $comment) {
                $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
                foreach ($reply as $key => $rep) {
                    $comment->reply[]=$rep;
                }
            }
        return view::make('ajax',compact('com2','new_limit','postid'));
    }

    public function ajax(Request $request)
    {
        $postid=$request->postid;
        // $field_type=$request->field_type;
        $new_limit=$request->limit;
        
        $com2=DB::table('comment_on_post')->where('postid',$postid)->get();
        foreach ($com2 as $comment) {
                $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
                foreach ($reply as $key => $rep) {
                    $comment->reply[]=$rep;
                }
            }
        return view::make('ajax',compact('com2','new_limit','postid'));
    }

    public function chart_data(Request $request)
    {
        
      
        $user=Auth::user();

      if(Auth::user()->groups=="all"||$user->hasRole('er.chaudhary.sandeep@gmail.com')){
	      
	        for ($i=0; $i <11 ; $i++) { 
	          $current_date=date('Y-m-d',strtotime('-'.$i.'days'));
	          $previous_date=date('Y-m-d',strtotime('-1 days',strtotime($current_date)));
	          $next_date=date('Y-m-d',strtotime('+1 days',strtotime($current_date)));
	            $count=DB::table('users')->where('created_at','>',$current_date)->where('created_at','<',$next_date)->count();
	            $cat[]=$current_date;
	            $users[]=$count;
	          }
	        $dd=['cat'=>$cat,'users'=>$users];

	        $latest_users=DB::table('users')->orderBy('id','desc')->limit(6)->get();
	      
	  }   
      else
      {
        $groups=Auth::user()->groups;
        $groups1=$groups;
        $groups=explode(',',$groups);

        for ($i=0; $i <11 ; $i++) { 
          $current_date=date('Y-m-d',strtotime('-'.$i.'days'));
          $previous_date=date('Y-m-d',strtotime('-1 days',strtotime($current_date)));
          $next_date=date('Y-m-d',strtotime('+1 days',strtotime($current_date)));
          $count1=0;
              foreach ($groups as $group) {
                $count=DB::table('group_follow_rec')->where('created_at','>',$current_date)->where('created_at','<',$next_date)->where('group_name',$group)->count();
                $count2=$count1+$count;
                $count1=$count;

              }
            $cat[]=$current_date;
            $users[]=$count2;
          }
             
        $dd=['cat'=>$cat,'users'=>$users];
        $user_group=DB::table('group_follow_rec')->whereIn('group_name',$groups)->orderBy('id','desc')->get();
        // dd($user_group);
        $users=[];
        foreach ($user_group as $user) {
            if(!in_array($user->profile_uid,$users))
            {
                $users[]=$user->profile_uid;
            }
            if(count($users)==6)
            {
                break;
            }

        }
        // dd($users);
        foreach ($users as $user) {
            $latest_users[]=DB::table('users')->where('email',$user)->first();
        }
        // echo DB::table('users')
        //     ->join('group_follow_rec', 'users.email', '=', 'group_follow_rec.profile_uid')
        //     ->select('users.name')->whereIn('group_name',$groups)->groupBy('name')->orderBy('group_follow_rec.id')->limit(6)
        //     ->get();

        
      }
      

        return view::make('users_chart',compact('dd','latest_users'));
    }


    public function test_search(Request $request)
    {
        $field_type=2;
		$prefix=$request['val'];
    
		
		$record=DB::select("select * from common_table where field_type='2' AND premium='0' AND  (post_description LIKE '%$prefix%' OR posttype LIKE '%$prefix%' OR jsondata LIKE '%$prefix%') ORDER BY `id` DESC");
   
       $new_record=[];
    foreach ($record as $key=>$rec) {
      $user_groups=Auth::user()->groups;
      $user_groups_is_array= strrchr($user_groups,",");

      $post_groups=$rec->groupid;
      $post_groups_is_array=strrchr($post_groups,",");

      if($user_groups_is_array)
      {
        $user_group=explode(',',$user_groups);

        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($post_group[0],$user_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if(in_array($post_groups,$user_group))
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
      else
      {
        if($post_groups_is_array)
        {
          $post_group=explode(',',$post_groups);
            if(in_array($user_groups,$post_group))
            {
              $new_record[]=$rec;
            }
        }
        else
        {
          if($post_groups==$user_groups)
          {
            $new_record[]=$rec;
          }
          elseif($post_groups=="all"||$post_groups=="All")
          {
            $new_record[]=$rec;
          }
        }
      }
    }
    if(Auth::user()->groups=="all")
    {
    	$new_record=$record;
    }
    if(!empty($new_record))
    {
      $new_record=$this->filter_test($new_record);
      // foreach ($new_record as $key => $new_rec) {
      // $notification=DB::table('sent_notifications')->where('postid',$new_rec->id)->first();
      //       if($notification)
      //       {
      //           $new_rec->notification="sent";
      //       }
      //       else
      //       {
      //           $new_rec->notification="";
      //       }
      //   $applicant=DB::table('result_record')->where('testid',$new_rec->id)->count();
      //   $new_rec->applicant=$applicant;

      //   $comments=DB::table('comment_on_post')->where('posttype',$field_type)->where('postid',$new_rec->id)->get();
      //   foreach ($comments as $comment) {
      //       $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
      //       foreach ($reply as $key => $rep) {
      //         $comment->reply[]=$rep;
      //       }
      //     $new_rec->comments[]=$comment;
      //   }
      // }
    } 
    $record=$new_record;
        return view::make('test_search',compact('record'));
    }


    public function users_results($id)
    {
      $data=DB::table('result_record')->where('testid',$id)->orderBy('mark','desc')->orderBy('timetaken','ASC')->get();

        if(count($data)>0)
        {
          foreach($data as $email)
          {
            
            $user=DB::table('users')->where('email',$email->uid)->first();
            if($user)
            {
              $email->user=$user;
            }
            else
            {
              $email->user='';
            }
            
          }
        }
      return view::make('users_test_result',compact('data','id'));
    }

    public function app_user_csv(Request $r)
    {
      if(!empty($r->from) && !empty($r->to))
      {

      
      $from=$r->from;
      $to=$r->to;
      $app_user=DB::table('users')->whereBetween('created_at', [$from, $to])->get();

      $CsvData[] = "No.,Name,Email,Mobile Number,College,City,State";       
      foreach ($app_user as $key=>$row) {    
       
         //$CsvData[]=$value->country_id.','.$value->country_name;
          $CsvData[] = ++$key.','.$row->name.','.$row->email.','.$row->mobile.','.$row->college.','.$row->city.','.$row->state;
      }
       
      $filename='appuser_'.date('Y-m-d').".csv";
      $file_path=base_path().'/'.$filename;   
      $file = fopen($file_path,"w+");
      foreach ($CsvData as $exp_data){
        fputcsv($file,explode(',',$exp_data));
      }   
      fclose($file);          
  
      $headers = ['Content-Type' => 'application/csv'];
      return response()->download($file_path,$filename,$headers );
    }
    else
    {
      return redirect::back();
    }
    }

    public function users_results_csv($id)
    {
        $date = date("d M Y");
        $users = DB::table('result_record')
            ->join('users', 'result_record.uid', '=', 'users.email')
            ->select('users.name','users.college','users.email','users.mobile','result_record.mark')
            ->orderBy('result_record.mark','desc')
            ->where('result_record.testid',$id)
            ->get();
foreach($users as $u)
{
    $u->college = str_replace(","," ",$u->college);
}
    $tot_record_found=1;
         
   // $CsvData[]=implode(",",array('Rank', 'Name', 'Collage','Email','Mobile Number', 'marks'));   
    $CsvData[] = "Rank, Name, Collage,Email,Mobile Number,marks";       
    foreach ($users as $key=>$row) {        
       //$CsvData[]=$value->country_id.','.$value->country_name;
        $CsvData[] = ++$key.','.$row->name.','.$row->college.','.$row->email.','.$row->mobile.','.$row->mark;
    }
     
    $filename=date('Y-m-d').".csv";
    $file_path=base_path().'/'.$filename;   
    $file = fopen($file_path,"w+");
    foreach ($CsvData as $exp_data){
      fputcsv($file,explode(',',$exp_data));
    }   
    fclose($file);          

    $headers = ['Content-Type' => 'application/csv'];
    return response()->download($file_path,$filename,$headers );
    }




    public function view_user_posts()
    {
        $ref_code=Auth::user()->ref_code;  

        $record=DB::table('common_table');

        if($ref_code == '')
        {
          $record=$record->where('ref_code','')->Orwhere('ref_code',NULL);
        }
        else {
          $record=$record->where('ref_code', $ref_code);
        }
        
        
        $record=$record->whereIn('field_type', [0, 7, 8])->orderBy('timestamp','desc')->paginate(15);
        $next_page_url=$record->nextPageUrl();
        foreach ($record as $key => $rec) {
            $rec->results = array();
            if($rec->field_type=='7')
            {
                $resper = array();

               
                $total=DB::table('postquestion_answer')->where('postid',$rec->id)->count();
                $resper['total'] = $total;
                $answerscount = DB::select("SELECT answer,COUNT(id) as count FROM `postquestion_answer` WHERE postid='$rec->id' GROUP BY answer");
                foreach($answerscount as $an)
                {
                    $resper[$an->answer] = $an->count;
                    $resper[$an->answer.'%'] =   round(($an->count * 100) / $total,2);
                }
                $rec->results = $resper;
            }
            $comments=DB::table('comment_on_post')->where('posttype',$rec->field_type)->where('postid',$rec->id)->get();
            foreach ($comments as $comment) {
                $reply=DB::table('comment_replies')->where('commentid',$comment->id)->get();
                foreach ($reply as $key => $rep) {
                    $comment->reply[]=$rep;
                }
              $rec->comments[]=$comment;
            }
        }

        // echo "<pre>";
        // print_r($record);
        // die;
        // dd($record);
        return view::make('view_user_posts',compact('record','next_page_url'));
    }

    public function approve_user_post($id)
    {
        $id=decrypt($id);
        $record=DB::table('common_table')->where('id',$id)->update(['status'=>'published']);
        return redirect()->back();
    }

    public function disapprove_user_post($id)
    {
        $id=decrypt($id);
        $record=DB::table('common_table')->where('id',$id)->update(['status'=>'notpublished']);
        return redirect()->back();
    }

    public function bulk_question(Request $r)
    {
        $title='Bulk Question';
        return view::make('bulk_question',compact('title'));
    }

    public function submit_bulk(Request $r)
    {
      try {
        
      
        $fileD = fopen($r->file('file'),"r");
        $column=fgetcsv($fileD);
        while(!feof($fileD))
        {
            $rowData[]=fgetcsv($fileD);
        }

       
        $insertion=array();
        foreach ($rowData as $key => $value) 
        {      
                       
          $question_data=[];
          $question_data['data']['question'] = array(
            'value'=> $value[2] ?? 'NULL',
            'image' => 'NULL',
          );

          $optiona='';
          if(isset($value[4]))
          {
            $optiona=$value[4];
          }

          $question_data['data']['option a'] = array(
            'value' =>$optiona,
            'image' => 'NULL',
            'orignal' => 'a'
          );

          $optionb='';
          if(isset($value[5]))
          {
            $optionb=$value[5];
          }

          $question_data['data']['option b'] = array(
            'value' =>$optionb,
            'image' => 'NULL',
            'orignal' => 'b'
          );

          $optionc='';
          if(isset($value[6]))
          {
            $optionc=$value[6];
          }

          $question_data['data']['option c'] = array(
            'value' => $optionc,
            'image' => 'NULL',
            'orignal' => 'c'
          );

          $optiond='';
          if(isset($value[7]))
          {
            $optiond=$value[7];
          }

          $question_data['data']['option d'] = array(
            'value' => $optiond,
            'image' => 'NULL',
            'orignal' => 'd'
          );

          $optione='';
          if(isset($value[8]))
          {
            $optione=$value[8];
          }

          $question_data['data']['option e'] = array(
            'value' => $optione,
            'image' => 'NULL',
            'orignal' => 'e'
          );

          $answer='';
          if(isset($value[9]))
          {
            $answer=$value[9];
          }

          $question_data['data']['answer'] = array(
            'value' => $answer,
            'image' => 'NULL'
          );


          $hint='';
          if(isset($value[3]))
          {
            $hint=$value[3];
          }

          $question_data['data']['hint'] = array(
            'value' => $hint,
            'image' => 'NULL'
          );

          $solution='';
          if(isset($value[10]) ){
            if($value[10] != '')
            {
              $solution=$value[10];
            }
          }
          

          $question_data['data']['solution'] = array(
            'value' => $solution,
            'image' => 'NULL'
          );

          $question_data['data']['by'] = array(
            'uid' => Auth::user()->name,
            'name' =>Auth::user()->email
          );

          $question_data['data']['time'] = array(
            'question_time' => null,
          );

          $json=json_encode($question_data);


          if(isset($value[0]))
          {
            $insertion[]=array(
              'testid'=>$value[0],
              'subject_name'=>$value[1],
              'sub_test_name'=>$value[1],
              'jsondata'=>$json,
              'timestamp'=>date('Y-m-d h:i:s'),
            );
          }

        }
       
        foreach (array_chunk($insertion,1000) as $t) {
          DB::table('test_question')->insert($t);
        }

        return back()->with('success','Questions has been added successfully.');
      } catch (Exception $e) {
            // print_r($e->getMessage());
            return back()->with('error',$e->getMessage());
        }  
    }


    public function publish_testSeries(Request $r)
    {
        $time=date('Y-m-d H:i:s');
        $update = DB::table('common_table')->where('id',$r->del)->update(['status' => $r->type,'timestamp'=>$time]);
        if($update)
        {
            return back()->with('success','Test has been '.$r->type.' successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }

    public function view_academy_info(){

      $data=Academy_Info::orderBy('id','DESC')->paginate(10);

      return view::make('view_academy_info',compact('data'));

    }


    //Add Test Series
    public function add_test_series()
    { 
        $sectors=DB::table('sectors')->get();        
        $test=DB::table('common_table')->where('field_type',2)->where('post_type',2)->where('status','published')->select('jsondata','id')->get();
        
        if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin')
        {
          $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();
        }
        else
        {
          $courseID=json_decode(Auth::user()->course_id);          
          $new_Courses=DB::table('new_courses')->whereIn('id',$courseID)->orderBy('name','ASC')->get();
        }

        return view::make('add_test_series',compact('sectors','test','new_Courses'));
    }

    //Submit Test Series
    
    public function submit_test_series(Request $r){

      $file = $r->file('image');

      if ($file!='' && $file!='null') {
          $rand = rand(1, 1000000);
          $destinationPath = public_path().'/img';
          $filename = $file->getClientOriginalName();
          $filename = str_replace(' ', '_', $filename);
          $filename = $rand.$filename;
          $upload = $file->move($destinationPath, $filename);
      } else {
          $filename = '';
      }

      //amount
      $amount=$r->amount;
      if($r->payment == 'free'){
        $amount=0;
      }

      //School ID
      $school=Auth::user()->id;
      $ref_code=Auth::user()->ref_code;

      $insert = DB::table('test_series')
      ->insert(['school_id'=>$school,'ref_code'=>$ref_code,'name'=>$r->title,'price' =>$amount,'payment'=>$r->payment ,'sector_id'=>$r->sector,'image'=>$filename,'test_id'=>json_encode($r->test_id),'course_id'=>json_encode($r->course_id)]);

      if($insert)
      {
        return redirect()->route('view_test_series')->with('success','Test Series add successfully.');
      }
      else
      {
        return back()->with('error','Something went wrong, please try again!');
      }

    }

    public function view_test_series(){

      $school_id=Auth::user()->id;
      
      $data = DB::table('test_series')
      ->join('sectors',  'test_series.sector_id', '=', 'sectors.id')
      ->select('test_series.*','sectors.name as sectorname')
      ->where('test_series.delete_status',1)
      // ->where('test_series.school_id',$school_id)
      ->orderBy('id','DESC')
      ->paginate(10);   
     
      return view::make('view_test_series',compact('data'));

    }

    public function publish_test_series(Request $r)
    {
        $update = DB::table('test_series')->where('id',$r->del)->update(['status' => $r->type]);
        if($update)
        {
            return back()->with('success','Test Series has been '.$r->type.' successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }

    public function delete_test_series(Request $r){
      $del = DB::table('test_series')->where('id',$r->del_id)->update(['delete_status'=>0]);
      if($del)
      {
          return redirect()->route('view_test_series')->with('success','Test Series has been delete successfully.');
      }
      else
      {
          return back()->with('error','Something went wrong, please try again!');
      }
    }


    //update test
    public function edit_series($id)
    {
      $id=decrypt($id);

      $data=DB::table('test_series')->where('id',$id)->first();

      $sectors=DB::table('sectors')->get();
      $test=DB::table('common_table')->where('field_type',2)->where('post_type',2)->where('status','published')->select('jsondata','id')->get();
      
      if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin')
      {
        $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();
      }
      else
      {
        $courseID=json_decode(Auth::user()->course_id);          
        $new_Courses=DB::table('new_courses')->whereIn('id',$courseID)->orderBy('name','ASC')->get();
      }

      return view::make('edit_series',compact('data','sectors','test','new_Courses'));
    }

    public function update_series(Request $r){

      $file = $r->file('image');
        if ($file!='' && $file!='null') {
            $rand = rand(1, 1000000);
            $destinationPath = public_path().'/img';
            $filename = $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $filename = $rand.$filename;
            $upload = $file->move($destinationPath, $filename);
        } else {
            $filename = $r->old_image;
        }


        // Check category 
        if($r->course_id == '' || $r->course_id == []){
          return back()->with('error','Sorry,Course ID can not be empty.');
        }

        $amount=$r->amount;
        if($r->payment == 'free'){
          $amount=0;
        }

        $insert = DB::table('test_series')->where('id',$r->id)
        ->update(['name'=>$r->title,'price' =>$amount,'payment'=>$r->payment,'sector_id'=>$r->sector,'image'=>$filename,'test_id'=>json_encode($r->test_id),'course_id'=>json_encode($r->course_id) ]);

        if($insert)
        {
          return redirect()->route('view_test_series')->with('success','Test Series updated successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }

    }
   

      public function array(){
        
        $arr=[2,3,4,5,3,5,3,4,7];
        $temp_array =[];

        foreach ($arr as $key=> $value) {

          if(++$temp_array[$value] > 1 ){
            echo $value; break;
          }else{
            $temp_array[$value] = $value;
          }

        }
       
      }

}