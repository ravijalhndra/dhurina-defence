<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Institute;
use App\InstituteSubadmin;
use Hash;
use Mail;
use App\Mail\AddEmail;
use Auth;

class InstituteController extends Controller
{
 	public function add_institute_subadmin(Request $request)
 	{
 		$all=$request->all();

    	$characters = '0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
						
            $charactersLength = strlen($characters);
            $length='8';
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
    		}
    		session(['password' => $randomString]);
    	try{

    		$subadmin=new User($all);
    		$subadmin->password=Hash::make($randomString);
    		$subadmin->save();



    		if($subadmin)
    		{
    			$user=User::find($subadmin->id);
    			$user->attachRole('4'); // parameter can be an Role object, array, or id
    			$user_id=Auth::user()->id;
 				$inst=Institute::where('admin_id',$user_id)->first();
 				$institute_id=$inst->id;


    			$institute=new InstituteSubadmin($all);
    			$institute->admin_id=$subadmin->id;
    			$institute->institute_id=$institute_id;
    			$institute->save();

	    		if($institute)
	    		{
	    			$add='institute_sub_admin';
	    			$password=$randomString;
                    $email=$request->email;

	    			Mail::to($email)->send(new AddEmail($add,$password,$email));

	            	return redirect('view_institute_subadmin')->with('success','Institute Employee Added Successfully.');
	    		}
    		}
    	}	
    	catch(Exception $e)
    	{
    		return redirect('add_institute_subadmin')->with('error','Something Went wrong.Please Try again.');
    	}
 	} 

 	public function view_institute_subadmin()
    {
        $user_id=Auth::user()->id;
		$inst=Institute::where('admin_id',$user_id)->first();
		$institute_id=$inst->id;
        $subadmins=InstituteSubadmin::where('institute_id',$institute_id)->get();
        return view('view_institute_subadmin',compact('subadmins'));
    }


}
