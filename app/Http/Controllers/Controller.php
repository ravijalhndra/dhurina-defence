<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use URL;
use App\notification;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function apicall($name,$meetingID,$password,$checksum)
    {
        $url='https://school.dhurina.in/bigbluebutton/api/create?name='.preg_replace('/\s+/', '_', $name).'&meetingID='.$meetingID.'&attendeePW=123456&moderatorPW='.$password.'&checksum='.$checksum;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $retValue = curl_exec($ch);          
        curl_close($ch);
        return $retValue;
    }

    public function get_checksum ($type,$name,$meetingID,$password)
    {
        if($type == 'create')
		{
		   $checksum= sha1('createname='.$name.'&meetingID='.$meetingID.'zp28SYNqcyfacl5ZSja6nPUZlFJKpLK01vX22mujE');
		}else
		{
			$checksum= sha1('joinfullName='.$name.'&meetingID='.$meetingID.'&password='.$password.'&userID='.$UserID.'zp28SYNqcyfacl5ZSja6nPUZlFJKpLK01vX22mujE');
			// $checksum= sha1('joinfullName=Pawan&meetingID=Rohit_123&password=QR6ijX3m&userID=5290zp28SYNqcyfacl5ZSja6nPUZlFJKpLK01vX22mujE');
		}

		return $checksum;
    }

    public function check_count($check)
	{
		if($check)
		{
			$val =  sizeof(json_decode(json_encode($check),true));
		}
		else
		{
			$val =  0;
		}
		return $val;
    }
    

    public function push_notification($content,$data,$type,$player_id)
	{
		$message = array("en" =>$content);
		$fields = array( 
			'app_id' => '43729d91-558f-4c77-b1b7-c355d663afdd', 
			'priority'=>'high',
			'contents' => $message,
			'data' => $data,
		); 
		
		if($type == 'tags')
		{
			$fields['tags'] = $player_id;
		}
		
		elseif ($type == 'player_id') 
		{
			$fields['include_player_ids'] = $player_id;
		}	
		else 
		{
			$fields['included_segments'] = array('Active Users');
		}
		if (array_key_exists("image_url",$data))
		{			
			$fields['big_picture'] = $data['image_url'];				
		}
		
		$fields = json_encode($fields);
			// print("\nJSON sent:\n");
			// print($fields);	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
					'Authorization: Basic NmEwYjkxNjYtNGJlYy00MGQ5LTg2NGItYTAwOTNiNThjMTZm'));
	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;

	}


	//OTP service
	public function sms($mobile,$otp)
	{
		return file_get_contents("https://2factor.in/API/V1/805e3c66-8869-11ea-9fa5-0200cd936042/SMS/".$mobile."/".$otp."/Edudream+OTP+Verification");

	}


	//Media path
	public function path()
	{
		$path=URL::to('/');
		return $path;
	}

	//Vimeo access
	function get_vimeo()
    {
		//new token 14 oct 2020   35e210dba308031ea5b022a1d7a756d5
		//old token               162eb652085f7272fe8311336216656a
		$lib = new \Vimeo\Vimeo('60e3af022b57d0f8030220f837db3d413dd48686', 'qQmtHl/3iIikfz+CHSSGtb4uk26xFGVcqh3xO017yHRwHWO+uzh724eSvwDUdgGMdJ6wXs+PJFDBX8xBlvL4Tm0cMTNM5Xkulmq0dplFPtw3fhcFQk3fysq7sJPzwUFa', '35e210dba308031ea5b022a1d7a756d5');
		return $lib;
	}
	

	//hit Vimeo CURL
	function vimeo_curl($url)
	{
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: bearer 35e210dba308031ea5b022a1d7a756d5',
            'Content-Type: json',
            'Accept: application/vnd.vimeo.*+json;version=3.4'
        ));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close ($ch);

		$response = json_decode($server_output, true);
		return $response;
	}


	//Add Notification
	public function add_notify($r,$image,$type)
	{
		$insert=new notification;		
		$insert->message=$r->msg;
		$insert->mode=$r->type;
		$insert->type=$type;

		if($image != '')
		{
			$insert->image=$image;
		}

		$insert->save();
	}

	
	
}
