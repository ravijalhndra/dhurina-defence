<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Institute;
use App\SentNotification;
use Hash;
use Mail;
use Auth;
use View;
use DB;
use App\Mail\AddEmail;
use Zipper;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use App\Jobs\PremiumTestResult;
use Carbon\Carbon;

use App\Traits\Common;

class PremiumTestController extends Controller
{
    use Common;

   


    public function add_test()
    {
        return view::make('add_premium_test');
    }

    public function edit_test($id)
    {
        $field_type=2;
        $record=DB::table('common_table')->where('field_type',$field_type)->where('id',$id)->first();
        return view::make('edit_premium_test',compact('record'));
    }

    public function view_tests()
    {
        $field_type=2;
        $record=DB::table('common_table')->where('field_type',$field_type)->where('premium',1)->orderBy('id','desc')->paginate(10);
        $next_page_url=$record->nextPageUrl();
        if($record->currentPage()>1)
        {
            $json='true';
        }
        else
        {
            $json='false';
        }
        
    if(!empty($record))
    {
      $record=$this->filter_test($record);
    } 
    session(['record_count'=>count($record)]);

    if($json=='true')
    {
        return view::make('view_premium_tests_page',compact('record','next_page_url'));
    }
    
        return view::make('view_premium_tests',compact('record','next_page_url'));
    
    }

    public function submit_test(Request $request)
    {
        $request->request->add(['posttype' => "Premium"]);
        $premium=1;
        $test=$this->add_test_common($request,$premium);
        if($test>0)
        {
            return redirect('premium_test')->with('success','Test Added Successfully.');
        }
        else
        {
            return redirect('add_premium_test')->with('error','Something went wrong. Please try again.');

        }
        
    }

    public function update_test(Request $request)
    {
        $update=$this->update_test_common($request);
        if($update==0||$update==1){
            $test=DB::table('common_table')->where('id',$request->id)->first();
              $json_data=json_decode($test->jsondata,true);
              if(isset($json_data['job_id']))
              {
                $job_id=$json_data['job_id'];
                $delete=DB::table('jobs')->where('id',$job_id)->delete();  
              }
              

            return redirect('premium_test')->with('success','Test Updated Successfully.');
        }
        else
        {
            return redirect('edit_premium_test')->with('error','Something went wrong. Please try again.');

        }
        
    }

    public function test_search(Request $request)
    {
        $field_type=2;
        $prefix=$request['val'];
    
        
        $record=DB::select("select * from common_table where field_type='2' AND premium='1' AND  (post_description LIKE '%$prefix%' OR posttype LIKE '%$prefix%' OR jsondata LIKE '%$prefix%') ORDER BY `id` DESC");
       
        if(!empty($record))
        {
            $record=$this->filter_test($record);
            return view::make('premium_test_search',compact('record'));
        }
    }




}