<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Discussion;
use App\Category;
use Auth;
use View;
use DB;
use Redirect;
use App\Banner;
use App\OurUsers;
class GroupController extends Controller
{
    public function view_groups()
    {
        $discussions = Discussion::orderBy('id', 'desc')->get();

        return view::make('view_groups', compact('discussions'));
    }

    public function view_categories()
    {
        $datas = Category::orderBy('id', 'desc')->get();

        return view::make('view_categories', compact('datas'));
    }

    public function add_groups()
    {
        $categories = Category::all();

        return view::make('add_groups', compact('categories'));
    }

    public function add_banner()
    {
        $refcode = DB::table('admins')->orderBy('ref_code','ASC')->get();
        return view::make('add_banner', compact('refcode'));
    }

    public function add_department()
    {
        return view::make('add_department');
    }


    public function view_department()
    {
        $data=DB::table('department')->get();
        return view::make('view_department',compact('data'));
    }

    //Insert Data in Department Table
    public function insert_department(Request $request)
    {
        $department = $request->department;
        $dept = DB::table('department')->insert(['dept_name' => $department]);
        return redirect()->route('view_department');
    }
    //Edit Departments
    public function edit_department(Request $request)
    {
        $id=$request->id;
        $department= DB::table('department')->where('id', $id)->get();
        return view::make('edit_department',compact('department'));

    }

    //Delete Departments From table

    public function delete_department(Request $request)
    {
        $id = $request->del_id;
        DB::table('department')->where('id', $id)->delete();
        return redirect()->route('view_department');
     
    }

    public function update_department(Request $request)
    {
        $id=$request->id;
        $dept_name=$request->department;
        $department= DB::table('department')->where('id', $id)->update(['dept_name'=>$dept_name]);
        $data=DB::table('department')->get();
        return redirect()->route('view_department');

    }

    public function add_college()
    {
        $departments = DB::table('department')->get();
        $course = DB::table('course')->get();
        return view::make('add_college', compact('departments', 'course'));
    }

    public function edit_college($id)
    {
        $departments = DB::table('department')->get();
        $course = DB::table('course')->get();
        $college = DB::table('college')->where('id', $id)->get();
        return view::make('edit_college', compact('departments', 'course', 'college'));
    }

    public function update_college(Request $request)
    {
        $course_id = $request->course;
        $department = $request->department;
        $college_name = $request->college;
        $reg_no = $request->reg_no;
        $stream = $request->stream;
        $year = $request->year;
        $id = $request->id;
        DB::table('college')->where('id', $id)->update(['course_id' => $course_id, 'college_name' => $college_name, 'reg_no' => $reg_no, 'stream' => json_encode($department), 'year' => $year]);
        $college = DB::table('college')->get();
        $course=DB::table('course')->get();
       
        return view::make('view_college', compact('college','course'));
    }

    public function delete_college(Request $request)
    {
        $id = $request->del_id;

        DB::table('college')->where('id', $id)->delete();
        $college = DB::table('college')->get();

        return redirect()->route('view_college');
        // return view::make('view_college', compact('college'));
    }

    public function view_college()
    {
        $college = DB::table('college')
        ->join('course', 'college.course_id', '=', 'course.id')            
        ->select('course.*', 'college.*')
        ->get();
        $course=DB::table('course')->get();
        
        return view::make('view_college', compact('college','course'));
    }

    public function view_college_list()
    {
        $college = DB::table('college')->get();
        return view::make('college_list', compact('college'));
    }

    public function student_college(Request $request)
    {
       
        $college = Input::get('college_id');
        
        if ($college != '') {
            $data = DB::table('student_college')
            ->join('course', 'course.id', '=', 'student_college.courseid')
            ->join('college', 'college.id', '=', 'student_college.collegeid')
            ->join('users', 'users.email', '=', 'student_college.uid')
            ->select('student_college.*','users.id','users.name','users.mobile','users.type','course.course_name', 'college.college_name')
         ->where('college.id', $college)->paginate(10);
        } else {
            $data = DB::table('student_college')
            ->join('course', 'course.id', '=', 'student_college.courseid')
            ->join('college', 'college.id', '=', 'student_college.collegeid')
            ->join('users', 'users.email', '=', 'student_college.uid')
            ->select('student_college.*', 'users.id','users.name','users.email','users.mobile','users.type','course.course_name', 'college.college_name')
            ->paginate(10);


        }
      
         $course = DB::table('course')->get();
        $colleges = DB::table('college')->get();
       
       
        return view::make('student_clg', compact('data', 'course','colleges'));
    }


    public function filter_course(Request $req)
    {
        $college = DB::table('college')->where('course_id', $req->course)->get();

        return response($college);
    }

    // public function filter_student(Request $request)
    // {
    //     $course = DB::table('course')->get();
    //     $data = DB::table('student_college')->select('student_college.*', 'users.*', 'course.course_name', 'college.college_name')->join('course', 'course.id', '=', 'student_college.courseid')->join('college', 'college.id', '=', 'student_college.collegeid')->join('users', 'users.email', '=', 'student_college.uid')->where('course.id', $request->course)->where('college.id', $request->college)->get();

    //     // $course = DB::table('course')->select('city_name')->where('city_id',$request->city)->get();
    //     // $users=DB::table('users')->where('city',$cityselected)->orderBy('id','desc')->paginate('100');
    //     return view('ajax.search_student', compact('data', 'course'));
    // }

    //Edit student college

    public function edit_student_college($id)
    {
        $course = DB::table('course')->get();
        $college = DB::table('college')->get();
        $departments = DB::table('department')->get();

        $data = DB::table('student_college')
        ->select('student_college.*', 'users.*', 'course.course_name', 'college.college_name')
        ->join('course', 'course.id', '=', 'student_college.courseid')
        ->where('users.id', $id)
        ->join('college', 'college.id', '=', 'student_college.collegeid')
        ->join('users', 'users.email', '=', 'student_college.uid')->first();

        $studentid = DB::table('student_college')->where('uid', $data->email)->first();

        return view::make('edit_student_clg', compact('data', 'course', 'college', 'departments', 'studentid'));
    }

    //Update Student College
    public function update_student_college(Request $request)
    {
        $courseid = $request->course;
        $department = $request->department;
        $collegeid = $request->collegeid;
        $stream = $request->stream;
        $year = $request->year;
        $id = $request->id;
        $userid = $request->userid;
        $insert = DB::table('student_college')->where('id', $id)->update(['courseid' => $courseid, 'collegeid' => $collegeid, 'stream' => $stream, 'year' => $year]);

        $insert = DB::table('users')->where('id', $userid)->update(['name' => $request->name, 'mobile' => $request->mobile]);

        return redirect('student_college')->with('Student Information Successfully.');
    }

    //Delete Student College
    public function delete_student_college(Request $request)
    {
        $users = DB::table('users')->where('id', $request->id)->first();

        $delete = DB::table('student_college')->where('uid', $users->email)->delete();
        if ($delete) {
            return redirect('student_college')->with('success', 'data delete Successfully');
        } else {
            return redirect('student_college')->with('error', 'Something went wrong');
        }
    }

    //Change Status of Student College
    public function change_status($id,$action,$type)
    {

       
        $data=OurUsers::where('email',$id)->first();

      
        // $user = DB::table('student_college')->where('uid', $id)->select('status')->pluck('status')->first();
        if ($action == 'true') {
            $update = DB::table('student_college')->where('uid', $id)->update(['status' => 'false']);
        } else {
           
            $playerid=array();
            $playerid[]=$data->gcmid;
            $msg='You account has been activated by your college';
            $this->sendMessage($msg,$playerid);
            $update = DB::table('student_college')->where('uid', $id)->update(['status' => 'true']);
        }
        // $users_update = DB::table('users')->where('email', $id)->select('type')->pluck('type')->first();
    
       
        if ($type == 0 || $type == 1) {

            $data->type== 2;
          
            $update1 = DB::table('users')->where('email', $id)->update(['type' => 2]);
        } else {
            $data->type == 0;
           
            $update1 = DB::table('users')->where('email', $id)->update(['type' => 0]);
        }
//$data->save();

        // $data = DB::table('student_college')->select('student_college.*', 'users.*', 'course.course_name', 'college.college_name')->join('course', 'course.id', '=', 'student_college.courseid')->join('college', 'college.id', '=', 'student_college.collegeid')->join('users', 'users.email', '=', 'student_college.uid')->get();
      
        return Redirect::back();
    }

    //Insert College in table
    public function insert_college(Request $request)
    {
        $course_id = $request->course;
        $department = $request->department;
        $college_name = $request->college;
        $reg_no = $request->reg_no;
        $stream = $request->stream;
        $year = $request->year;
        DB::table('college')->insert(['course_id' => $course_id, 'college_name' => $college_name, 'reg_no' => $reg_no, 'stream' => json_encode($department), 'year' => $year]);
        $departments = DB::table('department')->get();
        $course = DB::table('course')->get();
        $college = DB::table('college')->get();
        return view::make('view_college',compact('college','course'));
    }
    //Update Position
    public function update_banner_pos(Request $r)
    {
        $position=array();
        $position=$r->position;
        $array =  explode(',', $position);
        
        foreach ($array as $a) {
            $result_array[] = (int) $a;
        }
     $pos=json_encode($result_array);
      
        
      
       
        DB::table('banner_position')
            ->where('id', 1)
            ->update(['banner_position' => $pos]);
        $data=DB::table('banner_position')->get();
        
        foreach($data as $d)
        {
            $data=$d->banner_position;
        }
        
        return response($data);
        
    }


   //Insert Banner  
    public function insert_banner(Request $r)
    {
        $banner = new Banner();

        $file = $r->file('banner');
        if ($file!='' && $file!='null') {
            $rand = rand(1, 1000000);
            $destinationPath = public_path().'/img/banner_image';
            $filename = $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $filename = $rand.$filename;
            $upload = $file->move($destinationPath, $filename);
        }
        else 
        {
            $filename = '';
        }


        if($r->refcode != ''){
            $banner->refcode=json_encode($r->refcode);
        }
        

        $banner->image = $filename;
        $banner->group_id = Auth::User()->id;
        $b = $banner->save();

        if ($b) 
        {
            return redirect('view_banner')->with('Banner Added Successfully.');
        } else {
            return redirect()->back()->with('error', 'Something went wrong Please try later.');
        }
    }

    //View Banner Data In Table

    public function view_banner()
    {        
        $users = DB::table('banner')->where('group_id', Auth::User()->id)->get();       
        return view::make('view_banner',compact('users'));       
    }

    public function edit_banner($id)
    {
        $refcode = DB::table('admins')->orderBy('ref_code','ASC')->get();

        $banner= DB::table('banner')->where('id',$id)->first();        
        return view::make('edit_banner',compact('banner','refcode'));       
    }

   //<--------------------------- Update banner ------------------------>

    public function update_banner(Request $r)
    {
        $banner = Banner::find($r->id);
       
        $file = $r->file('banner');
        if ($file!='' && $file!='null') {
            $rand = rand(1, 1000000);
            $destinationPath = public_path().'/img/banner_image';
            $filename = $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $filename = $rand.$filename;
            $upload = $file->move($destinationPath, $filename);
        } 
        else 
        {
            $filename = $r->old_banner;
        }

        if($r->refcode != ''){
            $banner->refcode=json_encode($r->refcode);
        }else{
            $banner->refcode=NULL;
        }
        
        $banner->image = $filename;
        $banner->group_id = Auth::User()->id;
        $b = $banner->save();
        
        if ($b) 
        {
            return redirect('view_banner')->with('Banner Updated Successfully.');
        } else {
            return redirect()->back()->with('error', 'Something went wrong Please try later.');
        }
    }

    //<---------------Delete Banner------------->
    public function delete_banner(Request $r)
    {
      $banner_id=$r->del_id;
      
      $delete_banner=DB::table('banner')->where('id','=',$banner_id)->delete();


        if ($delete_banner) {
            return redirect('view_banner')->with('Banner Deleted Successfully.');
        } else {
            return redirect()->back()->with('error', 'Something went wrong Please try later.');
        }
    }

   

    public function add_categories()
    {
        return view::make('add_categories');
    }

    public function store_groups(Request $request)
    {
       
        try {
            // $find=Discussion::where('onesignal_segment',$request->onesignal_segment)->count();
            //          if($find>0)
            //          {
            //              return redirect()->back()->with('error','This onesignal segment is already exits.Please try another.');
            //          }
            //          else
            //          {

            $file = $request->file('logo');
            if ($file) {
                $rand = rand(1, 1000000);
                $destinationPath = public_path().'/mechanicalinsider/groupimage';

                $filename = $file->getClientOriginalName();
                $filename = str_replace(' ', '_', $filename);
                $filename = $rand.$filename;
                $upload = $file->move($destinationPath, $filename);
            } else {
                $filename = '';
            }
            $payment = $request->payment;
            if ($payment == 'paid') {
                $payment = $request->payment1;
            }
            // dd($payment);
            $cats = implode(',', $request->categories);
            $discussion = new Discussion();
            $discussion->image = $filename;
            $discussion->category = $cats;
            $discussion->topic = $request->topic;
            $discussion->status = $request->status;
            $discussion->city = $request->city;
            $discussion->payment = $payment;
            $discussion->lat = $request->lat;
            $discussion->lng = $request->lng;
            $discussion->address = $request->address;

            $done = $discussion->save();
            if ($done) {
                $new_dis = Discussion::find($discussion->id);
                $new_dis->onesignal_segment = $discussion->id;
                $new_dis->save();

                return redirect('groups')->with('success', 'Group Added Successfully.');
            } else {
                return redirect()->back()->with('error', 'Something went wrong Please try later.');
            }
            //          }
      //       $discussion= new Discussion();
      //       $discussion->image=$filename;

   //          $discussion->topic=$request->topic;
      //       $discussion->onesignal_segment=$request->onesignal_segment;
   //          $discussion->status=$request->status;
            // $discussion->city=$request->city;

      //       $done=$discussion->save();
      //       if($done)
      //       {
   //  			return redirect('groups')->with('success','Group Added Successfully.');
      //       }
      //       else
      //       {
      //       	return redirect()->back()->with('error','Something went wrong Please try later.');
      //       }
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong Please try later.');
        }
    }

    public function store_categories(Request $request)
    {
        try {
            $file = $request->file('logo');
            if ($file) {
                $rand = rand(1, 1000000);
                $destinationPath = public_path().'/mechanicalinsider/category_images';
                // dd($destinationPath);
                // Get the orginal filname or create the filename of your choice
                $filename = $file->getClientOriginalName();
                $filename = str_replace(' ', '_', $filename);
                $filename = $rand.$filename;
                $upload = $file->move($destinationPath, $filename);
            } else {
                $filename = '';
            }

            $insert = new Category();

            $insert->name = $request->name;
            $insert->image = $filename;
            $insert->status = $request->status;

            $done = $insert->save();
            if ($done) {
                return redirect('categories')->with('success', 'category Added Successfully.');
            } else {
                return redirect()->back()->with('error', 'Something went wrong Please try later.');
            }
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong Please try later.');
        }
    }

    public function edit_groups($id)
    {
        $discussion = Discussion::where('id', $id)->first();
        $categories = Category::all();

        return view::make('edit_group', compact('discussion', 'categories'));
    }

    public function edit_categories($id)
    {
        $data = Category::where('id', $id)->first();

        return view::make('edit_categories', compact('data'));
    }

    public function delete_groups(Request $request)
    {
        $delete = Discussion::destroy($request->del_id);
        if ($delete) {
            return redirect()->back()->with('success', 'Group Deleted Successfully.');
        } else {
            return redirect()->back()->with('error', 'Something went wrong Please try later.');
        }
    }

    public function delete_categories(Request $request)
    {
        $delete = Category::destroy($request->del_id);
        if ($delete) {
            return redirect()->back()->with('success', 'Category Deleted Successfully.');
        } else {
            return redirect()->back()->with('error', 'Something went wrong Please try later.');
        }
    }

    public function update_group(Request $request)
    {
        try {
            // $find=Discussion::where('onesignal_segment',$request->onesignal_segment)->where('id','!=',$request->id)->count();
            //       if($find>0)
            //       {
            //           return redirect()->back()->with('error','This onesignal segment is already exits.Please try another.');
            //       }
            //       else
            //       {

            $file = $request->file('logo');
            if ($file) {
                $rand = rand(1, 1000000);
                $destinationPath = public_path().'/mechanicalinsider/groupimage';
                // dd($destinationPath);
                // Get the orginal filname or create the filename of your choice
                $filename = $file->getClientOriginalName();
                $filename = str_replace(' ', '_', $filename);
                $filename = $rand.$filename;
                $upload = $file->move($destinationPath, $filename);
            }
            $payment = $request->payment;
            if ($payment == 'paid') {
                $payment = $request->payment1;
            }
            // dd($payment);

            $cats = implode(',', $request->categories);
            if ($file) {
                $update = Discussion::where('id', $request->id)->update(['topic' => $request->topic, 'status' => $request->status, 'image' => $filename, 'city' => $request->city, 'payment' => $payment, 'category' => $cats]);
            } else {
                $update = Discussion::where('id', $request->id)->update(['topic' => $request->topic, 'status' => $request->status, 'city' => $request->city, 'payment' => $payment, 'category' => $cats]);
            }

            if ($update) {
                return redirect('groups')->with('success', 'Group Updated Successfully.');
            } else {
                return redirect()->back()->with('error', 'Something went wrong Please try later.');
            }
            // }
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong Please try later.');
        }
    }

    public function update_categories(Request $request)
    {
        try {
            $file = $request->file('logo');
            if ($file) {
                $rand = rand(1, 1000000);
                $destinationPath = public_path().'/mechanicalinsider/category_images';
                // dd($destinationPath);
                // Get the orginal filname or create the filename of your choice
                $filename = $file->getClientOriginalName();
                $filename = str_replace(' ', '_', $filename);
                $filename = $rand.$filename;
                $upload = $file->move($destinationPath, $filename);
            } else {
                $filename = $request->old_logo;
            }

            $insert = Category::find($request->id);

            $insert->image = $filename;
            $insert->name = $request->name;
            $insert->status = $request->status;

            $done = $insert->save();
            if ($done) {
                return redirect('categories')->with('success', 'category Added Successfully.');
            } else {
                return redirect()->back()->with('error', 'Something went wrong Please try later.');
            }
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Something went wrong Please try later.');
        }
    }

    public function show(Request $request)
    {
        $g_id = $request->groupid;
        $record = DB::table('common_table')->where('groupid', $request->groupid)->whereIn('field_type', [0, 7, 8])->orderBy('timestamp', 'desc')->get();
        foreach ($record as $key => $rec) {
            $comments = DB::table('comment_on_post')->where('posttype', $rec->field_type)->where('postid', $rec->id)->get();
            foreach ($comments as $comment) {
                $reply = DB::table('comment_replies')->where('commentid', $comment->id)->get();
                foreach ($reply as $key => $rep) {
                    $comment->reply[] = $rep;
                }
                $rec->comments[] = $comment;
            }
        }
        // dd($record);
        return view::make('group_comments', compact('record', 'g_id'));
    }

    public function delete(Request $request)
    {
        $id = $request->del_id;
        $group_id = DB::table('common_table')->where('id', $id)->select('groupid')->first();
        $g_id = $group_id->groupid;
        $delete = DB::table('common_table')->where('id', $id)->delete();
        if ($delete) {
            $record = DB::table('common_table')->where('groupid', $request->groupid)->whereIn('field_type', [0, 7, 8])->orderBy('timestamp', 'desc')->get();
            foreach ($record as $key => $rec) {
                $comments = DB::table('comment_on_post')->where('posttype', $rec->field_type)->where('postid', $rec->id)->get();
                foreach ($comments as $comment) {
                    $reply = DB::table('comment_replies')->where('commentid', $comment->id)->get();
                    foreach ($reply as $key => $rep) {
                        $comment->reply[] = $rep;
                    }
                    $rec->comments[] = $comment;
                }
            }

            return view::make('group_comments', compact('record', 'g_id'))->with('success', 'Comment Deleted Successfully');
        }
    }

    public function search(Request $request)
    {
        $field_type = 1;
        $prefix = $request['val'];
        $g_id = $request['g_id'];

        $record = DB::select("select * from common_table where field_type IN (0,7,8) AND (groupid='$g_id')  AND  (post_description LIKE '%$prefix%' OR posttype LIKE '%$prefix%' OR jsondata LIKE '%$prefix%') ORDER BY `id` DESC");
        $new_record = $record;

        //    $new_record=[];
        // foreach ($record as $key=>$rec) {
        //   $user_groups=Auth::user()->groups;
        //   $user_groups_is_array= strrchr($user_groups,",");

        //   $post_groups=$rec->groupid;
        //   $post_groups_is_array=strrchr($post_groups,",");

        //   if($user_groups_is_array)
        //   {
        //     $user_group=explode(',',$user_groups);

        //     if($post_groups_is_array)
        //     {
        //       $post_group=explode(',',$post_groups);
        //         if(in_array($post_group[0],$user_group))
        //         {
        //           $new_record[]=$rec;
        //         }
        //     }
        //     else
        //     {
        //       if(in_array($post_groups,$user_group))
        //       {
        //         $new_record[]=$rec;
        //       }
        //       elseif($post_groups=="all"||$post_groups=="All")
        //       {
        //         $new_record[]=$rec;
        //       }
        //     }
        //   }
        //   else
        //   {
        //     if($post_groups_is_array)
        //     {
        //       $post_group=explode(',',$post_groups);
        //         if(in_array($user_groups,$post_group))
        //         {
        //           $new_record[]=$rec;
        //         }
        //     }
        //     else
        //     {
        //       if($post_groups==$user_groups)
        //       {
        //         $new_record[]=$rec;
        //       }
        //       elseif($post_groups=="all"||$post_groups=="All")
        //       {
        //         $new_record[]=$rec;
        //       }
        //     }
        //   }
        // }
        // if(Auth::user()->groups=="all")
        // {
        // 	$new_record=$record;
        // }
        if (!empty($new_record)) {
            foreach ($new_record as $key => $new_rec) {
                // $notification=DB::table('sent_notifications')->where('postid',$new_rec->id)->first();
                //       if($notification)
                //       {
                //           $new_rec->notification="sent";
                //       }
                //       else
                //       {
                //           $new_rec->notification="";
                //       }

                $comments = DB::table('comment_on_post')->where('posttype', $field_type)->where('postid', $new_rec->id)->get();
                foreach ($comments as $comment) {
                    $reply = DB::table('comment_replies')->where('commentid', $comment->id)->get();
                    foreach ($reply as $key => $rep) {
                        $comment->reply[] = $rep;
                    }
                    $new_rec->comments[] = $comment;
                }
            }
        }
        $record = $new_record;

        return view::make('group_comments_search', compact('record'));
    }

    public function view_follow_group($email)
    {
       // $data = DB::table('group_follow_rec')->select('group_follow_rec.*', 'discussions.topic', 'categories.name', 'college.college_name')->join('course', 'course.id', '=', 'student_college.courseid')->join('group_follow_rec', 'group_follow_rec.group_name', '=', 'discussions.id')->join('users', 'users.email', '=', 'student_college.uid')->where('college.id', $college)->get();
        $user_detail = DB::table('group_follow_rec')->orderBy('id','desc')->where('profile_uid',$email)->get();
        
        $group_id=array();
        foreach($user_detail as $user)
        { 
            $group_id[]=$user->group_name;
            $email=$user->profile_uid;
            
        }
        
        $group_id=array_unique($group_id);
        $group_name=DB::table('discussions')->orderBy('id','desc')->whereIn('id',$group_id)->get();
       
        return view('view_follow_group',['email' => $email, 'group_name' => $group_name]);
    }

    
    function sendMessage($msg,$playerid){
        
		
		$content = array(
		    "en"=>$msg,
			);
		
		
		//$jsondata=json_decode($data,true);
				//echo $playerid;						
		
		$fields = array(
			'app_id' => "306439b4-709b-468e-b687-d0f9e6917268",
			'include_player_ids' => $playerid,
			// 'data' => $data,
			// 'large_icon'=>$data['image'],
			'contents' => $content
		);
		
		$fields = json_encode($fields);
		
    	//print("\nJSON sent:\n");
    	print($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic MDQyY2E5MTEtMzdhZS00N2EwLWI4N2UtZTM3NGFlMjcxYWUw'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
    }
    


    //Create meeting room
    public function add_room()
    {
        $departments = DB::table('department')->get();
        $course = DB::table('course')->get();
        return view::make('add_room', compact('departments', 'course'));
    }

    public function insert_room(Request $r)
    {
        $meeting_id=preg_replace('/\s+/', '_', $r->name.'-'.rand(1111,9999));
        $sha1=sha1('createname='.$r->name.'&meetingID='.$meeting_id.'&attendeePW=123456&moderatorPW='.$r->password.'zp28SYNqcyfacl5ZSja6nPUZlFJKpLK01vX22mujE');
        $response=$this->apicall($r->name,$meeting_id,$r->password,$sha1);
       
        $xml = simplexml_load_string($response);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        
        if (array_key_exists("returncode",$array)) 
        {
            if ($array['returncode']=='SUCCESS')
            {
                $dept = DB::table('meeting_rooms')
                ->insert(['room_name' => $meeting_id,'school_name' =>$r->school,'teacher_name' =>$r->teacher_name,'class'=>$r->class_name ,'subject' =>$r->subject,'time' => '','password' =>$r->password ,'checksum'=> $sha1,'attendeePW'=>$array['attendeePW'], 'moderatorPW'=> $array['moderatorPW']]);
               
                return redirect()->route('view_room')->with('success','Your Meeting has been created successfully. Its valid for next 8 Hours.');
            }
            else
            {
                return back()->with('error', $array['message']);
            }
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }

    public function view_room()
    {
        $data=DB::table('meeting_rooms')->orderBy('id', 'desc')->get();
        if(count($data) > 0)
        {
            foreach($data as $dd)
            {
                $sha1=sha1('joinfullName='.preg_replace('/\s+/', '_', $dd->teacher_name).'&meetingID='.$dd->room_name.'&password='.$dd->moderatorPW.'zp28SYNqcyfacl5ZSja6nPUZlFJKpLK01vX22mujE');
                $url='https://school.dhurina.in/bigbluebutton/api/join?fullName='.preg_replace('/\s+/', '_',$dd->teacher_name).'&meetingID='.$dd->room_name.'&password='.$dd->moderatorPW.'&checksum='.$sha1;
               
                $dd->link=$url;
            }
        }

        return view::make('view_room', compact('data'));       
    }



}