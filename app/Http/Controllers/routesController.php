<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;

class routesController extends Controller
{
    //
    
    public function permission()
    {
  //   	$recover_password = new Permission();
		
		// $recover_password->name         = 'recover_password';
		// $recover_password->display_name = 'Recover Password'; // optional
		// // Allow a user to...
		// $recover_password->description  = 'To recover the password'; // optional
		// $recover_password->save();
		
		//----------------------------------------------------------\\
		// $dashboard = new Permission();
		// $dashboard->name         = 'dashboard';
		// $dashboard->display_name = 'Dashboard'; // optional
		// // Allow a user to...
		// $dashboard->description  = 'Admin Dashboard'; // optional
		// $dashboard->save();

    	$workshop = new Permission();
		$workshop->name         = 'workshop';
		$workshop->display_name = 'View Workshop'; // optional
		// Allow a user to...
		$workshop->description  = 'View Workshop '; // optional
		$workshop->save();

		//----------------------------------------------------------\\

		$workshop_reg_users = new Permission();
		$workshop_reg_users->name         = 'workshop_reg_users';
		$workshop_reg_users->display_name = 'View Workshop registered users'; // optional
		// Allow a user to...
		$workshop_reg_users->description  = 'View Workshop registered users '; // optional
		$workshop_reg_users->save();


		
		//----------------------------------------------------------\\
		$add_workshop = new Permission();
		$add_workshop->name         = 'add_workshop';
		$add_workshop->display_name = 'Add Workshop'; // optional
		// Allow a user to...
		$add_workshop->description  = 'Add New Workshop '; // optional
		$add_workshop->save();
		
		//----------------------------------------------------------\\
		$edit_workshop = new Permission();
		$edit_workshop->name         = 'edit_workshop';
		$edit_workshop->display_name = 'Edit Workshop'; // optional
		// Allow a user to...
		$edit_workshop->description  = 'Edit Workshop Content'; // optional
		$edit_workshop->save();
		
		//----------------------------------------------------------\\
		$delete_workshop = new Permission();
		$delete_workshop->name         = 'delete_workshop';
		$delete_workshop->display_name = 'Delete Workshop'; // optional
		// Allow a user to...
		$delete_workshop->description  = 'Delete Workshop'; // optional
		$delete_workshop->save();
		
		//----------------------------------------------------------\\
		$send_workshop_notification = new Permission();
		$send_workshop_notification->name         = 'send_workshop_notification';
		$send_workshop_notification->display_name = 'send_workshop_notification'; // optional
		// Allow a user to...
		$send_workshop_notification->description  = 'send workshop notifications'; // optional
		$send_workshop_notification->save();

		//----------------------------------------------------------\\
		
		
		$currentAffair_store = new Permission();
		$currentAffair_store->name         = 'currentAffair_store';
		$currentAffair_store->display_name = 'Add Current affair'; // optional
		// Allow a user to...
		$currentAffair_store->description  = 'Add Current affair'; // optional
		$currentAffair_store->save();

		//----------------------------------------------------------\\
		$currentAffair = new Permission();
		$currentAffair->name         = 'currentAffair';
		$currentAffair->display_name = 'Current Affair'; // optional
		// Allow a user to...
		$currentAffair->description  = 'View Current Affairs'; // optional
		$currentAffair->save();
		
		//----------------------------------------------------------\\
		$editCurrentAffair= new Permission();
		$editCurrentAffair->name         = 'editCurrentAffair';
		$editCurrentAffair->display_name = 'Edit CurrentAffair'; // optional
		// Allow a user to...
		$editCurrentAffair->description  = 'Edit Current Affair'; // optional
		$editCurrentAffair->save();
		
		//----------------------------------------------------------\\
		$delete_CurrentAffair = new Permission();
		$delete_CurrentAffair->name         = 'delete_CurrentAffair';
		$delete_CurrentAffair->display_name = 'Delete CurrentAffair'; // optional
		// Allow a user to...
		$delete_CurrentAffair->description  = 'Delete CurrentAffair'; // optional
		$delete_CurrentAffair->save();
		
		//----------------------------------------------------------\\

		$send_CurrentAffair_notification = new Permission();
		$send_CurrentAffair_notification->name         = 'send_CurrentAffair_notification';
		$send_CurrentAffair_notification->display_name = 'send_CurrentAffair_notification'; // optional
		// Allow a user to...
		$send_CurrentAffair_notification->description  = 'send CurrentAffair notifications'; // optional
		$send_CurrentAffair_notification->save();

		//----------------------------------------------------------\\
		
		
		//----------------------------------------------------------\\
		$jobs = new Permission();
		$jobs->name         = 'jobs';
		$jobs->display_name = 'Jobs View'; // optional
		// Allow a user to...
		$jobs->description  = 'View Jobs'; // optional
		$jobs->save();
		
		//----------------------------------------------------------\\
		$add_job = new Permission();
		$add_job->name         = 'add_job';
		$add_job->display_name = 'add Jobs'; // optional
		// Allow a user to...
		$add_job->description  = 'Add Jobs'; // optional
		$add_job->save();



		//----------------------------------------------------------\\
		$edit_job = new Permission();
		$edit_job->name         = 'edit_job';
		$edit_job->display_name = 'edit Jobs'; // optional
		// Allow a user to...
		$edit_job->description  = 'Edit Jobs'; // optional
		$edit_job->save();
		
		//----------------------------------------------------------\\
		$delete_job = new Permission();
		$delete_job->name         = 'delete_job';
		$delete_job->display_name = 'delete Jobs'; // optional
		// Allow a user to...
		$delete_job->description  = 'Delete Jobs'; // optional
		$delete_job->save();

		//----------------------------------------------------------\\
		$delete_job_comment = new Permission();
		$delete_job_comment->name         = 'delete_job_comment';
		$delete_job_comment->display_name = 'delete Jobs comment'; // optional
		// Allow a user to...
		$delete_job_comment->description  = 'Delete Jobs comment'; // optional
		$delete_job_comment->save();

		//----------------------------------------------------------\\
		$send_job_notification = new Permission();
		$send_job_notification->name         = 'send_job_notification';
		$send_job_notification->display_name = 'send_job_notification'; // optional
		// Allow a user to...
		$send_job_notification->description  = 'send job notifications'; // optional
		$send_job_notification->save();

		//----------------------------------------------------------\\
		//----------------------------------------------------------\\

		$groups = new Permission();
		$groups->name         = 'groups';
		$groups->display_name = 'View groups '; // optional
		// Allow a user to...
		$groups->description  = 'View groups'; // optional
		$groups->save();
		
		//----------------------------------------------------------\\
		$add_groups = new Permission();
		$add_groups->name         = 'add_groups';
		$add_groups->display_name = 'add groups'; // optional
		// Allow a user to...
		$add_groups->description  = 'Add groups'; // optional
		$add_groups->save();




		//----------------------------------------------------------\\
		$edit_groups = new Permission();
		$edit_groups->name         = 'edit_groups';
		$edit_groups->display_name = 'edit groups'; // optional
		// Allow a user to...
		$edit_groups->description  = 'Edit groups'; // optional
		$edit_groups->save();
		
		//----------------------------------------------------------\\
		$delete_groups = new Permission();
		$delete_groups->name         = 'delete_groups';
		$delete_groups->display_name = 'delete groups'; // optional
		// Allow a user to...
		$delete_groups->description  = 'Delete groups'; // optional
		$delete_groups->save();
		

		//----------------------------------------------------------\\
		
			//----------------------------------------------------------\\
		$pdf = new Permission();
		$pdf->name         = 'view_pdf';
		$pdf->display_name = 'View pdf '; // optional
		// Allow a user to...
		$pdf->description  = 'View pdf'; // optional
		$pdf->save();
		
		//----------------------------------------------------------\\
		$add_pdf = new Permission();
		$add_pdf->name         = 'add_pdf';
		$add_pdf->display_name = 'add pdf'; // optional
		// Allow a user to...
		$add_pdf->description  = 'Add pdf'; // optional
		$add_pdf->save();



		//----------------------------------------------------------\\
		$edit_pdf = new Permission();
		$edit_pdf->name         = 'edit_pdf';
		$edit_pdf->display_name = 'edit pdf'; // optional
		// Allow a user to...
		$edit_pdf->description  = 'Edit pdf'; // optional
		$edit_pdf->save();
		
		//----------------------------------------------------------\\
		$delete_pdf = new Permission();
		$delete_pdf->name         = 'delete_pdf';
		$delete_pdf->display_name = 'delete pdf'; // optional
		// Allow a user to...
		$delete_pdf->description  = 'Delete pdf'; // optional
		$delete_pdf->save();

		//----------------------------------------------------------\\
		$send_pdf_notification = new Permission();
		$send_pdf_notification->name         = 'send_pdf_notification';
		$send_pdf_notification->display_name = 'send_pdf_notification'; // optional
		// Allow a user to...
		$send_pdf_notification->description  = 'send pdf notifications'; // optional
		$send_pdf_notification->save();

		//----------------------------------------------------------\\
		
		//----------------------------------------------------------\\
		$add_admin = new Permission();
		$add_admin->name         = 'add_admin';
		$add_admin->display_name = 'Add Admin'; // optional
		// Allow a user to...
		$add_admin->description  = 'Add Admin'; // optional
		$add_admin->save();
		
		//----------------------------------------------------------\\
		$view_admin = new Permission();
		$view_admin->name         = 'view_admin';
		$view_admin->display_name = 'View Admin'; // optional
		// Allow a user to...
		$view_admin->description  = 'View Admin'; // optional
		$view_admin->save();

		//----------------------------------------------------------\\
		$edit_admin = new Permission();
		$edit_admin->name         = 'edit_admin';
		$edit_admin->display_name = 'Edit Admin'; // optional
		// Allow a user to...
		$edit_admin->description  = 'Edit Admin'; // optional
		$edit_admin->save();
		

		//----------------------------------------------------------\\
		$delete_admin = new Permission();
		$delete_admin->name         = 'delete_admin';
		$delete_admin->display_name = 'Delete Admin'; // optional
		// Allow a user to...
		$delete_admin->description  = 'Delete Admin'; // optional
		$delete_admin->save();
		
		//----------------------------------------------------------\\

		
		
		
		
		
		$post = new Permission();
		$post->name         = 'view_post';
		$post->display_name = 'View post '; // optional
		// Allow a user to...
		$post->description  = 'View post'; // optional
		$post->save();
		
		//----------------------------------------------------------\\
		$add_post = new Permission();
		$add_post->name         = 'add_post';
		$add_post->display_name = 'add post'; // optional
		// Allow a user to...
		$add_post->description  = 'Add post'; // optional
		$add_post->save();



		//----------------------------------------------------------\\
		$edit_post = new Permission();
		$edit_post->name         = 'edit_post';
		$edit_post->display_name = 'edit post'; // optional
		// Allow a user to...
		$edit_post->description  = 'Edit post'; // optional
		$edit_post->save();
		
		//----------------------------------------------------------\\
		$delete_post = new Permission();
		$delete_post->name         = 'delete_post';
		$delete_post->display_name = 'delete post'; // optional
		// Allow a user to...
		$delete_post->description  = 'Delete post'; // optional
		$delete_post->save();
		

		//----------------------------------------------------------\\

		$delete_post_comment = new Permission();
		$delete_post_comment->name         = 'delete_post_comment';
		$delete_post_comment->display_name = 'delete post comment'; // optional
		// Allow a user to...
		$delete_post_comment->description  = 'Delete post comment'; // optional
		$delete_post_comment->save();
		//----------------------------------------------------------\\

		$send_post_notification = new Permission();
		$send_post_notification->name         = 'send_post_notification';
		$send_post_notification->display_name = 'send_post_notification'; // optional
		// Allow a user to...
		$send_post_notification->description  = 'send post notifications'; // optional
		$send_post_notification->save();

		//----------------------------------------------------------\\
		

		//----------------------------------------------------------\\

		
		
		
		$test = new Permission();
		$test->name         = 'view_test';
		$test->display_name = 'View test '; // optional
		// Allow a user to...
		$test->description  = 'View test'; // optional
		$test->save();
		
		//----------------------------------------------------------\\
		$add_test = new Permission();
		$add_test->name         = 'add_test';
		$add_test->display_name = 'add test'; // optional
		// Allow a user to...
		$add_test->description  = 'Add test'; // optional
		$add_test->save();


		//----------------------------------------------------------\\
		$edit_test = new Permission();
		$edit_test->name         = 'edit_test';
		$edit_test->display_name = 'edit test'; // optional
		// Allow a user to...
		$edit_test->description  = 'Edit test'; // optional
		$edit_test->save();
		
		//----------------------------------------------------------\\
		$delete_test = new Permission();
		$delete_test->name         = 'delete_test';
		$delete_test->display_name = 'delete test'; // optional
		// Allow a user to...
		$delete_test->description  = 'Delete test'; // optional
		$delete_test->save();
		
		//----------------------------------------------------------\\
		$publish_test = new Permission();
		$publish_test->name         = 'publish_test';
		$publish_test->display_name = 'Publish test'; // optional
		// Allow a user to...
		$publish_test->description  = 'Publish test'; // optional
		$publish_test->save();
		

		//----------------------------------------------------------\\
		$unpublish_test = new Permission();
		$unpublish_test->name         = 'unpublish_test';
		$unpublish_test->display_name = 'unpublish test'; // optional
		// Allow a user to...
		$unpublish_test->description  = 'unpublish test'; // optional
		$unpublish_test->save();

		//----------------------------------------------------------\\
		$send_test_notification = new Permission();
		$send_test_notification->name         = 'send_test_notification';
		$send_test_notification->display_name = 'send_test_notification'; // optional
		// Allow a user to...
		$send_test_notification->description  = 'send test notifications'; // optional
		$send_test_notification->save();

		//----------------------------------------------------------\\
		

		//----------------------------------------------------------\\

		$question = new Permission();
		$question->name         = 'view_question';
		$question->display_name = 'View question '; // optional
		// Allow a user to...
		$question->description  = 'View question'; // optional
		$question->save();
		
		//----------------------------------------------------------\\
		$add_question = new Permission();
		$add_question->name         = 'add_question';
		$add_question->display_name = 'add question'; // optional
		// Allow a user to...
		$add_question->description  = 'Add question'; // optional
		$add_question->save();



		//----------------------------------------------------------\\
		$edit_question = new Permission();
		$edit_question->name         = 'edit_question';
		$edit_question->display_name = 'edit question'; // optional
		// Allow a user to...
		$edit_question->description  = 'Edit question'; // optional
		$edit_question->save();
		
		//----------------------------------------------------------\\
		$delete_question = new Permission();
		$delete_question->name         = 'delete_question';
		$delete_question->display_name = 'delete question'; // optional
		// Allow a user to...
		$delete_question->description  = 'Delete question'; // optional
		$delete_question->save();
		
		//----------------------------------------------------------\\
		
		

		//----------------------------------------------------------\\

		$group_comments = new Permission();
		$group_comments->name         = 'view_group_comments';
		$group_comments->display_name = 'View group_comments '; // optional
		// Allow a user to...
		$group_comments->description  = 'View group comments'; // optional
		$group_comments->save();

		
		//----------------------------------------------------------\\
		$delete_group_comments = new Permission();
		$delete_group_comments->name         = 'delete_group_comments';
		$delete_group_comments->display_name = 'delete group_comments'; // optional
		// Allow a user to...
		$delete_group_comments->description  = 'Delete Group comments'; // optional
		$delete_group_comments->save();
		
		//----------------------------------------------------------\\

		//----------------------------------------------------------\\
		$send_custom_notification = new Permission();
		$send_custom_notification->name         = 'send_custom_notification';
		$send_custom_notification->display_name = 'send custom notification'; // optional
		// Allow a user to...
		$send_custom_notification->description  = 'send custom notification'; // optional
		$send_custom_notification->save();
		
		//----------------------------------------------------------\\

		//----------------------------------------------------------\\
		$delete_comments = new Permission();
		$delete_comments->name         = 'delete_comment';
		$delete_comments->display_name = 'delete every type post comments'; // optional
		// Allow a user to...
		$delete_comments->description  = 'delete comments from all types of posts'; // optional
		$delete_comments->save();
		
		//----------------------------------------------------------\\

		
		//----------------------------------------------------------\\
		
		
		
		
		
		
		
	}
    
    
}
