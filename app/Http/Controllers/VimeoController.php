<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Vimeo\Laravel\Facades\Vimeo;
use DB;
use Hash;
use Redirect;
use Auth;
use View;
use App\Traits\Common;
use Illuminate\Support\Facades\Storage;
use \Cache;
class VimeoController extends Controller
{
    use Common;
    //get video from vimeo
    public function vimeo_video()
    {
        $title="Vimeo Video";
        $url="https://api.vimeo.com/me/videos?direction=desc&per_page=5&sort=date&fields=uri,name,description,pictures,stats,files";
        $response=$this->vimeo_curl($url);
        $response=$response['data'];
        return view::make('vimeo_video', compact('title','response')); 
    }

    public function assign_video(Request $r)
    {
        $title="Assign Video";
        $id=decrypt($r->id);   
        
        $url="https://api.vimeo.com/me/videos/".$id;
        $response=$this->vimeo_curl($url);

        $embed=$response['embed']['html'];

        $check=$response['pictures']['sizes'];
        $get=end($check);
        $img=$get['link_with_play_button'];       
        
        if(Auth::user()->role == 'admin')
        {
          $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
        }
        else
        {
          $new_Courses=DB::table('new_courses')->where('school_id',Auth::user()->id)->orderBy('name','ASC')->get();
        }
        $subject=DB::table('subject')->where('school_id',Auth::user()->id)->get();
        return view::make('assign_video', compact('title','id','new_Courses','subject','embed','img'));
    }

    public function insert_assign_video(Request $r)
    {
        
        if($r->video_id != '')
        { 
            $insert = DB::table('video')->insert(['school_id' => Auth::user()->id,'course_id'=>json_encode($r->course_id),
            'subject_id'=>json_encode($r->subject_id),'name'=>$r->name,'video'=> $r->video_id,'thumbnail'=>$r->thumb,
            'type'=>'vimeo','timestamp'=>date("YmdHis")]);
            if($insert)
            {
                return redirect()->route('vimeo_video')->with('success','Video has been uploaded successfully.');
            }
            else
            {
                return back()->with('error','Something went wrong, please try again!');
            }

        }else
        {
            return back()->with('error','Oops, Video could not be empty.');
        }

    }    

    //Upload video in panel 
    public function insert_vimeo_video(Request $r)
    {
       
            $apiKey = "n7aTnI7S";
            $apiV2Secret = "hhqUTTa88ULpd6R8MkZbg2InVEcxeWF6WTBVWEZUVGpoa2MwUTVjVmhhTVdKeFFYVm4n";
            
            if(!$r->file('video')) 
            {
                return back()->with('error','Oops, Video could not be empty.');
            }

            // try
            // {
            $res=$this->create_video($r->name);
          
           
               
               if ($res['err']) {
                    return back()->with('error','Something went wrong, please try again!');
               } else {
                   $response=$res['response'];
                   
                   // Step2. For getting upload_link
                   $curl1 = curl_init();
                   curl_setopt_array($curl1, [
                       CURLOPT_URL => "https://api.jwplayer.com/v2/uploads/$response->upload_id/parts?page=1&page_length=1",
                       CURLOPT_RETURNTRANSFER => true,
                       CURLOPT_ENCODING => "",
                       CURLOPT_MAXREDIRS => 10,
                       CURLOPT_TIMEOUT => 30,
                       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                       CURLOPT_CUSTOMREQUEST => "GET",
                       CURLOPT_HTTPHEADER => [
                       "Accept: application/json",
                       "Authorization: Bearer ".$response->upload_token,
                       "Content-Type: application/json"
                       ],
                   ]);
        
                   $response1 = curl_exec($curl1);
                   $err1 = curl_error($curl1);
                   curl_close($curl1);
        
                   if ($err1) {
                        return back()->with('error','Something went wrong, please try again!');
                   } 
                   else {
                       $response1=json_decode($response1);
                       
                       $url = $response1->parts[0]->upload_link;
                       // Step4. For uploading parts
                       $curl2 = curl_init($url);
                       curl_setopt($curl2, CURLOPT_URL, $url);
                       curl_setopt($curl2, CURLOPT_CUSTOMREQUEST, 'PUT');
                       curl_setopt($curl2, CURLOPT_BINARYTRANSFER, TRUE);
                       curl_setopt($curl2, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                       curl_setopt($curl2, CURLOPT_HTTPHEADER, array('Content-Type:'));
        
                       $file = $r->file('video');                  
                       $name =$r->video->getClientOriginalName();  
                       $success = $r->video->move(storage_path('app'), $name);
                       
                       $location=storage_path('app/').$name;
                       $handle = fopen(storage_path('app/').$name, "r");
                      
                       $data = fread($handle,filesize($location));
        
                       curl_setopt($curl2, CURLOPT_POSTFIELDS, $data);
        
                       $resp = curl_exec($curl2);
                       $err2 = curl_error($curl2);
                       
                       curl_close($curl2);
                    if ($err2) {
                        return back()->with('error','Something went wrong, please try again!');
                    } 
                       else {
        
                        //    Step 5. to complete the upload
                           $curl3 = curl_init();
                           curl_setopt_array($curl3, [
                             CURLOPT_URL => "https://api.jwplayer.com/v2/uploads/".$response->upload_id."/complete",
                             CURLOPT_RETURNTRANSFER => true,
                             CURLOPT_ENCODING => "",
                             CURLOPT_MAXREDIRS => 10,
                             CURLOPT_TIMEOUT => 30,
                             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                             CURLOPT_CUSTOMREQUEST => "PUT",
                             CURLOPT_HTTPHEADER => [
                               "Authorization: Bearer ".$response->upload_token,
                               "Content-Type: application/json"
                             ],
                           ]);
                           
                           $response3 = curl_exec($curl3);
                           $err3 = curl_error($curl3);
                           
                           curl_close($curl3);
                           
                           if ($err3) {
                            return response()->json(['status'=>'error']);
                        } else {
                               // Step 6. Get video details
                               $response6=$this->get_video($response->id);
                               
                               if (!isset(json_decode($response6)->id)) {
                                    return back()->with('error','Something went wrong, please try again!');
                                } 
                                else {
        
                                  $video_response=json_decode($response6);  
                                  
                                  $video_id=$video_response->id;
                                  $name =$r->video->getClientOriginalName();  
                                  $location=storage_path('app/').$name;
                                  $getID3 = new \getID3;
                                  $fil = $getID3->analyze($location);
                                  $second=(int)$fil['playtime_seconds'];
                                  
                                  $timing=date('H:i:s', mktime(0,0,$second));
                                  
                                  $p_code=$this->get_thumbnails($video_id);
                                  $thumbnail="https://cdn.jwplayer.com/v2/media/$video_id/poster.jpg";                     
                                  //get video id from the response
                                  if($this->check_count($video_response)>0){
                                    if($r->topic_id !='')
                                    {
                                        $insert = DB::table('video')->insertGetId(['school_id' => 1,'course_id'=>json_encode($r->course_id),
                                        'subject_id'=>json_encode($r->subject_id),'topic'=>json_encode($r->topic_id),'name'=>$r->name,'video'=> $video_id,'publish'=>'false',
                                        'position'=>(int)$r->position,'access'=>$r->access,'type'=>'jwplayer','thumbnail'=>$thumbnail,'timestamp'=>date("Y-m-d H:i:s"),'duration'=>$timing]);
                                    }
                                    else 
                                    {
                                        $insert = DB::table('video')->insertGetId(['school_id' => 1,'course_id'=>json_encode($r->course_id),
                                        'subject_id'=>json_encode($r->subject_id),'name'=>$r->name,'video'=> $video_id,'thumbnail'=>$thumbnail,'publish'=>'false','position'=>(int)$r->position,'access'=>$r->access,'type'=>'jwplayer','timestamp'=>date("Y-m-d H:i:s"),'duration'=>$timing]);
                                    }
                    
                    
                                    if($insert)
                                    {
                                        $name =$r->video->getClientOriginalName();
                                        $t=unlink(storage_path('app/'.$name));
                                        //Send push notification
                                        $content='Video-'.$r->name;
                                        $data=array("type"=>3,"name"=>'video',"id"=>$insert,'vimeo_id'=>$video_id,'title'=>$r->name);
                    
                                        $push= $this->push_notification($content,$data,'tags',null);
                                        // return redirect()->route('view_video')->with('success','Video has been uploaded successfully.');
                                        return response()->json(['status'=>'success','message'=>'Video has been uploaded successfully']);

                                    }
                                    else
                                    {
                                        // return back()->with('error','Something went wrong, please try again!');
                                        return response()->json(['status'=>'error','message'=>'Something went wrong, please try again!']);

                                    }  

                                  }
                                  else{
                                    // return back()->with('error','Something went wrong, please try again!');
                                    return response()->json(['status'=>'error','message'=>'Something went wrong, please try again!']);
                                }
                               }
                           }
                       }
                   }
               }

               

               
            
                     
            
        // } 
        // catch (\Exception $e) 
        // {
        //     $message=$e->getMessage();
        //     return back()->with('error',$message);
        // }   


    }




}
