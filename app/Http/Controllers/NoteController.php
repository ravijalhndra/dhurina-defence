<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\note;
use Auth;
use View;
use DB;
use Hash;
use Redirect;

class NoteController extends Controller
{
    public function add_note(Request $r){

        if(Auth::user()->role == 'school')
        {
          return back()->with('error','Sorry,You do not access to access this page.');
        }


        $title='Add Note || Ebook';

        $sector=DB::table('sectors')->orderBy('name','ASC')->get();
        $last_position=note::select('position')->orderBy('id','DESC')->first();
        if($last_position)
        {
            $last_position=$last_position->position +1;
        }
        else
        {
            $last_position=1;
        }

        return view::make('add_note', compact('title','sector','last_position'));
    }


    public function insert_note(Request $r)
    {
        $check=note::where('name',$r->name)->first();
        if(!$check){

            $insert=new note;
            $insert->sector_id=$r->sector;
            $insert->school_id=Auth::user()->id;
            $insert->name=$r->name;
            if($r->discount)
            {
                $insert->discount=$r->discount;

            }
            else
            {
            $insert->discount=0;

            }

            $thumb = $r->file('image');
            if($thumb!='')
            {
                $imageName = date("YmdHis").'.'.$r->image->getClientOriginalExtension();
                $success = $r->image->move(public_path('img/notes'), $imageName);            
                $insert->image=$imageName;                            
            }

            $file = $r->file('file');
            if($file!='')
            {
                $fileName = "file-".date("YmdHis").'.'.$r->file->getClientOriginalExtension();
                $success2 = $r->file->move(public_path('img/notes'), $fileName);            
                $insert->file=$fileName;                            
            }

            $insert->type=$r->type;
            $insert->position=$r->position;

            if($r->price_type=="1"){
             $insert->price=$r->price;
            }
            $insert->status=$r->price_type;
            
            $save=$insert->save();
            if($save){
                return redirect()->route('view_note')->with('success','Note has been added successfully.');
            }else{
                return back()->with('error','Something went wrong, please try again!');
            }
        }
        else 
        {
            return back()->with('error','Note and E-book name already exist,Please use diffrent one.');
        }

    }

    public function view_note(Request $r){

        $filter = $r->filter;
        $sector = $r->sector;

        $title='View notes';
        $data=DB::table('notes')->join('sectors','notes.sector_id','=','sectors.id')
        ->select('notes.*','sectors.name as sector_name')->OrderBy('position','ASC');
        
        if($filter != ''){
            $data= $data->where('notes.name','like','%'.$filter.'%');
        }

        if($sector != ''){
            $data=$data->where('notes.type',$sector);
        }
        
        $data=$data->paginate(50);
        return view::make('view_note', compact('title','data','filter','sector'));   
    }

    
    public function edit_note(Request $r){
        $title='View notes';
        $id=decrypt($r->id);
        $data=note::find($id);
        $sector=DB::table('sectors')->orderBy('name','ASC')->get();
        return view::make('edit_note', compact('title','data','sector'));
    }

    public function update_note(Request $r){
    
        $check=note::whereNotIn('id',[$r->id])->where('name',$r->name)->first();
        if(!$check){

            $insert=note::find($r->id);
            $insert->sector_id=$r->sector;
            $insert->name=$r->name;

            $thumb = $r->file('image');
            if($thumb!='')
            {
                $imageName = date("YmdHis").'.'.$r->image->getClientOriginalExtension();
                $success = $r->image->move(public_path('img/notes'), $imageName);            
                $insert->image=$imageName;                            
            }

            $file = $r->file('file');
            if($file!='')
            {
                $fileName = "file-".date("YmdHis").'.'.$r->file->getClientOriginalExtension();
                $success2 = $r->file->move(public_path('img/notes'), $fileName);            
                $insert->file=$fileName;                            
            }

            if($r->price_type=="1"){
             $insert->price=$r->price;
             $insert->discount=$r->discount;

            }
            else{
            $insert->price=0;  
            }
            $insert->status=$r->price_type;

            $insert->type=$r->type;
            $insert->position=$r->position;
            $save=$insert->save();
            if($save){
                return redirect()->route('view_note')->with('success','Note has been added successfully.');
            }else{
                return back()->with('error','Something went wrong, please try again!');
            }
        }
        else 
        {
            return back()->with('error','Note and E-book name already exist,Please use diffrent one.');
        }
    }

    public function delete_note(Request $r){
    
        $check=note::find($r->del_id);
        if($check){
            $check->delete();
            return redirect()->route('view_note')->with('success','Note has been deleted successfully.');
        }
        else{
            return back()->with('error','Sorry, Record not found.');

        }
    }

    public function published_note(Request $r){
    
        $check=note::find($r->del);
        if($check){

            $check->publish=$r->type;
            $save=$check->save();
            if($save){
                return redirect()->route('view_note')->with('success','Note has been changes successfully.');
            }
            else{
                return back()->with('error','Something went wrong, please try again!');
            }
        }
        else{
            return back()->with('error','Sorry, Record not found.');

        }
    }


}
