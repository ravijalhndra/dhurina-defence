<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Hash;
use DB;
use View;
use URL;
use Redirect;
use App\SentNotification;
use Auth;

class TestController extends Controller
{
    public function view_test(Request $r)
    {
        try {
            $title='View Test';
            $filter = $r->filter;
            $course_ids = $r->course;
            $subject_id = $r->subject;

            $data=DB::table('common_table')->where('field_type',2)->where('premium',0)->orderBy('id','desc');
            
            if($r->course != ''){
                $data=$data->where('groupid','like','%'.$r->course.'%');
            }

            if($r->subject != ''){
                $data=$data->orWhere(function ($query) use ($r) {
                    $query->whereIn('school', [$r->subject]);
                });  
            }
        
            if($filter != ''){
            $data=$data->whereJsonContains('jsondata->testname',$filter);
            }
            
            $data=$data->paginate(50);

       
       
            if($this->check_count($data) > 0)
            {
                foreach ($data as $key => $value) 
                {

                    //Course Name
                    $value->course_name=''; 
                    if($value->groupid != 'all' && $value->groupid != '')
                    {
                        $course_id=explode(",",$value->groupid); 
                        $course=DB::table('new_courses')->select('name')->whereIn('id',$course_id )->get();
                        if($this->check_count($course) > 0)
                        {
                            $cou_name=[];
                            foreach($course as $cou)
                            {
                                $cou_name[]=$cou->name;
                            }
                            
                            $value->course_name=implode(",",$cou_name); 
                        }
                    }
                    else if($value->groupid == 'all')
                    {
                        $value->course_name='all'; 
                    }                
                    // End Course name
                    
                    //Subject name
                    $value->subject_name='';
                        if($value->school != '')
                        {                     
                            $subject=DB::table('subject')->select('name')->whereIn('id',json_decode($value->school) )->get();
                            if($this->check_count($subject) > 0)
                            {
                                $sub_name=[];
                                foreach($subject as $s)
                                {
                                    $sub_name[]=$s->name;
                                }
                                $value->subject_name=implode(",",$sub_name); 
                            }  
                        }                    
                    //End Subject name

                    //Info json
                        $json=json_decode($value->jsondata);
                        $value->writer=$json->writer;
                        $value->test_name=$json->testname;
                        $value->time_name=$json->time;
                        $value->question=$json->question;
                        $value->payment=$json->payment; 
                    //End info     
    
                }
            }


       

            if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin' )
            {
                $new_Courses=DB::table('new_courses')->whereNull('school_id')->orWhere('school_id',1)->orderBy('name','ASC')->get();
                $subject=DB::table('subject')->where('school_id',1)->get();
            }
            else
            {
                $course_id=json_decode(Auth::user()->course_id,true);
                $new_Courses=DB::table('new_courses')->whereIn('id',$course_id)->orderBy('name','ASC')->get();

                $sub=array(); 
                foreach ($new_Courses as $values) 
                {
                    $sub=array_unique(array_merge(json_decode($values->subject),$sub));
                }

                $subject=DB::table('subject')->whereIn('id',$sub)->get();
            } 

            return view::make('view_test',compact('data','new_Courses','subject','filter','course_ids','subject_id'));  
        } catch (\Exception $e) {

            return $e->getMessage();
        }          

    }


    public function test_comment(Request $r)
    {
        $id=$r->id;
        $comment=DB::table('comment_on_post')
        ->join('users', 'comment_on_post.user_id', '=', 'users.id')
        ->select('comment_on_post.*','users.name as username','users.mobile as mobile')
        ->where('comment_on_post.postid',$id)->OrderBy('comment_on_post.id','DESC')->paginate(50);
        
        return view::make('test_comment',compact('comment','id')); 
    }

    public function test_reply(Request $r)
    {
        $common_table=DB::table('common_table')->select('field_type')->where('id',$r->test_id)->first();
        if($common_table)
        {

            $post_data = array(
                'comments' => $r->reply,
                'name' => 'Admin',
                'collage' => 'null',
                'userimage' => 'null',
                'com_image' => "null"
            );

            $json=addslashes(json_encode($post_data,JSON_HEX_QUOT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));

            $insert=DB::table('comment_on_post')->insert(
                ['posttype' =>$common_table->field_type, 'postid' =>$r->test_id,'uid'=>Auth::user()->id,'user_id'=>Auth::user()->id,
                'jsondata'=>json_encode($post_data),'d_com'=>1]);

            if($insert){
                return back()->with('success','Reply has been added successfully.');
            }  else{
                return back()->with('error','Something went wrong, please try again!');
            } 

        }
    }

    public function delete_reply_test(Request $r){
        $del = DB::table('comment_on_post')->where('id',$r->del_id)->delete();
        if($del)
        {
            return back()->with('success','Reply has been delete successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }
    }


    //-------------------Reply comment
    public function add_reply_post(Request $r)
    {
        $id= decrypt( $r->id);
        $type=$r->type;
       
        if($type == 'video'){
            $comment=DB::table('video_comment')->where('id',$id)->first();
            $reply=DB::table('comment_reply_post')->where('postid',$comment->video_id)->where('comment_id',$comment->id)->where('type','video')->paginate(50);     
        }else{
            $comment=DB::table('comment_on_post')->where('id',$id)->first();
            $reply=DB::table('comment_reply_post')->where('postid',$comment->postid)->where('comment_id',$comment->id)->where('type','post')->paginate(50);      
        }
            
        return view::make('add_reply_post',compact('comment','reply','type')); 

    }

    public function add_reply(Request $r)
    {
        date_default_timezone_set('Asia/Kolkata');
        $time=date('Y-m-d H:i:s');

        if($r->type == 'video'){
            $comment=DB::table('video_comment')->where('id',$r->id)->first();
            $insert=DB::table('comment_reply_post')->insertGetId(['posttype'=>5,'postid'=>$comment->video_id,'comment_id'=>$r->id,'reply'=>$r->reply,
            'type'=>'video','created_at'=>$time,'updated_at'=>$time]);
            $redirect_id=encrypt($comment->video_id);

            return redirect::route('get_comment',['id'=>$redirect_id])->with('success','Reply has been added Successfully.');  

        }else{

            $comment=DB::table('comment_on_post')->where('id',$r->id)->first();   
            $insert=DB::table('comment_reply_post')->insertGetId(['posttype'=>$comment->posttype,'postid'=>$comment->postid,'comment_id'=>$r->id,'reply'=>$r->reply,
            'type'=>'post','created_at'=>$time,'updated_at'=>$time]);

            $redirect_id=encrypt ($comment->postid);

            return redirect::route('test_comment',['id'=>$comment->postid ])->with('success','Reply has been added Successfully.');  
        }    

        

    }

    public function delete_reply_comment(Request $r)
    {   

        $del = DB::table('comment_reply_post')->where('id',$r->del_id)->delete();
        if($del)
        {
            return back()->with('success','Reply has been delete successfully.');
        }
        else
        {
            return back()->with('error','Something went wrong, please try again!');
        }

    }



    public function test_time(Request $r)
    {
        // echo $date = date('Y-m-d H:i:s');
        $course_id=[278];

        $get_testSeries=DB::table('test_series')->where(function ($q) use($course_id) {
            foreach($course_id as $key => $ord)
            {
                if($key == 0)
                {
                    $q->whereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
                else 
                {
                    $q->orWhereRaw('json_contains(course_id, \'["'.$ord.'"]\')');    
                }
            }
        })->get();

        dd($get_testSeries);

    }



}
