<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DB;


class Notification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $content,$player_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($content,$player_id)
    {
        $this->content=$content;
        $this->player_id=$player_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $player[]=$this->player_id;
        $message = array("en" =>$this->content);
        
        $fields = array( 
						'app_id' => '43729d91-558f-4c77-b1b7-c355d663afdd', 
						'priority'=>'high',
                        'include_player_ids' =>$player,
						'contents' => $message,
						'data' => array("type"=>9)
					  ); 

		$fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                    'Authorization: Basic NmEwYjkxNjYtNGJlYy00MGQ5LTg2NGItYTAwOTNiNThjMTZm'));
    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
        
    }
}
