<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use DB;

class PremiumTestResult implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $testid;
    

    public function __construct($testid)
    {
        $this->testid=$testid;
    }

    public function handle()
    {
        $testid=$this->testid;
        $test=DB::table('common_table')->where('id',$this->testid)->first();
        if($test)
        {
            $json_data=json_decode($test->jsondata,true);

            if($json_data['result_declare']=='no')
            {
                $results=DB::table('result_record')->where('testid',$testid)->orderBy('mark','DESC')->orderBy('id','ASC')->get();
                
                $i=0;

                if (count($results) > 0)
                {
                    foreach($results as $result)
                    {
                    
                        $i++;
                        $id=$result->id;
                        if($i==1)
                        {
                            $points=$json_data['first_prize'];
                        }
                        elseif($i==2)
                        {
                            $points=$json_data['second_prize'];
                        }
                        elseif($i==3)
                        {
                            $points=$json_data['third_prize'];
                        }
                        else
                        {
                            $points=0;
                        }

                        $update_result=DB::table('result_record')->where('id',$id)->update(['points'=>$points,'rank'=>$i]);
                                       
                    }   
                }
                $json_data['result_declare']='yes';
                $json_data=json_encode($json_data);
                $update_test=DB::table('common_table')->where('id',$testid)->update(['jsondata'=>$json_data]);
            }
        }
    }
}
