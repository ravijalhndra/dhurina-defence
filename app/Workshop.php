<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{
    //
    
     protected $table='common_table';
	 protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $fillable=['id', 'timestamp', 'field_type', 'groupid', 'likes', 'comment', 'view', 'posttype', 'uid', 'posturl', 'post_description', 'jsondata'];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
