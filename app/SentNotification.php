<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SentNotification extends Model
{
    protected $table="sent_notifications";
    protected $fillable=['postid'];
}
