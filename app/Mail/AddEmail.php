<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $add;
    public $password;
    public $email;
    public function __construct($add,$password,$email)
    {
        $this->add=$add;
        $this->password=$password;
        $this->email=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email_views.add_email')->with(['password'=>$this->password,$add=$this->add,$email=$this->email]);
    }
}
