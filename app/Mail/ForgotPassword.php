<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     * 
     */

    public $email;
    public $id;

    public function __construct($email,$id)
    {
        $this->email=$email;
        $this->id=$id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('developers.delainetech@gmail.com','Edudream')->subject('Reset Password')->view('mail.forgot')->with(['email'=>$this->email,'id'=>$this->id]);

    }
}
