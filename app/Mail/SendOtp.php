<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOtp extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $otp;
    public $name;
    public $type;

    public function __construct($otp,$name,$type)
    {
        $this->otp=$otp;
        $this->name=$name;
        $this->type=$type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('developers.delainetech@gmail.com','Dhurina School App')->subject('Account verification')->view('mail.verify_account')->with(['otp'=>$this->otp,'name'=>$this->name,'type'=>$this->type]);
    }
}
