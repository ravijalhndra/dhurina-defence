<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SchoolLogin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $password;
    public $email;
    public function __construct($name,$password,$email)
    {
        $this->name=$name;
        $this->password=$password;
        $this->email=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('developers.delainetech@gmail.com','EduDream School App')->subject('School credentials')->view('mail.school_login')->with(['name'=>$this->name,'password'=>$this->password,'email'=>$this->email]);
    }
}
