<?php
namespace App\Console;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use DB;
use Auth;
use File;
use Config;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Cron
        // $schedule->call(function () {
        //     $this->get_thumb();           
        // })->everyTenMinutes();

        //CashFree Payment webhook
        $schedule->call(function () {
            $this->cashfree_webhook();  
                    
        })->everyFiveMinutes();

        //Course name update
        // $schedule->call(function () {
        //     $this->courseName();           
        // })->twiceDaily(9,17);

        //nginx and mysql restart
        // $schedule->call(function () {
        //     $this->restart();           
        // })->daily();

    }

    //Get thumnail and trancode array in vimeo rest API
    protected function get_thumb()
    {
        $get_video=DB::table('video')->whereNotNull('vimeo')->whereNull('thumbnail')->OrWhere('thumbnail','')
        ->OrWhere('quality','[]')->OrWhere('quality','')->limit(20)->get();
        if($get_video) 
        {
            foreach ($get_video as $video) 
            {
                $id=$video->id;
                $video_id=$video->vimeo;
           
                //run Cron for vimeo rest api
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://api.vimeo.com/me/videos/".$video_id."?fields=status,pictures,stats,files");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: bearer 35e210dba308031ea5b022a1d7a756d5',
                    'Content-Type: json',
                    'Accept: application/vnd.vimeo.*+json;version=3.4'
                ));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $server_output = curl_exec($ch);
                curl_close ($ch);
                $response = json_decode($server_output, true);

                if (array_key_exists('status',$response)) 
                {
                    $thumbnail='';
                    if (array_key_exists('3',$response['pictures']['sizes'])) 
                    {
                        $thumbnail=$response['pictures']['sizes'][3]['link'];
                    }
                    else {
                        $thumbnail=$response['pictures']['sizes'][2]['link'];
                    }

                    $quality=json_encode($response['files']);

                    $update = DB::table('video')->where('id',$id)->update(['thumbnail'=>$thumbnail,'quality'=>$quality]);

                }

            }
        }
    }

    //Webhook Curl payment
    protected function cashfree_webhook()
    {
        //All pending Payments
       $payment=DB::table('order')->where('status','pending')->OrderBy('id','DESC')->get();
        
       foreach ($payment as $key => $value) {
           
           $orderId=$value->order_id;

           $curl = curl_init();
           curl_setopt_array($curl, array(
           CURLOPT_URL => "https://api.cashfree.com/api/v1/order/info/status",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS => "appId=6290878313f4f66475b40c98980926&secretKey=5fde76516ab3e55c5d0a4fc42c75e5c3b5fed4c8&orderId=".$orderId,
           CURLOPT_HTTPHEADER => array(
                   "cache-control: no-cache",
                   "content-type: application/x-www-form-urlencoded"
               ),
           ));

           $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

           if ($err) {}

           else 
           {
               $transaction=json_decode($response);
               if (array_key_exists('orderStatus',$transaction) && $transaction->status=='OK')
               {
                   if (array_key_exists('txStatus',$transaction))
                   {
                       if ($transaction->txStatus=='SUCCESS') 
                       {
                           date_default_timezone_set('Asia/Kolkata');
                           $date = date('YmdHis');

                           $status=$transaction->txStatus;

                           if($value->status != $status)
                           {
                               //update in DB Order table
                                $insert=DB::table('order')->where('id',$value->id)->update(['status'=>$status]);

                                //Add in course access
                                $assign=DB::table('course_access')->where('user_id',$value->user_id)->where('course_id',$value->course_id)->first();
                                if(!$assign)
                                {
                                    $course = DB::table('course_access')->insert(['school_id'=>'1','user_id' => $value->user_id,'course_id'=>$value->course_id,'course_name'=>$value->course_name,'ref_code'=>'online','created_at'=>$date,'updated_at'=>$date]);                       
                                } 
                           }

                       }
                   }
               }
           }

       }

    }


    protected function courseName()
    {
        //if admin change course name then it will make trouble in course access status 'true',false because we are matching coursename not course ID.
        $course=DB::table('course_access')->get();
        foreach ($course as $key => $value) 
        {
            $user=DB::table('new_courses')->where('id',$value->course_id)->first();
            if($user){
                $add_mobile=DB::table('course_access')->where('id',$value->id)->update(['course_name' =>$user->name]);
            }
        }
    }


    
    protected function restart()
    {

    }


    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
