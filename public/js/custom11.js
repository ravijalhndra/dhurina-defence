$(function() {
    $('.user-state').on('change', function() {
        var state = $('.user-state').val();

        var url = $('.urlroutecity').val();
        // console.log(url);
        $.ajax({
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "post",
            url: url,
            dataType: "JSON",
            data: { state: state, '_token': '{!!csrf_token()!!}' },
            // cache: false,
            success: function(data) {
                var options = "";
                for (var i = 0; i < data.length; i++) {
                    options += '<option value="?city=' + data[i].city_name + '">' + data[i].city_name + '</option>';
                }
                $('.user-city').html(options);
                // console.log(data);
            },
            error: function() {}
        });
        // alert(state);
    });


    // $('.user-city').on('change', function() {
    //     var city = $('.user-city').val();
    //     var state = $('.user-state').val();
    //     var url = $('urlrouteusers').val();
    //     // var url = "{{url('view_users/:slug')}}" ;
    //     // url = url.replace(':slug', city);
    //     $.ajax({
    //         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    //         type: "post",
    //         url: url,
    //         dataType: "JSON",
    //         data: { state: state, city: city, 'token': '{!!csrf_token()!!}' },
    //         success: function(data) {
    //             $("#ajaxresponse").html('');
    //             $("#ajaxresponse").html(data);
    //         },
    //         error: function() {}

    //     });
    // });
});