$(function() {
    $('.course-selected').on('change', function() {
        var course = $('.course-selected').val();
        var url = $('.urlcourse').val();
        $.ajax({
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "post",
            url: url,
            dataType: "JSON",
            data: { course: course, '_token': '{!!csrf_token()!!}' },
            // cache: false,
            success: function(response) {
                $('#collagedropdown').empty();
                $('#collagedropdown').append($('<option>', {
                    value: '',
                    text: 'Select Collage Name'
                }));
                $.each(response.data, function(index, val) {
                    $('#collagedropdown').append($('<option>', {
                        value: val['id'],
                        text: val['college_name']
                    }));

                    //console.log(val);

                });



            }
        });
        // alert(state);
    });

    $('.college-selected').on('change', function() {
        var college = $('.college-selected').val();
        var base_url = window.location.origin;
        var url = base_url + "/get_stream";
        // "{{route('get_stream')}}";
        $.ajax({
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "post",
            url: url,
            dataType: "JSON",
            data: { college: college, '_token': '{!!csrf_token()!!}' },
            // cache: false,
            success: function(data) {
                var stream = JSON.parse(data[0].stream);
                // console.log(stream);
                var year = data[0].year;
                var options = "";
                for (var i = 0; i < stream.length; i++) {
                    options += '<option value="' + stream[i] + '">' + stream[i] + '</option>';
                }
                var year_selected = "";
                for (var j = 1; j <= year; j++) {
                    year_selected += '<option value="' + j + '">' + j + '</option>';
                }

                $('.stream-selected').html(options);
                $('.year-selected').html(year_selected);


                // for (var i = 0; i < data.length; i++) {
                //     for(var)
                //     options += '<option value="'+ data[i] + '">' + data[i] + '</option>';
                // }
                // $('.stream-selected').html(options);
            },
            error: function() {}
        });
        // alert(state);
    });

});