$(document).ready(function () {
    'use strict';
    
    //WOW
    var wow = new WOW({
		mobile: false
	});
	wow.init();
    
    //Steller Parallax
    $(window).stellar({
        responsive: true,
        positionProperty: 'position',
        horizontalScrolling: false
    });
    
    /* ---------------------------------------------
    Smooth scroll
    --------------------------------------------- */
    $('a[href*="#"]:not([href="#"]):not(a[data-toggle="collapse"]):not([data-toggle="tab"])').on('click', function () {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
    
    /*==========================================================
		Navigation
	==========================================================*/
    //Dropdown
    $('.dropdown').on('mouseover', function () {
        $(this).stop(true, true).addClass('open');
    }).on('mouseout', function () {
        $(this).stop(true, true).removeClass('open');
    });
    
    /*==========================================================
		Carousel and Slider
	==========================================================*/
    
    /** Header slider **/
    $("#header-slider").owlCarousel({
        singleItem: true,
        autoPlay: true,
        stopOnHover: true,
        addClassActive: true
    });
    
    /** Screenshot slider **/
    $('#screenshot-carousel').owlCarousel({
        items: 3,
        itemsDesktopSmall: [1199,3],
        itemsTablet: [767,2],
        autoPlay: true,
        stopOnHover: true,
        addClassActive: true
    });
    
    /** Review slider **/
    $('#review-carousel').owlCarousel({
        singleItem: true,
        autoPlay: 10000,
        stopOnHover: true,
        slideSpeed: 500,
        addClassActive: true
    });
    
    /** Sponsor carousel **/
    $('#sponsor-carousel').owlCarousel({
        items: 4,
        itemsTablet: [991,3],
        itemsMobile: [767,1],
        navigation: true,
        navigationText: ['<i class=\"fa fa-long-arrow-left\"></i>', "<i class=\"fa fa-long-arrow-right\"></i>"],
        autoPlay: 10000,
        stopOnHover: true,
        slideSpeed: 500,
        addClassActive: true
    });
    
    function posScheduleNav(){
        var distance = $('#schedule-carousel').offset();
        $('#schedule-carousel .owl-prev').css('left', 0).css('left', '-30px');
        $('#schedule-carousel .owl-next').css('right', 0).css('right', '-30px');
        
        if(distance.left < 50){
            $('#schedule-carousel').closest('.col-md-12').css('padding-left', '60px').css('padding-right', '60px');
        }
    }
    
    if($('#schedule-carousel').length > 0 ) {
        posScheduleNav();
    }
    
    /** Schedule carousel **/
    $('#schedule-carousel').owlCarousel({
        singleItem: true,
        navigation: true,
        navigationText: ['<i class=\"fa fa-angle-left\"></i>', "<i class=\"fa fa-angle-right\"></i>"],
    });
    
    /*==========================================================
		Countdown
	==========================================================*/
    $('.countdown-time').each(function () {
        var endTime = $(this).data('time');
        $(this).countdown(endTime, function (tm) {
            $(this).html(tm.strftime('<span class="section_count"><span class="tcount days">%D </span><span class="text">Days</span></span><span class="section_count"><span class="tcount hours">%H</span><span class="text">Hours</span></span><span class="section_count"><span class="tcount minutes">%M</span><span class="text">Mins</span></span><span class="section_count"><span class="tcount seconds">%S</span><span class="text">Secs</span></span>'));
        });
    });
    
    /*==========================================================
		Magnific Popup
	==========================================================*/
	$('.image-large').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });
    $('.play-video').magnificPopup({
        type: 'iframe'
    });
    $.extend(true, $.magnificPopup.defaults, {
        iframe: {
            patterns: {
                youtube: {
                    index: 'youtube.com/',
                    id: 'v=',
                    src: 'http://www.youtube.com/embed/%id%?autoplay=1'
                }
            }
        }
    });
    
    /*==========================================================
		Ajax Contact Form
	==========================================================*/
	
	// Function for email address validation
	function isValidEmail(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

		return pattern.test(emailAddress);

	}
	$("#contactForm").on('submit', function (e) {
		e.preventDefault();
		var data = {
			name: $("#name").val(),
			email: $("#email").val(),
            subject: $("#subject").val(),
			message: $("#message").val()
		};

		if (isValidEmail(data['email']) && (data['message'].length > 1) && (data['name'].length > 1) && (data['subject'].length > 1)) {
			$.ajax({
				type: "POST",
				url: "sendmail.php",
				data: data,
				success: function () {
					$('#contactForm .input-success').delay(500).fadeIn(1000);
					$('#contactForm .input-error').fadeOut(500);
				}
			});
		} else {
			$('#contactForm .input-error').delay(500).fadeIn(1000);
			$('#contactForm .input-success').fadeOut(500);
		}

		return false;
	});
    
    /*==========================================================
		Window resize
	==========================================================*/
    $(window).on('resize orientationchange', function() {
        if($('#schedule-carousel').length > 0 ) {
            posScheduleNav();
        }
    });
    
    /*==========================================================
		Window Load
	==========================================================*/
    $(window).on('load', function() {
        if($('#schedule-carousel').length > 0 ) {
            posScheduleNav();
        }
    });
    
    /*==========================================================
		Newletter Subscribe	
	==========================================================*/
	$(".subscribe-form").ajaxChimp({
		callback: mailchimpResponse,
		url: "http://codepassenger.us10.list-manage.com/subscribe/post?u=6b2e008d85f125cf2eb2b40e9&id=6083876991" // Replace your mailchimp post url inside double quote "".  
	});

	function mailchimpResponse(resp) {
        if (resp.result === 'success') {
		 
			$('.newsletter-success').html(resp.msg).fadeIn().delay(3000).fadeOut();
			
		} else if (resp.result === 'error') {
			$('.newsletter-error').html(resp.msg).fadeIn().delay(3000).fadeOut();
		}
	}
});