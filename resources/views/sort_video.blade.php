@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				@if(Auth::user()->role == 'admin' )
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_video') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
				@endif

					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Video</h3>
						<div class="row" style="padding-top:50px;">
							<div class="col-md-12">
								<form action="" method="GET">
									<div class="col-md-4">
										<select id="course" name="course" data-placeholder="Select Course..." class="select select-border-color border-warning" >
											<option value="">Select Course...</option>
											@foreach ($new_Courses as $c)
											<option @if($course_ids == $c->id) selected @endif value="{{ $c->id }}">{{ $c->name }} </option>
											@endforeach
										</select> 
									</div>

									<div class="col-md-4">
										<select id="subject" name="subject" data-placeholder="Select Subject..." class="select select-border-color border-warning" >
											<option value="">Select Subject...</option>
											@foreach ($subject as $s)
											<option  @if($subject_id == $s->id) selected @endif value="{{ $s->id }}">{{ $s->name }}</option>
											@endforeach
										</select> 
                                    </div>
                                    

									<div class="col-md-4">
                                        <input type="submit" class="fab-menu-btn btn bg-success" value="search">
									</div>

								</form>						
							</div>
							
						</div>
					</div>
				

					<table class="table">
						<thead>
							<tr>								
                                <th>ID</th>
                                <th>Course</th>
                                <th>Subject</th>
                                <th>Name</th>
                                <th>Video</th>
								<th>Publish Status</th>
								<th>Position</th>
							</tr>
                        </thead>
                        <form id="login_form" action="{{ route('sort_videos') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <button type="submit" class="btn btn-primary margin-left">Change Position <i class="icon-arrow-right14 position-right"></i></button>
                               

                            <tbody>
                            @foreach($data as $u)
                                <tr>
                                    <td><input type="hidden" class="check_delete" name="ids[]" value="{{ $u->id }}" /> {{ $u->id }} </td>								
                                    <td class="table-width">
                                        <div class="limited-text" id="row{{ $u->id }}">
                                            {{ $u->course_name }}
                                        </div>	
                                        @if(strlen($u->course_name) > 40) 	
                                        <a href="javascript:;" id="more{{ $u->id }}" onClick="view_more({{ $u->id }})"> More</a>
                                        <a href="javascript:;" class="hide" id="less{{ $u->id }}"  onClick="view_less({{ $u->id }})"> Less</a>
                                        @endif										
                                    </td>
                                    <td>{{$u->subject_name}}</td>
                                    <td>{{$u->name}}</td>    
                                    <td>
                                        <a href="#" target="_blank"> <img src="{{$u->thumbnail}}" width="80" height="80"  </a>
                                    </td>   
                                    <td>
                                        <span class="fab-menu-btn btn-sm @if($u->publish == 'false') bg-red @endif  bg-success btn-rounded "> @if($u->publish == 'true') Published @else Unpublish  @endif </span>  
                                    </td>       
                                                    
                                    <td><input type="text" class="onlynumber widht25" name="position[]" value="{{ $u->position }}" /> </td>
                                </tr>				
                            @endforeach								
                            </tbody>
                        </form>
					</table>
				</div>
				@if($course_ids != '' || $subject_id != '')
					{{ $data->appends(request()->query())->links() }}
				@else 
					{{ $data->links() }}
				@endif
			</div>
		</div>
@endsection
@section('js')

	<script type="text/javascript">

		


	

	</script>
@endsection

