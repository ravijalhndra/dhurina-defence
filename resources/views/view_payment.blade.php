@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables1.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic1.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('js/custom11.js') }}></script>

    <style>
        .datatable-header { display:none; }
    </style>
@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					{{-- @php dd($users);@endphp --}}
					@php
						$from=($users->currentPage()-1)*($users->perPage())+1;
						$to=$users->currentPage()*$users->perPage();
						$total=$users->total();
					@endphp 
				<!-- Basic datatable -->

				<div class="panel panel-flat">
					<div class="panel-heading">				
						<div class="row" style="padding-top:50px;">
							
							<div class="col-md-12">
								<form action="" method="GET">
								<div class="col-md-2">
									<select id="status" name="status" data-placeholder="Select Status..." class="select select-border-color border-warning" >
										<option value="">Select Status...</option>
										<option value="cancel">Cancel</option>
										<option value="pending">Pending</option>
										<option value="success">Success</option>									
									</select> 
								</div>
								<div class="col-md-3">
									<div class="col-md-8"> 
										<label>							
											<input type="search" class="form-control" name="filter" placeholder="Type to filter...">									
										</label>
									</div>
									<div class="col-md-4"> 
									<input type="submit" class="fab-menu-btn btn bg-success" value="search">
									</div>
								</div>							
								</form>						
							</div>
						</div>
					</div>



					<table class="table ">
						<thead>
							<tr>
								<th>ID</th>
                                <th>OrderID</th>
								<th>Ref Code</th>
                                <th>Name</th>
								<th>Email</th>                                
								<th>Mobile</th>
								<th>Amount</th>
                                <th>Course Name</th>
                                <th>Status</th>
								<th>Credit On</th>	
							</tr>
						</thead>
						<tbody id="ajaxresponse">
							@foreach ( $users as $user)						
								<tr>
									<td>{{ $user->id }}</td>
                                    <td>{{ $user->order_id }}</td>
									<td>{{ $user->referralcode }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>                                    
									<td>{{ $user->mobile }}</td>
									<td>{{ $user->amount }}</td>
                                    <td>{{ $user->course_name }}</td>
									<td> 
                                        @if($user->status == 'SUCCESS')
                                            <span class="uk-badge-success"> {{ ucfirst($user->status) }}</span>                                           
                                        @else 
                                            <span class="uk-badge-warning"> {{ucfirst($user->status) }}</span>
                                        @endif
                                    </td>

									<td>{{ date('d M,Y',strtotime($user->created_at)) }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				
				<div class="col-md-12">
					<div class="col-md-4">
						<span class="float-left">Showing {{ $from }} to {{ $to }} from {{ $total }} users</span> 
						</div>
						<div class="col-md-4">
						<ul class="pager" style="padding-bottom: 10px;margin-top: -10px;">
						  <li><a href="{{ $users->previousPageUrl() }}">Previous</a></li>
						  <li><a href="{{ $users->nextPageUrl() }}">Next</a></li>
						</ul>
						</div>
						<div class="col-md-4">
							<div class="pull-right">
								GO To Page: 
								<select name="go_to" onchange="location = this.value;">

								@for ($i = 50; $i < $users->lastPage(); $i+=50)
									<option value="{{ $users->url($i) }}">{{ $i }}</option>
								@endfor
								</select>
							</div>
						</div>
				</div>
				{{-- <ul class="pager">
  <li><a href="{{ $users->previousPageUrl() }}">Previous</a></li>
  <li><a href="{{ $users->nextPageUrl() }}">Next</a></li>
</ul> --}}
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>
		<script>
		  $(function() {
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'https://datatables.yajrabox.com/eloquent/basic-data'
        });
    });
		</script>
@endsection
