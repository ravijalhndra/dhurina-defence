@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>
@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('update_pdf') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <input type="hidden" value="{{$data->id}}" name="id">
                        <input type="hidden" value="{{$json->link}}" name="link_existing">
                        
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit PDF</h5>									
								</div>

								<div class="panel-body">
									<div class="col-md-10  col-md-offset-1">

										<div class="form-group" id="groups_select">
											<label class="col-lg-2 control-label">Select @if(Auth::user()->role == 'admin' ) Courses @else Class @endif:</label>
											<div class="col-lg-10 multi-select-full">
												<select name="groupid[]" data-placeholder="Select one..." multiple="multiple" class="multiselect-select-all-filtering course_id" required>
													@foreach ($new_Courses as $c)
														<option @if(in_array($c->id,$data->course_id)) selected @endif   value="{{ $c->id }}">{{ $c->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
	
										<div class="form-group" id="groups_select">
											<label class="col-lg-2 control-label">Select Subject:</label>
											<div class="col-lg-10 multi-select-full">
												<select name="sub[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning sub_record " required>
													<option value="">Select one..</option>        
													@foreach ($subject as $s)
														<option @if(in_array($s->id,json_decode($data->school))) selected  @endif value="{{ $s->id }}">{{ $s->name }}</option>
													@endforeach
												</select>
											</div>
										</div>


										<div class="form-group @if(count($topic) == 0 ) hide @endif" id="topic_select"  >
											<label class="col-lg-2 control-label">Select Topic:</label>
											<div class="col-lg-10 multi-select-full">
												<select name="topic_id[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning topic_record ">
													<option value="">Select one..</option>
													@if(count($topic) > 0 )
														@foreach ($topic as $t)
															<option @if(json_decode($data->topic) != '')  @if(in_array($t->id,json_decode($data->topic))) selected  @endif @endif value="{{ $t->id }}">{{ $t->name }}</option>
														@endforeach
													@endif
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Writer:</label>
											<div class="col-lg-10">
												<input type="text" name="writer" class="form-control" value="{{ $json->writer }}" placeholder="Enter name" required > 
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Title:</label>
											<div class="col-lg-10">
												<input type="text" name="title" class="form-control" value="{{ $json->title }}" placeholder="Enter Title">
											</div>
										</div>
										
										<div class="form-group" id="mySelect">
											<label class="col-lg-2 display-block text-semibold">Select Type</label>
											<div class="col-lg-10">
											<label class="radio-inline">
												<input type="radio" name="type" value="link" class="styled" @if($json->type == 'link' ) checked="checked" @endif  >
												Link
											</label>
											<label class="radio-inline">
												<input type="radio" name="type" value="pdf" class="styled" @if($json->type == 'pdf' ) checked="checked" @endif >
												Pdf
											</label>
											</div>
										</div>
										
										<div class="form-group" @if($json->type == 'pdf') style="display:none;" @endif id="link">
											<label class="col-lg-2 control-label">Enter Link :</label>
											<div class="col-lg-10">
												<input id="link" type="text" class="form-control"  @if($json->type == 'link') value="{{ $json->link }}" @endif   placeholder="Enter Link" name="link">
											</div>
										</div>
										
										<div class="form-group" @if($json->type == 'link') style="display:none;" @endif id="pdf">
											<label class="col-lg-2 control-label">Enter Pdf:</label>
											<div class="col-lg-10">
                                                <input id="pdf" type="file" class="form-control" name="pdf">
                                                <span>Please leave blank if you do not want to change.</span>
											</div>
										</div>	
										

										<div class="form-group">
											<label class="col-lg-2 control-label">Access:</label>
											<div class="col-lg-10">
												<label class="radio-inline">
													<input type="radio" class="styled" name="access" value="public"  @if($data->access == 'public') checked="checked" @endif />
													Public
												</label>
													
												<label class="radio-inline">
													<input type="radio" class="styled" name="access" value="private" @if($data->access == 'private') checked="checked" @endif />
													Private
												</label>
											</div>
										</div>


								        <!-- /select All and filtering options -->
										<div class="text-right">
											<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->

		</div>
	
	<script type="text/javascript">

		$(document).ready(function(){
			$("#login_form").validate({

				rules:{
					title:{
						required:true
					},
					type:{
						required:true
					},
					writer:{
						required:true
					},
					link:{
						required:"#pdf:blank"
					},
					groupid:{
						required:true
					}
				}
			});

		});

		jQuery(document).ready(function(){
			jQuery('input:radio[name="type"]').change(function(){
				if(jQuery(this).val() == 'link'){
					jQuery("#link").show();
					jQuery("#pdf").hide();
				}
				else
				{
					jQuery("#link").hide();
					jQuery("#pdf").show();
				}
			});
		});


		$('select[name="groupid[]"]').change(function(){
			var course= $(this).val();

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_subject',   
				data: {course:course},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{						
						$('#subcategory').show();
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				}  
			});			
		});

		$('select[name="sub[]"]').change(function(){

			var course_ids=[];
			var defence=['Combo Course (Airforce-Navy)','Air Force X Group','Air Force Y Group','Indian Navy ( SSR & AA )','Army GD','Army Clerk','Army Technical'];


			$('select[name="groupid[]"] option:selected').each(function() 
			{
				course_ids.push($(this).val());
			});

			var subject= $(this).val();

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_topics',   
				data: {course_ids:course_ids,subject_ids:subject},                                      
				success: function(data)
				{
					$(".topic_record").html('');
					if(data.status == 'success')
					{		
						$('#topic_select').show();				
						$('#subcategory').show();

						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".topic_record").append(markup);
					}
					else
					{
						$('#topic_select').hide();
					}
				}  
			});			
		});

	</script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
@endsection
