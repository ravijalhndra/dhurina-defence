<!DOCTYPE html>
<html>
<head>
	<title>Users PDF</title>
	
</head>
<body>



@php
							$data=json_decode($data->jsondata,true);
							$count=1;
						@endphp
					<table class="table datatable-basic table-bordered">
						<thead>
							<tr>
								<th>S.NO.</th>
								@foreach ($data['field'] as $fields)
									@foreach ($fields as $field)
									<th>{{ $field }}</th>
									@endforeach
								@endforeach
								<th>Status</th>
							</tr>
						</thead>
						<tbody>

						@foreach ($users as $user)
						@php
							$json=json_decode($user->jsondata,true);
						@endphp
							<tr>
								<td>{{ $count }}</td>
								@foreach ($json as $jj)
								@php
									// dd($jj);
								@endphp
								<td>{{ $jj['value'] }}</td>
								@endforeach
								<td><span class="label label-success">Active</span></td>
							</tr>
							@php
							$count++;
							@endphp
						@endforeach
							
							
							
						</tbody>
					</table>

					</body>
</html>