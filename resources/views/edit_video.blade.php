@extends('layouts.main')
@section('js_head')
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

                  
		
	<script>
		$('#login_form').submit(function() {
		$('#gif').css('visibility', 'visible');
	});
	</script>
	<style>
		
		#loading {
			width:100px;
		height: 100px;
		position: fixed;
		top: 30%;
		left: 45%;
		z-index:2;
		background-color: transparent;
		
	}
	</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('update_video') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$data->id}}" />
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Video</h5>
									
								</div>

								<div class="panel-body">
                                    <div class="form-group" id="groups_select">
                                        <label class="col-lg-3 control-label">Select @if(Auth::user()->role == 'admin' ) Courses @else Class @endif:</label>
                                        <div class="col-lg-9 multi-select-full">
                                            <select name="course_id[]" data-placeholder="Select one..." multiple="multiple" class="multiselect-select-all-filtering course_id" required>
                                                @foreach ($new_Courses as $c)
                                                    <option @if(in_array($c->id,json_decode($data->course_id))) selected @endif   value="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

									<div class="form-group" id="groups_select">
										<label class="col-lg-3 control-label">Select Subject:</label>
										<div class="col-lg-9 multi-select-full">
                                        	<select name="subject_id[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning sub_record " required>
                                                <option value="">Select one..</option>        
                                                @foreach ($subject as $s)
													<option @if(in_array($s->id,json_decode($data->subject_id))) selected  @endif value="{{ $s->id }}">{{ $s->name }}</option>
												@endforeach
											</select>
										</div>
									</div>

									
									<div class="form-group @if(count($topic) == 0 ) hide @endif" id="topic_select"  >
										<label class="col-lg-3 control-label">Select Topic:</label>
										<div class="col-lg-9 multi-select-full">
                                        	<select name="topic_id[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning topic_record ">
												<option value="">Select one..</option>
												@if(count($topic) > 0 )
													@foreach ($topic as $t)
														<option @if(json_decode($data->topic) != '')  @if(in_array($t->id,json_decode($data->topic))) selected  @endif @endif value="{{ $t->id }}">{{ $t->name }}</option>
													@endforeach
												@endif
											</select>
										</div>
                                    </div>
                                    
                                    <div class="form-group">
										<label class="col-lg-3 control-label">Name</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Video Name" name="name" value="{{$data->name}}">
										</div>
									</div>

									@php 
                                        $video='';
                                        if(isset ($data->quality))
                                        {
											$url=json_decode($data->quality);
											if(count ($url) > 0)
											{
												if ( array_key_exists('2',$url) ) 
												{
													$video=$url[2]->link;
												}
												else 
												{
													$video=$url[0]->link;
												}
											}                                           
                                        }                                        
									@endphp
									
									<div class="form-group">
										<label class="col-lg-3 control-label">Position</label>
										<div class="col-lg-9">
											<input type="text" class="form-control onlynumber" placeholder="Video position" name="position" value="{{ $data->position }}">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Access:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" name="access" value="public" @if($data->access == 'public') checked="checked" @endif />
												Public
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" name="access" value="private" @if($data->access == 'private') checked="checked" @endif />
												Private
											</label>
										</div>
									</div>

									<!-- <div class="form-group">
										<label class="col-lg-3 control-label">Youtube video</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Youtube Video" name="video" value="{{ $data->video }}" required>
										</div>
									</div> -->

									<div class="form-group">
                                        <label class="col-lg-3 control-label">Video Player : </label>
                                        <div class="col-lg-9">
											<!-- @if($data->video != '')
												<iframe width="420" height="315"
													src="https://cdn.jwplayer.com/manifests/{{ $data->video }}.m3u8">
												</iframe>
											@endif -->
											<div id="myElement"></div>


                                            {{--  <video width="320" height="240" controls>
                                                <source src="{{ $video }}" type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>  --}}
                                        </div>
                                    </div>

								</div>
								<br /><br />
								<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Update Video <i class="icon-arrow-right14 position-right"></i></button>
								</div><br>

							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')
<script src="https://cdn.jwplayer.com/libraries/xjE7rkQy.js"></script>
  <script>jwplayer.key="FlhjcrNBeZ4zZcmsG8M2eCRoLSwXy+ekRnUJLTKIjP669+fO";</script>
                  
	<script type="text/javascript">
		$(document).ready(function() {
			var t="{!! $data->video !!}";
			
			$('.summernote').summernote();
			// jwplayer("myElement").setup({
			// "playlist": [{
			// 		"file": "https://cdn.jwplayer.com/manifests/"+t+".m3u8"
			// 	}],
			// "height": 360,
			// "width": 640,
			// "autostart": "viewable",
			// "advertising": {
			// 	"client": "vast",
			// 	"tag": "http://adserver.com/vastTag.xml"
			// }
			// });
			jwplayer("myElement").setup({ 
				"playlist": [{
					"file": "https://cdn.jwplayer.com/manifests/"+t+".m3u8"
				}]
			});
		});


		$('select[name="course_id[]"]').change(function(){
			var course= $(this).val();
			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_subject',   
				data: {course:course},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{
						console.log(data.data);
						
						$('#subcategory').show();
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				}  
			});			
		});

		$('select[name="subject_id[]"]').change(function(){

			var course_ids=[];
			var defence=['Combo Course (Airforce-Navy)','Air Force X Group','Air Force Y Group','Indian Navy ( SSR & AA )'];


			$('select[name="course_id[]"] option:selected').each(function() 
			{
				course_ids.push($(this).val());
			}); 

			var subject= $(this).val();

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_topics',   
				data: {course_ids:course_ids,subject_ids:subject},                                      
				success: function(data)
				{
					$(".topic_record").html('');
					if(data.status == 'success')
					{		
						$('#topic_select').show();				
						$('#subcategory').show();
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".topic_record").append(markup);
					}
					else
					{
						$('#topic_select').hide();
					}
				}  
			});			
		});
	</script>
	<script type="text/JavaScript">
		
	
</script>
@endsection
