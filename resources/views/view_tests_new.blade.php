@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/notifications/pnotify.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/components_notifications_pnotify.js') }}></script>


@endsection
@section('content')
		<!-- Page content -->
		<div class="page-content">
				@if(Auth::user()->role == 'admin' )
				<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
					<li>
						<a href="{{ url('add_test') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
							<i class="fab-icon-open icon-plus3"></i>
							<i class="fab-icon-close icon-cross2"></i>
						</a>
					</li>
				</ul>
				@endif
			<!-- Main sidebar -->
			@include('includes.left_sidebar')
			<!-- /main sidebar -->
			
		<!-- Main content -->
				<div class="content-wrapper">
				
				    <div id="post_section" class="row">
				
				        <!--get the result from Workshop Conrtroller-->
				        <!--checking the conditiion for result count-->
				        @if(!empty($record))
				        {{-- @php
				        	dd($record);
				        @endphp --}}
				        <!--getting the record  from the array value $record-->
				       @foreach ($record as $data)

								@php
							    $json=json_decode($data->jsondata,true);
							    $timestamp=strtotime($data->timestamp);
							
							@endphp
				        	<div class="col-md-12">

						<!-- Blog layout #4 with image -->
						<div class="panel panel-flat">
						    <div class="panel-footer panel-footer-condensed">
						    <div style="float:left; width: 100px;">
						    	<img style="border-radius: 50%;margin-top: 10px; margin-left: 20px;" src="/mechanicalinsider/admin/images/test_image.png" width="60" height="60">

						    </div>
						    <div style="float: left; width: 400px; margin-top: 15px; margin-left: 10px;">
						    	<span style="font-size: 16px;"><strong>New Online Test</strong></span><br>
						    	<span>{{ $json['writer'] }}</span>
						    </div>
						    <div >
						    <span>{{ date('M d, Y',$timestamp) }}</span><br>
						    <span><strong>Price:</strong></span>
						    	@if ($json['payment']=="free")
						    		<strong>Free</strong>
						    	@else
						    		<strong>&#8377; {{ $json['payment'] }}</strong>
						    	@endif
						    </div>
						    
							</div>
							<div class="row panel-body">
								<div class="panel-body bg-indigo-400 border-radius-bottom border-radius-top text-center col-md-4 col-md-offset-0" style="background-image: url(http://demo.interface.club/limitless/assets/images/bg.png); background-size: contain;">
									<h2>{{ $data->applicant }}</h2>
									<p style="font-size: 20px; text-align: center;">APPLICANT</p>
							
									
								</div>
								<div class="col-md-7 col-md-offset-1">
									<h2 style="margin:0;"><b>{{ ucfirst($json['testname']) }}</b></h2><br>
									<p style="font-size: 12px;"><b><span>{{ $json['question'] }} Questions</span><span style="padding: 1px 1px 1px 1px; background: grey; margin: 0 10px 0 10px;"></span><span>{{ $json['time'] }} Minutes</span></b></p><br>
									<p>
									
									<a href="{{ url('questions/'.$data->id) }}" style="border-color: #5c63b0; color: #5c63b0; padding:8px 10px 8px 10px; font-size: 12px;" class="btn border-slate btn-flat legitRipple text-center">
										<b> <span>View Ques</span> </b>
									</a>

									<a href="{{ url('add_questions/'.$data->id) }}" style="border-color: #5c63b0; color: #5c63b0; padding:8px 10px 8px 10px; font-size: 12px;" class="btn border-slate btn-flat legitRipple text-center">
										<b> <span>Add Ques</span> </b>
									</a>
									
									@if($json['question']==$data->added_questions)
										@if ($data->status=='published')
										
										<a href="{{ url('unpublish_test/'.$data->id) }}" style="border-color: orange; color: orange; padding:8px 10px 8px 10px; font-size: 12px;" class="btn border-slate btn-flat legitRipple text-center"><b>UnPublish</b></a>
									
										@endif
										@if ($data->status=='notpublished'||$data->status=="")
										
										<a href="{{ url('publish_test/'.$data->id) }}" style="border-color: green; color: green; padding:8px 10px 8px 10px; font-size: 12px;" class="btn border-slate btn-flat legitRipple text-center"><b>Publish</b></a>
									
										@endif
									@endif
									</p> 
									<p>
									
									</p>
								</div>
								</div>
							
							<div class="panel-footer panel-footer-transparent">
								<div class="heading-elements">
									<ul class="list-inline list-inline-separate heading-text">
										<li><a class="comment" id="comment{{ $data->id }}" href="" class="text-muted">{{$data->comment}} comments</a></li>
										{{-- <li><a href="#" class="text-default"><i class="icon-eye2 text-pink position-left"></i> {{ $data->view }}</a></li> --}}
										@if($data->status=='published')
										
											{{--  @if($data->notification=="sent")
												<li><a data-toggle="modal" data-target="#notification{{ $data->id }}" class="text-default"><i class="icon-bell-check text-pink position-left"></i></a></li>
											@else
												<li><a href="{{ url('send_test_notification/'.$data->id) }}" class="text-default"><i class="icon-bell2 text-pink position-left"></i></a></li>
											@endif  --}}
										
										@endif

										{{--  <li><a href="#" class="text-default"><i class="icon-heart6 text-pink position-left"></i> {{ $data->likes }}</a></li>  --}}

										{{--  @permission('edit_test')  --}}
										<li><a href="{{ url('edit_test/'.$data->id) }}" class="text-muted"><i class="icon-pencil text-size-base text-pink position-left"></i></a></li> 
										{{--  @endpermission  --}}


										{{--  @permission('delete_test')  --}}
											<li><a data-toggle="modal" data-target="#modal_mini{{ $data->id }}" class="text-muted"><i class="icon-trash text-size-base text-pink position-left"></i></a></li> 
										{{--  @endpermission  --}}

									</ul>
								
				                       <a href="{{ url('test/users_result/'.$data->id) }}" style="border-color: #ff5454; color: #ff5454; padding:6px 30px 6px 30px; font-size: 12px;" class="btn border-slate btn-flat legitRipple text-center pull-right"><b>Users Result</b></a>
				                     
									{{-- notificaion model --}}
										<div  data-backdrop="false" id="notification{{ $data->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header bg-warning">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Warning</h5>
										</div>

										<div class="modal-body">
											<p style="font-size: 14px;">This post is already notifyed.
												If you want again then click Yes.</p>

										</div>

										<div class="modal-footer">
											
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<a href="{{  url('send_test_notification/'.$data->id) }}"><button type="button" class="btn btn-primary bg-warning">Yes</button></a>
											</form>
										</div>
									</div>
									
								</div>
							</div>
									{{-- notificaion model --}}


									<div  data-backdrop="false" id="modal_mini{{ $data->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Test</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this Test.</p>

										</div>

										<div class="modal-footer">
										{{--  @permission('delete_test')  --}}
											<form action="{{ url('delete_test') }}" method="post">
											{{ csrf_field() }}
											<input type="hidden" name="del_id" value="{{ $data->id }}">
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
											{{--  @endpermission  --}}
										</div>
									</div>
									
								</div>
							</div>

								
								</div>
							
						<!-- /blog layout #4 with image -->
						
							@if (isset($data->comments))
				                   <div style="background:#edeaea; padding: 10px; margin-bottom: -11px; display: none;" class="comment-box col-md-12 comment{{ $data->id }}">
				                   @php
				                   	$count=0;
				                   	$comment_count=count($data->comments);
				                   @endphp
				                    @foreach ($data->comments as $comment)
				                    	@php
				                    	$timestamp1=$comment->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($comment->jsondata,true); 
				                    	@endphp	
				                    <div class=" col-md-12" style="padding: 0px; margin-bottom: 20px;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    	@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="35" height="35"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="35" height="35"></div>
				                    	@endif
				                    		
				                    		<div class="col-md-6"><span style="font-weight: bold;">{{ $com['name'] }}</span><br>
				                    		<span style="margin-top: 5px;">{{ $com['collage'] }}</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 10px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
			                    		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			                    			<p>{{ $com['comments'] }}</p>
			                    			@if ($com['com_image']!="null")
			                    			<img src="/mechanicalinsider/{{$com['com_image']}}" width="150" height="100">
			                    			@endif
			                    		</div>
			                    		@if (isset($comment->reply))
			                    		@php
			                    		$reply_count=count($comment->reply);
			                    		@endphp
			                    		<div id="comment_{{ $comment->id }}" class="reply_section_default" style="padding-left:60px;"">
				                    		<p class="pull-left"><a id="show_reply">{{ $reply_count }} replies</a>
				                    		<a style="display: none;" id="hide_reply">Hide {{ $reply_count }} replies</a></p>
				                    	
			                    		@foreach ($comment->reply as $reply)
			                    		@php
				                    	$timestamp1=$reply->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($reply->jsondata,true);
				                    		// print_r($reply); 
				                    	@endphp
				                    		
			                    			<div class="col-md-12 reply_section" style="; margin-bottom: 20px; display:none;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    	@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="25" height="25"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="25" height="25"></div>
				                    	@endif
				                    		
				                    		<div class="col-md-6"><span style="font-weight: bold; font-size: 11px;">{{ $com['name'] }}</span><br>
				                    		<span style=" font-size: 11px;" >{{ $com['collage'] }}</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 10px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
			                    		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			                    			<p>{{ $com['comment'] }}</p>
			                    			@if ($com['com_image']!="null")
			                    			<img src="/mechanicalinsider/{{$com['com_image']}}" width="100" height="50">
			                    			@endif
			                    		</div>
			                    		@permission('delete_comment')
			                    		<div class="col-md-10" style="margin-left: -10px;">
			                    			<a><span alt="{{ $reply->commentid }}" id='{{ $reply->id }}' class='delete_reply'>Delete</span></a>
			                    		</div>
			                    		@endpermission
				                    </div>
			                    		@endforeach
			                    		</div>
			                    		@endif
			                    		@permission('delete_comment')
			                    		<div class="col-md-10" style="margin-left: -10px;">
			                    			<a><span alt="{{ $data->id }}" id='{{ $comment->id }}' class='delete_comment'>Delete</span></a>
			                    		</div>
			                    		@endpermission
				                    </div>
				                    @php
				                    $count++;
				                    if($count==5&&$comment_count>$count)
				                    {
				                    	echo "<a><span alt='$data->field_type' id='$data->id' class='view_more_comment'>view more comments</span></a>";				          
				                    	break;
				                    }
				                    @endphp

				                    @endforeach
				                    </div>
				                    @endif
					</div>
					</div>
					</div>


				        @endforeach
				        @endif
				    </div>

				    <script>
				    	$(document).ready(function(){

				    		var last_page=1;
				    		$(window).scroll(function() {

				    			var scroll_top=$(document).scrollTop();
				    			page= Math.round(scroll_top/100);
				    			
				    			// 	page= Math.round(scroll_top/"<?php if(session('record_count')=='10'){ echo '1000'; }elseif(session('record_count')<'10' || session('record_count')>='7'){ echo '700'; }elseif(session('record_count')<'7' || session('record_count')>='4'){ echo '400'; } elseif(session('record_count')<'4' || session('record_count')>='2'){ echo '200'; }elseif(session('record_count')<'2' || session('record_count')>0){ echo '100'; } ?>");
				    			// 	"<?php if(session('record_count')==0){ echo 'page=last_page+1; ';} ?>";
				    				
				    			// 	console.log("<?php echo session('record_count');?>");
				    			// console.log(page);


				    			if(page>last_page)
				    			{
				    				last_page=page;
				    				var path = "<?php echo $next_page_url; ?>";
									path=path.slice(0,-1);

								      $.ajax({
								          url:path+last_page,
								          type:"GET",
								          success:function(data){
								          	  
								          $("#post_section").append(data);
								          }
								      });
				    			}

							  });



				    		var limit=5;
				    		var postid=$("#postid").val();
				    		var field_type=$("#field_type").val();

				    		 
				    		$(document).on('click','.comment',function(e){
				    			var id=$(this).attr('id');
				    			$("div."+id).slideToggle();
				    			e.preventDefault();
				    		});
				    		$(document).on('click','#show_reply',function(e){
				    			$(this).parents('div').children('div.reply_section').show();
				    			$(this).siblings("a").show();
				    			$(this).hide();
				    			e.preventDefault();
				    		});
				    		$(document).on('click','#hide_reply',function(e){
				    			$(this).parents('div').children('div.reply_section').hide();
				    			$(this).siblings("a").show();
				    			$(this).hide();
				    			e.preventDefault();
				    		});
				    		$(document).on('click','.view_more_comment',function(){
				    			var postid=$(this).attr('id');
				    			// alert(postid);
				    			var id=$(this).attr('id');
				    			// var field_type=$(this).attr('alt');
				    			// alert(field_type);
				    			limit=limit+5;
				    			var path = {!! json_encode(url('/')) !!}
				    			$.get(""+path+"/ajax2",{
				    				postid:postid,
				    				limit:limit },
				    				function(data,success){
				    					$(".comment"+id).html(data);
				    				
				    			});
				    		});
				    		$(document).on('click','.delete_reply',function(){
				    			var commentid=$(this).attr('alt');
				    			var id=$(this).attr('id');
				    			var path = {!! json_encode(url('/')) !!}
				    			$.get(""+path+"/delete_reply",{
				    				id:id,
				    				commentid:commentid },
				    				function(data,success){
				    					$("#"+id).parents('div.reply_section_default').html(data);
				    				
				    			});
				    		});
				    		$(document).on('click','.delete_comment',function(){
				    			var postid=$(this).attr('alt');
				    			var commentid=$(this).attr('id');
				    			var path = {!! json_encode(url('/')) !!}
				    			$.get(""+path+"/delete_comment",{
				    				postid:postid,
				    				commentid:commentid,limit:limit },
				    				function(data,success){
				    					$(".comment"+postid).html(data);
				    				
				    			});
				    		});
				    	});
				    </script>
				
				    <!-- /layout 1 -->
				</div>
        <!-- /main content -->
<!-- Secondary sidebar -->
			@include('includes.right_sidebar')
			<!-- /secondary sidebar -->
			

		</div>
		<!-- /page content -->
			
@endsection
@section('js')
<script>
	$(document).ready(function(){
		$(document).on('keyup',"#searchbar",function(e){

			if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
   
			var val=$("#searchbar").val();
			console.log(val);
			var path = "{{ url('/test_searchnew') }}";
			$.ajax({
				url: path,
				type: 'POST',
				data: {
					'_token' : "{{ csrf_token() }}",
					'val': val
					
				},
				success:function(response) {
					$("#post_section").html(response);
				}

		    

		});

	}
	});
});
</script>
@endsection