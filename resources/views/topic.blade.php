@extends('layouts.main')
<link rel="stylesheet" href="{{ URL::asset('assets/Date-Time-Picker-Bootstrap-4/build/css/bootstrap-datetimepicker.min.css')}}">

@section('js_head')
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">
	
	<script src="{{ URL::asset('assets/Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js')}}"></script>
	<script type="text/javascript">
		$(function () {
			$('#datetimepicker1').datetimepicker();
		});
	</script>
@include('mapjs')	
   
    <style>
        
        #loading {
            width:100px;
            height: 100px;
            position: fixed;
            top: 30%;
            left: 45%;
            z-index:2;
            background-color: transparent;        
        }
    </style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('insert_topic') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                            {{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Create New Topic</h5>									
								</div>

								<div class="panel-body">

									<div class="form-group" id="groups_select">
                                        <label class="col-lg-3 control-label">Select @if(Auth::user()->role == 'admin' ) Courses @else Class @endif:</label>
                                        <div class="col-lg-9 multi-select-full">
                                            <select name="course_id[]" data-placeholder="Select one..." multiple="multiple" class="multiselect-select-all-filtering course_id" required>
                                                @foreach ($new_Courses as $c)
                                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

									<div class="form-group" id="groups_select">
										<label class="col-lg-3 control-label">Select Subject:</label>
										<div class="col-lg-9 multi-select-full">
                                        	<select name="subject_id[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning sub_record " required>
                                                <option value="">Select one..</option>      
                                                
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-lg-3 control-label">Topic Name </label>
										<div class="input_fields_wrap col-md-9">
											<div class="col-md-12 field_section">
												<div class="col-md-4">
													<input type="text" class="form-control" name="sub[]" placeholder="Enter Topic Name" required>
												</div>
												<div class="col-md-4">
													<input type="text" class="form-control" name="position[]" placeholder="Enter Topic Position" required>
												</div>
												
												<div class="col-md-4">
													<a class="add_field_button btn btn-info btn-sm">Add More Fields</a>
												</div>
											</div>
										</div>
									</div>
									

								</div>
								<br /><br />
								<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Add Topic <i class="icon-arrow-right14 position-right"></i></button>
								</div><br>

							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')

    <script>
        $(document).ready(function() {
            var max_fields      = 8; //maximum input boxes allowed
            var wrapper         = $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID
            
            var x = 1; //initlal text box count
            $(".add_field_button").click(function(e){ //on add input button click
                e.preventDefault();
                if(x <= max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div style="margin-top:10px;" class="col-md-12 field_section"><div class="col-md-4"><input type="text" class="form-control" name="sub[]" placeholder="Enter Topic Name"></div><div class="col-md-4"><input type="text" class="form-control" name="position[]" placeholder="Enter Topic Position" required></div><div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div></div>'); //add input box
					
                    if(x>max_fields)
                    {
                        $('.add_field_button').hide();
                    }

                    // $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
                }												
            });
            
            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parents('div.field_section').remove(); x--;
                $('.add_field_button').show();
            })
		});
		
		$('select[name="course_id[]"]').change(function(){
			var course= $(this).val();
			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_subject',   
				data: {course:course},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{
						console.log(data.data);
						
						$('#subcategory').show();
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				}  
			});			
		});
    </script>

    <script>
        $('#login_form').submit(function() {
            $('#gif').css('visibility', 'visible');
        });
    </script>
	
@endsection
