@extends('layouts.main')
@section('js_head')


<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>
	

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>


	{{-- <script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_ckeditor.js') }}"></script>
 --}}

@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('add_posts') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Post</h5>
									
								</div>

								<div class="panel-body">
									<div class="col-md-10 col-md-offset-1">
										<div class="form-group">
											<label class="col-lg-2 control-label">Topic:</label>
											<div class="col-lg-5">
												<select id="topic" name="posttype" data-placeholder="Select one..." class="select select-border-color border-warning" required>
													<option value="">Select one...</option>
													@foreach ($query as $q)
													<option value="{{ $q->topic_name }}">{{ $q->topic_name }}</option>
													@endforeach
													<option id="create_topic" value="create_topic">create new topic</option>
												</select> 
											</div>
											<div id="new_topic1">
											<label class="col-lg-1 control-label">Create New:</label>
											<div class="col-lg-3">
												<input type="text" id="new_topic" name="new_topic" class="form-control" placeholder="add new topic name">
											</div>
											</div>
										</div>
										<script>
											$(document).ready(function(){
												$("#new_topic1").hide();
												$("#topic").change(function(){
													var topic=$("#topic").val();
													if(topic=='create_topic')
													{
														$("#new_topic1").show();
													}
													else
													{
														$("#new_topic1").hide();
													}
												});
												
											});
										</script>

										<div class="form-group">
											<label class="col-lg-2 control-label">Post Title:</label>
											<div class="col-lg-10">
												<input type="text" name="title" class="form-control" placeholder="Enter Post Title">
											</div>
										</div>
										<script type="text/javascript">
										$(document).ready(function() {
										  $('.summernote').summernote();
										});
									</script>
										<div class="form-group">
											<label class="col-lg-2 control-label">Post Description:</label>
											<div class="col-lg-10">
												<textarea  name="post_description" class="summernote"></textarea>
											</div>
										</div>
										<div class="form-group">
										<label class="col-lg-2 control-label">Post Type:</label>
										<div class="col-lg-10">
											<select class="form-control" name="post_type">
												<option value="wordpress">Wordpress</option>
												<option value="external_link">External link</option>
												<option value="news">News (only html text)</option>
											</select>
										</div>
									</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Post URL:</label>
											<div class="col-lg-10">
												<input type="text" name="posturl" class="form-control" placeholder="Enter Post URL">
											</div>
										</div>

										<div class="form-group">
										<label class="col-lg-2 control-label">Select:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" id="groups_institute" name="type_institute" value="group" checked="checked">
												Groups
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" id="college_institute" name="type_institute" value="college">
												College
											</label>

											<label class="radio-inline">
												<input type="radio" class="styled" id="both" name="type_institute" value="both">
												Both
											</label>

										</div>
									</div>
									<script>
											$(document).ready(function(){
												$("#college_select").hide();
												$("#stream_select").hide();
												$("#course_select").hide();
												$("#year_select").hide();
												$("#groups_institute").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();	
														$("#groups_select").show();
												});
												$("#college_institute").click(function(){
														$("#college_select").show();
														$("#stream_select").show();
														$("#course_select").show();
														$("#year_select").show();
														$("#groups_select").hide();
												});
												$("#both").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();
														$("#groups_select").hide();
												});
												
											});
									</script>
									
									<div class="form-group" id="course_select">
									<label class="col-lg-2 control-label">Select Course:</label>
									<div class="col-lg-10 multi-select-full">
										<select name="course" data-placeholder="Select Course..." id="coursedropdown" class="course-selected form-control" required>
										<!-- multiple="multiple"   -->
											@if($course)
												@foreach ($course as $cou_rse)
												@if($cou_rse)
													<option value="{{ $cou_rse->id }}">{{ $cou_rse->course_name }}</option>
												@endif
												@endforeach
												@endif
										</select>
										<input type="hidden" value="{{route('get_course')}}" name="urlcourse" class="urlcourse"/>
									</div>
								</div>

								<div class="form-group" id="college_select">
									<label class="col-lg-2 control-label">Select College:</label>
									<div class="col-lg-10 multi-select-full">
										<select name="college" data-placeholder="Select College..." id="collagedropdown" class="college-selected form-control" required>
										<option value="">Select College </option>
										</select>
										{{--<!-- <input type="hidden" value="{{ route('get_stream') }}" name="urlcollege" class="urlcollege"/> -->--}}
									</div>
								</div>

								<div class="form-group" id="stream_select">
									<label class="col-lg-2 control-label">Select Stream:</label>
									<div class="col-lg-10 multi-select-full">
										<select name="stream" data-placeholder="Select Stream..." class="stream-selected form-control" required>
											<option value="">Select Stream</option>
										</select>
									</div>
								</div>

								<div class="form-group" id="year_select">
									<label class="col-lg-2 control-label">Select Year:</label>
									<div class="col-lg-10 multi-select-full">
										<select name="year" data-placeholder="Select Year..."  class="year-selected form-control" required>
													<option value=""></option>
										</select>
									</div>
								</div>

										<div class="form-group" id="groups_select">
											<label class="col-lg-2 control-label">Select Groups:</label>
											{{-- <div class="col-lg-10">
												@foreach ($data as $d)
												<div class="col-md-4">
													<div class="checkbox">
														<label>
														<input type="checkbox" name="groupid[]" value="{{ $d->id }}">
															{{ $d->topic }}
														</label>
													</div>
												</div>
												@endforeach
											</div> --}}
											<div class="col-lg-10 multi-select-full">
												<select name="groupid[]" data-placeholder="Select groups..." multiple="multiple" class=" multiselect-select-all-filtering" id="groups" required>
												@if($data[0]!="")
												@foreach ($data as $d)
												@if($d)
													<option value="{{ $d->id }}">{{ $d->topic }}</option>
												@endif
												@endforeach
												@endif
												
												</select>
											</div>
										</div>
										<div class="form-group">
										
											<label class="col-lg-2 control-label">Price:</label>
											<div id="price" class="col-lg-10">
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="payment" value="free">
														Free
													</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="payment" value="paid">
														Paid
													</label>
												</div>
											</div>
											<input style="display: none;" id="payment1" type="text" name="payment1" class="form-control" placeholder="Enter amount">
											</div>
										</div>
										<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="payment"]').change(function(){
												    if(jQuery(this).val() == 'paid'){
												    	jQuery("#payment1").show();
												    }
												    else
												    {
												    	jQuery("#payment1").hide();
												    }
												});
											});
										</script>

										<div class="form-group">
											<label class="col-lg-2 control-label">Writer:</label>
											<div class="col-lg-10">
												<input type="text" name="writer" class="form-control" placeholder="Enter writer name">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Pic:</label>
											<div class="col-lg-10">
												<input id="photoInput" type="file" name="pic">
											</div>
											<div id="imgContainer"><span id="imgerr"></span></div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Thumbnail:</label>
											<div class="col-lg-10">
												<input id="photoInput1" type="file" name="thumbnail">
											</div>
											<div id="imgContainer1"><span id="imgerr"></span></div>
										</div>
										<fieldset>
											<legend></legend>
											{{-- <div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to like this post </label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_like" type="checkbox" class="switchery" value="off">
													</label>
												</div>
											</div> --}}
											<div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to comment this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_comment" type="checkbox" class="switchery" value="off">
													</label>
												</div>
											</div>
										</fieldset>

										<div class="text-right">
											<button id="submit" type="submit" class="btn btn-primary">Add<i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->


		</div>

@endsection
@section('js')
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				posttype:{
					required:true
				},
				title:{
					required:true
				},
				post_description:{
					required:true
				},
				groupid:{
					required:true
				},
				payment:{
					required:true
				},
				writer:{
					required:true
				},
				post_type:{
					required:true
				},
				pic:{
					required:function(element) {
				        var groups=jQuery("#groups").val();
				        if(groups.length>1)
				        {
				        	return true;
				        }
				        
				      }
				},
				thumbnail:{
					required:function(element) {
				        var groups=jQuery("#groups").val();
				        if(groups.length>1)
				        {
				        	return true;
				        }
				        
				      }
				},
				payment1:{
					required:true,
					number:true
				}
			}
		});
	});
</script>
{{-- <script>
	$(function () {
    $("#submit").bind("click", function () {
        // Get reference of FileUpload.
        var file_size = $('#photoInput')[0].files[0].size;
        file_size_kb=file_size/1024;
		

        var fileUpload = $("#photoInput")[0];
        //Check whether the file is valid Image.
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        if (regex.test(fileUpload.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if (typeof (fileUpload.files) != "undefined") {
                //Initiate the FileReader object.
                var reader = new FileReader();
                //Read the contents of Image File.
                reader.readAsDataURL(fileUpload.files[0]);
                reader.onload = function (e) {
                    //Initiate the JavaScript Image object.
                    var image = new Image();
                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;
                    image.onload = function () {
                        //Determine the Height and Width.
                        var height = this.height;
                        var width = this.width;
                        // if (height > 300 || width > 600) {
                        //     // alert("Height and Width must not exceed 100px.");
                        //     $("#imgContainer span#imgerr").html('<span style="color:red;">Max Width and height is 600px * 300px.</span>');
                        //     return false;
                        // }
                        // else if (height < 225 || width < 400)
                        // {
                        //     $("#imgContainer span#imgerr").html('<span style="color:red;">Min Width and height is 400px * 225px.</span>');
                        //     return false;
                        // }
                        // else
                         if(file_size_kb>400)
						{
							 $("#imgContainer span#imgerr").html('<span style="color:red;">Max upload size is 400kb.</span>');
				                            return false;
						}
						else
						{
							alert("Uploaded image has valid Height and Width.");
                        	$("#imgContainer span#imgerr").html(" ");
                        	return true;
						}

                        
                    };
                }
            } else {
                $("#imgContainer span#imgerr").html('<span style="color:red;">This browser does not support HTML5.</span>');
                return false;
            }
        } else {
            $("#imgContainer span#imgerr").html('<span style="color:red;">Please select a valid Image file</span>');
            return false;
        }
 
        //Get reference of FileUpload.
        var fileUpload1 = $("#photoInput1")[0];

        var file_size1 = $('#photoInput1')[0].files[0].size;
        file_size_kb1=file_size1/1024;
		

        //Check whether the file is valid Image.
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        if (regex.test(fileUpload1.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if (typeof (fileUpload1.files) != "undefined") {
                //Initiate the FileReader object.
                var reader1 = new FileReader();
                //Read the contents of Image File.
                reader1.readAsDataURL(fileUpload1.files[0]);
                reader1.onload = function (e) {
                    //Initiate the JavaScript Image object.
                    var image1 = new Image();
                    //Set the Base64 string return from FileReader as source.
                    image1.src = e.target.result;
                    image1.onload = function () {
                        //Determine the Height and Width.
                        var height1 = this.height;
                        var width1 = this.width;
                        // if (height1 > 100 || width1 > 100) {
                        //     // alert("Height and Width must not exceed 100px.");
                        //     $("#imgContainer1 span#imgerr").html('<span style="color:red;">Max Width and height is 100px * 100px.</span>');
                        //     return false;
                        // }
                        // else if (height1 < 20 || width1 < 20)
                        // {
                        //     $("#imgContainer1 span#imgerr").html('<span style="color:red;">Min Width and height is 20px * 20px.</span>');
                        //     return false;
                        // }
                        // else
                         if(file_size_kb1>100)
		{
			 $("#imgContainer1 span#imgerr").html('<span style="color:red;">Max upload size is 100kb.</span>');
                            return false;
		}

                        alert("Uploaded image has valid Height and Width.");
                        $("#imgContainer1 span#imgerr").html(" ");
                        return true;
                    };
                }
            } else {
                $("#imgContainer1 span#imgerr").html('<span style="color:red;">This browser does not support HTML5.</span>');
                return false;
            }
        } else {
            $("#imgContainer1 span#imgerr").html('<span style="color:red;">Please select a valid Image file</span>');
            return false;
        }
    });
});
</script> --}}
{{-- for switch buttons --}}
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	{{-- for switch buttons --}}


@endsection