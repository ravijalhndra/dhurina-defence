@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
@endsection
@section('content')
<style>
    .datatable-footer{
        display:none ! important;
    }
    .dataTables_length { display:none ! important;}
    .datatable-header { float:right; }
    .pagination { 
    float:right;  
    }
    .paginationdiv{
        min-height: 60px;
    padding: 10px 50px;
    background:#f7f7f7;
    }
</style>
	<div class="page-content">
        <div class="content-wrapper">
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_challenge_level') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Challenge Level</h3>
					</div>
				<table class="table">
						<thead>
							<tr>
                                {{-- <th>Request Id</th> --}}
                                <th>User Id</th>
								<th>Level</th>
                                <th>Status</th>
                                <th>Amount</th>
                                <th>Requested On</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@foreach($data as $d)
							<tr>
                                    {{-- <td>{{$d->id}}</td> --}}
								<td>{{$d->user_id}}</td>
								<td>{{$d->level_for}}</td>
                                <td>
                                    @if($d->status=='0')

                                    <span class="label label-warning"> Requested </span>

                                    @else
                                    <span class="label label-success"> Approved </span>
                                    @endif
                                  
                                </td> 
                                <td>{{$d->payment}}</td>
                                <td>@php echo date("d M, Y H:i",strtotime($d->date)) @endphp</td>
								 <td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
                                            <ul class="dropdown-menu dropdown-menu-right">
									           <li><a href="{{url('assign-amount/'.$d->id)}}"><i class="icon-pencil"></i> Assign Amount</a></li>
											</ul>
										</li>
									</ul>
								</td>  
                                							   
                          	</tr>													
						@endforeach								
						</tbody>
					</table>
                </div>
                <div class="paginationdiv panel panel-flat">
                Showing {{ $data->firstItem() }} to {{ $data->lastItem() }} of {{ $data->total() }} Records
                {!! $data->links() !!}
                </div>
                {{-- @php echo "<pre>"; print_r($data); @endphp --}}
            </div>
            {{-- {!! $data->links() !!} --}}
			<!-- /main content -->
		</div>
@endsection
