@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_institutes') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Institutes</h3>
						
					</div>

					<div class="panel-body">
						
					</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Address</th>
								<th>Created At</th>
								
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
						@foreach ( $institutes as $institute)
						@php
    $created_at=strtotime($institute->created_at);

@endphp
								<tr>
								<td>{{ $institute->ins_id }}</td>
								<td>{{ $institute->name }}</td>
								<td>{{ $institute->email }}</td>
								<td>{{ $institute->phone }}</td>
								<td>{{ $institute->address }}</td>
								<td>{{ date('M d,Y  h:i A',$created_at) }}</td>
								
								<td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												<li><a href="{{ url('institute/employees/'.$institute->ins_id) }}"><i class="icon-user"></i> View Employees</a></li>
												
											</ul>
										</li>
									</ul>
								</td>
								<div></div>
							</tr>
						@endforeach
							
							
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>
@endsection
