@extends('layouts.main')
<link rel="stylesheet" href="{{ URL::asset('assets/Date-Time-Picker-Bootstrap-4/build/css/bootstrap-datetimepicker.min.css')}}">

@section('js_head')
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">
	
	<script src="{{ URL::asset('assets/Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js')}}"></script>
	<script type="text/javascript">
		$(function () {
			$('#datetimepicker1').datetimepicker();
		});
	</script>
@include('mapjs')

    <script>	
        $(document).ready(function() {
        var brand = document.getElementById('logo-id');
        brand.className = 'attachment_upload';
        brand.onchange = function() {
            document.getElementById('fakeUploadLogo').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                    $('.img-preview').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#logo-id").change(function() {
            readURL(this);
        });
    });
    </script>   
    <style>
        
        #loading {
            width:100px;
            height: 100px;
            position: fixed;
            top: 30%;
            left: 45%;
            z-index:2;
            background-color: transparent;        
        }
    </style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('update_topic') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{ $data->id }}" />
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Topic</h5>									
								</div>

								<div class="panel-body">

                                    <div class="form-group" id="groups_select">
                                        <label class="col-lg-3 control-label">Select @if(Auth::user()->role == 'admin' ) Courses @else Class @endif:</label>
                                        <div class="col-lg-9 multi-select-full">
                                            <select name="course_id[]" data-placeholder="Select one..." multiple="multiple" class="multiselect-select-all-filtering course_id" required>
                                                @foreach ($new_Courses as $c)
                                                    <option  @if(in_array($c->id,json_decode($data->course_id))) selected @endif   value="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

									<div class="form-group" id="groups_select">
										<label class="col-lg-3 control-label">Select Subject:</label>
										<div class="col-lg-9 multi-select-full">
                                        	<select name="subject_id[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning sub_record " required>
                                                <option value="">Select one..</option>      
                                                @foreach ($subject as $s)
													<option @if(in_array($s->id,json_decode($data->subject_id))) selected  @endif value="{{ $s->id }}">{{ $s->name }}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Topic Name </label>
										<div class="col-md-9">
											<input type="text" class="form-control" name="sub" placeholder="Enter Topic Name"  value="{{ $data->name }}" required>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Position</label>
										<div class="col-lg-9">
											<input type="text" class="form-control onlynumber" value="{{ $data->position }}" placeholder="Position" name="position" />
										</div>
									</div>

								</div>
								<br /><br />
								<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Update Topic <i class="icon-arrow-right14 position-right"></i></button>
								</div><br>

							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')
    <script>
        $(document).ready(function() {
            var max_fields      = 8; //maximum input boxes allowed
            var wrapper         = $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID
            
            var x = 1; //initlal text box count
            $(".add_field_button").click(function(e){ //on add input button click
                e.preventDefault();
                if(x <= max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div style="margin-top:10px;" class="col-md-12 field_section"><div class="col-md-9"><input type="text" class="form-control" name="sub[]" placeholder="Enter Subject Name"></div><div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div></div>'); //add input box

                    if(x>max_fields)
                    {
                        $('.add_field_button').hide();
                    }

                    // $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
                }												
            });
            
            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parents('div.field_section').remove(); x--;
                $('.add_field_button').show();
            })
        });

        $('select[name="course_id[]"]').change(function(){
			var course= $(this).val();
			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_subject',   
				data: {course:course},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{						
						$('#subcategory').show();
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				}  
			});			
        });
        
        $('#login_form').submit(function() {
            $('#gif').css('visibility', 'visible');
        });
    </script>	
@endsection
