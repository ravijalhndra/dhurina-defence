@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
			

				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">View  Questions</h4>
					</div>

					<div class="panel-body">
						
					</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
								<th style="text-align: center;">ID</th>
								<th style="text-align:center;">Subject</th>
								<th style="text-align: center;">Question</th>
								<th style="text-align: center;">Option </th>
								<th style="text-align: center;">Answer</th>
								<th style="text-align: center;">Detail</th>
								<th style="text-align: center;">Delete</th>
							</tr>
						</thead>
						<tbody>
						@php
						$count=1;
						@endphp
						@foreach ( $query as $data)
						@php
						$json=json_decode($data->jsondata,true);
						// dd($json);
							// dd($json);
						@endphp
						
						
						<tr>
							<td style="text-align: center; ">{{ $count }}</td>
							<td style="text-align: center; ">{{ $data->sub_test_name }}</td>
							<td style="text-align: center;">
								@if ($json['data']['question']['image']=="NULL")
									<span>{!! $json['data']['question']['value'] !!}</span>
								
								@endif
							</td>
							<td style="text-align: center; ">Option A:
								@if ($json['data']['option a']['image']=="NULL")
									<span>{{ $json['data']['option a']['value'] }}</span>
								@endif
							    <br>Option B:
								@if ($json['data']['option b']['image']=="NULL")
									<span>{{ $json['data']['option b']['value'] }}</span>
								@endif
								<br>Option C:
								@if ($json['data']['option c']['image']=="NULL")
									<span>{{ $json['data']['option c']['value'] }}</span>
							
								@endif<br>
							    Option D:
								@if ($json['data']['option d']['image']=="NULL")
									<span>{{ $json['data']['option d']['value'] }}</span>
								@endif<br>
							    Option E:			
								@if ($json['data']['option e']['image']=="NULL")
									<span>{{ $json['data']['option e']['value'] }}</span>
							
								@endif
							</td>
							<td style="text-align: center; ">
								@if ($json['data']['answer']['image']=="NULL")
									<span>{{ $json['data']['answer']['value'] }}</span>
								
								@endif
							</td>
							@if($data->testid=='0')
                           <td style="text-align: center; "> {{ $json['data']['by']['uid'] }}<br>
							{{ $json['data']['by']['name'] }}<br><a href="{{url(('verify_question/'.$data->id))}}"> <button type="button" class="btn btn-danger">Unverified</button></a></td>  
                            @else
                         
                          <td style="text-align: center; ">{{ $json['data']['by']['uid'] }}<br>
							{{ $json['data']['by']['name'] }}<br><br><a href="{{url(('verify_question/'.$data->id))}}"> <button type="button" class="btn btn-success">Verified</button></a></td>    
                            @endif

                            <td class="text-center">
								<ul class="icons-list">

								<ul>
									
										<li style="text-align: center; " ><a data-toggle="modal" data-target="#modal_mini{{ $data->id }}" href="#"><i class="icon-trash"></i> Delete</a></li>
								</ul>
							
								</ul>
							</td>
						</tr>

								<div  data-backdrop="false" id="modal_mini{{ $data->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Question</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this question.</p>

										</div>

										<div class="modal-footer">
											<form action="{{ url('delete_sub_question') }}" method="post">
											{{ csrf_field() }}
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<input type="hidden" name="id" value="{{ $data->id }}">
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
											
										</div>
									</div>
								</div>
							</div>
						
							</tr>
							
							@php
								$count++;
							@endphp
						@endforeach
							
							
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<!--   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

@endsection
