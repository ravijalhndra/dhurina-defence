@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
<style>
		.datatable-footer{
			display:none ! important;
		}
		.dataTables_length { display:none ! important;}
		.datatable-header { float:right; }
		.pagination { 
		float:right;  
		}
		.paginationdiv{
			min-height: 60px;
		padding: 10px 50px;
		background:#f7f7f7;
		}
	</style>
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					<!-- @permission('add_admin')
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_admin') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
					@endpermission -->
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">Pending</h3>
					</div>

					

					<table  class="table table-striped table-bordered">
						<thead>
						
						
							<tr style="text-align:center">
								<th  style="text-align:center">ID</th>
								<th  style="text-align:center">User Name</th>
								<th  style="text-align:center">Opponent Name</th>
								<th  style="text-align:center">Status</th>
								
								<th  style="text-align:center">Subject</th>
								<th  style="text-align:center">Created At</th>
								
								
								
							</tr>
						</thead>
						<tbody>
						@foreach ( $data as $d)
							<tr style="text-align:center">
								<td>{{ $d->id }}</td>
								<td>{{ $d->user_name}}</td>
								<td>{{ $d->opponent_name }}</td>
								<td>@if($d->play_status == 0) Not Played @else($d->play_status == 1) User Played  @endif</td>
							
								<td>{{$d->subject}}</td>
								<td>{{$d->created_at}}</td>
							
							</tr>	
						@endforeach
							
							
						</tbody>
					</table>
				</div>
				<div class="paginationdiv panel panel-flat">
						Showing {{ $data->firstItem() }} to {{ $data->lastItem() }} of {{ $data->total() }} Records
						{!! $data->links() !!}
						</div>


				

			</div>
			<!-- /main content -->

		</div>
		
	


        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
       $(document).ready(function() {
    $('#example').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>
<script>

</script>
@endsection
