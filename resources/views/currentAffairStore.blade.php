@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<!-- Theme JS files -->

<script>
	
	$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



</script>
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form action="{{ url('currentAffair_store') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Current Affair</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<div class="form-group">
											<label class="col-lg-3 control-label">Select Groups:</label>
											<div class="col-lg-9 multi-select-full">
												<select name="groupid[]" data-placeholder="Select groups..." multiple="multiple" class="multiselect-select-all-filtering" required>
												
												@if ($data[0]!="")
												@foreach ($data as $d)
												@if($d)
													<option value="{{ $d->id }}">{{ $d->topic }}</option>
												@endif
												@endforeach
												@endif
												
												</select>
											</div>
										</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Title:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="title">
										</div>
									</div>
									<div class="form-group">
										
											<label class="col-lg-2 control-label">Choose one:</label>
											<div id="price" class="col-lg-10">
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="choose" value="description" checked>
														Description
													</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="choose" value="url">
														Website
													</label>
												</div>
											</div>
											</div>
											</div>
											<div id="description1"  class="form-group">
											<label class="col-lg-2 control-label">Description:</label>
											<div class="col-lg-10">
												<textarea id="description"  name="description" class="summernote"></textarea>
											</div>
										</div>
										<div id="url1" style="display: none;" class="form-group">
										<label class="col-lg-3 control-label">URL:</label>						
												<div class="col-lg-9">
											<input id="url" type="text" class="form-control" name="url" placeholder="www.xyz.com">
										</div>
									</div>
										
										<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="choose"]').change(function(){
												    if(jQuery(this).val() == 'description'){
												    	jQuery("#description1").show();
												    	jQuery("#url").val("");
												    	jQuery("#url1").hide();
												    }
												    else if(jQuery(this).val() == 'url'){
												    	jQuery("#description1").hide();
												    	jQuery("#url1").show();
												    	jQuery("#description").val('');

												    }
												    else
												    {
												    	jQuery("#description1").hide();
												    	jQuery("#url1").hide();
												    }
												});
											});
										</script>
										<div class="form-group">
										<label class="col-lg-3 control-label"></label>
										<div class="col-lg-9">
											 <div class="main-img-preview">
											<img class="thumbnail img-preview" src="http://farm4.static.flickr.com/3316/3546531954_eef60a3d37.jpg" title="Preview Logo" >
							              </div>
																		 <div class="input-group">
							                <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled" style="display:none">
							                <div class="input-group-btn">
							                  <div class="fileUpload btn btn-danger fake-shadow">
							                    <span><i class="glyphicon glyphicon-upload"></i> Upload Logo</span>
							                    <input id="logo-id" name="logo" type="file" class="attachment_upload" >
							                  </div>
							                </div>
							              </div>
										</div>
									</div>
									<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
									<div class="text-right">
										<button type="submit" class="btn btn-primary" onclick="$('#loading').show();"  >Submit form <i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection