@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
			{{-- table --}}
			<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Users Result{{-- <a href="{{ url('/export_csv/'.$id) }}" style="margin-right: 100px; margin-left: 10px;" class="pull-right btn btn-primary">Export CSV</a><a href="{{ url('/create_pdf/'.$id) }}" class="pull-right btn btn-success">Download PDF</a> --}}</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a href="{{ url('/test/users_result_csv/'.$id) }}" class="btn btn-success" style="color:#FFF;"> Download</a></li>
		                		{{-- <li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li> --}}
		                	</ul>
	                	</div>
					</div>

					<div class="panel-body">
						
					</div>

					<table class="table datatable-basic table-bordered">
						<thead>
							<tr>
								<th>Rank</th>
								<th>Full Name</th>
								<th>College</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Marks</th>
								<th>Time taken</th> 
							</tr>
						</thead>
						<tbody>
						@php
							$count=1;
						@endphp
						@foreach ($data as $test)
							<tr>
								<td>{{ $count }}</td>
								<td>{{$test->user->name}}</td>
								<td>{{$test->user->college}}</td>
								<td>{{$test->uid}}</td>
								<td>{{$test->user->mobile}}</td>
								<td>{{$test->mark}}</td>
								<td>{{ gmdate("H:i:s",$test->timetaken) }} </td>  
							</tr>
							@php
							$count++;
							@endphp
						@endforeach
							
							
							
						</tbody>
					</table>
				</div>
			{{-- table --}}
					

			</div>
			<!-- /main content -->

		</div>
@endsection
