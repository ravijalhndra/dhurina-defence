@extends('layouts.main')
@section('js_head')
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				@if(Auth::user()->role == 'admin' ||  Auth::user()->role == 'sub_admin')
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add-test-series') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
				@endif

					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Academy Info</h3>	
					</div>
				

					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
                                <th>Academy Name</th>
                              	<th>Mobile</th>								
                                <th>City</th>
                                <th>Address</th>
								<th>State</th>								
							</tr>
						</thead>
						<tbody>
							@foreach($data as $u)
							<tr>
								<td>{{$u->id}}</td>
                                <td>{{$u->name}}</td>														
                                <td>{{$u->mobile_no}}</td>
                                <td>{{$u->city}}</td>
								<td>{{$u->address}}</td>  
								<td>{{ $u->state }}</td>      
							    
                          	</tr>				
						@endforeach								
						</tbody>
					</table>
				</div>
				
                {{ $data->links() }}
			</div>
		</div>		

@endsection

