@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_banner') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					    <div class="panel-heading">
						    <h3 class="panel-title">Meeting Rooms</h3>
					    </div>
					<table class="table datatable-basic">
						<thead>
							<tr>
                                <th>ID</th>
								<th>Meeting ID</th>
								<th>School Name</th>
								<th>Teacher Name</th>
								<th>Class</th>
                                <th>Subject</th>
                                <th>Link</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
                            @foreach($data as $d)
                                <tr>
                                    <td>{{$d->id}}</td>
                                    <td>{{$d->room_name}}</td>
                                    
                                    <td>{{$d->school_name}}</td>
                                    <td>{{$d->teacher_name}}</td>	
                                    <td>{{$d->class}}</td>
                                    <td>{{$d->subject}}</td>  
                                    <td><a target = '_blank' href='{{$d->link}}' >Start </a></td>     
                                    <td></td>                           						   
                                </tr>													
                            @endforeach
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->
			</div>
			<!-- /main content -->
		</div>
@endsection
