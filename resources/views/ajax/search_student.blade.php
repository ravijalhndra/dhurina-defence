<tbody id="ajaxresponse">
					
                           
                         
                               
                                @foreach($data as $d)
                                <tr>
                                <td>{{$d->id}}</td>    
                                <td>{{$d->name}}</td>
                                <td><img src="{{$d->image}}" height="50" width="50"></td>
                                <td>{{$d->mobile}}</td>
                                <td>{{$d->course_name}}</td>
                                <td>{{$d->college_name}}</td>
                                <td>{{$d->stream}}</td>
                                <td>{{$d->year}}</td>
                                @if($d->status=='true')
                                <td><a href="{{url('change_status/'.$d->uid)}}" type="button" style="border-radius: 20px;" class="btn btn-success">Activate</a></td>
                                @else
                                <td><a href="{{url('change_status/'.$d->uid)}}" type="button" style="border-radius: 20px;" class="btn btn-danger">Deactivate</a></td>
                                @endif
                                
                                <td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												
												<li><a data-toggle="modal" data-target="#modal_mini{{$d->id}}" href="#"><i class="icon-trash"></i> Delete</a></li>
												
												<li><a href="{{url('edit_student_college/'.$d->id)}}"><i class="icon-pencil"></i> Edit</a></li>
												
												
											</ul>
										</li>
									</ul>
								</td>


								<div  data-backdrop="false" id="modal_mini{{ $d->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Student Data</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this data.</p>

										</div>

										<div class="modal-footer">
											<form action="{{ url('delete_student_college') }}" method="post">
											{{ csrf_field() }}
											<input type="hidden" name="id" value="{{ $d->id }}">
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										</div>
									</div>
									
								</div>
							</div>
                                
							</tr>
							@endforeach
												
							
						</tbody>