@extends('layouts.main')
@section('js_head')
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">



<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Video</h5>
									
								</div>

								<div class="panel-body">
                                    <div class="form-group" id="groups_select">
                                        <label class="col-lg-3 control-label">Select @if(Auth::user()->role == 'admin' ) Courses @else Class @endif:</label>
                                        <div class="col-lg-9 multi-select-full">
                                            <select name="course_id[]" data-placeholder="Select one..." multiple="multiple" class="multiselect-select-all-filtering course_id" required>
                                                @foreach ($new_Courses as $c)
                                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

									<div class="form-group" id="groups_select">
										<label class="col-lg-3 control-label">Select Subject:</label>
										<div class="col-lg-9 multi-select-full">
                                        	<select name="subject_id[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning sub_record " required>
                                                <option value="">Select one..</option>
											</select>
										</div>
									</div>
									
									<div class="form-group" id="topic_select">
										<label class="col-lg-3 control-label">Select Topic:</label>
										<div class="col-lg-9 multi-select-full">
                                        	<select name="topic_id[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning topic_record ">
                                                <option value="">Select one..</option>
											</select>
										</div>
                                    </div>
                                    
                                    <div class="form-group">
										<label class="col-lg-3 control-label">Name</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Video Name" name="name">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Position</label>
										<div class="col-lg-9">
											<input type="text" class="form-control onlynumber" placeholder="Video position" name="position">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Access:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" name="access" value="public" />
												Public
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" name="access" value="private"  checked="checked" />
												Private
											</label>
										</div>
									</div>
 
									{{--  <div class="form-group">
										<label class="col-lg-3 control-label">Add Video : </label>
										<div class="col-lg-9">
											<input type="file" class="form-control" placeholder="Video" name="image">
										</div>
									</div>  --}}

									<!-- <div class="form-group">
										<label class="col-lg-3 control-label">Youtube video</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Youtube Video" name="video" required>
										</div>
									</div> -->
									<div class="form-group">
										<label class="col-lg-3 control-label">Upload video</label>
										<div class="col-lg-9">
											<input type="file" class="form-control" placeholder="Upload Video" name="video" required>
										</div>
									</div>

								</div>
								<br /><br />
								<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Add Video <i class="icon-arrow-right14 position-right"></i></button>
								</div><br>

							</div>
						</form>
						<!-- /basic layout -->
						<div class="progress">
							<div class="progress-bar"></div>
						</div>
					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')

	<script type="text/javascript">
		$(document).ready(function() {
			
			$('#topic_select').hide();
			$('.summernote').summernote();

			$("#login_form").on('submit', function(e){
				e.preventDefault();
				$.ajax({
					xhr: function() {
						var xhr = new window.XMLHttpRequest();
						xhr.upload.addEventListener("progress", function(evt) {
							if (evt.lengthComputable) {
								var percentComplete = ((evt.loaded / evt.total) * 100);
								$(".progress-bar").width(percentComplete + '%');
								$(".progress-bar").html(percentComplete+'%');
							}
						}, false);
						return xhr;
					},
					type: 'POST',
					url: '{{ route("insert_vimeo_video") }}',
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					beforeSend: function(){
						$(".progress-bar").width('0%');
						// $('#uploadStatus').html('<img src="images/loading.gif"/>');
					},
					
					success: function(resp){
						if(resp.status == 'success'){
							$.toast({
									heading: 'Success',
									text: resp.message,
									showHideTransition: 'slide',
									icon: 'success'
								});
							window.location.href="{{route('view_video')}}";
						}else {
							$.toast({
									heading: 'Error',
									text: resp.message,
									showHideTransition: 'slide',
									icon: 'error'
								});
						}
					}
				});
			});
			
			
		});
	
		$('select[name="course_id[]"]').change(function(){
			var course= $(this).val();

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_subject',   
				data: {course:course},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{						
						$('#subcategory').show();
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				}  
			});			
		});

		$('select[name="subject_id[]"]').change(function(){

			var course_ids=[];
			var defence=['Combo Course (Airforce-Navy)','Air Force X Group','Air Force Y Group','Indian Navy ( SSR & AA )','Army GD','Army Clerk','Army Technical'];


			$('select[name="course_id[]"] option:selected').each(function() 
			{
				course_ids.push($(this).val());

			});

			var subject= $(this).val();

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_topics',   
				data: {course_ids:course_ids,subject_ids:subject},                                      
				success: function(data)
				{
					$(".topic_record").html('');
					if(data.status == 'success')
					{		
						$('#topic_select').show();				
						$('#subcategory').show();
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".topic_record").append(markup);
					}
					else
					{
						$('#topic_select').hide();
					}
				}  
			});			
		});
		
	</script>
@endsection
