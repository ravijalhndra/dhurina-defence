@extends('layouts.main')
@section('js_head')
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
@include('mapjs')

<script>
	
	$(document).ready(function() {
		var brand = document.getElementById('logo-id');
		brand.className = 'attachment_upload';
		brand.onchange = function() {
			document.getElementById('fakeUploadLogo').value = this.value.substring(12);
		};

		// Source: http://stackoverflow.com/a/4459419/6396981
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('.img-preview').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#logo-id").change(function() {
			readURL(this);
		});
	});

</script>
		
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Banner</h5>
								</div>
								<div class="panel-body">
								 
									<!-- <div class="text-right">
										<button class="btn btn-primary submit"  onclick="banner_position()" >Update Position <i class="icon-arrow-right14 position-right"></i></button>
									</div> -->
									<input type="hidden" value="{{ csrf_token() }}" name="_token">
                                    <input type="hidden" name="url" class="banner_pos" value="{{route('update_banner_pos')}}" />
							
										<!-- Basic layout-->
								<form id="login_form" action="{{ url('insert_banner') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
									{{csrf_field()}}
									
							
									<div class="form-group">										

										<div class="col-lg-12">
											<label class="col-lg-3 control-label">Select Reffral Code</label>
											<div class="col-lg-9 multi-select-full">
												<select name="refcode[]" data-placeholder="Select one..." multiple="multiple" class="multiselect-select-all-filtering" required>
													@foreach ($refcode as $c)
														<option @if(Auth::user()->ref_code == $c->ref_code) selected @endif  value="{{ $c->ref_code }}">{{ $c->ref_code }}</option>
													@endforeach
												</select>
											</div>
										</div> </br>

										<div class="col-lg-12">
											<label class="col-lg-3 control-label" style="top:40px;">
												<div class="fileUpload btn btn-danger fake-shadow">
													<span><i class="glyphicon glyphicon-upload"></i> Upload Banners</span>
													<input id="logo-id" name="banner" type="file" class="attachment_upload" >
												</div>							                  
											</label>
											<div class="col-lg-9">
												<div class="main-img-preview">
													<img class="thumbnail img-preview" src="{{  URL::asset('img/upload-icon.png') }}" style="min-height: 150px;" title="Preview Logo" >
												</div>
												<div class="input-group">
													<input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled" style="display:none">
													<div class="input-group-btn">
													
													</div>
												</div>
											</div>
										</div>

									</div>

										<br />
									<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
									<div class="text-right">
										<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Add Banner <i class="icon-arrow-right14 position-right"></i></button>
									</div><br>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script>
function banner_position()
{
	var value=$('#position').val();
	
	var url=$(".banner_pos").val();
	var token = $("input[name='_token']").val();
	
	$.ajax({
          url: url,
          method: 'POST',
          data: {position:value,_token:token},
          success: function(data) {
            
			$("#position").html(data);
              
              if(data.length > 0)
              {

				var res = $.parseJSON(data);
			     console.log(res);
              for(var i=0; i< res.length; i++)
              {
				
              }
             
              
            }
            
            else{
                var trHTML = '';
            }

            
             
          },
          
          error: function() {}
          
     
  });
}
</script>

<script>
function myfunction() {
    var x = $("#position").val();
	var y=x.replace(/\,/g,"");
	var length=y.toString().length;
	
	for(var i=0;i< length;i++)
	{
	
		
		if(y[i]%2 == 0)
		{
		document.getElementById('errorname').innerHTML="Use only odd value";  
		$(".submit").attr("disabled", true);
		}
		else
		{
		document.getElementById('errorname').innerHTML='';  
		$(".submit").attr("disabled", false);
		}
		}
		
}
</script>

@endsection
@section('js')
