@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				@if(Auth::user()->role == 'admin' )
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_video') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
				@endif

					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Video</h3>
						<div class="row" style="padding-top:50px;">
							<div class="col-md-12">
								<form action="" method="GET">
									<div class="col-md-4">
										<select id="course" name="course" data-placeholder="Select Course..." class="select select-border-color border-warning course_id" >
											<option value="">Select Course...</option>
											@foreach ($new_Courses as $c)
											<option @if($course_ids == $c->id) selected @endif value="{{ $c->id }}">{{ $c->name }} </option>
											@endforeach
										</select> 
									</div>

									<div class="col-md-4">
										<select id="subject" name="subject" data-placeholder="Select Subject..." class="select select-border-color border-warning sub_record" >
											<option value="">Select Subject...</option>
											@if($course_ids != '')
												@foreach ($subject as $s)
													<option  @if($subject_id == $s->id) selected @endif value="{{ $s->id }}">{{ $s->name }}</option>
												@endforeach
											@endif
										</select> 
									</div>

									<div class="col-md-4">
										<div class="col-md-6"> 
											<label>	<input type="search" class="form-control" name="filter" value="{{ $filter }}" placeholder="Type to filter..."></label>
										</div>
										<div class="col-md-6">
											<input type="submit" class="fab-menu-btn btn bg-success" value="search"> 											
										</div>	
									</div>

								</form>						
							</div>
							
							<div class="col-md-12  delete_div" >
								</br>
								<input type="hidden" name="multi_delete[]" id="multi_delete" value="" />	
								<input type="hidden" name="multi_publish[]" id="multi_publish" value="" />	

								@if(Auth::user()->role == 'admin' ) <a href="javascript:;" onClick="multi_delete()" ><i class="icon-trash"></i> Delete</a> &nbsp; @endif
								<a href="javascript:;" onClick="multi_publish('true')" <i class="icon-pencil"></i> Publish</a> &nbsp; 
								<a href="javascript:;" onClick="multi_publish('false')" ><i class="icon-pencil"></i> Unpublish</a>

							</div>
						</div>
					</div>
				

					<table class="table">
						<thead>
							<tr>								
                                <th>ID</th>
                                <th>Course</th>
                                <th>Subject</th>
                                <th>Name</th>
                                <th>Video</th>
								<th>Publish Status</th>
								<th>Position</th>
								<th>Access</th>
                                <th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if(count($data)>0)	
						@foreach($data as $u)
							<tr>
								<td><input type="checkbox" class="check_delete" name="delete_id" value="{{ $u->id }}" /> &nbsp; </td>								
								<td class="table-width">
									<div class="limited-text" id="row{{ $u->id }}">
										{{ $u->course_name }}
									</div>	
									@if(strlen($u->course_name) > 40) 	
									<a href="javascript:;" id="more{{ $u->id }}" onClick="view_more({{ $u->id }})"> More</a>
									<a href="javascript:;" class="hide" id="less{{ $u->id }}"  onClick="view_less({{ $u->id }})"> Less</a>
									@endif										
								</td>
                                <td>{{$u->subject_name}}</td>
                                <td>{{$u->name}}</td>    
                                <td>
									<a href="#" target="_blank"> <img src="{{$u->thumbnail}}" width="80" height="80"  </a>
								</td>   
                                <td>
                                    <span class="fab-menu-btn btn-sm @if($u->publish == 'false') bg-red @endif  bg-success btn-rounded "> @if($u->publish == 'true') Published @else Unpublish  @endif </span>  
								</td>       
								                   
								<td>{{ $u->position }}</td>

								<td>
                                    <span class="fab-menu-btn btn-sm @if($u->access == 'public') bg-info  @else bg-warning @endif btn-rounded "> {{ ucfirst($u->access) }} </span>  
								</td>  

								<td class="text-center">										
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
											<ul class="dropdown-menu dropdown-menu-right" >
												
												@if(Auth::user()->role == 'admin' )
													<li><a href="javascript:;"  onClick="fun_delete({{ $u->id }})"><i class="icon-trash"></i> Delete</a></li>
													<li><a href="{{route('edit_video',encrypt ($u->id))}}"><i class="icon-pencil"></i> Edit</a></li>
													<li><a href="{{route('get_comment',encrypt ($u->id))}}"><i class="icon-comment"></i> Comment</a></li>

												@endif

												@if($u->publish == 'true')
													<li><a href="javascript:;" onClick="fun_publish({{ $u->id }},'false')" ><i class="icon-pencil"></i> Unpublish</a></li>
												@else
													<li><a href="javascript:;" onClick="fun_publish({{ $u->id }},'true')" ><i class="icon-pencil"></i> Publish</a></li>
												@endif

											</ul>
										</li>
									</ul>									
								</td> 								
                                
                          	</tr>				
						@endforeach	
						@else
						<tr><td colspan="8"><center>No record</center></td></tr>	
						@endif						
						</tbody>
					</table>
				</div>

				@php
					$from=($data->currentPage()-1)*($data->perPage())+1;
					$to=$data->currentPage()*$data->perPage();
					$total=$data->total();
				@endphp 

				<div class="col-md-12">
					<div class="col-md-4">
						<span class="float-left">Showing {{ $from }} to {{ $to }} from {{ $total }}</span> 
					</div>
				</div> </br>
				
				@if($course_ids != '' || $subject_id != '' || $filter != '')
					{{ $data->appends(request()->query())->links() }}
				@else 
					{{ $data->links() }}
				@endif
				

			</div>
		</div>

		<div  data-backdrop="false" id="modal_mini" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Click on Yes if you want to delete this Record.</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('delete_video') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del_id" name="del_id" value="">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div  data-backdrop="false" id="modal_2" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Are you want want to change this record?</p>
					</div>
					<div class="modal-footer">
						@if(Auth::user()->role == 'admin' )
							<form action="{{ route('publish_video') }}" method="post">
						@else
							<form action="{{ route('school_publish_video') }}" method="post">
						@endif		

						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del" name="del" value="">
							<input type="hidden" id="type" name="type" value="">
							<input type="hidden" id="school_id" name="school_id" value="{{ Auth::user()->id  }}">
							<input type="hidden" id="ref_code" name="ref_code" value="{{ Auth::user()->ref_code  }}">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

@endsection
@section('js')

	<script type="text/javascript">

		function fun_delete(id)
		{	
			var val = [];
			val.push(id);
			$('#del_id').val(val);
			$('#modal_mini').modal('toggle');		
		}

		function multi_delete()
		{		
			$('#del_id').val( $('#multi_delete').val() );
			$('#modal_mini').modal('toggle');
		}

		function fun_publish(id,type)
		{
			$('#del').val(id);
			$('#type').val(type);
			$('#modal_2').modal('toggle');	
		}

		function multi_publish(type)
		{
			$('#del').val( $('#multi_publish').val() );
			$('#type').val(type);
			$('#modal_2').modal('toggle');	
		}

		//Default hide delete button
		$( document ).ready(function() {
			$(".delete_div").css("display", "none");
		});

		$('input[name=delete_id]').change(function(){
			
			var val = [];
			$(':checkbox:checked').each(function(i){
				val[i] = $(this).val();
			});
			
			if(val.length > 0){	
					
				$('#multi_delete').val(val);		
				$('#multi_publish').val(val);
				$(".delete_div").css("display", "block");
			}	
			else{
				$(".delete_div").css("display", "none");
			}
		});

		$('select[name="course"]').change(function(){
			var course= $(this).val();

			var courseids=[];
			var course_id=courseids.push(course);

			console.log(courseids);			
			// var jsonString = JSON.stringify(course_ids);

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_subject',   
				data: {course:courseids},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{
						console.log(data.data);
						
						$('#subcategory').show();
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				}                      
	
			});
			
			
		});

	</script>
@endsection

