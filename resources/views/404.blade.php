@extends('layouts.main')
@section('js_head')

@endsection
@section('content')

		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Error title -->
				<div class="text-center content-group">
					<h1 class="error-title">403</h1>
					<h5>Oops, an error has occurred. Forbidden! </h5>
				</div>
				<!-- /error title -->


			</div>
			<!-- /main content -->

		</div>

@endsection
