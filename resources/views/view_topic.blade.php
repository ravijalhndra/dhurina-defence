@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					@if(Auth::user()->role == 'admin' )
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ route('add_topic') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
					@endif
					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Topic</h3>
					</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>Position</th>
								<th>Course</th>
								<th>Subject</th>
								<th> <b>Topic</b>  </th>
								<th><b>Action </b></th>
								<th class="hide"></th>
                                <th class="hide"></th>
                                <th class="hide"></th>
							</tr>
						</thead>
						<tbody>
						@foreach($data as $u)
							<tr>
								<td>{{ $u->position }}</td>
								<td class="table-width">
									<div class="limited-text" id="row{{ $u->id }}">
										{{ $u->course_name }}
									</div>	
									@if(strlen($u->course_name) > 40) 	
									<a href="javascript:;" id="more{{ $u->id }}" onClick="view_more({{ $u->id }})"> More</a>
									<a href="javascript:;" class="hide" id="less{{ $u->id }}"  onClick="view_less({{ $u->id }})"> Less</a>
									@endif										
								</td>
								<td>{{ $u->subject_name }}</td>
                                <td>{{$u->name}}</td>													
                                <td>
									@if(Auth::user()->role == 'admin' )
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
                                            <ul class="dropdown-menu dropdown-menu-right" >
												<li><a href="javascript:;"  onClick="fun_delete({{ $u->id }})"><i class="icon-trash"></i> Delete</a></li>
												<li><a href="{{route('edit_topic',encrypt ($u->id))}}"><i class="icon-pencil"></i> Edit</a></li>
											</ul>
										</li>
									</ul>
									@endif
								</td> 
								<td class="hide"></td>
								<td class="hide"></td>
                                <td class="hide"></td>
                          	</tr>				
						@endforeach								
						</tbody>
					</table>
				</div>

				@php
					$from=($data->currentPage()-1)*($data->perPage())+1;
					$to=$data->currentPage()*$data->perPage();
					$total=$data->total();
				@endphp 

				<div class="col-md-12">
					<div class="col-md-4">
						<span class="float-left">Showing {{ $from }} to {{ $to }} from {{ $total }}</span> 
					</div>
				</div> </br>
				
				{{ $data->links() }}

			</div>
		</div>

		<div  data-backdrop="false" id="modal_mini" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Click on Yes if you want to delete this Record.</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('delete_topic') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del_id" name="del_id" value="">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div  data-backdrop="false" id="modal_2" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Are you want want to change this record?</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('publish_event') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del" name="del" value="">
							<input type="hidden" id="type" name="type" value="">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

@endsection
@section('js')

	<script type="text/javascript">

		function fun_delete(id)
		{
			$('#del_id').val(id);
			$('#modal_mini').modal('toggle');		
		}

		function fun_publish(id,type)
		{
			$('#del').val(id);
			$('#type').val(type);
			$('#modal_2').modal('toggle');	
		}

	</script>
@endsection

