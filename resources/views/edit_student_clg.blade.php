@extends('layouts.main')
@section('js_head')


	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
@include('mapjs')

	
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				topic:{
					required:true
				},
				city:{
					required:true
				},
				status:{
					required:true
				},
				logo:{
					required:true
				},
				payment:{
					required:true
				},
				payment1:{
					number:true
				}
				
			}
		});
	});
</script>
<script>
	
	$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



</script>


		
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('update_student_college') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Student College</h5>
									
								</div>
                                <div class="panel-body">
								<div class="form-group">
										<label class="col-lg-3 control-label">Name</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Enter College Name" name="name" value="{{$data->name}}" required>
										</div>
								</div>
								<div class="form-group">
										<label class="col-lg-3 control-label">Mobile Number</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Enter College Name" name="mobile" value="{{$data->mobile}}" required>
										</div>
								</div>
							<div class="form-group">
									<label class="col-lg-3 control-label">Select Course:</label>
									<div class="col-lg-9 multi-select-full">
										<input type="hidden" value="{{$studentid->id}}" name="id">
										<input type="hidden" value="{{$data->id}}" name="userid">
										<select name="course" data-placeholder="Select Course..."    class="select select-border-color border-warning" required style="width:100%;">
											@if($course[0]!="")
												@foreach ($course as $c)
                                                @if($c)
                                           		
													<option  value="{{$c->id}}" 
														@if($data->courseid==$c->id) selected @endif>
														{{ $c->course_name }}
													</option>
												
                                                    @endif
												@endforeach
												@endif
											
										</select>
									</div>
                                </div>
                             

									<div class="form-group">
										<label class="col-lg-3 control-label">College</label>
										<div class="col-lg-9">
											<select name="collegeid" data-placeholder="Select college..."    class="select select-border-color border-warning" required style="width:100%;">
											@if($college[0]!="")
												@foreach ($college as $col)
                                                @if($c)
                                           		
													<option  value="{{$col->id}}" 
														@if($data->collegeid==$col->id) selected @endif>
														{{ $col->college_name }}
													</option>
												
                                                    @endif
												@endforeach
												@endif
											
										</select>
										</div>
									</div>
									<div class="form-group">
									<label class="col-lg-3 control-label">Stream:</label>
									<div class="col-lg-9 multi-select-full">
										<input type="text" class="form-control" placeholder="Enter year" name="stream" value="{{$data->stream}}" required>
											
										</select>
									</div>
								</div>

								
                                    <div class="form-group">
										<label class="col-lg-3 control-label">Year</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Enter year" name="year" value="{{$data->year}}" required>
										</div>
									</div>
										<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="payment"]').change(function(){
												    if(jQuery(this).val() == 'paid'){
												    	jQuery("#payment1").show();
												    }
												    else
												    {
												    	jQuery("#payment1").hide();
												    }
												});
											});
										</script>
									

								

										<div class="form-group">
										<label class="col-lg-3 control-label" style="top:40px;">
											<div class="fileUpload btn btn-danger fake-shadow">
							                    <span><i class="glyphicon glyphicon-upload"></i> Upload Student Pic</span>
							                    <input id="logo-id" name="student_pic" type="file" class="attachment_upload" >
							                  </div>
							                  
										</label>
										<div class="col-lg-9">
											 <div class="main-img-preview">
											 <img class="thumbnail img-preview" src="{{$data->image}}" style="min-height: 150px;" title="Preview Logo" >
											 <input id="logo-id" name="old_student_pic" value="{{$data->image}}" type="hidden" class="attachment_upload" >
							              </div>
											
										</div>
									</div>

									
									</div>
								
										<br />
										<br />
									<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
									<div class="text-right">
										<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Edit Student College <i class="icon-arrow-right14 position-right"></i></button>
									</div><br>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')
@endsection