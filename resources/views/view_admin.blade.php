@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					@permission('add_admin')
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_admin') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
					@endpermission
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Admins</h3>
					</div>


					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>User Since</th>
								<th>Role</th>
							
								
								<th class="text-center">Actions</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach ( $admins as $admin)
						@php
    $created_at=strtotime($admin->created_at);
    $updated_at=strtotime($admin->updated_at);

@endphp
								<tr>
								<td>{{ $admin->id }}</td>
								<td>{{ $admin->name }}</td>
								<td>{{ $admin->email }}</td>
								<td>{{ date('M d,Y  h:i A',$created_at) }}</td>
								<td>{{ $admin->role }}</td>
								<td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												@permission('delete_admin')
												<li><a data-toggle="modal" data-target="#modal_mini{{ $admin->id }}" href="#"><i class="icon-trash"></i> Delete</a></li>
												@endpermission

												@permission('edit_admin')
												<li><a href="{{ url('edit_admin/'.$admin->id) }}"><i class="icon-pencil"></i> Edit</a></li>
												@endpermission
												
											</ul>
										</li>
									</ul>
								</td>
								<td></td>
								<div  data-backdrop="false" id="modal_mini{{ $admin->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Admin</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this admin.</p>

										</div>

										<div class="modal-footer">
											@permission('delete_admin')
											<form action="{{ url('delete_admin') }}" method="post">
											{{ csrf_field() }}
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<input type="hidden" name="del_id" value="{{ $admin->id }}">
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
											@endpermission
										</div>
									</div>
								</div>
							</div>

							</tr>
													@endforeach
							
							
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>
@endsection
