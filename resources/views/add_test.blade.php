@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>
	
	

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>

	<script src="{{ URL::asset('assets/Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js')}}"></script>
	<script type="text/javascript">
		$(function () {
			$('#datetimepicker1').datetimepicker();
		});
	</script>
	

	{{-- <script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_ckeditor.js') }}"></script>
 --}}



@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('add_test') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Test</h5>									
								</div>

								<div class="panel-body">
									<input type="hidden" id="college_institute"  name="type_institute" value="both" />
							
									<div class="form-group" id="groups_select">
										<label class="col-lg-2 control-label">Select Courses:</label>
										<div class="col-lg-10 multi-select-full">
											<select name="groupid[]" data-placeholder="Select Courses..." multiple="multiple" class="multiselect-select-all-filtering course_id" required>
												@foreach ($new_Courses as $c)
													<option value="{{ $c->id }}">{{ $c->name }}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="form-group" id="groups_select">
										<label class="col-lg-2 control-label">Select Subject:</label>
										<div class="col-lg-10 multi-select-full">
											<select name="subject_id[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning sub_record " required>
                                                <option value="">Select one..</option>                                                      
											</select>
										</div>
									</div>
										

									<div class="form-group">
										<label class="col-lg-2 control-label">Test Name</label>
										<div class="col-lg-10">
											<input type="text" name="test_name" class="form-control" placeholder="Enter Test name" required>
										</div>
									</div>

										
									<div class="form-group">
										<label class="col-lg-2 control-label">Test Description:</label>
										<div class="col-lg-10">
											<textarea  class="summernote" name="description" required></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-2 control-label">Time:</label>
										<div class="col-lg-10">
											<input type="number" name="time" min="0" class="form-control" placeholder="Enter test time in minutes">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-2 control-label">Categories and Questions:</label>
										<div class="input_fields_wrap col-md-10">
											<div class="col-md-12 field_section">
												<div class="col-md-6">
													<input type="text" class="form-control" name="cat[]" placeholder="Enter Category">
												</div>
												<div class="col-md-3">
													<input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions">
												</div>												
											</div>
											<div class="col-md-12 field_section"><div class="col-md-6"><input type="text" class="form-control" name="cat[]" placeholder="Enter Category"></div><div class="col-md-3"><input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions"></div><div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div></div>
										</div>

										<div class="col-md-10 col-md-offset-2"><br>
											<button class="add_field_button btn btn-info btn-sm">Add More Fields</button>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-2 control-label">Test subjects:</label>
										<div class="wrapSubject col-md-10">
											<div class="col-md-12 ">
												<div class="col-md-6">
													<input type="text" class="form-control" name="subject[]" placeholder="Enter Subject" required>
												</div>	
												<div class="col-md-6">
													<button class="addMoreSubject btn btn-info btn-sm">Add More Fields</button>
												</div>										
											</div>
										</div>
									</div>
										
										
									<div class="form-group">
										<label class="col-lg-2 control-label">Posted By:</label>
										<div class="col-lg-10">
											<input type="text" name="postby" class="form-control" placeholder="Enter writer name">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-2 control-label">Rank disclosed time</label>
										<div class="col-lg-10">
											<input type='text'  value="{{ date('m/d/Y H:i a') }}" class="form-control" id='datetimepicker1' name="rank_time" required />
										</div>
                                    </div>

									<div class="form-group">
										<label class="col-lg-2 control-label">Price:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" name="payment" value="free" checked="checked">
												Free
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" name="payment" value="paid">
												Paid
											</label>
										</div>
									</div>
									
									<div class="form-group" style="display:none" id="payment1">
										<label class="col-lg-2 control-label">Amount:</label>
										<div class="col-lg-9">
											<input type="number" class="form-control" name="amount" >
										</div>
									</div>


									<div class="form-group">
										<label class="col-lg-2 control-label">Negative marking:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" name="negative" value="" checked="checked">
												No
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" name="negative" value="marks">
												Yes
											</label>
										</div>
									</div>

									<div class="form-group" style="display:none" id="negitive">
										<label class="col-lg-2 control-label">Negitive Number:</label>
										<div class="col-lg-9">
											<input type="number" class="form-control" name="negitive_number" >
										</div>
									</div>


									<div class="text-right">
										<button id="submit" type="submit" class="btn btn-primary">Add<i class="icon-arrow-right14 position-right"></i></button>
									</div>

									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->


		</div>

@endsection
@section('js')

	<script>

		jQuery(document).ready(function(){
			jQuery('input:radio[name="payment"]').change(function(){
				if(jQuery(this).val() == 'paid'){
					jQuery("#payment1").show();
				}
				else
				{
					jQuery("#payment1").hide();
				}
			});
		});

		jQuery(document).ready(function(){
			jQuery('input:radio[name="negative"]').change(function(){
				if(jQuery(this).val() == 'marks'){
					jQuery("#negitive").show();
				}
				else
				{
					jQuery("#negitive").hide();
				}
			});
		});

		$(document).ready(function(){

			$('.summernote').summernote();

			$("#login_form").validate({

				rules:{
					
					test_name:{
						required:true
					},
					description:{
						required:true
					},
					groupid:{
						required:true
					},
					time:{
						required:true,
						number:true
					},
					postby:{
						required:true
					},
					sub_tests:{
						required:true
					}
				}
			});

			var max_fields      = 4; //maximum input boxes allowed
			var wrapper         = $(".input_fields_wrap"); //Fields wrapper
			var add_button      = $(".add_field_button"); //Add button ID
			var subject_button      = $(".addMoreSubject"); //Add button ID



			
			var x = 1; //initlal text box count
			$(add_button).click(function(e){ //on add input button click
				e.preventDefault();
				if(x <= max_fields){ //max input box allowed
					x++; //text box increment
					$(wrapper).append('<div class="col-md-12 field_section"><div class="col-md-6"><input type="text" class="form-control" name="cat[]" placeholder="Enter Category"></div><div class="col-md-3"><input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions"></div><div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div></div>'); //add input box

					if(x>max_fields)
					{
						$('.add_field_button').hide();
					}

					// $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
				}
				
			});
			
			$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
				e.preventDefault(); $(this).parents('div.field_section').remove(); x--;
				$('.add_field_button').show();
			})


			var removeID=1;

			//Subject function button
			$(subject_button).click(function(e){

				e.preventDefault();
				var subjectAddmore= $(".wrapSubject");

				var fields=	'<div id="subjectID'+removeID+'" class="col-md-12">'+
								'<div class="col-md-6">'+
									'<input type="text" class="form-control" name="subject[]" placeholder="Enter Subject ">'+
								'</div>'+
								'<div class="col-md-6">'+
									'<button class="btn btn-danger btn-sm" onclick=removeSubject('+removeID+')>Remove</button>'+
								'</div>'+
							'</div>';
				
				subjectAddmore.append(fields);
				removeID++;

			});	
		});

		$('select[name="groupid[]"]').change(function(){
			var course= $(this).val();

			// console.log(course_ids);			
			// var jsonString = JSON.stringify(course_ids);

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_subject',   
				data: {course:course},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				} 
			});
		});


		function removeSubject (removeID){
			
			var id='#subjectID'+removeID;
			
			$(id).remove();

		}

	</script>

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>
	

@endsection