@extends('layouts.main')
<link rel="stylesheet" href="{{ URL::asset('assets/Date-Time-Picker-Bootstrap-4/build/css/bootstrap-datetimepicker.min.css')}}">

@section('js_head')
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

	<script src="{{ URL::asset('assets/Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js')}}"></script>
	<script type="text/javascript">
		$(function () {
			$('#datetimepicker1').datetimepicker();
		});
	</script>
@include('mapjs')

<script>
	
	$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



</script>


		
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('update_event') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Event</h5>
									
                                </div>
                                
                                <input type="hidden" name="id" value="{{ $data->id }}" />

								<div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Select @if(Auth::user()->role == 'admin' ) Courses @else Class @endif:</label>
                                        <div class="col-lg-9 multi-select-full">
                                            <select name="course_id" class="bootstrap-select" data-placeholder="Select Course..." required>
                                                <option value="" selected disabled>Select @if(Auth::user()->role == 'admin' ) Courses @else Class @endif..</option>  
                                                @foreach ($course as $c)                                                    
                                                    <option @if($c->id == $data->course_id) selected  @endif value="{{ $c->id }}">{{ $c->name }}</option>                                                        
                                                @endforeach                                                     
                                            </select>                                             
                                        </div>
                                    </div>

                                    <div class="form-group">
										<label class="col-lg-3 control-label">Select Access:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" id="groups_institute" name="access" value="public"  @if($data->access == 'public') checked="checked" @endif>
												Public
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" id="college_institute" name="access" value="private" @if($data->access == 'private') checked="checked" @endif>
												Private
											</label>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Event Name </label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Event Name" value="{{ $data->name }}" name="name" required>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Link</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="link" value="{{ $data->link }}" name="link" required>
										</div>
                                    </div>
									
									@php $time=date('m/d/Y H:i a', $data->start_time);   @endphp
                                    <div class="form-group">
										<label class="col-lg-3 control-label">Start time</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Start time" value="{{ $time }}" id='datetimepicker1'  name="start" required>
										</div>
                                    </div>
                                    
                                    {{--  <div class="form-group">
										<label class="col-lg-3 control-label">End time</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="End time" value="{{ $data->end_time }}" name="end" required>
										</div>
									</div>  --}}

									<div class="form-group">
										<label class="col-lg-2 control-label">Description:</label>
										<div class="col-lg-10">
											<textarea  name="description" class="summernote">{{ $data->description }}</textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label" style="top:40px;">
											<div class="fileUpload btn btn-danger fake-shadow">
							                    <span><i class="glyphicon glyphicon-upload"></i> Upload Course Image</span>
							                    <input id="logo-id" name="banner" type="file" class="attachment_upload" >
							                  </div>
							                  
										</label>
										<div class="col-lg-9">
											 <div class="main-img-preview">
											 <img class="thumbnail img-preview" @if($data->image != '' )  src="{{  URL::asset('img/'.$data->image) }}" @else   src="{{  URL::asset('img/upload-icon.png') }}"  @endif   style="min-height: 150px;" title="Preview Logo" >
							              </div>
																		 <div class="input-group">
							                <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled" style="display:none">
							                <div class="input-group-btn">
							                  
							                </div>
							              </div>
										</div>
									</div>

								</div>
								<br /><br />
								<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Update Event <i class="icon-arrow-right14 position-right"></i></button>
								</div><br>

							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')

	<script type="text/javascript">
		$(document).ready(function() {
		$('.summernote').summernote();
		});
	</script>
@endsection
