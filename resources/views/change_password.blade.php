@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/form_layouts.js') }}></script>

@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-8 col-md-offset-2">

						<!-- Basic layout-->
						<form action="{{ url('change_password') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Change Password</h5>
									
								</div>

								<div class="panel-body">
									<div class="form-group">
										<label class="col-lg-3 control-label">Old Password:</label>
										<div class="col-lg-9">
											<input type="password" name="old_password" class="form-control" placeholder="Enter your old password"> 
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">New Password:</label>
										<div class="col-lg-9">
											<input type="password" name="new_password" class="form-control" placeholder="Enter new password">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Confirm Password:</label>
										<div class="col-lg-9">
											<input type="password" name="confirm_password" class="form-control" placeholder="Re-type new password">
										</div>
									</div>

									<div class="text-right">
										<button type="submit" class="btn btn-primary">Change<i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->

		</div>

@endsection