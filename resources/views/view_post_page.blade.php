@if(!empty($record))
				        {{-- @php
				        	dd($record);
				        @endphp --}}
				        <!--getting the record  from the array value $record-->
				       @foreach ($record as $data)

								@php
							    $json=json_decode($data->jsondata,true);
							    $timestamp=strtotime($data->timestamp);
							
							@endphp
				        	<div class="col-md-12">

						<!-- Blog layout #4 with image -->
						<div class="panel panel-flat">
						    <div class="panel-footer panel-footer-condensed">
							    <div style="float:left; width: 100px;">
							    	<img style="border-radius: 50%;margin-top: 10px; margin-left: 20px;" src="/mechanicalinsider/newsimage/{{ $json['thumbnail'] }}" width="60" height="60">

							    </div>
							    <div style="float: left; width: 400px; margin-top: 15px; margin-left: 10px;">
							    	<span style="font-size: 16px;"><strong>{{ $data->posttype }}</strong></span><br>
							    	<span>{{ $json['writer'] }}</span>
							    </div>
							    <div style="float: right; margin-right: 20px; margin-top: -45px;">
							    <span>{{ date('M d, Y',$timestamp) }}</span><br>
							    <span><strong>Price:</strong></span>
							    	@if ($json['payment']=="free")
							    		<strong>Free</strong>
							    	@else
							    		<strong>&#8377; {{ $json['payment'] }}</strong>
							    	@endif
							    </div>
						    
							</div>
							<div class="thumb content-group">
									<img style="height: 300px;" src="/mechanicalinsider/newsimage/{{ $json['pic'] }}" alt="" class="">
									<div class="caption-overflow">
										<span>
											<a href="{{ url('post/'.$data->id) }}" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
										</span>
									</div>
							</div>
							<div  style="max-height: 160px;min-height: 160px; overflow: hidden; text-overflow: ellipsis;" class="panel-body">
								

								<h5 class="text-semibold mb-5">
									<a href="{{ url('post/'.$data->id) }}" class="text-default">{{ $json['title'] }}</a>
								</h5>


								<div>{!! $data->post_description !!}</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="panel-footer panel-footer-transparent">
								<div class="heading-elements">
									<ul class="list-inline list-inline-separate heading-text">
										<li><a class="comment" id="comment{{ $data->id }}" href="" class="text-muted">{{$data->comment}} comments</a></li>
										<li><a href="#" class="text-default"><i class="icon-eye2 text-pink position-left"></i> {{ $data->view }}</a></li>
										@permission('send_post_notification')
										@if($data->notification=="sent")
										<li><a data-toggle="modal" data-target="#notification{{ $data->id }}" class="text-default"><i class="icon-bell-check text-pink position-left"></i></a></li>
										@else
										<li><a href="{{ url('send_notification/'.$data->id) }}" class="text-default"><i class="icon-bell2 text-pink position-left"></i></a></li>
										@endif
										@endpermission

										<li><a href="#" class="text-default"><i class="icon-heart6 text-pink position-left"></i> {{ $data->likes }}</a></li>

										@permission('edit_post')<li><a href="{{ url('edit_post/'.$data->id) }}" class="text-muted"><i class="icon-pencil text-size-base text-pink position-left"></i></a></li> @endpermission


										@permission('delete_post')	<li><a data-toggle="modal" data-target="#modal_mini{{ $data->id }}" class="text-muted"><i class="icon-trash text-size-base text-pink position-left"></i></a></li> @endpermission

									</ul>
									{{-- notificaion model --}}
										<div  data-backdrop="false" id="notification{{ $data->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header bg-warning">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Warning</h5>
										</div>

										<div class="modal-body">
											<p style="font-size: 14px;">This post is already notifyed.
												If you want again then click Yes.</p>

										</div>

										<div class="modal-footer">
											
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<a href="{{  url('send_notification/'.$data->id) }}"><button type="button" class="btn btn-primary bg-warning">Yes</button></a>
										</div>
									</div>
									
								</div>
							</div>
									{{-- notificaion model --}}


									<div  data-backdrop="false" id="modal_mini{{ $data->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Post</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this Post.</p>

										</div>

										<div class="modal-footer">
										@permission('delete_post')
											<form action="{{ url('delete_post') }}" method="post">
											{{ csrf_field() }}
											<input type="hidden" name="del_id" value="{{ $data->id }}">
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										@endpermission
										</div>
									</div>
									
								</div>
							</div>

								@permission('view_post')	<a href="{{ url('post/'.$data->id) }}" class="heading-text pull-right">Full article <i class="icon-circle-right2 position-right"></i></a>@endpermission
								</div>
							
						<!-- /blog layout #4 with image -->
						
							@if (isset($data->comments))
				                   <div style="background:#edeaea; padding: 10px; margin-bottom: -11px; display: none;" class="comment-box col-md-12 comment{{ $data->id }}">
				                   @php
				                   	$count=0;
				                   	$comment_count=count($data->comments);
				                   @endphp
				                    @foreach ($data->comments as $comment)
				                    	@php
				                    	$timestamp1=$comment->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($comment->jsondata,true); 
				                    	@endphp	
				                    <div class=" col-md-12" style="padding: 0px; margin-bottom: 20px;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    	@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="35" height="35"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="35" height="35"></div>
				                    	@endif
				                    		
				                    		<div class="col-md-6"><span style="font-weight: bold;">{{ $com['name'] }}</span><br>
				                    		<span style="margin-top: 5px;">{{ $com['collage'] }}</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 10px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
			                    		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			                    			<p>{{ $com['comments'] }}</p>
			                    			@if ($com['com_image']!="null")
			                    			<img src="/mechanicalinsider/{{$com['com_image']}}" width="150" height="100">
			                    			@endif
			                    		</div>
			                    		@if (isset($comment->reply))
			                    		@php
			                    		$reply_count=count($comment->reply);
			                    		@endphp
			                    		<div id="comment_{{ $comment->id }}" class="reply_section_default" style="padding-left:60px;"">
				                    		<p class="pull-left"><a id="show_reply">{{ $reply_count }} replies</a>
				                    		<a style="display: none;" id="hide_reply">Hide {{ $reply_count }} replies</a></p>
				                    	
			                    		@foreach ($comment->reply as $reply)
			                    		@php
				                    	$timestamp1=$reply->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($reply->jsondata,true);
				                    		// print_r($reply); 
				                    	@endphp
				                    		
			                    			<div class="col-md-12 reply_section" style="; margin-bottom: 20px; display:none;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    	@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="25" height="25"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="25" height="25"></div>
				                    	@endif
				                    		
				                    		<div class="col-md-6"><span style="font-weight: bold; font-size: 11px;">{{ $com['name'] }}</span><br>
				                    		<span style=" font-size: 11px;" >{{ $com['collage'] }}</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 10px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
			                    		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			                    			<p>{{ $com['comment'] }}</p>
			                    			@if ($com['com_image']!="null")
			                    			<img src="/mechanicalinsider/{{$com['com_image']}}" width="100" height="50">
			                    			@endif
			                    		</div>
			                    		@permission('delete_comment')
			                    		<div class="col-md-10" style="margin-left: -10px;">
			                    			<a><span alt="{{ $reply->commentid }}" id='{{ $reply->id }}' class='delete_reply'>Delete</span></a>
			                    		</div>
			                    		@endpermission
				                    </div>
			                    		@endforeach
			                    		</div>
			                    		@endif
			                    		@permission('delete_comment')
			                    		<div class="col-md-10" style="margin-left: -10px;">
			                    			<a><span alt="{{ $data->id }}" id='{{ $comment->id }}' class='delete_comment'>Delete</span></a>
			                    		</div>
			                    		@endpermission
				                    </div>
				                    @php
				                    $count++;
				                    if($count==5&&$comment_count>$count)
				                    {
				                    	echo "<a><span alt='$data->field_type' id='$data->id' class='view_more_comment'>view more comments</span></a>";				          
				                    	break;
				                    }
				                    @endphp

				                    @endforeach
				                    </div>
				                    @endif
					</div>
					</div>
					</div>


				        @endforeach
				        @endif