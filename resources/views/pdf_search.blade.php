@if(!empty($query))

@foreach($query as $q)
						<div style="display:none">{{ $json=$q->jsondata  }} </div>
						@php

						$json = json_decode($json,true);
						$writer=$json['writer'];
						$title=$json['title'];
						$link=$json['link'];
						$type=$json['type'];
					@endphp

						<!-- Blog layout #1 with image -->
						<div class="panel panel-flat blog-horizontal blog-horizontal-1">
							<div class="panel-heading" style="padding:12px 0px 5px 4px !important;">
								<div class="row">
								<div class="col-md-2" >
									<img src="{{ url('img/reunion.png') }}" style="height:45px; float:right;">
								</div>
								<div class="col-md-10" style="margin-left:-5px;">
								
								<h5 class="panel-title text-semibold" style="font-size:15px;"><b>Useful Stuff</b>
									<p style="font-size:12px;">By &nbsp; : &nbsp; {{ $writer }}</p>
									
								</h5>
								</div>
							
							</div>
							</div>
							<hr style="margin:0px;">
							<div class="row panel-body" style="padding:12px 20px;">
								<div class="col-md-4 thumb" style="width:100px;">
									<img style="width:100px; height:auto;" src="/img/pdf1.png" class="" alt="">
									
								</div>

								<div class="col-md-8 blog-preview">
									<h5 style="font-size:15px;"><b>@php  echo $json['title']; @endphp
										</b>
										<br>
										</h5>
										
									<p><a href="{{$link}}" style="border-color: #5c63b0; color: #5c63b0; padding:5px 12px 5px 12px;"  class="btn border-slate btn-flat legitRipple" target="_blank">View Pdf</a></p>
									
									
								</div>
							</div>

							<div class="panel-footer panel-footer-transparent">
								<div class="heading-elements">
									<ul class="list-inline list-inline-separate heading-text text-muted">
										
										
									
										<li><a class="comment" id="comment{{ $q->id }}" href="" class="text-muted">{{$q->comment}} comments</a></li>
										<li><a href="#" class="text-muted"><i class="icon-heart6 text-size-base text-pink position-left"></i></a></li>
										@permission('send_pdf_notification')
										@if($q->notification=="sent")
										<li><a data-toggle="modal" data-target="#notification{{ $q->id }}" class="text-default"><i class="icon-bell-check text-pink position-left"></i></a></li>
										@else
										<li><a href="{{ url('send_pdf_notification/'.$q->id) }}" class="text-default"><i class="icon-bell2 text-pink position-left"></i></a></li>
										@endif
										@endpermission
									

										@permission('edit_pdf')
										<li><a data-toggle="modal" data-target="#edit{{ $q->id }}" href="#" class="text-muted"><i class="icon-pencil text-size-base text-pink position-left"></i></a></li>
										 @endpermission


									     @permission('delete_pdf')
									     <li><a data-toggle="modal" data-target="#modal_mini{{ $q->id }}" href="#" class="text-muted"><i class="icon-trash text-size-base text-pink position-left"></i></a></li>
									       @endpermission 
										</ul>
										
										
								</div>
								@if (isset($q->comments))
				                   <div style="background:#edeaea; padding: 10px; margin-bottom: -11px; display: none;" class="comment-box col-md-12 comment{{ $q->id }}">
				                   @php
				                   	$count=0;
				                   	$comment_count=count($q->comments);
				                   @endphp
				                    @foreach ($q->comments as $comment)
				                    	@php
				                    	$timestamp1=$comment->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($comment->jsondata,true); 
				                    	@endphp	
				                    <div class=" col-md-12" style="padding: 0px; margin-bottom: 20px;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    	@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="35" height="35"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="35" height="35"></div>
				                    	@endif
				                    		
				                    		<div class="col-md-6"><span style="font-weight: bold;">{{ $com['name'] }}</span><br>
				                    		<span style="margin-top: 5px;">{{ $com['collage'] }}</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 10px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
			                    		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			                    			<p>{{ $com['comments'] }}</p>
			                    			@if ($com['com_image']!="null")
			                    			<img src="/mechanicalinsider/{{$com['com_image']}}" width="150" height="100">
			                    			@endif
			                    		</div>
			                    		@if (isset($comment->reply))
			                    		@php
			                    		$reply_count=count($comment->reply);
			                    		@endphp
			                    		<div id="comment_{{ $comment->id }}" class="reply_section_default" style="padding-left:60px;"">
				                    		<p class="pull-left"><a id="show_reply">{{ $reply_count }} replies</a>
				                    		<a style="display: none;" id="hide_reply">Hide {{ $reply_count }} replies</a></p>
				                    	
			                    		@foreach ($comment->reply as $reply)
			                    		@php
				                    	$timestamp1=$reply->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($reply->jsondata,true);
				                    		// print_r($reply); 
				                    	@endphp
				                    		
			                    			<div class="col-md-12 reply_section" style="; margin-bottom: 20px; display:none;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    	@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="25" height="25"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="25" height="25"></div>
				                    	@endif
				                    		
				                    		<div class="col-md-6"><span style="font-weight: bold; font-size: 11px;">{{ $com['name'] }}</span><br>
				                    		<span style=" font-size: 11px;" >{{ $com['collage'] }}</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 10px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
			                    		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			                    			<p>{{ $com['comment'] }}</p>
			                    			@if ($com['com_image']!="null")
			                    			<img src="/mechanicalinsider/{{$com['com_image']}}" width="100" height="50">
			                    			@endif
			                    		</div>
			                    		<div class="col-md-10" style="margin-left: -10px;">
											@permission('delete_comment')
											<a><span alt="{{ $reply->commentid }}" id='{{ $reply->id }}' class='delete_reply'>Delete</span></a>
											@endpermission
										</div>
									</div>
										@endforeach
										</div>
										@endif
										<div class="col-md-10" style="margin-left: -10px;">
											@permission('delete_comment')
											<a><span alt='{{ $q->id }}' id='{{ $comment->id }}' class='delete_comment'>Delete</span></a>
											@endpermission
										</div>
									</div>
				                    @php
				                    $count++;
				                    if($count==5&&$comment_count>$count)
				                    {
				                    	echo "<a><span alt='$q->field_type' id='$q->id' class='view_more_comment'>view more comments</span></a>";				          
				                    	break;
				                    }
				                    @endphp

				                    @endforeach
				                    </div>
				                    @endif
								
								{{-- notificaion model --}}
										<div  data-backdrop="false" id="notification{{ $q->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header bg-warning">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Warning</h5>
										</div>

										<div class="modal-body">
											<p style="font-size: 14px;">This post is already notifyed.
												If you want again then click Yes.</p>

										</div>

										<div class="modal-footer">
											
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<a href="{{  url('send_pdf_notification/'.$q->id) }}"><button type="button" class="btn btn-primary bg-warning">Yes</button></a>
											</form>
										</div>
									</div>
									
								</div>
							</div>
									{{-- notificaion model --}}
							<div  data-backdrop="false" id="modal_mini{{ $q->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Pdf Data</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this Pdf.</p>

										</div>

										<div class="modal-footer">
											<form action="{{ url('delete_pdf') }}" method="post">
											{{ csrf_field() }}
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<input type="hidden" id="current_id" name="del_id" value="{{ $q->id }}">
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										</div>
									</div>
								</div>
							</div>
							
							
							
							<div  data-backdrop="false" id="edit{{ $q->id }}" class="modal fade">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Edit Pdf Data</h5>
										</div>

										<div class="modal-body">
						<form id="login_form" action="{{ url('pdf_update') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" value="{{$q->id}}" name="id">
						<input type="hidden" value="{{$link}}" name="link_existing">
							<div class="panel panel-flat">
								<div class="panel-heading">
									
								</div>

								<div class="panel-body">
									
										<div class="form-group">
										<label class=" control-label">Select Groups:</label>
										<div class=" multi-select-full">
										@php
											$groups=explode(",", $q->groupid);
											@endphp
												<select name="groupid[]" data-placeholder="Select groups..." multiple="multiple" class="multiselect-select-all-filtering">
												@if($data[0]!="")
												@foreach ($data as $d)
													<option value="{{ $d->id }}" @if($q->groupid=="all")
											selected 
											@elseif (in_array($d->id, $groups))
												selected 
											@endif>{{ $d->topic }}</option>
												@endforeach
												@endif
												
												</select>
											</div>
									</div>
										<div class="form-group">
											<label class="control-label">Writer:</label>
											<div class="">
												<input type="text" name="writer" class="form-control" value="{{$writer}}" placeholder="Enter name" required autofocus> 
											</div>
										</div>

										<div class="form-group">
											<label class=" control-label">Title:</label>
											<div class="">
												<input type="text" name="title" class="form-control" value="{{$title}}" placeholder="Enter Title">
											</div>
										</div>
										
										<div class="form-group" id="mySelect">
											<label class="display-block text-semibold">Select Type</label>
											<div class="">
									
										
											
											@if($type=="pdf")
										
										
												<label class="radio-inline">
													<input type="radio" name="type" value="link" onclick="change_div(10)">
													Link
												</label>
												<label class="radio-inline">
													<input type="radio"  name="type" value="pdf" onclick="change_div(20)" checked>
													Pdf
												</label>
												
											
											@else
											
												<label class="radio-inline">
													<input type="radio" name="type" value="link" onclick="change_div(10)" checked>
													Link
												</label>
												<label class="radio-inline">
													<input type="radio"   name="type" onclick="change_div(20)"  value="pdf">
													Pdf
												</label>
												
											
										@endif
											
											
											</div>
										</div>
										
										
										@if($type=="pdf")
										
											<div class="form-group link"  style="display:none;">
											<label class=" control-label">Enter Link :</label>
											<div class="">
												<input id="link" type="text" class="form-control"  placeholder="Enter Link" name="link">
											</div>
										</div>
										
										@else
											<div class="form-group link">
											<label class=" control-label">Enter Link :</label>
											<div class="">
												<input id="link" type="text" class="form-control" value="{{$link}}" placeholder="Enter Link" name="link">
											</div>
										</div>
										
										
										@endif
										
											
										
								
								
										@if($type=="pdf")
										
											<div class="form-group pdf">
										@else
										
											<div class="form-group pdf"   style="display:none;" >
										
										@endif
											<label class="control-label">Enter Pdf:</label>
											<div class="">
												<input type="file" class="form-control" name="pdf">
											</div>
										</div>
										
									
								</div>
							</div>
						

										</div>

										<div class="modal-footer">
										
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
									
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										</div>
									</div>
								</div>
							</div>
							
							
							
							
								
							</div>
			
						<!-- /blog layout #1 with image -->

					</div>
		
				        
				        
				 @endforeach
				 @endif