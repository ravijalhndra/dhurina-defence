@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>
	
	{{-- <script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script> --}}

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/components_popups.js') }}"></script>

	{{-- <script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_ckeditor.js') }}"></script>
 --}}

<style type="text/css">
	.errorTxt{
  border: 1px solid red;
  min-height: 20px;
}
</style>

@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">
					@php
					// print_r($tests_ques);
					// dd($tests_ques);
					
						for ($i=0; $i <count($test_name) ; $i++) { 
							$test = $test_name[$i];
							for ($j=0; $j <count($tests_ques) ; $j++) { 
								// echo $test;
								 $total_q=$tests_ques[$i][$test];

							}
							$answered=$question_entered[$test];
							$required=$total_q-$answered;
							if($required>0){
								$disable="false";
								$statement= "Enter ".$test." question";
							break;
							}
							else
							{
								$statement="You have entered all questions in this test.";
								$disable="true";
							}
						}
						if($required<1)
						{
							echo "<script>
									$(window).load(function()
									{
										
	    								$('#myModal').modal('show');
										
									});
								</script>";
						}
						
					@endphp

					
								<div class="modal fade" id="myModal">
									<div class="modal-dialog modal-sm">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title">Alert</h4>
											</div>
											<div class="modal-body">
												You have added all questions in this test.
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
												<a href="{{ url('publish_test/'.$test_id) }}" class="btn btn-primary">Publish</a>
											</div>
										</div>
									</div>
								</div>
					
						<!-- Basic layout-->
						<form id="login_form" action="{{ url('add_questions') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Questions</h5><br>
									
									<input type="hidden" name="sub_test_name" value="{{ $test }}">
									<input type="hidden" name="test_id" value="{{ $test_id }}">
									<input type="hidden" name="test_name" value="{{ $tests_name }}">
								</div>

								<div class="panel-body">
									<div class="col-md-10 col-md-offset-1">
										<div class="form-group">

										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Select Subject:</strong></label>
											<div class="col-lg-6">
												<select id="subjectQuestion" name="subjectQuestion" data-placeholder="choose options" class="select select-border-color border-warning" required>
													<option value="">Select one.</option>
													@foreach ($subjectQuestion as $key=>$value)
														<option value="{{ $value }}">{{ ucfirst($value) }}</option>	
													@endforeach			
												</select>
											</div>
										</div>

										<label class="col-lg-2 control-label"><strong>Subject:</strong></label>
											<div class="col-lg-10">
												<input style="background: #828c96; font-size: 18px;" type="text" name="subject" value="{{ $statement }}" class="form-control" placeholder="" readonly>
											</div>
										</div>


										<script type="text/javascript">
										$(document).ready(function() {
										  $('.new_summernote').summernote({
											  toolbar: [
											    // [groupName, [list of button]]
											    ['style', ['bold', 'italic', 'underline', 'clear']],
											    ['font', ['strikethrough', 'superscript', 'subscript']],
											    ['fontsize', ['fontsize']],
											    ['color', ['color']],
											    ['para', ['ul', 'ol', 'paragraph']],
											    ['height', ['height']]
											  ]
											});
										});
										</script>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Question:</strong></label>
											<div class="col-lg-10">
												<textarea id="question" class="mygroup new_summernote" name="question" ></textarea>
												<span class="erroTxt"></span>
												<input id="question_image" type="file" name="question_image" class="mygroup">
												<span class="erroTxt"></span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Hint(question part2):</strong></label>
											<div class="col-lg-10">
												<textarea id="hint"  class="new_summernote" name="hint" ></textarea>
												<input id="hint_image" type="file" name="hint_image">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option A:</strong></label>
											<div class="col-lg-6">
												<input type="text" id="option_a" name="option_a" class="form-control" placeholder="" >
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input type="file" id="option_a_image" name="option_a_image" class="form-control" >
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option B:</strong></label>
											<div class="col-lg-6">
												<input type="text" id="option_b" name="option_b" class="form-control" placeholder="">
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input type="file" id="option_b_image" name="option_b_image" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option C:</strong></label>
											<div class="col-lg-6">
												<input id="" type="text" id="option_c" name="option_c" class="form-control" placeholder="" >
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input id="option_c_image" type="file" id="option_c_image" name="option_c_image" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option D:</strong></label>
											<div class="col-lg-6">
												<input id="option_d" type="text" name="option_d" class="form-control" placeholder="" >
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input id="option_d_image" type="file" name="option_d_image" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option E:</strong></label>
											<div class="col-lg-6">
												<input id="option_e" type="text" name="option_e" class="form-control" placeholder="">
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input id="option_e_image" type="file" name="option_e_image" class="form-control">
											</div>
										</div>
										{{-- <div class="form-group">
											<label class="col-lg-2 control-label"><strong>Answer:</strong></label>
											<div class="col-lg-6">
												<input id="answer" type="text" name="answer" class="form-control" placeholder="">
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input id="answer_image" type="file" name="answer_image" class="form-control">
											</div>
										</div> --}}


										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Answer:</strong></label>
											<div class="col-lg-6">
												<select id="answer" name="answer" data-placeholder="choose options" class="select select-border-color border-warning" required>
															<option value="">Select answer</option>
															<option value="a">Option A</option>
															<option value="b">Option B</option>
															<option id="c" value="c">Option C</option>
															<option id="d" value="d" hidden>Option D</option>
															<option id="e" value="e">Option E</option>
												</select>
											</div>
										</div>

										
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Solution:</strong></label>
											<div class="col-lg-10">
												<textarea id="solution"  class="new_summernote" name="solution" ></textarea>
												<input id="solution_image" type="file" name="solution_image">
											</div>
										</div>

										{{-- <div class="form-group">
												<div class="col-lg-4">
													<label>Show Solution to users?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="ans_status" checked type="checkbox" class="switchery" value="yes">
													</label>
												</div>
											</div> --}}


										<!-- <div class="form-group">
											<label class="col-lg-2 control-label"><strong>Solution:</strong></label>
											<div class="col-lg-10">
												<textarea id="solution"  class="new_summernote" name="solution" required></textarea>
												<input id="solution_image" type="file" name="solution_image">
											</div>
										</div> -->

										<!-- <div class="form-group">
											<label class="col-lg-2 control-label">Add Time(In Second):</label>
											<div class="col-lg-10">
												<input type="number" name="question_time" class="form-control" min="10">
											</div>
										</div>-->
										

										<div class="text-right">
											<span @if ($disable=="true")
													data-popup="tooltip" title="you have entered all question in this test.!"
												@endif>
												<button @if ($disable=="true")
													disabled data-popup="tooltip" title="you have entered all question in this test.!"
												@endif id="submit" type="submit" class="btn btn-primary">Add<i class="icon-arrow-right14 position-right"></i></button>
											</span>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->


		</div>

@endsection
@section('js')
{{-- for switch buttons --}}
	{{-- <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script> --}}
	{{-- for switch buttons --}}
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules: {
				question: {
	                required:"#question_image:blank"
	            },
	            question_image: {
	                required:"#question:blank"
	            },
	            option_a: {
	                required:"#option_a_image:blank"
	            },
	            option_a_image: {
	                required:"#option_a:blank"
	            },
	            option_b: {
	                required:"#option_b_image:blank"
	            },
	            option_b_image: {
	                required:"#option_b:blank"
	            },
	            // option_c: {
	            //     required:"#option_c_image:blank"
	            // },
	            // option_c_image: {
	            //     required:"#option_c:blank"
	            // },
	            // option_d: {
	            //     required:"#option_d_image:blank"
	            // },
	            // option_d_image: {
	            //     required:"#option_d:blank"
	            // },
	            // option_e: {
	            //     required:"#option_e_image:blank"
	            // },
	            // option_e_image: {
	            //     required:"#option_e:blank"
				// },
				
	            //answer: {
	            //    required:true
	            //},
	            //option_a: {
	            //    required:true
	            //},
				//option_b: {
	            //    required:true
	            /*},
				option_c: {
	                required:true
	            },
				option_d: {
	                required:true
				}, */
				
	            // hint: {
	            //     required:"#hint_image:blank"
	            // },
	            // hint_image: {
	            //     required:"#hint:blank"
	            // },
	            // solution: {
	            //     required:"#solution_image:blank"
	            // },
	            // solution_image: {
	            //     required:"#solution:blank"
	            // }
        },
        messages:{},
        errorElement : 'span',
    	errorLabelContainer: '.errorTxt'
        
		});
	});
</script>

@endsection