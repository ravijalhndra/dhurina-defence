@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('Assign-Course') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">Assign Ebook & Notes List</h3>

						<div class="row" style="padding-top:50px;">
							<div class="col-md-12">
								<form action="" method="GET">									
									<div class="col-md-12">
										<div class="col-md-3"> 
											<label>	<input type="search" class="form-control" name="filter" value="{{ $filter }}" placeholder="Type to filter..."></label>
										</div>
										<div class="col-md-3">
										 <select name="type" class="form-control" required>
										    <option value="">Select Type</option>
										 	<option value="notes"  @if($type=='notes') selected @endif >Notes</option>
											<option value="ebook" @if($type=='ebook') selected @endif>Ebook</option>
										</select>
										</div>
										<div class="col-md-6">
											<input type="submit" class="fab-menu-btn btn bg-success" value="search"> 											
										</div>	
									</div>

								</form>						
							</div>
						</div>
					</div>
				
					<table class="table">
						<thead>
							<tr>
                                <th>ID</th>
                                <th>User Name</th>
                                <th>Mobile</th>
								<th>Course name</th>
								<th>Referral code</th>
                                <th>Action</th>
							</tr>
						</thead>
						<tbody>
						@foreach($data as $u)
							<tr>
								<td>{{$u->assess_id}}</td>								
								<td>{{$u->name}}</td>
                                <td>{{$u->mobile}}</td> 
								<td>{{$u->course_name}}</td>    
								<td>{{$u->ref_code}}</td>                  
                                <td >
									<a href="javascript:;"  onClick="fun_delete({{ $u->assess_id }})"><i class="icon-trash"></i> Delete</a>
                                </td> 
                          	</tr>				
						@endforeach								
						</tbody>
					</table>
				</div>

				@php
					$from=($data->currentPage()-1)*($data->perPage())+1;
					$to=$data->currentPage()*$data->perPage();
					$total=$data->total();
				@endphp 

				<div class="col-md-12">
					<div class="col-md-4">
						<span class="float-left">Showing {{ $from }} to {{ $to }} from {{ $total }}</span> 
					</div>
				</div> </br>

				{{ $data->links() }}
			</div>
		</div>

		<div  data-backdrop="false" id="modal_mini" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Click on Yes if you want to delete this Record.</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('delete_assign') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del_id" name="del_id" value="">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div  data-backdrop="false" id="modal_2" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Are you want want to change this record?</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('publish_video') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del" name="del" value="">
							<input type="hidden" id="type" name="type" value="">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

@endsection
@section('js')

	<script type="text/javascript">

		function fun_delete(id)
		{
			$('#del_id').val(id);
			$('#modal_mini').modal('toggle');		
		}

		function fun_publish(id,type)
		{
			$('#del').val(id);
			$('#type').val(type);
			$('#modal_2').modal('toggle');	
		}

	</script>
@endsection

