@extends('layouts.main')
@section('js_head')
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

                <!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('add_reply') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}

                        <input id="id" name="id" type="hidden" value="{{$comment->id}}" />
                        <input id="type" name="type" type="hidden" value="{{$type}}" />

                        @if($type == 'post')
                            @php $json=json_decode($comment->jsondata); @endphp
                        @endif
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Comment Reply</h5>									
								</div>

								<div class="panel-body">
                                    <div class="form-group">
										<label class="col-lg-2 control-label">Comment </label>
										<div class="col-lg-10">
                                            <label class="control-label">@if($type == 'video') {{ $comment->comment }}  @else {{ $json->comments }}  @endif </label>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-2 control-label">Reply </label>
										<div class="col-lg-10">
											<input type="text" name="reply" value=""  class="form-control" placeholder="Enter reply">
										</div>
									</div>									

									<div class="text-right">
										<button id="submit" type="submit" class="btn btn-primary">Reply<i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>							
						</form>
                    </div>
				</div>	
                
                <!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Reply</h3>							
					</div>				

					<table class="table">
						<thead>
							<tr>				
                                <th>Reply</th>                               
                                <th>Action</th>
							</tr>
						</thead>
						<tbody>
						@foreach($reply as $u)
							<tr>
                                <td>{{ $u->reply }}</td>
                                <td class="text-center">										
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
											<ul class="dropdown-menu dropdown-menu-right" >												
											
												<li><a href="javascript:;"  onClick="fun_delete({{ $u->id }},'$type')"><i class="icon-trash"></i> Delete</a></li>													
												
											</ul>
										</li>
									</ul>									
								</td> 					
                                
                          	</tr>				
						@endforeach								
						</tbody>
					</table>
				</div>
				
                {{ $reply->links() }}
                


			</div>
		</div>

        <div  data-backdrop="false" id="modal_mini" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Click on Yes if you want to delete this Record.</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('delete_reply_comment') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del_id" name="del_id" value="">
                            <input type="hidden" id="type" name="type" value="">

						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>
@endsection
@section('js')

	<script type="text/javascript">		

		function fun_delete(id,type)
		{	
			var val = [];
			val.push(id);
			$('#del_id').val(val);
            $('#type').val(type);
			$('#modal_mini').modal('toggle');		
		}

		function fun_publish(id,type)
		{
			$('#del').val(id);
			$('#type').val(type);
			$('#modal_2').modal('toggle');	
		}

	</script>
@endsection

