@extends('layouts.main')
@section('js_head')
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">
@include('mapjs')

		
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('update_user') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
                                <input type="hidden" name="id" value="{{ $data->id }}" />
                            <div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit User</h5>									
								</div>

								<div class="panel-body">
									
									<div class="form-group">
										<label class="col-lg-3 control-label"> User name</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" value="{{ $data->name }}" readonly placeholder="User Name" name="name" >
										</div>
                                    </div>

                                    <div class="form-group">
										<label class="col-lg-3 control-label"> Email</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" value="{{ $data->email }}" readonly placeholder="Email" name="email" >
										</div>
                                    </div>

                                    <div class="form-group">
										<label class="col-lg-3 control-label"> Mobile number</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" value="{{ $data->mobile }}" readonly placeholder="Mobile number" name="mobile" >
										</div>
                                    </div>
                                    
									<div class="form-group">
										<label class="col-lg-3 control-label">Ref Code </label>
										<div class="col-lg-9">
											<input type="text" class="form-control" value="{{ $data->referralcode }}" placeholder="Ref code" name="ref_code" >
										</div>
									</div>
									
								</div>
								<br /><br />
								<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Update User <i class="icon-arrow-right14 position-right"></i></button>
								</div><br>

							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')

	<script type="text/javascript">
		$(document).ready(function() {
		    $('.summernote').summernote();
		});
	</script>
@endsection
