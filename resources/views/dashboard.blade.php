@extends('layouts.main')
@section('js_head')
	
	<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

@endsection
@section('content')
	<div class="page-content">

    @if(Auth::user()->role == 'admin' )
			<div id="container" >
        
      </div>
    @else 

    
    @endif
			

		</div>
@endsection
@section('js')
<script>
    $(document).ready(function(){
              
        var path = {!! json_encode(url('/')) !!};
        $.ajax({
          url:""+path+"/chart_data",
          type:"POST",
          data:{ "_token": "{{ csrf_token() }}"},
          success:function(d){
            console.log(d);
                $('div#container').html(d);
          }
        });
    });
</script>

@endsection
