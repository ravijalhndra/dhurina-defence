@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_department') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Department</h3>
					</div>

                       
					<table id="datatable" class="table datatable-basic">
						<thead>
							<tr>
                                <th>ID</th>
                                <th>Department Name</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
					   
						@foreach ( $data as $d)
						    <tr>
								<td>{{ $d->id }}</td>
								<td>{{ $d->dept_name}}</td>
							    <td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
                                            <ul class="dropdown-menu dropdown-menu-right">
												<li><a data-toggle="modal" data-target="#modal_mini{{ $d->id }}" href="#"><i class="icon-trash"></i> Delete</a></li>
												<li><a href="{{ url('edit_department/'.$d->id) }}"><i class="icon-pencil"></i> Edit</a></li>
											</ul>
										</li>
									</ul>
								</td> 
                                
                                <div  data-backdrop="false" id="modal_mini{{ $d->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete College</h5>
										</div>
                                        <div class="modal-body">
											<p>Click on Yes if you want to delete this college.</p>
                                        </div>
                                        <div class="modal-footer">
											<form action="{{ url('delete_department') }}" method="post">
											{{ csrf_field() }}
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<input type="hidden" name="del_id" value="{{ $d->id }}">
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										</div>
									</div>
								</div>
							</div>
							
							</tr>
						@endforeach
							
							
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>
		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
       $(document).ready(function() {
    $('#datatable').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>
@endsection
