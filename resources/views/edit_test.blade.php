@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>
	
	{{-- <script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script> --}}

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>

	<script src="{{ URL::asset('assets/Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js')}}"></script>
	<script type="text/javascript">
		$(function () {
			$('#datetimepicker1').datetimepicker();
		});
	</script>
	
	{{-- <script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_ckeditor.js') }}"></script>
 --}}



@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">
					@php
						$json=json_decode($record->jsondata,true);
						$sub_tests=json_decode($record->post_description,true);
					@endphp

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('update_test') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}

						<input type="hidden" value="{{$record->id}}" name="id">

							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Test</h5>									
								</div>

								<div class="panel-body">
							
								<div class="form-group" id="groups_select">
									<label class="col-lg-2 control-label">Select Courses:</label>
									<div class="col-lg-10  multi-select-full">
										<select name="groupid[]" data-placeholder="Select Courses..." multiple="multiple" class="multiselect-select-all-filtering course_id" required autofocus>
											@foreach ($new_Courses as $c)
												<option @if($record->groupid == 'all') selected @else  @if(in_array($c->id,$record->course_id)) selected @endif @endif      value="{{ $c->id }}">{{ $c->name }}</option>
											@endforeach												
										</select>
									</div>
								</div>

								
								@php if($record->school) {   $sub_id=json_decode($record->school); } else { $sub_id=[];} @endphp 

								<div class="form-group" id="groups_select">
									<label class="col-lg-2 control-label">Select Subject:</label>
									<div class="col-lg-10 multi-select-full">
										<select name="subject_id[]" data-placeholder="Select Subject..." multiple="multiple" class="multiselect-select-all-filtering sub_record" required>
											@foreach ($subject as $s)
												<option @if(in_array ($s->id,$sub_id) ) selected   @endif  value="{{ $s->id }}">{{ $s->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
										

								<div class="form-group">
									<label class="col-lg-2 control-label">Test Name</label>
									<div class="col-lg-10">
										<input type="text" name="test_name" class="form-control" value="{{ $json['testname'] }}" placeholder="Enter Test name" required>
									</div>
								</div>
										
								<div class="form-group">
									<label class="col-lg-2 control-label">Test Description:</label>
									<div class="col-lg-10">
										<textarea  class="summernote" name="description" required>{{ $json['description'] }}</textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Time:</label>
									<div class="col-lg-10">
										<input type="number" name="time" min="0" class="form-control" value="{{ $json['time'] }}" placeholder="Enter test time in minutes">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Categories and Questions:</label>
									<div class="input_fields_wrap col-md-10">
										
										<div class="col-md-12 field_section">
											<div class="col-md-6">
												<input type="text" class="form-control" name="cat[]" @if(isset($sub_tests[0]['test'])) value="{{ $sub_tests[0]['test'] }}" @endif placeholder="Enter Category">
											</div>
											<div class="col-md-3">
												<input type="number" class="form-control" name="questions[]" @if(isset($sub_tests[0]['question_no']))  value="{{ $sub_tests[0]['question_no'] }}" @endif placeholder="Enter no. of questions">
											</div>
										</div>
										
										@for($i=1; $i < count($sub_tests); $i++)
											<div class="col-md-12 field_section">
												<div class="col-md-6">
													<input type="text" class="form-control" name="cat[]" @if(isset($sub_tests[$i]['test'])) value="{{ $sub_tests[$i]['test'] }}" @endif placeholder="Enter Category">
												</div>
												<div class="col-md-3">
													<input type="number" class="form-control" name="questions[]" @if(isset($sub_tests[$i]['question_no'])) value="{{ $sub_tests[$i]['question_no'] }}" @endif placeholder="Enter no. of questions">
												</div>
												<div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div>
												
											</div>

										@endfor
									</div>
																					
								</div>

								<div class="col-md-10 col-md-offset-2"><br>
									<button class="add_field_button btn btn-info btn-sm">Add More Fields</button>
								</div>

								
								@if( $record->subject_details != "null" && $record->subject_details != '' && $record->subject_details != '[]'  )	
									@php $subject=json_decode($record->subject_details , true); @endphp																	
										<div class="form-group">
											<label class="col-lg-2 control-label">Test subjects:</label>
											@foreach ($subject as $key=>$value)	
											
											<div id="subjectID{{ $key }}" class=" @if($key == 0) wrapSubject col-md-10 @else col-md-offset-2 col-md-10   @endif">
												<div class="col-md-12 ">
													<div class="col-md-6">
														<input type="text" class="form-control" value="{{ $value }}" name="subject[]" placeholder="Enter Subject ">
													</div>	
													<div class="col-md-6">
														@if($key == 0)
															<button class="addMoreSubject btn btn-info btn-sm">Add More Fields</button>
														@else 
															<button class="btn btn-danger btn-sm" onclick=removeSubject({{ $key }})>Remove</button>
														@endif
													</div>										
												</div>
											</div>
											@endforeach
										</div>
								@else 
									<div class="form-group">
										<label class="col-lg-2 control-label">Test subjects:</label>
										<div class="wrapSubject col-md-10">
											<div class="col-md-12 ">
												<div class="col-md-6">
													<input type="text" class="form-control" name="subject[]" placeholder="Enter Subject" required>
												</div>	
												<div class="col-md-6">
													<button class="addMoreSubject btn btn-info btn-sm">Add More Fields</button>
												</div>										
											</div>
										</div>
									</div>		

								@endif	

								</div>
										
								<div class="form-group">
									<label class="col-lg-2 control-label">Posted By:</label>
									<div class="col-lg-10">
										<input type="text" name="postby" class="form-control" value="{{ $json['writer'] }}" placeholder="Enter writer name">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Rank disclosed time</label>
									<div class="col-lg-10">
										<input type='text'  value="{{ date('m/d/Y H:i a',strtotime($record->rank_time)) }}" class="form-control" id='datetimepicker1' name="rank_time" required />
									</div>
								</div>

										
								<div class="form-group">
									<label class="col-lg-3 control-label">Price:</label>
									<div id="price" class="col-lg-9">
										<div class="col-md-6">
											<input type="radio" name="payment" value="free" @if ($json['payment']=='free')
											checked 
											@endif>Free
										</div>
										<div class="col-md-6">
											<input type="radio" name="payment" value="paid" @if ($json['payment']!="free")
											checked 
											@endif>Paid
										</div>

										<input style="display: none;" id="payment1" type="text" name="payment1" class="form-control" placeholder="Enter amount"
										@if ($json['payment']=="free") value="" @else value="{{ $json['payment'] }}" @endif />
									</div>
								</div>

									@if ($json['payment']!="free")
										<script>
											$(document).ready(function(){
												$("#payment1").show();
												});
										</script>
									@endif



									<div class="form-group">
										<label class="col-lg-2 control-label">Negative marking:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" name="negative" value="" @if ($json['negative_marks']!="marks") checked="checked"  @endif >
												No
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" name="negative" value="marks" @if ($json['negative_marks']=="marks") checked="checked"  @endif>
												Yes
											</label>
										</div>
									</div>

									<div class="form-group" @if($json['negative_marks'] != '')  @if ($json['negative_marks']!="marks") style="display:none" @endif @endif id="negitive">
										<label class="col-lg-2 control-label">Negitive Number:</label>
										<div class="col-lg-9">
											<input type="number" class="form-control" name="negitive_number" value="{{ $json['negative_mark'] }}" >
										</div>
									</div>


								
									<input type="hidden" name="id" value="{{ $record->id }}">
									<input type="hidden" name="old_topic" value="{{ $record->posttype }}">


									
										
										
										

										<div class="text-right">
											<button id="submit" type="submit" class="btn btn-primary">Update<i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->


		</div>

@endsection
@section('js')
	<script>

		jQuery(document).ready(function(){
			jQuery('input:radio[name="payment"]').change(function(){
				if(jQuery(this).val() == 'paid'){
					jQuery("#payment1").show();
				}
				else
				{
					jQuery("#payment1").hide();
				}
			});
		});

		jQuery(document).ready(function(){
			jQuery('input:radio[name="negative"]').change(function(){
				if(jQuery(this).val() == 'marks'){
					jQuery("#negitive").show();
				}
				else
				{
					jQuery("#negitive").hide();
				}
			});
		});
		
		$(document).ready(function(){
			$('.summernote').summernote();

			$("#login_form").validate({

				rules:{
					
					test_name:{
						required:true
					},
					description:{
						required:true
					},
					groupid:{
						required:true
					},
					time:{
						required:true,
						number:true
					},
					postby:{
						required:true
					},
					sub_tests:{
						required:true
					}
				}
			});

			var max_fields = 6; //maximum input boxes allowed
			var wrapper = $(".input_fields_wrap"); //Fields wrapper
			var add_button = $(".add_field_button"); //Add button ID
			var x =    "<?php echo ((count($sub_tests))); ?>"; //initlal text box count

			if(x>=max_fields)
			{
				$('.add_field_button').hide();
			}

			$(add_button).click(function(e){ //on add input button click
				e.preventDefault();
				if(x < max_fields){ //max input box allowed
					$(wrapper).append('<div class="col-md-12 field_section"><div class="col-md-6"><input type="text" class="form-control" name="cat[]" placeholder="Enter Category"></div><div class="col-md-3"><input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions"></div><div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div></div>'); //add input box
					x++; //text box increment
					

					// $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
				}
				

				if(x>=max_fields)
					{
						$('.add_field_button').hide();
					}

				
			});
											    
			$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
				e.preventDefault(); $(this).parents('div.field_section').remove();x--; 
				$('.add_field_button').show();
			});



			var subject_button      = $(".addMoreSubject"); //Add button ID
			var removeID=20;
			//Subject function button
			$(subject_button).click(function(e){

				e.preventDefault();
				var subjectAddmore= $(".wrapSubject");
				var fields=	'<div id="subjectID'+removeID+'" class="col-md-12">'+
								'<div class="col-md-6">'+
									'<input type="text" class="form-control" name="subject[]" placeholder="Enter Subject ">'+
								'</div>'+
								'<div class="col-md-6">'+
									'<button class="btn btn-danger btn-sm" onclick=removeSubject('+removeID+')>Remove</button>'+
								'</div>'+
							'</div>';
				
				subjectAddmore.append(fields);
				removeID++;

			});	

		});

		$('select[name="groupid[]"]').change(function(){
			var course= $(this).val();

			// console.log(course_ids);			
			// var jsonString = JSON.stringify(course_ids);

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_subject',   
				data: {course:course},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				} 
			});
		});

		function removeSubject (removeID){
			
			var id='#subjectID'+removeID;
			
			$(id).remove();

		}
	</script>
	{{-- for switch buttons --}}
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	{{-- for switch buttons --}}

@endsection