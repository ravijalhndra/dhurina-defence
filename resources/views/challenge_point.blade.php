@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/pages/datatables_basic.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/tags/tagsinput.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/tags/tokenfield.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_tags_input.js') }}"></script>
<!-- Theme JS fi

	les -->
@endsection
@section('content')
<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Horizontal form options -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Basic layout-->
                    <form action="{{ route('edit_challenge_point') }}" id="myForm" method="post" class="form-horizontal"
                        enctype="multipart/form-data" novalidate>
                        {{csrf_field()}}
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">Edit Challenge Point</h5>
                            </div>
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Description:</label>
                                    <div class="col-lg-9">
                                        <textarea name="description" class="summernote">{{$data->description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Min</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="" name="min" value="{{$data->min}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Max</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="" name="max" value="{{$data->max}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Request Level</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="" name="request_level"
                                            value="{{$data->request_level}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Message</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="" name="message" value="{{$data->message}}">
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Update Challenge Point <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- /basic layout -->
                </div>
            </div>
            <!-- /vertical form options -->
            <!-- /fieldset legend -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
</div>
<!-- /page container -->
<script type="text/javascript">
    $('input:radio[name="amount"]').change(function () {
        if ($(this).val() == 'paid') {
            $("#payment1").show();
        } else {
            $("#payment1").hide();
        }
    });
</script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#show_img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#upload_img").change(function () {
        readURL(this);
    })
</script>
@endsection