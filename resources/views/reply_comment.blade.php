@if(!empty($data))
@php
	$count=count($data);
@endphp
<p class="pull-left"><a id="hide_reply">Hide {{ $count }} replies</a><a style="display: none;" id="show_reply">{{ $count }} replies</a></p>
@foreach ($data as $reply)
@php
	$timestamp1=$reply->timestamp;
	$date1=date('M d,Y ');
	$time1=date('h:i:a');
	$com=json_decode($reply->jsondata,true); 
@endphp
	<div class="col-md-12 reply_section" style="; margin-bottom: 20px;">
    	<div class="col-md-12"  style="padding: 0px;">
    		@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
        		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="25" height="25"></div>
        	@else
        		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="35" height="35"></div>
        	@endif
    		<div class="col-md-6"><span style="font-weight: bold;">{{ $com['name'] }}</span><br>
    		<span style="margin-top: 5px;">{{ $com['collage'] }}</span>
    		</div>
    		<div  class="col-md-5"><span style="float: right; font-size: 8px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
    	</div>
		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			<p>{{ $com['comment'] }}</p>
			@if ($com['com_image']!="null")
			<img src="/mechanicalinsider/{{$com['com_image']}}" width="100" height="50">
			@endif
		</div>
		<div class="col-md-10" style="margin-left: -10px;">
			@permission('delete_comment')
			<a><span alt="{{ $reply->commentid }}" id='{{ $reply->id }}' class='delete_reply'>Delete</span></a>
			@endpermission
		</div>
    </div>
@endforeach
@endif

