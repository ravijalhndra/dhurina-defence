@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
			

				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">
                            Question :: {{ $jsondata['question'] }} 
                        </h4>
                         <h6> Options 
                            @if(isset($jsondata['a']))
                            <br> A: {{ $jsondata['a'] }} 
                            @endif
                            @if(isset($jsondata['b']))
                            <br> B: {{ $jsondata['b'] }} 
                            @endif
                            @if(isset($jsondata['c']))
                            <br> C: {{ $jsondata['c'] }} 
                            @endif
                            @if(isset($jsondata['d']))
                            <br> D : {{ $jsondata['d'] }} 
                            @endif
                            @if(isset($jsondata['e']))
                            <br> E : {{ $jsondata['e'] }} 
                            @endif

                        </h6>
					</div>

					<div class="panel-body">
						
					</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
                                <th>#</th>
								<th>Email</th>
								<th>Phone No</th>
								<th>Name</th>
                                <th>Answer</th>
                                <th>Option</th>
								
								
							</tr>
						</thead>
						<tbody>
					
						@foreach ( $answars as $key=>$data)
						
						
						
								<tr>
                                <td>{{ $key }}</td>
								<td>{{ $data->email }}</td>
                                <td>{{ $data->mobile }}</td>
                                <td>{{ $data->name }}</td>
                                <td>{{ $data->answar }}</td>
                                <td>{{ $data->answer }}</td>
							
					
							</tr>
						
						@endforeach
							
							
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>
@endsection
