@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>

				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">View Posts</h4>
					</div>

					<div class="panel-body">
						
					</div>

					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Time</th>
								<th>Like</th>
								<th>Comment</th>
								<th>View</th>
								<th>Post type</th>
								<th>Post description</th>
								<th>writer</th>
								<th>title</th>
								<th>description</th>
								<th>description</th>
								<th>description</th>
								
								
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
						@foreach ( $query as $data)
								<tr>
								<td>{{ $data->id }}</td>
								<td>{{ $data->timestamp }}</td>
								<td>{{ $data->likes }}</td>
								<td>{{ $data->comment }}</td>
								<td>{{ $data->view }}</td>
								<td>{{ $data->posttype }}</td>
								<td>{!! $data->post_description !!}</td>
								@php
									$json_string=json_decode($data->jsondata,true);
								@endphp
								<td>{{ $json_string['writer'] }}</td>
								<td>{{ $json_string['title'] }}</td>
								<td><img src="http://139.59.58.141/mechanicalinsider/newsimage/{{ $json_string['pic'] }}"></td>
								<td><img src="http://139.59.58.141/mechanicalinsider/newsimage/{{ $json_string['thumbnail'] }}"></td>
								
								
								
								
								<td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												<li><a data-toggle="modal" data-target="#modal_mini{{ $data->id }}" href="#"><i class="icon-trash"></i> Delete</a></li>
												
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<div  data-backdrop="false" id="modal_mini{{ $data->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Post</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this post.</p>

										</div>

										<div class="modal-footer">
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<button type="button" class="btn btn-primary">Yes</button>
										</div>
									</div>
								</div>
							</div>

						@endforeach
							
							
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>
@endsection
