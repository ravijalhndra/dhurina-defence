<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }}</title>

    @include('includes.css')

    <!-- Core JS files -->
    @include('includes.js')
    <!-- /core JS files -->

<style>
    .error{
        color:red;
    }
</style>
</head>

<body class="navbar-bottom login-container">

    <!-- Main navbar -->
    @include('includes.login_header')
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Password recovery -->
                <form id="login_form" action="{{ route('send_email') }}" method="post" class="login-form">
                {{ csrf_field() }}
                    <div class="panel panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (\Session::has('error'))
                        <div class="alert alert-warning">
                            {!! \Session::get('error') !!}
                        </div>
                    @endif
                    
                        <div class="text-center">
                            <div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
                            <h5 class="content-group">Password recovery <small class="display-block">We'll send you instructions in email</small></h5>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" name="email" class="form-control" placeholder="Your email" value="{{ old('email') }}" required>
                            <div class="form-control-feedback">
                                <i class="icon-mail5 text-muted"></i>
                            </div>
                            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <button type="submit" class="btn bg-teal-400 btn-block">Reset password <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
                <!-- /password recovery -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


    <!-- Footer -->
    @include('includes.footer')
    <!-- /footer -->

</body>
<script>
    $(document).ready(function(){
        $("#login_form").validate({

            rules:{
                email:{
                    required:true,
                    email:true
                }
            }
        
        });
    });
</script>
</html>
