<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }}</title>

    @include('includes.css')

    <!-- Core JS files -->
    @include('includes.js')
    <!-- /core JS files -->
<style>
    .error{
        color:red;
        margin-left: 20px;
    }
    .bg-indigo {
        background-color: #416a5c;
        border-color: #416a5c;
        color: #fff;
    }
</style>

</head>

<body class="navbar-bottom login-container">

    <!-- Main navbar -->
    @include('includes.login_header')
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Simple login form -->
                <form id="login_form" action="{{ route('login') }}" class="login-form" method="POST">
                {{ csrf_field() }}
                    <div class="panel panel-body">

                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                {!! \Session::get('success') !!}
                            </div>
                        @endif

                        @if (\Session::has('error'))
                            <div class="alert alert-warning">
                                {!! \Session::get('error') !!}
                            </div>
                        @endif
                        
                        <div class="text-center">
                            <img height="100" width="100" src="{{ URL::asset('img/defence_logo.png') }}"  />
                            {{--  <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>  --}}
                            <h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
                        </div>

                        <div class="form-group has-feedback has-feedback-left {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required autofocus>
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group has-feedback has-feedback-left {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                            @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn bg-pink-400 btn-block">Sign in <i class="icon-circle-right2 position-right"></i></button>
                        </div>

                        <div class="text-center">
                            <a href="{{ route('password.request') }}">Forgot password?</a>
                        </div>
                    </div>
                </form>
                <!-- /simple login form -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


    <!-- Footer -->
    @include('includes.footer')
    <!-- /footer -->

</body>
<script>
    $(document).ready(function(){
        $("#login_form").validate({

            rules:{
                password:{
                    required:true
                },
                email:{
                    required:true,
                    email:true
                }
            }
        
        });
    });
</script>
</html>
