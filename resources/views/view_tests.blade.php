@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				
				<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
					<li>
						<a href="{{ url('add_test') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
							<i class="fab-icon-open icon-plus3"></i>
							<i class="fab-icon-close icon-cross2"></i>
						</a>
					</li>
				</ul>
			
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">View Online Tests</h4>
					</div>

					<div class="panel-body">
						
					</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
								{{-- <th>ID</th> --}}
								<th colspan="5">Groups</th>
								<th>Test Name</th>
								<th>Post By</th>
								<th>Test Time</th>
								<th>Attempts</th>
								<th>Sub Tests</th>
								<th>Description</th>
								<th>Total Questions</th>
								<th>Payment</th>
								<th>Status</th>
								
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
						@foreach ( $query as $data)
						@php
						$json=json_decode($data->post_description,true);
						$jsondata=json_decode($data->jsondata,true);
							// dd($jsondata);
						@endphp
						
						
								<tr>
								{{-- <td>{{ $data->id }}</td> --}}
								<td colspan="5">{{ $data->groups }}</td>
								<td>{{ $jsondata['testname'] }}</td>
								<td>{{ $jsondata['writer'] }}</td>
								<td>{{ $jsondata['time'] }} min</td>
								<td>{{ $data->view }}</td>
								<td>
									@foreach ($json as $sub)
							{{ $sub['test'] }} = {{ $sub['question_no'] }},
						@endforeach
								</td>
								<td>{!! $jsondata['description'] !!}</td>
								<td>{{ $jsondata['question'] }}</td>
								<td>{{ $jsondata['payment'] }}</td>
								<td>{{ $data->status }}</td>
								{{-- <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal_mini">Launch <i class="icon-play3 position-right"></i></button> --}}
								<td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												@permission('delete_test')<li><a data-toggle="modal" data-target="#modal_mini{{ $data->id }}" href="#"><i class="icon-trash"></i> Delete</a></li>@endpermission
												@permission('publish_test')
												<li><a @if ($data->status=="publish")
													class="btn btn-warning"
													@else
													class="btn btn-primary"
												@endif  href="{{ url('publish_test/'.$data->id) }}">@if ($data->status=="publish")
													UnPublish
													@else
													Publish
												@endif</a></li>
												@endpermission
												
												@permission('questions')
												<li><a href="{{ url('questions/'.$data->id) }}"><i class="icon-eye"></i> View Questions</a></li>
												@endpermission
												@permission('add_questions')
												<li><a href="{{ url('add_questions/'.$data->id) }}"><i class="icon-pencil"></i> Add Questions</a></li>
												@endpermission
												@permission('make_test_zip')
												<li><a href="{{ url('make_test_zip/'.$data->id) }}"><i class="icon-file-zip"></i>Make Zip</a></li>
												@endpermission
												
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<div  data-backdrop="false" id="modal_mini{{ $data->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Test</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this test.</p>

										</div>

										<div class="modal-footer">
											<form action="{{ url('delete_test') }}" method="post">
											{{ csrf_field() }}
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<input type="hidden" name="del_id" value="{{ $data->id }}">
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										</div>
									</div>
								</div>
							</div>
						@endforeach
							
							
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>
@endsection
