@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>

@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form action="{{ url('edit_post/'.$all->id) }}" method="POST" enctype="multipart/form-data" class="form-horizontal" novalidate>
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Post</h5>
									
								</div>
@php
    $json=json_decode($all->jsondata,true);

@endphp
								<div class="panel-body">
									
									<div class="form-group">
											<label class="col-lg-3 control-label">Topic:</label>
											<div class="col-lg-4">
												<select id="topic" name="posttype" data-placeholder="Select one..." class="select select-border-color border-warning" required>
													<option value="">Select one...</option>
													@foreach ($query as $q)
													<option value="{{ $q->topic_name }}" @if ($all->posttype==$q->topic_name)
													selected
												@endif >{{ $q->topic_name }}</option>
													@endforeach
													<option id="create_topic" value="create_topic">create new topic</option>
												</select> 
											</div>
											<div id="new_topic1">
											<label class="col-lg-1 control-label">Create New:</label>
											<div class="col-lg-3">
												<input type="text" id="new_topic" name="new_topic" class="form-control" placeholder="add new topic name">
											</div>
											</div>
										</div>
										<script>
											$(document).ready(function(){
												$("#new_topic1").hide();
												$("#topic").change(function(){
													var topic=$("#topic").val();
													if(topic=='create_topic')
													{
														$("#new_topic1").show();
													}
													else
													{
														$("#new_topic1").hide();
													}
												});
												
											});
										</script>

									<div class="form-group">
										<label class="col-lg-3 control-label">Post Title:</label>
										<div class="col-lg-9">
											<input type="text" name="title" value="{{ $json['title'] }}" class="form-control" placeholder="Enter Post Title">
										</div>
									</div>
									<script type="text/javascript">
										$(document).ready(function() {
										  $('.summernote').summernote();
										});
									</script>
									<div class="form-group">
										<label class="col-lg-3 control-label">Post Description:</label>
										<div class="col-lg-9">
											<textarea  name="post_description" class="summernote">{!! $all->post_description !!}</textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Post Type:</label>
										<div class="col-lg-9">
											<select class="form-control" name="post_type">
												<option value="wordpress" @if(isset($json['post_type']) && $json['post_type']=='wordpress') selected @endif>Wordpress</option>
												<option value="external_link" @if(isset($json['post_type']) && $json['post_type']=='external_link') selected @endif>External link</option>
												<option value="news" @if(isset($json['post_type']) && $json['post_type']=='news') selected @endif>News (only html text)</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Post URL:</label>
										<div class="col-lg-9">
											<input type="text" name="posturl" class="form-control" placeholder="Enter Post URL" @if ($all->posturl=='news')
												value="" 
												@else
												value="{{ $all->posturl }}" 
											@endif>
										</div>
									</div>

										<div class="form-group">
										<label class="col-lg-3 control-label">Select:</label>
										<div class="col-lg-9">
											
											<label class="radio-inline">
												<input type="radio" class="styled" id="groups_institute" name="type_institute" value="group" @if($all->groupid != 0) checked @endif>
												Groups
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" id="college_institute" name="type_institute" value="college" @if($all->groupid == 0) checked @endif>
												College
											</label>
											
											<label class="radio-inline">
												<input type="radio" class="styled" id="both"  name="type_institute" value="both" @if($all->post_type == 2) checked @endif>
												Both
											</label>
											
										</div>
									</div>
									
									@if($all->groupid!= 0)
									<script>
											$(document).ready(function(){
												$("#college_select").hide();
												$("#stream_select").hide();
												$("#course_select").hide();
												$("#year_select").hide();
												$("#groups_institute").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();	
														$("#groups_select").show();
												});
												$("#college_institute").click(function(){
														$("#college_select").show();
														$("#stream_select").show();
														$("#course_select").show();
														$("#year_select").show();
														$("#groups_select").hide();
												});
												$("#both").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();
														$("#groups_select").hide();
												});
											});
									</script>
										@elseif($all->groupid == 0 && $all->groupid!='all')
									<script>
											$(document).ready(function(){
												$("#college_select").show();
												$("#stream_select").show();
												$("#course_select").show();
												$("#year_select").show();
												$("#groups_select").hide();
												$("#groups_institute").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();	
														$("#groups_select").show();
												});
												$("#college_institute").click(function(){
														$("#college_select").show();
														$("#stream_select").show();
														$("#course_select").show();
														$("#year_select").show();
														$("#groups_select").hide();
												});
												$("#both_institute").click(function(){
													$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();
														$("#groups_select").hide();
												});
												
											});
									</script>
									@else
									<script>
											$(document).ready(function(){
												$("#college_select").show();
												$("#stream_select").show();
												$("#course_select").show();
												$("#year_select").show();
												$("#groups_select").hide();
												$("#groups_institute").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();	
														$("#groups_select").show();
												});
												$("#college_institute").click(function(){
														$("#college_select").show();
														$("#stream_select").show();
														$("#course_select").show();
														$("#year_select").show();
														$("#groups_select").hide();
												});
												$("#both").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();
														$("#groups_select").hide();
												});
											});
									</script>
									@endif
									@if($all->post_type!=2)
									<div class="form-group" id="course_select">
									<label class="col-lg-3 control-label">Select Course:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="course" data-placeholder="Select Course..." id="coursedropdown" class="course-selected form-control" required>
										<!-- multiple="multiple"   -->
										    
											@if(isset($course) && $course!='null' && !empty($course))
												@foreach ($course as $cou_rse)
													<option value="{{ $cou_rse->id }}"@if(isset($collegedata))  @if($collegedata->courseid == $cou_rse->id) selected @endif @endif>{{ $cou_rse->course_name }}</option>
												@endforeach
											@endif
										</select>
										<input type="hidden" value="{{route('get_course')}}" name="urlcourse" class="urlcourse"/>
									</div>
								</div>
								
								<div class="form-group" id="college_select">
									<label class="col-lg-3 control-label">Select College:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="college" data-placeholder="Select College..." id="collagedropdown" class="college-selected form-control" required>
										<option value="">Select College </option>
											@if( isset($college_slected) && $college_slected!='null' && !empty($college_slected))
												@foreach ($college_slected as $colle_ge)
													<option value="{{ $colle_ge->id }}" @if($collegedata->college_id == $colle_ge->id) selected @endif>{{ $colle_ge->college_name }}</option>
												@endforeach
											@else
											@foreach ($college as $c)
													<option value="{{ $c->id }}">{{ $c->college_name }}</option>
												@endforeach
											@endif
										</select>
										{{--<!-- <input type="hidden" value="{{ route('get_stream') }}" name="urlcollege" class="urlcollege"/> -->--}}
									</div>
								</div>

								<div class="form-group" id="stream_select">
									<label class="col-lg-3 control-label">Select Stream:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="stream" data-placeholder="Select Stream..." class="stream-selected form-control" required>
											<option value="">Select Stream</option>
											@if(isset($stream))
												@foreach ($stream as $str_eam)
													<option value="{{ $str_eam }}" @if($collegedata->stream == $str_eam) selected @endif>{{ $str_eam }}</option>
												@endforeach
												@foreach ($depart as $d)
													<option value="{{ $d->dept_name }}">{{ $d->dept_name }}</option>
												@endforeach
											@endif
										</select>
									</div>
								</div>

							 	<div class="form-group" id="year_select">
									<label class="col-lg-3 control-label">Select Year:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="year" data-placeholder="Select Year..."  class="year-selected form-control" required>
													<option value=""></option>
													@if(isset($year))
														@for ($i = 1; $i<= $year; $i++)
															<option value="{{ $i }}" @if($collegedata->year == $i) selected @endif>{{ $i }}</option>
														@endfor
												    @else
													@foreach($college as $c)
													<option value="{{$c->year}}">{{$c->year}}</option>
													@endforeach
													@endif
										</select>
									</div>
								</div> 

								@else
								
								<div class="form-group" id="course_select">
									<label class="col-lg-3 control-label">Select Course:</label>
									<div class="col-lg-2 multi-select-full">
										<select name="course" data-placeholder="Select Course..." id="coursedropdown" class="course-selected form-control" required>
										<!-- multiple="multiple"   -->
											@if($course)
												@foreach ($course as $cou_rse)
												@if($cou_rse)
													<option value="{{ $cou_rse->id }}">{{ $cou_rse->course_name }}</option>
												@endif
												@endforeach
												@endif
										</select>
										<input type="hidden" value="{{route('get_course')}}" name="urlcourse" class="urlcourse"/>
									</div>
								</div>

								<div class="form-group" id="college_select">
									<label class="col-lg-3 control-label">Select College:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="college" data-placeholder="Select College..." id="collagedropdown" class="college-selected form-control" required>
										<option value="">Select College </option>
										</select>
										{{--<!-- <input type="hidden" value="{{ route('get_stream') }}" name="urlcollege" class="urlcollege"/> -->--}}
									</div>
								</div>

								<div class="form-group" id="stream_select">
									<label class="col-lg-3 control-label">Select Stream:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="stream" data-placeholder="Select Stream..." class="stream-selected form-control" required>
											<option value="">Select Stream</option>
										</select>
									</div>
								</div>

								<div class="form-group" id="year_select">
									<label class="col-lg-3 control-label">Select Year:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="year" data-placeholder="Select Year..."  class="year-selected form-control" required>
													<option value=""></option>
										</select>
									</div>
								</div>
								@endif				
								    @if($all->post_type!=2)
									<div class="form-group" id="groups_select">
										<label class="col-lg-3 control-label">Select Groups:</label>
										<div class="col-lg-9">
											{{-- @foreach ($data as $d)
											<div class="col-md-4">
											@php
											$groups=explode(",", $all->groupid);
											@endphp
											<input type="checkbox" name="groupid[]" value="{{ $d->id }}"
											@if($all->groupid=="all")
											checked 
											@elseif (in_array($d->id, $groups))
												checked 
											@endif> &nbsp;&nbsp;{{ $d->topic }}
											</div>
											@endforeach --}}
										</div>
										<div class="col-lg-9 multi-select-full">
										@php
											$groups=explode(",", $all->groupid);
											@endphp
												<select name="groupid[]" data-placeholder="Select groups..." multiple="multiple" class="multiselect-select-all-filtering">
												@if($data[0]!="")
												@foreach ($data as $d)
												@if($d)
													<option value="{{ $d->id }}" @if($all->groupid=="all")
											selected 
											@elseif (in_array($d->id, $groups))
												selected 
											@endif>{{ $d->topic }}</option>
											@endif
												@endforeach
												@endif
												
												</select>
											</div>
									</div>
								@endif
									<div class="form-group">
										<label class="col-lg-3 control-label">Price:</label>
										<div id="price" class="col-lg-9">
										<div class="col-md-6">
											<input type="radio" name="payment" value="free" @if ($json['payment']=='free')
											checked 
											@endif>Free
										</div>
										<div class="col-md-6">
											<input type="radio" name="payment" value="paid" @if ($json['payment']!="free")
											checked 
											@endif>Paid
										</div>
										<input style="display: none;" id="payment1" type="text" name="payment1" class="form-control" placeholder="Enter amount"
										@if ($json['payment']=="free")
											value=""
										@else
										value="{{ $json['payment'] }}" 
										@endif ">
										</div>
									</div>
									@if ($json['payment']!="free")
										<script>
											$(document).ready(function(){
												$("#payment1").show();
												});
										</script>
									@endif
									<script type="text/javascript">
										$('input:radio[name="payment"]').change(function(){
										    if($(this).val() == 'paid'){
										    	$("#payment1").show();
										    }
										    else
										    {
										    	$("#payment1").hide();
										    }
									});
									</script>

									<div class="form-group">
										<label class="col-lg-3 control-label">Writer:</label>
										<div class="col-lg-9">
											<input type="text" name="writer" class="form-control" placeholder="Enter writer name" value="{{ $json['writer'] }}">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Pic:</label>
										<div class="col-lg-9">
											<input id="photoInput" type="file" name="pic">
										</div>
										<input type="hidden" name="old_pic" value="{{ $json['pic'] }}">
										<div id="imgContainer"><span id="imgerr"></span></div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Thumbnail:</label>
										<div class="col-lg-9">
											<input id="photoInput1" type="file" name="thumbnail">
										</div>
										<input type="hidden" name="old_thumbnail" value="{{ $json['thumbnail'] }}">
										<div id="imgContainer1"><span id="imgerr"></span></div>
									</div>
									<fieldset>
											<legend></legend>
											{{-- <div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to like this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_like" type="checkbox" class="switchery" @if(isset($json['add_like'])) @if($json['add_like']=="off") checked @endif @endif  value="off">
													</label>
												</div>
											</div> --}}
											<div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to comment this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_comment" type="checkbox" class="switchery" @if(isset($json['add_comment'])) @if($json['add_comment']=="off") checked @endif @endif  value="off">
													</label>
												</div>
											</div>
										</fieldset>


										<input type="hidden" name="old_topic" value="{{ $all->posttype }}">
									<div class="text-right">
										<button type="submit" class="btn btn-primary">Add<i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->


		</div>

@endsection
@section('js')
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				posttype:{
					required:true
				},
				title:{
					required:true
				},
				post_description:{
					required:true
				},
				groupid:{
					required:true
				},
				payment:{
					required:true
				},
				writer:{
					required:true
				},
				post_type:{
					required:true
				},
				payment1:{
					required:true,
					number:true
				}
			}
		});
	});
</script>
<script>
	$(function () {
    $("#submit").bind("click", function () {
        // Get reference of FileUpload.
        var file_size = $('#photoInput')[0].files[0].size;
        file_size_kb=file_size/1024;
		if(file_size_kb>50)
		{
			 $("#imgContainer span#imgerr").html('<span style="color:red;">Max upload size is 50kb.</span>');
                            return false;
		}

        var fileUpload = $("#photoInput")[0];
        //Check whether the file is valid Image.
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        if (regex.test(fileUpload.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if (typeof (fileUpload.files) != "undefined") {
                //Initiate the FileReader object.
                var reader = new FileReader();
                //Read the contents of Image File.
                reader.readAsDataURL(fileUpload.files[0]);
                reader.onload = function (e) {
                    //Initiate the JavaScript Image object.
                    var image = new Image();
                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;
                    image.onload = function () {
                        //Determine the Height and Width.
                        var height = this.height;
                        var width = this.width;
                        if (height > 300 || width > 600) {
                            // alert("Height and Width must not exceed 100px.");
                            $("#imgContainer span#imgerr").html('<span style="color:red;">Max Width and height is 600px * 300px.</span>');
                            return false;
                        }
                        else if (height < 225 || width < 400)
                        {
                            $("#imgContainer span#imgerr").html('<span style="color:red;">Min Width and height is 400px * 225px.</span>');
                            return false;
                        }

                        // alert("Uploaded image has valid Height and Width.");
                        $("#imgContainer span#imgerr").html(" ");
                        return true;
                    };
                }
            } else {
                $("#imgContainer span#imgerr").html('<span style="color:red;">This browser does not support HTML5.</span>');
                return false;
            }
        } else {
            $("#imgContainer span#imgerr").html('<span style="color:red;">Please select a valid Image file</span>');
            return false;
        }
 
        //Get reference of FileUpload.
        var fileUpload1 = $("#photoInput1")[0];

        var file_size1 = $('#photoInput1')[0].files[0].size;
        file_size_kb1=file_size1/1024;
		if(file_size_kb1>20)
		{
			 $("#imgContainer1 span#imgerr").html('<span style="color:red;">Max upload size is 20kb.</span>');
                            return false;
		}

        //Check whether the file is valid Image.
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        if (regex.test(fileUpload1.value.toLowerCase())) {
            //Check whether HTML5 is supported.
            if (typeof (fileUpload1.files) != "undefined") {
                //Initiate the FileReader object.
                var reader1 = new FileReader();
                //Read the contents of Image File.
                reader1.readAsDataURL(fileUpload1.files[0]);
                reader1.onload = function (e) {
                    //Initiate the JavaScript Image object.
                    var image1 = new Image();
                    //Set the Base64 string return from FileReader as source.
                    image1.src = e.target.result;
                    image1.onload = function () {
                        //Determine the Height and Width.
                        var height1 = this.height;
                        var width1 = this.width;
                        if (height1 > 100 || width1 > 100) {
                            // alert("Height and Width must not exceed 100px.");
                            $("#imgContainer1 span#imgerr").html('<span style="color:red;">Max Width and height is 100px * 100px.</span>');
                            return false;
                        }
                        else if (height1 < 20 || width1 < 20)
                        {
                            $("#imgContainer1 span#imgerr").html('<span style="color:red;">Min Width and height is 20px * 20px.</span>');
                            return false;
                        }

                        // alert("Uploaded image has valid Height and Width.");
                        $("#imgContainer1 span#imgerr").html(" ");
                        return true;
                    };
                }
            } else {
                $("#imgContainer1 span#imgerr").html('<span style="color:red;">This browser does not support HTML5.</span>');
                return false;
            }
        } else {
            $("#imgContainer1 span#imgerr").html('<span style="color:red;">Please select a valid Image file</span>');
            return false;
        }
    });
});
</script>

{{-- for switch buttons --}}
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	{{-- for switch buttons --}}

@endsection