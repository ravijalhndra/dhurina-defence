@extends('layouts.main')
@section('js_head')
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">


	<script>
		$('#login_form').submit(function() {
		$('#gif').css('visibility', 'visible');
	});
	</script>

	<style>		
		#loading {
			width:100px;
			height: 100px;
			position: fixed;
			top: 30%;
			left: 45%;
			z-index:2;
			background-color: transparent;		
		}
	</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('send_notification') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Notification</h5>									
								</div>

								<div class="panel-body">
                                    
                                    <div class="form-group">
										<label class="col-lg-3 control-label">Type:</label>
										<div class="col-lg-9">

											<label class="radio-inline">
												<input type="radio" class="styled" name="type" value="class" checked="checked" />
												Class
											</label>

											<!-- <label class="radio-inline">
												<input type="radio" class="styled" name="type" value="school" />
												School
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" name="type" value="edudream"  />
												Edudream
                                            </label>
                                            
                                            <label class="radio-inline">
												<input type="radio" class="styled" name="type" value="both"  checked="checked" />
												Both
											</label> -->
										</div>
									</div>

									<div class="form-group" id="course_ids">
                                        <label class="col-lg-3 control-label">Select @if(Auth::user()->role == 'admin' ) Courses @else Class @endif:</label>
                                        <div class="col-lg-9 multi-select-full">
                                            <select name="course_id[]" data-placeholder="Select one..." multiple="multiple" class="multiselect-select-all-filtering course_id" required>
                                                @foreach ($new_Courses as $c)
                                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
										</div>
									</br>
										<label class="col-lg-3 control-label">Status:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" name="status" value="wherein" checked />
												With In
											</label>

											<label class="radio-inline">
												<input type="radio" class="styled" name="status" value="wherenotin" />
												Without
											</label>
										</div>
                                    </div>
									                                    
                                    <div class="form-group">
										<label class="col-lg-3 control-label">Message</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Notification message" name="msg">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Image : </label>
										<div class="col-lg-9">
											<input type="file" class="form-control" placeholder="Image" name="image" >
										</div>
									</div>

								</div>

								<br/> <br/>
								<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Send <i class="icon-arrow-right14 position-right"></i></button>
								</div><br>

							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')

	<script type="text/javascript">		

		$(document).ready(function() {			
			$("#course_ids").hide();	
		});

		$('input:radio[name="type"]').change(function(){				
			if( $(this).val() == 'class'){
				$("#course_ids").show();
			}
			else
			{
				$("#course_ids").hide();
			}
		});
	
		$('select[name="course_id[]"]').change(function(){
			var course= $(this).val();

			// console.log(course_ids);			
			// var jsonString = JSON.stringify(course_ids);

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_subject',   
				data: {course:course},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{
						console.log(data.data);
						
						$('#subcategory').show();
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				} 
			});
		});
		
	</script>
@endsection
