@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables1.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic1.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('js/custom11.js') }}></script>

<style>
.datatable-header { display:none; }
</style>
@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					{{-- @php dd($users);@endphp --}}
					@php
						$from=($users->currentPage()-1)*($users->perPage())+1;
						$to=$users->currentPage()*$users->perPage();
						$total=$users->total();
					@endphp 
				<!-- Basic datatable -->

				<div class="panel panel-flat">
					<div class="panel-heading">				
						<div class="row" style="padding-top:50px;">
							<div class="col-md-12">
								<form action="" method="GET">
									<div class="col-md-12">
										<label>							
											<input type="search" class="form-control" name="filter" placeholder="Type to filter...">									
										</label> 
											&nbsp;									
										<input type="submit" class="fab-menu-btn btn bg-success" value="search">
									</div>							
								</form>						
							</div>
						</div>
					</div>



					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Ref Code</th>
								<th>Joined On</th>	
								<th>Duration</th>
								<th>Block Status</th>
								<th>Block Comment</th>
								<th>Action</th>	
							</tr>
						</thead>
						<tbody id="ajaxresponse">
							@foreach ( $users as $user)						
								<tr>
									<td>{{ $user->id }}</td>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ $user->mobile }}</td>
									<td>{{ $user->referralcode }}</td>
									<td>{{ date('d M,Y',strtotime($user->created_at)) }}</td>
									<td>{{ $user->duration }} min</td>								
									<td>
										<span class="fab-menu-btn btn-sm @if($user->block_status == 'block') bg-red @endif  bg-success btn-rounded "> {{ucfirst($user->block_status)  }} </span>  
									</td>

									<td>
										<span class="fab-menu-btn btn-sm @if($user->block_comment == 'block')  bg-warning @endif bg-info btn-rounded "> {{ucfirst($user->block_comment)  }} </span>  
									</td>

									<td class="text-center">										
										<ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>
												<ul class="dropdown-menu dropdown-menu-right" >	
													<li>
														<a href="{{route('edit_user',encrypt ($user->id))}}"><i class="icon-pencil"></i> Edit</a>
													</li>
													<li>
														<a href="{{route('video_duration',encrypt ($user->id))}}"><i class="fa fa-clock-o"></i> Duration</a>
													</li>

													@if(Auth::user()->role == 'admin' )
														<li>
															@if($user->block_status == 'unblock')
															<a href="javascript:;"  onClick="fun_block({{ $user->id }},'block','{{ $user->gcmid }}')"><i class="icon-lock"></i> Block User</a>
																@else 
															<a href="javascript:;"  onClick="fun_block({{ $user->id }},'unblock','{{ $user->gcmid }}')"><i class="icon-lock"></i> Unblock User</a>
															@endif

															
														</li>

														<li>
															@if($user->block_comment == 'unblock')
																<a href="javascript:;"  onClick="fun_comment({{ $user->id }},'block','{{ $user->gcmid }}')"><i class="icon-comment"></i> Block Comment</a>
															@else 
																<a href="javascript:;"  onClick="fun_comment({{ $user->id }},'unblock','{{ $user->gcmid }}')"><i class="icon-comment"></i> Unblock Comment</a>
															@endif
														</li>
													@endif
												</ul>
											</li>
										</ul>									
									</td> 

								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				
				<div class="col-md-12">
					<div class="col-md-4">
						<span class="float-left">Showing {{ $from }} to {{ $to }} from {{ $total }} users</span> 
						</div>
						<div class="col-md-4">
						<ul class="pager" style="padding-bottom: 10px;margin-top: -10px;">
						  <li><a href="{{ $users->previousPageUrl() }}">Previous</a></li>
						  <li><a href="{{ $users->nextPageUrl() }}">Next</a></li>
						</ul>
						</div>
						<div class="col-md-4">
							<div class="pull-right">
								GO To Page: 
								<select name="go_to" onchange="location = this.value;">

								@for ($i = 50; $i < $users->lastPage(); $i+=50)
									<option value="{{ $users->url($i) }}">{{ $i }}</option>
								@endfor
								</select>
							</div>
						</div>
				</div>
				{{-- <ul class="pager">
  <li><a href="{{ $users->previousPageUrl() }}">Previous</a></li>
  <li><a href="{{ $users->nextPageUrl() }}">Next</a></li>
</ul> --}}
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>

		<div  data-backdrop="false" id="modal_mini" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Click on Yes if you want to Block this User.</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('block_user') }}" method="post">
							{{ csrf_field() }}
							<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="id" name="id" value="">
							<input type="hidden" id="type" name="type" value="">
							<input type="hidden" id="player_id" name="player_id" value="">
							<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div  data-backdrop="false" id="modal_mini_comment" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Click on Yes if you want to Block Comment this User.</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('block_comment') }}" method="post">
							{{ csrf_field() }}
							<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="u_id" name="u_id" value="">
							<input type="hidden" id="u_type" name="u_type" value="">
							<input type="hidden" id="u_player_id" name="u_player_id" value="">
							<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		<script>
		  $(function() {
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'https://datatables.yajrabox.com/eloquent/basic-data'
        });
    });
		</script>
@endsection

@section('js')

	<script type="text/javascript">

		function fun_block(id,type,player_id)
		{	
			$('#id').val(id);
			$('#type').val(type);
			$('#player_id').val(player_id);			
			$('#modal_mini').modal('toggle');		
		}


		function fun_comment(id,type,player_id)
		{	
			$('#u_id').val(id);
			$('#u_type').val(type);
			$('#u_player_id').val(player_id);			
			$('#modal_mini_comment').modal('toggle');		
		}
	</script>
@endsection
