@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>

	<!-- Theme JS files -->


<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('job_store') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Job</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">

								<div class="form-group">
										<label class="col-lg-3 control-label">Select:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" id="groups_institute" name="type_institute" value="group" checked="checked">
												Groups
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" id="college_institute" name="type_institute" value="college">
												College
											</label>
										</div>
									</div>
									<script>
											$(document).ready(function(){
												$("#college_select").hide();
												$("#stream_select").hide();
												$("#course_select").hide();
												$("#year_select").hide();
												$("#groups_institute").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();	
														$("#groups_select").show();
												});
												$("#college_institute").click(function(){
														$("#college_select").show();
														$("#stream_select").show();
														$("#course_select").show();
														$("#year_select").show();
														$("#groups_select").hide();
												});
												
											});
									</script>
									
									<div class="form-group" id="course_select">
									<label class="col-lg-3 control-label">Select Course:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="course" data-placeholder="Select Course..." id="coursedropdown" class="course-selected form-control" required>
										<!-- multiple="multiple"   -->
											@if($course)
												@foreach ($course as $cou_rse)
												@if($cou_rse)
													<option value="{{ $cou_rse->id }}">{{ $cou_rse->course_name }}</option>
												@endif
												@endforeach
												@endif
										</select>
										<input type="hidden" value="{{route('get_course')}}" name="urlcourse" class="urlcourse"/>
									</div>
								</div>

								<div class="form-group" id="college_select">
									<label class="col-lg-3 control-label">Select College:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="college" data-placeholder="Select College..." id="collagedropdown" class="college-selected form-control" required>
										<option value="">Select College </option>
										</select>
										{{--<!-- <input type="hidden" value="{{ route('get_stream') }}" name="urlcollege" class="urlcollege"/> -->--}}
									</div>
								</div>

								<div class="form-group" id="stream_select">
									<label class="col-lg-3 control-label">Select Stream:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="stream" data-placeholder="Select Stream..." class="stream-selected form-control" required>
											<option value="">Select Stream</option>
										</select>
									</div>
								</div>

								<div class="form-group" id="year_select">
									<label class="col-lg-3 control-label">Select Year:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="year" data-placeholder="Select Year..."  class="year-selected form-control" required>
													<option value=""></option>
										</select>
									</div>
								</div>

									<div class="form-group" id="groups_select">
											<label class="col-lg-3 control-label">Select Groups:</label>
											<div class="col-lg-9 multi-select-full">
												<select name="groupid[]" data-placeholder="Select groups..." multiple="multiple" class="multiselect-select-all-filtering" required>
												@if($data[0]!="")
												@foreach ($data as $d)
												@if($d)
													<option value="{{ $d->id }}">{{ $d->topic }}</option>
												@endif
												@endforeach
												@endif
												
												</select>
											</div>
										</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Title:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="title">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Post Name:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="postname">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">No. of Posts:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="posts">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Last Date:</label>
										<div class="col-lg-9">
											<div class="content-group-lg">
									<div class="input-group">
										<span class="input-group-addon"></span>
											
										<input type="text" class="form-control pickadate-accessibility" id="txtstartdate" name="lastdate" placeholder="Try me&hellip;">
									</div>
								</div>
										</div>
									</div>

									

									{{-- <div class="form-group">
											<label class="col-lg-2 control-label">Description:</label>
											<div class="col-lg-10">
												<textarea id="description"  name="description" class="summernote"></textarea>
											</div>
										</div>
										<div class="col-md-10 col-md-offset-2">
											<p>You have to fill only one field from description and URL.</p>
										</div> --}}

									{{-- <div class="form-group">
										<label class="col-lg-3 control-label">URL:</label>						
												<div class="col-lg-9">
											<input id="url" type="text" class="form-control" name="website" placeholder="www.xyz.com">
										</div>
									</div>
 --}}
									<div class="form-group">
										
											<label class="col-lg-2 control-label">Choose one:</label>
											<div id="price" class="col-lg-10">
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="choose" value="description" checked>
														Description
													</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="choose" value="url">
														Website
													</label>
												</div>
											</div>
											</div>
											</div>
											<div id="description1"  class="form-group">
											<label class="col-lg-2 control-label">Description:</label>
											<div class="col-lg-10">
												<textarea id="description"  name="description" class="summernote"></textarea>
											</div>
										</div>
										<div id="url1" style="display: none;" class="form-group">
										<label class="col-lg-3 control-label">URL:</label>						
												<div class="col-lg-9">
											<input id="url" type="text" class="form-control" name="website" placeholder="www.xyz.com">
										</div>
									</div>
										
										<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="choose"]').change(function(){
												    if(jQuery(this).val() == 'description'){
												    	jQuery("#description1").show();
												    	jQuery("#url").val("");
												    	jQuery("#url1").hide();
												    }
												    else if(jQuery(this).val() == 'url'){
												    	jQuery("#description1").hide();
												    	jQuery("#url1").show();
												    	jQuery("#description").val('');

												    }
												    else
												    {
												    	jQuery("#description1").hide();
												    	jQuery("#url1").hide();
												    }
												});
											});
										</script>
									
									
									
									{{-- <div class="form-group">
										
											<label class="col-lg-2 control-label">Price:</label>
											<div id="price" class="col-lg-10">
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="payment" value="free">
														Free
													</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="payment" value="paid">
														Paid
													</label>
												</div>
											</div>
											<input style="display: none;" id="payment1" type="number" min="0" name="payment1" class="form-control" placeholder="Enter amount">
											</div>
										</div>
										<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="payment"]').change(function(){
												    if(jQuery(this).val() == 'paid'){
												    	jQuery("#payment1").show();
												    }
												    else
												    {
												    	jQuery("#payment1").hide();
												    }
												});
											});
										</script> --}}

										<div class="form-group">
										<label class="col-lg-3 control-label"></label>
										<div class="col-lg-9">
											 <div class="main-img-preview">
											<img class="thumbnail img-preview" src="http://farm4.static.flickr.com/3316/3546531954_eef60a3d37.jpg" title="Preview Logo" >
							              </div>
																		 <div class="input-group">
							                <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled" style="display:none">
							                <div class="input-group-btn">
							                  <div class="fileUpload btn btn-danger fake-shadow">
							                    <span><i class="glyphicon glyphicon-upload"></i> Upload Logo</span>
							                    <input id="logo-id" name="logo" type="file" class="attachment_upload" >
							                  </div>
							                </div>
							              </div>
										</div>
									</div>
									<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>

									<fieldset>
										<legend>Notificaton Image (optional)</legend>
										<div class="form-group" id="mySelect">
											<label class="col-lg-2 display-block text-semibold">Add Job Icon</label>
											<div class="col-lg-10">
											<label class="radio-inline">
												<input type="radio" name="type" value="link" class="styled" checked="checked">
												Link
											</label>
											<label class="radio-inline">
												<input type="radio" name="type" value="image" class="styled">
												Image
											</label>
											</div>
										</div>
										
										<div class="form-group" id="link">
											<label class="col-lg-2 control-label">Enter Link :</label>
											<div class="col-lg-10">
												<input id="link" type="text" class="form-control" placeholder="Enter Link" name="link">
											</div>
										</div>
										
										<div class="form-group" style="display:none;" id="pdf">
											<label class="col-lg-2 control-label">Enter Icon:</label>
											<div class="col-lg-10">
												<input id="pdf" type="file" class="form-control" name="icon">
											</div>
										</div>
										<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="type"]').change(function(){
												    if(jQuery(this).val() == 'link'){
												    	jQuery("#link").show();
												    	jQuery("#pdf").hide();
												    }
												    else
												    {
												    	jQuery("#link").hide();
												    	jQuery("#pdf").show();
												    }
												});
											});
										</script>
									</fieldset>

									<div class="text-right">
										<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Submit form <i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				title:{
					required:true
				},
				posts:{
					required:true
				},
				postname:{
					required:true
				},
				website:{
					required:true
				},
				lastdate:{
					required:true
				},
				groupid:{
					required:true
				},
				// logo:{
				// 	required:true
				// },
			}
		});

	});
</script>
@endsection
@section('js')


<script>
	
	$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



</script>
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<script>
	$(document).ready(function(){
		alert('hello');
	});
	// $(document).on('keyup','#url',function(){
	// 		alert('el');
	// 		var val=$("#url").val();
	// 		if(val!="")
	// 		{
	// 			$(this).disabled();
	// 		}
	// 		else
	// 		{
	// 			$("#url").disabled();
	// 		}
	// 	});
	// 	$("input['name']=url").keyup(function(){
	// 		alert('hello');
	// 	});
</script>

