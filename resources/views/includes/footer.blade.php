<div class="navbar navbar-default navbar-fixed-bottom footer" style="background-color: #444">
		<ul class="nav navbar-nav visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="footer">
			<div class="navbar-text">
				&copy; 2020. All Rights reserved by <a href="#" class="navbar-link">{{ config('app.name') }}</a>.
			</div>

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="#">Help</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div>
		</div>
	</div>