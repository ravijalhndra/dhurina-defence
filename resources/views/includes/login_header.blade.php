<div class="navbar navbar-inverse bg-indigo has-shadow">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ url('dashboard') }}"><strong style="font-size: 20px;"> {{ config('app.name') }} </strong>{{-- <img src="assets/images/logo_light.png" alt=""> --}}</a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-grid3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				{{-- <li><a href="#">Help center</a></li> --}}
			</ul>

			<div class="navbar-right">
				<ul class="nav navbar-nav navbar-right">
					<li>
						
					</li>

					{{-- <li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-cog3"></i>
							<span class="visible-xs-inline-block position-right"> Options</span>
						</a>
					</li> --}}
				</ul>
			</div>
		</div>
	</div>