<div class="sidebar sidebar-main sidebar-default">
	<div class="sidebar-content">
		<div class="thumbnail">
			<div class="sidebar-user-material" style="">
				<div class="category-content">
					<div class="sidebar-user-material-content">
						<a href="javascript:;" class="legitRipple"><img style="height: 10%;" src="{{ url('/img/defence_logo.png') }}" class="img-circle img-responsive" alt=""></a>
					
						<h6 class="color-black">{{ config('app.name') }} Institute</h6>
						<span class="text-size-small color-black">Public</span>
					</div>	
				</div>							
			</div>
		</div>
	</div>
</div>