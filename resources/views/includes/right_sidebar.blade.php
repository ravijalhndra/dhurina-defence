<div class="sidebar sidebar-secondary sidebar-default" style="padding-left:20px">
				<div class="sidebar-content">

					<!-- Title -->
					<div class="category-title h6">
						<span>Secondary sidebar</span>
						<ul class="icons-list">
							<li><a href="#"><i class="icon-gear"></i></a></li>
						</ul>
					</div>
					<!-- /title -->


					<!-- Sidebar search -->
					<div class="sidebar-category">
						<div class="category-title">
							<span>Search</span>
							<ul class="icons-list">
								<li><a href="" data-action="collapse"></a></li>
							</ul>
						</div>

						<div class="category-content">
								<div class="has-feedback has-feedback-left">
									<input id="searchbar" type="search" class="form-control" placeholder="Search">
									<div class="form-control-feedback">
										<i class="icon-search4 text-size-base text-muted"></i>
									</div>
								</div>
								<br>
								<div class="has-feedback has-feedback-left">
									<input id="searchbar" type="checkbox">&nbsp;&nbsp;Unpublish
									
								</div>
						</div>
					</div>
					<!-- /sidebar search -->


					<!-- Action buttons -->
					<div class="sidebar-category">
						<div class="category-title">
							<span>Facebook Page</span>
							<ul class="icons-list">
								<li><a href="#" data-action="collapse"></a></li>
							</ul>
						</div>

						<div class="category-content" style="padding: 0px;">
								<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&tabs=timeline&width=240&height=350&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="240" height="350" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
						</div>
					</div>
					<!-- /action buttons -->


					<!-- Online users -->
					{{-- <div class="sidebar-category">
						<div class="category-title">
							<span>Online users</span>
							<ul class="icons-list">
								<li><a href="#" data-action="collapse"></a></li>
							</ul>
						</div>

						<div class="category-content">
							<ul class="media-list">
								<li class="media">
									<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt=""></a>
									<div class="media-body">
										<a href="#" class="media-heading text-semibold">James Alexander</a>
										<span class="text-size-mini text-muted display-block">Santa Ana, CA.</span>
									</div>
									<div class="media-right media-middle">
										<span class="status-mark border-success"></span>
									</div>
								</li>

								<li class="media">
									<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt=""></a>
									<div class="media-body">
										<a href="#" class="media-heading text-semibold">Jeremy Victorino</a>
										<span class="text-size-mini text-muted display-block">Dowagiac, MI.</span>
									</div>
									<div class="media-right media-middle">
										<span class="status-mark border-danger"></span>
									</div>
								</li>

								<li class="media">
									<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt=""></a>
									<div class="media-body">
										<a href="#" class="media-heading text-semibold">Margo Baker</a>
										<span class="text-size-mini text-muted display-block">Kasaan, AK.</span>
									</div>
									<div class="media-right media-middle">
										<span class="status-mark border-success"></span>
									</div>
								</li>

								<li class="media">
									<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt=""></a>
									<div class="media-body">
										<a href="#" class="media-heading text-semibold">Beatrix Diaz</a>
										<span class="text-size-mini text-muted display-block">Neenah, WI.</span>
									</div>
									<div class="media-right media-middle">
										<span class="status-mark border-warning"></span>
									</div>
								</li>

								<li class="media">
									<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-sm img-circle" alt=""></a>
									<div class="media-body">
										<a href="#" class="media-heading text-semibold">Richard Vango</a>
										<span class="text-size-mini text-muted display-block">Grapevine, TX.</span>
									</div>
									<div class="media-right media-middle">
										<span class="status-mark border-grey-400"></span>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- /online-users -->


					<!-- Latest updates -->
					<div class="sidebar-category">
						<div class="category-title">
							<span>Latest updates</span>
							<ul class="icons-list">
								<li><a href="#" data-action="collapse"></a></li>
							</ul>
						</div>

						<div class="category-content">
							<ul class="media-list">
								<li class="media">
									<div class="media-left">
										<a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
									</div>

									<div class="media-body">
										Drop the IE <a href="#">specific hacks</a> for temporal inputs
										<div class="media-annotation">4 minutes ago</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-commit"></i></a>
									</div>
									
									<div class="media-body">
										Add full font overrides for popovers and tooltips
										<div class="media-annotation">36 minutes ago</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn border-info text-info btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-branch"></i></a>
									</div>
									
									<div class="media-body">
										<a href="#">Chris Arney</a> created a new <span class="text-semibold">Design</span> branch
										<div class="media-annotation">2 hours ago</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn border-success text-success btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-merge"></i></a>
									</div>
									
									<div class="media-body">
										<a href="#">Eugene Kopyov</a> merged <span class="text-semibold">Master</span> and <span class="text-semibold">Dev</span> branches
										<div class="media-annotation">Dec 18, 18:36</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
									</div>
									
									<div class="media-body">
										Have Carousel ignore keyboard events
										<div class="media-annotation">Dec 12, 05:46</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- /latest updates -->


					<!-- Filter -->
					<div class="sidebar-category">
						<div class="category-title">
							<span>Filter</span>
							<ul class="icons-list">
								<li><a href="#" data-action="collapse"></a></li>
							</ul>
						</div>

						<div class="category-content">
							<form action="#">
								<div class="form-group">
									<div class="checkbox checkbox-switchery checkbox-right switchery-xs">
										<label class="display-block">
											<input type="checkbox" class="switchery" checked="checked">
											Free People
										</label>	
									</div>

									<div class="checkbox checkbox-switchery checkbox-right switchery-xs">
										<label class="display-block">
											<input type="checkbox" class="switchery">
											GAP
										</label>
									</div>

									<div class="checkbox checkbox-switchery checkbox-right switchery-xs">
										<label class="display-block">
											<input type="checkbox" class="switchery" checked="checked">
											Lane Bryant
										</label>
									</div>

									<div class="checkbox checkbox-switchery checkbox-right switchery-xs">
										<label class="display-block">
											<input type="checkbox" class="switchery" checked="checked">
											Ralph Lauren
										</label>
									</div>

									<div class="checkbox checkbox-switchery checkbox-right switchery-xs">
										<label class="display-block">
											<input type="checkbox" class="switchery">
											Liz Claiborne
										</label>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /filter -->


					<!-- Contacts -->
					<div class="sidebar-category">
						<div class="category-title">
							<span>Contacts</span>
							<ul class="icons-list">
								<li><a href="#" data-action="collapse"></a></li>
							</ul>
						</div>

						<div class="category-content">
							<ul class="media-list">
								<li class="media">
									<div class="media-left">
										<a href="#">
											<img src="assets/images/placeholder.jpg" class="img-xs img-circle" alt="">
											<span class="badge badge-info media-badge">6</span>
										</a>
									</div>

									<div class="media-body media-middle">
										Rebecca Jameson
									</div>

									<div class="media-right media-middle">
										<ul class="icons-list text-nowrap">
											<li>
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
												<ul class="dropdown-menu">
													<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
													<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
													<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#">
											<img src="assets/images/placeholder.jpg" class="img-xs img-circle" alt="">
											<span class="badge badge-info media-badge">9</span>
										</a>
									</div>

									<div class="media-body media-middle">
										Walter Sommers
									</div>

									<div class="media-right media-middle">
										<ul class="icons-list text-nowrap">
											<li>
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
												<ul class="dropdown-menu">
													<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
													<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
													<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#"><img src="assets/images/placeholder.jpg" class="img-xs img-circle" alt=""></a>
									</div>

									<div class="media-body media-middle">
										Otto Gerwald
									</div>

									<div class="media-right media-middle">
										<ul class="icons-list text-nowrap">
											<li>
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
												<ul class="dropdown-menu">
													<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
													<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
													<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#"><img src="assets/images/placeholder.jpg" class="img-xs img-circle" alt=""></a>
									</div>

									<div class="media-body media-middle">
										Vince Satmann
									</div>

									<div class="media-right media-middle">
										<ul class="icons-list text-nowrap">
											<li>
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
												<ul class="dropdown-menu">
													<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
													<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
													<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#"><img src="assets/images/placeholder.jpg" class="img-xs img-circle" alt=""></a>
									</div>

									<div class="media-body media-middle">
										Jason Leroy
									</div>

									<div class="media-right media-middle">
										<ul class="icons-list text-nowrap">
											<li>
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-more2"></i></a>
												<ul class="dropdown-menu">
													<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
													<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
													<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
													<li class="divider"></li>
													<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div> --}}
					<!-- /contacts -->

				</div>
			</div>