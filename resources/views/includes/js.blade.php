

	<!-- /theme JS files -->

<!-- Core JS files -->
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/ui/nicescroll.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/ui/drilldown.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/ui/fab.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/ui/ripple.min.js') }}"></script>
	<!-- /theme JS files -->
	{{-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
	<script src="https://unpkg.com/angular-toastr/dist/angular-toastr.tpls.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular-animate.min.js"></script> --}}

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.js"></script>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/additional-methods.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/layout_navbar_secondary_fixed.js')}}"></script>
	
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/ui/prism.min.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js')}}"></script>

		<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	
<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_multiselect.js')}}"></script>


<!-- Date JS files -->
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/pickers/anytime.min.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/pickers/pickadate/legacy.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/picker_date.js')}}"></script>


	
	
	<!-- /Date JS files -->


	<script type="text/javascript">
		$(document).ready(function(){
			//  $("link[href*=http://sandeepchaudhary.in/wp-content/themes/twentyfourteen/style.css]").attr("disabled", "disabled");
			$('link[rel=stylesheet][href*="http://sandeepchaudhary.in/wp-content/themes/twentyfourteen/style.css"]').remove();
			//  $('link[href="http://sandeepchaudhary.in/wp-content/themes/twentyfourteen/style.css"]').remove();
			$("div#wp_rp_first").hide();
			$("footer.entry-meta").hide();
			$("nav.post-navigation").hide();
		});

		// jQuery ".Class" SELECTOR.
		$(document).ready(function() {
			$('.onlynumber').keypress(function (event) {
				return isNumber(event, this)
			});
		});
		// THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
		function isNumber(evt, element) {
	
			var charCode = (evt.which) ? evt.which : event.keyCode
	
			if (
				(charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
				(charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
				(charCode < 48 || charCode > 57))
				return false;
	
			return true;
		}  
		
		//View more and less script
		function view_more(id)
		{
			var ids="#row"+id;	
			var more="#more"+id;	
			var less="#less"+id;	

			$(ids).css('height', 'auto');
			$(more).addClass('hide');
			$(less).removeClass('hide');
		}
		
		function view_less(id)
		{
			var ids="#row"+id;
			var more="#more"+id;	
			var less="#less"+id;

			$(ids).css('height', '20px');
			$(more).removeClass('hide');
			$(less).addClass('hide');
		}



		//BaseURL
		var base_url = window.location.origin+'/';
	</script>
	@if (session('warning'))
<script type="text/javascript">
    $.toast({
        heading: 'Warning',
        text: "{{ session('warning') }}",
        icon: 'warning',
        loader: true,
        loaderBg: '#ea2739',
        allowToastClose: true,
        position : 'top-right'
    });    
</script>
@php
    session()->forget('error');
@endphp
@endif
