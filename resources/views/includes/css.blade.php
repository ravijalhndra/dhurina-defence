<!-- Global stylesheets -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,300" rel="stylesheet" type="text/css">
	
	<link href="{{ URL::asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('assets/css/icons/fontawesome/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('assets/css/core.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('assets/css/components.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('assets/css/colors.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	{{-- <link rel="stylesheet" href="https://unpkg.com/angular-toastr/dist/angular-toastr.css" /> --}}

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.1/jquery.toast.min.css">

	<!--css for image upload preview-->
		<link href="{{ URL::asset('css/fileuploadpreview.css') }}" rel="stylesheet" type="text/css">

	<style>
		.dropdown-content{ z-index: 999! important}

		.uk-badge-warning{
			background: #ffa000;
			color: white;
			padding: 2px 6px;
			border: none;
			border-radius:100px;
			text-shadow: none;
			font-size: 11px;
			line-height: 15px;
			font-weight: 400;
			white-space: nowrap;
		}

		.uk-badge-danger{
			background: #e53935;
			color: white;
			padding: 2px 6px;
			border: none;
			border-radius:100px;
			text-shadow: none;
			font-size: 11px;
			line-height: 15px;
			font-weight: 400;
			white-space: nowrap;
		}
		.uk-badge-success{
			background: #7cb342;
			color: white;
			padding: 2px 6px;
			border: none;
			border-radius:100px;
			text-shadow: none;
			font-size: 11px;
			line-height: 15px;
			font-weight: 400;
			white-space: nowrap;
		}

		.bg-red 
		{
			background:red;
			border-color:red;
			color: white;
		}

		.bg-info 
		{
			background:#2296F3;
			border-color:#2296F3;
			color: white;
		}

		.bg-warning{			
			background:#ffa000;
			border-color:#ffa000;
			color: white;
		}

		.limited-text
		{
			height: 20px; 
			overflow: hidden; 
		}

		.table-width{
			width: 25%;
		}

		.margin-left{
			margin-left:20px;
		}

		.widht25{
			width: 85%;
		}

		.btn-pdf {
			padding: 4px 8px;
		}

		.hide{
			display: none;
		}

	</style>

