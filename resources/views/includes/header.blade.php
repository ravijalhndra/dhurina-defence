<div class="page-header page-header-inverse bg-indigo">

	<!-- Main navbar -->
	<div class="navbar navbar-inverse navbar-transparent">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ url('/dashboard') }}">
				<strong class="bg_header">{{ config('app.name') }}</strong>{{--
				<img src="assets/images/logo_light.png" alt=""> --}}</a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-grid3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">


			<ul class="nav navbar-nav">
				
				{{-- @php
				$role=Auth::user()->hasRole('admin');
				$can=Auth::user()->can(['edit-user', 'create-post'], true);

				$role1=Auth::user()->hasRole('superadmin');
				$role2=Auth::user()->hasRole('institute_admin');
				$role3=Auth::user()->hasRole('institute_sub_admin');
				@endphp
				@if ($role=='true')
				<li><a href="#"><strong>Admin</strong></a></li>
				@elseif($role1=='true')
				<li><a href="#"><strong>SuperAdmin</strong></a></li>
				@elseif($role2=='true')
				<li><a href="#"><strong>Institute Admin</strong></a></li>
				@elseif($role3=='true')
				<li><a href="#"><strong>Institute sub Admin</strong></a></li>

				@endif --}}


			</ul>

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					{{-- <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-bell2"></i>
							<span class="visible-xs-inline-block position-right">Activity</span>
							<span class="status-mark border-pink-300"></span>
						</a>

						<div class="dropdown-menu dropdown-content">
							<div class="dropdown-content-heading">
								Activity
								<ul class="icons-list">
									<li><a href="#"><i class="icon-menu7"></i></a></li>
								</ul>
							</div>

							<ul class="media-list dropdown-content-body width-350">
								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-success-400 btn-rounded btn-icon btn-xs"><i class="icon-mention"></i></a>
									</div>

									<div class="media-body">
										<a href="#">Taylor Swift</a> mentioned you in a post "Angular JS. Tips and tricks"
										<div class="media-annotation">4 minutes ago</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-pink-400 btn-rounded btn-icon btn-xs"><i class="icon-paperplane"></i></a>
									</div>

									<div class="media-body">
										Special offers have been sent to subscribed users by <a href="#">Donna Gordon</a>
										<div class="media-annotation">36 minutes ago</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-blue btn-rounded btn-icon btn-xs"><i class="icon-plus3"></i></a>
									</div>

									<div class="media-body">
										<a href="#">Chris Arney</a> created a new <span class="text-semibold">Design</span> branch in <span class="text-semibold">Limitless</span>
										repository
										<div class="media-annotation">2 hours ago</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-purple-300 btn-rounded btn-icon btn-xs"><i class="icon-truck"></i></a>
									</div>

									<div class="media-body">
										Shipping cost to the Netherlands has been reduced, database updated
										<div class="media-annotation">Feb 8, 11:30</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-warning-400 btn-rounded btn-icon btn-xs"><i class="icon-bubble8"></i></a>
									</div>

									<div class="media-body">
										New review received on <a href="#">Server side integration</a> services
										<div class="media-annotation">Feb 2, 10:20</div>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs"><i class="icon-spinner11"></i></a>
									</div>

									<div class="media-body">
										<strong>January, 2016</strong> - 1320 new users, 3284 orders, $49,390 revenue
										<div class="media-annotation">Feb 1, 05:46</div>
									</div>
								</li>
							</ul>
						</div>
					</li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-bubble8"></i>
							<span class="visible-xs-inline-block position-right">Messages</span>
						</a>

						<div class="dropdown-menu dropdown-content width-350">
							<div class="dropdown-content-heading">
								Messages
								<ul class="icons-list">
									<li><a href="#"><i class="icon-menu7"></i></a></li>
								</ul>
							</div>

							<ul class="media-list dropdown-content-body">
								<li class="media">
									<div class="media-left">
										<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
										<span class="badge bg-danger-400 media-badge">5</span>
									</div>

									<div class="media-body">
										<a href="#" class="media-heading">
											<span class="text-semibold">James Alexander</span>
											<span class="media-annotation pull-right">04:58</span>
										</a>

										<span class="text-muted">who knows, maybe that would be the best thing for me...</span>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
										<span class="badge bg-danger-400 media-badge">4</span>
									</div>

									<div class="media-body">
										<a href="#" class="media-heading">
											<span class="text-semibold">Margo Baker</span>
											<span class="media-annotation pull-right">12:16</span>
										</a>

										<span class="text-muted">That was something he was unable to do because...</span>
									</div>
								</li>

								<li class="media">
									<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
									<div class="media-body">
										<a href="#" class="media-heading">
											<span class="text-semibold">Jeremy Victorino</span>
											<span class="media-annotation pull-right">22:48</span>
										</a>

										<span class="text-muted">But that would be extremely strained and suspicious...</span>
									</div>
								</li>

								<li class="media">
									<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
									<div class="media-body">
										<a href="#" class="media-heading">
											<span class="text-semibold">Beatrix Diaz</span>
											<span class="media-annotation pull-right">Tue</span>
										</a>

										<span class="text-muted">What a strenuous career it is that I've chosen...</span>
									</div>
								</li>

								<li class="media">
									<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
									<div class="media-body">
										<a href="#" class="media-heading">
											<span class="text-semibold">Richard Vango</span>
											<span class="media-annotation pull-right">Mon</span>
										</a>

										<span class="text-muted">Other travelling salesmen live a life of luxury...</span>
									</div>
								</li>
							</ul>
						</div>
					</li> --}}

					<li class="dropdown dropdown-user">
						<a class="dropdown-toggle " data-toggle="dropdown">
							<img src="{{ url('img/defence_logo.png') }}" alt="">
							<span class="bg_header">{{ Auth::user()->name }}</span>
							<i class="caret"></i>
						</a>

						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="{{ url('change_password') }}"><i class="icon-user-plus"></i> Change Password</a></li>
							{{-- <li><a href="#"><i class="icon-coins"></i> My balance</a></li> --}}
							{{-- <li><a href="#"><span class="badge bg-blue pull-right">26</span> <i class="icon-comment-discussion"></i>
									Messages</a></li> --}}
							<li class="divider"></li>
							{{-- <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li> --}}
							<li><a onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
								 href="{{ route('logout') }}"><i class="icon-switch2"></i> Logout</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page header content -->
	{{-- <div class="page-header-content">
		<div class="page-title">
			<h4>Dashboard <small>Good morning, Eugene</small></h4>
		</div>

		<div class="heading-elements">
			<ul class="breadcrumb heading-text">
				<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
				<li class="active">Dashboard</li>

			</ul>
		</div>
	</div> --}}
	<!-- /page header content -->


	<!-- Second navbar -->
	<div class="navbar navbar-inverse navbar-transparent bg" id="navbar-second">
		<ul class="nav navbar-nav visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-paragraph-justify3"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="navbar-second-toggle">
			<ul class="nav navbar-nav navbar-nav-material">



				{{-- <li class="active"><a href="{{ url('dashboard') }}">Dashboard</a></li>&nbsp;&nbsp;&nbsp;&nbsp; --}}


				<div class="dropdown">
					<div class="dropbtn bg-header-button">Dashboard & About</div>
					<div class="dropdown-content">
						
						<li>
							<a href="{{url('dashboard')}}">
								Dashboard
							</a>
						</li>

						@if(Auth::user()->role == 'admin' ||  Auth::user()->role == 'school')
							<li>
								<a href="{{url('About')}}">
									About
								</a>
							</li>

							<li>
								<a href="{{route('view_users')}}">
									Users
								</a>
							</li>
							<!-- <li>
								<a href="{{route('view_user_posts')}}">
									Discussion Forum
								</a>
							</li> -->

						@endif

						@if(Auth::user()->role == 'admin')
							<li>
								<a href="{{route('notification')}}">
									Notification
								</a>
							</li>
						@endif	

					</div>
				</div>

				{{-- <div class="dropdown">
					<div class="dropbtn bg-header-button"> <a class="white-color" href="{{url('About')}}">About </a> </div>					
				</div> --}}

				{{--  <div class="dropdown">
					<div class="dropbtn bg-header-button">Users</div>
					<div class="dropdown-content">

						@permission('view_admin')
						<li>
							<a href="{{ url('view_admin') }}" class="">
								Admins
							</a>
						</li>
						@endpermission
						@permission('view_users')
						<li>
							<a href="{{ url('view_users') }}" class="">
								App Users
							</a>
						</li>
						@endpermission
						@permission('groups')
						<li>
							<a href="{{url('groups')}}">
								Groups
							</a>

						</li>
						@endpermission
						<li>
							<a href="{{url('categories')}}">
								Categories
							</a>
						</li>
					</div>
				</div>  --}}

				

					<div class="dropdown">
						<div class="dropbtn bg-header-button">Test & Post</div>
						<div class="dropdown-content">
									
							<li>
								<a href="{{ url('add_test') }}" class="">
									Add Test
								</a>
							</li>

							<li>
								<a href="{{ url('view_test') }}" class="">
									View Test
								</a>
							</li>

							<li>
								<a href="{{ route('bulk_question') }}" class="">
									Bulk upload Question
								</a>
							</li>
							

							{{-- <li>
								<a href="{{ url('view_unpublish_test') }}" class="">
									Unpublish Test
								</a>
							</li> --}}

							<li>
								<a href="{{ url('view_user_posts') }}" class="">
									Approve post
								</a>
							</li>

							<li>
								<a href="{{ url('view-test-series') }}" class="">
									View Test Series
								</a>
							</li>

							<li>
								<a href="{{ url('add-test-series') }}" class="">
									Add Test Series
								</a>
							</li>

						</div>
					</div>

				
				
					{{--  <div class="dropdown">
						<div class="dropbtn bg-header-button"> <a class="white-color" href="{{url('view_pdf')}}"> PDF & Notes </a> </div>					
					</div>  --}}


					<div class="dropdown">
						<div class="dropbtn bg-header-button">PDF & Ebook</div>
						<div class="dropdown-content">
							@if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin' )
								<li>
									<a href="{{ url('pdf') }}" class="">
										Add PDF
									</a>
								</li>
							@endif	

							<li>
								<a href="{{ url('view_pdf2') }}" class="">
									View PDF
								</a>
							</li>

							@if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin' )
								<li>
									<a href="{{ route('add_note') }}">
										Add Notes & Ebook
									</a>
								</li>

								<li>
									<a href="{{ route('view_note') }}" >
										View  Notes & Ebook
									</a>
								</li>
							@endif

						</div>
					</div>

				

				<!-- <div class="dropdown">
					<div class="dropbtn bg-header-button">Schools </div>
					<div class="dropdown-content">
						
						<li>
							<a href="{{url('add_college')}}">
								Add New School
							</a>
						</li>

						<li>
							<a href="{{url('view_department')}}">
								Streams
							</a>
						</li>
						
						<li>
							<a href="{{url('view_college')}}">
								View School
							</a>
						</li>
						
						<li>
							<a href="{{url('student_college')}}">
								List of Students
							</a>
						</li>

						<li>
							<a href="{{url('view_college_list')}}">
								List of Schools
							</a>
						</li>

						<li>
							<a href="{{url('add_room')}}">
								Create Meeting Room
							</a>
						</li>

						<li>
							<a href="{{url('view_room')}}">
								View Meeting Room
							</a>
						</li>
						
					</div>
				</div> -->

				{{--  <div class="dropdown">
					<div class="dropbtn bg-header-button">Payment</div>
					<div class="dropdown-content">
						
						<li>
							<a href="{{url('view_payment')}}">
								Payment History
							</a>
						</li>
						

					</div>
				</div>  --}}

				@if(Auth::user()->role == 'admin' || Auth::user()->role == 'school')
					<div class="dropdown">
						<div class="dropbtn bg-header-button" >Banner</div>
						<div class="dropdown-content">						
							<li>
								<a href="{{url('add_banner')}}">
									Add Banner
								</a>
							</li>

							<li>
								<a href="{{url('view_banner')}}">
									View Banner
								</a>
							</li>						
						</div>
					</div>
				@endif

				@if(Auth::user()->role == 'admin' || Auth::user()->role == 'school')
				<div class="dropdown">
					<div class="dropbtn bg-header-button"> @if(Auth::user()->role == 'admin' ) Courses @else Class @endif</div>
					<div class="dropdown-content">
						
						@if(Auth::user()->role == 'admin' )
							<li>
								<a href="{{route('add_course')}}">
									Add New @if(Auth::user()->role == 'admin' ) Courses @else Class @endif
								</a>
							</li>
						@endif

						<li>
							<a href="{{route('view_course')}}">
								View @if(Auth::user()->role == 'admin' ) Courses @else Class @endif
							</a>
						</li>

						@if(Auth::user()->role == 'admin' )
							<li>
								<a href="{{route('add_sector')}}">
									Add New Sector
								</a>
							</li>

							<li>
								<a href="{{route('view_sector')}}">
									View Sector
								</a>
							</li>
						@endif

					</div>
				</div>
				@endif

				@if(Auth::user()->role == 'admin' || Auth::user()->role == 'school')
				<div class="dropdown">
					<div class="dropbtn bg-header-button">Live Event</div>
					<div class="dropdown-content">
						<li>
							<a href="{{route('add_event')}}">
								Add New Event
							</a>
						</li>
						

						<li>
							<a href="{{route('view_event')}}">
								View Event
							</a>
						</li>
					</div>
				</div>
				@endif

				<!-- @if(Auth::user()->role == 'admin' )
					<div class="dropdown">
						<div class="dropbtn bg-header-button">School</div>
						<div class="dropdown-content">
							
							<li>
								<a href="{{route('add_school')}}">
									Add New School
								</a>
							</li>

							<li>
								<a href="{{route('view_school')}}">
									View School
								</a>
							</li>
						</div>
					</div>					
				@endif -->

				@if(Auth::user()->role == 'admin' || Auth::user()->role == 'school')
					<div class="dropdown">
						<div class="dropbtn bg-header-button">Subject</div>
						<div class="dropdown-content">

							@if(Auth::user()->role == 'admin' )
							<li>
								<a href="{{route('add_subject')}}">
									Add New Subject
								</a>
							</li>
							@endif

							<li>
								<a href="{{route('view_subject')}}">
									View Subject
								</a>
							</li>

							@if(Auth::user()->role == 'admin' )
								<li>
									<a href="{{route('add_topic')}}">
										Add Topic
									</a>
								</li>

								<li>
									<a href="{{route('view_topic')}}">
										View Topic
									</a>
								</li>
							@endif

						</div>
					</div>
				@endif	

				
				<div class="dropdown">
					<div class="dropbtn bg-header-button">Video</div>
					<div class="dropdown-content">
						
						@if(Auth::user()->role == 'admin' || Auth::user()->role == 'sub_admin' )
							<li>
								<a href="{{route('add_video')}}">
									Add New video
								</a>
							</li>

							<li>
								<a href="{{route('sort_video')}}">
									Sort Video
								</a>
							</li>
						@endif
						
						<li>
							<a href="{{route('view_video')}}">
								View video
							</a>
						</li>
						
					</div>
				</div>
				
				@if(Auth::user()->role == 'admin' || Auth::user()->role == 'school')
					<div class="dropdown">
						<div class="dropbtn bg-header-button">Assign Course</div>
						<div class="dropdown-content">
							
							<li>
								<a href="{{route('assign_course')}}">
									Assign Course
								</a>
							</li>

							<li>
								<a href="{{route('assign_course_view')}}">
									View Assigned Course
								</a>
							</li>

							<li>
								<a href="{{route('assign_ebook_view')}}">
									View Assigned Ebook & Notes
								</a>
							</li>

							<li>
								<a href="{{route('assign_manully_course')}}">
									Assign manually course
								</a>
							</li>

							@if(Auth::user()->role == 'admin' )
							<li>
								<a href="{{route('view_payment')}}">
									View Online Orders
								</a>
							</li>
							@endif
						</div>
					</div>
				@endif	


				@if(Auth::user()->role == 'admin')
					<div class="dropdown">
						<div class="dropbtn bg-header-button">Academy</div>
						<div class="dropdown-content">
							<li>
								<a href="{{route('view_academy_info')}}">
								  View Academy Info
								</a>
							</li>

						</div>
					</div>
				@endif	

				
				


			</ul>

			<ul class="nav navbar-nav navbar-nav-material navbar-right">

			</ul>
		</div>
	</div>
	<!-- /second navbar -->


	<!-- Floating menu -->
	{{-- <ul class="fab-menu fab-menu-top-right" data-fab-toggle="click">
		<li>
			<a class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
				<i class="fab-icon-open icon-plus3"></i>
				<i class="fab-icon-close icon-cross2"></i>
			</a>

			<ul class="fab-menu-inner">
				<li>
					<div data-fab-label="Compose email">
						<a href="#" class="btn btn-default btn-rounded btn-icon btn-float">
							<i class="icon-pencil"></i>
						</a>
					</div>
				</li>
				<li>
					<div data-fab-label="Conversations">
						<a href="#" class="btn btn-default btn-rounded btn-icon btn-float">
							<i class="icon-bubbles7"></i>
						</a>
						<span class="badge bg-primary-400">5</span>
					</div>
				</li>
				<li>
					<div data-fab-label="Chat with Jack">
						<a href="#" class="btn bg-pink-400 btn-rounded btn-icon btn-float">
							<img src="assets/images/placeholder.jpg" class="img-responsive" alt="">
						</a>
						<span class="status-mark border-pink-400"></span>
					</div>
				</li>
			</ul>
		</li>
	</ul> --}}
	<!-- /floating menu -->

</div>
<style>
	.white-color
	{
		color: #fff;
	}

	.white-color:hover{
		color: #000;
	}

	.dropbtn {
		/* background-color:#4d70ff;  */
		color:#fff;
		padding: 16px;
		font-size: 14px;
		border: none;
	}

	.dropdown {
		position: relative;
		display: inline-block;
	}

	.dropdown-content {
		display: none;
		position: absolute;
		background-color: white;
		color: white;
		min-width: 160px;
		box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
		z-index: 1;
	}

	.dropdown-content a {
		color: black;
		padding: 12px 16px;
		text-decoration: none;
		display: block;
	}

	.dropdown-content a:hover {
		background-color: #ddd;
	}

	.dropdown:hover .dropdown-content {
		display: block;
	}

	.dropdown:hover .dropbtn {
		/* background-color: #444; */
		color: #000;
	}
</style>