@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>
<script>
											$(document).ready(function(){
												$("#college_select").hide();
												$("#stream_select").hide();
												$("#course_select").hide();
												$("#year_select").hide();
												$("#groups_institute").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();	
														$("#groups_select").show();
												});
												$("#college_institute").click(function(){
														$("#college_select").show();
														$("#stream_select").show();
														$("#course_select").show();
														$("#year_select").show();
														$("#groups_select").hide();
												});
												$("#both_institute").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();
														$("#groups_select").hide();
												});
												
											});
									</script>

<script type="text/javascript" src="{{ URL::asset('theme/HTML/assets/plugins/summernote/dist/summernote.min.js')}}"></script>
<link href="{{ URL::asset('theme/HTML/assets/plugins/summernote/dist/summernote.css')}}" rel="stylesheet" />
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/forms/tags/tagsinput.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/forms/tags/tokenfield.min.js"></script>
	
	<script type="text/javascript" src="assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>


	<script type="text/javascript" src="assets/js/pages/form_tags_input.js"></script>


@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form action="{{ url('add_workshop') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data" novalidate>
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">WorkShop Detail</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
								<div class="form-group">
										<label class="col-lg-3 control-label">Select:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" id="groups_institute" name="type_institute" value="group" checked="checked">
												Groups
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" id="college_institute" name="type_institute" value="college">
												College
											</label>
											<label class="radio-inline">
												<input type="radio" class="styled" id="both_institute" name="type_institute" value="both">
												Both
											</label>
										</div>
									</div>
									
									
									<div class="form-group" id="course_select">
									<label class="col-lg-3 control-label">Select Course:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="course" data-placeholder="Select Course..." id="coursedropdown" class="course-selected form-control" required>
										<!-- multiple="multiple"   -->
											@if($course)
												@foreach ($course as $cou_rse)
												@if($cou_rse)
													<option value="{{ $cou_rse->id }}">{{ $cou_rse->course_name }}</option>
												@endif
												@endforeach
												@endif
										</select>
										<input type="hidden" value="{{route('get_course')}}" name="urlcourse" class="urlcourse"/>
									</div>
								</div>

								<div class="form-group" id="college_select">
									<label class="col-lg-3 control-label">Select College:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="college" data-placeholder="Select College..." id="collagedropdown" class="college-selected form-control" required>
										<option value="">Select College </option>
										</select>
										{{--<!-- <input type="hidden" value="{{ route('get_stream') }}" name="urlcollege" class="urlcollege"/> -->--}}
									</div>
								</div>

								<div class="form-group" id="stream_select">
									<label class="col-lg-3 control-label">Select Stream:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="stream" data-placeholder="Select Stream..." class="stream-selected form-control" required>
											<option value="">Select Stream</option>
										</select>
									</div>
								</div>

								<div class="form-group" id="year_select">
									<label class="col-lg-3 control-label">Select Year:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="year" data-placeholder="Select Year..."  class="year-selected form-control" required>
													<option value=""></option>
										</select>
									</div>
								</div>
									<div class="form-group" id="groups_select"> 
											<label class="col-lg-3 control-label">Select Groups:</label>
											<div class="col-lg-9 multi-select-full">
												<select name="groupid[]" data-placeholder="Select groups..." multiple="multiple" class="multiselect-select-all-filtering" >
												@if($data[0]!="")
												@foreach ($data as $d)
												@if($d)
													<option value="{{ $d->id }}">{{ $d->topic }}</option>
												@endif
												@endforeach
												@endif
												</select>
											</div>
										</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Title:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="title">
										</div>
									</div>

									<div class="form-group">
										
										<label class="col-lg-3 control-label">Description:</label>
										<div class="col-lg-9">
												<textarea  name="description" class="summernote"></textarea>
											</div>
									</div>
									
									
		
									<div class="form-group">
										<label class="col-lg-3 control-label">Price:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" name="payment" value="free" checked="checked">
												Free
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" name="payment" value="paid">
												Paid
											</label>
										</div>
									</div>
									
										<div class="form-group" style="display:none" id="payment1">
										<label class="col-lg-3 control-label">Amount:</label>
										<div class="col-lg-9">
											<input type="number" class="form-control" name="amount" >
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Workshop Image:</label>
										<div class="col-lg-9">
											<input type="file" class="file-styled" name="upload">
											<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Contact Number:</label>
										<div class="col-lg-9">
											<input type="number" class="form-control" name="phone" placeholder="9999999999"maxlength="10">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">URL:</label>						
												<div class="col-lg-9">
											<input type="text" class="form-control" name="url" placeholder="www.xyz.com">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Location:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" name="location" placeholder="Example :-New Delhi">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Date:</label>
										<div class="col-lg-9">
											<div class="content-group-lg">
									<div class="input-group">
										<span class="input-group-addon"></span>
											
										<input type="text" class="form-control pickadate-accessibility" id="txtstartdate" name="date" placeholder="Try me&hellip;">
									</div>
								</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-lg-3 control-label">Registration Form Fields:</label>
										<div class="col-lg-9">
												<!-- Default usage -->
								<div class="content-group">
									<p>Enter the name of the required field and press tab or enter for adding another field..</p>

									<div class="content-group">
										<input type="text" value="Full Name,Email Id" name="tags" class="tags-input">
									</div>
							
								</div>
										</div>
									</div>
								{{-- <fieldset>
											<legend></legend>
											<div class="form-group">
												<div class="col-lg-4">
													<label>Users are able to like this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_like" type="checkbox" class="switchery" checked>
													</label>
												</div>
											</div>
											<div class="form-group">
												<div class="col-lg-4">
													<label>Users are able to comment this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_comment" type="checkbox" class="switchery" checked>
													</label>
												</div>
											</div>
										</fieldset> --}}

									<div class="text-right">
										<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>

@endsection