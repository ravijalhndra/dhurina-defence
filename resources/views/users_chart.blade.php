

			<div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			

		</div>
<div class="row">
    <h2>Latest Users</h2>
    <div class="row">
    @foreach ($latest_users as $user)
        <div class="col-lg-2 col-sm-6">
            <div class="thumbnail">
                <div class="thumb thumb-rounded">
                    <img src="{{ $user->image }}" alt="">
                    <div class="caption-overflow">
                        <span>
                            <a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
                            <a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
                        </span>
                    </div>
                </div>
            
                <div class="caption text-center">
                    <h6 style="min-height: 80px; max-height: 80px;" class="text-semibold no-margin">{{ $user->name }}<small class="display-block">{{ $user->college }}</small></h6>
                    <p style="padding-top: 5px; min-height: 20px; max-height: 20px;">@if($user->city==""){{ $user->state }}@else{{ $user->city }}, {{ $user->state }}@endif</p>
                    
                </div>
            </div>
        </div>
    @endforeach
    </div>
</div>


@php
$cat=$dd['cat'];
foreach($cat as $c)
{
    $timestamp=strtotime($c);
    $date=date('d M Y',$timestamp);
    $cc[]="'$date'";
}
$cat=implode(',',$cc);
$users=$dd['users'];
$users=implode(",",$users);
// dd($users);

@endphp
<script type="text/javascript">
	Highcharts.chart('container1', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Daily Registered Users'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [<?php echo $cat; ?>],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'No. of users'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Registered Users',
        data: [{{ $users }}]

    }]
});
</script>
