@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					@if(Auth::user()->role == 'admin' )
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_banner') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
					@endif
					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Live Event</h3>
					</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Course Name</th>
								<th>Name</th>								
                                <th>Start</th>
                                <th>Access Status</th>
								<th>Publish Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@foreach($data as $u)
							<tr>
								<td>{{$u->id}}</td>
								<td><img src="{{ URL::to('/') }}/img/{{$u->image}}" height="80" width="80"></td>								
                                <td>{{$u->course_name}}</td>
                                <td>{{$u->name}}</td>
                                <td>{{ date('m/d/Y H:i', $u->start_time)}}</td>
								<td>{{ucfirst($u->access) }}</td>
                                <td>
                                    <span class="fab-menu-btn btn-sm @if($u->publish == 'false') bg @endif  bg-success btn-rounded "> @if($u->publish == 'true') Published @else Unpublish  @endif </span>  
                                </td>
                                <td class="text-center">
									@if(Auth::user()->role == 'admin' )
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
                                            <ul class="dropdown-menu dropdown-menu-right">
												<li><a href="javascript:;"  onClick="fun_delete({{ $u->id }})"><i class="icon-trash"></i> Delete</a></li>
												<li><a href="{{route('edit_event',encrypt ($u->id))}}"><i class="icon-pencil"></i> Edit</a></li>
													@if($u->publish == 'true')
														<li><a href="javascript:;" onClick="fun_publish({{ $u->id }},'false')" ><i class="icon-pencil"></i> Unpublish</a></li>
													@else
														<li><a href="javascript:;" onClick="fun_publish({{ $u->id }},'true')" ><i class="icon-pencil"></i> Publish</a></li>
													@endif
											</ul>
										</li>
									</ul>
									@endif
								</td> 
                          	</tr>				
						@endforeach								
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div  data-backdrop="false" id="modal_mini" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Click on Yes if you want to delete this Record.</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('delete_event') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del_id" name="del_id" value="">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div  data-backdrop="false" id="modal_2" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Are you want want to change this record?</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('publish_event') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del" name="del" value="">
							<input type="hidden" id="type" name="type" value="">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

@endsection
@section('js')

	<script type="text/javascript">

		function fun_delete(id)
		{
			$('#del_id').val(id);
			$('#modal_mini').modal('toggle');		
		}

		function fun_publish(id,type)
		{
			$('#del').val(id);
			$('#type').val(type);
			$('#modal_2').modal('toggle');	
		}

	</script>
@endsection

