@extends('layouts.main')

@section('js_head')
{{-- <style type="text/css">
	.box-size{
		max-height: 200px;
		overflow: hidden;
		text-overflow: ellipsis;
	}
</style> --}}
<script type="text/javascript" src="http://viralpatel.net/blogs/demo/jquery/jquery.shorten.1.0.js"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/notifications/pnotify.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/components_notifications_pnotify.js') }}></script>

<style>
.pagination{margin-left: 30px;}
</style>


@endsection
@section('content')
		<!-- Page content -->
		<div class="page-content">
			<!-- Main sidebar -->
			@include('includes.left_sidebar')
			<!-- /main sidebar -->
		<!-- Main content -->
				<div class="content-wrapper">
				
				    <div id="post_section" class="row">
				
				        <!--get the result from Workshop Conrtroller-->
				        <!--checking the conditiion for result count-->
				        @if(!empty($record))
				       
				        <!--getting the record  from the array value $record-->
				       @foreach ($record as $data)

								@php
								$json=json_decode($data->jsondata,true);
								
								$result=json_decode(json_encode($data->results),true);

							    $timestamp=strtotime($data->timestamp);
							
							@endphp
					

				        	<div class="col-md-12">

						<!-- Blog layout #4 with image -->
						<div class="panel panel-flat">
						    
							@if($data->field_type==0)
							<div class="panel-footer panel-footer-condensed">
						    <div style="float:left; width: 100px;">
						    	<img style="border-radius: 50%;margin-top: 10px; margin-left: 20px;" src="{{ URL::asset('img/Blank-avatar.png') }}" width="60" height="60">

						    </div>
						    <div style="float: left; width: 300px; margin-top: 15px; margin-left: 10px;">
						    	<span style="font-size: 16px;"><strong>@if(isset($json['name'])){{ $json['name'] }} @endif</strong></span><br>
						    	<span>@if(isset($json['collage'])){{ $json['collage'] }}@endif</span>
						    </div>
						    <div style="float: right; margin-right: 20px; margin-top: 15px;">
						    <span>{{ date('M d, Y',$timestamp) }}</span>
						    
						    </div>
						    
							</div>
							@if(isset($json['com_image']) && $json['com_image']!="null")
							<div class="thumb content-group">
									<img style="height: 300px;" src="/mechanicalinsider/{{ $json['com_image'] }}" alt="" class="">
									<div class="caption-overflow">
										{{-- <span>
											<a href="{{ url('post/'.$data->id) }}" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
										</span> --}}
									</div>
								</div>
								@endif
							<div " class="panel-body">
								

								{{-- <h5 class="text-semibold mb-5">
									<a href="{{ url('post/'.$data->id) }}" class="text-default">{{ $json['title'] }}</a>
								</h5> --}}


								<div class="less_section">@if(isset($json['comments'])){!! $json['comments'] !!} @endif</div>
								<div class="clearfix"></div>
							</div>
							
							@elseif($data->field_type==7)
							<div class="panel-footer panel-footer-condensed">
						    <div style="float:left; width: 100px;">
						    	<img style="border-radius: 50%;margin-top: 10px; margin-left: 20px;" src="@if(isset($json['uimage'])){{ $json['uimage'] }} @endif" width="60" height="60">

						    </div>
						    <div style="float: left; width: 300px; margin-top: 15px; margin-left: 10px;">
						    	<span style="font-size: 16px;"><strong>@if(isset($json['username'])){{ $json['username'] }} @endif</strong></span><br>
						    	<span>@if(isset($json['collage'])){{ $json['collage'] }}@endif</span>
						    </div>
						    <div style="float: right; margin-right: 20px; margin-top: 15px;">
						    <span>{{ date('M d, Y',$timestamp) }}</span>
						    
						    </div>
						    
							</div>
							@if(isset($json['post_image']))
							<h5 style="padding: 5px 10px;" class="text-semibold mb-5">
									<a href="" class="text-default less_section ">{{ $json['question'] }}</a>
							</h5>
								@if($json['post_image']!="null")
							<div class="thumb content-group">
									<img style="height:300px;" src="/mechanicalinsider/{{ $json['post_image'] }}" alt="" class="">
									<div class="caption-overflow">
										{{-- <span>
											<a href="{{ url('post/'.$data->id) }}" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
										</span> --}}
									</div>
								</div>
								@endif
								@endif
							<div class="panel-body">
							@if (isset($json['a'])&&$json['ans']=="a" )
								<div style="background: #3eef50; padding: 10px; border-radius: 4px; margin-bottom: 10px;" class="col-md-12">
									<div class="col-md-1">
										<span style="border: 1px solid black; border-radius: 50%; padding: 5px 10px; background: white;"><strong>A</strong></span>
									</div>
									<div class="col-md-11">
										<strong>{{ $json['a'] }}</strong>
										<span class="pull-right"><i class="fa fa-check"></i> @if(isset($result['a%'])) {{ $result['a%'] }} % @endif</span>
									</div>
								</div>	
							@elseif (isset($json['a']))
								<div style=" background: #e5e7ea; padding: 10px; border-radius: 4px; margin-bottom: 10px;" class="col-md-12">
									<div class="col-md-1">
										<span style="border: 1px solid black; border-radius: 50%; padding: 5px 10px; background: white;"><strong>A</strong></span>
									</div>
									<div class="col-md-11">
										<strong>{{ $json['a'] }}</strong>
										<span class="pull-right">  @if(isset($result['a%'])) {{ $result['a%'] }} % @endif</span>

									</div>
								</div>
							@endif
							@if (isset($json['b'])&&$json['ans']=="b" )
							<div style=" background: #3eef50; padding: 10px; border-radius: 4px; margin-bottom: 10px;" class="col-md-12">
									<div class="col-md-1">
										<span style="border: 1px solid black; border-radius: 50%; padding: 5px 10px; background: white;"><strong>B</strong></span>
									</div>
									<div class="col-md-11">
										<strong>{{ $json['b'] }}</strong>
										<span class="pull-right"><i class="fa fa-check"></i>  @if(isset($result['b%'])) {{ $result['b%'] }} % @endif </span>
									</div>
								</div>
							@elseif (isset($json['b']) && $json['b']!='')
								<div style=" background: #e5e7ea; padding: 10px; border-radius: 4px; margin-bottom: 10px;" class="col-md-12">
									<div class="col-md-1">
										<span style="border: 1px solid black; border-radius: 50%; padding: 5px 10px; background: white;"><strong>B</strong></span>
									</div>
									<div class="col-md-11">
										<strong>{{ $json['b'] }}</strong>
										<span class="pull-right">  @if(isset($result['b%'])) {{ $result['b%'] }} %  @endif</span>
									</div>
								</div>
							@endif
							@if (isset($json['c'])&&$json['ans']=='c' && $json['c']!='')
							<div style=" background: #3eef50; padding: 10px; border-radius: 4px; margin-bottom: 10px;" class="col-md-12">
									<div class="col-md-1">
										<span style="border: 1px solid black; border-radius: 50%; padding: 5px 10px; background: white;"><strong>C</strong></span>
									</div>
									<div class="col-md-11">
										<strong>{{ $json['c'] }}</strong>
										<span class="pull-right"><i class="fa fa-check"></i>@if(isset($result['c%'])) {{ $result['c%'] }} % @endif</span>
									</div>
								</div>
							@elseif (isset($json['c']) && $json['c']!='')
								<div style=" background: #e5e7ea; padding: 10px; border-radius: 4px; margin-bottom: 10px;" class="col-md-12">
									<div class="col-md-1">
										<span style="border: 1px solid black; border-radius: 50%; padding: 5px 10px; background: white;"><strong>C</strong></span>
									</div>
									<div class="col-md-11">
										<strong>{{ $json['c'] }}</strong>
										<span class="pull-right">  @if(isset($result['c%'])) {{ $result['c%'] }} % @endif</span>
									</div>
								</div>
							@endif
							@if (isset($json['d'])&&$json['ans']=="d" && $json['d']!='')
							<div style=" background: #3eef50; padding: 10px; border-radius: 4px; margin-bottom: 10px;" class="col-md-12">
									<div class="col-md-1">
										<span style="border: 1px solid black; border-radius: 50%; padding: 5px 10px; background: white;"><strong>D</strong></span>
									</div>
									<div class="col-md-11">
										<strong>{{ $json['d'] }}</strong>
										<span class="pull-right"><i class="fa fa-check"></i> @if(isset($result['d%'])) {{ $result['d%'] }} %  @endif</span>
									</div>
								</div>
							@elseif (isset($json['d']) && $json['d']!='')
								<div style=" background: #e5e7ea; padding: 10px; border-radius: 4px; margin-bottom: 10px;" class="col-md-12">
									<div class="col-md-1">
										<span style="border: 1px solid black; border-radius: 50%; padding: 5px 10px; background: white;"><strong>D</strong></span>
									</div>
									<div class="col-md-11">
										<strong>{{ $json['d'] }}</strong>
										<span class="pull-right">  @if(isset($result['d%'])) {{ $result['d%'] }} %  @endif</span>
									</div>
								</div>
							@endif
							@if (isset($json['e'])&&$json['ans']=="e"&& $json['e']!='')
							<div style=" background: #3eef50; padding: 10px; border-radius: 4px; margin-bottom: 10px;" class="col-md-12">
									<div class="col-md-1">
										<span style="border: 1px solid black; border-radius: 50%; padding: 5px 10px; background: white;"><strong>E</strong></span>
									</div>
									<div class="col-md-11">
										<strong>{{ $json['e'] }}</strong>
										<span class="pull-right"><i class="fa fa-check"></i> @if(isset($result['e%'])) {{ $result['e%'] }} %  @endif</span>
									</div>
								</div>
							@elseif (isset($json['e']) && $json['e']!='')
								<div style=" background: #e5e7ea; padding: 10px; border-radius: 4px; margin-bottom: 10px;" class="col-md-12">
									<div class="col-md-1">
										<span style="border: 1px solid black; border-radius: 50%; padding: 5px 10px; background: white;"><strong>E</strong></span>
									</div>
									<div class="col-md-11">
										<strong>{{ $json['e'] }}</strong>
										<span class="pull-right">  @if(isset($result['e%'])) {{ $result['e%'] }} % @endif</span>
									</div>
								</div>
							@endif

							<a href="{{ URL('poll-answears/'.$data->id) }}"> <span style="float:right;font-size:16px;"> @if(isset($result['total'])) Total : {{ $result['total'] }}  @endif </span> </a>
								
								<div class="clearfix"></div>
							</div>
							
							@elseif($data->field_type==8)
							<div class="panel-footer panel-footer-condensed">
						    <div style="float:left; width: 100px;">
						    	<img style="border-radius: 50%;margin-top: 10px; margin-left: 20px;" src="@if(isset($json['userimage'])){{ $json['userimage'] }} @endif" width="60" height="60">

						    </div>
						    <div style="float: left; width: 300px; margin-top: 15px; margin-left: 10px;">
						    	<span style="font-size: 16px;"><strong>@if(isset($json['name'])){{ $json['name'] }} @endif</strong></span><br>
						    	<span>@if(isset($json['collage'])){{ $json['collage'] }}@endif</span>
						    </div>
						    <div style="float: right; margin-right: 20px; margin-top: 15px;">
						    <span>{{ date('M d, Y',$timestamp) }}</span>
						    
						    </div>
						    
							</div>
							@if(isset($json['com_image']) && $json['com_image']!="null")
							<div class="thumb content-group">
									<img style="height: 300px;" src="{{ $json['com_image'] }}" alt="" class="">
									<div class="caption-overflow">
										{{-- <span>
											<a href="{{ url('post/'.$data->id) }}" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
										</span> --}}
									</div>
								</div>
								@endif
							<div " class="panel-body">
								

								<h5 class="text-semibold mb-5">
									<a href="" class="text-default less_section">@if(isset($json['comments'])){{ $json['comments'] }} @endif</a>
								</h5>


								<div class="less_section">{!! $data->post_description !!}</div><br>
								<p><b>For More Info:</b> <a target="_blank" href="{{ $data->posturl }}">{{ $data->posturl }}</a></p>
								<div class="clearfix"></div>
							</div>
							@endif

							
					
					<div class="panel-footer panel-footer-transparent">
								<div class="heading-elements">
									<ul class="list-inline list-inline-separate heading-text">
										<li><a class="comment" id="comment{{ $data->id }}" href="" class="text-muted">{{$data->comment}} comments</a></li>

										<li><a href="#" class="text-default"><i class="icon-heart6 text-pink position-left"></i> {{ $data->likes }}</a></li>

										@if($data->status=='published')
										<li><a href="{{ url('/disapprove_user_post/'.encrypt($data->id)) }}" class="text-default">Unpublish</a></li>
										@else
											<li><a href="{{ url('/approve_user_post/'.encrypt($data->id)) }}" class="text-default">Publish</a></li>
										@endif

										<li><a data-toggle="modal" data-target="#modal_mini{{ $data->id }}" class="text-muted"><i class="icon-trash text-size-base text-pink position-left"></i></a></li> 

									</ul>
								
							<div  data-backdrop="false" id="modal_mini{{ $data->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Comment</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this Comment.</p>

										</div>

										<div class="modal-footer">
										
											<form action="{{ url('delete_group_comment') }}" method="post">
											{{ csrf_field() }}
											<input type="hidden" name="del_id" value="{{ $data->id }}">
											<input type="hidden" name="groupid" value="{{ $data->groupid }}">
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
											
										</div>
									</div>
									
								</div>
							</div>

								</div>
							
						<!-- /blog layout #4 with image -->
						
							@if (isset($data->comments))
				                   <div style="background:#edeaea; padding: 10px; margin-bottom: -11px; display: none;" class="comment-box col-md-12 comment{{ $data->id }}">
				                   @php
				                   	$count=0;
				                   @endphp
				                    @foreach ($data->comments as $comment)
				                    	@php
				                    	$timestamp1=$comment->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($comment->jsondata,true); 
				                    	@endphp	
				                    <div class=" col-md-12" style="padding: 0px; margin-bottom: 20px;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    		@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="35" height="35"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="35" height="35"></div>
				                    	@endif
				                    		<div class="col-md-6"><span style="font-weight: bold;">@if(isset($com['name'])){{ $com['name'] }} @endif</span><br>
				                    		<span style="margin-top: 5px;">@if(isset($com['collage'])){{ $com['collage'] }} @endif</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 10px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
			                    		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			                    			<p>{{ $com['comments'] }}</p>
			                    			@if ($com['com_image']!="null")
			                    			<img src="/mechanicalinsider/{{$com['com_image']}}" width="150" height="100">
			                    			@endif
			                    		</div>
			                    		@if (isset($comment->reply))
			                    		@php
			                    		$reply_count=count($comment->reply);
			                    		@endphp
			                    		<div id="comment_{{ $comment->id }}" class="reply_section_default" style="padding-left:60px;">
				                    		<p class="pull-left"><a id="show_reply">{{ $reply_count }} replies</a>
				                    		<a style="display: none;" id="hide_reply">Hide {{ $reply_count }} replies</a></p>
				                    	
			                    		@foreach ($comment->reply as $reply)
			                    		@php
				                    	$timestamp1=$reply->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($reply->jsondata,true);
				                    		// print_r($reply); 
				                    	@endphp
				                    		
			                    			<div class="col-md-12 reply_section" style="; margin-bottom: 20px; display:none;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    	@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="25" height="25"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="25" height="25"></div>
				                    	@endif
				                    		<div class="col-md-6"><span style="font-weight: bold;">@if(isset($com['name'])){{ $com['name'] }} @endif</span><br>
				                    		<span style="margin-top: 5px;">@if(isset($com['collage'])){{  $com['collage'] }} @endif</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 8px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
			                    		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			                    			<p>{{ $com['comment'] }}</p>
			                    			@if ($com['com_image']!="null")
			                    			<img src="/mechanicalinsider/{{$com['com_image']}}" width="100" height="50">
			                    			@endif
			                    		</div>
			                    		<div class="col-md-10" style="margin-left: -10px;">
											
											<a><span alt="{{ $reply->commentid }}" id='{{ $reply->id }}' class='delete_reply'>Delete</span></a>
											
										</div>
				                    </div>
			                    		@endforeach
			                    		</div>
			                    		@endif
			                    		<div class="col-md-10" style="margin-left: -10px;">
											
											<a><span alt='{{ $comment->postid }}' id='{{ $comment->id }}' class='delete_comment'>Delete</span></a>
											
										</div>
				                    </div>
				                    @php
				                    $count++;
				                    if($count==5)
				                    {
				                    	echo "<a><span alt='$data->field_type' id='$data->id' class='view_more_comment'>view more comments</span></a>";				          
				                    	break;
				                    }
				                    @endphp

				                    @endforeach
				                    </div>
				                    @endif
						</div>
						</div>
						</div>
				        @endforeach
						@endif
						{{ $record->links() }}
				    </div>

				    <script>
				    	$(document).ready(function(){
							var last_page=1;
							
				    		var limit=5;
				    		var postid=$("#postid").val();
				    		var field_type=$("#field_type").val();

				    		 
				    		$(document).on('click','.comment',function(e){
				    			var id=$(this).attr('id');
				    			$("div."+id).slideToggle();
				    			e.preventDefault();
				    		});
				    		$(document).on('click','#show_reply',function(e){
				    			$(this).parents('div').children('div.reply_section').show();
				    			$(this).siblings("a").show();
				    			$(this).hide();
				    			e.preventDefault();
				    		});
				    		$(document).on('click','#hide_reply',function(e){
				    			$(this).parents('div').children('div.reply_section').hide();
				    			$(this).siblings("a").show();
				    			$(this).hide();
				    			e.preventDefault();
				    		});
				    		$(document).on('click','.view_more_comment',function(){
				    			var postid=$(this).attr('id');
				    			// alert(postid);
				    			var id=$(this).attr('id');
				    			// var field_type=$(this).attr('alt');
				    			// alert(field_type);
				    			limit=limit+5;
				    			var path = {!! json_encode(url('/')) !!}
				    			$.get(""+path+"/ajax2",{
				    				postid:postid,
				    				limit:limit },
				    				function(data,success){
				    					$(".comment"+id).html(data);
				    				
				    			});
				    		});
				    		$(document).on('click','.delete_reply',function(){
				    			var commentid=$(this).attr('alt');
				    			var id=$(this).attr('id');
				    			var path = {!! json_encode(url('/')) !!}
				    			$.get(""+path+"/delete_reply",{
				    				id:id,
				    				commentid:commentid },
				    				function(data,success){
				    					$("#"+id).parents('div.reply_section_default').html(data);
				    				
				    			});
				    		});
				    		$(document).on('click','.delete_comment',function(){
				    			var postid=$(this).attr('alt');
				    			var commentid=$(this).attr('id');
				    			var path = {!! json_encode(url('/')) !!}
				    			$.get(""+path+"/delete_comment",{
				    				postid:postid,
				    				commentid:commentid,limit:limit },
				    				function(data,success){
				    					$(".comment"+postid).html(data);
				    				
				    			});
				    		});
				    	});
				    </script>
				
				    <!-- /layout 1 -->
				</div>
        <!-- /main content -->
<!-- Secondary sidebar -->
			@include('includes.right_sidebar')
			<!-- /secondary sidebar -->
			

		</div>
		<!-- /page content -->
			
@endsection
@section('js')
<script type="text/javascript">
	$(document).ready(function() {
	
		$(".less_section").shorten({
			"showChars" : 400,
			"moreText"	: "See More",
			"lessText"	: "Show Less",
		});
	
	});
</script>
<script>
	$(document).ready(function(){
		$(document).on('keyup',"#searchbar",function(){
			var val=$("#searchbar").val();
			var path = {!! json_encode(url('/')) !!}
		      $.ajax({
		          url:""+path+"/group_comments_search",
		          type:"POST",
		          data:{ "_token": "{{ csrf_token() }}","val":val},
		          success:function(data){
		            $("#post_section").html(data);
		          }
		      });

		});
	});
	//,'g_id':g_id
</script>
@endsection