@extends('layouts.main')
@section('js_head')



@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
@php
//dd($query);
    $json=json_decode($query->jsondata,true);
  

				  
				  
@endphp
				<!-- Post -->
				<div class="panel">
					<div class="panel-body">
				
						<div class="content-group-lg">
							<div class="content-group text-center">
								<a  href="#" class="display-inline-block">
									<img style="width: auto; height: 200px;" src="/mechanicalinsider/jobsimage/{{ $json['image'] }}" class="" alt="">
								</a>
							</div>

							<h3 class="text-semibold mb-5">
								<a href="#" class="text-default">{{ $json['title'] }}</a>
							</h3>

							<ul class="list-inline list-inline-separate text-muted content-group">
								<li></li>
								<li><a href="{{ url('#comments') }}" class="text-muted">{{ $query->comment }} comments</a></li>
								<li><a href="#" class="text-muted"><i class="icon-heart6 text-size-base text-pink position-left"></i>{{ $query->likes }}</a></li>
								<li><a href="#" class="text-default"><i class="icon-eye2 text-pink position-left"></i> {{ $query->view }}</a></li>
							</ul>
							{{-- <p><i class="fa fa-rupee" style="font-size: 16px; padding:3px; margin-left:5px;"></i>&nbsp;&nbsp;&nbsp;&nbsp;Fees:&nbsp;{{ $json['fees'] }}</p> --}}
							<p><i class="fa fa-users" style="font-size: 16px; padding:3px;"></i>&nbsp;&nbsp;Posts:&nbsp;{{ $json['post'] }}</p>
							<p><i class="fa fa-black-tie" style="font-size: 16px; padding:3px;"></i>&nbsp;&nbsp;&nbsp;Postname : &nbsp;{{ $json['postname'] }}</p>
							@if ($json['website']!="")
							<p><i class="fa fa-globe" style="font-size: 16px; padding:3px;"></i>&nbsp;&nbsp;&nbsp;Website : &nbsp;{{ $json['website'] }}</p>
							@endif
							<p>@if ($query->post_description!="")
								{!!$query->post_description!!}
							@endif</p>
							<div class="content-group">
							</div>

							
					</div>
				</div>
				</div>
	


				<!-- Comments -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title text-semiold">Comments</h6>
						<div class="heading-elements">
							<ul class="list-inline list-inline-separate heading-text text-muted">
								<li>{{ $query->comment }} comments</li>
							</ul>
	                	</div>
					</div>
					@if (isset($query->comments))
					
					<div id="comments" class="panel-body">
						<ul class="media-list stack-media-on-mobile">
						@foreach ($query->comments as $c)
						@php
							$json=json_decode($c->jsondata,true);
							$timestamp=strtotime($c->timestamp);
							$time=date('M d, Y',$timestamp);
						@endphp
							<li class="media">
								<div class="media-left">
									<a href="#"><img style="width: 50px; height: 50px;" src="{{ $json['userimage'] }}" class="img-circle" alt=""></a>
								</div>

								<div class="media-body">
									<div class="media-heading">
									<div class="row">
										<div class="col-md-3">
											<a href="#" class="text-semibold">{{ $json['name'] }}</a>
											<span class="media-annotation dotted">{{ $time }}</span>
											
										</div>
										<div class="col-md-2">
										@permission('delete_post_comment')
											<form action="{{ url('delete_post_comment') }}" method="post" style="height:10px;">
												{{ csrf_field() }}
												<input type="hidden" name="comment_id" value="{{ $c->id }}">
												<input type="hidden" name="postid" value="{{ $query->id }}">

												<button type="submit" title="delete" class="btn btn-blue btn-icon btn-rounded legitRipple" name="submit" style="height:30px; padding:3px 11px 5px 10px;"><i class="fa fa-trash" style="font-size: 15px;"></i></button>
											</form>
											@endpermission
										</div>
											
										
										
									</div>
										
										
										
									
									
								
									
									<br>
										<span style="font-size: 11px;">{{ $json['collage'] }}</span>
										
										
									</div>

									<p style="margin-top: 10px;">{{$json['comments']}}</p>
									@if ($json['com_image']!="null")
									<p><img src="/mechanicalinsider/{{$json['com_image']}}" width="300" height="200"></p>
									@endif

									
								</div>
							</li>
						@endforeach

							
						</ul>
					</div>
					@endif
				</div>
				<!-- /comments -->

			</div>
			<!-- /main content -->

		</div>
	
@endsection
