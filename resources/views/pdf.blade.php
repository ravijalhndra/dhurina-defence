@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>
@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('pdf') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add PDF</h5>
									
								</div>

								<div class="panel-body">
									<div class="col-md-10  col-md-offset-1">

										<input type="hidden" id="groups_institute" name="type_institute" value="college" />
									
										<div class="form-group" id="groups_select">
											<label class="col-lg-2 control-label">Select @if(Auth::user()->role == 'admin' ) Courses @else Class @endif:</label>
											<div class="col-lg-10 multi-select-full">
												<select name="groupid[]" data-placeholder="Select one..." multiple="multiple" class="multiselect-select-all-filtering course_id" required>
													@foreach ($new_Courses as $c)
														<option value="{{ $c->id }}">{{ $c->name }}</option>
													@endforeach
												</select>
											</div>
										</div>


										<div class="form-group" id="groups_select">
											<label class="col-lg-2 control-label">Select Subject:</label>
											<div class="col-lg-10 multi-select-full">
												<select name="sub[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning sub_record " required>
													<option value="">Select one..</option>
												</select>
											</div>
										</div>

										<div class="form-group" id="topic_select">
											<label class="col-lg-2 control-label">Select Topic:</label>
											<div class="col-lg-10 multi-select-full">
												<select name="topic_id[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning topic_record ">
													<option value="">Select one..</option>
												</select>
											</div>
										</div>


										<div class="form-group">
											<label class="col-lg-2 control-label">Writer:</label>
											<div class="col-lg-10">
												<input type="text" name="writer" class="form-control" placeholder="Enter name" required > 
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Title:</label>
											<div class="col-lg-10">
												<input type="text" name="title" class="form-control" placeholder="Enter Title">
											</div>
										</div>
										
										<div class="form-group" id="mySelect">
											<label class="col-lg-2 display-block text-semibold">Select Type</label>
											<div class="col-lg-10">
											<label class="radio-inline">
												<input type="radio" name="type" value="link" class="styled" checked="checked">
												Link
											</label>
											<label class="radio-inline">
												<input type="radio" name="type" value="pdf" class="styled">
												Pdf
											</label>
											</div>
										</div>
										
										<div class="form-group" id="link">
											<label class="col-lg-2 control-label">Enter Link :</label>
											<div class="col-lg-10">
												<input id="link" type="text" class="form-control" placeholder="Enter Link" name="link">
											</div>
										</div>


										
										<div class="form-group" style="display:none;" id="pdf">
											<label class="col-lg-2 control-label">Enter Pdf:</label>
											<div class="col-lg-10">
												<input id="pdf" type="file" class="form-control" name="pdf">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Access:</label>
											<div class="col-lg-10">
												<label class="radio-inline">
													<input type="radio" class="styled" name="access" value="public" />
													Public
												</label>
													
												<label class="radio-inline">
													<input type="radio" class="styled" name="access" value="private"  checked="checked" />
													Private
												</label>
											</div>
										</div>

										<fieldset>
											<legend></legend>
											{{-- <div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to like this post </label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_like" type="checkbox" class="switchery" value="off">
													</label>
												</div>
											</div> --}}
											<div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to comment this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_comment" type="checkbox" class="switchery" value="off">
													</label>
												</div>
											</div>
										</fieldset>
										
										
								<!-- /select All and filtering options -->
										<div class="text-right">
											<button type="submit" class="btn btn-primary">Insert <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->

		</div>

	<script>
		$(document).ready(function(){

			$('#topic_select').hide();

			$("#login_form").validate({

				rules:{
					title:{
						required:true
					},
					type:{
						required:true
					},
					writer:{
						required:true
					},
					link:{
						required:"#pdf:blank"
					},
					pdf:{
						required:"#link:blank"
					},
					groupid:{
						required:true
					}
				}
			});

		});

		$('select[name="groupid[]"]').change(function(){
			var course= $(this).val();

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_subject',   
				data: {course:course},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{						
						$('#subcategory').show();
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				}  
			});			
		});

		$('select[name="sub[]"]').change(function(){

			var course_ids=[];
			var defence=['Combo Course (Airforce-Navy)','Air Force X Group','Air Force Y Group','Indian Navy ( SSR & AA )','Army GD','Army Clerk','Army Technical'];


			$('select[name="groupid[]"] option:selected').each(function() 
			{
				course_ids.push($(this).val());
			});

			var subject= $(this).val();

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_topics',   
				data: {course_ids:course_ids,subject_ids:subject},                                      
				success: function(data)
				{
					$(".topic_record").html('');
					if(data.status == 'success')
					{		
						$('#topic_select').show();				
						$('#subcategory').show();

						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".topic_record").append(markup);
					}
					else
					{
						$('#topic_select').hide();
					}
				}  
			});			
		});
		

	</script>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('input:radio[name="type"]').change(function(){
				if(jQuery(this).val() == 'link'){
					jQuery("#link").show();
					jQuery("#pdf").hide();
				}
				else
				{
					jQuery("#link").hide();
					jQuery("#pdf").show();
				}
			});
		});
	</script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
@endsection
