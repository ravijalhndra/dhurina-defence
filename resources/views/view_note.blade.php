@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				@if(Auth::user()->role == 'admin' )
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_note') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
				@endif

					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
                        <h3 class="panel-title">View Note & E-book</h3>	
                        <div class="row" style="padding-top:50px;">
							<div class="col-md-12">
								<form action="" method="GET">
									<div class="col-md-4">				 						
                                        <select name="sector" data-placeholder="Select type..." class="select select-border-color border-warning ">
                                            <option selected default value="">Select Type..</option>
                                            <option @if($sector == 'notes') selected @endif value="notes">Notes</option>
                                            <option @if($sector == 'e_book') selected @endif value="e_book">E-book</option>
                                       </select>
									</div>

									<div class="col-md-4">
										<div class="col-md-6"> 
											<label>	<input type="search" class="form-control" name="filter" value="{{ $filter }}" placeholder="Type to filter..."></label>
										</div>
										<div class="col-md-6">
											<input type="submit" class="fab-menu-btn btn bg-success" value="search"> 											
										</div>	
									</div>

								</form>						
							</div>
						</div>					
					</div>
				

					<table class="table">
						<thead>
							<tr>			
                                <th>Sector</th>                                
                                <th>Name</th>
                                <th>File</th>
								<th>Publish Status</th>
                                <th>Position</th>
                                <th>Type</th>
								<th>Price</th>
								<th>Discount</th>
                                <th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if(count($data)>0)
						@foreach($data as $u)
							<tr>								
                                <td>{{$u->sector_name}}</td>
                                <td>{{$u->name}}</td>    
                                <td>
									<a href="{{'img/notes/'.$u->file}}" target="_blank"> <img src="{{'img/notes/'.$u->image}}" width="80" height="80"  </a>
								</td>   
                                <td>
                                    <span class="fab-menu-btn btn-sm @if($u->publish == 'false') bg-red @endif  bg-success btn-rounded "> @if($u->publish == 'true') Published @else Unpublish  @endif </span>  
								</td>       
								                   
								<td>{{ $u->position }}</td>
                                <td>@if($u->type == 'notes') Notes @else  E-Book @endif </td>
								
								<td>@if($u->price!=='0') {{$u->price}} @else Free @endif</td>
								<td> {{$u->discount}} </td>
								<td class="text-center">										
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
											<ul class="dropdown-menu dropdown-menu-right" >
												
												@if(Auth::user()->role == 'admin' )
													<li><a href="javascript:;"  onClick="fun_delete({{ $u->id }})"><i class="icon-trash"></i> Delete</a></li>
													<li><a href="{{route('edit_note',encrypt ($u->id))}}"><i class="icon-pencil"></i> Edit</a></li>
                                                @endif

												@if($u->publish == 'true')
													<li><a href="javascript:;" onClick="fun_publish({{ $u->id }},'false')" ><i class="icon-pencil"></i> Unpublish</a></li>
												@else
													<li><a href="javascript:;" onClick="fun_publish({{ $u->id }},'true')" ><i class="icon-pencil"></i> Publish</a></li>
												@endif

											</ul>
										</li>
									</ul>									
								</td> 	
								

                                
                          	</tr>				
						@endforeach	
						@else
						<tr>
								<td colspan="7"><center>No record</center></td>
						</tr>
						@endif								
						</tbody>
					</table>
				</div>

				@php
					$from=($data->currentPage()-1)*($data->perPage())+1;
					$to=$data->currentPage()*$data->perPage();
					$total=$data->total();
				@endphp 

				<div class="col-md-12">
					<div class="col-md-4">
						<span class="float-left">Showing {{ $from }} to {{ $to }} from {{ $total }}</span> 
					</div>
				</div> </br>
				
				{{ $data->links() }}
				

			</div>
		</div>

		<div  data-backdrop="false" id="modal_mini" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Click on Yes if you want to delete this Record.</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('delete_note') }}" method="post">
                            {{ csrf_field() }}
                            <button type="button" class="btn btn-link" data-dismiss="modal">No</button>
                                <input type="hidden" id="del_id" name="del_id" value="">
                            <button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div  data-backdrop="false" id="modal_2" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Are you want want to change this record?</p>
					</div>
					<div class="modal-footer">

                        <form action="{{ route('published_note') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del" name="del" value="">
							<input type="hidden" id="type" name="type" value="">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

@endsection
@section('js')

	<script type="text/javascript">

		function fun_delete(id)
		{	
			var val = [];
			val.push(id);
			$('#del_id').val(val);
			$('#modal_mini').modal('toggle');		
		}

		function multi_delete()
		{		
			$('#del_id').val( $('#multi_delete').val() );
			$('#modal_mini').modal('toggle');
		}

		function fun_publish(id,type)
		{
			$('#del').val(id);
			$('#type').val(type);
			$('#modal_2').modal('toggle');	
		}

		function multi_publish(type)
		{
			$('#del').val( $('#multi_publish').val() );
			$('#type').val(type);
			$('#modal_2').modal('toggle');	
		}

		

	</script>
@endsection

