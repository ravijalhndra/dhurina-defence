@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('js/test.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					@permission('add_admin')
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_college') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
					@endpermission
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Student</h3>
					</div>
					<div class="row">
						<div class="col-md-offset-2 col-md-8 col-md-offset-2">
							<div class="col-md-6 row">
							<label class="col-md-3">Course</label>
								<!-- Course : -->
								<div class="col-md-9">
									<select class="pull-right course form-control">
								
										<option value="">Select course</option>
										@foreach($course as $key)
											<option value="{{$key->id}}">{{$key->course_name}}</option>
										@endforeach 
									</select>
								</div>
							</div>
							<div class="col-md-6 pull-right">
								<label class="col-md-3">College</label>
								<div class="col-md-9">
									<select class="pull-right college form-control"  onchange="if (this.value) window.location.href=this.value">
										<option value="">Select college </option>
									     
										@foreach($colleges as $c)
											<option value="{{$c->id}}">{{$c->college_name}}</option>
										@endforeach 
									</select>
								</div>
								<input type="hidden" name="urlcourse" class="urlcourse" value="{{route('filter_course')}}"/>
								<input type="hidden" name="urlcollege" class="urlcollege" value="{{route('filter_student')}}"/>
							</div>
						</div>
					</div>


					<table class="table datatable-basic">
						<thead>
							<tr>
                                <th>ID</th>
								<th>Name</th>
								<th>MobileNo</th>
                                <th>Course Name</th>
                                <th>College Name</th>
								<th>Stream</th>
                                <th>Year</th>
                                <th>Status</th>
								<th class="text-center">Actions</th>
								
							</tr>
						</thead>
						<tbody id="ajaxresponse">
					
                           
                         
                              
								@foreach($data as $d)
						
                                <tr>
                                <td>{{$d->id}}</td>    
								<td>{{$d->name}}</td>
                                <td>{{$d->mobile}}</td>
                                <td>{{$d->course_name}}</td>
                                <td>{{$d->college_name}}</td>
                                <td>{{$d->stream}}</td>
                                <td>{{$d->year}}</td>
                                @if($d->status=='true')
                                <td><a href="{{url('change_status/'.$d->uid.'/'.$d->status.'/'.$d->type)}}" type="button" style="border-radius: 20px;" class="btn btn-success">Activated</a></td>
                                @else
                                <td><a  href="{{url('change_status/'.$d->uid.'/'.$d->status.'/'.$d->type)}}" style="border-radius: 20px;" class="btn btn-danger">Deactivated</a></td>
                                @endif
                                
                                <td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												
												<li><a data-toggle="modal" data-target="#modal_mini{{$d->id}}" href="#"><i class="icon-trash"></i> Delete</a></li>
												
												<li><a href="{{url('edit_student_college/'.$d->id)}}"><i class="icon-pencil"></i> Edit</a></li>
												
												
											</ul>
										</li>
									</ul>
								</td>


								<div  data-backdrop="false" id="modal_mini{{ $d->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Student Data</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this data.</p>

										</div>

										<div class="modal-footer">
											<form action="{{ url('delete_student_college') }}" method="post">
											{{ csrf_field() }}
											<input type="hidden" name="id" value="{{ $d->id }}">
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										</div>
									</div>
									
								</div>
							</div>
                                
							</tr>
							@endforeach
												
							
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->
{{ $data->links() }}

				

			</div>
			<!-- /main content -->

		</div>
@endsection
