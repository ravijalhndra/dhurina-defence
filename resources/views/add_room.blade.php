@extends('layouts.main')
@section('js_head')


	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
@include('mapjs')

	
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				topic:{
					required:true
				},
				city:{
					required:true
				},
				status:{
					required:true
				},
				logo:{
					required:true
				},
				payment:{
					required:true
				},
				payment1:{
					number:true
				}
				
			}
		});
	});
</script>
<script>
	
	$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



</script>


		
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('insert_room') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Create Meeting Room</h5>
									
								</div>

							<div class="panel-body">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Meeting Room Name </label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Meeting Room Name" name="name" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">School Name</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Enter School Name" name="school" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Teacher Name </label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Teacher Name" name="teacher_name" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Class name </label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Class Name" name="class_name" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Subject name </label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Subject Name" name="subject" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Password </label>
                                    <div class="col-lg-9">
                                        <input type="password" class="form-control" placeholder="Password" name="password" required>
                                    </div>
                                </div>



							    {{--  <div class="form-group">
									<label class="col-lg-3 control-label">Select Course:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="course" class="form-control select2" data-style="btn-white" required>
											@if($course[0]!="")
												@foreach ($course as $c)
												@if($c)
													<option value="{{$c->id}}">{{ $c->course_name }}</option>
												@endif
												@endforeach
												@endif
											
										</select>
									</div>
                                </div>  --}}
                            </div>
                                    <br /><br />
                                    <div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Add meeting room <i class="icon-arrow-right14 position-right"></i></button>
                                    </div><br>

								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')
