@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>
	
	

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>
	

	{{-- <script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_ckeditor.js') }}"></script>
 --}}



@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('add_test') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Test</h5>
									
								</div>

								<div class="panel-body">
								<div class="form-group">
								
										<label class="col-lg-2 control-label">Select:</label>
										<div class="col-lg-10">
											<label class="radio-inline">
												<input type="radio" class="styled" id="groups_institute" name="type_institute" value="group" checked="checked">
												Groups
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" id="college_institute" name="type_institute" value="college">
												College
											</label>
											<label class="radio-inline">
												<input type="radio" class="styled" id="both_institute" name="type_institute" value="both">
												Both
											</label>

										</div>
									</div>

									<div class="form-group" id="all_topic">
											<label class="col-lg-2 control-label">Topic:</label>
										     <div class="col-lg-5">
												<select id="topic" name="posttype_all" data-placeholder="Select one..." class="select select-border-color border-warning" required>
													<option value="">Select one...</option>
													@foreach ($all_topic as $at)
													<option value="{{ $at->topic_name }}">{{ $at->topic_name }}</option>
													@endforeach
												</select> 
											</div>
										</div>

									
									    <div class="form-group" id="grp">
											<label class="col-lg-2 control-label">Topic:</label>
										     <div class="col-lg-5">
												<select  name="posttype" id="topic_group" data-placeholder="Select one..." class="select select-border-color border-warning" required>
													<option value="">Select one...</option>
													@foreach ($query as $q)
													<option value="{{ $q->topic_name }}">{{ $q->topic_name }}</option>
													@endforeach
													<option  value="create_topic">create new topic</option>
												</select> 
											</div>
											<div id="new_group_topic" style="display:none;">
											    <label class="col-lg-1 control-label">Create New:</label>
												<div class="col-lg-3">
													<input type="text" id="new_topic_group" name="new_topic" class="form-control" placeholder="add new topic name">
												</div>
											</div>
										</div>
 
										<div class="form-group" id="clg">
											<label class="col-lg-2 control-label">Topic:</label>
										     <div class="col-lg-5">
												<select  name="posttype_college" id="topic_college" data-placeholder="Select one..." class="select select-border-color border-warning"  required>
													<option value="">Select one...</option>
													@foreach ($college_topic as $c)
													<option value="{{ $c->topic_name }}">{{ $c->topic_name }}</option>
													@endforeach
													<option  value="create_topic">create new topic</option>
												</select> 
											</div>
											<div  id="college_new_topic" style="display:none;">
											<label class="col-lg-1 control-label">Create New:</label>
											<div class="col-lg-3">
												<input type="text" id="new_topic_college" name="new_topic_college" class="form-control" placeholder="add new topic name">
											</div>
											</div>
										</div> 
										
										
										

									
									<script>
											$(document).ready(function(){
												$("#college_select").hide();
												$("#stream_select").hide();
												$("#course_select").hide();
												$("#year_select").hide();
												$("#all_topic").hide();
												$("#clg").hide();
												$("#grp").show();
												$("#groups_institute").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();	
														$("#groups_select").show();
														$("#all_topic").hide();
														$("#clg").hide();
														$("#grp").show();
													
														
														
												});
												$("#college_institute").click(function(){
														$("#college_select").show();
														$("#stream_select").show();
														$("#course_select").show();
														$("#year_select").show();
														$("#groups_select").hide();
														$("#all_topic").hide();
														$("#grp").hide();
														$("#clg").show();
														
														
														
												});
												$("#both_institute").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();
														$("#groups_select").hide();
														$("#grp").hide();
														$("#clg").hide();
														$("#all_topic").show();
												});
												
											});
									</script>
									
									<div class="form-group" id="course_select">
									<label class="col-lg-2 control-label">Select Course:</label>
									<div class="col-lg-10 multi-select-full">
										<select name="course" data-placeholder="Select Course..." id="coursedropdown" class="course-selected form-control" required>
										<!-- multiple="multiple"   -->
											@if($course)
												@foreach ($course as $cou_rse)
												@if($cou_rse)
													<option value="{{ $cou_rse->id }}">{{ $cou_rse->course_name }}</option>
												@endif
												@endforeach
												@endif
										</select>
										<input type="hidden" value="{{route('get_course')}}" name="urlcourse" class="urlcourse"/>
									</div>
								</div>
							<script>
								$("#topic_group").change(function(){
									var topic=$(this).val();
									if(topic=='create_topic')
									{	
										$("#new_group_topic").show();
									}
									else
									{
										$("#new_group_topic").hide();
									}
								});
							</script>
							<script>
								$("#topic_college").change(function(){
									var topic=$(this).val();
									if(topic=='create_topic')
									{	
									$("#college_new_topic").show();
										
									}
									else
									{
										$("#college_new_topic").hide();
									}
								});
							</script>
								
							
								{{-- <script>
							
	function coursedropdownfunction() {
		console.log("success");
		var val = document.getElementById("coursedropdown").value;
		var Apiurl = "{{ url('/api/get_collages') }}";
		$.ajax({
                        url: Apiurl,
                        type: 'POST',
                        enctype: 'multipart/form-data',
        
                        data: {"id":val},
                        processData: false,
                        contentType: false,
                        cache: false,
                    
                         success:function(response) {
                             console.log(response);
                                        if(response.message=='success'){
                                            
                                        }
                                        else
                                        {
                                        }
                                  
                            
                           
                        }
                   });
        
	}
	</script> --}}


									<div class="form-group" id="college_select">
									<label class="col-lg-2 control-label">Select College:</label>
									<div class="col-lg-10 multi-select-full">
										<select name="college" data-placeholder="Select College..." id="collagedropdown" class="college-selected form-control" required>
										<option value="">Select College </option>
										</select>
										{{--<!-- <input type="hidden" value="{{ route('get_stream') }}" name="urlcollege" class="urlcollege"/> -->--}}
									</div>
								</div>

								<div class="form-group" id="stream_select">
									<label class="col-lg-2 control-label">Select Stream:</label>
									<div class="col-lg-10 multi-select-full">
										<select name="stream" data-placeholder="Select Stream..." class="stream-selected form-control" required>
											<option value="">Select Stream</option>
										</select>
									</div>
								</div>

								<div class="form-group" id="year_select">
									<label class="col-lg-2 control-label">Select Year:</label>
									<div class="col-lg-10 multi-select-full">
										<select name="year" data-placeholder="Select Year..."  class="year-selected form-control" required>
													<option value=""></option>
										</select>
									</div>
								</div>

									<div class="form-group" id="groups_select">
									<label class="col-lg-2 control-label">Select Groups:</label>
									<div class="col-lg-10 multi-select-full">
										<select name="groupid[]" data-placeholder="Select groups..." multiple="multiple" class="multiselect-select-all-filtering" required>
											@if($data[0]!="")
												@foreach ($data as $d)
												@if($d)
													<option value="{{ $d->id }}">{{ $d->topic }}</option>
												@endif
												@endforeach
												@endif
											
										</select>
									</div>
								</div>
										

										<div class="form-group">
											<label class="col-lg-2 control-label">Test Name</label>
											<div class="col-lg-10">
												<input type="text" name="test_name" class="form-control" placeholder="Enter Test name" required>
											</div>
										</div>
										<script type="text/javascript">
										$(document).ready(function() {
										  $('.summernote').summernote();
										});
									</script>
										<div class="form-group">
											<label class="col-lg-2 control-label">Test Description:</label>
											<div class="col-lg-10">
												<textarea  class="summernote" name="description" required></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Time:</label>
											<div class="col-lg-10">
												<input type="number" name="time" min="0" class="form-control" placeholder="Enter test time in minutes">
											</div>
										</div>

										{{-- <div class="form-group">
											<label class="col-lg-2 control-label">Categories and Questions:</label>
											<div class="col-lg-10">
												<input type="text" name="sub_tests" class="form-control" placeholder="Add categories and questions like reasoning=10, math=20">
											</div>
										</div> --}}
										<div class="form-group">
											<label class="col-lg-2 control-label">Categories and Questions:</label>
											<div class="input_fields_wrap col-md-10">
												<div class="col-md-6 field_section">
												   <select name="cat[]" class="form-control">
															<option value=''>Select Category</option>
															@foreach($subject_question as $sq)
															<option value="{{$sq->sub_test_name}}">{{$sq->sub_test_name}}</option>
															@endforeach
														</select>
													</div>
												    <div class="col-md-3">
												    <input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions">
												    </div>
												    
												</div>

												<div class="col-md-12 field_section">
													<div class="col-md-2">
													</div>
													<div class="col-md-4">
														<select name="cat[]" class="form-control">
															<option value=''>Select Category</option>
															@foreach($subject_question as $sq)
															<option value="{{$sq->sub_test_name}}">{{$sq->sub_test_name}}</option>
															@endforeach
														</select>
													</div>
													<div class="col-md-4">
														<input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions">
													</div>
													<div class="col-md-2">
														<button class="remove_field btn btn-danger btn-sm">Remove</button>
													</div>
												</div>
											</div>
											<div class="col-md-10 col-md-offset-2"><br>
												  <button class="add_field_button btn btn-info btn-sm">Add More Fields</button>
											</div>
										</div>
										<script>
											$(document).ready(function() {
											    var max_fields      = 4; //maximum input boxes allowed
											    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
											    var add_button      = $(".add_field_button"); //Add button ID
											    
											    var x = 1; //initlal text box count
											    $(add_button).click(function(e){ //on add input button click
											        e.preventDefault();
											        if(x <= max_fields){ //max input box allowed
											            x++; //text box increment
											            $(wrapper).append('<div class="col-md-2>div class="col-md-4"><select name="cat[]" class="form-control"><option value=''>Select Category</option>@foreach($subject_question as $sq)<option value="{{$sq->sub_test_name}}">{{$sq->sub_test_name}}</option>@endforeach</select></div><div class="col-md-4"><input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions"></div><div class="col-md-2"><button class="remove_field btn btn-danger btn-sm">Remove</button></div>'); //add input box

											            if(x>max_fields)
												        {
												        	$('.add_field_button').hide();
												        }

											            // $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
											        }
											        
											    });
											    
											    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
											        e.preventDefault(); $(this).parents('div.field_section').remove(); x--;
											        $('.add_field_button').show();
											    })
											});
										</script>
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Posted By:</label>
											<div class="col-lg-10">
												<input type="text" name="postby" class="form-control" placeholder="Enter writer name">
											</div>
										</div>
										
										

										<div class="form-group">
											<label class="col-lg-2 control-label">Price:</label>
											<div class="col-lg-9">
												<label class="radio-inline">
													<input type="radio" class="styled" name="payment" value="free" checked="checked">
													Free
												</label>
													
												<label class="radio-inline">
													<input type="radio" class="styled" name="payment" value="paid">
													Paid
												</label>
											</div>
										</div>
									
										<div class="form-group" style="display:none" id="payment1">
										<label class="col-lg-3 control-label">Amount:</label>
										<div class="col-lg-9">
											<input type="number" class="form-control" name="amount" >
										</div>
									</div>
									<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="payment"]').change(function(){
												    if(jQuery(this).val() == 'paid'){
												    	jQuery("#payment1").show();
												    }
												    else
												    {
												    	jQuery("#payment1").hide();
												    }
												});
											});
										</script>


										<div class="form-group">
											<label class="col-lg-2 control-label">Secret Key (Optional):</label>
											<div class="col-lg-10">
												<input type="password" name="secret_key" class="form-control" placeholder="Enter a secret key for this test">
											</div>
										</div>
										

										<fieldset>
										<legend>Notification Image (optional)</legend>
										<div class="form-group" id="mySelect">
											<label class="col-lg-2 display-block text-semibold">Add Test Image</label>
											<div class="col-lg-10">
											<label class="radio-inline">
												<input type="radio" name="type" value="link" class="styled" checked="checked">
												Link
											</label>
											<label class="radio-inline">
												<input type="radio" name="type" value="image" class="styled">
												Image
											</label>
											</div>
										</div>
										
										<div class="form-group" id="link">
											<label class="col-lg-2 control-label">Enter Link :</label>
											<div class="col-lg-10">
												<input id="link" type="text" class="form-control" placeholder="Enter Link" name="link">
											</div>
										</div>
										
										<div class="form-group" style="display:none;" id="pdf">
											<label class="col-lg-2 control-label">Enter Image:</label>
											<div class="col-lg-10">
												<input id="pdf" type="file" class="form-control" name="image">
											</div>
										</div>
										<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="type"]').change(function(){
												    if(jQuery(this).val() == 'link'){
												    	jQuery("#link").show();
												    	jQuery("#pdf").hide();
												    }
												    else
												    {
												    	jQuery("#link").hide();
												    	jQuery("#pdf").show();
												    }
												});
											});
										</script>
										</fieldset>
										<fieldset>
											<legend></legend>
											
											<div class="form-group">
												<div class="col-lg-4">
													<label>Show Answere to users?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="ans_status_new" checked type="checkbox" class="switchery" value="yes">
													</label>
												</div>
											</div>

												<div class="form-group">
												<div class="col-lg-4">
													<label>Office Test</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="office_test" checked type="checkbox" class="switchery" value="yes">
													</label>
												</div>
											</div>

											<div class="form-group">
												<div class="col-lg-4">
													<label>Negative Marking?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="negative_marks" checked  type="checkbox" class="switchery" value="yes">
													</label>
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to comment this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_comment" type="checkbox" class="switchery" value="off">
													</label>
												</div>
											</div>
										</fieldset>

										<div class="text-right">
											<button id="submit" type="submit" class="btn btn-primary">Add<i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->


		</div>

@endsection
@section('js')
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				
				test_name:{
					required:true
				},
				description:{
					required:true
				},
				groupid:{
					required:true
				},
				time:{
					required:true,
					number:true
				},
				postby:{
					required:true
				},
				sub_tests:{
					required:true
				}
			}
		});
	});
</script>

<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>
	

@endsection