@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables1.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic1.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('js/custom11.js') }}></script>

<style>
.datatable-header { display:none; }
</style>
@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					{{-- @php dd($users);@endphp --}}
					@php
						$from=($users->currentPage()-1)*($users->perPage())+1;
						$to=$users->currentPage()*$users->perPage();
						$total=$users->total();
					@endphp 
				<!-- Basic datatable -->

				<div class="panel panel-flat">
					<div class="panel-heading">	
                        <h3 class="panel-title">Video Duration</h3>
			
						<div class="row" style="padding-top:50px;">
							<div class="col-md-12">                            
                                <label> <b>Name:</b>  {{ $user_detail->name }} &nbsp;  <b>Mobile: </b>  {{ $user_detail->mobile }}</label>
							</div>
						</div>
					</div>



					<table class="table datatable-basic">
						<thead>
							<tr>

								<th>Date</th>
                                <th>Duration</th>
                                
							</tr>
						</thead>
						<tbody id="ajaxresponse">
							@foreach ( $users as $user)						
								<tr>

									<td>{{ $user->date }}</td>
                                    <td>{{ $user->duration }} min</td>
                                    
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				
				<div class="col-md-12">
					<div class="col-md-4">
						<span class="float-left">Showing {{ $from }} to {{ $to }} from {{ $total }} users</span> 
					</div>
                        
                    {{ $users->links() }}
				</div>
				
				


				

			</div>
			<!-- /main content -->

		</div>
		<script>
		  $(function() {
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'https://datatables.yajrabox.com/eloquent/basic-data'
        });
    });
		</script>
@endsection
