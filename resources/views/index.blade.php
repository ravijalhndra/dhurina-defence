<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Metas -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edudream -  A new way of learning</title>
    <meta name="description" content="A new way of learning">
    
    <!-- External CSS -->
    <link rel="stylesheet" href="{{ URL::asset('Craze_Main_Template/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('Craze_Main_Template/assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('Craze_Main_Template/assets/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('Craze_Main_Template/assets/css/et-line-icons.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('Craze_Main_Template/assets/css/animate.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('Craze_Main_Template/assets/css/magnific-popup.css')}}">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('Craze_Main_Template/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('Craze_Main_Template/assets/css/responsive.css')}}">
    
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800%7cRaleway:400,500,600,700%7cRoboto:300,500,700" rel="stylesheet">

   
	<style>
        .navbar-default.affix,.banner-area,.subcribe-area,.btn-solid,.lower-footer,.panel-title a
        {
            background-color:#4A85C5 !important;            
        }
        h1, h2, h3, h4, h5, h6,i.star.on:before,.fa,.icon-ribbon,.icon-tools,.icon-gift,.icon-toolbox,.icon-tools-2,.icon-layers,.icon-hourglass,.icon-trophy,.icon-telescope,.icon-megaphone,.panel-title a[aria-expanded=true]
        ,.icon-pictures,.icon-hotairballoon,.icon-global,.optional-feature i btn-white
        {
            color: #000000 !important;      
        }   

        .btn, .btn-white:hove{
            color:#000000;
            border:1px solid #000000;
        }  

        .black{
            color: #fff !important; 
        }  
        .vertical-slider.white-dots .owl-page
        {
            background-color: #000000 !important;
        } 
            
            
    </style>

</head>
<body class="default">
   
    <!--------------- Navigation header --------------->
    <nav id="top" class="navbar navbar-default top" data-spy="affix" data-offset-top="120">
        <div class="container">
           
            <!-- Logo and navigation toggle -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ URL('/landing') }}"><img height="60" src="{{ URL::asset("img/EduDream Final-01.png")}}" alt="..."></a>
            </div>

            <!-- Navigation -->
            {{-- <div class="collapse navbar-collapse" id="main-menu">
                <ul class="nav navbar-nav navbar-right">
                   
                    <li><a href="{{ URL('/contact') }}">Contact</a></li>
                </ul>
            </div> --}}
            
        </div>
    </nav>
    
    <!--------------- Banner Area --------------->
    <section id="banner-area" class="banner-area">
        <div class="container">
            <div class="row">
                <div class="display-flex">
                    <div class="col-md-8">
                        <div>
                            <div>
                                <h1><span style="transition-delay: 1s; color:#fff;">A new way of learning from best Educators of India</span><span style="transition-delay: 1.5s; color:#fff;"></span></h1>                                
                                <div class="button-group" style="transition-delay: 2.5s">
                                    {{-- <a href="#" class="btn btn-default btn-white"><i class="fa fa-apple"></i>App Store</a> --}}
                                    <a href="{{ URL::asset("apk/EduDream-app.apk")}}" download rel="noopener noreferrer" target="_blank"><img src="{{ URL::asset("img/google.png") }}"  ></a>
                                </div>
                            </div>     
                            <!-- <div>
                                <h1><span style="transition-delay: 1s">Stylish, elegant colors</span><span style="transition-delay: 1.5s">And easy to customize</span></h1>
                                <p style="transition-delay: 2s">Change how you want it to.</p>
                                <div class="button-group" style="transition-delay: 2.5s">
                                    {{-- <a href="#" class="btn btn-default btn-white"><i class="fa fa-apple"></i>App Store</a> --}}
                                    <a href="{{ URL::asset("apk/app.apk")}}" target="_blank" class="btn btn-default btn-white black"><i class="fa fa-android"></i>Download App</a>
                                </div>
                            </div> -->
                            <div class="slide" style="display:none">
                                <h1><span style="transition-delay: 1s">Display any kind of apps</span><span style="transition-delay: 1.5s">With great view</span></h1>
                                <p style="transition-delay: 2s">Make it convert more.</p>
                                <div class="button-group" style="transition-delay: 2.5s">
                                    {{-- <a href="#" class="btn btn-default btn-white"><i class="fa fa-apple"></i>App Store</a> --}}
                                    <a href="{{ URL::asset("apk/app.apk")}}" target="_blank" class="btn btn-default btn-white black"><i class="fa fa-android"></i>Download App</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-right hidden-sm hidden-xs">
                        <img class="banner-mock-1 wow animated fadeInUp"  data-wow-duration="1.5s" src="{{ URL::asset("img/edu.png")}}" height="600px" alt="...">
                    </div>
                </div>
            </div>
        </div>
    </section>
           
       
        
    </div>
    
    <footer>  
        <!--------------- Lower footer --------------->
        <div class="lower-footer">
            <div class="container">
                <div class="row">                    
                    <div class="col-sm-6">
                        <ul class="menu footer-menu">
                            {{-- <li class="menu-item"><a href="{{ URL('/terms-condition') }}"> Terms & Condition</a></li> --}}
                            <li class="menu-item"><a href="{{ URL('/privacy-policy') }}" class="black">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <p class="copyright black">&copy; 2021 Edudream, All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    <!--------------- Footer Script --------------->
    <script src="{{ URL::asset("Craze_Main_Template/assets/js/jquery.min.js")}}"></script> 
    <script src="{{ URL::asset("Craze_Main_Template/assets/js/bootstrap.min.js")}}"></script> 
    <script src="{{ URL::asset("Craze_Main_Template/assets/js/owl.carousel.js")}}"></script>
    <script src="{{ URL::asset("Craze_Main_Template/assets/js/jquery.countdown.min.js")}}"></script>
    <script src="{{ URL::asset("Craze_Main_Template/assets/js/wow.min.js")}}"></script>
    <script src="{{ URL::asset("Craze_Main_Template/assets/js/jquery.magnific-popup.min.js")}}"></script>
    <script src="{{ URL::asset("Craze_Main_Template/assets/js/jquery.stellar.min.js")}}"></script>
    <script src="{{ URL::asset("Craze_Main_Template/assets/js/jquery.ajaxchimp.min.js")}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmiJjq5DIg_K9fv6RE72OY__p9jz0YTMI"></script>
    <script src="{{ URL::asset("Craze_Main_Template/assets/js/map.js")}}"></script>
    <script src="{{ URL::asset("Craze_Main_Template/assets/js/custom.js")}}"></script>
</body>
</html>
