@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/notifications/pnotify.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/components_notifications_pnotify.js') }}></script>




@endsection
@section('content')
		<!-- Page content -->
		<div class="page-content">
			{{--@permission('add_categories')--}}
				<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_categories') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
			{{--@endpermission--}}
			
		<!-- Main content -->
			<div class="content-wrapper">

				<!-- Dashboard content -->
				<div class="row">
					<div class="col-lg-12 col-md-offset-0">

						<!-- Marketing campaigns -->
						<div class="panel panel-flat">
							

							<div class="table-responsive">
								<table class="table table-lg text-nowrap">
									<tbody>
										<tr class="active border-double">
											<td colspan="5">Categories</td>
											<td class="text-right">
												<span class="progress-meter" id="today-progress" data-progress="30"></span>
											</td>
										</tr>
									</tbody>
								</table>	
							</div>

							<div class="table-responsive">
								<table class="table text-nowrap">
									<thead>
										<tr>
											<th>Image</th>
											<th>Category Name</th>
											{{--<th>OneSignal Segment</th>
											<th>City</th>
											<th class="col-md-2">Members</th>
											<th class="col-md-2">Comments</th>--}}
										
											<th class="col-md-2">Status</th>
											<th class="text-center" style="width: 20px;">Action <i class="icon-arrow-down12"></i></th>
										</tr>
									</thead>
									<tbody>
										@foreach ($datas as $data)
										@php
											$timestamp=strtotime($data->timestamp);
											$date=date('d M,Y',$timestamp);
										@endphp
										
										<tr>
											<td>
												@if ($data->image==""||$data->image=="NULL")
													<div class="media-left media-middle">
													<a href="#"><img src="/mechanicalinsider/groupimage/default.png" class="img-circle img-xs" alt=""></a>
												</div>
												@else
												<div class="media-left media-middle">
													<a href="#"><img src="/mechanicalinsider/category_images/{{ $data->image }}" class="img-circle img-xs" alt=""></a>
												</div>
												@endif
											</td>
											<td><span class="text-muted">{{ $data->name }}</span></td>
											{{--<td><span class="text-muted"></span></td>
											<td><span class="text-muted"></span></td>
											<td><span class="text-success-600"><i class="icon-comment position-left"></i>{{ $data->comments }}</span></td>--}}
										
											@if($data->status=="public")
											<td><span class="label label-success">{{ $data->status }}</span></td>
											@else
											<td><span class="label bg-danger">{{ $data->status }}</span></td>
											@endif
											<td class="text-center">
												<ul class="icons-list">
													<li class="dropdown">
														<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
														<ul class="dropdown-menu dropdown-menu-right">
														{{--@permission('delete_categories')--}}
															<li><a data-toggle="modal" data-target="#modal_mini{{ $data->id }}" href=""><i class="icon-trash"></i> Delete</a></li>
															{{--@endpermission
															@permission('edit_categories')--}}
															<li><a href="{{ url('edit_categories/'.$data->id) }}"><i class="icon-pencil"></i> Edit</a></li>
															{{--@endpermission--}}
															
															
														</ul>
													</li>
												</ul>
											</td>
										</tr>
										<div  data-backdrop="false" id="modal_mini{{ $data->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Category</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this group.</p>

										</div>

										<div class="modal-footer">
										{{--@permission('delete_categories')--}}
											<form action="{{ url('delete_categories') }}" method="post">
											{{ csrf_field() }}
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<input type="hidden" name="del_id" value="{{ $data->id }}">
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										{{--@endpermission--}}
										</div>
									</div>
								</div>
							</div>
										@endforeach
										
									</tbody>
								</table>
							</div>
						</div>
						<!-- /marketing campaigns -->


						

					</div>

					
				</div>
				<!-- /dashboard content -->

			</div>
			<!-- /main content -->

			

		</div>
		<!-- /page content -->
			
@endsection
