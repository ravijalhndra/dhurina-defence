@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>
	
	{{-- <script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script> --}}

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>

	{{-- <script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_ckeditor.js') }}"></script>
 --}}



@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">
@php
	 $json=json_decode($record->jsondata,true);
	 $sub_tests=json_decode($record->post_description,true);
@endphp
						<!-- Basic layout-->
						<form id="login_form" action="{{ url('update_premium_test') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Premium Test</h5>
									
								</div>

								<div class="panel-body">
									<div class="col-md-10 col-md-offset-1">
										<div class="form-group">
											<label class="col-lg-2 control-label">Test Name</label>
											<div class="col-lg-10">
												<input type="text" name="test_name" class="form-control" value="{{ $json['testname'] }}" placeholder="Enter Test name" required>
											</div>
										</div>
										<script type="text/javascript">
										$(document).ready(function() {
										  $('.summernote').summernote();
										});
									</script>
										<div class="form-group">
											<label class="col-lg-2 control-label">Test Description:</label>
											<div class="col-lg-10">
												<textarea  class="summernote" name="description" required>{{ $json['description'] }}</textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Time:</label>
											<div class="col-lg-10">
												<input type="number" name="time" min="0" class="form-control" value="{{ $json['time'] }}" placeholder="Enter test time in minutes">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Categories and Questions:</label>
											<div class="input_fields_wrap col-md-10">
												<div class="col-md-12 field_section">
												    <div class="col-md-6">
												    <input type="text" class="form-control" name="cat[]" @if(isset($sub_tests[
												    	0]['test'])) value="{{ $sub_tests[0]['test'] }}" @endif placeholder="Enter Category">
												    </div>
												    <div class="col-md-3">
												    <input type="number" class="form-control" name="questions[]" @if(isset($sub_tests[
												    	0]['question_no']))  value="{{ $sub_tests[0]['question_no'] }}" @endif placeholder="Enter no. of questions">
												    </div>
												</div>
												@for($i=1; $i<count($sub_tests); $i++)
												<div class="col-md-12 field_section">
												    <div class="col-md-6">
												    <input type="text" class="form-control" name="cat[]" @if(isset($sub_tests[
												    	$i]['test'])) value="{{ $sub_tests[$i]['test'] }}" @endif placeholder="Enter Category">
												    </div>
												    <div class="col-md-3">
												    <input type="number" class="form-control" name="questions[]" @if(isset($sub_tests[
												    	$i]['question_no'])) value="{{ $sub_tests[$i]['question_no'] }}" @endif placeholder="Enter no. of questions">
												    </div>
												    <div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div>
												</div>
												@endfor
												
											</div>
											<div class="col-md-10 col-md-offset-2"><br>
												    	<button class="add_field_button btn btn-info btn-sm">Add More Fields</button>
													</div>
										</div>
										<script>

											$(document).ready(function() {
											    var max_fields      = 6; //maximum input boxes allowed
											    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
											    var add_button      = $(".add_field_button"); //Add button ID
											    var x = "<?php echo ((count($sub_tests))); ?>"; //initlal text box count

											    if(x>=max_fields)
										        {
										        	$('.add_field_button').hide();
										        }

											    $(add_button).click(function(e){ //on add input button click
											        e.preventDefault();
											        if(x < max_fields){ //max input box allowed
											            $(wrapper).append('<div class="col-md-12 field_section"><div class="col-md-6"><input type="text" class="form-control" name="cat[]" placeholder="Enter Category"></div><div class="col-md-3"><input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions"></div><div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div></div>'); //add input box
											            x++; //text box increment
											            

											            // $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
											        }
											        alert(x);

											        if(x>=max_fields)
												        {
												        	$('.add_field_button').hide();
												        }

											        
											    });
											    
											    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
											        e.preventDefault(); $(this).parents('div.field_section').remove();x--; 
											        $('.add_field_button').show();
											    });
											});
										</script>
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Posted By:</label>
											<div class="col-lg-10">
												<input type="text" name="postby" class="form-control" value="{{ $json['writer'] }}" placeholder="Enter writer name">
											</div>
										</div>

										
										<div class="form-group">
										<label class="col-lg-3 control-label">Price:</label>
										<div id="price" class="col-lg-9">
										<div class="col-md-6">
											<input type="radio" name="payment" value="free" @if ($json['payment']=='free')
											checked 
											@endif>Free
										</div>
										<div class="col-md-6">
											<input type="radio" name="payment" value="paid" @if ($json['payment']!="free")
											checked 
											@endif>Paid
										</div>
										<input style="display: none;" id="payment1" type="text" name="payment1" class="form-control" placeholder="Enter amount"
										@if ($json['payment']=="free")
											value=""
										@else
										value="{{ $json['payment'] }}" 
										@endif ">
										</div>
									</div>
									@if ($json['payment']!="free")
										<script>
											$(document).ready(function(){
												$("#payment1").show();
												});
										</script>
									@endif
									<script type="text/javascript">
										$('input:radio[name="payment"]').change(function(){
										    if($(this).val() == 'paid'){
										    	$("#payment1").show();
										    }
										    else
										    {
										    	$("#payment1").hide();
										    }
									});
									</script>
									<input type="hidden" name="id" value="{{ $record->id }}">
									<input type="hidden" name="old_topic" value="{{ $record->posttype }}">


									<div class="form-group">
											<label class="col-lg-2 control-label">Secret Key (Optional):</label>
											<div class="col-lg-10">
												<input type="password" name="secret_key" class="form-control" placeholder="Enter a secret key" value="@if(isset($json['secret_key'])){{ $json['secret_key'] }} @endif">
											</div>
										</div>
									@php if(isset($json['start_date']))
									{
										$start_date=explode(" ",$json['start_date']);
										if(!isset($start_date[1]))
										{
											$start_date[1]="";
										}
									}
									else
									{
										$start_date=["0"=>"","1"=>""];
									}
									if(isset($json['end_date']))
									{
										$end_date=explode(" ",$json['end_date']);
										if(!isset($end_date[1]))
										{
											$end_date[1]="";
										}
									}
									else
									{
										$end_date=["0"=>"","1"=>""];
									}
									@endphp
									<div class="form-group">
											<label class="col-lg-2 control-label">Start Date:</label>
											<div class="col-lg-4">
												<input id="start_date" data-value="{{ $start_date[0] }}" type="text" class="form-control" placeholder="Enter start date" name="start_date" value="{{ $start_date[0] }}">
											</div>

											<label class="col-lg-2 control-label">Start Time:</label>
											<div class="col-lg-4">
												<input id="start_time" data-value="{{ $start_date[1] }}" type="text" class="form-control" placeholder="Enter start time" name="start_time" value="{{ $start_date[1] }}">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">End Date:</label>
											<div class="col-lg-4">
												<input id="end_date" type="text" data-value="{{ $end_date[0] }}" class="form-control" placeholder="Enter end date" name="end_date" value="{{ $end_date[0] }}">
											</div>

											<label class="col-lg-2 control-label">End Time:</label>
											<div class="col-lg-4">
												<input id="end_time" type="text" data-value="{{ $end_date[1] }}" class="form-control" placeholder="Enter end time" name="end_time" value="{{ $end_date[1] }}">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Winner Points:</label>
											<div class="col-lg-10">
												<div class="col-lg-4">
													<input id="" type="text" class="form-control" placeholder="1st prize points" name="first_prize" value="@if(isset($json['first_prize'])){{ $json['first_prize'] }} @endif"">
												</div>
												<div class="col-lg-4">
													<input id="" type="text" class="form-control" placeholder="2nd prize points" name="second_prize" value="@if(isset($json['second_prize'])){{ $json['second_prize'] }} @endif">
												</div>
												<div class="col-lg-4">
													<input id="" type="text" class="form-control" placeholder="3rd prize points" name="third_prize" value="@if(isset($json['third_prize'])){{ $json['third_prize'] }} @endif">
												</div>
											</div>
										</div>


									<fieldset>
										<legend>Notification Image (optional)</legend>
										<div class="form-group" id="mySelect">
											<label class="col-lg-2 display-block text-semibold">Add Test Image</label>
											<div class="col-lg-10">
											<label class="radio-inline">
												<input type="radio" name="type" value="link" class="styled" checked="checked">
												Link
											</label>
											<label class="radio-inline">
												<input type="radio" name="type" value="image" class="styled">
												Image
											</label>
											</div>
										</div>
										
										<div class="form-group" id="link">
											<label class="col-lg-2 control-label">Enter Link :</label>
											<div class="col-lg-10">
												<input id="link" type="text" class="form-control" placeholder="Enter Link" name="link" value="@if(isset($json['image'])){{$json['image']}} @endif">
											</div>
										</div>
										
										<div class="form-group" style="display:none;" id="pdf">
											<label class="col-lg-2 control-label">Enter Image:</label>
											<div class="col-lg-10">
												<input id="pdf" type="file" class="form-control" name="image">
											</div>
										</div>
										<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="type"]').change(function(){
												    if(jQuery(this).val() == 'link'){
												    	jQuery("#link").show();
												    	jQuery("#pdf").hide();
												    }
												    else
												    {
												    	jQuery("#link").hide();
												    	jQuery("#pdf").show();
												    }
												});
											});
										</script>
										</fieldset>
										<fieldset>
											<legend></legend>
											<fieldset>
											<legend></legend>
											{{-- <div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to like this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_like" type="checkbox" class="switchery" @if(isset($json['add_like'])) @if($json['add_like']=="off") checked @endif @endif value="off">
													</label>
												</div>
											</div> --}}

											<div class="form-group">
												<div class="col-lg-4">
													<label>Show Solution to users?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="ans_status" type="checkbox" class="switchery" @if(isset($json['ans_status'])) @if($json['ans_status']=="yes") checked @endif @endif value="yes">
													</label>
												</div>
											</div>

											<div class="form-group">
												<div class="col-lg-4">
													<label>Negative Marking?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="negative_marks" type="checkbox" class="switchery" @if(isset($json['negative_marks'])) @if($json['negative_marks']=="yes") checked @endif @endif value="yes">
													</label>
												</div>
											</div>


											<div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to comment this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_comment" type="checkbox" class="switchery" @if(isset($json['add_comment'])) @if($json['add_comment']=="off") checked @endif @endif value="off">
													</label>
												</div>
											</div>
										</fieldset>
										

										<div class="text-right">
											<button id="submit" type="submit" class="btn btn-primary">Update<i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->


		</div>

@endsection
@section('js')
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				
				test_name:{
					required:true
				},
				description:{
					required:true
				},
				time:{
					required:true,
					number:true
				},
				postby:{
					required:true
				},
				sub_tests:{
					required:true
				},
				start_date:
				{
					required:true
				},
				end_date:
				{
					required:true
				},
				start_time:
				{
					required:true
				},
				end_time:
				{
					required:true
				}
			}
		});

		var current=new Date();
		var y=current.getFullYear();
		var Mo=current.getMonth()+1;
		var d=current.getDate();
		var hr=0;
		var m=0;
		var hr1=0;
		var m1=0;
		var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
		var date=new Date();

		var e_date=$("#end_date").val();
		if(e_date!="")
		{
			e_date=e_date.split('-');
			if(y=e_date[0])
			{
				if(Mo==e_date[1])
				{
					if(d==e_date[2])
					{
						hr1=current.getHours();
						m1=current.getMinutes();
					}
				}
			}
		}

		var endtime=$('#end_time').pickatime({
		  format: 'HH:i',
		  formatSubmit: 'HH:i',
		  interval: 30,
		  disable: [
		    { from: [0,0], to: [hr1,m1] }
		  ]
		});
		var s_date=$("#start_date").val();
		if(s_date!="")
		{
			s_date=s_date.split('-');
			if(y=s_date[0])
			{
				if(Mo==s_date[1])
				{
					if(d==s_date[2])
					{
						hr=current.getHours();
						m=current.getMinutes();
					}
				}
			}
		}
		var starttime=$('#start_time').pickatime({
		  format: 'HH:i',
		  formatSubmit: 'HH:i',
		  interval: 30,
		  disable: [
		    { from: [0,0], to: [hr,m] }
		  ],
		  onClose: function(){
		  		var date=$('#start_date').val();
		  		date=date.split("-");
			  	date=new Date(date[0],date[1]-1,date[2],00,00,00,0);
	    		date=date.setDate(date.getDate()-1);
	    		date=new Date(date);

		    	$("#end_time").val('');
			  	$("#enddate").val('');
			  	var end_date_picker = enddate.pickadate('picker');
				end_date_picker.set('disable',[
				    { from: [0,0,0], to: date }
				  ]);			  	
			  }
		});

		
		if(s_date=="")
		{
			s_date=yesterday;
		}
		else
		{
			s_date=new Date(s_date[0],s_date[1]-1,s_date[2],00,00,00,0);
			s_date=s_date.setDate(s_date.getDate()-1);
	    	s_date=new Date(s_date);
		}
		

		var enddate=$('#end_date').pickadate({
		  format: 'yyyy-mm-dd',
		  formatSubmit: 'yyyy-mm-dd',
		  disable: [
		    { from: [0,0,0], to: s_date }
		  ],
		  onClose: function(){
		  	var e_date=$('#end_date').val();
		  	e_date=e_date.split("-");
		  	var date=$('#start_date').val();
		  	date=date.split("-");
		  	var time=$('#start_time').val();
			time=time.split(":");		  	
		  	if(e_date[2]==date[2])
		  	{
		  		var hr=time[0];
				var m=time[1];
		  	}
		  	else
		  	{
		  		var hr=0;
		  		var m=0;
		  	}
		  	$("#end_time").val('');
		  	var end_time_picker = endtime.pickatime('picker');
			end_time_picker.set('disable',[
			    { from: [0,0,0], to: [hr,m] }
			  ]);
		  }		  
		});

		
		

		$('#start_date').pickadate({
		  format: 'yyyy-mm-dd',
		  formatSubmit: 'yyyy-mm-dd',
		  disable: [
		    { from: [0,0,0], to: yesterday }
		  ],
		  onClose: function()
		  {
		  	var now=new Date();
		  	date=$('#start_date').val();
		  	date=date.split("-");
		  	var d=now.getDate();
		  	if(d==date[2])
		  	{
		  		var hr=now.getHours();
				var m=now.getMinutes();
		  	}
		  	else
		  	{
		  		var hr=0;
		  		var m=0;
		  	}
		  	$("#start_time").val('');
		  	$("#end_time").val('');
		  	$("#enddate").val('');
		  	var start_time_picker = starttime.pickatime('picker');
			start_time_picker.set('disable',[
			    { from: [0,0], to: [hr,m] }
			  ]);
		  }		
		});



		// var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
		// var s_date=$('#start_date').val();
		// s_date=s_date.split("-");
		// s_date=new Date(s_date[0],s_date[1]-1,s_date[2],00,00,00,0);
		// if(s_date=="")
		// {
		// 	s_date=yesterday;
		// }

		// var enddate=$('#end_date').pickadate({
		//   format: 'yyyy-mm-dd',
		//   formatSubmit: 'yyyy-mm-dd',
		//   disable: [
		//     { from: [0,0,0], to: s_date }
		//   ]
		// });
		// $('#start_date').pickadate({
		//   format: 'yyyy-mm-dd',
		//   formatSubmit: 'yyyy-mm-dd',
		//   disable: [
		//     { from: [0,0,0], to: yesterday }
		//   ],
		//   onClose: function(){
		//   	var date=$('#start_date').val();
		//   	date=date.split("-");
		//   	date=new Date(date[0],date[1]-1,date[2],00,00,00,0);
		//   	$("#end_date").val('');
		//   	var picker = enddate.pickadate('picker');
		// 	picker.set('disable', [
		// 	    { from: [0,0,0], to: date }
		// 	  ]);
		//   }
		// });
	});
</script>
{{-- for switch buttons --}}
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	{{-- for switch buttons --}}

@endsection