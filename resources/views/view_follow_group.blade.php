@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_admin') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
				
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Groups</h3>
					</div>
				

					<table class="table datatable-basic">
						<thead>
							<tr>
								
								<th>Email</th>
								<th>Group Name</th>
								
								
							</tr>
						</thead>
						<tbody>
						
						

								<tr>
							
								<td>{{$email}}</td>
								
							  @php
							 
							 $numItems = count($group_name);
								$i = 0;
							 @endphp
								<td> @foreach($group_name as $key => $g)  {{$g->topic}} @if(++$i!= $numItems) , @endif @endforeach</td>
								

							{{--	<td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												@permission('delete_admin')
												<li><a data-toggle="modal" data-target="#modal_mini{{ $admin->id }}" href="#"><i class="icon-trash"></i> Delete</a></li>
												@endpermission

												@permission('edit_admin')
												<li><a href="{{ url('edit_admin/'.$admin->id) }}"><i class="icon-pencil"></i> Edit</a></li>
												@endpermission
												
											</ul>
										</li>
									</ul>
								</td>
								
								<div  data-backdrop="false" id="modal_mini{{ $admin->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Admin</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this admin.</p>

										</div>

										<div class="modal-footer">
											@permission('delete_admin')
											<form action="{{ url('delete_admin') }}" method="post">
											{{ csrf_field() }}
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<input type="hidden" name="del_id" value="{{ $admin->id }}">
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
											@endpermission
										</div>
									</div>
								</div>
							</div> --}}

							</tr>
											
							
							
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>
@endsection
