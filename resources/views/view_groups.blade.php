@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/notifications/pnotify.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/components_notifications_pnotify.js') }}></script>




@endsection
@section('content')
		<!-- Page content -->
		<div class="page-content">
			@permission('add_groups')
				<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_groups') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
			@endpermission
			
		<!-- Main content -->
			<div class="content-wrapper">

				<!-- Dashboard content -->
				<div class="row">
					<div class="col-lg-12 col-md-offset-0">

						<!-- Marketing campaigns -->
						<div class="panel panel-flat">
							

							<div class="table-responsive">
								<table class="table table-lg text-nowrap">
									<tbody>
										<tr class="active border-double">
											<td colspan="5">Groups</td>
											<td class="text-right">
												<span class="progress-meter" id="today-progress" data-progress="30"></span>
											</td>
										</tr>
									</tbody>
								</table>	
							</div>

							<div class="table-responsive">
								<table class="table text-nowrap">
									<thead>
										<tr>
											<th>Title</th>
											<th>OneSignal Segment</th>
											<th>City</th>
											<th class="col-md-2">Members</th>
											<th class="col-md-2">Comments</th>
										
											<th class="col-md-2">Status</th>
											<th class="text-center" style="width: 20px;">Action <i class="icon-arrow-down12"></i></th>
										</tr>
									</thead>
									<tbody>
										@foreach ($discussions as $discussion)
										@php
											$timestamp=strtotime($discussion->timestamp);
											$date=date('d M,Y',$timestamp);
										@endphp
										
										<tr>
											<td>
												@if ($discussion->image==""||$discussion->image=="NULL")
													<div class="media-left media-middle">
													<a href="#"><img src="/mechanicalinsider/groupimage/default.png" class="img-circle img-xs" alt=""></a>
												</div>
												@else
												<div class="media-left media-middle">
													<a href="#"><img src="/mechanicalinsider/groupimage/{{ $discussion->image }}" class="img-circle img-xs" alt=""></a>
												</div>
												@endif
												<div class="media-left">
													<div class=""><a style="font-size: 13px;" href="#" class="text-default text-bold">{{ $discussion->topic }}</a></div>
													<div class="text-muted text-size-small">
														<span class="icon-calendar position-left"></span>
														{{ $date }}
													</div>
												</div>
											</td>
											<td><span class="text-muted">{{ $discussion->id }}</span></td>
											<td><span class="text-muted">{{ $discussion->city }}</span></td>
											<td><span class="text-muted">{{ $discussion->member }}</span></td>
											<td><span class="text-success-600"><i class="icon-comment position-left"></i>{{ $discussion->comments }}</span></td>
										
											@if($discussion->status=="public")
											<td><span class="label label-success">{{ $discussion->status }}</span></td>
											@else
											<td><span class="label bg-danger">{{ $discussion->status }}</span></td>
											@endif
											<td class="text-center">
												<ul class="icons-list">
													<li class="dropdown">
														<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
														<ul class="dropdown-menu dropdown-menu-right">
														@permission('delete_groups')
															<li><a data-toggle="modal" data-target="#modal_mini{{ $discussion->id }}" href=""><i class="icon-trash"></i> Delete</a></li>
															@endpermission
															@permission('edit_groups')
															<li><a href="{{ url('edit_group/'.$discussion->id) }}"><i class="icon-pencil"></i> Edit</a></li>
															@endpermission
															
															
														</ul>
													</li>
												</ul>
											</td>
										</tr>
										<div  data-backdrop="false" id="modal_mini{{ $discussion->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Group</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this group.</p>

										</div>

										<div class="modal-footer">
										@permission('delete_groups')
											<form action="{{ url('delete_group') }}" method="post">
											{{ csrf_field() }}
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<input type="hidden" name="del_id" value="{{ $discussion->id }}">
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										@endpermission
										</div>
									</div>
								</div>
							</div>
										@endforeach
										
									</tbody>
								</table>
							</div>
						</div>
						<!-- /marketing campaigns -->


						

					</div>

					
				</div>
				<!-- /dashboard content -->

			</div>
			<!-- /main content -->

			

		</div>
		<!-- /page content -->
			
@endsection
