@extends('layouts.main')
@section('js_head')

@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('add_admin') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Admin</h5>
									
								</div>

								<div class="panel-body">
									<div class="col-md-10  col-md-offset-1">
									
										<div class="form-group">
											<label class="col-lg-2 control-label">Name:</label>
											<div class="col-lg-10">
												<input type="text" name="name" class="form-control" placeholder="Enter name" required autofocus> 
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Email:</label>
											<div class="col-lg-10">
												<input type="email" name="email" class="form-control" placeholder="Enter Email">
											</div>
										</div>
																<!-- Select All and filtering options -->
								
										<div class="form-group">
											<label class="col-lg-2 control-label">Group:</label>
											<div class="col-lg-10">
												<div class="multi-select-full">
										<select class="multiselect-select-all-filtering" multiple="multiple" name="group[]">
										   @foreach ($query as $q)
												<option value="{{ $q->id }}">{{ $q->topic }}</option>
											@endforeach
										</select>
									</div>
											</div>
										</div>
									<div class="form-group">
											<label class="col-lg-2 control-label">Permissions:</label>
											<div class="col-lg-10">
												<div class="multi-select-full">
										<select class="multiselect-select-all-filtering" multiple="multiple" name="link[]">
										@foreach ($permissions as $q1)
												<option value="{{ $q1->id }}">{{ $q1->display_name }}</option>
											@endforeach
										</select>
									</div>
											</div>
										</div>
										
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Role:</label>
											<div class="col-lg-10">
												<div class="multi-select-full">
										<select class="form-control"  name="role" id="role">
										
												<option value="admin">Admin</option>
												<option value="mentor">Mentor</option>
											
										</select>
									</div>
											</div>
										</div>
										
									<div id='mentors' style="display: none;">
										
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Highest Qualification:</label>
											<div class="col-lg-10">
												<div class="multi-select-full">
										<select class="form-control"  name="qualification">
										
												<option value="">  </option>
									            <option value="B.A"> B.A </option>
									            <option value="M.A"> M.A </option>
									            <option value="B.E"> B.E </option>
									            <option value="M.E"> M.E </option>
									            <option value="B.Tech"> B.Tech </option>
									            <option value="M.Tech"> M.Tech </option>
									            <option value="B.Sc"> B.Sc </option>
									            <option value="M.Sc"> M.Sc </option>
									            <option value="BBA"> BBA </option>
									            <option value="MBA"> MBA </option>
									            <option value="BCA"> BCA </option>
									            <option value="MCA"> MCA </option>
									            <option value="P.hd"> P.hd </option>
									            <option value="M.Phil"> M.Phil </option>
									            <option value="B.Arch"> B.Arch </option>
									            <option value="MBBS"> MBBS </option>
									            <option value="MD"> MD </option>
									            <option value="BDS"> BDS </option>
									            <option value="MDS"> MDS </option>
									            <option value="BPT"> BPT </option>
									            <option value="B.Pharm"> B.Pharm </option>
									            <option value="M.Pharm"> M.Pharm </option>
									            <option value="B.Com"> B.Com </option>
									            <option value="M.Com"> M.Com </option>
									            <option value="C.A"> C.A </option>
									            <option value="ICWA"> ICWA </option>
									            <option value="B.Cs"> B.Cs </option>
									            <option value="12th"> 12th </option>
									            <option value="11th"> 11th </option>
									            <option value="LLB"> LLB </option>
									            <option value="other"> OTHER </option>
											
										</select>
									</div>
											</div>
										</div>
										
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Years of experience in teaching :</label>
											<div class="col-lg-10">
												<div class="multi-select-full">
										<select class="form-control"  name="experience">
										
									            <option value="">  </option>
									            <option value="Less than a year"> Less than a year </option>
									            <option value="Less Two years"> Less Two years </option>
									            <option value="Less Four years"> Less Four years </option>
									            <option value="More than 5 years"> More than 5 years </option>
											
										</select>
									</div>
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-lg-2 control-label">City:</label>
											<div class="col-lg-10">
												<input type="text" name="city" class="form-control" placeholder="Enter City" > 
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Date of Birth:</label>
											<div class="col-lg-10">
												<input type="date" name="dob" class="form-control" placeholder="Enter Date of Birth"> 
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Educational Documents:</label>
											<div class="col-lg-10">
												<input type="file" name="documents[]" class="form-control" placeholder="Educational Documents" multiple > 
											</div>
										</div>
										
										
										
									</div>	
										
								<!-- /select All and filtering options -->
										<div class="text-right">
											<button type="submit" class="btn btn-primary">Add<i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->

		</div>
		<script>
			
			
			$("#role").change(function () {
        var val = this.value;
        if(val == 'mentor')
        {
        	$('#mentors').show(500);
        }
        if(val == 'admin')
        {
        	$('#mentors').hide(500);
        }
    });
		</script>

@endsection
