@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>
	
	

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>

	{{-- <script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_ckeditor.js') }}"></script>
 --}}



@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('add_premium_test') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Premium Test</h5>
									
								</div>

								<div class="panel-body">
									<div class="col-md-10 col-md-offset-1">								
										<div class="form-group">
											<label class="col-lg-2 control-label">Test Name</label>
											<div class="col-lg-10">
												<input type="text" name="test_name" class="form-control" placeholder="Enter Test name" required>
											</div>
										</div>
										<script type="text/javascript">
										$(document).ready(function() {
										  $('.summernote').summernote();
										});
									</script>
										<div class="form-group">
											<label class="col-lg-2 control-label">Test Description:</label>
											<div class="col-lg-10">
												<textarea  class="summernote" name="description" required></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">Time:</label>
											<div class="col-lg-10">
												<input type="number" name="time" min="0" class="form-control" placeholder="Enter test time in minutes">
											</div>
										</div>

										{{-- <div class="form-group">
											<label class="col-lg-2 control-label">Categories and Questions:</label>
											<div class="col-lg-10">
												<input type="text" name="sub_tests" class="form-control" placeholder="Add categories and questions like reasoning=10, math=20">
											</div>
										</div> --}}
										<div class="form-group">
											<label class="col-lg-2 control-label">Categories and Questions:</label>
											<div class="input_fields_wrap col-md-10">
												<div class="col-md-12 field_section">
												    <div class="col-md-6">
												    <input type="text" class="form-control" name="cat[]" placeholder="Enter Category">
												    </div>
												    <div class="col-md-3">
												    <input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions">
												    </div>
												    
												</div>

												<div class="col-md-12 field_section"><div class="col-md-6"><input type="text" class="form-control" name="cat[]" placeholder="Enter Category"></div><div class="col-md-3"><input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions"></div><div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div></div>
											</div>
											<div class="col-md-10 col-md-offset-2"><br>
												    	<button class="add_field_button btn btn-info btn-sm">Add More Fields</button>
													</div>
										</div>
										<script>
											$(document).ready(function() {
											    var max_fields      = 4; //maximum input boxes allowed
											    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
											    var add_button      = $(".add_field_button"); //Add button ID
											    
											    var x = 1; //initlal text box count
											    $(add_button).click(function(e){ //on add input button click
											        e.preventDefault();
											        if(x <= max_fields){ //max input box allowed
											            x++; //text box increment
											            $(wrapper).append('<div class="col-md-12 field_section"><div class="col-md-6"><input type="text" class="form-control" name="cat[]" placeholder="Enter Category"></div><div class="col-md-3"><input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions"></div><div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div></div>'); //add input box

											            if(x>max_fields)
												        {
												        	$('.add_field_button').hide();
												        }

											            // $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
											        }
											        
											    });
											    
											    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
											        e.preventDefault(); $(this).parents('div.field_section').remove(); x--;
											        $('.add_field_button').show();
											    })
											});
										</script>
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Posted By:</label>
											<div class="col-lg-10">
												<input type="text" name="postby" class="form-control" placeholder="Enter writer name">
											</div>
										</div>
										
										

										<div class="form-group">
										<label class="col-lg-3 control-label">Price:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" name="payment" value="free" checked="checked">
												Free
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" name="payment" value="paid">
												Paid
											</label>
										</div>
									</div>
									
										<div class="form-group" style="display:none" id="payment1">
										<label class="col-lg-3 control-label">Amount:</label>
										<div class="col-lg-9">
											<input type="number" class="form-control" name="amount" >
										</div>
									</div>
									<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="payment"]').change(function(){
												    if(jQuery(this).val() == 'paid'){
												    	jQuery("#payment1").show();
												    }
												    else
												    {
												    	jQuery("#payment1").hide();
												    }
												});
											});
										</script>


										<div class="form-group">
											<label class="col-lg-2 control-label">Secret Key (Optional):</label>
											<div class="col-lg-10">
												<input type="password" name="secret_key" class="form-control" placeholder="Enter a secret key for this test">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Start Date:</label>
											<div class="col-lg-4">
												<input id="start_date" type="text" class="form-control" placeholder="Enter start date" name="start_date">
											</div>

											<label class="col-lg-2 control-label">Start Time:</label>
											<div class="col-lg-4">
												<input id="start_time" type="text" class="form-control" placeholder="Enter start time" name="start_time">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label">End Date:</label>
											<div class="col-lg-4">
												<input id="end_date" type="text" class="form-control" placeholder="Enter end date" name="end_date">
											</div>


											<label class="col-lg-2 control-label">End Time:</label>
											<div class="col-lg-4">
												<input id="end_time" type="text" class="form-control" placeholder="Enter end time" name="end_time">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Winner Points:</label>
											<div class="col-lg-10">
												<div class="col-lg-4">
													<input id="" type="text" class="form-control" placeholder="1st prize points" name="first_prize">
												</div>
												<div class="col-lg-4">
													<input id="" type="text" class="form-control" placeholder="2nd prize points" name="second_prize">
												</div>
												<div class="col-lg-4">
													<input id="" type="text" class="form-control" placeholder="3rd prize points" name="third_prize">
												</div>
											</div>
										</div>
										

										<fieldset>
										<legend>Notification Image (optional)</legend>
										<div class="form-group" id="mySelect">
											<label class="col-lg-2 display-block text-semibold">Add Test Image</label>
											<div class="col-lg-10">
											<label class="radio-inline">
												<input type="radio" name="type" value="link" class="styled" checked="checked">
												Link
											</label>
											<label class="radio-inline">
												<input type="radio" name="type" value="image" class="styled">
												Image
											</label>
											</div>
										</div>
										
										<div class="form-group" id="link">
											<label class="col-lg-2 control-label">Enter Link :</label>
											<div class="col-lg-10">
												<input id="link" type="text" class="form-control" placeholder="Enter Link" name="link">
											</div>
										</div>
										
										<div class="form-group" style="display:none;" id="pdf">
											<label class="col-lg-2 control-label">Enter Image:</label>
											<div class="col-lg-10">
												<input id="pdf" type="file" class="form-control" name="image">
											</div>
										</div>
										<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="type"]').change(function(){
												    if(jQuery(this).val() == 'link'){
												    	jQuery("#link").show();
												    	jQuery("#pdf").hide();
												    }
												    else
												    {
												    	jQuery("#link").hide();
												    	jQuery("#pdf").show();
												    }
												});
											});
										</script>
										</fieldset>
										<fieldset>
											<legend></legend>
											{{-- <div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to like this post </label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_like" type="checkbox" class="switchery" value="off">
													</label>
												</div>
											</div> --}}
											<div class="form-group">
												<div class="col-lg-4">
													<label>Show Answere to users?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="ans_status_new" checked type="checkbox" class="switchery" value="yes">
													</label>
												</div>
											</div>

												<div class="form-group">
												<div class="col-lg-4">
													<label>Offline Status</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="offline_status"  type="checkbox" class="switchery" value="no">
													</label>
												</div>
											</div>

											<div class="form-group">
												<div class="col-lg-4">
													<label>Negative Marking?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="negative_marks"   type="checkbox" class="switchery" value="no">
													</label>
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to comment this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_comment" type="checkbox" class="switchery" value="off">
													</label>
												</div>
											</div>
										</fieldset>

										<div class="text-right">
											<button id="submit" type="submit" class="btn btn-primary">Add<i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->


		</div>

@endsection
@section('js')
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				
				test_name:{
					required:true
				},
				description:{
					required:true
				},
				time:{
					required:true,
					number:true
				},
				postby:{
					required:true
				},
				sub_tests:{
					required:true
				},
				start_date:
				{
					required:true
				},
				end_date:
				{
					required:true
				},
				start_time:
				{
					required:true
				},
				end_time:
				{
					required:true
				}
			}
		});

		var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
		var date=new Date();

		var endtime=$('#end_time').pickatime({
		  format: 'HH:i',
		  formatSubmit: 'HH:i',
		  interval: 30
		});
		var starttime=$('#start_time').pickatime({
		  format: 'HH:i',
		  formatSubmit: 'HH:i',
		  interval: 30,
		  onClose: function(){
		  		var date=$('#start_date').val();
		  		date=date.split("-");
			  	date=new Date(date[0],date[1]-1,date[2],00,00,00,0);
	    		date=date.setDate(date.getDate()-1);
	    		date=new Date(date);

		    	$("#end_time").val('');
			  	$("#enddate").val('');
			  	var end_date_picker = enddate.pickadate('picker');
				end_date_picker.set('disable',[
				    { from: [0,0,0], to: date }
				  ]);			  	
			  }
		});
		var enddate=$('#end_date').pickadate({
		  format: 'yyyy-mm-dd',
		  formatSubmit: 'yyyy-mm-dd',
		  onClose: function(){
		  	var e_date=$('#end_date').val();
		  	e_date=e_date.split("-");
		  	var date=$('#start_date').val();
		  	date=date.split("-");
		  	var time=$('#start_time').val();
			time=time.split(":");		  	
		  	if(e_date[2]==date[2])
		  	{
		  		var hr=time[0];
				var m=time[1];
		  	}
		  	else
		  	{
		  		var hr=0;
		  		var m=0;
		  	}
		  	$("#end_time").val('');
		  	var end_time_picker = endtime.pickatime('picker');
			end_time_picker.set('disable',[
			    { from: [0,0,0], to: [hr,m] }
			  ]);
		  }		  
		});

		
		

		$('#start_date').pickadate({
		  format: 'yyyy-mm-dd',
		  formatSubmit: 'yyyy-mm-dd',
		  disable: [
		    { from: [0,0,0], to: yesterday }
		  ],
		  onClose: function()
		  {
		  	var now=new Date();
		  	date=$('#start_date').val();
		  	date=date.split("-");
		  	var d=now.getDate();
		  	if(d==date[2])
		  	{
		  		var hr=now.getHours();
				var m=now.getMinutes();
		  	}
		  	else
		  	{
		  		var hr=0;
		  		var m=0;
		  	}
		  	$("#start_time").val('');
		  	$("#end_time").val('');
		  	$("#enddate").val('');
		  	var start_time_picker = starttime.pickatime('picker');
			start_time_picker.set('disable',[
			    { from: [0,0], to: [hr,m] }
			  ]);
		  }		
		});
	});
</script>
<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
@endsection