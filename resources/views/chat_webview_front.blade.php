<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.container {
  position: inherit;
  width: 100%;
  overflow: hidden;
  padding-top: 56.25%; /* 16:9 Aspect Ratio */
}

.responsive-iframe {
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
  border: none;
}
</style>
<title>
Test WebView
</title>

</head>
<body>
<div class="container">

<iframe class="responsive-iframe" src="{{ $url }}" allowfullscreen></iframe>
</div>

</body>