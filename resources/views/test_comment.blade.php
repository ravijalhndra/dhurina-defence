@extends('layouts.main')
@section('js_head')
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

                <!-- Horizontal form options -->
				{{-- <div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('test_reply') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}
                        <input id="test_id" name="test_id" type="hidden" value="{{$id}}" />
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Reply</h5>									
								</div>

								<div class="panel-body">
									<div class="form-group">
										<label class="col-lg-2 control-label">Reply </label>
										<div class="col-lg-10">
											<input type="text" name="reply" class="form-control" placeholder="Enter reply">
										</div>
									</div>										

									<div class="text-right">
										<button id="submit" type="submit" class="btn btn-primary">Reply<i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>							
						</form>
                    </div>
				</div> --}}
					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Comment</h3>							
					</div>				

					<table class="table">
						<thead>
							<tr>				
                                <th>Name</th>
                                <th>Mobile</th>
								<th>Comment</th>                                
                                <th>Action</th>
							</tr>
						</thead>
						<tbody>
						@foreach($comment as $u)
                            @php $json=json_decode($u->jsondata); @endphp
							<tr>
								<td>@if($u->d_com == 1) Admin @else {{ $u->username }} @endif </td>
                                <td>@if($u->d_com == 1)  @else {{ $u->mobile }} @endif</td>
                                <td>{{ $json->comments }}</td>
                                <td class="text-center">										
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
											<ul class="dropdown-menu dropdown-menu-right" >												
												@if(Auth::user()->role == 'admin' )
													<li><a href="javascript:;"  onClick="fun_delete({{ $u->id }})"><i class="icon-trash"></i> Delete</a></li>
													<li><a href="{{route('add_reply_post',['id'=>encrypt ($u->id),'type'=>'post'])}}" ><i class="icon-comment"></i> Reply</a></li>

												@endif
											</ul>
										</li>
									</ul>									
								</td> 					
                                
                          	</tr>				
						@endforeach								
						</tbody>
					</table>
				</div>
				
                {{ $comment->links() }}
			</div>
		</div>

        <div  data-backdrop="false" id="modal_mini" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Click on Yes if you want to delete this Record.</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('delete_reply_test') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del_id" name="del_id" value="">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		

@endsection
@section('js')

	<script type="text/javascript">

		

		function fun_delete(id)
		{	
			var val = [];
			val.push(id);
			$('#del_id').val(val);
			$('#modal_mini').modal('toggle');		
		}

		function fun_publish(id,type)
		{
			$('#del').val(id);
			$('#type').val(type);
			$('#modal_2').modal('toggle');	
		}

		

	</script>
@endsection

