@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
			@permission('add_question')
				@if($add_question=='true')
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_questions/'.request()->segment(2)) }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
				@endif
			@endpermission

				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">View {{ $test_name }} Questions</h4>
					</div>

					<div class="panel-body">
						
					</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>ID</th>
								<th>Subject</th>
								<th>Question</th>
								<th>Answer</th>
								<th>Hint</th>
								{{-- <th>Option A</th>
								<th>Option B</th>
								<th>Option C</th>
								<th>Option D</th>
								<th>Option E</th>
								<th>Solution</th> --}}
								
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
						@php
							$count=1;
						@endphp
						@foreach ( $query as $data)
						@php
						$json=json_decode($data->jsondata,true);
						// dd($json);
							// dd($json);
						@endphp
						
						
								<tr>
								<td>{{ $count }}</td>
								<td>{{ $data->subject_name }}</td>
								<td>
									@if ($json['data']['question']['image']=="NULL")
										<span>{!! $json['data']['question']['value'] !!}</span>
									@else
										<span>{!! $json['data']['question']['value'] !!}</span><br>
										<img src="/mechanicalinsider/admin/{{ $json['data']['question']['image'] }}" width="80" height="80">
									@endif
								</td>
								<td>
									@if ($json['data']['answer']['image']=="NULL")
										<span>{{ $json['data']['answer']['value'] }}</span>
									@else
										<span>{{ $json['data']['answer']['value'] }}</span><br>
										<img src="/mechanicalinsider/admin/{{ $json['data']['answer']['image'] }}" width="80" height="80">
									@endif
								</td>
								<td>
									@if ($json['data']['hint']['image']=="NULL")
										<span>{!! $json['data']['hint']['value'] !!}</span>
									@else
										<span>{!! $json['data']['hint']['value'] !!}</span><br>
										<img src="/mechanicalinsider/admin/{{ $json['data']['hint']['image'] }}" width="80" height="80">
									@endif
								</td>
								
								
								
								<td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												<li><a data-toggle="modal" data-target="#modal_mini{{ $data->id }}" href="#"><i class="icon-trash"></i> Delete</a></li>
												<li><a href="{{ url('question/'.$data->id) }}"><i class="icon-eye"></i> Details</a></li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<div  data-backdrop="false" id="modal_mini{{ $data->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Question</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this question.</p>

										</div>

										<div class="modal-footer">
											
											<form action="{{ url('delete_question') }}" method="post">
												{{ csrf_field() }}
												<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
												<input type="hidden" name="del_id" value="{{ $data->id }}">
												<input type="hidden" name="test_id" value="{{ $data->testid }}">
												<button type="submit" class="btn btn-primary">Yes</button>
											</form>
											
										</div>
									</div>
								</div>
							</div>
							@php
								$count++;
							@endphp
						@endforeach
							
							
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>
@endsection
