@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>
@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('assign_manully') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Assign course</h5>
									
								</div>

								<div class="panel-body">
									<div class="col-md-10  col-md-offset-1">                                        
                                        <div class="form-group" id="mySelect">
											<label class="col-lg-2 display-block text-semibold">Select Type</label>
											<div class="col-lg-10">
                                                <label class="radio-inline">
                                                    <input type="radio" name="type" value="course" class="styled" checked="checked">Course
                                                </label>
                                                
                                                <label class="radio-inline">
                                                    <input type="radio" name="type" value="notes" class="styled">Notes
                                                </label>

                                                <label class="radio-inline">
                                                    <input type="radio" name="type" value="ebook" class="styled">Ebook
												</label>
												
												<label class="radio-inline">
                                                    <input type="radio" name="type" value="test_series" class="styled">Test series
                                                </label> 
											</div>
										</div>
									
										<div class="form-group" id="course_select">
											<label class="col-lg-2 control-label text">Select Courses:</label>
											<div class="col-lg-10">
												<select name="course_id" data-placeholder="Select one..."  class="select select-border-color border-warning course" >
                                                    <option value="" selected > Select one..</option>
                                                    @foreach ($couse as $c)                                                        
														<option value="{{ $c->id }}">{{ $c->name }}</option>
													@endforeach
												</select>
											</div>
                                        </div>

                                        <div class="form-group" id="note_select">
											<label class="col-lg-2 control-label text">Select Notes:</label>
											<div class="col-lg-10">
												<select name="note_id" data-placeholder="Select one..."  class="select select-border-color border-warning notes">
                                                    <option value="" selected > Select one..</option>
                                                    @foreach ($notes as $n)                                                        
														<option value="{{ $n->id }}">{{ $n->name }}</option>
													@endforeach
												</select>
											</div>
                                        </div>

                                        <div class="form-group " id="ebook_select">
											<label class="col-lg-2 control-label text">Select Ebook:</label>
											<div class="col-lg-10">
												<select name="ebook_id" data-placeholder="Select one..."  class="select select-border-color border-warning ebook">
                                                    <option value="" selected > Select one..</option>
                                                    @foreach ($ebook as $e)                                                        
														<option value="{{ $e->id }}">{{ $e->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										
										<div class="form-group " id="test_select">
											<label class="col-lg-2 control-label text">Select Test Series:</label>
											<div class="col-lg-10">
												<select name="test_id" data-placeholder="Select one..."  class="select select-border-color border-warning test">
                                                    <option value="" selected > Select one..</option>
                                                    @foreach ($test as $t)                                                        
														<option value="{{ $t->id }}">{{ $t->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
										
                                        <div class="form-group" >
											<label class="col-lg-2 control-label">User Mobile:</label>
											<div class="col-lg-10">
                                                <input type="text" name="mobile" minlength="10"  maxlength="10"  class="form-control onlynumber" placeholder="Enter User Mobile">
											</div>
										</div>

										
										<div class="text-right">
											<button type="submit" class="btn btn-primary">Insert <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->

		</div>

	
    <script type="text/javascript">
        
        $(document).ready(function(){

            $('#note_select').hide();
			$('#ebook_select').hide();
			$('#test_select').hide();
            $('.course').prop('required',true);

            
        });

		jQuery(document).ready(function(){
			jQuery('input:radio[name="type"]').change(function(){

				if(jQuery(this).val() == 'notes'){
                    
                    $('#note_select').show();
                    $('.notes').prop('required',true);

                    //hide select
                    $('#course_select').hide();
					$('#ebook_select').hide();
					$('#test_select').hide();

                    $('.course').prop('required',false);
					$('.ebook').prop('required',false);
					$('.test').prop('required',false);

				}
				else if(jQuery(this).val() == 'ebook'){
                    $('#ebook_select').show();
                    $('.ebook').prop('required',true);

                    //hide select
                    $('#course_select').hide();
					$('#note_select').hide();
					$('#test_select').hide();

                    $('.course').prop('required',false);
					$('.notes').prop('required',false);
					$('.test').prop('required',false);
				}
				else if(jQuery(this).val() == 'test_series'){
                    $('#test_select').show();
                    $('.test').prop('required',true);

                    //hide select
                    $('#course_select').hide();
                    $('#note_select').hide();
					$('#ebook_select').hide();

                    $('.course').prop('required',false);
					$('.notes').prop('required',false);
					$('.ebook').prop('required',false)

                }else{

                    $('#course_select').show();
                    $('.course').prop('required',true);
                    
                    //hide select
                    $('#ebook_select').hide();
                    $('#note_select').hide();
					$('#test_select').hide();

                    $('.ebook').prop('required',false);
					$('.notes').prop('required',false);
					$('.test').prop('required',false);
                }

			});
		});
	</script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
@endsection
