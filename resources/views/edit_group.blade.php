@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>

	<!-- Theme JS files -->
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				topic:{
					required:true
				},
				city:{
					required:true
				},
				status:{
					required:true
				},
				payment:{
					requried:true
				},
				payment1:{
					number:true
				}
				
				
			}
		});
	});
</script>
<script>
	
	$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



</script>
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">
{{-- @php
	dd($discussion);
@endphp --}}
						<!-- Basic layout-->
						<form id="login_form" action="{{ url('update_group') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Group</h5>
								
								</div>

								<div class="panel-body">
									<div class="form-group">
									<label class="col-lg-3 control-label">Select Categories:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="categories[]" data-placeholder="Select categories..." multiple="multiple" class="multiselect-select-all-filtering" required>
											@php $cats=explode(',',$discussion->category);  @endphp
											@if($categories[0]!="")
												@foreach ($categories as $c)
													<option value="{{ $c->id }}" @if(in_array($c->id,$cats)) selected @endif>{{ $c->name }}</option> 
												@endforeach
												@endif
											
										</select>
									</div>
								</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Group Name:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="topic" value="{{ $discussion->topic }}">
										</div>
									</div>
									<!-- <div class="form-group">
										<label class="col-lg-3 control-label">OneSignal Segment:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="onesignal_segment"  value="{{ $discussion->onesignal_segment }}" required>
										</div>
									</div> -->
									<div class="form-group">
										<label class="col-lg-3 control-label">City:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="city" value="{{ $discussion->city }}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Price:</label>
										<div id="price" class="col-lg-9">
										<div class="col-md-6">
											<input type="radio" name="payment" value="0" @if ($discussion->payment=='0')
											checked 
											@endif>Free
										</div>
										<div class="col-md-6">
											<input type="radio" name="payment" value="paid" @if ($discussion->payment!="0")
											checked 
											@endif>Paid
										</div>
										<input style="display: none;" id="payment1" type="text" name="payment1" class="form-control" placeholder="Enter amount"
										@if ($discussion->payment=="0")
											value=""
										@else
										value="{{ $discussion->payment }}" 
										@endif ">
										</div>
									</div>
									@if ($discussion->payment!="0")
										<script>
											$(document).ready(function(){
												$("#payment1").show();
												});
										</script>
									@endif
									<script type="text/javascript">
										$('input:radio[name="payment"]').change(function(){
										    if($(this).val() == 'paid'){
										    	$("#payment1").show();
										    }
										    else
										    {
										    	$("#payment1").hide();
										    }
									});
									</script>									


									<div class="form-group">
										<label class="col-lg-3 control-label">Status</label>
										<div class="col-lg-9">
											<select name="status" class="bootstrap-select" data-width="100%">
												<option value="public"@if ($discussion->status=="public") selected @endif >Public</option>
												<option value="private"@if ($discussion->status=="private") selected @endif >Private</option>
											</select>
										</div>
									</div>

										<div class="form-group">

										<input type="hidden" name="old_image" value="{{ $discussion->image }}">
										<input type="hidden" name="id" value="{{ $discussion->id }}">

										<label class="col-lg-3 control-label"  style="top:40px;">
											 <div class="input-group-btn">
							                  <div class="fileUpload btn btn-danger fake-shadow">
							                    <span><i class="glyphicon glyphicon-upload"></i> Upload Logo</span>
							                    <input id="logo-id" name="logo" type="file" class="attachment_upload" >
							                  </div>
							                </div>
										</label>
										<div class="col-lg-9">
											 <div class="main-img-preview">
											<img class="thumbnail img-preview" style="min-height: 150px;" src="../../../mechanicalinsider/groupimage/{{ $discussion->image }}" title="Preview Logo" >
							              </div>
																		 <div class="input-group">
							                <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled" style="display:none">
							               
							              </div>
										</div>
									</div>
									<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
									<div class="text-right">
										<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Update <i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')
