@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>


@endsection
@section('content')
		<!-- Page content -->
		<div class="page-content">
				@if(Auth::user()->role == 'admin' )
				<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
					<li>
						<a href="{{ url('pdf') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
							<i class="fab-icon-open icon-plus3"></i>
							<i class="fab-icon-close icon-cross2"></i>
						</a>
					</li>
				</ul>
				@endif
				
			<!-- Main sidebar -->
			@include('includes.left_sidebar')
			<!-- /main sidebar -->
		<!-- Main content -->
				<div id="post_section" class="content-wrapper">
				
				{{-- @php
					dd($query);
				@endphp --}}

				 @foreach($query as $q)
						<div style="display:none">{{ $json=$q->jsondata  }} </div>
						@php

						$json = json_decode($json,true);
						$writer=$json['writer'];
						$title=$json['title'];
						$link=$json['link'];
						$type=$json['type'];
					@endphp

						<!-- Blog layout #1 with image -->
						<div class="panel panel-flat blog-horizontal blog-horizontal-1">
							<div class="panel-heading" style="padding:12px 0px 5px 4px !important;">
								<div class="row">
								<div class="col-md-2" >
									<img src="{{url::asset('img/reunion.png')}}" style="height:45px; float:right;">
								</div>
								<div class="col-md-10" style="margin-left:-5px;">
								
								<h5 class="panel-title text-semibold" style="font-size:15px;"><b>Useful Stuff</b>
									<p style="font-size:12px;">By &nbsp; : &nbsp; {{ $writer }}</p>
									
								</h5>
								</div>
							
							</div>
							</div>
							<hr style="margin:0px;">
							<div class="row panel-body" style="padding:12px 20px;">
								<div class="col-md-4 thumb" style="width:100px;">
									<img style="width:100px; height:auto;" src="/img/pdf1.png" class="" alt="">
									
								</div>

								<div class="col-md-8 blog-preview">
									<h5 style="font-size:15px;"><b>@php  echo $json['title']; @endphp
										</b>
										<br>
										</h5>
										
									<p><a href="{{$link}}" style="border-color: #5c63b0; color: #5c63b0; padding:5px 12px 5px 12px;"  class="btn border-slate btn-flat legitRipple" target="_blank">View Pdf</a></p>
									
									
								</div>
							</div>

							<div class="panel-footer panel-footer-transparent">
								<div class="heading-elements">
									<ul class="list-inline list-inline-separate heading-text text-muted">
										
										
										
									
										<li><a class="comment" id="comment{{ $q->id }}" href="" class="text-muted">{{$q->comment}} comments</a></li>
										{{--  <li><a href="#" class="text-muted"><i class="icon-heart6 text-size-base text-pink position-left"></i></a></li>  --}}
										{{--  @permission('send_pdf_notification')  --}}
										{{--  @if($q->notification=="sent")
										<li><a data-toggle="modal" data-target="#notification{{ $q->id }}" class="text-default"><i class="icon-bell-check text-pink position-left"></i></a></li>
										@else
										<li><a href="{{ url('send_pdf_notification/'.$q->id) }}" class="text-default"><i class="icon-bell2 text-pink position-left"></i></a></li>
										@endif  --}}
										{{--  @endpermission  --}}
									

										{{--  @permission('edit_pdf')  --}}
										<li><a data-toggle="modal" data-target="#edit{{ $q->id }}" href="#" class="text-muted"><i class="icon-pencil text-size-base text-pink position-left"></i></a></li>
										 {{--  @endpermission  --}}


									     {{--  @permission('delete_pdf')  --}}
									     <li><a data-toggle="modal" data-target="#modal_mini{{ $q->id }}" href="#" class="text-muted"><i class="icon-trash text-size-base text-pink position-left"></i></a></li>
									       {{--  @endpermission   --}}
										</ul>
										
										
								</div>
								@if (isset($q->comments))
				                   <div style="background:#edeaea; padding: 10px; margin-bottom: -11px; display: none;" class="comment-box col-md-12 comment{{ $q->id }}">
				                   @php
				                   	$count=0;
				                   	$comment_count=count($q->comments);
				                   @endphp
				                    @foreach ($q->comments as $comment)
				                    	@php
				                    	$timestamp1=$comment->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($comment->jsondata,true); 
				                    	@endphp	
				                    <div class=" col-md-12" style="padding: 0px; margin-bottom: 20px;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    	@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="35" height="35"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="35" height="35"></div>
				                    	@endif
				                    		
				                    		<div class="col-md-6"><span style="font-weight: bold;">{{ $com['name'] }}</span><br>
				                    		<span style="margin-top: 5px;">{{ $com['collage'] }}</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 10px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
			                    		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			                    			<p>{{ $com['comments'] }}</p>
			                    			@if ($com['com_image']!="null")
			                    			<img src="/mechanicalinsider/{{$com['com_image']}}" width="150" height="100">
			                    			@endif
			                    		</div>
			                    		@if (isset($comment->reply))
			                    		@php
			                    		$reply_count=count($comment->reply);
			                    		@endphp
			                    		<div id="comment_{{ $comment->id }}" class="reply_section_default" style="padding-left:60px;"">
				                    		<p class="pull-left"><a id="show_reply">{{ $reply_count }} replies</a>
				                    		<a style="display: none;" id="hide_reply">Hide {{ $reply_count }} replies</a></p>
				                    	
			                    		@foreach ($comment->reply as $reply)
			                    		@php
				                    	$timestamp1=$reply->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($reply->jsondata,true);
				                    		// print_r($reply); 
				                    	@endphp
				                    		
			                    			<div class="col-md-12 reply_section" style="; margin-bottom: 20px; display:none;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    	@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="25" height="25"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="25" height="25"></div>
				                    	@endif
				                    		
				                    		<div class="col-md-6"><span style="font-weight: bold; font-size: 11px;">{{ $com['name'] }}</span><br>
				                    		<span style=" font-size: 11px;" >{{ $com['collage'] }}</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 10px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
			                    		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			                    			<p>{{ $com['comment'] }}</p>
			                    			@if ($com['com_image']!="null")
			                    			<img src="/mechanicalinsider/{{$com['com_image']}}" width="100" height="50">
			                    			@endif
			                    		</div>
			                    		<div class="col-md-10" style="margin-left: -10px;">
											{{--  @permission('delete_comment')  --}}
											<a><span alt="{{ $reply->commentid }}" id='{{ $reply->id }}' class='delete_reply'>Delete</span></a>
											{{--  @endpermission  --}}
										</div>
									</div>
										@endforeach
										</div>
										@endif
										<div class="col-md-10" style="margin-left: -10px;">
											{{--  @permission('delete_comment')  --}}
											<a><span alt='{{ $q->id }}' id='{{ $comment->id }}' class='delete_comment'>Delete</span></a>
											{{--  @endpermission  --}}
										</div>
									</div>
				                    @php
				                    $count++;
				                    if($count==5&&$comment_count>$count)
				                    {
				                    	echo "<a><span alt='$q->field_type' id='$q->id' class='view_more_comment'>view more comments</span></a>";				          
				                    	break;
				                    }
				                    @endphp

				                    @endforeach
				                    </div>
				                    @endif
								
								{{-- notificaion model --}}
										<div  data-backdrop="false" id="notification{{ $q->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header bg-warning">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Warning</h5>
										</div>

										<div class="modal-body">
											<p style="font-size: 14px;">This post is already notifyed.
												If you want again then click Yes.</p>

										</div>

										<div class="modal-footer">
											
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<a href="{{  url('send_pdf_notification/'.$q->id) }}"><button type="button" class="btn btn-primary bg-warning">Yes</button></a>
											</form>
										</div>
									</div>
									
								</div>
							</div>
									{{-- notificaion model --}}
							<div  data-backdrop="false" id="modal_mini{{ $q->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Pdf Data</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this Pdf.</p>

										</div>

										<div class="modal-footer">
											<form action="{{ url('delete_pdf') }}" method="post">
											{{ csrf_field() }}
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<input type="hidden" id="current_id" name="del_id" value="{{ $q->id }}">
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										</div>
									</div>
								</div>
							</div>
							
							
							
							<div  data-backdrop="false" id="edit{{ $q->id }}" class="modal fade">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Edit Pdf Data</h5>
										</div>

										<div class="modal-body">
						<form id="login_form" action="{{ url('pdf_update') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" value="{{$q->id}}" name="id">
						<input type="hidden" value="{{$link}}" name="link_existing">
							<div class="panel panel-flat">
								<div class="panel-heading">
									
								</div>

								<div class="panel-body">

									<input type="hidden" id="groups_institute" name="type_institute" value="college" />
										{{--  <div class="form-group">
											<label class="col-lg-2 control-label">Select:</label>
											<div class="col-lg-9">
												<label class="radio-inline">
													<input type="radio" class="styled" id="groups_institute" name="type_institute" value="group" @if($q->groupid !=0) checked @endif>
													Groups
												</label>
													
												<label class="radio-inline">
													<input type="radio" class="styled" id="college_institute" name="type_institute" value="college" @if($q->groupid ==0) checked @endif>
													College
												</label>
											</div>
										</div>  --}}
										@if($q->groupid == 0)
										<script>
												$(document).ready(function(){
															$("#college_select").show();
															$("#stream_select").show();
															$("#course_select").show();
															$("#year_select").show();
															$("#groups_select").hide();
													$("#groups_institute").click(function(){
															$("#college_select").hide();
															$("#stream_select").hide();
															$("#course_select").hide();
															$("#year_select").hide();	
															$("#groups_select").show();
													});
													$("#college_institute").click(function(){
															$("#college_select").show();
															$("#stream_select").show();
															$("#course_select").show();
															$("#year_select").show();
															$("#groups_select").hide();
													});
													
												});
										</script>
										@else
											<script>
												$(document).ready(function(){
													$("#college_select").hide();
													$("#stream_select").hide();
													$("#course_select").hide();
													$("#year_select").hide();
													$("#groups_select").show();
													$("#groups_institute").click(function(){
															$("#college_select").hide();
															$("#stream_select").hide();
															$("#course_select").hide();
															$("#year_select").hide();	
															$("#groups_select").show();
													});
													$("#college_institute").click(function(){
															$("#college_select").show();
															$("#stream_select").show();
															$("#course_select").show();
															$("#year_select").show();
															$("#groups_select").hide();
													});
													
												});
											</script>
										@endif
										
										<div class="form-group" id="course_select">
											<label class="col-lg-2 control-label">Select Course:</label>											
											<div class="col-lg-10 multi-select-full">
												@php $groups=explode(",", $q->groupid); @endphp
												<select name="groupid[]" data-placeholder="Select Courses..." multiple="multiple" class="multiselect-select-all-filtering" required autofocus>
													@if($new_Courses)
														@foreach ($new_Courses as $c)
															
															@if($q->groupid=="all")	
																<option value="{{ $c->id }}" selected >{{ $c->name }}</option>
															@elseif (in_array($c->id, $groups))
																<option value="{{ $c->id }}" selected >{{ $c->name }}</option>
															@else
																<option value="{{ $c->id }}">{{ $c->name }}</option>
															@endif

														@endforeach
													@endif
												</select>
												<input type="hidden" value="{{route('get_course')}}" name="urlcourse" class="urlcourse"/>
											</div>
										</div>

										{{--  <div class="form-group" id="groups_select">
											<label class=" control-label">Select Groups:</label>
											<div class=" multi-select-full">
											@php
												$groups=explode(",", $q->groupid);
												@endphp
													<select name="groupid1[]" data-placeholder="Select groups..." multiple="multiple" class="multiselect-select-all-filtering">
													@if($data[0]!="")
													@foreach ($data as $d)
													@if($d)
														<option value="{{ $d->id }}" @if($q->groupid=="all")
												selected 
												@elseif (in_array($d->id, $groups))
													selected 
												@endif>{{ $d->topic }}</option>
												@endif
													@endforeach
													@endif
													
													</select>
												</div>
										</div>  --}}

										{{--  <div class="form-group" id="college_select" style="display: none;">
											<label class="col-lg-2 control-label">Select College:</label>
											<div class="col-lg-10 multi-select-full">
												<select name="college" data-placeholder="Select College..." id="collagedropdown" class="college-selected form-control" required>
												<option value="">Select College </option>
													@if(isset($q->college))
														@if(isset($q->college_slected))
															@foreach($q->college_slected[0] as $coll_ege)
																<option value="{{$coll_ege->id}}" @if($q->college==$coll_ege->id) selected @endif>{{$coll_ege->college_name}}</option>
															@endforeach
														@endif
													@endif
												</select>
												 <input type="hidden" value="{{ route('get_stream') }}" name="urlcollege" class="urlcollege"/> 
											</div>
										</div>  --}}
													
										{{--  <div class="form-group" id="stream_select" style="display: none;">
											<label class="col-lg-2 control-label">Select Stream:</label>
											<div class="col-lg-10 multi-select-full">
												<select name="stream" data-placeholder="Select Stream..." class="stream-selected form-control" required>
													<option value="">Select Stream</option>
													@if(isset($q->stream))
														@if(isset($q->allstream))
															@foreach($q->allstream[0] as $stre_am)
																<option value="{{$stre_am}}" @if($q->stream==$stre_am) selected @endif>{{$stre_am}}</option>
															@endforeach
														@endif
													@endif
												</select>
											</div>
										</div>  --}}

										{{--  <div class="form-group" id="year_select" style="display: none;">
											<label class="col-lg-2 control-label">Select Year:</label>
											<div class="col-lg-10 multi-select-full">
												<select name="year" data-placeholder="Select Year..."  class="year-selected form-control" required>
													<option value=""></option>
													@if(isset($q->year))
														@if(isset($q->year_selected))
															@for($i = 1; $i <= $q->year_selected;$i++)
																<option value="{{$i}}" @if($q->year==$i) selected @endif>{{$i}}</option>
															@endfor
														@endif
													@endif
												</select>
											</div>
										</div>  --}}

									
									
										<div class="form-group">
											<label class="control-label">Writer:</label>
											<div class="">
												<input type="text" name="writer" class="form-control" value="{{$writer}}" placeholder="Enter name" required autofocus> 
											</div>
										</div>

										<div class="form-group">
											<label class=" control-label">Title:</label>
											<div class="">
												<input type="text" name="title" class="form-control" value="{{$title}}" placeholder="Enter Title">
											</div>
										</div>
										
										<div class="form-group" id="mySelect">
											<label class="display-block text-semibold">Select Type</label>
											<div class="">
									
										
											
											@if($type=="pdf")
										
										
												<label class="radio-inline">
													<input type="radio" name="type" value="link" onclick="change_div(10)">
													Link
												</label>
												<label class="radio-inline">
													<input type="radio"  name="type" value="pdf" onclick="change_div(20)" checked>
													Pdf
												</label>
												
											
											@else
											
												<label class="radio-inline">
													<input type="radio" name="type" value="link" onclick="change_div(10)" checked>
													Link
												</label>
												<label class="radio-inline">
													<input type="radio"   name="type" onclick="change_div(20)"  value="pdf">
													Pdf
												</label>
												
											
										@endif
											
											
											</div>
										</div>
										
										
										@if($type=="pdf")
										
											<div class="form-group link"  style="display:none;">
											<label class=" control-label">Enter Link :</label>
											<div class="">
												<input id="link" type="text" class="form-control"  placeholder="Enter Link" name="link">
											</div>
										</div>
										
										@else
											<div class="form-group link">
											<label class=" control-label">Enter Link :</label>
											<div class="">
												<input id="link" type="text" class="form-control" value="{{$link}}" placeholder="Enter Link" name="link">
											</div>
										</div>
										
										
										@endif
										
											
										
								
								
										@if($type=="pdf")
										
											<div class="form-group pdf">
										@else
										
											<div class="form-group pdf"   style="display:none;" >
										
										@endif
											<label class="control-label">Enter Pdf:</label>
											<div class="">
												<input type="file" class="form-control" name="pdf">
											</div>
										</div>
										
									
								</div>
								<fieldset>
											<legend></legend>
											{{-- <div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to like this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_like" type="checkbox" class="switchery" @if(isset($json['add_like'])) @if($json['add_like']=="off") checked @endif @endif value="off">
													</label>
												</div>
											</div> --}}
											<div class="form-group">
												<div class="col-lg-4">
													<label>Restrict users to comment this post?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="add_comment" type="checkbox" class="switchery" @if(isset($json['add_comment'])) @if($json['add_comment']=="off") checked @endif @endif value="off">
													</label>
												</div>
											</div>
										</fieldset>
							</div>
						

										</div>

										<div class="modal-footer">
										
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
									
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										</div>
									</div>
								</div>
							</div>
							
							
							
							
								
							</div>
			
						<!-- /blog layout #1 with image -->

					</div>
		
				        
				        
				 @endforeach
	
				
				 
				
				    <!-- /layout 1 -->
				</div>
        <!-- /main content -->
		<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				title:{
					required:true
				},
				type:{
					required:true
				},
				writer:{
					required:true
				},
				link:{
					required:"#pdf:blank"
				},
				
				groupid:{
					required:true
				}
			}
		});

	});
</script>

        
        <script type="text/javascript">
		function change_div(val)
		{
			if(val==10)
			{
				
				$(".link").show();
			    $(".pdf").hide();
			}
			else
			{
		
				$(".link").hide();
			    $(".pdf").show();
			}
		}
	</script>
	<script>
				    	$(document).ready(function(){
				    		var limit=5;
				    		var postid=$("#postid").val();
				    		var field_type=$("#field_type").val();

				    		 
				    		$(document).on('click','.comment',function(e){
				    			var id=$(this).attr('id');
				    			$("div."+id).slideToggle();
				    			e.preventDefault();
				    		});
				    		$(document).on('click','#show_reply',function(e){
				    			$(this).parents('div').children('div.reply_section').show();
				    			$(this).siblings("a").show();
				    			$(this).hide();
				    			e.preventDefault();
				    		});
				    		$(document).on('click','#hide_reply',function(e){
				    			$(this).parents('div').children('div.reply_section').hide();
				    			$(this).siblings("a").show();
				    			$(this).hide();
				    			e.preventDefault();
				    		});
				    		$(document).on('click','.view_more_comment',function(){
				    			var postid=$(this).attr('id');
				    			// alert(postid);
				    			var id=$(this).attr('id');
				    			// var field_type=$(this).attr('alt');
				    			// alert(field_type);
				    			limit=limit+5;
				    			var path = {!! json_encode(url('/')) !!}
				    			$.get(""+path+"/ajax2",{
				    				postid:postid,
				    				limit:limit },
				    				function(data,success){
				    					$(".comment"+id).html(data);
				    				
				    			});
				    		});
				    		$(document).on('click','.delete_reply',function(){
				    			var commentid=$(this).attr('alt');
				    			var id=$(this).attr('id');
				    			var path = {!! json_encode(url('/')) !!}
				    			$.get(""+path+"/delete_reply",{
				    				id:id,
				    				commentid:commentid },
				    				function(data,success){
				    					$("#"+id).parents('div.reply_section_default').html(data);
				    				
				    			});
				    		});
				    		$(document).on('click','.delete_comment',function(){
				    			var postid=$(this).attr('alt');
				    			var commentid=$(this).attr('id');
				    			var path = {!! json_encode(url('/')) !!}
				    			$.get(""+path+"/delete_comment",{
				    				postid:postid,
				    				commentid:commentid,limit:limit },
				    				function(data,success){
				    					$(".comment"+postid).html(data);
				    				
				    			});
				    		});
				    	});
				    </script>
<!-- Secondary sidebar -->
			@include('includes.right_sidebar')
			<!-- /secondary sidebar -->
			

		</div>
		<!-- /page content -->
			
@endsection
@section('js')
<script>
	$(document).ready(function(){
		$(document).on('keyup',"#searchbar",function(){
			var val=$("#searchbar").val();
			var path = {!! json_encode(url('/')) !!}
		      $.ajax({
		          url:""+path+"/pdf_search",
		          type:"POST",
		          data:{ "_token": "{{ csrf_token() }}","val":val},
		          success:function(data){
		            $("#post_section").html(data);
		          }
		      });

		});
	});
</script>
{{-- for switch buttons --}}
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	{{-- for switch buttons --}}
@endsection
