@extends('layouts.main')
@section('js_head')
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>
@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				@if(Auth::user()->role == 'admin' )
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('pdf') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
				@endif

					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View PDF</h3>
						<div class="row" style="padding-top:50px;">
							<div class="col-md-12">
								<form action="" method="GET">
									<div class="col-md-4">
										<select id="course" name="course" data-placeholder="Select Course..." class="select select-border-color border-warning" >
											<option value="">Select Course...</option>
											@foreach ($new_Courses as $c)
											<option @if($course_ids == $c->id) selected @endif  value="{{ $c->id }}">{{ $c->name }} </option>
											@endforeach
										</select> 
									</div>

									<div class="col-md-4">
										<select id="subject" name="subject" data-placeholder="Select Subject..." class="select select-border-color border-warning" >
											<option value="">Select Subject...</option>
											@foreach ($subject as $s)
											<option @if($subject_id == $s->id) selected @endif value="{{ $s->id }}">{{ $s->name }}</option>
											@endforeach
										</select> 
									</div>

									<div class="col-md-4">
										<div class="col-md-6"> 
											<label>	<input type="search" class="form-control" name="filter" value="{{ $filter }}" placeholder="Type to filter..."></label>
										</div>
										<div class="col-md-6">
											<input type="submit" class="fab-menu-btn btn bg-success" value="search"> 											
										</div>	
									</div>

								</form>						
							</div>
						</div>						
					</div>
				

					<table class="table">
						<thead>
							<tr>			
                                <th>Course</th>
                                <th>Subject</th>
                                <th>Name</th>
								<th>PDF</th>
								<th>Access</th>
								<th>Status</th>   
                                <th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if(count($data)>0)	
						@foreach($data as $u)
							<tr>
                                <td class="table-width">
									<div class="limited-text" id="row{{ $u->id }}">
										{{ $u->course_name }}
									</div>	
									@if(strlen($u->course_name) > 40) 	
										<a href="javascript:;" id="more{{ $u->id }}" onClick="view_more({{ $u->id }})"> More</a>
										<a href="javascript:;" class="hide" id="less{{ $u->id }}"  onClick="view_less({{ $u->id }})"> Less</a>
									@endif
								</td>
                                <td>{{ $u->subject_name }}</td>
                                <td class="table-width">{{ $u->pdf_name }} </td>    
                                <td> <a href="{{$u->pdf_link   }}" class="fab-menu-btn btn-pdf  bg-red   btn-rounded " target="_blank" > <i class="fa fa-file-pdf-o"></i> View PDF  </a>  </td> 
								
								<td>
                                    <span class="fab-menu-btn btn-sm @if($u->access == 'public') bg-info  @else bg-warning @endif btn-rounded "> {{ ucfirst($u->access) }} </span>  
								</td>  

								<td>
                                    <span class="fab-menu-btn btn-sm @if($u->status == 'notpublished') bg-red @endif  bg-success btn-rounded "> @if($u->status == 'published') Published @else Unpublish  @endif </span>  
								</td>  
								
								<td class="text-center">										
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
											<ul class="dropdown-menu dropdown-menu-right" >
												
												@if(Auth::user()->role == 'admin' )
													<li><a href="javascript:;"  onClick="fun_delete({{ $u->id }})"><i class="icon-trash"></i> Delete</a></li>
													<li><a href="{{route('edit_pdf',encrypt ($u->id))}}"><i class="icon-pencil"></i> Edit</a></li>
													<li><a href="{{route('get_comment',encrypt ($u->id))}}"><i class="icon-comment"></i> Comment</a></li>

													@if($u->status == 'published')
														<li><a href="javascript:;" onClick="fun_publish({{ $u->id }},'notpublished')" ><i class="icon-lock"></i> Unpublish</a></li>
													@else
														<li><a href="javascript:;" onClick="fun_publish({{ $u->id }},'published')" ><i class="icon-lock"></i> Publish</a></li>
													@endif													
												
												@endif

											</ul>
										</li>
									</ul>									
								</td> 								
                                
                          	</tr>				
						@endforeach	
						@else
						<tr>
                                <td colspan="6"><center>No record</center>
								</td>
						</tr>
						@endif							
						</tbody>
					</table>
				</div>
				
                {{ $data->links() }}
			</div>
		</div>

        <div  data-backdrop="false" id="modal_mini" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Click on Yes if you want to delete this Record.</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('delete_pdf') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del_id" name="del_id" value="">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div  data-backdrop="false" id="modal_2" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Are you want want to change this record?</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('publish_testSeries') }}" method="post">	

						{{ csrf_field() }}
							<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del" name="del" value="">
							<input type="hidden" id="type" name="type" value="">
							<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		

@endsection
@section('js')

	<script type="text/javascript">

		function fun_delete(id)
		{	
			var val = [];
			val.push(id);
			$('#del_id').val(val);
			$('#modal_mini').modal('toggle');		
		}

		function fun_publish(id,type)
		{
			$('#del').val(id);
			$('#type').val(type);
			$('#modal_2').modal('toggle');	
		}

		

	</script>
@endsection

