@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
				@if(Auth::user()->role == 'admin' )
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_video') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
				@endif

					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View Video</h3>
                    </div>
                    
					<table class="table">
						<thead>
							<tr>
                                <th>User Name</th>
                                <th>Mobile</th>
                                <th>Video Name</th>
                                <th>Comment</th>
                                <th>Action</th>
							</tr>
						</thead>
						<tbody>
						@foreach($data as $u)
							<tr>								
                                <td>{{$u->username}}</td>
                                <td>{{$u->usermobile}}</td> 
								<td>{{ $u->video_name }}</td>
                                <td>{{ $u->comment }}</td>
								<td class="text-center">										
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>
											<ul class="dropdown-menu dropdown-menu-right" >												
												@if(Auth::user()->role == 'admin' )
													<li><a href="javascript:;"  onClick="fun_delete({{ $u->id }})"><i class="icon-trash"></i> Delete</a></li>
													<li><a href="{{route('add_reply_post',['id'=>encrypt ($u->id),'type'=>'video'])}}" ><i class="icon-comment"></i> Reply</a></li>

												@endif
											</ul>
										</li>
									</ul>									
								</td>
                          	</tr>				
						@endforeach								
						</tbody>
					</table>
                </div>
                
			</div>
		</div>

		<div  data-backdrop="false" id="modal_mini" class="modal fade">
			<div class="modal-dialog modal-xs">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title ">Are you sure?</h5>
					</div>
					<div class="modal-body">
						<p id="heading">Click on Yes if you want to delete this Record.</p>
					</div>
					<div class="modal-footer">
						<form action="{{ route('delete_comment') }}" method="post">
						{{ csrf_field() }}
						<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
							<input type="hidden" id="del_id" name="del_id" value="">
						<button type="submit" class="btn btn-primary">Yes</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		
@endsection
@section('js')

	<script type="text/javascript">

		function fun_delete(id)
		{	
			var val = [];
			val.push(id);
			$('#del_id').val(val);
			$('#modal_mini').modal('toggle');		
		}

	</script>
@endsection

