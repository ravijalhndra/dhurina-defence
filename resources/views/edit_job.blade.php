@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>

	<!-- Theme JS files -->
<script>
	
	$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
			@if(!empty($all))
					@php
						
						$json=json_decode($all->jsondata,true);
						
						
					@endphp	
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form action="{{ url('update_job') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Job</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
								<div class="form-group">
										<label class="col-lg-3 control-label">Select:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" id="groups_institute" name="type_institute" value="group" @if($all->groupid != 0) checked @endif>
												Groups
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" id="college_institute" name="type_institute" value="college" @if($all->groupid == 0) checked @endif>
												College
											</label>
										</div>
									</div>
									@if($all->groupid != 0)
									<script>
											$(document).ready(function(){
												$("#college_select").hide();
												$("#stream_select").hide();
												$("#course_select").hide();
												$("#year_select").hide();
												$("#groups_institute").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();	
														$("#groups_select").show();
												});
												$("#college_institute").click(function(){
														$("#college_select").show();
														$("#stream_select").show();
														$("#course_select").show();
														$("#year_select").show();
														$("#groups_select").hide();
												});
												
											});
									</script>
									@else
									<script>
											$(document).ready(function(){
												$("#college_select").show();
												$("#stream_select").show();
												$("#course_select").show();
												$("#year_select").show();
												$("#groups_select").hide();
												$("#groups_institute").click(function(){
														$("#college_select").hide();
														$("#stream_select").hide();
														$("#course_select").hide();
														$("#year_select").hide();	
														$("#groups_select").show();
												});
												$("#college_institute").click(function(){
														$("#college_select").show();
														$("#stream_select").show();
														$("#course_select").show();
														$("#year_select").show();
														$("#groups_select").hide();
												});
												
											});
									</script>
									@endif
									
									<div class="form-group" id="course_select">
									<label class="col-lg-3 control-label">Select Course:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="course" data-placeholder="Select Course..." id="coursedropdown" class="course-selected form-control" required>
										<!-- multiple="multiple"   -->
											@if($course)
												@foreach ($course as $cou_rse)
													<option value="{{ $cou_rse->id }}" @if($collegedata->courseid == $cou_rse->id) selected @endif>{{ $cou_rse->course_name }}</option>
												@endforeach
											@endif
										</select>
										<input type="hidden" value="{{route('get_course')}}" name="urlcourse" class="urlcourse"/>
									</div>
								</div>

								<div class="form-group" id="college_select">
									<label class="col-lg-3 control-label">Select College:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="college" data-placeholder="Select College..." id="collagedropdown" class="college-selected form-control" required>
										<option value="">Select College </option>
											@if($college_slected)
												@foreach ($college_slected as $colle_ge)
													<option value="{{ $colle_ge->id }}" @if($collegedata->college_id == $colle_ge->id) selected @endif>{{ $colle_ge->college_name }}</option>
												@endforeach
											@endif
										</select>
										{{--<!-- <input type="hidden" value="{{ route('get_stream') }}" name="urlcollege" class="urlcollege"/> -->--}}
									</div>
								</div>

								<div class="form-group" id="stream_select">
									<label class="col-lg-3 control-label">Select Stream:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="stream" data-placeholder="Select Stream..." class="stream-selected form-control" required>
											<option value="">Select Stream</option>
											@if($stream)
												@foreach ($stream as $str_eam)
													<option value="{{ $str_eam }}" @if($collegedata->stream == $str_eam) selected @endif>{{ $str_eam }}</option>
												@endforeach
											@endif
										</select>
									</div>
								</div>

								<div class="form-group" id="year_select">
									<label class="col-lg-3 control-label">Select Year:</label>
									<div class="col-lg-9 multi-select-full">
										<select name="year" data-placeholder="Select Year..."  class="year-selected form-control" required>
													<option value=""></option>
													@if($year)
														@for ($i = 1; $i<= $year; $i++)
															<option value="{{ $i }}" @if($collegedata->year == $i) selected @endif>{{ $i }}</option>
														@endfor
													@endif
										</select>
									</div>
								</div>
									
									<div class="form-group" id="groups_select">
										<label class="col-lg-3 control-label">Select Groups:</label>
										<div class="col-lg-9">
											
										</div>
										<div class="col-lg-9 multi-select-full">
										@php
											$groups=explode(",", $all->groupid);
											@endphp
												<select name="groupid[]" data-placeholder="Select groups..." multiple="multiple" class="multiselect-select-all-filtering">
												@if($data[0]!="")
												@foreach ($data as $d)
												@if($d)
													<option value="{{ $d->id }}" @if($all->groupid=="all")
											selected 
											@elseif (in_array($d->id, $groups))
												selected 
											@endif>{{ $d->topic }}</option>
												@endif
												@endforeach
												@endif
												
												</select>
											</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Title:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="title" value="{{$json['title']}}">
											<input type="hidden" name="old_image" value="{{ $json['image'] }}">
											<input type="hidden" name="id" value="{{ $all->id }}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Post Name:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="postname" value="{{$json['postname']}}">
											
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">No. of Posts:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="post" value="{{$json['post']}}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Last Date:</label>
										<div class="col-lg-9">
											<div class="content-group-lg">
									<div class="input-group">
										<span class="input-group-addon"></span>
											
										<input type="text" class="form-control pickadate-accessibility" id="txtstartdate" name="lastdate" placeholder="Try me&hellip;" value="{{ $json['lastdate'] }}">
									</div>
								</div>
										</div>
									</div>

									{{-- <div class="form-group">
										<label class="col-lg-3 control-label">Price:</label>
										<div id="price" class="col-lg-9">
										<div class="col-md-6">
											<input type="radio" name="payment" value="free" @if ($json['fees']=='free')
											checked 
											@endif>Free
										</div>
										<div class="col-md-6">
											<input type="radio" name="payment" value="paid" @if ($json['fees']!="free")
											checked 
											@endif>Paid
										</div><br>
										<input style="display: none;" id="payment1" type="text" name="payment1" class="form-control" placeholder="Enter amount"
										@if ($json['fees']=="free")
											value=""
										@else
										value="{{ $json['fees'] }}" 
										@endif ">
										</div>
									</div>
									@if ($json['fees']!="free")
										<script>
											$(document).ready(function(){
												$("#payment1").show();
												});
										</script>
									@endif
									<script type="text/javascript">
										$('input:radio[name="payment"]').change(function(){
										    if($(this).val() == 'paid'){
										    	$("#payment1").show();
										    }
										    else
										    {
										    	$("#payment1").hide();
										    }
									});
									</script> --}}
									<div class="form-group">
										
											<label class="col-lg-2 control-label">Choose one:</label>
											<div id="price" class="col-lg-10">
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="choose" value="description" @if ($all->post_description!="")
												checked 
											@endif >
														Description
													</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="choose" value="url" @if ($json['website']!="")
												checked 
											@endif >
														Website
													</label>
												</div>
											</div>
											</div>
											</div>
											<div id="description1" @if ($all->post_description=="")
												style="display: none;"
											@endif  class="form-group">
											<label class="col-lg-2 control-label">Description:</label>
											<div class="col-lg-10">
												<textarea id="description"  name="description" class="summernote">{{ $all->post_description }}</textarea>
											</div>
										</div>
										<div id="url1" @if ($json['website']=="null")
												style="display: none;"
											@endif  class="form-group">
										<label class="col-lg-3 control-label">URL:</label>						
												<div class="col-lg-9">
											<input id="url" type="text" class="form-control" name="website" placeholder="www.xyz.com" value="{{ $json['website'] }}">
										</div>
									</div>
										
										<script type="text/javascript">
											jQuery(document).ready(function(){
												var choose_val=jQuery('input:radio[name="choose"]').val();
												if(choose_val == 'description'){
												    	jQuery("#description1").show();
												    	jQuery("#url").val("");
												    	jQuery("#url1").hide();
												    }
												    else if(choose_val == 'url'){
												    	jQuery("#description1").hide();
												    	jQuery("#url1").show();
												    	jQuery("#description").val('');

												    }
												    else
												    {
												    	jQuery("#description1").hide();
												    	jQuery("#url1").hide();
												    }

												jQuery('input:radio[name="choose"]').change(function(){
												    if(jQuery(this).val() == 'description'){
												    	jQuery("#description1").show();
												    	jQuery("#url").val("");
												    	jQuery("#url1").hide();
												    }
												    else if(jQuery(this).val() == 'url'){
												    	jQuery("#description1").hide();
												    	jQuery("#url1").show();
												    	jQuery("#description").val('');

												    }
												    else
												    {
												    	jQuery("#description1").hide();
												    	jQuery("#url1").hide();
												    }
												});
											});
										</script>
										<div class="form-group">
										<label class="col-lg-3 control-label"></label>
										<div class="col-lg-9">
											 <div class="main-img-preview">
											
							                <img class="thumbnail img-preview" src="../../../mechanicalinsider/jobsimage/{{ $json['image'] }}" title="Preview Logo" >
							              </div>
																		 <div class="input-group">
							                <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled" style="display:none">
							                <div class="input-group-btn">
							                  <div class="fileUpload btn btn-danger fake-shadow">
							                    <span><i class="glyphicon glyphicon-upload"></i> Upload Logo</span>
							                    <input id="logo-id" name="logo" type="file" class="attachment_upload" >
							                  </div>
							                </div>
							              </div>
										</div>
									</div>
									<div id="loading" style="display:none;"><img src="{{url::asset('img/EPINPUzG3GNIQ.gif')}}" alt="" /></div>

									<fieldset>
										<legend>Notificaton Image (optional)</legend>
										<div class="form-group" id="mySelect">
											<label class="col-lg-2 display-block text-semibold">Add Job Icon</label>
											<div class="col-lg-10">
											<label class="radio-inline">
												<input type="radio" name="type" value="link" class="styled" checked="checked">
												Link
											</label>
											<label class="radio-inline">
												<input type="radio" name="type" value="image" class="styled">
												Image
											</label>
											</div>
										</div>
										
										<div class="form-group" id="link">
											<label class="col-lg-2 control-label">Enter Link :</label>
											<div class="col-lg-10">
												<input id="link" type="text" class="form-control" placeholder="Enter Link" name="link" value="@if(isset($json['icon'])){{$json['icon']}} @endif"">
											</div>
										</div>
										
										<div class="form-group" style="display:none;" id="pdf">
											<label class="col-lg-2 control-label">Enter Icon:</label>
											<div class="col-lg-10">
												<input id="pdf" type="file" class="form-control" name="icon">
											</div>
										</div>
										<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="type"]').change(function(){
												    if(jQuery(this).val() == 'link'){
												    	jQuery("#link").show();
												    	jQuery("#pdf").hide();
												    }
												    else
												    {
												    	jQuery("#link").hide();
												    	jQuery("#pdf").show();
												    }
												});
											});
										</script>
									</fieldset>

					<div class="text-right">
										<button type="submit" onclick="$('#loading').show();"   class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->
		@endif
	</div>
	<!-- /page container -->


	<script>
	$(document).ready(function(){
		$("#myForm").validate({

			rules:{
				title:{
					required:true
				},
				post:{
					required:true
				},
				postname:{
					required:true
				},
				website:{
					required:true
				},
				lastdate:{
					required:true
				},
				groupid:{
					required:true
				},
				logo:{
					required:true
				},
			}
		});

	});
</script>

@endsection