@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>

    <style>
        .block{
            padding-left:15px;
            padding-bottom:10px;
        }

    </style>
@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					
					
					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">Vimeo Video</h3>
					</div>
                
                    <div class="row">
                        @foreach ($response as $resp)
                            @php 
                               $check=$resp['pictures']['sizes'];
                               $get=end($check);
                               $img=$get['link_with_play_button'];

                               $video_url=explode('/',$resp['uri']);
                               $video_id=end($video_url);
                            @endphp
                            <div class="col-md-3 block">
                                <a href="{{ $img }}" target="_blank"><img  src="{{ $img }}" width="250" height="150"/></a>  </br>
                                <h6 class="color-black">{{ $resp['name'] }}</h6> </br>
                                <a href="{{route('assign_video',encrypt($video_id))}}"> <i class="icon-pencil"></i> Assign Video </a> </br>

                            </div>
                        @endforeach
                    </div>

				</div>
			</div>
		</div>


@endsection
@section('js')

	<script type="text/javascript">

		function fun_delete(id)
		{
			$('#del_id').val(id);
			$('#modal_mini').modal('toggle');		
		}

		function fun_publish(id,type)
		{
			$('#del').val(id);
			$('#type').val(type);
			$('#modal_2').modal('toggle');	
		}

	</script>
@endsection

