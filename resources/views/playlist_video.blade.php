@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">Video Playlist</h3>
						<div class="row" style="padding-top:50px;">
							
							<div class="col-md-12" >
                                </br>

                                <form id="login_form" action="{{ route('save_playlist') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <input type="hidden" name="course_id" id="multi_delete" value="{{ $course_id }}" />
                                    <input type="hidden" name="subject_id[]" id="multi_delete" value="{{ $subject_id }}" />
                                    <input type="hidden" name="arrange[]" id="multi_delete" value="" />	
                                    <div>
                                        <button type="submit" class="btn btn-primary">Save Playlist <i class="icon-arrow-right14 position-right"></i></button>
                                    </div><br>
                                </form>
                            </div>
                            
						</div>
					</div>
				

					<table class="table">
						<thead>
							<tr>
                                <th>ID</th>
                                <th>Course</th>
                                <th>Subject</th>
                                <th>Name</th>
                                <th>Video</th>
							</tr>
						</thead>
						<tbody>
						@foreach($data as $u)
							<tr>
								<td><input type="checkbox" class="check_delete" name="delete_id" value="{{ $u->id }}" /> &nbsp; </td>								
								<td class="table-width">
									<div class="limited-text" id="row{{ $u->id }}">
										{{ $u->course_name }}
									</div>	
									@if(strlen($u->course_name) > 40) 	
									<a href="javascript:;" id="more{{ $u->id }}" onClick="view_more({{ $u->id }})"> More</a>
									<a href="javascript:;" class="hide" id="less{{ $u->id }}"  onClick="view_less({{ $u->id }})"> Less</a>
									@endif										
								</td>
                                <td>{{$u->subject_name}}</td>
                                <td>{{$u->name}}</td>    
                                <td>
									<a href="#" target="_blank"> <img src="{{$u->thumbnail}}" width="80" height="80"  </a>
								</td>        								
                                
                          	</tr>				
						@endforeach								
						</tbody>
					</table>
                </div>

                {{ $data->links() }}				

			</div>
		</div>

@endsection
@section('js')

    <script type="text/javascript">

        $('input[name=delete_id]').change(function(){
            
            var val = [];
            $(':checkbox:checked').each(function(i){
                val[i] = $(this).val();
            });
            
            $('#multi_delete').val(val);

        });

    </script>
@endsection

