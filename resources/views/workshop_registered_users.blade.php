@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
			{{-- table --}}
			<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Registered Users {{-- <a href="{{ url('/export_csv/'.$id) }}" style="margin-right: 100px; margin-left: 10px;" class="pull-right btn btn-primary">Export CSV</a><a href="{{ url('/create_pdf/'.$id) }}" class="pull-right btn btn-success">Download PDF</a> --}}</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					<div class="panel-body">
						
					</div>
						@php
							$data=json_decode($data->jsondata,true);
							$count=1;
						@endphp
					<table class="table datatable-basic table-bordered">
						<thead>
							<tr>
								<th>S.NO.</th>
								<th>User Id</th>
								@foreach ($data['field'] as $fields)
									@foreach ($fields as $field)
									<th>{{ $field }}</th>
									@endforeach
								@endforeach
								<th>Status</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>

						@foreach ($users as $user)
						@php
							$json=json_decode($user->jsondata,true);
						@endphp
							<tr>
								<td>{{ $count }}</td>
								<td>{{ $user->uid }}</td>
								@foreach ($json as $jj)
								@php
									// dd($jj);
								@endphp
								<td>{{ $jj['value'] }}</td>
								
								@endforeach
								<td>@if($user->status=='false') <span class="label label-warning">Pending</span> @else <span class="label label-success">Success</span> @endif</td>
								<td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												<li><a href="{{ url('/create_pdf/'.$id) }}"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
												<li><a href="{{ url('/export_csv/'.$id) }}"><i class="icon-file-excel"></i> Export to .csv</a></li>
												
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							@php
							$count++;
							@endphp
						@endforeach
							
							
							
						</tbody>
					</table>
				</div>
			{{-- table --}}
					

			</div>
			<!-- /main content -->

		</div>
@endsection
