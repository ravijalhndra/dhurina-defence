@extends('layouts.main')
@section('js_head')


	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
@include('mapjs')

	
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				topic:{
					required:true
				},
				city:{
					required:true
				},
				status:{
					required:true
				},
				logo:{
					required:true
				},
				payment:{
					required:true
				},
				payment1:{
					number:true
				}
				
			}
		});
	});
</script>
<script>
	
	$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



</script>


		
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{route('update_challenge_level')}}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<input type="hidden" name="id" id="clevel_id" value="{{$user->id}}">
							<input type="hidden" value="{{ csrf_token() }}" name="_token">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Challenge Level</h5>
								</div>
							<div class="panel-body">
							<div class="form-group">
								<label class="col-lg-3 control-label">LeveL Name</label>
								<div class="col-lg-9">
									<input type="text" class="form-control" id="level" placeholder="Enter level name" value="{{$user->level}}" readonly="" name="level"  required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Point</label>
								<div class="col-lg-9">
									<input type="text" class="form-control" id="point" value="{{$user->point}}" placeholder="Enter points" name="point" required  onkeyup="edit_clevel()">
								    <span id="errorname" style="color:red">
								</div>
							</div>
						   <div class="form-group">
								<label class="col-lg-3 control-label">Name</label>
								<div class="col-lg-9">
									<input type="text" class="form-control cname" value="{{$user->name}}" placeholder="Enter name" name="name"  required>
								</div>
							</div>
							<script type="text/javascript">
								jQuery(document).ready(function(){
									jQuery('input:radio[name="payment"]').change(function(){
									    if(jQuery(this).val() == 'paid'){
									    	jQuery("#payment1").show();
									    }
									    else
									    {
									    	jQuery("#payment1").hide();
									    }
									});
								});
							</script>
						    </div>
							<br/>
							<br/>
							<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
							<div class="text-right">
								<button type="submit" id="update_challenge_level" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Update Challenge Level <i class="icon-arrow-right14 position-right"></i></button>
							</div><br>
						    </div>
						</div>
						</form>
						<!-- /basic layout -->
					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->
	<script>
function edit_clevel()
{
	var cid=$('#clevel_id').val();
	
	var point=$('#point').val();
	var token = $("input[name='_token']").val();
	$.ajax({

          url: '{{route('get_clevel_data')}}',
          method: 'POST',
           data:{id:cid,_token:token},
          success: function(data) { 

		  var pon_level1=data['current_level']['point'];
          var pon_level2=data['next_level'];
        
          if(point < pon_level1) 
          {
          document.getElementById('errorname').innerHTML="Enter valid value"; 
          $( "#update_challenge_level" ).prop( "disabled", true );
		 }
		 else if(pon_level2 != null)
		 {
		 	if(point > pon_level2['point'])
		 	{
		 		document.getElementById('errorname').innerHTML="Enter valid value"; 
          $( "#update_challenge_level" ).prop( "disabled", true );
		 	}
		 	else
		 	{
		 		document.getElementById('errorname').innerHTML=""; 
         $( "#update_challenge_level" ).prop( "disabled", false );

		 	}
		 }
		  else
         {
         document.getElementById('errorname').innerHTML=""; 
         $( "#update_challenge_level" ).prop( "disabled", false );
         }
     
		},
          
          error: function() {}
          
     
  });
}
</script>
@endsection
@section('js')
