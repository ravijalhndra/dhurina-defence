@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					@permission('add_admin')
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_college') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
					@endpermission
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">View College</h3>
					</div>


					<table id="datatable" class="table datatable-basic">
						<thead>
							<tr>
                                <th>ID</th>
                                <th>Course Name</th>
								<th>College Name</th>
								<th>Registration No</th>
								<th>Stream</th>
								<th>Years</th>
								
							
								
								<th class="text-center">Actions</th>
								
							</tr>
						</thead>
						<tbody>
					  
						@foreach ( $college as $c)
						
								<tr>
								<td>{{ $c->id }}</td>
							
								<td>@foreach($course as $co) @if($c->course_id == $co->id) {{ $co->course_name }} @endif @endforeach</td>
								<td>{{ $c->college_name }}</td>
                                <td>{{ $c->reg_no }}</td>
                             @php $s=json_decode($c->stream,true);  @endphp
							
                               <td>@for($i=0; $i< sizeof($s); $i++)  {{ $s[$i] }}   @endfor</td>
                                <td>{{ $c->year }}</td>
                                <td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												
												<li><a data-toggle="modal" data-target="#modal_mini{{ $c->id }}" href="#"><i class="icon-trash"></i> Delete</a></li>
												

											
												<li><a href="{{ url('edit_college/'.$c->id) }}"><i class="icon-pencil"></i> Edit</a></li>
												
												
											</ul>
										</li>
									</ul>
								</td>
                                <td></td>
                                <div  data-backdrop="false" id="modal_mini{{ $c->id }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete College</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this college.</p>

										</div>

										<div class="modal-footer">
											
											<form action="{{ url('delete_college') }}" method="post">
											{{ csrf_field() }}
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<input type="hidden" name="del_id" value="{{ $c->id }}">
											<button type="submit" class="btn btn-primary">Yes</button>
											</form>
										
										</div>
									</div>
								</div>
							</div>
							
							</tr>
													@endforeach
							
							
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>
		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
       $(document).ready(function() {
    $('#datatable').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>
@endsection
