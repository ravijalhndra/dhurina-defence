@extends('layouts.main')
@section('js_head')

	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/tables/datatables/datatables.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/datatables_basic.js') }}></script>


@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					<!-- @permission('add_admin')
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('add_admin') }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-plus3"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
					@endpermission -->
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h3 class="panel-title">Leadership Board</h3>
					</div>

					<div class="row">
                    <div class="col-md-offset-3 col-md-6 col-md-offset-3">
                    <div class="col-md-6">
                        <input type="radio" name="type" onclick="show1();" requeired>
                        <label for="individual">Individual</label>
                    </div>
                    <div class="col-md-6">
                        <input type="radio" name="type" onclick="show2();" required>
                        <label for="individual" name="type">College</label>
                    </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-offset-3 col-md-6 col-md-offset-3 " id="div1" >
                            <div class="col-md-4">
                            <select class="form-control subject" id="time" onchange="ajaxfunction()">
                                <option value="">Choose</option>
                                <option value="today">Today</option>
                                <option value="week">Week</option>
                                <option value="month">This Month</option>
                                <option value="year">Academic Year</option>
                                </select>
                            </div>
                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            <input type="hidden" name="url" class="urlleadership" value="{{route('filter_leadership')}}" />
								
                              
                        
                           <div class="col-md-4">
                            <select name="subject" id="sub"  class='form-control' onchange="ajaxfunction()">
                                <option value="">Choose Subject</option>
                                 @foreach($subject as $s)
                                <option value="{{$s->sub_test_name}}" class="from-control sub">{{$s->sub_test_name}}</option>
                                @endforeach
                            </select>
                           </div>
                          
                    </div>
                    <div class="col-md-offset-3 col-md-6 col-md-offset-3" id="div2" style="display:none" onchange="ajax_second()">
                            <div class="col-md-4">
                            <select class="form-control" id="c_time">
                                <option value="">Choose</option>
                                <option value="today">Today</option>
                                <option value="week">Week</option>
                                <option value="month">This Month</option>
                                <option value="year">Academic Year</option>
                                </select>
                            </div>
                        
                        
                            <div class="col-md-4">
                            <select name="type" id="type" class="form-control" onchange="ajax_second()">
                                <option value="">Choose Stream</option>
                                @foreach($dept as $d)
                                <option value="{{$d->dept_name}}" >{{$d->dept_name}}</option>
                                @endforeach
                            </select>
                           </div>
                      </div>
                    </div>
              					<table id="example"  class="table table-striped table-bordered">
              						<thead>
              						<tr style="text-align:center">
                              <th  style="text-align:center">ID</th>
                              <th  style="text-align:center">Name</th>
                              <th  style="text-align:center">Level</th>
                              <th  style="text-align:center">Total Point</th>
              						</tr>
              						</thead>
              						<tbody id="ex">
                                        
                          @foreach($data as $d)
              						<tr style="text-align:center;">
                            <td style="text-align:center;">{{$d->student_id}}</td>
                            <td style="text-align:center;">{{$d->user_name}}</td>
                            <td style="text-align: center;">@if($d->points < 500) Level 1 @elseif($d->points >500 && $d->points < 1000) Level 2 @elseif ($d->points >1000 && $d->points < 2500) Level 3 @elseif ($d->points >2500 && $d->points < 5000) Level 4 @else ($d->points >5000 && $d->points <= 700) Level 5 @endif
                            <td style="text-align:center;"s>{{$d->points}}</td>
                          </tr>
                          @endforeach
              					</tbody>
              				</table>
				</div>
				<!-- /basic datatable -->
		</div>
			<!-- /main content -->
</div>
		
	


        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
       $(document).ready(function() {
    $('#example').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>
<script>
function show1(){
  document.getElementById('div1').style.display ='block';
  document.getElementById('div2').style.display = 'none';
}
function show2(){
  
  document.getElementById('div1').style.display ='none';
  document.getElementById('div2').style.display = 'block';
}
</script>
<script type="text/javascript">
  function ajaxfunction()
  {
     
      var subject_value = $('#sub').val();
      var s_time=$("#time").val();
      var token = $("input[name='_token']").val();
      var url = $('.urlleadership').val();

      if(subject_value!='' && s_time!='')
      {
      $.ajax({
          url: url,
          method: 'POST',
          data: {subject:subject_value,time:s_time,_token:token},
          success: function(data) {
              console.log(data);
              
             var id='';
            var total_point=0;
            $("#ex").html('');
              if(data.length > 0)
              {
              for(var i=0; i< data.length; i++)
              {
                var trHTML =  '<tr style="text-align:center"><td>' + data[i].student_id + '</td><td>' +  data[i].user_name + '</td><td>' +  data[i].points + '</td></tr>';
                $("#ex").append(trHTML);  
            }
             
              
            }
            
            else{
                var trHTML = '';
            }

            
             
          },
          
          error: function() {}
          
     
  });
}
}
</script>
<script type="text/javascript">
 
 function ajax_second()
  {
   
      var subject_value = $('#type').val();
      var s_time=$("#c_time").val();
    
      var token = $("input[name='_token']").val();
      var url = $('.urlleadership').val();
      if(subject_value!='' && s_time!='')
      {
      $.ajax({
          url: url,
          method: 'POST',
          data: {subject:subject_value,time:s_time,_token:token},
          success: function(data) {
              console.log(data);
              
             var id='';
            var total_point=0;
            $("#ex").html('');
              if(data.length > 0)
              {
              for(var i=0; i< data.length; i++)
              {
                var trHTML =    '<tr style="text-align:center"><td>' + data[i].student_id + '</td><td>' +  data[i].user_name + '</td><td>' +  data[i].points + '</td></tr>';
                $("#ex").append(trHTML);  
            }
             
              
            }
            
            else{
                var trHTML = '';
            }

            
             
          },
          
          error: function() {}
          
    
  });
}
}
</script>
@endsection
