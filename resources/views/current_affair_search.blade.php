@if(!empty($record))
				        @php
				        	// dd($record);
				        @endphp
				        <!--getting the record  from the array value $record-->
				        @foreach($record as $data)
				        	@php  $json=json_decode($data['jsondata'],true);
				        	$date=strtotime($data['timestamp']);
							$newDate=date('M d, Y',$date);
							@endphp
						
				        	
				        <div class="col-md-12">
				
				            <!-- Blog layout #1 with video -->
				            <div class="panel panel-flat">
				                <div class="panel-heading">
				                    <h5 class="panel-title text-semibold"><a href="#"><b>{{$json['title']}}</b></a></h5>
				                     <ul class="list-inline list-inline-separate heading-text text-muted">
				                     	 <li>Location: <a href="#" class="text-muted"></a></li>  <li>{{$newDate}}</li>
				                     </ul>
				                </div>
				                <div class="row panel-body">
								<div class="col-md-6">
									<img style="width:200px; height:180px;;" src="/mechanicalinsider/currentaffair/{{ $json['image'] }}" class="" alt="">
							
									
								</div>
								<div class="col-md-6" style="font-size:14px;">
								{!! substr($data['post_description'],0,200) !!}
								</div>
								</div>
							
				
				                <div class="panel-footer panel-footer-transparent">
				                    <div class="heading-elements">
				                        <ul class="list-inline list-inline-separate heading-text text-muted">
				                           
				                            <li><a class="comment" id="comment{{ $data['id'] }}" href="" class="text-muted">{{$data['comment']}} comments</a></li>
				                            @permission('send_CurrentAffair_notification')
				                            @if($data['notification']=="sent")
											<li><a data-toggle="modal" data-target="#notification{{ $data['id'] }}" class="text-default"><i class="icon-bell-check text-pink position-left"></i></a></li>
											@else
											<li><a href="{{ url('send_currentAffair_notification/'.$data['id']) }}" class="text-default"><i class="icon-bell2 text-pink position-left"></i></a></li>
											@endif
											@endpermission

				                            <li><a href="#" class="text-muted"><i class="icon-heart6 text-size-base text-pink position-left"></i> {{$data['likes']}}</a></li>
				                      		@permission('editCurrentAffair')<li><a href="{{ url('editCurrentAffair/'.$data['id']) }}" class="text-muted"><i class="icon-pencil text-size-base text-pink position-left"></i></a></li>@endpermission
				                        	@permission('delete_CurrentAffair') <li><a data-toggle="modal" data-target="#modal_mini{{$data['id']}}" class="text-muted"><i class="icon-trash text-size-base text-pink position-left"></i></a></li>
				                        	@endpermission
				                        </ul>

				                        {{-- notificaion model --}}
										<div  data-backdrop="false" id="notification{{ $data['id'] }}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content">
										<div class="modal-header bg-warning">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Warning</h5>
										</div>

										<div class="modal-body">
											<p style="font-size: 14px;">This post is already notifyed.
												If you want again then click Yes.</p>

										</div>

										<div class="modal-footer">
											
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											<a href="{{  url('send_currentAffair_notification/'.$data['id']) }}"><button type="button" class="btn btn-primary bg-warning">Yes</button></a>
											</form>
										</div>
									</div>
									
								</div>
							</div>
									{{-- notificaion model --}}
				
					<div  data-backdrop="false" id="modal_mini{{$data['id']}}" class="modal fade">
								<div class="modal-dialog modal-xs">
									<div class="modal-content" style="width:350px;">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h5 class="modal-title">Delete Data</h5>
										</div>

										<div class="modal-body">
											<p>Click on Yes if you want to delete this Current Affair.</p>

										</div>

										<div class="modal-footer">
											
											<button type="button" class="btn btn-link" data-dismiss="modal">No</button>
											
											<a href="{{ url('delete_CurrentAffair/'.$data['id']) }}"  class="btn btn-primary">Yes</a>
										
										</div>
									</div>
								</div>
							</div>
							
				                        <a href="{{ url('currentAffairDetail/'.$data['id']) }}" class="heading-text pull-right">Read more <i class="icon-arrow-right14 position-right"></i></a>
				                    </div>
				                     
				                    @if (isset($data['comments']))
				                   <div style="background:#edeaea; padding: 10px; margin-bottom: -11px; display: none;" class="comment-box col-md-12 comment{{ $data['id'] }}">
				                   @php
				                   	$count=0;
				                   @endphp
				                    @foreach ($data['comments'] as $comment)
				                    	@php
				                    	$timestamp1=$comment->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($comment->jsondata,true); 
				                    	@endphp	
				                    <div class=" col-md-12" style="padding: 0px; margin-bottom: 20px;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    	@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="35" height="35"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="35" height="35"></div>
				                    	@endif
				                    		
				                    		<div class="col-md-6"><span style="font-weight: bold;">{{ $com['name'] }}</span><br>
				                    		<span style="margin-top: 5px;">{{ $com['collage'] }}</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 10px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
			                    		<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
			                    			<p>{{ $com['comments'] }}</p>
			                    			@if ($com['com_image']!="null")
			                    			<img src="/mechanicalinsider/{{$com['com_image']}}" width="150" height="100">
			                    			@endif
			                    		</div>
			                    		@if (isset($comment->reply))
			                    		@php
			                    		$reply_count=count($comment->reply);
			                    		@endphp
			                    		<div id="comment_{{ $comment->id }}" class="reply_section_default" style="padding-left:60px;"">
				                    		<p class="pull-left"><a id="show_reply">{{ $reply_count }} replies</a>
				                    		<a style="display: none;" id="hide_reply">Hide {{ $reply_count }} replies</a></p>
				                    	
			                    		@foreach ($comment->reply as $reply)
			                    		@php
				                    	$timestamp1=$reply->timestamp;
				                    	$date1=date('M d,Y ');
				                    	$time1=date('h:i:a');
				                    		$com=json_decode($reply->jsondata,true);
				                    		// print_r($reply); 
				                    	@endphp
				                    		
			                    			<div class="col-md-12 reply_section" style="; margin-bottom: 20px; display:none;">
				                    	<div class="col-md-12"  style="padding: 0px;">
				                    	@if ($com['userimage']==""||$com['userimage']=="null"||$com['userimage']=="NULL")
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ url('img/userimage.png') }}" width="25" height="25"></div>
				                    	@else
				                    		<div class="col-md-1" style="padding: 0px;"><img  style="float:left;" src="{{ $com['userimage'] }}" width="25" height="25"></div>
				                    	@endif
				                    		
				                    		<div class="col-md-6"><span style="font-weight: bold; font-size: 11px;">{{ $com['name'] }}</span><br>
				                    		<span style=" font-size: 11px;" >{{ $com['collage'] }}</span>
				                    		</div>
				                    		<div  class="col-md-5"><span style="float: right; font-size: 10px;">{{ $date1 }}&nbsp;&nbsp;&nbsp;&nbsp;{{ $time1 }} </span></div>
				                    	</div>
										<div style="word-wrap: break-word;"  class="col-md-10 col-md-offset-1" style="margin-top: 10px;">
											<p>{{ $com['comment'] }}</p>
											@if ($com['com_image']!="null")
											<img src="/mechanicalinsider/{{$com['com_image']}}" width="100" height="50">
											@endif
										</div>
										<div class="col-md-10" style="margin-left: -10px;">
											@permission('delete_comment')
											<a><span alt="{{ $reply->commentid }}" id='{{ $reply->id }}' class='delete_reply'>Delete</span></a>
											@endpermission
										</div>
									</div>
										@endforeach
										</div>
										@endif
										<div class="col-md-10" style="margin-left: -10px;">
											@permission('delete_comment')
											<a><span alt='{{ $comment->postid }}' id='{{ $comment->id }}' class='delete_comment'>Delete</span></a>
											@endpermission
										</div>
									</div>
				                    @php
				                    $count++;
				                    // echo $limit;
				                    if($count==5)
				                    {
				                    	echo "<a><span alt='$data->field_type' id='$data->id' class='view_more_comment'>view more comments</span></a>";				          
				                    	break;
				                    }
				                    @endphp

				                    @endforeach
				                    </div>
				                    @endif
				                </div>
				            </div>
				            <!-- /blog layout #1 with video -->
				
				        </div>
				        @endforeach
				        @endif