@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>
	
	{{-- <script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script> --}}

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/components_popups.js') }}"></script>

	{{-- <script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_ckeditor.js') }}"></script>
 --}}



@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">
					@php
						$json=json_decode($query->jsondata,true);
					@endphp
					
						<!-- Basic layout-->
						<form id="login_form" action="{{ url('edit_question') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Question</h5><br>
									
									<input type="hidden" name="sub_test_name" value="{{ $query->sub_test_name }}">
									<input type="hidden" name="test_id" value="{{ $query->testid }}">
									<input type="hidden" name="id" value="{{ $query->id }}">
									<input type="hidden" name="test_name" value="{{ $test_name }}">
								</div>

								<div class="panel-body">
									<div class="col-md-10 col-md-offset-1">
										<div class="form-group">


										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Select Subject:</strong></label>
											<div class="col-lg-6">
												<select id="subjectQuestion" name="subjectQuestion" data-placeholder="choose options" class="select select-border-color border-warning" required>
													<option value="">Select one.</option>
													@if($subjectQuestion )
														@foreach ($subjectQuestion as $key=>$value)
															<option @if($query->subject_name  == $value) selected  @endif value="{{ $value }}">{{ ucfirst($value) }}</option>	
														@endforeach		
													@endif	
												</select>
											</div>
										</div>


										<label class="col-lg-2 control-label"><strong>Subject:</strong></label>
											<div class="col-lg-10">
												<input style="background: #828c96; font-size: 18px;" type="text" name="subject" value="This is {{ $query->sub_test_name }} question " class="form-control" placeholder="" readonly>
											</div>
										</div>
										<script type="text/javascript">
										$(document).ready(function() {
										  $('.summernote').summernote();
										});
										</script>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Question:</strong></label>
											<div class="col-lg-10">
												<textarea  class="summernote" name="question" required>{{  $json['data']['question']['value'] }}</textarea>
												<input type="file" name="question_image">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Hint(question part2):</strong></label>
											<div class="col-lg-10">
												<textarea  class="summernote" name="hint" required>{{  $json['data']['hint']['value'] }}</textarea>
												<input type="file" name="hint_image">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option A:</strong></label>
											<div class="col-lg-6">
												<input type="text" name="option_a" class="form-control" placeholder="" required="required" value="{{  $json['data']['option a']['value'] }}">
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input type="file" name="option_a_image" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option B:</strong></label>
											<div class="col-lg-6">
												<input type="text" name="option_b" class="form-control" placeholder="" required="required" value="{{  $json['data']['option b']['value'] }}">
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input type="file" name="option_b_image" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option C:</strong></label>
											<div class="col-lg-6">
												<input type="text" name="option_c" class="form-control" placeholder="" required="required" value="{{  $json['data']['option c']['value'] }}">
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input type="file" name="option_c_image" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option D:</strong></label>
											<div class="col-lg-6">
												<input type="text" name="option_d" class="form-control" placeholder="" required="required" value="{{  $json['data']['option d']['value'] }}">
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input type="file" name="option_d_image" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option E:</strong></label>
											<div class="col-lg-6">
												<input type="text" name="option_e" class="form-control" placeholder="" value="{{  $json['data']['option e']['value'] }}">
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input type="file" name="option_e_image" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Answer:</strong></label>
											<div class="col-lg-6">
												<select id="answer" name="answer" data-placeholder="choose options" class="select select-border-color border-warning" required>
													<option value="a" @if ($json['data']['answer']['value']=='a')
														selected 
													@endif >Option A</option>
													<option value="b" @if ($json['data']['answer']['value']=='b')
														selected 
													@endif >Option B</option>
													<option id="c" value="c" @if ($json['data']['answer']['value']=='c')
														selected 
													@endif >Option C</option>
													<option  id="d" value="d" @if ($json['data']['answer']['value']=='d')
														selected 
													@endif >Option D</option>
													<option id="e" value="e" @if ($json['data']['answer']['value']=='e')
														selected 
													@endif >Option E</option>
												</select>
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Solution:</strong></label>
											<div class="col-lg-10">
												<textarea  class="summernote" name="solution" required>{{  $json['data']['solution']['value'] }}</textarea>
												
												@if($json['data']['solution']['image'] != 'NULL')
													<img id="solution_image" src="/mechanicalinsider/admin/{{$json['data']['solution']['image']}}"  width="100" height="100" /> </br>
													<a id="removeButton" href="javascript:;" onclick="removeImage()" >Remove</a>  </br>  </br>  </br>
												@endif
												
												<input type="file" name="solution_image">
											</div>
										</div>

										{{-- <div class="form-group">
												<div class="col-lg-4">
													<label>Show Solution to users?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="ans_status" type="checkbox" class="switchery" @if(isset($json['data']['ans_status'])) @if($json['data']['ans_status']=="yes") checked @endif @endif value="yes">
													</label>
												</div>
											</div> --}}



										<input type="hidden" name="old_question_image" value="{{ $json['data']['question']['image'] }}">
										<input type="hidden" name="old_option_a_image" value="{{ $json['data']['option a']['image'] }}">
										<input type="hidden" name="old_option_b_image" value="{{ $json['data']['option b']['image'] }}">
										<input type="hidden" name="old_option_c_image" value="{{ $json['data']['option c']['image'] }}">
										<input type="hidden" name="old_option_d_image" value="{{ $json['data']['option d']['image'] }}">
										<input type="hidden" name="old_option_e_image" value="{{ $json['data']['option e']['image'] }}">
										<input type="hidden" name="old_answer_image" value="{{ $json['data']['answer']['image'] }}">
										<input type="hidden" name="old_hint_image" value="{{ $json['data']['hint']['image'] }}">
										<input type="hidden" id="old_solution_image" name="old_solution_image" value="{{ $json['data']['solution']['image'] }}">

										<input type="hidden" name="test_id" value="{{ $test_id }}">										

										<div class="text-right">
										
											<button  id="submit" type="submit" class="btn btn-primary">Update<i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->


		</div>

@endsection
@section('js')
{{-- for switch buttons --}}
	{{-- <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script> --}}
	{{-- for switch buttons --}}
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules: {
				question: {
	                required:"#question_image:blank"
	            },
	            question_image: {
	                required:"#question:blank"
	            },
	            option_a: {
	                required:"#option_a_image:blank"
	            },
	            option_a_image: {
	                required:"#option_a:blank"
	            },
	            option_b: {
	                required:"#option_b_image:blank"
	            },
	            option_b_image: {
	                required:"#option_b:blank"
	            },
	            // option_c: {
	            //     required:"#option_c_image:blank"
	            // },
	            // option_c_image: {
	            //     required:"#option_c:blank"
	            // },
	            // option_d: {
	            //     required:"#option_d_image:blank"
	            // },
	            // option_d_image: {
	            //     required:"#option_d:blank"
	            // },
	            // option_e: {
	            //     required:"#option_e_image:blank"
	            // },
	            // option_e_image: {
	            //     required:"#option_e:blank"
	            // },
	            answer: {
	                required:true
	            },
				option_a: {
	                required:true
	            },
				option_b: {
	                required:true
	            },
				option_c: {
	                required:true
	            },
				option_d: {
	                required:true
	            },
	            
	            // hint: {
	            //     required:"#hint_image:blank"
	            // },
	            // hint_image: {
	            //     required:"#hint:blank"
	            // },
	            // solution: {
	            //     required:"#solution_image:blank"
	            // },
	            // solution_image: {
	            //     required:"#solution:blank"
	            // }
        },
        messages:{},
        errorElement : 'span',
    	errorLabelContainer: '.errorTxt'
        
		});
	});

	function removeImage(){

		$("#old_solution_image").val('NULL');
		$("#solution_image").hide();
		$("#removeButton").hide();
		
	}
</script>

@endsection