@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>
	
	

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>
	

	{{-- <script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_ckeditor.js') }}"></script>
 --}}



@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('submit_test_series') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Test Series</h5>									
								</div>

								<div class="panel-body">
								
							
									<div class="form-group" >
										<label class="col-lg-2 control-label">Sector</label>
										 <div class="col-lg-10">
											<select id="sector" name="sector" data-placeholder="Select one..." class="select select-border-color border-warning" required>
												<option value="">Select one...</option>
												@foreach ($sectors as $s_name)
													<option value="{{ $s_name->id }}">{{ $s_name->name }}</option>
												@endforeach
											</select> 
										</div>
									</div>


									<div class="form-group" id="groups_select">
                                        <label class="col-lg-2 control-label">Select @if(Auth::user()->role == 'admin' ) Courses @else Class @endif:</label>
                                        <div class="col-lg-10 multi-select-full">
                                            <select name="course_id[]" data-placeholder="Select one..." multiple="multiple" class="multiselect-select-all-filtering course_id" required>
                                                @foreach ($new_Courses as $c)
                                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                

									<div class="form-group" id="groups_select">
										<label class="col-lg-2 control-label">Select Test:</label>
										<div class="col-lg-10 multi-select-full">
											<select name="test_id[]" data-placeholder="Select one..." multiple="multiple" class="select select-border-color border-warning sub_record " required>
                                                <option value="">Select one..</option>    
												@foreach ($test as $t)
													@php $test_name=json_decode($t->jsondata,true);  @endphp
													<option value="{{ $t->id }}">{{ $test_name['testname']}}</option>
												@endforeach                                                  
											</select>
										</div>
									</div>
										

									<div class="form-group">
										<label class="col-lg-2 control-label">Test Series Name</label>
										<div class="col-lg-10">
											<input type="text" name="title" class="form-control" placeholder="Enter test series name" required>
										</div>
									</div>

                                    <div class="form-group">
										<label class="col-lg-2 control-label">Image</label>
										<div class="col-lg-10">
											<input type="file" name="image" class="form-control">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-2 control-label">Price:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" name="payment" value="free" checked="checked">
												Free
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" name="payment" value="paid">
												Paid
											</label>
										</div>
									</div>
									
									<div class="form-group"  id="payment1">
										<label class="col-lg-2 control-label">Amount:</label>
										<div class="col-lg-9">
											<input type="number" id="amount" class="form-control" name="amount" >
										</div>
									</div>


									<div class="text-right">
										<button id="submit" type="submit" class="btn btn-primary">Add<i class="icon-arrow-right14 position-right"></i></button>
									</div>

									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->


		</div>

@endsection
@section('js')

	<script>

		jQuery(document).ready(function(){

			jQuery("#payment1").hide();
			$('#amount').prop('required',false);

			jQuery('input:radio[name="payment"]').change(function(){
				if(jQuery(this).val() == 'paid'){
					jQuery("#payment1").show();
					$('#amount').prop('required',true);
				}
				else
				{
					jQuery("#payment1").hide();
					$('#amount').prop('required',false);
				}
			});
		});

		jQuery(document).ready(function(){
			jQuery('input:radio[name="negative"]').change(function(){
				if(jQuery(this).val() == 'marks'){
					jQuery("#negitive").show();
				}
				else
				{
					jQuery("#negitive").hide();
				}
			});
		});

		$(document).ready(function(){

			$('.summernote').summernote();

			$("#login_form").validate({

				rules:{
					
					test_name:{
						required:true
					},
					description:{
						required:true
					},
					groupid:{
						required:true
					},
					time:{
						required:true,
						number:true
					},
					postby:{
						required:true
					},
					sub_tests:{
						required:true
					}
				}
			});

			var max_fields      = 4; //maximum input boxes allowed
			var wrapper         = $(".input_fields_wrap"); //Fields wrapper
			var add_button      = $(".add_field_button"); //Add button ID
			
			var x = 1; //initlal text box count
			$(add_button).click(function(e){ //on add input button click
				e.preventDefault();
				if(x <= max_fields){ //max input box allowed
					x++; //text box increment
					$(wrapper).append('<div class="col-md-12 field_section"><div class="col-md-6"><input type="text" class="form-control" name="cat[]" placeholder="Enter Category"></div><div class="col-md-3"><input type="number" class="form-control" name="questions[]" placeholder="Enter no. of questions"></div><div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div></div>'); //add input box

					if(x>max_fields)
					{
						$('.add_field_button').hide();
					}

					// $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
				}
				
			});
			
			$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
				e.preventDefault(); $(this).parents('div.field_section').remove(); x--;
				$('.add_field_button').show();
			})
		});

		$('select[name="groupid[]"]').change(function(){
			var course= $(this).val();

			// console.log(course_ids);			
			// var jsonString = JSON.stringify(course_ids);

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_test',   
				data: {course:course},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				} 
			});
		});
	</script>

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dynamicdropdown.js') }}"></script>
	

@endsection