@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>

	<!-- Theme JS files -->
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				topic:{
					required:true
				},
				city:{
					required:true
				},
				status:{
					required:true
				},
				payment:{
					requried:true
				},
				payment1:{
					number:true
				}
				
				
			}
		});
	});
</script>
<script>
	
	$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



</script>
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">
{{-- @php
	dd($discussion);
@endphp --}}
						<!-- Basic layout-->
						<form id="login_form" action="{{ url('update_categories') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Category</h5>
								
								</div>

								<div class="panel-body">
									<div class="form-group">
										<label class="col-lg-3 control-label">Category Name:</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="" name="name" value="{{ $data->name }}">
										</div>
									</div>
									
																	


									<div class="form-group">
										<label class="col-lg-3 control-label">Status</label>
										<div class="col-lg-9">
											<select name="status" class="bootstrap-select" data-width="100%">
												<option value="public"@if ($data->status=="public") selected @endif >Public</option>
												<option value="private"@if ($data->status=="private") selected @endif >Private</option>
											</select>
										</div>
									</div>

										
									<div class="form-group">

										<input type="hidden" name="old_logo" value="{{ $data->image }}">
										<input type="hidden" name="id" value="{{ $data->id }}">

										<label class="col-lg-3 control-label"  style="top:40px;">
											 <div class="input-group-btn">
							                  <div class="fileUpload btn btn-danger fake-shadow">
							                    <span><i class="glyphicon glyphicon-upload"></i> Upload Logo</span>
							                    <input id="logo-id" name="logo" type="file" class="attachment_upload" >
							                  </div>
							                </div>
										</label>
										<div class="col-lg-9">
											 <div class="main-img-preview">
											<img class="thumbnail img-preview" style="min-height: 150px;" src="../../../mechanicalinsider/category_images/{{ $data->image }}" title="Preview Logo" >
							              </div>
																		 <div class="input-group">
							                <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled" style="display:none">
							               
							              </div>
										</div>
									</div>
									<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
									<div class="text-right">
										<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Update <i class="icon-arrow-right14 position-right"></i></button>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')
