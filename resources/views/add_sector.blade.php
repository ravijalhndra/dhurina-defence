@extends('layouts.main')
<link rel="stylesheet" href="{{ URL::asset('assets/Date-Time-Picker-Bootstrap-4/build/css/bootstrap-datetimepicker.min.css')}}">

@section('js_head')
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">
	
	<script src="{{ URL::asset('assets/Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js')}}"></script>
	<script type="text/javascript">
		$(function () {
			$('#datetimepicker1').datetimepicker();
		});
	</script>
@include('mapjs')	
   
	<script>
		
		$(document).ready(function() {
			var brand = document.getElementById('logo-id');
			brand.className = 'attachment_upload';
			brand.onchange = function() {
				document.getElementById('fakeUploadLogo').value = this.value.substring(12);
			};

			// Source: http://stackoverflow.com/a/4459419/6396981
			function readURL(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					
					reader.onload = function(e) {
						$('.img-preview').attr('src', e.target.result);
					};
					reader.readAsDataURL(input.files[0]);
				}
			}
			$("#logo-id").change(function() {
				readURL(this);
			});
		});
	</script>

    <style>
        
        #loading {
            width:100px;
            height: 100px;
            position: fixed;
            top: 30%;
            left: 45%;
            z-index:2;
            background-color: transparent;        
        }
    </style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('insert_sector') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                            {{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Create New Sector</h5>									
								</div>

								<div class="panel-body">
									<div class="form-group">
										<label class="col-lg-3 control-label">Sector Name </label>
										<div class="input_fields_wrap col-md-9">
                                            <input type="text" class="form-control" name="name" placeholder="Enter Sector Name" required>											
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-lg-3 control-label" style="top:40px;">
											<div class="fileUpload btn btn-danger fake-shadow">
							                    <span><i class="glyphicon glyphicon-upload"></i> Upload Sector Icon</span>
							                    <input id="logo-id" name="banner" type="file" class="attachment_upload"  required>
							                  </div>
							                  
										</label>
										<div class="col-lg-9">
											 <div class="main-img-preview">
											 <img class="thumbnail img-preview" src="{{  URL::asset('img/upload-icon.png') }}" style="min-height: 150px;" title="Preview Logo" >
							              </div>
																		 <div class="input-group">
							                <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled" style="display:none">
							                <div class="input-group-btn">
							                  
							                </div>
							              </div>
										</div>
									</div>
								</div>

								<br /><br />
								<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Add Sector <i class="icon-arrow-right14 position-right"></i></button>
								</div><br>

							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')
    <script>
        $(document).ready(function() {
            var max_fields      = 8; //maximum input boxes allowed
            var wrapper         = $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID
            
            var x = 1; //initlal text box count
            $(".add_field_button").click(function(e){ //on add input button click
                e.preventDefault();
                if(x <= max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div style="margin-top:10px;" class="col-md-12 field_section"><div class="col-md-4"><input type="text" class="form-control" name="sub[]" placeholder="Enter Subject Name"></div><div class="col-md-4"><input type="file" class="form-control" name="icon[]" placeholder="Subject icon"></div><div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div></div>'); //add input box

                    if(x>max_fields)
                    {
                        $('.add_field_button').hide();
                    }

                    // $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
                }												
            });
            
            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parents('div.field_section').remove(); x--;
                $('.add_field_button').show();
            })
        });
    </script>

    <script>
        $('#login_form').submit(function() {
            $('#gif').css('visibility', 'visible');
        });
    </script>
	
@endsection
