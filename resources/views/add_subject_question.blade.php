@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_layouts.js') }}"></script>
	
	{{-- <script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script> --}}

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>

	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/components_popups.js') }}"></script>

	{{-- <script type="text/javascript" src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_ckeditor.js') }}"></script>
 --}}

<style type="text/css">
	.errorTxt{
  border: 1px solid red;
  min-height: 20px;
}
</style>

@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">
			
					
								
					
						<!-- Basic layout-->
						<form id="login_form" action="{{route('submit_sub_question')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Questions</h5><br>
									
								
								</div>

								<div class="panel-body">
									<div class="col-md-10 col-md-offset-1">
									<div class="form-group">
									<label class="col-lg-3 control-label">Select Subject:</label>
									<div class="col-lg-9 multi-select-full">
										
										<select name="subject_name" id="sub_name" data-placeholder="Select subject..."    class="filtering form-control select2 valid" required>
											@if($topic!='null')
												@foreach ($topic as $t)
												
													<option value="{{$t->sub_test_name}}">{{ $t->sub_test_name }}</option>
												
												@endforeach
												<option id="create_new_sub" value="new_sub">Create new subject</option>
											@endif
											
										</select><br>
									</div>



									<div id="new_subject">
										<label class="col-lg-3 control-label">Create New Subject:</label>
										<div class="col-lg-9">
											<input type="text" id="new_subject" name="new_subject" class="form-control" placeholder="Add new subject name" required="">
										</div>
									</div>
									
									<script>
											$(document).ready(function(){
												$("#new_subject").hide();
												$("#sub_name").change(function(){
													var subject=$("#sub_name").val();
													if(subject=='new_sub')
													{
														$("#new_subject").show();
													}
													else
													{
														$("#new_subject").hide();
													}
												});
												
											});
										</script>

									</div>

								
										<script type="text/javascript">
										$(document).ready(function() {
										  $('.new_summernote').summernote({
											  toolbar: [
											    // [groupName, [list of button]]
											    ['style', ['bold', 'italic', 'underline', 'clear']],
											    ['font', ['strikethrough', 'superscript', 'subscript']],
											    ['fontsize', ['fontsize']],
											    ['color', ['color']],
											    ['para', ['ul', 'ol', 'paragraph']],
											    ['height', ['height']]
											  ]
											});
										});
										</script>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Question:</strong></label>
											<div class="col-lg-10">
												<textarea id="question" class="mygroup new_summernote" name="question" ></textarea>
												<span class="erroTxt"></span>
												<!-- <input id="question_image" type="file" name="question_image" class="mygroup">
												<span class="erroTxt"></span> -->
											</div>
										</div>
										{{-- <div class="form-group">
											<label class="col-lg-2 control-label"><strong>Hint(question part2):</strong></label>
											<div class="col-lg-10">
												<textarea id="hint"  class="new_summernote" name="hint" required></textarea>
												<!-- <input id="hint_image" type="file" name="hint_image"> -->
											</div>
										</div> --}}
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option A:</strong></label>
											<div class="col-lg-6">
												<input type="text" id="option_a" name="option_a" class="form-control" placeholder="" required="required">
											</div>
											<!-- <div class="col-lg-3 col-md-offset-1">
												<input type="file" id="option_a_image" name="option_a_image" class="form-control" >
											</div> -->
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option B:</strong></label>
											<div class="col-lg-6">
												<input type="text" id="option_b" name="option_b" class="form-control" placeholder="" required="required">
											</div>
											<!-- <div class="col-lg-3 col-md-offset-1">
												<input type="file" id="option_b_image" name="option_b_image" class="form-control">
											</div> -->
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option C:</strong></label>
											<div class="col-lg-6">
												<input  type="text" id="option_c" name="option_c" class="form-control" placeholder="" required="required">
											</div>
											<!-- <div class="col-lg-3 col-md-offset-1">
												<input id="option_c_image" type="file" id="option_c_image" name="option_c_image" class="form-control">
											</div> -->
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option D:</strong></label>
											<div class="col-lg-6">
												<input id="option_d" type="text" name="option_d" class="form-control" placeholder="" required="required">
											</div>
											<!-- <div class="col-lg-3 col-md-offset-1">
												<input id="option_d_image" type="file" name="option_d_image" class="form-control">
											</div> -->
										</div>
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Option E:</strong></label>
											<div class="col-lg-6">
												<input id="option_e" type="text" name="option_e" class="form-control" placeholder="">
											</div>
											<!-- <div class="col-lg-3 col-md-offset-1">
												<input id="option_e_image" type="file" name="option_e_image" class="form-control">
											</div> -->
										</div>
										{{-- <div class="form-group">
											<label class="col-lg-2 control-label"><strong>Answer:</strong></label>
											<div class="col-lg-6">
												<input id="answer" type="text" name="answer" class="form-control" placeholder="">
											</div>
											<div class="col-lg-3 col-md-offset-1">
												<input id="answer_image" type="file" name="answer_image" class="form-control">
											</div>
										</div> --}}


										<div class="form-group" id="hello">
											<label class="col-lg-2 control-label"><strong>Answer:</strong></label>
											<div class="col-lg-6">
												<select id="answer" name="answer" data-placeholder="choose options" class="select select-border-color border-warning" required>
															<option  value="">Select answer</option>
															<option id="a" value="a">Option A</option>
															<option id="b" value="b">Option B</option>
															<option id="c" value="c">Option C</option>
															<option id="d" value="d" hidden>Option D</option>
															<option id="e" value="e">Option E</option>
												</select>
											</div>
										</div>

										
										<div class="form-group">
											<label class="col-lg-2 control-label"><strong>Solution:</strong></label>
											<div class="col-lg-10">
												<textarea id="solution"  class="new_summernote" name="solution" required></textarea>
												<!-- <input id="solution_image" type="file" name="solution_image"> -->
											</div>
										</div>

										{{-- <div class="form-group">
												<div class="col-lg-4">
													<label>Show Solution to users?</label>
												</div>
												<div class="checkbox checkbox-switchery col-lg-8">
													<label>
														<input name="ans_status" checked type="checkbox" class="switchery" value="yes">
													</label>
												</div>
											</div> --}}

											
										
										

										

										<div class="text-right">
										
											<button  title="you have entered all question in this test.!"
											 id="submit" type="submit" class="btn btn-primary">Add<i class="icon-arrow-right14 position-right"></i></button></span>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->


		</div>

@endsection
@section('js')
{{-- for switch buttons --}}
	{{-- <script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_checkboxes_radios.js') }}"></script> --}}
	{{-- for switch buttons --}}
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules: {
				question: {
	                required:"#question_image:blank"
	            },
	            question_image: {
	                required:"#question:blank"
	            },
	            option_a: {
	                required:"#option_a_image:blank"
	            },
	            option_a_image: {
	                required:"#option_a:blank"
	            },
	            option_b: {
	                required:"#option_b_image:blank"
	            },
	            option_b_image: {
	                required:"#option_b:blank"
	            },
	            // option_c: {
	            //     required:"#option_c_image:blank"
	            // },
	            // option_c_image: {
	            //     required:"#option_c:blank"
	            // },
	            // option_d: {
	            //     required:"#option_d_image:blank"
	            // },
	            // option_d_image: {
	            //     required:"#option_d:blank"
	            // },
	            // option_e: {
	            //     required:"#option_e_image:blank"
	            // },
	            // option_e_image: {
	            //     required:"#option_e:blank"
	            // },
	            answer: {
	                required:true
	            },
				option_a: {
	                required:true
	            },
				option_b: {
	                required:true
	            },
				option_c: {
	                required:true
	            },
				option_d: {
	                required:true
	            },
	            
	            // hint: {
	            //     required:"#hint_image:blank"
	            // },
	            // hint_image: {
	            //     required:"#hint:blank"
	            // },
	            // solution: {
	            //     required:"#solution_image:blank"
	            // },
	            // solution_image: {
	            //     required:"#solution:blank"
	            // }
        },
        messages:{},
        errorElement : 'span',
    	errorLabelContainer: '.errorTxt'
        
		});
	});

</script>

@endsection