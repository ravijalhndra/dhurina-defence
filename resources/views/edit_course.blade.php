@extends('layouts.main')
@section('js_head')
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">
@include('mapjs')

<script>
	
	$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



</script>


		
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('update_course') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
                                <input type="hidden" name="id" value="{{ $data->id }}" />
                            <div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Create New @if(Auth::user()->role == 'admin' ) Courses @else Class @endif</h5>									
								</div>

								<div class="panel-body">
									
									@if(Auth::user()->role == 'school' )
										<div class="form-group">
											<label class="col-lg-3 control-label">Select School:</label>
											<div class="col-lg-9 multi-select-full">
												<select name="school_id" class="bootstrap-select" data-placeholder="Select School..."  @if(Auth::user()->role == 'school') required @endif>
													<option value="" selected disabled>Select Course..</option>  
													@foreach ($school as $c)
														<option  @if($data->school_id == $c->id) selected @endif @if(Auth::user()->role == 'school')  @if(Auth::user()->id == $c->id ) selected @endif @endif value="{{ $c->id }}">{{ $c->name }}</option>                                                        
													@endforeach                                                     
												</select>                                             
											</div>
										</div>
									@endif

									<div class="form-group">
										<label class="col-lg-3 control-label">@if(Auth::user()->role == 'admin' ) Courses @else Class @endif Name </label>
										<div class="col-lg-9">
											<input type="text" class="form-control" value="{{ $data->name }}" placeholder="@if(Auth::user()->role == 'admin' ) Courses @else Class @endif Name" name="name" required>
										</div>
									</div>

									@php $sub_id=json_decode($data->subject); @endphp
									<div class="form-group" id="groups_select">
										<label class="col-lg-3 control-label">Select Subject:</label>
										<div class="col-lg-9 multi-select-full">
											<select name="sub[]" data-placeholder="Select Subject..." multiple="multiple" class="multiselect-select-all-filtering" required>
												@foreach ($subject as $s)
													<option @if(in_array ($s->id,$sub_id) ) selected   @endif    value="{{ $s->id }}">{{ $s->name }}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Price</label>
										<div class="col-lg-9">
											<input type="number" class="form-control onlynumber" value="{{ $data->price }}" placeholder="Price" name="price" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Discount(In Rs)</label>
										<div class="col-lg-9">
											<input type="number" class="form-control onlynumber" value="{{ $data->discount }}" placeholder="Discount(In Rs)" name="discount" required>
										</div>
									</div>

									<script>
										$(document).ready(function() {
											var max_fields      = 8; //maximum input boxes allowed
											var wrapper         = $(".input_fields_wrap"); //Fields wrapper
											var add_button      = $(".add_field_button"); //Add button ID
											
											var x = 1; //initlal text box count
											$(add_button).click(function(e){ //on add input button click
												e.preventDefault();
												if(x <= max_fields){ //max input box allowed
													x++; //text box increment
													$(wrapper).append('<div style="margin-top:10px;" class="col-md-12 field_section"><div class="col-md-9"><input type="text" class="form-control" name="sub[]" placeholder="Enter Subject Name"></div><div class="col-md-3"><button class="remove_field btn btn-danger btn-sm">Remove</button></div></div>'); //add input box

													if(x>max_fields)
													{
														$('.add_field_button').hide();
													}

													// $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>');
												}												
											});
											
											$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
												e.preventDefault(); $(this).parents('div.field_section').remove(); x--;
												$('.add_field_button').show();
											})
										});
									</script>

									

									<div class="form-group" >
										<label class="col-lg-3 control-label">Sector</label>
										 <div class="col-lg-9">
											<select id="sector" name="sector" data-placeholder="Select one..." class="select select-border-color border-warning" required>
												<option value="">Select one...</option>
												@foreach ($sector as $s_name)
													<option @if($data->sector == $s_name->id ) selected  @endif value="{{ $s_name->id }}">{{ $s_name->name }}</option>
												@endforeach
											</select> 
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Position</label>
										<div class="col-lg-9">
											<input type="text" class="form-control onlynumber" value="{{ $data->position }}" placeholder="Position" name="position" />
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-2 control-label">Description:</label>
										<div class="col-lg-10">
											<textarea  name="description" class="summernote"> {{ $data->description }}</textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label" style="top:40px;">
											<div class="fileUpload btn btn-danger fake-shadow">
							                    <span><i class="glyphicon glyphicon-upload"></i> Upload Course Image</span>
							                    <input id="logo-id" name="banner" value="{{ $data->image }}" type="file" class="attachment_upload" >
							                  </div>
							                  
										</label>
										<div class="col-lg-9">
											 <div class="main-img-preview">
											 <img class="thumbnail img-preview" @if($data->image != '') src="{{  URL::asset('img/'.$data->image) }}" @else src="{{  URL::asset('img/upload-icon.png') }}" @endif   style="min-height: 150px;" title="Preview Logo" >
							              </div>
																		 <div class="input-group">
							                <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled" style="display:none">
							                <div class="input-group-btn">
							                  
							                </div>
							              </div>
										</div>
									</div>
								</div>
								<br /><br />
								<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Update Course <i class="icon-arrow-right14 position-right"></i></button>
								</div><br>

							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')

	<script type="text/javascript">
		$(document).ready(function() {
		    $('.summernote').summernote();
		});
	</script>
@endsection
