@extends('layouts.main')
@section('js_head')


<style type="text/css">
	h3{
		margin-top: 0px !important;
		margin-bottom: 10px !important;
	}
</style>
@endsection
@section('content')
	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
					
					<ul style="position: fixed; right: 5%; top: 88%;" class="fab-menu fab-menu-top-right" data-fab-toggle="click">
						<li>
							<a href="{{ url('edit_question/'.request()->segment(2)) }}" class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
								<i class="fab-icon-open icon-pencil"></i>
								<i class="fab-icon-close icon-cross2"></i>
							</a>
						</li>
					</ul>
					
@php
						$json=json_decode($data->jsondata,true);
						// dd($json);
							// dd($json);
						@endphp
				<!-- Basic datatable -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title">Question Details:</h4>
					</div>

					<div class="panel-body">
						<div class="panel panel-flat" style="background-color: #fafafa;">
							<div class="panel-body">
								<h3>Question:</h3>
								<div class="col-md-12">
									@if ($json['data']['question']['image']=="NULL")
										<p>{!! $json['data']['question']['value'] !!}</p>
									@else
										<div class="col-md-4">
											<p>{!! $json['data']['question']['value'] !!}</p>
										</div>
										<div class="col-md-8">
											<img class="img-responsive" src="/mechanicalinsider/admin/{{ $json['data']['question']['image'] }}" >
										</div>
									@endif
								</div>
							</div>
						</div>
						<div class="panel panel-flat" style="background-color: #fafafa;">
							<div class="panel-body">
								<h3>Options:</h3>
								<div class="col-md-2">
									<label><strong>Option A:</strong></label>
								</div>
								<div class="col-md-10">
									@if ($json['data']['option a']['image']=="NULL")
										<p>{{ $json['data']['option a']['value'] }}</p>
									@else
										<div class="col-md-4">
											<p>{{ $json['data']['option a']['value'] }}</p>
										</div>
										<div class="col-md-8">
											<img class="img-responsive" src="/mechanicalinsider/admin/{{ $json['data']['option a']['image'] }}" >
										</div>
									@endif
								</div>
								<div class="col-md-2">
									<label><strong>Option B:</strong></label>
								</div>
								<div class="col-md-10">
									@if ($json['data']['option b']['image']=="NULL")
										<p>{{ $json['data']['option b']['value'] }}</p>
									@else
										<div class="col-md-4">
											<p>{{ $json['data']['option b']['value'] }}</p>
										</div>
										<div class="col-md-8">
											<img class="img-responsive" src="/mechanicalinsider/admin/{{ $json['data']['option b']['image'] }}" >
										</div>
									@endif
								</div>
								<div class="col-md-2">
									<label><strong>Option c:</strong></label>
								</div>
								<div class="col-md-10">
									@if ($json['data']['option c']['image']=="NULL")
										<p>{{ $json['data']['option c']['value'] }}</p>
									@else
										<div class="col-md-4">
											<p>{{ $json['data']['option c']['value'] }}</p>
										</div>
										<div class="col-md-8">
											<img class="img-responsive" src="/mechanicalinsider/admin/{{ $json['data']['option c']['image'] }}" >
										</div>
									@endif
								</div>
								@if ($json['data']['option d']['image']=="NULL" && $json['data']['option d']['value']=="NULL")
								@else
								<div class="col-md-2">
									<label><strong>Option D:</strong></label>
								</div>
								<div class="col-md-10">
									@if ($json['data']['option d']['image']=="NULL")
										<p>{{ $json['data']['option d']['value'] }}</p>
									@else
										<div class="col-md-4">
											<p>{{ $json['data']['option d']['value'] }}</p>
										</div>
										<div class="col-md-8">
											<img class="img-responsive" src="/mechanicalinsider/admin/{{ $json['data']['option d']['image'] }}" >
										</div>
									@endif
								</div>
								@endif

								@if ($json['data']['option e']['image']=="NULL" && $json['data']['option e']['value']=="NULL")
								@else
								
								<div class="col-md-2">
									<label><strong>Option E:</strong></label>
								</div>
								<div class="col-md-10">
									@if ($json['data']['option e']['image']=="NULL")
										<p>{{ $json['data']['option e']['value'] }}</p>
									@else
										<div class="col-md-4">
											<p>{{ $json['data']['option e']['value'] }}</p>
										</div>
										<div class="col-md-8">
											<img class="img-responsive" src="/mechanicalinsider/admin/{{ $json['data']['option e']['image'] }}" >
										</div>
									@endif
								</div>
								@endif
							</div>
						</div>
						<div class="panel panel-flat" style="background-color: #fafafa;">
							<div class="panel-body">
								<h3>Answer:</h3>
								<div class="col-md-2">
									<label><strong>Answer:</strong></label>
								</div>
								<div class="col-md-10">
									@if ($json['data']['answer']['image']=="NULL")
										<p>{{ $json['data']['answer']['value'] }}</p>
									@else
										<div class="col-md-4">
											<p>{{ $json['data']['answer']['value'] }}</p>
										</div>
										<div class="col-md-8">
											<img class="img-responsive" src="/mechanicalinsider/admin/{{ $json['data']['answer']['image'] }}" >
										</div>
									@endif
								</div>
								<div class="col-md-2">
									<label><strong>Hint:</strong></label>
								</div>
								<div class="col-md-10">
									@if ($json['data']['hint']['image']=="NULL")
										<p>{!! $json['data']['hint']['value'] !!}</p>
									@else
										<div class="col-md-4">
											<p>{!! $json['data']['hint']['value'] !!}</p>
										</div>
										<div class="col-md-8">
											<img class="img-responsive" src="/mechanicalinsider/admin/{{ $json['data']['hint']['image'] }}" >
										</div>
									@endif
								</div>
								<div class="col-md-2">
									<label><strong>Solution:</strong></label>
								</div>
								<div class="col-md-10">
									@if ($json['data']['solution']['image']=="NULL")
										<p>{!! $json['data']['solution']['value'] !!}</p>
									@else
										<div class="col-md-4">
											<p>{!! $json['data']['solution']['value'] !!}</p>
										</div>
										<div class="col-md-8">
											<img class="img-responsive" src="/mechanicalinsider/admin/{{ $json['data']['solution']['image'] }}" >
										</div>
									@endif
								</div>
							</div>
						</div>
					</div>

					
				</div>
				<!-- /basic datatable -->


				

			</div>
			<!-- /main content -->

		</div>
@endsection
