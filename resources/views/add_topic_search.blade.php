@extends('layouts.main')
@section('js_head')


	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
@include('mapjs')

	
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				topic:{
					required:true
				},
				city:{
					required:true
				},
				status:{
					required:true
				},
				logo:{
					required:true
				},
				payment:{
					required:true
				},
				payment1:{
					number:true
				}
				
			}
		});
	});
</script>
<script>
	
	$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



</script>


		
<script>
	$('#login_form').submit(function() {
    $('#gif').css('visibility', 'visible');
});
</script>
<style>
	
	#loading {
		width:100px;
    height: 100px;
    position: fixed;
    top: 30%;
    left: 45%;
	z-index:2;
    background-color: transparent;
	
}
</style>
@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('insert_topic') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Topic</h5>
									
								</div>

							
									<div class="form-group">
										<label class="col-lg-3 control-label">Topic Name</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Enter Topic Name" name="topic_name" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Test Count</label>
										<div class="col-lg-9">
											<input type="date" class="form-control" placeholder="Test Count" name="test_count" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label">Post Count</label>
										<div class="col-lg-9">
											<input type="date" class="form-control" placeholder="Post Count" name="post_count" required>
										</div>
									</div>
									
									<!-- <div class="form-group">
										
											<label class="col-lg-3 control-label">Price:</label>
											<div id="price" class="col-lg-9">
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="payment" value="0">
														Free
													</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="radio">
													<label>
														<input type="radio" name="payment" value="paid">
														Paid
													</label>
												</div>
											</div>
											<input style="display: none;" id="payment1" type="text" name="payment1" class="form-control" placeholder="Enter amount">
											</div>
										</div> -->
										<script type="text/javascript">
											jQuery(document).ready(function(){
												jQuery('input:radio[name="payment"]').change(function(){
												    if(jQuery(this).val() == 'paid'){
												    	jQuery("#payment1").show();
												    }
												    else
												    {
												    	jQuery("#payment1").hide();
												    }
												});
											});
										</script>
									

									<!-- <div class="form-group">
										<label class="col-lg-3 control-label">Status</label>
										<div class="col-lg-9">
											<select name="status" class="bootstrap-select" data-width="100%">
												<option value="public">Public</option>
												<option value="private">Private</option>
											</select>
										</div>
									</div> -->

										<div class="form-group">
										<label class="col-lg-3 control-label" style="top:40px;">
											<div class="fileUpload btn btn-danger fake-shadow">
							                    <span><i class="glyphicon glyphicon-upload"></i> Upload Banners</span>
							                    <input id="logo-id" name="banner" type="file" class="attachment_upload" >
							                  </div>
							                  
										</label>
										<div class="col-lg-9">
											 <div class="main-img-preview">
											 <img class="thumbnail img-preview" src="{{  URL::asset('img/upload-icon.png') }}" style="min-height: 150px;" title="Preview Logo" >
							              </div>
																		 <div class="input-group">
							                <input id="fakeUploadLogo" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled" style="display:none">
							                <div class="input-group-btn">
							                  
							                </div>
							              </div>
										</div>
									</div>

									<!-- <div class="form-group">
											<label class="col-lg-3 control-label">Search Your Location</label>
										<div class="col-md-12">
												<div class="pac-card" id="pac-card">
														<div>
														 
														  <div id="type-selector" class="pac-controls">
															<input type="radio" name="type" id="changetype-all" checked="checked">
															<label for="changetype-all">All</label>
												  
															<input type="radio" name="type" id="changetype-establishment">
															<label for="changetype-establishment">Establishments</label>
												  
															<input type="radio" name="type" id="changetype-address">
															<label for="changetype-address">Addresses</label>
												  
															<input type="radio" name="type" id="changetype-geocode">
															<label for="changetype-geocode">Geocodes</label>
														  </div>
														  <div id="strict-bounds-selector" class="pac-controls">
															<input type="checkbox" id="use-strict-bounds" value="">
															<label for="use-strict-bounds">Strict Bounds</label>
														  </div>
														</div>
														<div id="pac-container">
														  <input id="pac-input" type="text"
															  placeholder="Enter a location">
														</div>
													  </div>
													  <div id="map"></div>
													  <div id="infowindow-content">
														<img src="" width="16" height="16" id="place-icon">
														<span id="place-name"  class="title"></span><br>
														<span id="place-address"></span>
													  </div>
										</div>
									</div> -->
									<!-- <div class="form-group">
										<div class="col-lg-4"><input type="text" name="lat" class="form-control" readonly placeholder="Group Latitude" id="lat" required> </div>
										<div class="col-lg-4">	<input type="text" name="lng" class="form-control" readonly placeholder="Group Longitude" id="lng" required> </div> -->
											<!-- <div class="col-lg-4"><input type="text" name="address" class="form-control" placeholder="Complete Address" id="address" required> </div> -->
							                  
									</div>
								
										<br />
										<br />
									<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
									<div class="text-right">
										<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Add Banner <i class="icon-arrow-right14 position-right"></i></button>
									</div><br>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')
