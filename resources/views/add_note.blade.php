@extends('layouts.main')
@section('js_head')
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_select2.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/pages/editor_summernote.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/js/plugins/editors/summernote/summernote.css') }}">

    <style>        
        #loading {
            width:100px;
            height: 100px;
            position: fixed;
            top: 30%;
            left: 45%;
            z-index:2;
            background-color: transparent;        
        }
    </style>

@endsection
@section('content')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ route('insert_note') }}" id="myForm" method="post" class="form-horizontal" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Note & Ebook</h5>									
								</div>

								<div class="panel-body">

                                    <div class="form-group">
										<label class="col-lg-3 control-label">Type:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" name="type" value="notes" checked="checked"  />
												Notes
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" name="type" value="ebook"/>
												E-book
											</label>
										</div>
									</div>

                                    <div class="form-group" id="groups_select">
                                        <label class="col-lg-3 control-label">Select Sector:</label>
                                        <div class="col-lg-9">
                                            <select name="sector" data-placeholder="Select one..." class="select select-border-color border-warning " required>
                                                 <option selected default value="">Select one..</option>
                                                @foreach ($sector as $s)
                                                    <option value="{{ $s->id }}">{{$s->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
										<label class="col-lg-3 control-label">Name</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" placeholder="Name" name="name" required>
										</div>
                                    </div>
                                    
                                    <div class="form-group">
										<label class="col-lg-3 control-label">Note & e-book Image</label>
										<div class="col-lg-9">
											<input type="file"  class="form-control"  name="image" required>
										</div>
                                    </div>

                                    <div class="form-group">
										<label class="col-lg-3 control-label">File</label>
										<div class="col-lg-9">
											<input type="file"  class="form-control"  name="file" required>
										</div>
                                    </div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Position</label>
										<div class="col-lg-9">
											<input type="text" class="form-control onlynumber" placeholder="position" name="position" value="{{ $last_position }}">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-3 control-label">Price Type:</label>
										<div class="col-lg-9">
											<label class="radio-inline">
												<input type="radio" class="styled" name="price_type" value="0" checked="checked"  />
												Free
											</label>
												
											<label class="radio-inline">
												<input type="radio" class="styled" name="price_type" value="1"/>
												Paid
											</label>
										</div>
									</div>

									<div class="form-group" id="price" style="display:none;">
										<label class="col-lg-3 control-label">Price</label>
										<div class="col-lg-9">
											<input type="text" class="form-control onlynumber" placeholder="Price" name="price" value="">
										</div>
									</div>
									<div class="form-group" id="discount" style="display:none;">
										<label class="col-lg-3 control-label">Discount(In Rs)</label>
										<div class="col-lg-9">
											<input type="number" class="form-control onlynumber" placeholder="Discount" name="discount" value="">
										</div>
									</div>
 
								</div>
                                <br /><br />
                                
								<div id="loading" style="display:none;"><img src="img/EPINPUzG3GNIQ.gif" alt="" /></div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary" {{-- onclick="$('#loading').show();" --}}  >Add Note <i class="icon-arrow-right14 position-right"></i></button>
								</div><br>
							</div>
						</form>
						<!-- /basic layout -->

					</div>
				</div>
				<!-- /vertical form options -->
			<!-- /fieldset legend -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

@endsection
@section('js')

	<script type="text/javascript">
		$(document).ready(function() {

			$('#topic_select').hide();
			$('.summernote').summernote();

		});
	
		$('select[name="course_id[]"]').change(function(){
			var course= $(this).val();

			$.ajax({		            	
				type: "POST",  
				url: base_url+'api/get_subject',   
				data: {course:course},                                      
				success: function(data)
				{
					$(".sub_record").html('');
					if(data.status == 'success')
					{						
						$('#subcategory').show();
						var markup=new Array();
						markup.push("<option value='' >Select One. </option>");						
						$.each(data.data , function(index, val) {
							var  record="<option value='"+val.id+"' >"+val.name+" </option>";
							markup.push(record);
						});

						$(".sub_record").append(markup);
					}
				}  
			});			
		});

		
		
	</script>

	<script>
		$(document).ready(function(){
			$(':radio[name=price_type]').change(function(){
				if($(this).val()=="1")
				{
				$("#price").css("display","block");
				$("#discount").css("display","block");
				}
			else
				{
				$("#price").css("display","none");
				$("#discount").css("display","none");
				}
			});
		});
	</script>
@endsection
