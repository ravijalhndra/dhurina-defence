
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ config('app.name') }}</title>

	<!-- Global stylesheets -->
	@include('includes.css')
	<!-- /global stylesheets -->
    @yield('css')
    @include('includes.js')

    @yield('js_head')
    <style>
    	.error{
    		color: red;
    	}
    </style>

</head>

<body class="navbar-bottom">

	<!-- Page header -->
	@include('includes.header')	
	<!-- /page header -->

	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		@yield('content')
		<!-- /page content -->
	</div>
	<!-- /page container -->

	<!-- Footer -->
	@include('includes.footer')
	<!-- /footer -->
@if (session('success'))
<script type="text/javascript">
$.toast({
    heading: 'Success',
    text: '{{ session('success') }}',
    showHideTransition: 'slide',
    position: 'top-right',
    stack: 2,
    icon: 'success'
});
</script>
@php
	session()->forget('success');
@endphp
@endif

@if (session('error'))
<script type="text/javascript">
$.toast({
    heading: 'Error',
    text: '{{ session('error') }}',
    position: 'top-right',
    stack: 2,
    icon: 'error',
    loader: true,        // Change it to false to disable loader
    loaderBg: '#9EC600'  // To change the background
})
</script>
@php
	session()->forget('error');
@endphp
@endif

	@yield('js')



</body>
</html>
