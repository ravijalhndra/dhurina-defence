@extends('layouts.main')
@section('js_head')

@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('update_admin') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Edit Admin</h5>
									
								</div>

								<div class="panel-body">
									<div class="col-md-10  col-md-offset-1">
									
										<div class="form-group">
											<label class="col-lg-2 control-label">Name:</label>
											<div class="col-lg-10">
												<input type="text" name="name" class="form-control" value="{{ $admins->name }}" placeholder="Enter name" required autofocus> 
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Email:</label>
											<div class="col-lg-10">
												<input type="email" name="email" class="form-control" value="{{ $admins->email }}"  placeholder="Enter Email" readonly>
											</div>
										</div>
																<!-- Select All and filtering options -->
										@php
											if(strrchr($admins->groups,","))
									        {
											  $groups=explode(",",$admins->groups);
											
											 
									        }
									        else
									        {
											  $groups[]=$admins->groups;
											
											
									        }
										@endphp	
										<div class="form-group">
											<label class="col-lg-2 control-label">Group:</label>
											<div class="col-lg-10">
												<div class="multi-select-full">
										<select class="multiselect-select-all-filtering" multiple="multiple" name="group[]">
										@foreach ($query as $q)
											{{-- @if($groups[0]!="") --}}
												<option value="{{ $q->id }}" @if($groups=="all") selected 
												@elseif(in_array($q->id,$groups))
												selected
												@endif 
												>{{ $q->topic }}</option>
												{{-- @endif --}}
											@endforeach
										</select>
									</div>
											</div>
										</div>


									<div class="form-group">
											<label class="col-lg-2 control-label">Permissions:</label>
											<div class="col-lg-10">
												<div class="multi-select-full">
										<select class="multiselect-select-all-filtering" multiple="multiple" name="link[]">
										@foreach ($permissions as $q1)
												<option value="{{ $q1->id }}" @if (in_array($q1->id,$selected_permissions)) selected
												@endif>{{ $q1->display_name }}</option>
											@endforeach
										</select>
									</div>
											</div>
										</div>
										<input type="hidden" name="role_id" value="{{ $role_id }}">
										<input type="hidden" name="id" value="{{ $admins->id }}">
										
										
										
											<div class="form-group">
											<label class="col-lg-2 control-label">Role:</label>
											<div class="col-lg-10">
												<div class="multi-select-full">
										<select class="form-control"  name="role" id="role">
										
												<option value="admin" @if($admins->role == 'admin') selected @endif>Admin</option>
												<option value="mentor" @if($admins->role == 'mentor') selected @endif>Mentor</option>
											
										</select>
									</div>
											</div>
										</div>
										
									<div id='mentors' @if($admins->name == 'admin')style="display: none;" @endif>
										
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Highest Qualification:</label>
											<div class="col-lg-10">
												<div class="multi-select-full">
										<select class="form-control"  name="qualification">
										
												<option value="">  </option>
									            <option value="B.A" @if($admins->qualification == 'B.A') selected @endif> B.A </option>
									            <option value="M.A" @if($admins->qualification == 'M.A') selected @endif> M.A </option>
									            <option value="B.E" @if($admins->qualification == 'B.E') selected @endif> B.E </option>
									            <option value="M.E" @if($admins->qualification == 'M.E') selected @endif> M.E </option>
									            <option value="B.Tech" @if($admins->qualification == 'B.Tech') selected @endif> B.Tech </option>
									            <option value="M.Tech" @if($admins->qualification == 'M.Tech') selected @endif> M.Tech </option>
									            <option value="B.Sc" @if($admins->qualification == 'B.Sc') selected @endif> B.Sc </option>
									            <option value="M.Sc" @if($admins->qualification == 'M.Sc') selected @endif> M.Sc </option>
									            <option value="BBA" @if($admins->qualification == 'BBA') selected @endif> BBA </option>
									            <option value="MBA" @if($admins->qualification == 'MBA') selected @endif> MBA </option>
									            <option value="BCA" @if($admins->qualification == 'BCA') selected @endif> BCA </option>
									            <option value="MCA" @if($admins->qualification == 'MCA') selected @endif> MCA </option>
									            <option value="P.hd" @if($admins->qualification == 'P.hd') selected @endif> P.hd </option>
									            <option value="M.Phil" @if($admins->qualification == 'M.Phil') selected @endif> M.Phil </option>
									            <option value="B.Arch" @if($admins->qualification == 'B.Arch') selected @endif> B.Arch </option>
									            <option value="MBBS" @if($admins->qualification == 'MBBS') selected @endif> MBBS </option>
									            <option value="MD" @if($admins->qualification == 'MD') selected @endif> MD </option>
									            <option value="BDS" @if($admins->qualification == 'BDS') selected @endif> BDS </option>
									            <option value="MDS" @if($admins->qualification == 'MDS') selected @endif> MDS </option>
									            <option value="BPT" @if($admins->qualification == 'BPT') selected @endif> BPT </option>
									            <option value="B.Pharm" @if($admins->qualification == 'B.pharm') selected @endif> B.Pharm </option>
									            <option value="M.Pharm" @if($admins->qualification == 'M.Pharm') selected @endif> M.Pharm </option>
									            <option value="B.Com" @if($admins->qualification == 'B.Com') selected @endif> B.Com </option>
									            <option value="M.Com" @if($admins->qualification == 'M.Com') selected @endif> M.Com </option>
									            <option value="C.A" @if($admins->qualification == 'C.A') selected @endif> C.A </option>
									            <option value="ICWA" @if($admins->qualification == 'ICWA') selected @endif> ICWA </option>
									            <option value="B.Cs" @if($admins->qualification == 'B.Cs') selected @endif> B.Cs </option>
									            <option value="12th" @if($admins->qualification == '12th') selected @endif> 12th </option>
									            <option value="11th" @if($admins->qualification == '11th') selected @endif> 11th </option>
									            <option value="LLB" @if($admins->qualification == 'LLB') selected @endif> LLB </option>
									            <option value="other" @if($admins->qualification == 'other') selected @endif> OTHER </option>
											
										</select>
									</div>
											</div>
										</div>
										
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Years of experience in teaching :</label>
											<div class="col-lg-10">
												<div class="multi-select-full">
										<select class="form-control"  name="experience">
										
									            <option value="">  </option>
									            <option value="Less than a year" @if($admins->experience == 'Less than a year') selected @endif> Less than a year </option>
									            <option value="Less Two years" @if($admins->experience == 'Less Two years') selected @endif> Less Two years </option>
									            <option value="Less Four years" @if($admins->experience == 'Less Four years') selected @endif> Less Four years </option>
									            <option value="More than 5 years" @if($admins->experience == 'More than 5 years') selected @endif> More than 5 years </option>
											
										</select>
									</div>
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-lg-2 control-label">City:</label>
											<div class="col-lg-10">
												<input type="text" name="city" value="{{$admins->city}}" class="form-control" placeholder="Enter City" required autofocus> 
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Date of Birth:</label>
											<div class="col-lg-10">
												<input type="date" name="dob" class="form-control" value="{{$admins->dob}}" placeholder="Enter Date of Birth" required autofocus> 
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-lg-2 control-label">Educational Documents:</label>
											<div class="col-lg-10">
												<input type="file" name="documents[]" class="form-control" placeholder="Educational Documents" multiple autofocus> 
											</div>
										</div>
										
										
										
									</div>
										
										
										
								<!-- /select All and filtering options -->
										<div class="text-right">
											<button type="submit" class="btn btn-primary">Add<i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->

		</div>
		<script>
			
			
			$("#role").change(function () {
        var val = this.value;
        if(val == 'mentor')
        {
        	$('#mentors').show(500);
        }
        if(val == 'admin')
        {
        	$('#mentors').hide(500);
        }
    });
		</script>

@endsection
