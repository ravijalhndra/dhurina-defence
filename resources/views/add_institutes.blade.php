@extends('layouts.main')
@section('js_head')
<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/selects/select2.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/plugins/forms/styling/uniform.min.js') }}></script>
	<script type="text/javascript" src={{ URL::asset('assets/js/pages/form_layouts.js') }}></script>

@endsection
@section('content')

	<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Horizontal form options -->
				<div class="row">
					<div class="col-md-12">

						<!-- Basic layout-->
						<form id="login_form" action="{{ url('add_institutes') }}" method="POST" class="form-horizontal">
						{{ csrf_field() }}
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Add Institute</h5>
									
								</div>

								<div class="panel-body">
									<div class="col-md-10  col-md-offset-1">
										<div class="form-group">
											<label class="col-lg-2 control-label">Name:</label>
											<div class="col-lg-10">
												<input type="text" name="name" class="form-control" placeholder="Enter name" required autofocus> 
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Email:</label>
											<div class="col-lg-10">
												<input type="email" name="email" class="form-control" placeholder="Enter Email">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Phone:</label>
											<div class="col-lg-10">
												<input type="text" name="phone" class="form-control" placeholder="Enter Phone">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-2 control-label">Address:</label>
											<div class="col-lg-10">
												<textarea name="address" class="form-control" placeholder="Enter addresss" ></textarea>
											</div>
										</div>

										<div class="text-right">
											<button type="submit" class="btn btn-primary">Add<i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<!-- /basic layout -->

					</div>

					
				</div>
				<!-- /vertical form options -->


				

			</div>
			<!-- /main content -->

		</div>

@endsection
@section('js')
<script>
	$(document).ready(function(){
		$("#login_form").validate({

			rules:{
				name:{
					required:true
				},
				email:{
					required:true,
					email:true
				},
				phone:{
					required:true,
					number:true
				},
				address:{
					required:true
				}
			}
		
		});
	});
</script>
@endsection