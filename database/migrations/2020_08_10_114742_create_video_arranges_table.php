<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoArrangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_arranges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('school_id');
            $table->String('course_id');
            $table->String('subject_id');
            $table->String('videos_id');
            $table->String('publish')->default('true');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_arranges');
    }
}
