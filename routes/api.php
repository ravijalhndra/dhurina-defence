<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/signup','Api\AuthController@signup');
Route::post('/login','Api\AuthController@login');
Route::post('/verify','Api\AuthController@verify');

Route::post('/get_sector','Api\AuthController@get_sector');
Route::post('/get_sector2','Api\AuthController@get_sector2');   //get banner according to ref code.

Route::post('/get_course2','Api\AuthController@get_course2');


Route::post('/get_course','Api\AuthController@get_course');
Route::post('/get_event','Api\AuthController@get_event');
Route::get('/get_live_video','Api\AuthController@get_live_video');
Route::post('/get_video','Api\AuthController@get_video');

Route::post('/get_all_video','Api\AuthController@get_all_video');
Route::post('/about','Api\AuthController@about');

//Vimeo video
Route::post('/vimeo_video_get','Api\AuthController@vimeo_video_get');
Route::post('/vimeo_detail','Api\AuthController@vimeo_detail');

//Pagination
Route::post('/vimeo_video_get_2','Api\AuthController@vimeo_video_get_2');
Route::post('/demovimeo_video_get_2','Api\AuthController@demovimeo_video_get_2');

Route::post('/forgot_password','Api\AuthController@forgot_password');
Route::post('/reset','Api\AuthController@reset');
Route::post('/verify_otp','Api\AuthController@verify_otp');
Route::post('/check_refcode','Api\AuthController@check_refcode');
Route::post('/get_quality_video','Api\AuthController@get_quality_video');
// Route::post('/get_thumbnail_video','Api\AuthController@get_thumbnail_video');

Route::post('/version_update','Api\AuthController@version_update');

Route::post('/transcoder','Api\AuthController@transcoder');
Route::post('/video_transfer','Api\AuthController@video_transfer');
Route::post('/video_transfer_quality','Api\AuthController@video_transfer_quality');
Route::post('/video_transfer_thumb','Api\AuthController@video_transfer_thumb');

//Video comment
Route::post('/insert_comment','Api\AuthController@insert_comment');
Route::post('/get_comment','Api\AuthController@get_comment');
Route::post('/delete_comment','Api\AuthController@delete_comment');
Route::post('/get_subject','Api\AuthController@get_subject');
// Route::post('/get_test','Api\AuthController@get_test');
Route::post('/get_topics','Api\AuthController@get_topics');


//pdf
Route::post('/get_pdf','Api\AuthController@get_pdf');
Route::post('/get_pdf2','Api\AuthController@get_pdf2');

//update player 
Route::post('/update_player','Api\AuthController@update_player');

//One signal
Route::post('/notify','Api\AuthController@notify');
Route::post('/get_notify','Api\AuthController@get_notify');

//Get topic
Route::post('/get_topic','Api\AuthController@get_topic');
Route::post('/get_new_topic','Api\AuthController@get_new_topic');
    //solve pdf count
Route::post('/get_new_topic2','Api\AuthController@get_new_topic2');

//Testing Controllers
    //Vimeo
    Route::post('/vimeo_video','Api\TestController@vimeo_video');
    Route::post('/get_vimeo_video','Api\TestController@get_vimeo_video');

    //move course ID to admin
    Route::post('/course_move','Api\TestController@course_move');
    Route::post('/access_course_move','Api\TestController@access_course_move');

    //Get thumanil url in Vimeo rest api
    Route::post('/get_thumnail','Api\TestController@get_thumnail');
   
    //AWS to vimeo
    Route::post('/aws_to_vimeo','Api\TestController@aws_to_vimeo');

    //move video school access
    Route::post('/move_video_access','Api\TestController@move_video_access');
    Route::get('/duplicate_user','Api\TestController@duplicate_user');
    Route::post('/course_mobile','Api\TestController@course_mobile');

    Route::post('/check_pdf','Api\TestController@check_pdf');    
    Route::post('/dummy','Api\TestController@dummy');


//Video Controller
    Route::post('/video_duration','Api\VideoController@duration');
    
//Payment Controller
    Route::post('/generate_token','Api\PaymentController@generate_token');
    Route::post('/order_response','Api\PaymentController@order_response');
    Route::post('/webhook','Api\PaymentController@webhook');
    Route::post('/cancel_payment','Api\PaymentController@cancel_payment');
    Route::post('/webhook_curl','Api\PaymentController@webhook_curl');
  
// NOte and E-book    
    Route::post('/get_notes','Api\NoteController@get_notes');
    Route::post('/get_notes1','Api\NoteController@get_notes1');


//Core Controller
    Route::post('/getusercomment','Api\CoreController@getusercomment');
    Route::post('/get_common_comment','Api\CoreController@get_common_comment');
    Route::post('/insert_common_comment','Api\CoreController@insert_common_comment');
    Route::post('/appversion','Api\CoreController@appversion');
    Route::post('/like_post','Api\CoreController@like_post');
    Route::post('/profile_update','Api\CoreController@profile_update');
    Route::post('/update_players','Api\CoreController@update_players');
    Route::post('/homedata','Api\CoreController@homedata');
    Route::post('/login2','Api\CoreController@login2');
    Route::post('/getuserinfo','Api\CoreController@getuserinfo');
    Route::post('/get_Premiumtest','Api\CoreController@get_Premiumtest');
    Route::post('/postcomment_ingroup','Api\CoreController@postcomment_ingroup');
    Route::post('/gtq','Api\CoreController@gtq');
    Route::post('/insert_result','Api\CoreController@insert_result');
    Route::post('/test_rank','Api\CoreController@test_rank');
    Route::post('/check_test_submission','Api\CoreController@check_test_submission');

    Route::post('/get_post','Api\CoreController@get_post');
    Route::post('/delete_groupcomment','Api\CoreController@delete_groupcomment');
    Route::post('/likecomment','Api\CoreController@likecomment');   
    Route::post('/replies_delete','Api\CoreController@replies_delete');  
    //get question
    Route::post('/get_question','Api\CoreController@get_question');  
 


    Route::post('/get_checksum','Api\Common@get_checksum');
    Route::post('/insert_review','Reviewcontroller@insert_review');
    Route::post('/get_collages','Api\panelcontroller@get_collages');
    Route::post('/collage','Api\CollegeController@insert');
    Route::post('/getchallenge','Api\ChallengeController@getChallenge');
    Route::post('/getsubject','Api\ChallengeController@getChallengeSubject');
    Route::post('/getcollegesubject','Api\ChallengeController@getChallengeSubjectcollege');
    Route::get('get_stream','Api\ChallengeController@get_Stream');

Route::post('/gettopic','Api\ChallengeController@getTopic');
Route::post('/gettopics','Api\ChallengeController@getTopics');
Route::post('/getopponents','Api\ChallengeController@getOpponents');
Route::post('/getopponents_new','Api\ChallengeController@getOpponents_new');
Route::post('/getopponent_details','Api\ChallengeController@getopponent_details');
Route::post('/getchallenge_question','Api\ChallengeController@getchallenge_question');

Route::post('/competition_list','Api\ChallengeController@competition_list');
Route::post('/accept_challenge','Api\ChallengeController@accept_challenge');
Route::post('/answer_chall_question','Api\ChallengeController@answer_chall_question');
Route::post('/challenge_result','Api\ChallengeController@challenge_result');
Route::post('/challange_questions','Api\ChallengeController@challange_questions');



Route::post('/getsingle_question','Api\ChallengeController@getsingle_question');
// Route::post('/getChallenge','Api\Common@getgroupdetails');

Route::post('/getblockeduser','Api\ChallengeController@getblockeduser');
Route::post('/unblockuser','Api\ChallengeController@unblockuser');
Route::post('/getChallengePoints','Api\ChallengeController@getChallengePoints');
Route::post('/leaderboard','Api\ChallengeController@leaderboard');
Route::post('/withdrawmoney','Api\ChallengeController@withdrawmoney');

Route::post('/answer_chall','Api\ChallengeController@answer_chall87'); //dummy


Route::post('/answer_chall87','Api\ChallengeController@answer_chall87'); //version 3.87 for testing


Route::post('/getgroupdetails','Api\Common@getgroupdetails');


Route::post('/leaderboarddemo','Api\Common@leaderboarddemo');
Route::post('/banner_views','Api\Common@banner_views');


//Add academy info
Route::post('/add_academy_info','Api\AuthController@add_info');

Route::post('/get_test_series','Api\AuthController@get_test_series');
Route::post('/series_test_list','Api\AuthController@series_test_list');

// Route::post('/challenge_timeout','Api\ChallengeController@challenge_timeout');
Route::post('/my_orders','Api\CoreController@my_orders');

Route::post('/top_courses','Api\AuthController@top_courses');







