<?php

use App\Http\Controllers\GroupController;

Auth::routes();

Route::get('chat-webview-front/{id}', 'GeneralController@chat_webview_front')->name('chat_webview_front');


// Route::get('/demo_query','GeneralController@demo_query');

Route::get('/403', function () {
    return view('404');
});

Route::get('/download_csv', 'DemoController@export_csv'); //route to create csv of cgl test
// Route::get('routes','routesController@permission');//view all question of a test

//forgot password route
Route::get('/recover_password', function () {
    return view('recover_password');
});
//forgot password route

Route::get('/admin', function () {
    return view('404');
});

// Route::get('/', function () {
//     return redirect('dashboard');
// });

// Route::get('/login', function () {
//     return redirect('dashboard');
// });

Route::get('/', 'CourseController@index')->name('index');
// Route::get('/', 'CourseController@dashboard')->name('dashboard');
// Route::get('/admin', 'CourseController@dashboard')->name('dashboard');



Route::get('privacy-policy', 'CourseController@privacy_policy')->name('privacy_policy');

 // Forgot password
 Route::post('send_email', 'CourseController@send_email')->name('send_email');
 Route::get('change_password/{id}', 'CourseController@change_password')->name('change_password');
 Route::post('reset_password', 'CourseController@reset_password')->name('reset_password');

 Route::get('upload_req', 'YoutubeController@upload_req')->name('upload_req');

 Route::post('upload_youtube', 'YoutubeController@upload_youtube')->name('upload_youtube');


Route::group(['middleware' => 'auth'], function () {
    
    Route::get('dashboard', function () {
        return view('dashboard');
    });

        //Academy Info
        Route::get('view-academy-info','GeneralController@view_academy_info')->name('view_academy_info');

        // School module    
        Route::get('push', 'CourseController@push')->name('push');

    

        //Add New Courses
        Route::get('add_course', 'CourseController@add_course')->name('add_course');
        Route::post('insert_course', 'CourseController@insert_course')->name('insert_course');
        Route::get('view_course', 'CourseController@view_course')->name('view_course');
        Route::get('edit_course/{id}', 'CourseController@edit_course')->name('edit_course');
        Route::post('update_course', 'CourseController@update_course')->name('update_course');
        Route::post('delete_course','CourseController@delete_course')->name('delete_course');
        Route::post('publish','CourseController@publish')->name('publish');

        //Add New live event
        Route::get('add_event', 'CourseController@add_event')->name('add_event');
        Route::post('insert_event', 'CourseController@insert_event')->name('insert_event');
        Route::get('view_event', 'CourseController@view_event')->name('view_event');
        Route::get('edit_event/{id}', 'CourseController@edit_event')->name('edit_event');
        Route::post('update_event', 'CourseController@update_event')->name('update_event');
        Route::post('delete_event','CourseController@delete_event')->name('delete_event');
        Route::post('publish_event','CourseController@publish_event')->name('publish_event');

        //Add new School
        Route::get('add_school', 'CourseController@add_school')->name('add_school');
        Route::post('insert_school', 'CourseController@insert_school')->name('insert_school');
        Route::get('view_school', 'CourseController@view_school')->name('view_school');
        Route::get('edit_school/{id}', 'CourseController@edit_school')->name('edit_school');
        Route::post('update_school', 'CourseController@update_school')->name('update_school');
        Route::post('delete_school','CourseController@delete_school')->name('delete_school');
        Route::get('sort_video/{course?}/{subject?}','CourseController@sort_video')->name('sort_video');
        Route::post('sort_videos','CourseController@sort_videos')->name('sort_videos');


        //School
        Route::get('add_subject', 'CourseController@add_subject')->name('add_subject');
        Route::post('insert_subject', 'CourseController@insert_subject')->name('insert_subject');
        Route::get('view_subject', 'CourseController@view_subject')->name('view_subject');
        Route::get('edit_subject/{id}', 'CourseController@edit_subject')->name('edit_subject');
        Route::post('update_subject', 'CourseController@update_subject')->name('update_subject');
        Route::post('delete_subject','CourseController@delete_subject')->name('delete_subject');

        //video
            Route::get('add_video', 'CourseController@add_video')->name('add_video');
            Route::post('insert_video', 'CourseController@insert_video')->name('insert_video');
            Route::get('view_video/{course?}/{subject?}/{filter?}', 'CourseController@view_video')->name('view_video');
            Route::get('edit_video/{id}', 'CourseController@edit_video')->name('edit_video');
            Route::post('update_video', 'CourseController@update_video')->name('update_video');
            Route::post('delete_video','CourseController@delete_video')->name('delete_video');
            Route::post('publish_video','CourseController@publish_video')->name('publish_video');

            Route::post('school_publish_video','CourseController@school_publish_video')->name('school_publish_video');


        //Video Comment
            Route::get('get_comment/{id}', 'CourseController@get_comment')->name('get_comment');
            Route::post('delete_comment', 'CourseController@delete_comment')->name('delete_comment');

        //Vimeo Controller
            //Upload video in Vimeo panel then assign there 
            Route::get('Vimeo-video', 'VimeoController@vimeo_video')->name('vimeo_video');
            Route::get('Assign-video/{id}', 'VimeoController@assign_video')->name('assign_video');
            Route::post('insert_assign_video', 'VimeoController@insert_assign_video')->name('insert_assign_video');

            //upload video in Edudream panel 
            Route::post('insert_vimeo_video', 'VimeoController@insert_vimeo_video')->name('insert_vimeo_video');

        //About       
            Route::get('About', 'CourseController@about')->name('about');
            Route::post('insert_about', 'CourseController@insert_about')->name('insert_about');

        //Assign course

            Route::get('Assign-Course', 'CourseController@assign_course')->name('assign_course');
            Route::post('insert_assign', 'CourseController@insert_assign')->name('insert_assign');
            Route::get('Assign-Course-view/{filter?}/{type?}', 'CourseController@assign_course_view')->name('assign_course_view');
            Route::post('delete_assign', 'CourseController@delete_assign')->name('delete_assign');
            Route::get('assign-ebook-view/{filter?}/{type?}', 'CourseController@assign_ebook_view')->name('assign_ebook_view');
            //Assign course/note/ebook manual
            Route::get('assign-manully-course', 'CourseController@assign_manully_course')->name('assign_manully_course');
            Route::post('assign_manully', 'CourseController@assign_manully')->name('assign_manully');

        //User detail
            Route::get('view_users', 'SuperController@view_users')->name('view_users');
            Route::post('get_city', 'SuperController@get_city')->name('get_city');
            Route::post('user_search', 'SearchController@user_search')->name('user_search');
            Route::get('edit_user/{id}', 'SuperController@edit_user')->name('edit_user');
            Route::post('update_user', 'SuperController@update_user')->name('update_user');

        //Payment
            Route::get('view_payment/{status?}/{filter?}', 'CashfreeController@view_payment')->name('view_payment');
       
        //Sectors
            Route::get('add_sector', 'SectorController@add_sector')->name('add_sector');
            Route::post('insert_sector', 'SectorController@insert_sector')->name('insert_sector');
            Route::get('view_sector', 'SectorController@view_sector')->name('view_sector');
            Route::get('edit_sector/{id}', 'SectorController@edit_sector')->name('edit_sector');
            Route::post('update_sector', 'SectorController@update_sector')->name('update_sector');
            Route::post('delete_sector','SectorController@delete_sector')->name('delete_sector');
            Route::get('sector_move','SectorController@sector_move')->name('sector_move');


        //Test  
            Route::get('view_test/{course?}/{subject?}/{filter?}', 'TestController@view_test')->name('view_test');
            
            Route::get('test_comment/{id}', 'TestController@test_comment')->name('test_comment');
           
            Route::post('test_reply', 'TestController@test_reply')->name('test_reply');
            Route::post('delete_reply_test', 'TestController@delete_reply_test')->name('delete_reply_test');

            Route::get('add_reply_post/{id}/{type?}', 'TestController@add_reply_post')->name('add_reply_post');
            Route::post('add_reply','TestController@add_reply')->name('add_reply');
            Route::post('delete_reply_comment', 'TestController@delete_reply_comment')->name('delete_reply_comment');

            Route::get('test_time', 'TestController@test_time')->name('test_time');

        // Arrange video
            Route::get('playlist_subject/{course_id}', 'VideoArrange@playlist_subject')->name('playlist_subject');
            Route::post('playlist_video', 'VideoArrange@playlist_video')->name('playlist_video');
            Route::post('save_playlist', 'VideoArrange@save_playlist')->name('save_playlist');

        //VIdeo duration
            Route::get('video_duration/{id}', 'VideoArrange@video_duration')->name('video_duration'); 
        

        //Notification
            Route::get('notification', 'NotifyController@notification')->name('notification');
            Route::post('send_notification', 'NotifyController@send_notification')->name('send_notification');

        //Topic
            Route::get('add_topic','CourseController@add_topic')->name('add_topic');
            Route::post('insert_topic','CourseController@insert_topic')->name('insert_topic');
            Route::get('view_topic','CourseController@view_topic')->name('view_topic');
            Route::get('edit_topic/{id}','CourseController@edit_topic')->name('edit_topic');
            Route::post('update_topic','CourseController@update_topic')->name('update_topic');
            Route::post('delete_topic','CourseController@delete_topic')->name('delete_topic');


       //Note and Ebook
            Route::get('add_note','NoteController@add_note')->name('add_note');
            Route::post('insert_note','NoteController@insert_note')->name('insert_note');
            Route::get('view_note/{sector?}/{filter?}','NoteController@view_note')->name('view_note');
            Route::get('edit_note/{id}','NoteController@edit_note')->name('edit_note');
            Route::post('update_note','NoteController@update_note')->name('update_note');
            Route::post('delete_note','NoteController@delete_note')->name('delete_note');
            Route::post('published_note','NoteController@published_note')->name('published_note');

    //    School module


    //routes for show data on dashboard
    Route::post('chart_data', 'GeneralController@chart_data');
    //routes for show data on dashboard

    // route for send custom notification
    Route::post('send_custom_notification', 'GeneralController@send_custom_notification')->middleware('permission:send_custom_notification');
    // route for send custom notification

    //notification routes
    Route::get('send_notification/{id}', 'GeneralController@send_notification')->middleware('permission:send_post_notification');
    Route::get('send_currentAffair_notification/{id}', 'CurrentAffair@send_notification')->middleware('permission:send_CurrentAffair_notification');
    Route::get('send_jobs_notification/{id}', 'JobsController@send_notification')->middleware('permission:send_job_notification');
    Route::get('send_workshop_notification/{id}', 'WorkshopController@send_notification')->middleware('permission:send_workshop_notification');
    Route::get('send_pdf_notification/{id}', 'pdfController@send_notification')->middleware('permission:send_pdf_notification');
    Route::get('send_test_notification/{id}', 'GeneralController@send_test_notification')->middleware('permission:send_test_notification');
    //notification routes ends

    //workshop routes
    Route::get('workshop', 'WorkshopController@ViewDetail')->middleware('permission:workshop');
    Route::get('workshop/registered_users/{id}', 'WorkshopController@show_registered_users')->middleware('permission:workshop_reg_users');
    Route::get('add_workshop', 'WorkshopController@add')->middleware('permission:add_workshop');
    Route::post('add_workshop', 'WorkshopController@store');
    Route::get('edit_workshop/{id}', 'WorkshopController@edit_workshop')->middleware('permission:edit_workshop');
    Route::get('delete_workshop/{id}', 'WorkshopController@delete_workshop')->middleware('permission:delete_workshop');
    Route::get('workshop_detail/{id}', 'WorkshopController@workshop_detail')->middleware('permission:workshop');
    Route::post('edit_workshop/{id}', 'WorkshopController@update_workshop');
    Route::post('workshop_search', 'WorkshopController@search');

    Route::get('export_csv/{id}', 'WorkshopController@export_csv');
    Route::get('create_pdf/{id}', 'WorkshopController@create_pdf');
    // end the routes here for workshop

    //routes  for currentaffair
    Route::get('currentAffair', 'CurrentAffair@view')->name('currentAffair');
    Route::get('currentAffairDetail/{id}', 'CurrentAffair@currentAffairDetail')->name('currentAffair');
    Route::get('editCurrentAffair/{id}', 'CurrentAffair@update_currentAffair')->name('editCurrentAffair');
    Route::get('delete_CurrentAffair/{id}', 'CurrentAffair@delete_currentAffair')->name('delete_CurrentAffair');
    Route::post('editCurrentAffair/{id}', 'CurrentAffair@update_currentAffair_Action');
    Route::post('current_affair_search', 'CurrentAffair@search');
    Route::post('currentAffair_store', 'CurrentAffair@store');
    Route::get('currentAffair_store', 'CurrentAffair@add')->name('currentAffair_store');
    //end routes for current affair

    //rouutes  for jobs category
    Route::get('jobs', 'JobsController@jobs')->middleware('permission:jobs');
    Route::get('add_job', 'JobsController@add_job')->middleware('permission:add_job');
    Route::get('job_detail/{id}', 'JobsController@job_detail')->middleware('permission:jobs');
    Route::post('job_store', 'JobsController@job_store');
    Route::get('edit_job/{id}', 'JobsController@edit_job')->middleware('permission:edit_job');
    Route::post('update_job', 'JobsController@update_job');
    Route::post('delete_job_comment/', 'JobsController@delete_job_comment')->middleware('permission:delete_comment');
    Route::post('delete_job/', 'JobsController@delete_job')->middleware('permission:delete_job');
    Route::post('jobs_search', 'JobsController@search');
    // end routes for job category

    //routes for groups
    Route::get('groups', 'GroupController@view_groups')->middleware('permission:groups');
    Route::get('add_groups', 'GroupController@add_groups')->middleware('permission:add_groups');
    Route::post('store_group', 'GroupController@store_groups');
    Route::get('edit_group/{id}', 'GroupController@edit_groups')->middleware('permission:edit_groups');
    Route::post('update_group', 'GroupController@update_group');
    Route::post('delete_group', 'GroupController@delete_groups')->middleware('permission:delete_groups');
    Route::get('view_follow_group/{id}','GroupController@view_follow_group')->name('view_follow_group');

    //banner
    Route::get('add_banner', 'GroupController@add_banner')->name('add_banner');
    Route::post('insert_banner', 'GroupController@insert_banner');
    Route::get('view_banner', 'GroupController@view_banner');
    Route::get('edit_banner/{id}', 'GroupController@edit_banner')->name('edit_banner');
    Route::post('update_banner', 'GroupController@update_banner')->name('update_banner');
    Route::post('delete_banner','GroupController@delete_banner')->name('delete_banner');
    Route::post('update_banner_pos','GroupController@update_banner_pos')->name('update_banner_pos');
    
    
    //departments
    Route::get('add_department', 'GroupController@add_department')->name('add_department');
    Route::get('view_department', 'GroupController@view_department')->name('view_department');
    Route::post('insert_department', 'GroupController@insert_department');
    Route::get('edit_department/{id}', 'GroupController@edit_department')->name('edit_department');
    Route::post('update_department', 'GroupController@update_department');
    Route::post('delete_department', 'GroupController@delete_department')->name('delete_department');

    //college
    Route::get('add_college', 'GroupController@add_college')->name('add_college');
    Route::get('view_college', 'GroupController@view_college')->name('view_college');
    Route::get('edit_college/{id}', 'GroupController@edit_college')->name('edit_college');
    Route::post('update_college', 'GroupController@update_college');
    Route::post('delete_college', 'GroupController@delete_college')->name('delete_college');
    Route::post('insert_college', 'GroupController@insert_college');


    //Create Meeting Room
    Route::get('add_room', 'GroupController@add_room')->name('add_room');
    Route::get('view_room', 'GroupController@view_room')->name('view_room');
    Route::post('insert_room', 'GroupController@insert_room')->name('insert_room');
    Route::get('edit_room/{id}', 'GroupController@edit_room')->name('edit_room');
    Route::post('update_room', 'GroupController@update_room')->name('update_room');
    Route::post('delete_room', 'GroupController@delete_room')->name('delete_room');

    //Student College
    Route::get('student_college', 'GroupController@student_college');
    Route::get('change_status/{id}/{action}/{type}', 'GroupController@change_status');
    Route::get('edit_student_college/{id}', 'GroupController@edit_student_college')->name('edit_student_college');
    Route::post('update_student_college', 'GroupController@update_student_college');
    Route::post('delete_student_college', 'GroupController@delete_student_college')->name('delete_student_college');
    Route::post('filter_course', 'GroupController@filter_course')->name('filter_course');
    Route::post('filter_student', 'GroupController@filter_student')->name('filter_student');

    // Route::get('categories','GroupController@view_categories')->middleware('permission:categories');
    Route::get('categories', 'GroupController@view_categories');
    Route::get('add_categories', 'GroupController@add_categories');
    // Route::get('add_categories','GroupController@add_categories')->middleware('permission:add_categories');
    Route::post('store_categories', 'GroupController@store_categories');
    // Route::get('edit_categories/{id}','GroupController@edit_categories')->middleware('permission:edit_categories');
    Route::get('edit_categories/{id}', 'GroupController@edit_categories');
    Route::post('update_categories', 'GroupController@update_categories');
    Route::post('delete_categories', 'GroupController@delete_categories');
    // Route::post('delete_categories','GroupController@delete_categories')->middleware('permission:delete_categories');
    //routes for groups ends

    //routes for pdf
        Route::get('pdf', 'pdfController@add')->name('add_pdf');
        Route::post('pdf', 'pdfController@store_data');
        Route::post('pdf_update', 'pdfController@edit_data')->name('pdf_update');
        Route::get('view_pdf', 'pdfController@view_pdf')->name('view_pdf');
        Route::post('delete_pdf', 'pdfController@delete_pdf')->name('delete_pdf');
        Route::post('pdf_search', 'pdfController@search');

        Route::get('view_pdf2/{course?}/{subject?}/{filter?}', 'pdfController@view_pdf2')->name('view_pdf2');
        Route::get('edit_pdf/{id}', 'pdfController@edit_pdf')->name('edit_pdf');
        Route::post('update_pdf', 'pdfController@update_pdf')->name('update_pdf');

    //roues for pdf ends

    Route::post('add_admin', 'SuperController@add_admin');
    Route::get('add_admin', ['middleware' => ['permission:add_admin'], 'uses' => 'SuperController@addAdmin']);
    Route::get('edit_admin/{id}', 'SuperController@edit_admin')->middleware('permission:edit_admin');
    Route::post('update_admin', 'SuperController@update_admin');
    Route::get('view_admin', 'SuperController@view_admin')->middleware('permission:view_admin');
    Route::post('delete_admin', 'SuperController@delete_admin')->middleware('permission:delete_admin');

    Route::post('block_user', 'SuperController@block_user')->name('block_user');
    Route::post('block_comment', 'SuperController@block_comment')->name('block_comment');


    

    // post routes
    Route::get('view_posts', 'GeneralController@view_posts')->middleware('permission:view_post');
    Route::get('post/{id}', 'GeneralController@view_posts_detail')->middleware('permission:view_post');
    Route::get('add_posts', 'GeneralController@add_posts')->middleware('permission:add_post')->middleware('permission:add_post');
    Route::post('add_posts', 'GeneralController@submit_posts');
    Route::get('edit_post/{id}', 'GeneralController@edit_posts')->middleware('permission:edit_post');
    Route::post('edit_post/{id}', 'GeneralController@update_post');
    Route::post('delete_post', 'GeneralController@delete_post')->middleware('permission:delete_post');
    Route::post('delete_post_comment', 'GeneralController@delete_post_comment')->middleware('permission:delete_post_comment');
    Route::post('posts_search', 'GeneralController@search');
    //post routes ends

    //user post routes
    Route::get('view_user_posts', 'GeneralController@view_user_posts')->name('view_user_posts');
    Route::get('approve_user_post/{id}', 'GeneralController@approve_user_post');
    Route::get('disapprove_user_post/{id}', 'GeneralController@disapprove_user_post');

    //user post routes ends

    // online test routes
    Route::get('test', 'GeneralController@view_tests');
    Route::get('view_college_tests/{id}', 'GeneralController@view_college_tests')->name('view_college_tests');
    Route::get('view_college_list', 'GroupController@view_college_list')->name('view_college_list');

    Route::get('view_unpublish_test', 'GeneralController@view_unpublish_test');


    //Test Series
    Route::get('add-test-series','GeneralController@add_test_series')->name('add_test_series');
    Route::post('add-test-series','GeneralController@submit_test_series')->name('submit_test_series');
    Route::get('view-test-series','GeneralController@view_test_series')->name('view_test_series');
    Route::post('publish-test-series', 'GeneralController@publish_test_series')->name('publish_test_series');
    Route::post('delete_test_series', 'GeneralController@delete_test_series')->name('delete_test_series');
    Route::get('edit-series/{id}','GeneralController@edit_series')->name('edit_series');
    Route::post('update-series','GeneralController@update_series')->name('update_series');
    
    Route::get('array', 'GeneralController@array')->name('array');

    Route::get('add_test', 'GeneralController@add_test')->name('add_test');
    Route::post('add_test', 'GeneralController@submit_test')->name('submit_test');
    Route::get('edit_test/{id}', 'GeneralController@edit_test')->name('edit_test');
    Route::post('update_test', 'GeneralController@update_test');
    Route::post('test_search', 'GeneralController@test_search');
    Route::post('test_searchnew', 'GeneralController@test_search');
    Route::get('publish_test/{id}', 'GeneralController@publish_test')->name('publish_test');
    Route::get('unpublish_test/{id}', 'GeneralController@unpublish_test')->name('unpublish_test');
    Route::get('make_test_zip/{id}', 'GeneralController@make_test_zip');

    Route::post('delete_test', 'GeneralController@delete_test')->name('delete_test');

    Route::post('get_course', 'GeneralController@get_course')->name('get_course');
    Route::post('/get_stream', 'GeneralController@get_stream')->name('get_stream');

    Route::get('questions/{id}', 'GeneralController@view_questions')->name('view_question'); //view all question of a test
    Route::get('poll-answears/{id}', 'GeneralController@poll_questions')->name('view_question'); //view all question of a test
   
    Route::get('question/{question_id}', 'GeneralController@view_full_question')->name('view_question'); //view details of a single question
    Route::get('add_questions/{id}', 'GeneralController@add_questions')->name('add_question'); // add questions to a test
    Route::get('edit_question/{id}', 'GeneralController@edit_question')->name('edit_question'); //edit question of a test
    Route::post('add_questions', 'GeneralController@submit_questions'); // on question submit
    Route::post('edit_question', 'GeneralController@update_question'); // on question update
    Route::post('delete_question', 'GeneralController@delete_question')->name('delete_question');
    Route::get('add_demo_test', 'GeneralController@add_demo_test');


    Route::get('bulk_question', 'GeneralController@bulk_question')->name('bulk_question');
    Route::post('submit_bulk', 'GeneralController@submit_bulk')->name('submit_bulk');
    Route::post('publish_testSeries', 'GeneralController@publish_testSeries')->name('publish_testSeries');


    //premium tests

    Route::get('premium_test', 'PremiumTestController@view_tests')->middleware('permission:view_test');
    Route::get('add_premium_test', 'PremiumTestController@add_test')->middleware('permission:add_test');
    Route::post('add_premium_test', 'PremiumTestController@submit_test')->name('submit_premium_test');
    Route::get('edit_premium_test/{id}', 'PremiumTestController@edit_test')->middleware('permission:edit_test');
    Route::post('update_premium_test', 'PremiumTestController@update_test')->name('update_premium_test');
    Route::post('premium_test_search', 'PremiumTestController@test_search');

    //Challenge
    Route::get('edit_challenge_point','ChallengeController@edit_challenge');
    Route::post('edit-challenge-point','ChallengeController@_edit_challenge')->name('edit_challenge_point');
    Route::get('view_challenge', 'ChallengeController@view_challenge');
    Route::get('pending_challenge', 'ChallengeController@pending_challenge');
    Route::get('leadership', 'ChallengeController@leadership');
    Route::post('filter_leadership', 'ChallengeController@filter_leadership')->name('filter_leadership');
    Route::get('challenge-level', 'ChallengeController@challengelevel')->name('challengelevel');
    Route::get('add_challenge_level', 'ChallengeController@add_challenge_level');
    Route::get('get_last_data', 'ChallengeController@get_last_data')->name('get_challenge_data');
    Route::post('add_challenge_level_data', 'ChallengeController@_add_challenge_data')->name('add_challenge_level_data');
    Route::get('edit_challenge_level/{id}', 'ChallengeController@edit_clevel')->name('edit_challenge_level');
    Route::post('update_clevel', 'ChallengeController@update_clevel')->name('update_challenge_level');
    Route::post('edit_clevel', 'ChallengeController@_get_clevel_data')->name('get_clevel_data');
    Route::get('add_subject_question', 'ChallengeController@add_sub_ques')->name('add_sub_ques');
    Route::post('submit_subject_question','ChallengeController@submit_sub_question')->name('submit_sub_question');
    Route::get('subject_question','ChallengeController@view_sub_question')->name('view_sub_question');
    Route::get('verify_question/{id}','ChallengeController@verify_question')->name('sub_question');  
    Route::post('delete_sub_question', 'ChallengeController@delete_question')->name('delete_sub_question');  
   
    //Payment
    // Route::get('view_payment', 'PaymentController@view_payment')->name('view_payment');


    Route::get('pending-requests', 'ChallengeController@pendingrequests')->name('pendingrequests');
    Route::get('assign-amount/{id}', 'ChallengeController@assignamount')->name('assignamount');
    // Route::get('our_mentors','MentorController@our_mentors')->middleware('permission:our_mentors');
    // Route::post('delete_mentor','MentorController@delete_mentor')->middleware('permission:delete_mentor');
    // Route::post('add_mentor','MentorController@add_mentor')->middleware('permission:add_mentor');
    // Route::get('edit_mentor/{id}','MentorController@edit_mentor')->middleware('permission:edit_mentor');

    // Route::get('premium_test','PreminumTestController@view_tests')->name('premium_test')->middleware('permission:view_premium_test');
    // Route::get('add_premium_test','PreminumTestController@add_test')->name('add_premium_test')->middleware('permission:add_premium_test');
    // Route::post('add_premium_test','PreminumTestController@submit_test')->name('submit_premium_test');
    // Route::get('edit_premium_test/{id}','PreminumTestController@edit_test')->name('edit_premium_test')->middleware('permission:edit_premium_test');
    // Route::post('update_premium_test','PreminumTestController@update_test')->name('update_premium_test');
    // Route::post('premium_test_search','PreminumTestController@test_search');

    // onlint test routes ends

    //group comments starts
    Route::post('group_comments', 'GroupController@show')->middleware('permission:view_group_comments'); //show group comments
    Route::post('group_comments_search', 'GroupController@search'); //show group comments
    Route::post('delete_group_comment', 'GroupController@delete')->name('delete_group_comments');
    //group commments ends

    //routes for show comments in all sections
    Route::get('ajax2', 'GeneralController@ajax');
    Route::get('delete_reply', 'GeneralController@delete_reply')->middleware('permission:delete_comment');
    Route::get('delete_comment', 'GeneralController@delete_comment')->middleware('permission:delete_comment');
    //routes for show comments in all sections ends

    //routes for change password
    Route::get('change_password', function () {
        return view('change_password');
    });

    Route::post('change_password', 'GeneralController@change_password');
    //routes for change password ends

    Route::get('test/users_result/{id}', 'GeneralController@users_results')->name('users_test_result');

    Route::get('test/users_result_csv/{id}', 'GeneralController@users_results_csv');

    Route::post('/app_user_csv', 'GeneralController@app_user_csv')->name('app_user_csv');
});
